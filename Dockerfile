FROM php:5.6-fpm-alpine


RUN docker-php-ext-install mysql pdo pdo_mysql

COPY php.ini "$PHP_INI_DIR/conf.d/"
CMD [ "php","-S","0.0.0.0:8000","-t","app" ]
EXPOSE 8000