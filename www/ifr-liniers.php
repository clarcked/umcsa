<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<?php
include 'includes/funciones.php';
include 'includes/MySQL.php';
$link = Conectarse();

if (isset($_GET["f"])) {
    $fecha = $_GET["f"];
} else {
    $fecha = date('Y-m-d');
}
//echo $fecha;
?>

<script type="text/javascript">
    function cambiaClase(pId, pClase) {
        document.getElementById(pId).className = pClase;
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Expires" CONTENT="0"> 
            <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
                <script type="text/javascript" src="js/epoch_v202_es/epoch_classes.js"></script>
                <link rel="stylesheet" type="text/css" href="js/epoch_v202_es/epoch_styles.css" />
                <title>:: UMC S.A. | Negocios Agropecuarios ::</title>
                <link rel="stylesheet" href="css/frontend.css" type="text/css" media="screen" />
                </head>	
                <body topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" rightmargin="0" onload="mLoad();" onresize="mLoad();">
                <script type="text/javascript">
                    var bas_cal, dp_cal, ms_cal; // declare the calendars as global variables
                    window.onload = function() {
                        /*initialize any calendars on the page - in this case 3.*/
                        //bas_cal = new Epoch('bas_cal', 'flat', document.getElementById('bas_cal'));
                        //dp_cal = new Epoch('dp_cal', 'popup', document.getElementById('date_field'));
                        popupcalendarD = new Epoch('cal', 'popup', document.getElementById('fecha_desde'), false);
                        //popupcalendarH = new Epoch('cal', 'popup', document.getElementById('fecha_hasta'), false);
                        //ms_cal = new Epoch('ms_cal', 'flat', document.getElementById('ms_cal'), true);
                        /*checkInputs();
                         if (fechaDesde) {
                         completaFechas(fechaDesde, fechaHasta);
                         }*/

                    };
                </script>
                    <div align="center" id="todoDiv" style="position:absolute; top:0; left:0; z-index:0; opacity:0.0;filter:alpha(opacity=0); background-color:#000;width:100%;"></div>	
                    <div id="divConteTodo">
                        <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="32">
                            <tr>
                                <td width="20"></td>
                                <td width="1" valign="middle" class="txtMenu" style="color: 000;"><b>NUESTRAS VENTAS Y ENTRADA EN LINIERS</b></td>
                                <td width="30"></td>
                                <script type="text/javascript">
                                function abreDS(){
                                    varF = document.getElementById('fecha_desde').value
                                    pFechaArr = varF.split('/');
                                    window.open('http://www.delsector.com/verprecios.php?f='+pFechaArr[2]+'-'+pFechaArr[1]+'-'+pFechaArr[0]);
                                }    
                                function cambiaListado(pTipo){
                                    if(pTipo=='1'){
                                        varTipo='&lisTipo=1';
                                        varLista = '7';
                                    }else{
                                        varTipo='';
                                        varLista = '6';
                                    }
                                    varfecha= document.getElementById('fecha_desde').value;
                                    //alert(varfecha);
                                    varLista='http://www.mercadodeliniers.com.ar/dll/hacienda1.dll/haciinfo00000'+ varLista +'?txtFECHA='+ varfecha +'&lisConsignatario=48'+ varTipo +'&CP=&LISTADO=SI';
                                    iframeL = document.getElementById('ifrLiniers');
                                    iframeL.src = varLista;
                                }
                                </script>
                                <td width="1">
                                    <table cellpadding="0" cellspacing="0" width="100%" height="100%" border="0">
                                        <tr>
                                            <td width="1" style="color: #000; font-family: Trebuchet MS, Verdana; font-size: 10pt; text-transform: uppercase;">Fecha</td>
                                            <td width="10"></td>
                                            <td hight="1" width="1"><img src="images/calendar-icon.png" onclick="popupcalendarD.toggle('fast');" style="cursor:pointer;"></img></td>
                                            <td width="4"></td>
                                            <td width="1"><input type="text" id="fecha_desde" value="<?php echo cambiaf_a_normal($fecha) ?>" style="width:70px; color:#666666; font-weight:bold; text-align:center; font-size:8pt;"/></td>
                                            <td>&nbsp;</td>
                                            <td height="1" width="200" valign="middle" style="color: #8c433d; font-family: Verdana; font-size: 10pt; text-transform: uppercase; text-align: right; vertical-align: middle;">
                                                    <b>
                                                    <div id="entradaml" style="cursor:pointer; float:left; vertical-align: middle; padding: 8px 12px 8px 12px;" onmouseover="this.style.textDecoration = 'underline';" onmouseout="this.style.textDecoration = 'none';" onclick="cambiaListado('2');">ENTRADA</div><div style="float:left;"><div style="float:left; background-color:#ffffff; width:5px; height:32px;"></div></div>
                                                    <div id="ventasml" style="cursor:pointer; float:left; vertical-align: middle; padding: 8px 12px 8px 12px;" onmouseover="this.style.textDecoration = 'underline';" onmouseout="this.style.textDecoration = 'none';" onclick="cambiaListado('1');">VENTAS</div>
                                                    <div id="" onclick="abreDS();" style="cursor:pointer; float:left; vertical-align: middle; padding: 8px 12px 8px 12px;" onmouseover="this.style.textDecoration = 'underline';" onmouseout="this.style.textDecoration = 'none';">PRECIOS</div></b>
                                            </td>
                                        </tr>
                                    </table>     
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding:20px; margin: auto;">
                                    <iframe frameborder="0" scrolling="yes" id="ifrLiniers" marginheight="0" marginwidth="0" width="96%" height="290" src="http://www.mercadodeliniers.com.ar/dll/hacienda1.dll/haciinfo000006?txtFECHA=<?php echo cambiaf_a_normal($fecha)?>&lisConsignatario=48&CP=&LISTADO=SI"></iframe>
                                </td>
                            </tr>
                        </table>                   
                    </div>  
                </body>
                </html>