﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<?php
include 'includes/MySQL.php';

Conectarse();

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $locaselected = '';
} else {
    $locaId = '';
    $locaselected = 'selected';
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
} else {
    $traza = '';
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
} else {
    $garpta = '';
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
} else {
    $miomio = '';
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
} else {
    $cate = '';
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
} else {
    $lotesel = '';
}

if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
} else {
    $conjuntolote = '';
}

$miURLsinP = $_SERVER['SCRIPT_NAME'];

$miURL = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];

//echo $miURL;
// maximo por pagina

$limit = 18;

// pagina pedida
if (isset($_GET["pag"])) {
    $pag = (int) $_GET["pag"];
} else {
    $pag = (int) 0;
}
if ($pag < 1) {
    $pag = 1;
}
$offset = ($pag - 1) * $limit;

if (isset($_GET["remaid"])) {
    $remaid = (int) $_GET["remaid"];
} else {
    ?>	
    <script type="text/javascript">
        location.href = 'index.php'
    </script>
    <?php
}

//Busqueda consultas--------

$mesesArr = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
$diasArr = array("DOMINGO", "LUNES", "MARTES", "MI&Eacute;RCOLES", "JUEVES", "VIERNES", "S&Aacute;BADO");


//---------------------------

$sqlwhere = "";
$sqlwherecl = "";
if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
    $sqlwhere = $sqlwhere . "AND lotes.conjunto_lote_id =  '$conjuntolote' ";
    $sqlwherecl = $sqlwherecl . "AND lts.conjunto_lote_id =  '$conjuntolote' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $sqlwhere = $sqlwhere . "AND lotes.localidad =  '$locaId' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
    $sqlwhere = $sqlwhere . "AND lotes.id =  '$lotesel' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
    $sqlwhere = $sqlwhere . "AND lotes.trazabilidad =  '$traza' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
    $sqlwhere = $sqlwhere . "AND lotes.categoria =  '$cate' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
    if ($garpta == 'SI') {
        $sqlwhere = $sqlwhere . "AND (lotes.garrapata =  '$garpta' OR lotes.garrapata =  'ZONA DE LUCHA')";
    } else {
        $sqlwhere = $sqlwhere . "AND lotes.garrapata =  '$garpta'";
    }
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
    $sqlwhere = $sqlwhere . "AND lotes.mio_mio =  '$miomio'";
} else {
    $sqlwhere = $sqlwhere . "";
}

$sqlConjLot="SELECT
conjuntos_lotes.id,
conjuntos_lotes.remate_id,
conjuntos_lotes.titulo,
conjuntos_lotes.activo
FROM
conjuntos_lotes
WHERE 
conjuntos_lotes.remate_id = $remaid";


$sqlLT = "SELECT
lts.remate_id AS rmtid,
lts.conjunto_lote_id,
lts.orden AS ltorden,
lts.codigo AS ltcert,
lts.id AS ltid,
lts.categoria AS ltcate
FROM
lotes AS lts
WHERE
lts.remate_id = '".$remaid."' ".$sqlwherecl." ORDER BY ifnull(ltorden, ltcert)";

//echo $sqlLT;

$rsLT = mysql_query($sqlLT);
$cuenta = mysql_num_rows($rsLT);
//echo $cuenta;
$innerselLotes = '';
$lotescuenta = 0;
while ($rowLT = mysql_fetch_array($rsLT)) {
    $lotescuenta+=1;
    if ($lotescuenta == 1) {
        if ($lotesel == '') {

            $innerselLotes = $innerselLotes . '<option value="" selected>TODOS</option>';
        } else {

            $innerselLotes = $innerselLotes . '<option value="">TODOS</option>';
        }
    }

    $ordenLT = $rowLT['ltorden'];
    if ($ordenLT != '') {
        if (substr($ordenLT, 0, 2) == "00") {
            $ordenLT = str_replace("00", "", $ordenLT);
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = str_replace("0", "", $ordenLT);
            }
        } else {
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = substr_replace($ordenLT, "", 0, 1);
            }
        }
    }

    $ordenInner = '';

    if ($rowLT['ltorden'] == '') {
        $ordenInner = 'Certif. N&ordm; ' . $rowLT['ltcert'];
    } else {
        $ordenInner = 'N&ordm; ' . $ordenLT;
    }

    if ($rowLT['ltid'] == $lotesel) {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '" selected>' . $ordenInner . '</option>';
    } else {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '">' . $ordenInner . '</option>';
    }
}



$sqlCAT = "SELECT DISTINCT
SQL_CALC_FOUND_ROWS categorias_ganado.id,
categorias_ganado.id AS cateidselect,
categorias_ganado.nombre AS catenomselect,
categorias_ganado.descripcion,
lotes.categoria,
lotes.remate_id
FROM
categorias_ganado
Inner Join lotes ON lotes.categoria = categorias_ganado.id
WHERE
lotes.remate_id =  '$remaid' AND
categorias_ganado.id IN  ( lotes.categoria)";

$sqlRema = "SELECT
remates.id,
remates.numero,
remates.titulo,
remates.banner_g,
remates.banner_ch,
remates.fecha,
remates.hora_inicio,
remates.hora_fin,
remates.zona,
remates.lugar,
remates.locacion,
remates.descripcion,
remates.yacare_visible,
remates.banner_g,
remates.banner_g_visible,
remates.banner_g_z_index,
remates.banner_g_pos_x,
remates.banner_g_pos_y,
remates.banner_g_link,
remates.cantidad,
remates.activo,
remates.video_final_1,
remates.video_titu_1,
remates.video_final_2,
remates.video_titu_2,
remates.video_final_3,
remates.video_titu_3,
remates.video_final_4,
remates.video_titu_4,
remates.video_final_5,
remates.video_titu_5,
remates.archivo,
remates.precios,
remates.mapa_img
FROM
remates
WHERE
remates.id =  '$remaid'";

$sqlCompr = "SELECT
lotes.orden
FROM
lotes
WHERE
remate_id = '$remaid' AND
orden IS NOT NULL";

$rsCompr = mysql_query($sqlCompr);
$cuentaCompr = mysql_num_rows($rsCompr);

if ($cuentaCompr > 0) {
    $consulorder = "ORDER BY ifnull(lotes.orden, catenombre)";
} else {
    $consulorder = "ORDER BY catenombre";
}


$sqlLugar = "SELECT 
SQL_CALC_FOUND_ROWS lotes.id,
lotes.id,
lotes.provincia,
lotes.lugar,
provincias.id AS provid,
provincias.nombre AS provinombre,
localidades.id as locaid,
localidades.nombre as locanombre
FROM
lotes
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
WHERE
lotes.remate_id =  '$remaid'
GROUP BY lotes.localidad
ORDER BY provinombre ASC, locanombre ASC";



$rsLugar = mysql_query($sqlLugar);
$cuentaLugar = mysql_num_rows($rsLugar);
$lugarcombo = '';
$countRowlugar = 0;
while ($rowLugar = mysql_fetch_array($rsLugar)) {
    if ($locaId == $rowLugar['locaid']) {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '" selected>' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    } else {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '">' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    }
}

$sql = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.orden,
lotes.id AS loteid,
lotes.remate,
lotes.remate_id,
lotes.conjunto_lote_id,
lotes.tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.venta,
lotes.venta_id,
lotes.codigo,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.provincia,
lotes.localidad,
lotes.lugar,
lotes.video,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
localidades.id,
localidades.nombre AS locanombre,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
remates.id AS remaid,
remates.fecha,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre,
videos_lotes.id AS vlid,
videos_lotes.remate_id AS vlrid,
videos_lotes.lote_id AS vllid,
videos_lotes.video AS vlv
FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Left Join videos_lotes ON videos_lotes.lote_id = lotes.id
WHERE
lotes.remate_id =  '$remaid' 
" . $sqlwhere . "

" . $consulorder . " 
 
LIMIT $offset, $limit";

//echo $sql;

$rs = mysql_query($sql);
$sqlTotal = "SELECT FOUND_ROWS() as total";
$rsTotal = mysql_query($sqlTotal);
$rowTotal = mysql_fetch_assoc($rsTotal);
// Total de registros sin limit
$total = $rowTotal["total"];

$rsCAT = mysql_query($sqlCAT);

$sqlCATTotal = "SELECT FOUND_ROWS() as total";
$rsCATTotal = mysql_query($sqlCATTotal);
$rowCATTotal = mysql_fetch_assoc($rsCATTotal);
// Total de registros sin limit
$totalCAT = $rowCATTotal["total"];


$rsRema = mysql_query($sqlRema);
?>
<script type="text/javascript" src="includes/funcFondopopup.js"></script>
<script type="text/javascript">
    function cambiaFondo(pId, pColor) {
        document.getElementById(pId).style.backgroundColor = pColor;
    }
</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Expires" CONTENT="0"> 
            <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
                <title>:: Haciendas Correntinas S.R.L. ::</title>
                <link rel="stylesheet" href="css/frontend.css" type="text/css" media="screen" />
                </head>	
                <body topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" rightmargin="0" onload="mLoad();" onresize="mLoad();" onorientationchange="mLoad();">
                    <div align="center" id="todoDiv" style="position:absolute; top:0; left:0; z-index:0; opacity:0.0;filter:alpha(opacity=00); background-color:#000;display:; width:100%;"></div>	
                    <div id="divConteTodo">
                        <div align="center" id="videoRural" style="position:absolute; display:none; top:52; z-index:30; text-align:center; width:100%; height:100%;">
                            <table cellspacing="0" cellpadding="0" border="0" align="center" height="1"  width="100%" align="center" background="">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td width="1">
                                        <div align="center" style="position:relative; top:52; z-index:30; text-align:center; width:662px; height:594px; overflow:hidden;">
                                            <div align="center" id="conteFrameset" style="position:absolute; top:-250px; left:-10px;">
                                                <iframe src="" width="662" height="1000" name="ruraliframe" id="ruraliframe" marginwidth=0 marginheight=0 scrolling="no"></iframe>
                                            </div> 
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right"><img src="images/btn-cerrar-off.png" onmouseover="this.src = 'images/btn-cerrar-on.png';" onmouseout="this.src = 'images/btn-cerrar-off.png';" onclick="cierraCatalogo('videoRural');" style="cursor:pointer;">&nbsp;&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>	
                            </table>		  	
                        </div>
                        <div align="center" id="catalogDiv" style="position:fixed; top:0; z-index:30; display:none; text-align:center;">
                            <div id="conteVideoylote">
                                <?php include 'videoinclude.php' ?>
                            </div>	
                        </div>
                        <div align="center" id="videofinalDiv" style="position:fixed; top:52; z-index:30; display:none; text-align:center;">
                            <div id="conteVideoeinfo">
                                <?php include 'videofinalinclude.php' ?>
                            </div>	
                        </div>
                        <div id="divTotal">
                            <table cellspacing="0" cellpadding="0" border="0" id="tablaTotal" align="center" height="100%">
                                <tr>
                                    <td width="958" height="1">
                                        <div id="conteCentro" style="position:relative; border-left: 1px solid #b1b0b0;border-right: 1px solid #b1b0b0;">
                                            <div id="btnOnline" style="position:absolute;z-index:11; left:787px; top:160px; display:none;"><img src="images/ver-onlineOFF.png" border="0" onmouseover="this.src = 'images/ver-onlineON.png'" onmouseout="this.src = 'images/ver-onlineOFF.png'" onclick="abreRural('videoRural');" style="cursor:pointer;"></div>
                                            <?php
                                            include 'menu.php'
                                            ?>
                                            <div id="conteContenido" style="position:relative;">
                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="958" height="1" background="images/fdo-centro.jpg">
                                            </div>	
                                            <tr>
                                                <td height="20"></td>
                                            </tr>
                                            <!--Componentes Banner-->
                                            <tr>
                                                <td height="1">
                                                    <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                        <?php
                                                        while ($rowRema = mysql_fetch_array($rsRema)) {
                                                            $precioSINO = $rowRema['precios'];

                                                            $botArchivoOff = 'images/bot_desc-cat-off.png';
                                                            $botArchivoOn = 'images/bot_desc-cat-on.png';

                                                            if ($rowRema['archivo']) {
                                                                $comparchivo = 1;
                                                                $descCatalogoDisplay = '';
                                                                $archivoCat = 'repositorio/' . $rowRema['archivo'];
                                                            } else {
                                                                $comparchivo = 2;
                                                                $descCatalogoDisplay = 'none';
                                                                $archivoCat = '';
                                                            }

                                                            if ($rowRema['precios'] == 'SI') {
                                                                $botArchivoOff = 'images/bot_desc-precios-off.png';
                                                                $botArchivoOn = 'images/bot_desc-precios-on.png';
                                                                $archivoCat = 'repositorio/' . $rowRema['archivo'];
                                                            }

                                                            if ($rowRema['zona'] == 'CORR') {
                                                                $colorFont = '#a92000';
                                                                $imgBannerTop = 'fondo_corr.png';
                                                                $imgBannerBottom = 'tira_corrientes.png';
                                                            }
                                                            if ($rowRema['zona'] == 'MIS') {
                                                                $colorFont = '#2b6f32';
                                                                $imgBannerTop = 'fondo_mis.png';
                                                                $imgBannerBottom = 'tira_misiones.png';
                                                            }
                                                            if ($rowRema['zona'] == 'BUE') {
                                                                $colorFont = '#006788';
                                                                $imgBannerTop = 'fondo_bsas.png';
                                                                $imgBannerBottom = 'tira_bsas.png';
                                                            }

                                                            if ($rowRema['cantidad']) {
                                                                $remaCanti = number_format($rowRema['cantidad'], 0, ',', '.');
                                                                $displayVacunos = '';
                                                            } else {
                                                                $remaCanti = '<div id="bannerFilmando" style="border: 0px solid #b1b0b0;position: absolute; top:-40px; left:0px; z-index:20; width:334px; height:116px;"><img src="images/consigne.png" onclick="location.href=\'umc.php#quieroconsignar\'" style="cursor:pointer;"></div>';
                                                                $displayVacunos = 'none';
                                                            }
                                                            ?>		
                                                            <tr>
                                                                <?php
                                                                $btnexcel = '<a href="remaplanilla.php?remaid=' . $remaid . '&xlsname=' . utf8_encode($rowRema['titulo']) . '"><img src="images/ico-excel.png" border="0"></a>';
                                                                if ($rowRema['archivo']) {
                                                                    if ($rowRema['precios'] == 'SI') {
                                                                        $btnpdf = '<a href="repositorio/' . $rowRema['archivo'] . '"><img src="images/icono_pdf_precios.png" border="0"></a>';
                                                                    } else {
                                                                        $btnpdf = '';
                                                                    }
                                                                    if ($rowRema['precios'] == 'SI') {
                                                                        $textobtnpdf = '<a href="repositorio/' . $rowRema['archivo'] . '" class="txtFiltro">DESCARGAR RESULTADOS</a>';
                                                                    } else {
                                                                        $textobtnpdf = '';
                                                                    }
                                                                } else {
                                                                    $btnpdf = '';
                                                                }
                                                                ?>	
                                                                <td>
                                                                    <div id="conteBanners" style="position:relative;">
                                                                        <?php
                                                                        //---------Mapa--------//
                                                                        if ($rowRema['mapa_img']) {
                                                                            $mapaDisplay = '';
                                                                            $mapa = 'repositorio/' . $rowRema['mapa_img'];
                                                                            $mapaLink = '<a target="_self" class="" rel="lightbox[\'mapa\']" href="' . $mapa . '" data-gal="mapa" title="Mapa del lugar del Remate"><img src="images/como-llegar-off.png" border="0" style="cursor:pointer;" onmouseover="this.src=\'images/como-llegar-on.png\'" onmouseout="this.src=\'images/como-llegar-off.png\'"></a>';
                                                                        } else {
                                                                            $mapaDisplay = 'none';
                                                                            $mapaLink = '';
                                                                        }
                                                                        //----------Mapa-------//

                                                                        $fechaRem = strtotime($rowRema['fecha']);
                                                                        //--------función Hora remate----//
                                                                        $remaHoraIni = strtotime($rowRema['hora_inicio']);
                                                                        $remaHoraFin = strtotime($rowRema['hora_fin']);
                                                                        $hora_actual = strtotime(date("H:i:s"));
                                                                        $remaFecha = date('d/m/Y', $fechaRem);
                                                                        $fechaHoy = date('d/m/Y');
                                                                        $veaRemateDisplay = 'none';
                                                                        //echo $remaFecha.' / '.$fechaHoy.'<br><br><br>';
                                                                        //echo $remaHoraIni.' / '.$remaHoraFin.' - '.$hora_actual.'<br><br><br>';
                                                                        if ($remaFecha == $fechaHoy) {
                                                                            //echo $remaFecha.' / '.$fechaHoy;
                                                                            if ($hora_actual >= $remaHoraIni && $hora_actual <= $remaHoraFin) {
                                                                                $veaRemateDisplay = '';
                                                                            } else {
                                                                                $veaRemateDisplay = 'none';
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <div style="position:relative;">
                                                                            <div style="position:relative; z-index:10;">
                                                                                 <?php
                                                                                    $yacarevisible= str_replace('SI','',$rowRema['yacare_visible']);
                                                                                    $yacarevisible= str_replace('NO','none',$yacarevisible);
                                                                                   if($rowRema['banner_g_visible']=='SI' && $rowRema['banner_g']){ 
                                                                                       $foto='repositorio/'.$rowRema['banner_g'];
                                                                                       if (file_exists($foto)) {
                                                                                           $size = GetImageSize("$foto"); 
                                                                                           $ancho=$size[0]; 
                                                                                           $alto=$size[1]; 
                                                                                           if($rowRema['banner_g_link']!=''){
                                                                                   ?>  
                                                                                       <div style="position:absolute; z-index:<?php echo $rowRema['banner_g_z_index'] ?>; left:<?php echo $rowRema['banner_g_pos_x'] ?>px; top:<?php echo $rowRema['banner_g_pos_y'] ?>px; display:<?php echo $rowRema['banner_g_visible'] ?>; width:<?php echo $ancho?>px; height:<?php echo $alto?>px;"><a href="<?php echo $rowRema['banner_g_link'] ?>"><img src="<?php echo $foto ?>" border="0"></a></div>  
                                                                                   <?php
                                                                                           }else{
                                                                                   ?>
                                                                                       <div style="position:absolute; z-index:<?php echo $rowRema['banner_g_z_index'] ?>; left:<?php echo $rowRema['banner_g_pos_x'] ?>px; top:<?php echo $rowRema['banner_g_pos_y'] ?>px; display:<?php echo $rowRema['banner_g_visible'] ?>; width:<?php echo $ancho?>px; height:<?php echo $alto ?>px;"><img src="<?php echo $foto ?>" border="0"></div>  
                                                                                   <?php
                                                                                           }    
                                                                                       }
                                                                                   }
                                                                                   ?>
                                                                                <div id="bannerNumerema" class="txtTahomabanner" style="color:<?php echo $colorFont ?>; letter-spacing:1pt; position: absolute; top:-9px; left:105px; z-index:12; overflow: hidden; width:215px; border: 0px solid #b1b0b0;"><b><?php echo $rowRema['numero'] ?>&ordm;</b>	 Remate TV</div>
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td valign="top" align="center" width="933" height="1" style="background-image:url('images/fdo-bajo-banner.png');background-position: 0px 50px;">
                                                                                            <div style="position:relative; top:-15px; left:-1px; z-index:11; width:933px; height:px; background-image:url('images/<?php echo $imgBannerTop ?>'); background-repeat:no-repeat; border: 0px solid #b1b0b0;">
                                                                                                <div id="bannerYacare" style="position: absolute; top:2px; left:785px; z-index:30; width:125px; height:162px; display:<?php echo $yacarevisible ?>"><img src="images/yacare.png"></div>
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="860" height="1">
                                                                                                    <tr>
                                                                                                        <td width="20">&nbsp;</td>
                                                                                                        <td width="800" height="160" valign="top">
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" height="1">
                                                                                                                <tr>
                                                                                                                    <td height="65"></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="center" height="1" width="390" style="font-family:Tahoma; font-size:17pt; color:#363636; font-weight:bold; letter-spacing:0px; text-align:center;border: 0px solid #b1b0b0;">
                                                                                                                        <?php echo utf8_encode($diasArr[date('w', $fechaRem)]) . ' ' . date('d', $fechaRem) . ' DE ' . utf8_encode($mesesArr[(int) date('m', $fechaRem)]) ?>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td height="5"></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="center" height="1" class="txtTahomabanner" style="font-size:16pt; color:#363636; letter-spacing:0pt;">
                                                                                                                        <?php echo str_replace(':00', '', $rowRema['hora_inicio']) . ' hs. ' . utf8_encode($rowRema['lugar']) ?>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td height="5"></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td align="center" height="1" class="txtTahomabanner" style="font-size:14pt; color:#7d7d7d; letter-spacing:0pt;">
                                                                                                                        <?php echo utf8_encode($rowRema['locacion']) ?>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>    
                                                                                                        </td>
                                                                                                        <td width="20"></td>
                                                                                                        <td width="420">
                                                                                                            <?php if ($rowRema['cantidad'] < 9999) { ?>
                                                                                                                <div id="bannerCanti" class="txtTahomabanner" style="position: relative; top:0px; left:20px; z-index:20; overflow:; width:450px; font-size:98pt; color:<?php echo $colorFont ?>; font-weight:bold; letter-spacing:-4pt; text-align:left; border: 0px solid #b1b0b0;">
                                                                                                                    <div id="bannerVacunos" style="display:<?php echo $displayVacunos ?>;position: absolute; top:117px; left:170px; z-index:20; width:178px; height:40px;"><img src="images/vacunos.png"></div>
                                                                                                                <?php } else { ?>
                                                                                                                    <div id="bannerCanti" class="txtTahomabanner" style="position: relative; top:0px; left:20px; z-index:20; overflow:; width:450px; font-size:80pt; color:<?php echo $colorFont ?>; font-weight:bold; letter-spacing:-4pt; text-align:left; border: 0px solid #b1b0b0;">
                                                                                                                        <div id="bannerVacunos" style="display:<?php echo $displayVacunos ?>;position: absolute; top:100px; left:170px; z-index:40; width:178px; height:40px;"><img src="images/vacunos.png"></div>
                                                                                                                    <?php } ?>
                                                                                                                    <?php echo $remaCanti ?>
                                                                                                                </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                    if ($rowRema['descripcion']) {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <td colspan="4" height="10"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="4" height="1" valign="top">
                                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                                                                                                                    <tr>
                                                                                                                        <td width="35"></td>
                                                                                                                        <td class="txtTahomabanner" style="font-size:14px; letter-spacing: 1pt; color:<?php echo $colorFont ?>;">
                                                                                                                            <?php
                                                                                                                            echo utf8_encode($rowRema['descripcion']);
                                                                                                                            ?>
                                                                                                                        </td>
                                                                                                                        <td width="35"></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    <?php } ?>
                                                                                                    <tr>
                                                                                                        <td colspan="4" height="10"></td>
                                                                                                    </tr>
                                                                                                </table>	
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                        <td width="900" height="69">
                                                                                            <div style="position:relative; top:-30px; left:10px; z-index:13; width:900px; height:69px; background-image:url('images/<?php echo $imgBannerBottom ?>'); background-repeat:no-repeat; border: 0px solid #b1b0b0;">
                                                                                                <div id="bannerDescCat" style="display:<?php echo $descCatalogoDisplay ?>; position: absolute; top:16px; left:376px; z-index:40; width:241px; height:51px;"><a href="<?php echo $archivoCat ?>" target="_blank"><img src="<?php echo $botArchivoOff ?>" style="cursor:pointer;" onmouseover="this.src='<?php echo $botArchivoOn ?>';" onmouseout="this.src='<?php echo $botArchivoOff ?>';" border="0"></a></div>
                                                                                                <div id="bannerRemateenVivo" style="display:<?php echo $veaRemateDisplay ?>; position: absolute; top:16px; left:115px; z-index:40; width:241px; height:51px;"><img src="images/bot_verremate-off.png" style="cursor:pointer;" onmouseover="this.src='images/bot_verremate-on.png'" onmouseout="this.src='images/bot_verremate-off.png'" onclick="abreRural('videoRural');"  border="0"></div>
                                                                                                <div id="bannerMapa" style="display:<?php echo $mapaDisplay ?>; position: absolute; top:16px; left:10px; z-index:30; width:241px; height:51px;"><?php echo $mapaLink ?></div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="3" height="15"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            if ($rowRema['video_final_1']) {
                                                                ?>
                                                                <tr>
                                                                    <td height="25"></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td align="center">
                                                                    <table cellspacing="0" cellpadding="0" border="0" width="942" height="100%">
                                                                        <tr>
                                                                            <td width="15">&nbsp;</td>
                                                                            <td width="910">
                                                                                <?php
                                                                                if ($rowRema['video_final_1']) {
                                                                                    ?>
                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="915" height="40" bgcolor="#ffffff" style="border: 1px dotted #b1b0b0;">
                                                                                        <tr>
                                                                                            <td colspan="10" height="5"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="10">
                                                                                                <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                    <tr style="display:;">
                                                                                                        <td height="10" colspan="3"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="30"></td>
                                                                                                        <td><img src="images/titu-videos-trans.png"></td>
                                                                                                        <td width="30"></td>  
                                                                                                    </tr>
                                                                                                    <tr style="display:;">
                                                                                                        <td height="3" colspan="3"></td>
                                                                                                    </tr>
                                                                                                    <tr style="display:">
                                                                                                        <td width="30"></td>
                                                                                                        <td colspan="2" height="1" style="border-top: 1px solid #000000;"><img src="images/bit-trans.gif"></td>
                                                                                                        <td width="30"></td>
                                                                                                    </tr>
                                                                                                    <tr style="display:;">
                                                                                                        <td colspan="3" height="10"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>  	
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="20"></td>
                                                                                            <td></td>
                                                                                            <?php
                                                                                            if ($rowRema['video_final_1']) {
                                                                                                ?>
                                                                                                <td align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"  class="txtVideolink"><a href="javascript:abreVideofinal('videofinalDiv', '<?php echo $rowRema['video_final_1'] ?>', '<?php echo utf8_encode($rowRema['video_titu_1']) ?>', '', <?php echo $rowRema['numero'] ?>);" class="txtVideolink"><b>VIDEO 1: </b><?php echo utf8_encode($rowRema['video_titu_1']) ?></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td> 	                  	  		  
                                                                                                <?php
                                                                                            }
                                                                                            if ($rowRema['video_final_2']) {
                                                                                                ?>
                                                                                                <td align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"  class="txtVideolink"><a href="javascript:abreVideofinal('videofinalDiv', '<?php echo $rowRema['video_final_2'] ?>', '<?php echo utf8_encode($rowRema['video_titu_2']) ?>', '', <?php echo $rowRema['numero'] ?>);" class="txtVideolink"><b>VIDEO 2: </b><?php echo utf8_encode($rowRema['video_titu_2']) ?></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td> 	                  	  		  
                                                                                                <?php
                                                                                            }
                                                                                            if ($rowRema['video_final_3']) {
                                                                                                ?>
                                                                                                <td align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"  class="txtVideolink"><a href="javascript:abreVideofinal('videofinalDiv', '<?php echo $rowRema['video_final_3'] ?>', '<?php echo utf8_encode($rowRema['video_titu_3']) ?>', '', <?php echo $rowRema['numero'] ?>);" class="txtVideolink"><b>VIDEO 3: </b><?php echo utf8_encode($rowRema['video_titu_3']) ?></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td> 	                  	  		  
                                                                                                <?php
                                                                                            }
                                                                                            if ($rowRema['video_final_4']) {
                                                                                                ?>
                                                                                                <td align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"  class="txtVideolink"><a href="javascript:abreVideofinal('videofinalDiv', '<?php echo $rowRema['video_final_4'] ?>', '<?php echo utf8_encode($rowRema['video_titu_4']) ?>', '', <?php echo $rowRema['numero'] ?>);" class="txtVideolink"><b>VIDEO 4: </b><?php echo utf8_encode($rowRema['video_titu_4']) ?></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td> 	                  	  		  
                                                                                                <?php
                                                                                            }
                                                                                            if ($rowRema['video_final_5']) {
                                                                                                ?>
                                                                                                <td align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"  class="txtVideolink"><a href="javascript:abreVideofinal('videofinalDiv', '<?php echo $rowRema['video_final_5'] ?>', '<?php echo utf8_encode($rowRema['video_titu_5']) ?>', '', <?php echo $rowRema['numero'] ?>);" class="txtVideolink"><b>VIDEO 5: </b><?php echo utf8_encode($rowRema['video_titu_5']) ?></a></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td height="5"></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td> 	                  	  		  
                                                                                                <?php
                                                                                            }
                                                                                            ?>


                                                                                            <td></td>
                                                                                            <td width="40"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="12" height="15"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <?php
                                                                        if ($rowRema['video_final_1']) {
                                                                            ?>
                                                                            <tr>
                                                                                <td height="25"></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </table>
                                                </td>
                                            </tr>
                                            <!--Fin Componentes Banner-->
                                            <tr>
                                                <td width="958" height="100%"  valign="top">
                                                    <div id="conteMedio">
                                                        <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                        <tr>
                                                                            <td width="20"></td>
                                                                            <td align="center" bgcolor="#FFFFFF">
                                                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="840" height="1">
                                                                                    <tr style="display:none;">
                                                                                        <td height="20"></td>
                                                                                    </tr>
                                                                                    <tr style="display:none;">
                                                                                        <td valign="top">
                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1">
                                                                                                <tr>
                                                                                                    <td width="1"><img src="images/titu-videos.jpg"></td>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <td width="5"></td>	
                                                                                                </tr>
                                                                                            </table>	
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="display:none;">
                                                                                        <td height="3"></td>
                                                                                    </tr>
                                                                                    <tr style="display:none;">
                                                                                        <td height="1" style="border-top: 1px solid #000000;"><img src="images/bit-trans.gif"></td>
                                                                                    </tr>
                                                                                    <tr style="display:none;">
                                                                                        <td height="3"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="20"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1">
                                                                                                <tr>
                                                                                                    <td width="1"><img src="images/titu-lotes.jpg"></td>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <?php
                                                                                                    if ($btnpdf == '') {
                                                                                                        
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <td width="170" class="txtFiltro"><?php echo $textobtnpdf ?></td>
                                                                                                        <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                    <td class="txtFiltro" valign="middle" width="1"><?php echo $btnpdf ?></td>
                                                                                                    <td width="5"></td>	
                                                                                                </tr>
                                                                                            </table>	
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="1" style="border-top: 1px solid #000000;"><img src="images/bit-trans.gif"></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    $rsConjLot = mysql_query($sqlConjLot);
                                                                                    $cuentaConjLot = mysql_num_rows($rsConjLot);
                                                                                    if($cuentaConjLot>1){
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td height="15"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1">
                                                                                                <tr>
                                                                                                    <?php
                                                                                                     while ($rowConjLot = mysql_fetch_array($rsConjLot)){
                                                                                                    ?>
                                                                                                    <td class="txtConjlink" width="1"><a class="txtConjlink" href="rematedetanew.php?remaid=<?php echo $remaid ?>&cl=<?php echo $rowConjLot['id'] ?>"><?php echo $rowConjLot['titulo']?></a></td>
                                                                                                    <td class="txtConjlink" width="20" align="center"> | </td>
                                                                                                    <?php
                                                                                                     }
                                                                                                    ?>
                                                                                                    <td>&nbsp;</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="15"></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    }else{
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td height="3"></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                    <!-------->
                                                                                    <tr style="display:;">
                                                                                        <td height="1">
                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1" bgcolor="#e4e1e1">
                                                                                                <tr><td colspan="10" height="6"></td></tr>
                                                                                                <tr>
                                                                                                    <td width="10"></td>
                                                                                                    <td align="right" class="txtFiltro" width="170" style="color:"><b>FILTROS DE B&Uacute;SQUEDA</b></td>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <td align="right" class="txtFiltro" width="150">UBICACI&Oacute;N DEL LOTE</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <div id="comboLocalidades"><select id="lotesLoca" class="select" name="lotesLoca" onchange="cambiaPagFiltro('loca', this.value, '<?php echo $locaId ?>', '<?php echo $remaid ?>');"
                                                                                                                                           <option value="5" <?php echo $locaselected ?>>TODOS</option>
                                                                                                                                               <?php echo '<option value="" ' . $locaselected . '>TODOS</option>' . $lugarcombo; ?>
                                                                                                            </select></div>
                                                                                                    </td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="10"></td>  
                                                                                                </tr>
                                                                                                <tr><td colspan="10" height="6"></td></tr>  
                                                                                            </table>
                                                                                        </td>	 
                                                                                    </tr>
                                                                                    <!----------->
                                                                                    <tr>
                                                                                        <td height="1">
                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1" bgcolor="#faf4e2">
                                                                                                <tr><td colspan="10" height="6"></td></tr>
                                                                                                <tr>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <td align="right" class="txtFiltro" width="1">LOTE</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <select id="lotesSel" class="select" name="categorias" onchange="cambiaPagFiltro('lotenum', this.value, '<?php echo $lotesel ?>', '<?php echo $remaid ?>');">
                                                                                                            <?php echo "VER" . $innerselLotes ?>
                                                                                                        </select>
                                                                                                    </td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">|</td>
                                                                                                    <td width="5"></td>	
                                                                                                    <td align="right" class="txtFiltro" width="1">CATEG.</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <select class="select" name="categorias" onchange="cambiaPagFiltro('cate', this.value, '<?php echo $cate ?>', '');">
                                                                                                            <?php
                                                                                                            $cuentarowscate = 0;
                                                                                                            if ($totalCAT > 0) {
                                                                                                                while ($rowCAT = mysql_fetch_array($rsCAT)) {
                                                                                                                    $cuentarowscate+=1;
                                                                                                                    if ($cuentarowscate == 1) {
                                                                                                                        if ($cate == "") {
                                                                                                                            ?>
                                                                                                                            <option value="" selected>TODOS</option>
                                                                                                                            <?php
                                                                                                                        } else {
                                                                                                                            ?>
                                                                                                                            <option value="">TODOS</option>
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                    }

                                                                                                                    if ($cate == $rowCAT['cateidselect']) {
                                                                                                                        ?>
                                                                                                                        <option value="<?php echo $rowCAT['cateidselect'] ?>" selected ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                                                                                                        <?php
                                                                                                                    } else {
                                                                                                                        ?>
                                                                                                                        <option value="<?php echo $rowCAT['cateidselect'] ?>" ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                                                                                                        <?php
                                                                                                                    }
                                                                                                                }
                                                                                                            } else {
                                                                                                                ?>
                                                                                                                <option value="" selected>TODOS</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </select>
                                                                                                    </td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">|</td>
                                                                                                    <td width="5"></td>	
                                                                                                    <td align="right" class="txtFiltro" width="1">TRAZ.</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <select class="select" name="colores" onchange="cambiaPagFiltro('tr', this.value, '<?php echo $traza ?>', '');">
                                                                                                            <?php
                                                                                                            if ($traza == '') {
                                                                                                                ?>
                                                                                                                <option value="" selected>TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }

                                                                                                            if ($traza == 'NO') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO" selected>NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            if ($traza == 'SI') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI"selected>SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>  
                                                                                                        </select>
                                                                                                    </td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">|</td>
                                                                                                    <td width="5"></td>	
                                                                                                    <td align="right" class="txtFiltro" width="1">GARPTA.</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <select class="select" name="colores" onchange="cambiaPagFiltro('gr', this.value, '<?php echo $garpta ?>', '');">
                                                                                                            <?php
                                                                                                            if ($garpta == '') {
                                                                                                                ?>
                                                                                                                <option value="" selected>TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }

                                                                                                            if ($garpta == 'NO') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO" selected>NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            if ($garpta == 'SI') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI"selected>SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>  
                                                                                                        </select>
                                                                                                    </td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">|</td>
                                                                                                    <td width="5"></td>	
                                                                                                    <td align="right" class="txtFiltro" width="60">MIO-MIO.</td>
                                                                                                    <td width="5"></td>
                                                                                                    <td width="1">
                                                                                                        <select class="select" name="colores" onchange="cambiaPagFiltro('mm', this.value, '<?php echo $miomio ?>', '');">
                                                                                                            <?php
                                                                                                            if ($miomio == '') {
                                                                                                                ?>
                                                                                                                <option value="" selected>TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }

                                                                                                            if ($miomio == 'NO') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI">SI</option>
                                                                                                                <option value="NO" selected>NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            if ($miomio == 'SI') {
                                                                                                                ?>
                                                                                                                <option value="">TODOS</option>
                                                                                                                <option value="SI"selected>SI</option>
                                                                                                                <option value="NO">NO</option>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>  
                                                                                                        </select>
                                                                                                    </td>
                                                                                                    <td width="10"></td>  
                                                                                                </tr>
                                                                                                <tr><td colspan="10" height="6"></td></tr>  
                                                                                            </table>
                                                                                        </td>	 
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="20"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="20"></td>	
                                                                            <tr>
                                                                                <tr>
                                                                                    <td width="20"></td>
                                                                                    <td align="center" bgcolor="#FFFFFF">
                                                                                        <table cellspacing="0" cellpadding="0" border="0" id="" width="840" height="100%">
                                                                                            <tr>
                                                                                                <?php
                                                                                                $cuentatres = 0;
                                                                                                $cuentatotal = 0;
                                                                                                while ($row = mysql_fetch_assoc($rs)) {
                                                                                                    $rematitu = $row['rematitu'];
                                                                                                    $id = $row["id"];
                                                                                                    $name = utf8_encode($row["orden"]);
                                                                                                    $cuentatres+=1;
                                                                                                    $cuentatotal+=1;
                                                                                                    $detalote = '<b>' . utf8_encode($row['cantidad']) . ' vacunos</b><br>Tipo: <b>' . utf8_encode($row['tiponombre']) . '</b><br>Categor&iacute;a: <b>' . utf8_encode($row['catenombre']) . '</b><br>Peso.: <b>' . utf8_encode($row['peso']) . ' Kgs.</b><br>Localidad: <b>' . utf8_encode($row['locanombre']) . '</b><br>Provincia: <b>' . utf8_encode($row['provinombre']) . '</b><br>Trazabilidad: <b>' . $row['trazabilidad'] . '</b><br>Garrapata: <b>' . $row['garrapata'] . '</b><br>MIO-MIO: <b>' . $row['mio_mio'] . '</b><br>Caracter&iacute;sticas: <b>' . utf8_encode($row['caracteristicas']) . '</b>';
                                                                                                    //echo $detalote;

                                                                                                    if ($row['orden'] == '') {
                                                                                                        $displayCode = '';
                                                                                                        $displayOrden = 'none';
                                                                                                    } else {
                                                                                                        $displayCode = 'none';
                                                                                                        $displayOrden = '';
                                                                                                    }
                                                                                                    $orden = $row['orden'];
                                                                                                    if ($orden != '') {
                                                                                                        if (substr($orden, 0, 2) == "00") {
                                                                                                            $orden = str_replace("00", "", $orden);
                                                                                                            if (substr($orden, 0, 1) == "0") {
                                                                                                                $orden = str_replace("0", "", $orden);
                                                                                                            }
                                                                                                        } else {
                                                                                                            if (substr($orden, 0, 1) == "0") {
                                                                                                                $orden = substr_replace($orden, "", 0, 1);
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    ?>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <?php
                                                                                                    if ($row['vlv']) {
                                                                                                        $cadena = $row['vlv'];
                                                                                                    } else {
                                                                                                        $cadena = $row['video'];
                                                                                                    }
                                                                                                    $maximo = strlen($cadena);
                                                                                                    if ($maximo > 3) {
                                                                                                        $cadena_comienzo = "v=";
                                                                                                        $totalstr = strpos($cadena, $cadena_comienzo);
                                                                                                        $varURLYT = substr($cadena, ($totalstr + 2), strlen($cadena));
                                                                                                    } else {
                                                                                                        $varURLYT = '';
                                                                                                    }
                                                                                                    //echo $final;
                                                                                                    ?>
                                                                                                    <td width="270" height="110" background="images/fdo-lote.png" style="cursor:pointer;" onclick="abreCatalogo('catalogDiv', '<?php echo $varURLYT ?>', '<?php echo utf8_encode($row['rematitu']) ?>', '<?php echo $detalote ?>', '<?php echo $orden ?>', '<?php echo $row['codigo'] ?>', '<?php echo $displayOrden ?>', '<?php echo $displayCode ?>')">
                                                                                                        <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                            <tr>
                                                                                                                <td width="10"></td>
                                                                                                                <td width="100" valign="top">
                                                                                                                    <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                                        <tr style="display:<?php echo $displayCode ?>;">
                                                                                                                            <td class="txtCodilote" height="1" valign="top" style="display:<?php echo $displayCode ?>;">
                                                                                                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1">
                                                                                                                                    <tr>
                                                                                                                                        <td height="10"></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td align="center" class="txtCodiloteCH2" height="1" valign="top">Certif. N°</td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td height="10"></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="txtCodilote" valign="top" height="45"><?php echo $row['codigo'] ?></td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>  	
                                                                                                                        </tr>
                                                                                                                        <tr style="display:<?php echo $displayOrden ?>;">
                                                                                                                            <td height="1" valign="middle">
                                                                                                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                                                    <tr>
                                                                                                                                        <td height="10"></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td height="1" align="center" class="txtLotecertif2">CERT. N° <?php echo $row['codigo'] ?></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td height="5"></td>
                                                                                                                                    </tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="txtNumelote" height="1" valign="middle"><?php echo $orden ?></td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>  	
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td></td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td width="5"></td>
                                                                                                                <td>
                                                                                                                    <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                                        <tr>
                                                                                                                            <td></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td height="1" class="txtTextolote"><?php echo $row['cantidad'] ?> cabezas</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td height="3"></td>
                                                                                                                        </tr>
                                                                                                                        <?php
                                                                                                                        if ($row['peso'] == '-' || $row['peso'] == '' || $row['peso'] == 'S / P') {
                                                                                                                            $pesolote = '';
                                                                                                                        } else {
                                                                                                                            $pesolote = '<br>' . utf8_encode($row['peso'] . ' Kgs.');
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                        <tr>
                                                                                                                            <td height="1" class="txtLotecate"><?php echo utf8_encode($row['catenombre']) ?><?php echo $pesolote ?><br><?php echo utf8_encode($row['locanombre']) ?>, <?php echo utf8_encode($row['provinombre']) ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td height="10"></td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td width="10"></td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td>&nbsp;</td>
                                                                                                    <?php
                                                                                                    if ($cuentatres == 3) {
                                                                                                        $cuentatres = 0;
                                                                                                        ?>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td colspan="7" height="20"></td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                    if ($cuentatotal == $total) {
                                                                                                        
                                                                                                    } else {
                                                                                                        ?>
                                                                                                        <tr>
                                                                                                            <?php
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="7" height="20" align="center"></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="8" height="20" align="center">
                                                                                                    <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="20">
                                                                                                        <tr>
                                                                                                            <?php
                                                                                                            $totalPag = ceil($total / $limit);
                                                                                                            ?>
                                                                                                            <script type="text/javascript">

    function utf8_encode(argString) {
        // Encodes an ISO-8859-1 string to UTF-8  
        // 
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/utf8_encode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: sowberry
        // +    tweaked by: Jack
        // +   bugfixed by: Onno Marsman    // +   improved by: Yves Sucaet
        // +   bugfixed by: Onno Marsman
        // +   bugfixed by: Ulrich
        // +   bugfixed by: Rafal Kukawski
        // *     example 1: utf8_encode('Kevin van Zonneveld');    // *     returns 1: 'Kevin van Zonneveld'
        if (argString === null || typeof argString === "undefined") {
            return "";
        }
        var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
        var utftext = "",
                start, end, stringl = 0;

        start = end = 0;
        stringl = string.length;
        for (var n = 0; n < stringl; n++) {
            var c1 = string.charCodeAt(n);
            var enc = null;
            if (c1 < 128) {
                end++;
            } else if (c1 > 127 && c1 < 2048) {
                enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
            } else {
                enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
            }
            if (enc !== null) {
                if (end > start) {
                    utftext += string.slice(start, end);
                }
                utftext += enc;
                start = end = n + 1;
            }
        }
        if (end > start) {
            utftext += string.slice(start, stringl);
        }
        return utftext;
    }


    function utf8_decode(str_data) {
        // Converts a UTF-8 encoded string to ISO-8859-1  
        // 
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/utf8_decode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
        // +      input by: Aman Gupta
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Norman "zEh" Fuchs
        // +   bugfixed by: hitwork    // +   bugfixed by: Onno Marsman
        // +      input by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // *     example 1: utf8_decode('Kevin van Zonneveld');
        // *     returns 1: 'Kevin van Zonneveld'    
        var tmp_arr = [],
                i = 0,
                ac = 0,
                c1 = 0,
                c2 = 0, c3 = 0;

        str_data += '';

        while (i < str_data.length) {
            c1 = str_data.charCodeAt(i);
            if (c1 < 128) {
                tmp_arr[ac++] = String.fromCharCode(c1);
                i++;
            } else if (c1 > 191 && c1 < 224) {
                c2 = str_data.charCodeAt(i + 1);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = str_data.charCodeAt(i + 1);
                c3 = str_data.charCodeAt(i + 2);
                tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return tmp_arr.join('');
    }


    function urlencode(str) {
        str = escape(str);
        str = str.replace('+', '%2B');
        str = str.replace('%20', '+');
        str = str.replace('*', '%2A');
        str = str.replace('/', '%2F');
        str = str.replace('@', '%40');
        return str;
    }

    function urldecode(str) {
        str = str.replace('+', ' ');
        str = unescape(str);
        return str;
    }

    pagSig = <?php echo $pag ?> + 1
    pagAnt = <?php echo $pag ?> - 1
    function cambiaPag(pPag) {
        if (pPag > <?php echo $totalPag ?> || pPag < 1) {

        } else {
            url = '<?php echo $miURL ?>';
            url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=' + pPag;
            window.location = url;

        }

    }



    function cambiaPagFiltro(pPar, pFiltro, pVar, pRemaid) {
        if (pPar == 'lotenum') {
            if (pFiltro == '') {
                url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid;
            } else {
                url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid + '&lotenum=' + pFiltro;
            }
        } else {
            url = '<?php echo $miURL ?>';
            url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=1';
            url = url.replace('&lotenum=<?php echo $lotesel ?>', '');
            if (pFiltro == '') {
                url = url.replace('&' + pPar + '=' + pVar, '');
            } else {
                url = url.replace('&' + pPar + '=' + pVar, '') + '&' + pPar + '=' + pFiltro;
            }
        }

        window.location = url;
    }
    function enviar_formulario() {
        document.selUbic.submit();
    }

                                                                                                            </script>
                                                                                                            <td>&nbsp;</td>
                                                                                                            <td>
                                                                                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="20">
                                                                                                                    <tr>
                                                                                                                        <td>&nbsp;</td>
                                                                                                                        <td width="1"><img src="images/ico-pag-ret.jpg" style="cursor:pointer;" onclick="cambiaPag(pagAnt)"></td>
                                                                                                                        <td width="10">&nbsp;</td>
                                                                                                                        <td width="1" class="txtNumepagiOff">P&aacute;gina&nbsp;&nbsp;</td>
                                                                                                                        <?php
                                                                                                                        $links = array();
                                                                                                                        for ($i = 1; $i <= $totalPag; $i++) {
                                                                                                                            if ($pag == $i) {
                                                                                                                                $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagi\" valign=\"middle\" bgcolor=\"#b44422;\">" . $i . "</td>";
                                                                                                                            } else {
                                                                                                                                $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagiOff\" valign=\"middle\" style=\"cursor:pointer;\" onclick=\"cambiaPag(" . $i . ")\"><a href=\"javascript:cambiaPag(" . $i . ")\">" . $i . "</a></td>";
                                                                                                                            }
                                                                                                                        }
                                                                                                                        echo implode("<td width=\"1\"></td>", $links);
                                                                                                                        ?>
                                                                                                                        <td width="10">&nbsp;</td>
                                                                                                                        <td width="1"><img src="images/ico-pag-av.jpg" style="cursor:pointer;" onclick="cambiaPag(pagSig)"></td>
                                                                                                                        <td>&nbsp;</td>
                                                                                                                    </tr>
                                                                                                                </table>	
                                                                                                            </td>
                                                                                                            <td>&nbsp;</td> 	  
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr> 
                                                                                            <tr>
                                                                                                <td colspan="7"></td>
                                                                                            </tr>     
                                                                                            <tr>
                                                                                                <td colspan="7" height="20" align="center"></td>
                                                                                            </tr>  
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="20"></td>
                                                                                </tr>
                                                                                </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="40"></td>
                                                                            </tr>
                                                                    </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr><td></td></tr>
                                                        </table>
                                                    </div>
                                                    <?php
                                                    include 'footer.php';
                                                    ?>
                                                </td>
                                            </tr>
                                            </table>
                                        </div>
                                        </div>  
                                        </body>
                                        <link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
                                        <script src="js/prototype.js" type="text/javascript"></script>
                                        <script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
                                        <script src="js/lightbox.js" type="text/javascript"></script>
                                        </html>