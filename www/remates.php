<?php
require_once("app/config/bootstrap.php");
use Umc\App\Services\Auth\Auth;
$auth = new Auth();
?>
<!DOCTYPE html>
<?php
include 'includes/MySQL.php';
$link = Conectarse();

include 'includes/funciones.php';

$pageID= 'rema';

$result = mysql_query("SELECT
remates.id AS remaid,
remates.tipo,
remates.plataforma,
remates.ext_id,
remates.fecha,
remates.lugar,
remates.numero,
remates.titulo,
remates.descripcion,
remates.banner_g,
remates.banner_ch,
remates.activo
FROM
remates
WHERE
remates.fecha <=  curdate()
ORDER BY
remates.fecha DESC", $link);

$resultprox = mysql_query("SELECT
remates.id AS remaid,
remates.tipo,
remates.plataforma,
remates.ext_id,
remates.fecha,
remates.lugar,
remates.numero,
remates.titulo,
remates.descripcion,
remates.banner_g,
remates.banner_ch,
remates.activo
FROM
remates
WHERE
remates.fecha >=  curdate()
ORDER BY
remates.fecha DESC", $link);

$innerBanners = '';
$innerGrilla = '';
$posIndex = 50;
$controw = 0;
$contcolor = 0;

while ($row = mysql_fetch_array($result)) {
    
    if($row['plataforma']=='APP'){
        $pagRemaDeta = 'rematedeta_a.php';
        $linkrema = $pagRemaDeta . '?remaid=' . $row['remaid'].'&extappid='.$row['ext_id'];
    }else if($row['plataforma']=='WEB'){
        $pagRemaDeta = 'rematedeta_n.php';
        $linkrema = $pagRemaDeta . '?remaid=' . $row['remaid'];
    }
   

    $titlerema = 'Ver detalle';

    $controw+=1;
    if ($contcolor == 0) {
        $rowcolor = '#f2f4fa';
        $contcolor = 1;
    } else {
        $rowcolor = '#f8f9fc';
        $contcolor = 0;
    }




    if ($controw == 1) {
        ?>
        <script type="text/javascript">
            objAnt = 'remate<?php echo $row["numero"] ?>';
        </script>
        <?php

        $innerGrilla = $innerGrilla .
        '<tr bgcolor="' . $rowcolor . '">
            <td data-title="REMATE" align="center" class="txtGrillas"><b>N&deg; ' . $row['numero'] . '</b></td>
            <td data-title="FECHA" align="center" class="txtGrillas">' . cambiaf_a_normal($row['fecha']) . '</td>
            <td data-title="TITULO" align="center"  class="txtGrillas">' . $row['titulo'] . '</td>
            <td data-title="LUGAR" align="center"  class="txtGrillas">' . $row['lugar'] . '</td>
            <td data-title="DETALLE" align="center"><a href="' . $linkrema . '" title="' . $titlerema. '"><i class="fa fa-plus-circle blue" aria-hidden="true"></i></a></td>

        </tr>';
        //echo $innerGrilla;
    } else {
        $innerGrilla = $innerGrilla .
        '<tr bgcolor="' . $rowcolor . '">
            <td data-title="REMATE" align="center" class="txtGrillas"><b>N&deg; ' . $row['numero'] . '</b></td>
            <td data-title="FECHA" align="center" class="txtGrillas">' . cambiaf_a_normal($row['fecha']) . '</td>
            <td data-title="TITULO" align="center"  class="txtGrillas">' . $row['titulo'] . '</td>
            <td data-title="LUGAR" align="center"  class="txtGrillas">' . $row['lugar'] . '</td>
            <td data-title="DETALLE" align="center"><a href="' . $linkrema . '" title="' . $titlerema. '"><i class="fa fa-plus-circle blue" aria-hidden="true"></i></a></td>

        </tr>';
    }



    $posIndex = -1;
}

$innerBanners = '';
$innerGrillaprox = '';
$posIndexprox = 50;
$controwprox = 0;
$contcolorprox = 0;

while ($rowprox = mysql_fetch_array($resultprox)) {
    
    if($rowprox['plataforma']=='APP'){
        $pagRemaDetaprox = 'rematedeta_a.php';
        $linkremaprox = $pagRemaDetaprox . '?remaid=' . $rowprox['remaid'].'&extappid='.$rowprox['ext_id'];
    }else if($rowprox['plataforma']=='WEB'){
        $linkremaprox = $pagRemaDetaprox . '?remaid=' . $rowprox['remaid'];
    }
    

    
    $titleremaprox = 'Ver detalle';

    $controwprox+=1;

    $rowcolorprox = '#fff7e4';

    $innerGrillaprox = $innerGrillaprox . '
    <tr bgcolor="' . $rowcolorprox . '">
        <td data-title="REMATE" align="center" class="txtGrillasProx">N&deg; ' . $rowprox['numero'] . '</td>
        <td data-title="FECHA" align="center" class="txtGrillasProx">' . cambiaf_a_normal($rowprox['fecha']) . '</td>
        <td data-title="TITULO" align="center"  class="txtGrillasProx">' . $rowprox['titulo'] . '</td>
        <td data-title="LUGAR" align="center"  class="txtGrillasProx">' . $rowprox['lugar'] . '</td>
        <td data-title="DETALLE" align="center"><a href="' . $linkremaprox . '" title="' . $titleremaprox . '"><i class="fa fa-plus-circle red" aria-hidden="true"></i></a></td>
    </tr> ';

    $innerGrillaprox = $innerGrillaprox . '<tr><td colspan="8" height="2"></td></tr>';

    $posIndexprox = -1;
}
?>



<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>:: UMC S.A. Negocios Agropecuarios ::</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
include 'header.php';
?>

    <div class="container" style="padding-top:230px;">
        <div class="container-fluid">
            <div style="" class="row">
                <!--contenido remates-->
                <div id="no-more-tables">
                    <div class="col-xs-12 txtTituremates">
                                            <b>PRÓXIMOS</b> <span style="color: #7a7a7a;">REMATES</span>

                    </div>

                    <table cellspacing="0" cellpadding="0" border="0" class="col-md-12 table-condensed cf" style="margin: 0 auto;">
                                		<thead class="cf">
                                			<tr style="height:30px;">
                                				<th class="txtTituGrillas">REMATE</th>
                                				<th class="txtTituGrillas">FECHA</th>
                                				<th class="txtTituGrillas">TITULO</th>
                                				<th class="txtTituGrillas">LUGAR</th>
                                				<th class="txtTituGrillas">DETALLE</th>
                                			</tr>
                                		</thead>
                                		<tbody>
                                        <?php echo $innerGrillaprox ?>

                        </tbody>
                    </table>

                    <div class="col-xs-12 txtTituremates" style="margin-top: 20px;">
                                            <b>RESULTADOS</b> <span style="color: #7a7a7a;">DE REMATES</span>

                    </div>

                    <table cellspacing="0" cellpadding="0" border="0" class="col-md-12 table-condensed cf sasa" style="">
                                		<thead class="cf">
                                			<tr style="height:30px;">
                                				<th class="txtTituGrillas">REMATE</th>
                                				<th class="txtTituGrillas">FECHA</th>
                                				<th class="txtTituGrillas">TITULO</th>
                                				<th class="txtTituGrillas">LUGAR</th>
                                				<th class="txtTituGrillas">DETALLE</th>
                                			</tr>
                                        </thead>
                        <tbody>
                        <?php echo $innerGrilla ?>
                        </tbody>
                    </table>

                </div>
                <!--fin contenido remates-->
            </div>
        </div>
    </div>
        <div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
        <?php include 'footer.php'?>
    </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
