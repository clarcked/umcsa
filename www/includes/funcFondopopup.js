var tempAlpha;
var pOpacity;
pOpacity=0;

var innerVideo='';
var useragent = navigator.userAgent;
var videoIncludemob;

function Browser() {   
    // ---- public properties -----
    this.fullName = 'unknow'; // getName(false);
    this.name = 'unknow'; // getName(true);
    this.code = 'unknow'; // getCodeName(this.name);
    this.fullVersion = 'unknow'; // getVersion(this.name);
    this.version = 'unknow'; // getBasicVersion(this.fullVersion);
    this.mobile = false; // isMobile(navigator.userAgent);
    this.width = screen.width;
    this.height = screen.height;
    this.platform =  'unknow'; //getPlatform(navigator.userAgent);
    
    // ------- init -------    
    this.init = function() { //operative system, is an auxiliary var, for special-cases
        //the first var is the string that will be found in userAgent. the Second var is the common name
        // IMPORTANT NOTE: define new navigators BEFORE firefox, chrome and safari
        var navs = [
            { name:'Opera Mobi', fullName:'Opera Mobile', pre:'Version/' },
            { name:'Opera Mini', fullName:'Opera Mini', pre:'Version/' },
            { name:'Opera', fullName:'Opera', pre:'Version/' },
            { name:'MSIE', fullName:'Microsoft Internet Explorer', pre:'MSIE ' },  
            { name:'BlackBerry', fullName:'BlackBerry Navigator', pre:'/' }, 
            { name:'BrowserNG', fullName:'Nokia Navigator', pre:'BrowserNG/' }, 
            { name:'Midori', fullName:'Midori', pre:'Midori/' }, 
            { name:'Kazehakase', fullName:'Kazehakase', pre:'Kazehakase/' }, 
            { name:'Chromium', fullName:'Chromium', pre:'Chromium/' }, 
            { name:'Flock', fullName:'Flock', pre:'Flock/' }, 
            { name:'Galeon', fullName:'Galeon', pre:'Galeon/' }, 
            { name:'RockMelt', fullName:'RockMelt', pre:'RockMelt/' }, 
            { name:'Fennec', fullName:'Fennec', pre:'Fennec/' }, 
            { name:'Konqueror', fullName:'Konqueror', pre:'Konqueror/' }, 
            { name:'Arora', fullName:'Arora', pre:'Arora/' }, 
            { name:'Swiftfox', fullName:'Swiftfox', pre:'Firefox/' }, 
            { name:'Maxthon', fullName:'Maxthon', pre:'Maxthon/' },
            // { name:'', fullName:'', pre:'' } //add new broswers
            // { name:'', fullName:'', pre:'' }
            { name:'Firefox',fullName:'Mozilla Firefox', pre:'Firefox/' },
            { name:'Chrome', fullName:'Google Chrome', pre:'Chrome/' },
            { name:'Safari', fullName:'Apple Safari', pre:'Version/' }
        ];
    
        var agent = navigator.userAgent, pre;
        //set names
        for (i in navs) {
           if (agent.indexOf(navs[i].name)>-1) {
               pre = navs[i].pre;
               this.name = navs[i].name.toLowerCase(); //the code name is always lowercase
               this.fullName = navs[i].fullName; 
                if (this.name=='msie') this.name = 'iexplorer';
                if (this.name=='opera mobi') this.name = 'opera';
                if (this.name=='opera mini') this.name = 'opera';
                break; //when found it, stops reading
            }
        }//for
        
      //set version
        if ((idx=agent.indexOf(pre))>-1) {
            this.fullVersion = '';
            this.version = '';
            var nDots = 0;
            var len = agent.length;
            var indexVersion = idx + pre.length;
            for (j=indexVersion; j<len; j++) {
                var n = agent.charCodeAt(j); 
                if ((n>=48 && n<=57) || n==46) { //looking for numbers and dots
                    if (n==46) nDots++;
                    if (nDots<2) this.version += agent.charAt(j);
                    this.fullVersion += agent.charAt(j);
                }else j=len; //finish sub-cycle
            }//for
            this.version = parseInt(this.version);
        }
        
        // set Mobile
        var mobiles = ['mobi', 'mobile', 'mini', 'iphone', 'ipod', 'ipad', 'android', 'blackberry'];
        for (var i in mobiles) {
            if (agent.indexOf(mobiles[i])>-1) this.mobile = true;
        }
        if (this.width<700 || this.height<600) this.mobile = true;
        
        // set Platform        
        var plat = navigator.platform;
        if (plat=='Win32' || plat=='Win64') this.platform = 'Windows';
        if (agent.indexOf('NT 5.1') !=-1) this.platform = 'Windows XP';        
        if (agent.indexOf('NT 6') !=-1)  this.platform = 'Windows Vista';
        if (agent.indexOf('NT 6.1') !=-1) this.platform = 'Windows 7';
        if (agent.indexOf('Mac') !=-1) this.platform = 'Macintosh';
        if (agent.indexOf('Linux') !=-1) this.platform = 'Linux';
        if (agent.indexOf('iPhone') !=-1) this.platform = 'iOS iPhone';
        if (agent.indexOf('iPod') !=-1) this.platform = 'iOS iPod';
        if (agent.indexOf('iPad') !=-1) this.platform = 'iOS iPad';
        if (agent.indexOf('Android') !=-1) this.platform = 'Android';
        
        if (this.name!='unknow') {
            this.code = this.name+'';
            if (this.name=='opera') this.code = 'op';
            if (this.name=='firefox') this.code = 'ff';
            if (this.name=='chrome') this.code = 'ch';
            if (this.name=='safari') this.code = 'sf';
            if (this.name=='iexplorer') this.code = 'ie';
            if (this.name=='maxthon') this.code = 'mx';
        }
        
        //manual filter, when is so hard to define the navigator type
        if (this.name=='safari' && this.platform=='Linux') {
            this.name = 'unknow';
            this.fullName = 'unknow';
            this.code = 'unknow';
        }
        
    };//function
    
    this.init();

}//Browser class

var brw = new Browser();

//alert('fullName: ' + brw.fullName + '\n' + 'name: ' + brw.name + '\n' + 'fullVersion: ' + brw.fullVersion + '\n' + 'version: ' + brw.version + '\n' + 'platform: ' + brw.platform+ '\n' + 'mobile: ' + brw.mobile+ '\n' + 'resolution: ' + brw.width + 'x' + brw.height);

function abreCatalogo(pDivqueabre, pVideo, pTitulote, pDetalote, pNumelote, pCodilote, pDisplaylote, pDisplaycodi){
	document.getElementById('todoDiv').style.zIndex=20;
	document.getElementById(pDivqueabre).style.display='';
	innerVideo=pVideo;
	
		
	  if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 || useragent.indexOf('Blackberry') != -1 || useragent.indexOf('Opera Mini') != -1 || useragent.indexOf('Windows CE') != -1 || document.documentElement.offsetWidth < 800){
	  	//alert('true');
		  embedVideo='<iframe class="youtube-player" type="text/html" width="450" height="375" src="http://www.youtube.com/embed/' + innerVideo + '" allowfullscreen frameborder="0"></iframe>';
		  if(innerVideo==''){
	      embedVideo='<img src="images/no-video-ch.jpg">';
	    }
		  videoIncludemob = '<tr><td height="40"></td></tr><tr><td></td><td height="1" width="774"><table cellspacing="0" cellpadding="0" border="0" align="center" width="774" align="center"><tr><td height="1" class="txtXprecblan" align="center" id="conteTitu">'+ pTitulote +'</td><td width="1"><img src="images/btn-cerrar-x.png" title="CIERRA VIDEO" onclick="cierraCatalogo(\'catalogDiv\');" style="cursor:pointer;"></td></tr></table></td><td></td></tr><tr><td colspan="3" height="5"></td></tr><tr><td></td><td height="2" align="center"><img src="images/punteada-video-ch.png"></td><td></td></tr><tr><td colspan="3" height="2"></td></tr><tr><td>&nbsp;</td><td align="center" width="1"><table cellspacing="0" cellpadding="0" border="0" align="center" height="375" width="1" align="center" background=""><tr><td>&nbsp;</td><td align="center" style="border-left: 0px solid #f6f4ee; border-top: 0px solid #f6f4ee; border-bottom: 0px solid #f6f4ee;" width="452" height="375"><div style="position:relative; width:450px;"><div id="tdContevideo" style="position:absolute; z-index:10; height:375px; width:450px; left:0px;">'+embedVideo+'</div><div style="position:relative; top:0px; z-index:9;"><table cellspacing="0" cellpadding="0" border="0" align="center" height="375"  width="450" align="center" bgcolor="#000000"><tr><td>&nbsp;</td></tr><tr><td align="center" valign="middle"><img src="images/loading51.gif"></td></tr><tr><td>&nbsp;</td></tr></table></div></div></td><td><div style="position:relative;"><div style="position:absolute; top:345px; left:240px"><table cellspacing="0" cellpadding="0" border="0" align="center" height="1"  width="1" align="center" background=""><tr><td><img src="images/btn-cerrar-off.png" onmouseover="this.src=\'images/btn-cerrar-on.png\';" onmouseout="this.src=\'images/btn-cerrar-off.png\';" onclick="cierraCatalogo(\'catalogDiv\');" style="cursor:pointer;"></td></tr></table></div><table cellspacing="0" cellpadding="0" border="0" align="center" height="375"  width="326" background="images/fdo-lote-info-ch.png" align="center" background=""><tr><td height="40"></td></tr><tr><td height="1"><table cellspacing="0" cellpadding="0" border="0" align="center" height="1"  width="100%" align="center" background=""><tr><td width="38"></td><td id="loteOrden" height="40" width="97" class="txtNumeloteCH" valign="middle" align="center" style="display:'+pDisplaylote+';">'+pNumelote+'</td><td id="loteCodi" height="40" width="97" class="txtCodiloteCH" valign="middle" align="center" style="display:'+pDisplaycodi+';">'+pCodilote+'</td><td>&nbsp;</td></tr>              </table>            </td>          </tr>          <tr>            <td valign="top">             <div style="overflow:hidden;">              <table cellspacing="0" cellpadding="0" border="0" align="center" height="1"  width="100%" align="center" background=""><tr>  <td colspan="2" height="25"></td></tr><tr>  <td width="28"></td><td id="detaLote" class="txtDetalote" align="left">'+pDetalote+'</td></tr></table></div></td></tr></table></div></td><td>&nbsp;</td></tr></table></td><td>&nbsp;</td></tr><tr><td colspan="3" height="2"></td></tr><tr><td></td><td height="2" align="center"><img src="images/punteada-video-ch.png"></td><td></td></tr><tr><td></td></tr>';
		  document.getElementById('tablaVideo').innerHTML=videoIncludemob;
		}else{	
			embedVideo='<object width="650" height="541">';
			embedVideo+='<param name="movie" wmode="transparent" value="https://www.youtube.com/v/Y1G1j0urKYE?rel=0&color1=0x9a9a9a&color2=0xa8a7a7&border=0&fs=1&showinfo=0&autoplay=1"></param>';
			embedVideo+='<param name="allowFullScreen" value="true"></param>';
			embedVideo+='<embed src="https://www.youtube.com/v/'+innerVideo+'?rel=0&color1=0x9a9a9a&color2=0xa8a7a7&border=0&fs=1&showinfo=0&autoplay=1" type="application/x-shockwave-flash" width="650" height="541" allowfullscreen="true" wmode="transparent"></embed>';
			embedVideo+='</object>';
			if(innerVideo==''){
	      embedVideo='<img src="images/no-video.jpg">';
	    }
			document.getElementById('tdContevideo').innerHTML=embedVideo;
			document.getElementById('conteTitu').innerHTML=pTitulote;
			document.getElementById('loteOrden').innerHTML=pNumelote;
			document.getElementById('loteOrden').style.display=pDisplaylote;
			document.getElementById('loteCodi').innerHTML=pCodilote;
			document.getElementById('loteCodi').style.display=pDisplaycodi;  
			document.getElementById('detaLote').innerHTML=pDetalote;
		}
	tempAlpha = setInterval('setAlphaUp()', 5);
}

function abreRural(pDivqueabre){
	document.getElementById('ruraliframe').src='http://www.elrural.com/canal_rural/vivo';
	document.getElementById('todoDiv').style.zIndex=20;
	document.getElementById(pDivqueabre).style.display='';
	tempAlpha = setInterval('setAlphaUp()', 5);
}

function abreFormC(pDivqueabre){
	document.getElementById('todoDiv').style.zIndex=20;
	document.getElementById(pDivqueabre).style.display='';
	if(navigator.appName == "Microsoft Internet Explorer"){
		document.getElementById(pDivqueabre).style.height=1800+'px';
	  document.getElementById('formCreditoint').style.height=1780+'px';		
		}else{
	  document.getElementById(pDivqueabre).style.height=1700+'px';
	  document.getElementById('formCreditoint').style.height=1700+'px';		
		}
	window.scrollTo(0,0);
  document.getElementById('todoDiv').focus();
	tempAlpha = setInterval('setAlphaUp()', 5);
}

function abreVideofinal(pDivqueabre, pVideo, pTituvideo, pDetavideo, pNumerema){
	if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 || useragent.indexOf('Blackberry') != -1 || useragent.indexOf('Opera Mini') != -1 || useragent.indexOf('Windows CE') != -1 || document.documentElement.offsetWidth < 800){
	innerVideo='<iframe src="http://player.vimeo.com/video/'+pVideo+'?title=0&amp;byline=0&amp;portrait=0&amp;color=49a10a" width="650" height="541" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'
	}else{
	innerVideo='<iframe src="http://player.vimeo.com/video/'+pVideo+'?title=0&amp;byline=0&amp;portrait=0&amp;color=49a10a" width="650" height="541" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'
	}
	
	
	if(innerVideo==''){
	embedVideo='<img src="images/no-video.jpg">';
	}
	else{
	embedVideo=innerVideo;
  }
	document.getElementById('tdContevideofinal').innerHTML=embedVideo;
	document.getElementById('conteTituvideo').innerHTML='Remate TV N&ordm; '+pNumerema+' - '+pTituvideo;
	document.getElementById('todoDiv').style.zIndex=20;
	document.getElementById(pDivqueabre).style.display='';
	tempAlpha = setInterval('setAlphaUp()', 5);
}

function cierraCatalogo(pDivquecierra){
	document.getElementById(pDivquecierra).style.display='none';
	document.getElementById('tdContevideo').innerHTML='';
	document.getElementById('tdContevideofinal').innerHTML='';
	document.getElementById('ruraliframe').src='';
	tempAlpha = setInterval('setAlphaDown()', 5);
	}

function cierraFormC(pDivquecierra){
	document.getElementById('todoDiv').style.zIndex=30;
	document.getElementById(pDivquecierra).style.display='none';
	document.getElementById(pDivquecierra).style.height=0+'px';
	document.getElementById('formCreditoint').style.height=0+'px';
	tempAlpha = setInterval('setAlphaDown()', 5);
	window.scrollTo(0,0);
  document.getElementById('divConteTodo').focus();
}

function setAlphaUp(){
	mLoad();
	if(pOpacity<70){
	if(navigator.appName == "Microsoft Internet Explorer"){
	   pOpacity+=40;	
	   document.getElementById('todoDiv').style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=\'" + pOpacity + "\')";
	}else{
		pOpacity+=15;	
		if (pOpacity<10){
		 document.getElementById('todoDiv').style.opacity = '00.0'+pOpacity;
	  }else{
		 document.getElementById('todoDiv').style.opacity = '0.'+pOpacity;
		}
	 }
	}else{
		clearInterval(tempAlpha);
		pOpacity=80;	
		if(navigator.appName == "Microsoft Internet Explorer"){
	  document.getElementById('todoDiv').style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=\'" + pOpacity + "\')";
	}else{
		document.getElementById('todoDiv').style.opacity = '0.'+pOpacity;
	}
	
 }

}
function setAlphaDown(){
	if(pOpacity>0){
	if(navigator.appName == "Microsoft Internet Explorer"){
	   pOpacity-=30;	
	   document.getElementById('todoDiv').style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=\'" + pOpacity + "\')";
	}else{
		pOpacity-=10;	
		if (pOpacity<30){
		 document.getElementById('todoDiv').style.opacity = '0.0'+pOpacity;
	}else{
		 document.getElementById('todoDiv').style.opacity = '0.'+pOpacity;
		}
	 }
	}else{
		clearInterval(tempAlpha);
		document.getElementById('todoDiv').style.zIndex=0;
	  document.getElementById('todoDiv').style.height=0+'px';
		}
	
	}


function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function mLoad(){

	if(navigator.appName == "Microsoft Internet Explorer"){
	var divh = getDocHeight();		
	var ancho= document.documentElement.offsetWidth;
	document.getElementById('tablaVideo').style.width=ancho+'px';
	document.getElementById('tablaVideofinal').style.width=ancho+'px';
	var alto= divh;
	document.getElementById('todoDiv').style.height=alto+'px';
	//document.getElementById('btnOnline').style.display='';
	}else{
	var divh = getDocHeight();
	var ancho= window.innerWidth;
	//alert(ancho);
	document.getElementById('tablaVideo').style.width=ancho+'px';
	document.getElementById('tablaVideofinal').style.width=ancho+'px';
	var alto= divh;
	document.getElementById('todoDiv').style.height=alto+'px';
	//document.getElementById('btnOnline').style.display='';
	}
}

function abrePDF(pFile){
	window.open(pFile,'PapeDocPresent','toolbar=NO,location=NO,directories=NO,status=YES,menubar=NO,scrollbars=YES,resizable=YES');
}