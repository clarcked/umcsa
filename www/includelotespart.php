<?php

$limit = 3;
$sqlwhere = "";

$sql = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.orden,
lotes.id AS loteid,
lotes.remate_id,
lotes.conjunto_lote_id,
lotes.tipo,
lotes.venta_tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.codigo,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.provincia,
lotes.localidad,
lotes.lugar,
lotes.video,
lotes.foto_1,
lotes.foto_2,
lotes.foto_3,
lotes.foto_4,
lotes.plazo,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
localidades.id,
localidades.nombre AS locanombre,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
remates.id AS remaid,
remates.fecha,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre,
videos_lotes.id AS vlid,
videos_lotes.remate_id AS vlrid,
videos_lotes.lote_id AS vllid,
videos_lotes.video AS vlv
FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Left Join videos_lotes ON videos_lotes.lote_id = lotes.id
WHERE
lotes.venta_tipo =  'PARTICULAR'
" . $sqlwhere . "
LIMIT 0, $limit";

//echo $sql;

$rs = mysql_query($sql);
$sqlTotal = "SELECT FOUND_ROWS() as total";
$rsTotal = mysql_query($sqlTotal);
$rowTotal = mysql_fetch_assoc($rsTotal);
// Total de registros sin limit
$total = $rowTotal["total"];

$sqlCATTotal = "SELECT FOUND_ROWS() as total";
$rsCATTotal = mysql_query($sqlCATTotal);
$rowCATTotal = mysql_fetch_assoc($rsCATTotal);
// Total de registros sin limit
$totalCAT = $rowCATTotal["total"];
?>

<script type="text/javascript">
</script>



    <?php
    $cuentatres = 0;
    $cuentatotal = 0;
    while ($row = mysql_fetch_assoc($rs)) {
        $rematitu = $row['rematitu'];
        $id = $row["id"];
        $lid = $row["loteid"];
        $tituLote = utf8_encode($row["titulo"]);
        $name = utf8_encode($row["orden"]);
        $cuentatres+=1;
        $cuentatotal+=1;
        $detalote1 = 'Cantidad: <b>' . utf8_encode($row['cantidad']) . ' vacunos</b><br>Tipo: <b>' . utf8_encode($row['tiponombre']) . '</b><br>Categor&iacute;a: <b>' . utf8_encode($row['catenombre']) . '</b><br>Peso.: <b>' . utf8_encode($row['peso']) . ' Kgs.</b><br>Localidad: <b>' . utf8_encode($row['locanombre']);
        $detalote2 =  '</b>Provincia: <b>' . utf8_encode($row['provinombre']) . '</b><br>Trazabilidad: <b>' . $row['trazabilidad'] . '</b><br>Garrapata: <b>' . $row['garrapata'] . '</b><br>MIO-MIO: <b>' . $row['mio_mio'] . '</b><br>Caracter&iacute;sticas: <b>' . utf8_encode($row['caracteristicas']) . '</b>';
        //$detalote= "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">";


        $orden = $row['orden'];
        if ($orden != '') {
            if (substr($orden, 0, 2) == "00") {
                $orden = str_replace("00", "", $orden);
                if (substr($orden, 0, 1) == "0") {
                    $orden = str_replace("0", "", $orden);
                }
            } else {
                if (substr($orden, 0, 1) == "0") {
                    $orden = substr_replace($orden, "", 0, 1);
                }
            }
        }

        ?>
        <?php
        if ($row['vlv']) {
            $cadena = $row['vlv'];
        } else {
            $cadena = $row['video'];
        }
        $maximo = strlen($cadena);
        if ($maximo > 3) {
            $cadena_comienzo = "v=";
            $totalstr = strpos($cadena, $cadena_comienzo);
            $varURLYT = substr($cadena, ($totalstr + 2), strlen($cadena));
        } else {
            $varURLYT = '';
        }
        if($cuentatotal==1){
            $fdobtnlote='sel';
            ?>
        <script type="text/javascript">


        </script>
            <?php
        }else{
            $fdobtnlote='off';
         }
        ?>
        
        <div id="divlote<?php echo $lid ?>" class="lote-cont" style="padding: 20px; position: relative; border: solid #FFF 3px; background-color: #dddedf; border-radius: 6px;" onmouseover="mouseOverout(this.id,'on')" onmouseout="mouseOverout(this.id,'off')" onclick="location.href='lotespart.php';">
                                
            
           <span style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:12pt; color: #383838; text-align:left;">LOTE &nbsp; <?php echo $row['codigo'] ?></span>
            
        
            
                <div height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:10pt; color: #383838; text-align:left;"><?php echo $tituLote ?></div>
                    
                <?php
                    if ($row['peso'] == '-' || $row['peso'] == '' || $row['peso'] == 'S / P') {
                        $pesolote = '';
                    } else {
                        $pesolote = utf8_encode($row['peso'] . ' Kgs.');
                    }
                ?>
                <div height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:10pt; color: #383838; text-align:left;"><?php echo $pesolote ?></div>
                <div height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:10pt; color: #383838; text-align:left;"><?php echo utf8_encode($row['locanombre']) ?>, <?php echo utf8_encode($row['provinombre']) ?></div>
            
            
        </div>
        
        <?php

    }
    ?>
