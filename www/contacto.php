<?php
require_once("app/config/bootstrap.php");
use Umc\App\Services\Auth\Auth;
$auth = new Auth();
?>
<!DOCTYPE html>

<?php
$pageID = 'conta';
?>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>:: UMC S.A. Negocios Agropecuarios ::</title>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="includes/funcFondopopup.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#abreStaff").click(function(e) {
				$("#staffTD").fadeIn(1500);
				$("#conteTD").fadeOut(50);

			});
			$("#cierraStaff").click(function(e) {
				$("#conteTD").fadeIn(1500);
				$("#staffTD").fadeOut(50);
			});

		})


		function cambiarEstado() {
			window.status = "UMC S.A. Negocios Agropecuarios - Integrantes"
		}
		cambiarEstado();

		function cambiaFondo(pId, pColor) {
			document.getElementById(pId).style.backgroundColor = pColor;
		}

		function abreDiv(pDiv, pImagenbtn) {
			varDisplay = document.getElementById(pDiv).style.display;
			//alert(varDisplay);
			if (varDisplay == 'none') {
				document.getElementById(pDiv).style.display = '';
				pImagenbtn.src = 'images/btn-div-cierra.png';
			} else {
				document.getElementById(pDiv).style.display = 'none';
				pImagenbtn.src = 'images/btn-div-abre.png';
			}
		}

		function cambiaClase(pId, pClase) {
			document.getElementById(pId).className = pClase;
		}
	</script>

	<?php
	include 'includes/funciones.php';
	?>
</head>

<body>





	<?php
	include 'header.php';
	?>


	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close cierramodal" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 id="myModalLabel">Envienos su consulta</h3>
				</div>
				<div class="modal-body">

					<form class="form-horizontal col-sm-12" method="POST" name="contacto" onSubmit="return validar(this)" action="contacto-send.php">
						<div class="form-group"><label>Asunto</label><input id="subjectH" name="subjectH" type="text" value="<?php echo $subject ?>" class="form-control required" placeholder="Asunto" data-placement="top" data-trigger="manual"></div>
						<div class="form-group"><label>Nombre y Apellido</label><input id="name" name="name" type="text" class="form-control required" placeholder="Tu Nombre y Apellido" data-placement="top" data-trigger="manual"></div>
						<div class="form-group"><label>Empresa</label><input id="empresa" name="empresa" type="text" class="form-control required" placeholder="Tu Empresa" data-placement="top" data-trigger="manual"></div>
						<div class="form-group"><label>E-Mail</label><input id="email" name="email" type="text" class="form-control email" placeholder="email@you.com" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)"></div>
						<div class="form-group"><label>Consulta</label><textarea id="message" name="message" class="form-control" placeholder="Mensaje.." data-placement="top" data-trigger="manual"></textarea></div>
						<div class="form-group"><button type="submit" class="btn btn-success pull-right">ENVIAR</button>
							<p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container" style="margin-top:250px;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6">

					<aside class="sidebar">
						<div class="single contact-info">
							<h3 class="side-title">Informacion de Contacto</h3>
							<ul class="list-unstyled">
								<li>
									<div class="icon"><i class="fa fa-map-marker"></i></div>
									<div class="info">
										<p>Vuelta de Obligado 1947 <br>C.A.B.A. - Argentina</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<div class="info">
										<p> Email:<br>umcfacturacion@gmail.com</p>
									</div>
								</li>
								<li>

									<div class="info">
										<p><b>Representantes:</b></p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>VILLAGUAY: Haciendas Villaguay<br>Tel. (03455) 421050</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>CORRIENTES: Carlos Pellegrini 2072<br>Tel. (0379) 4426066</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>ESQUINA: Federico Casal <br>Tel. (03777) 460340</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>POSADAS: Cía. Misionera de Haciendas<br>Tel. (0376) 4458306</p>
									</div>
								</li>



								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>OLAVARRÍA: Cooperativa Agraria Limitada de Olavarría<br>
											Sr. Julio C. Schmale<br>
											Ofic. 2284-441404. Cel. 2284-356637<br>
											jc_shmale@hotmail.com</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>RAUCH: Sr. Raúl Artiguenave<br>
											Cel. 2494-212742<br>
											raulartiguenave@yahoo.com.ar</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>SANTA ISABEL - TEODELINA:Cooperativa Agraria Unión y Fuerza de Santa Isabel<br>
											Sr. Damián Tazzioli<br>
											Ofic. 3462-490550 Cel. 3462-301937<br>
											oficinas2@coopuyf.com.ar</p>
									</div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="info">
										<p>LAS FLORES:Sr. Cristhian Ruíz.<br>
											Cel. 2244-448662<br>
											floresdelcarmen_2@hotmail.com</p>
									</div>
								</li>


							</ul>
						</div>
					</aside>
				</div><!-- end col -->

				<!-- Contact form -->
				<div class="col-sm-6">
					<form role="form" name="ajax-form" id="ajax-form" action="#" method="post" class="form-main">

						<div class="form-group">
							<label for="name">Nombre y Apellido</label>
							<input class="form-control" id="name" name="name" type="text" value="" placeholder="Nombre y Apellido" required>
							<div class="error" id="err-name" style="display: none;">POR FAVOR INGRESE SU NOMBRE Y APELLIDO</div>
						</div> <!-- /Form-name -->


						<div class="form-group">
							<label for="email">Empresa</label>
							<input class="form-control" id="empresa" name="empresa" type="text" placeholder="Empresa" value="" required>
							<div class="error" id="err-emailvld" style="display: none;">COMPLETE LA EMPRESA</div>
						</div> <!-- /Empresa -->

						<div class="form-group">
							<label for="email">Email</label>
							<input class="form-control" id="email" name="email" type="text" placeholder="Email" value="" required>
							<div class="error" id="err-emailvld" style="display: none;">FORMATO DE CORREO ELECTRÓNICO NO VÁLIDO</div>
						</div> <!-- /Form-email -->

						<div class="form-group">
							<label for="message">Mensaje</label>
							<textarea class="form-control" id="message" name="message" placeholder="Mensaje" rows="5" required></textarea>

							<div class="error" id="err-message" style="display: none;">POR FAVOR ESCRIBA UN MENSAJE</div>
						</div> <!-- /col -->

						<div class="row">
							<div class="col-xs-12">
								<div id="ajaxsuccess" class="text-success">SU MENSAJE FUE ENVIADO CORRECTAMENTE.</div>
								<div class="error" id="err-form" style="display: none;">POR FAVOR REVISE LOS CAMPOS DEL FORMULARIO</div>
								<div class="error" id="err-timedout">LA CONEXIÓN CON EL SERVIDOR HA TERMINADO</div>
								<div class="error" id="err-state"></div>
								<button type="submit" class="btn btn-primary btn-shadow btn-rounded w-md" id="send">ENVIAR</button>
							</div> <!-- /col -->
						</div> <!-- /row -->

					</form> <!-- /form -->
				</div> <!-- end col -->

			</div> <!-- end row -->

		</div>
	</div>

	<br><br>
	<div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
		<?php include 'footer.php' ?>
	</div>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>

</body>

</html>