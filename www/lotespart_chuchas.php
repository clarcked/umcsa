<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<?php
include 'includes/MySQL.php';
include 'includes/funciones.php';

Conectarse();

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $locaselected = '';
} else {
    $locaId = '';
    $locaselected = 'selected';
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
} else {
    $traza = '';
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
} else {
    $garpta = '';
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
} else {
    $miomio = '';
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
} else {
    $cate = '';
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
} else {
    $lotesel = '';
}

if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
} else {
    $conjuntolote = '';
}

$miURLsinP = $_SERVER['SCRIPT_NAME'];

$miURL = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];

$displayCode = '';

//echo $miURL;
// maximo por pagina

$limit = 3;

// pagina pedida
if (isset($_GET["pag"])) {
    $pag = (int) $_GET["pag"];
} else {
    $pag = (int) 0;
}
if ($pag < 1) {
    $pag = 1;
}
$offset = ($pag - 1) * $limit;


//$remaid = '97';


//Busqueda consultas--------

$mesesArr = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
$diasArr = array("DOMINGO", "LUNES", "MARTES", "MI&Eacute;RCOLES", "JUEVES", "VIERNES", "S&Aacute;BADO");


//---------------------------

$sqlwhere = "";
$sqlwherecl = "";
if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
    $sqlwhere = $sqlwhere . "AND lotes.conjunto_lote_id =  '$conjuntolote' ";
    $sqlwherecl = $sqlwherecl . "AND lts.conjunto_lote_id =  '$conjuntolote' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $sqlwhere = $sqlwhere . "AND lotes.localidad =  '$locaId' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
    $sqlwhere = $sqlwhere . "AND lotes.id =  '$lotesel' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
    $sqlwhere = $sqlwhere . "AND lotes.trazabilidad =  '$traza' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
    $sqlwhere = $sqlwhere . "AND lotes.categoria =  '$cate' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
    if ($garpta == 'SI') {
        $sqlwhere = $sqlwhere . "AND (lotes.garrapata =  '$garpta' OR lotes.garrapata =  'ZONA DE LUCHA')";
    } else {
        $sqlwhere = $sqlwhere . "AND lotes.garrapata =  '$garpta'";
    }
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
    $sqlwhere = $sqlwhere . "AND lotes.mio_mio =  '$miomio'";
} else {
    $sqlwhere = $sqlwhere . "";
}


$sqlLT = "SELECT
lts.remate_id AS rmtid,
lts.venta_tipo,
lts.conjunto_lote_id,
lts.orden AS ltorden,
lts.codigo AS ltcert,
lts.id AS ltid,
lts.categoria AS ltcate
FROM
lotes AS lts
WHERE
lts.venta_tipo = 'PARTICULAR' ".$sqlwherecl." ORDER BY ifnull(ltorden, ltcert)";

//echo $sqlLT;

$rsLT = mysql_query($sqlLT);
$cuenta = mysql_num_rows($rsLT);
//echo $cuenta;
$innerselLotes = '';
$lotescuenta = 0;
while ($rowLT = mysql_fetch_array($rsLT)) {
    $lotescuenta+=1;
    if ($lotescuenta == 1) {
        if ($lotesel == '') {

            $innerselLotes = $innerselLotes . '<option value="" selected>TODOS</option>';
        } else {

            $innerselLotes = $innerselLotes . '<option value="">TODOS</option>';
        }
    }

    $ordenLT = $rowLT['ltorden'];
    if ($ordenLT != '') {
        if (substr($ordenLT, 0, 2) == "00") {
            $ordenLT = str_replace("00", "", $ordenLT);
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = str_replace("0", "", $ordenLT);
            }
        } else {
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = substr_replace($ordenLT, "", 0, 1);
            }
        }
    }

    $ordenInner = '';

    if ($rowLT['ltorden'] == '') {
        $ordenInner = 'Certif. N&ordm; ' . $rowLT['ltcert'];
    } else {
        $ordenInner = 'N&ordm; ' . $ordenLT;
    }

    if ($rowLT['ltid'] == $lotesel) {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '" selected>' . $ordenInner . '</option>';
    } else {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '">' . $ordenInner . '</option>';
    }
}



$sqlCAT = "SELECT DISTINCT
SQL_CALC_FOUND_ROWS categorias_ganado.id,
categorias_ganado.id AS cateidselect,
categorias_ganado.nombre AS catenomselect,
categorias_ganado.descripcion,
lotes.categoria,
lotes.remate_id
FROM
categorias_ganado
Inner Join lotes ON lotes.categoria = categorias_ganado.id
WHERE
lotes.venta_tipo =  'PARTICULAR' AND
categorias_ganado.id IN  ( lotes.categoria)";


$sqlCompr = "SELECT
lotes.orden
FROM
lotes
WHERE
lotes.venta_tipo = 'PARTICULAR' AND
orden IS NOT NULL";

$rsCompr = mysql_query($sqlCompr);
$cuentaCompr = mysql_num_rows($rsCompr);

if ($cuentaCompr > 0) {
    $consulorder = "ORDER BY ifnull(lotes.orden, catenombre)";
} else {
    $consulorder = "ORDER BY catenombre";
}


$sqlLugar = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.id,
lotes.provincia,
lotes.lugar,
provincias.id AS provid,
provincias.nombre AS provinombre,
localidades.id as locaid,
localidades.nombre as locanombre
FROM
lotes
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
WHERE
lotes.venta_tipo =  'PARTICULAR'
GROUP BY lotes.localidad
ORDER BY provinombre ASC, locanombre ASC";



$rsLugar = mysql_query($sqlLugar);
$cuentaLugar = mysql_num_rows($rsLugar);
$lugarcombo = '';
$countRowlugar = 0;
while ($rowLugar = mysql_fetch_array($rsLugar)) {
    if ($locaId == $rowLugar['locaid']) {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '" selected>' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    } else {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '">' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    }
}

$sql = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.orden,
lotes.id AS loteid,
lotes.remate_id,
lotes.conjunto_lote_id,
lotes.tipo,
lotes.venta_tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.codigo,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.provincia,
lotes.localidad,
lotes.lugar,
lotes.video,
lotes.foto_1,
lotes.foto_2,
lotes.foto_3,
lotes.foto_4,
lotes.plazo,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
localidades.id,
localidades.nombre AS locanombre,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
remates.id AS remaid,
remates.fecha,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre,
videos_lotes.id AS vlid,
videos_lotes.remate_id AS vlrid,
videos_lotes.lote_id AS vllid,
videos_lotes.video AS vlv
FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Left Join videos_lotes ON videos_lotes.lote_id = lotes.id
WHERE
lotes.venta_tipo =  'PARTICULAR'
" . $sqlwhere . "

" . $consulorder . "

LIMIT $offset, $limit";

//echo $sql;

$rs = mysql_query($sql);
$sqlTotal = "SELECT FOUND_ROWS() as total";
$rsTotal = mysql_query($sqlTotal);
$rowTotal = mysql_fetch_assoc($rsTotal);
// Total de registros sin limit
$total = $rowTotal["total"];

$rsCAT = mysql_query($sqlCAT);

$sqlCATTotal = "SELECT FOUND_ROWS() as total";
$rsCATTotal = mysql_query($sqlCATTotal);
$rowCATTotal = mysql_fetch_assoc($rsCATTotal);
// Total de registros sin limit
$totalCAT = $rowCATTotal["total"];
?>


<script type="text/javascript">
    function cambiaFondo(pId, pColor) {
        document.getElementById(pId).style.backgroundColor = pColor;
    }
    var divflecha='';
function abreCatalogoNew(pLoteId, pCertif, pDivqueabre, pVideo, pTitulote, pDetalote1, pDetalote2, pPlazo, pDivflecha){
        document.getElementById('todoDiv').style.zIndex=20;
        innerVideo = pVideo;
        embedVideo = '<iframe class="youtube-player" type="text/html" width="410" height="326" src="http://www.youtube.com/embed/'+ innerVideo + '?rel=0&theme=light&ps=docs&showinfo=0&controls=1" allowfullscreen frameborder="0" style="background-color: #545b54;"></iframe>';
            if (innerVideo == '') {
                embedVideo = '<img src="images/no-video.jpg" width="410">';

            }
            if(divflecha!=''){
            jQuery('#' + divflecha).fadeOut(200);
            }
            jQuery('#' + pDivqueabre).fadeOut(200, function() {
                document.getElementById('conteVideoylote').style.display='';
                document.getElementById('tdContevideo').innerHTML = embedVideo;
                varAsunto = pCertif;
                document.getElementById('detaLote1').innerHTML = pDetalote1;
                document.getElementById('detaLote2').innerHTML = pDetalote2;
                document.getElementById('spanPlazo').innerHTML = 'PLAZO: ' + pPlazo;
                document.getElementById('divbtnWhatsapp').innerHTML ='<a href="whatsapp://send?text=http://www.umcsa.net/lotespart.php?lotenum='+ pLoteId +'" data-action="share/whatsapp"><i class="fa fa-whatsapp circle" aria-hidden="true"></i></a>';
                var pTituFormat = pTitulote.replace(" ", "+");
                document.getElementById('divbtnFacebook').innerHTML ='<a href=http://www.facebook.com/sharer.php?s=100&p[title]=Sample+title%21&p[url]=http://www.umcsa.net/lotespart.php?lotenum='+ pLoteId +'&p[summary]='+ pTituFormat +'&p[images][0]=http://umcsa.net/images/yacare.png"><i class="fa fa-facebook circle" aria-hidden="true"></i></a>';
                jQuery('#' + pDivqueabre).fadeIn(400);
                jQuery('#' + pDivflecha).fadeIn(400);
                divflecha = pDivflecha;
            });

        tempAlpha = setInterval('setAlphaUp()', 5);
    }
    var btnloteSel='';
    function mouseOverout(pDivid,pInst) {
       if(pInst == 'sel'){
         document.getElementById(btnloteSel).style.backgroundImage = "url('images/fdo-lote-part-off.png')";
         btnloteSel = pDivid;
         document.getElementById(pDivid).style.backgroundImage = "url('images/fdo-lote-part-"+pInst+".png')";
       }else{
           if(btnloteSel!=pDivid){
               document.getElementById(pDivid).style.backgroundImage = "url('images/fdo-lote-part-"+pInst+".png')";
           }
       }
};

</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Expires" CONTENT="0">
            <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
                <meta property="og:title" content="UMC S.A. - Negocios Agropecuarios" />
                <meta property="og:description" content="Consignatarios de hacienda" />
                <meta property="og:image" content="http://umcsa.net/images/yacare.png" />
                <title>:: Haciendas Correntinas S.R.L. ::</title>

                </head>
                <body topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" rightmargin="0" onload="abrePrimero(); mLoad();" onresize="mLoad();" onorientationchange="mLoad();">
                    <div align="center" id="todoDiv" style="position:absolute; top:0; left:0; z-index:0; opacity:0.0;filter:alpha(opacity=00); background-color:#000;display:; width:100%;"></div>
                    <div id="divConteTodo">
                        <div id="divTotal">
                            
                            <div id="conteCentro" style="position:relative;">
                                <?php
                                include 'header.php'
                                ?>
                                <div id="conteContenido" style="position:relative;">
                                </div>
                                        <div id="conteMedio">
                                            <td width="400" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:15pt; line-height:20px; color:#b0b0b0;">LOTES A LA VENTA</td>
                                            <br>                                          
                                            <br>                                          
                                            <td align="right" class="txtFiltro" width="170" style="text-align: left;"><b>FILTROS DE B&Uacute;SQUEDA</b></td>
                                            <td align="right" class="txtFiltro" width="150">UBICACI&Oacute;N DEL LOTE</td>
                                            
                                            <div id="comboLocalidades">
                                                <select id="lotesLoca" class="select" name="lotesLoca" onchange="cambiaPagFiltro('loca', this.value, '<?php echo $locaId ?>');"
                                                                                <option value="5" <?php echo $locaselected ?>>TODOS</option>
                                                                                    <?php echo '<option value="" ' . $locaselected . '>TODOS</option>' . $lugarcombo; ?>
                                                </select>
                                            </div>
                                            <td align="right" class="txtFiltro" width="1">LOTE</td>
                                                                                        
                                            <select id="lotesSel" class="select" name="categorias" onchange="cambiaPagFiltro('lotenum', this.value, '<?php echo $lotesel ?>');">
                                                <?php echo "VER" . $innerselLotes ?>
                                            </select>
                                                                                    
                                            <td align="right" class="txtFiltro" width="1">CATEG.</td>
                                            <select class="select" name="categorias" onchange="cambiaPagFiltro('cate', this.value, '<?php echo $cate ?>', '');">
                                                <?php
                                                $cuentarowscate = 0;
                                                if ($totalCAT > 0) {
                                                    while ($rowCAT = mysql_fetch_array($rsCAT)) {
                                                        $cuentarowscate+=1;
                                                        if ($cuentarowscate == 1) {
                                                            if ($cate == "") {
                                                                ?>
                                                                <option value="" selected>TODOS</option>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <option value="">TODOS</option>
                                                                <?php
                                                            }
                                                        }

                                                        if ($cate == $rowCAT['cateidselect']) {
                                                            ?>
                                                            <option value="<?php echo $rowCAT['cateidselect'] ?>" selected ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $rowCAT['cateidselect'] ?>" ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    ?>
                                                    <option value="" selected>TODOS</option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                                                                        
                                            <td align="right" class="txtFiltro" width="1">TRAZ.</td>
                                                <select class="select" name="colores" onchange="cambiaPagFiltro('tr', this.value, '<?php echo $traza ?>', '');">
                                                    <?php
                                                    if ($traza == '') {
                                                        ?>
                                                        <option value="" selected>TODOS</option>
                                                        <option value="SI">SI</option>
                                                        <option value="NO">NO</option>
                                                        <?php
                                                    }

                                                    if ($traza == 'NO') {
                                                        ?>
                                                        <option value="">TODOS</option>
                                                        <option value="SI">SI</option>
                                                        <option value="NO" selected>NO</option>
                                                        <?php
                                                    }
                                                    if ($traza == 'SI') {
                                                        ?>
                                                        <option value="">TODOS</option>
                                                        <option value="SI"selected>SI</option>
                                                        <option value="NO">NO</option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                                                                        
                                                <td align="right" class="txtFiltro" width="1">GARPTA.</td>
                                                
                                                    <select class="select" name="colores" onchange="cambiaPagFiltro('gr', this.value, '<?php echo $garpta ?>', '');">
                                                        <?php
                                                        if ($garpta == '') {
                                                            ?>
                                                            <option value="" selected>TODOS</option>
                                                            <option value="SI">SI</option>
                                                            <option value="NO">NO</option>
                                                            <?php
                                                        }

                                                        if ($garpta == 'NO') {
                                                            ?>
                                                            <option value="">TODOS</option>
                                                            <option value="SI">SI</option>
                                                            <option value="NO" selected>NO</option>
                                                            <?php
                                                        }
                                                        if ($garpta == 'SI') {
                                                            ?>
                                                            <option value="">TODOS</option>
                                                            <option value="SI"selected>SI</option>
                                                            <option value="NO">NO</option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                                                        
                                                <td align="right" class="txtFiltro" width="60">MIO-MIO.</td>
                                                
                                                    <select class="select" name="colores" onchange="cambiaPagFiltro('mm', this.value, '<?php echo $miomio ?>', '');">
                                                        <?php
                                                        if ($miomio == '') {
                                                            ?>
                                                            <option value="" selected>TODOS</option>
                                                            <option value="SI">SI</option>
                                                            <option value="NO">NO</option>
                                                            <?php
                                                        }

                                                        if ($miomio == 'NO') {
                                                            ?>
                                                            <option value="">TODOS</option>
                                                            <option value="SI">SI</option>
                                                            <option value="NO" selected>NO</option>
                                                            <?php
                                                        }
                                                        if ($miomio == 'SI') {
                                                            ?>
                                                            <option value="">TODOS</option>
                                                            <option value="SI"selected>SI</option>
                                                            <option value="NO">NO</option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>

                                                                            <div id="Layer1" style="width:417px; height:610px; border:solid 0px;">
                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="1" height="100%">
                                                                                <tr>
                                                                                    <?php
                                                                                    $cuentatres = 0;
                                                                                    $cuentatotal = 0;
                                                                                    while ($row = mysql_fetch_assoc($rs)) {
                                                                                        $rematitu = $row['rematitu'];
                                                                                        $id = $row["id"];
                                                                                        $lid = $row["loteid"];
                                                                                        $tituLote = utf8_encode($row["titulo"]);
                                                                                        $name = utf8_encode($row["orden"]);
                                                                                        $cuentatres+=1;
                                                                                        $cuentatotal+=1;
                                                                                        $detalote1 = 'Cantidad: <b>' . utf8_encode($row['cantidad']) . ' vacunos</b><br>Tipo: <b>' . utf8_encode($row['tiponombre']) . '</b><br>Categor&iacute;a: <b>' . utf8_encode($row['catenombre']) . '</b><br>Peso.: <b>' . utf8_encode($row['peso']) . ' Kgs.</b><br>Localidad: <b>' . utf8_encode($row['locanombre']);
                                                                                        $detalote2 =  '</b>Provincia: <b>' . utf8_encode($row['provinombre']) . '</b><br>Trazabilidad: <b>' . $row['trazabilidad'] . '</b><br>Garrapata: <b>' . $row['garrapata'] . '</b><br>MIO-MIO: <b>' . $row['mio_mio'] . '</b><br>Caracter&iacute;sticas: <b>' . utf8_encode($row['caracteristicas']) . '</b>';
                                                                                        //$detalote= "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">";


                                                                                        $orden = $row['orden'];
                                                                                        if ($orden != '') {
                                                                                            if (substr($orden, 0, 2) == "00") {
                                                                                                $orden = str_replace("00", "", $orden);
                                                                                                if (substr($orden, 0, 1) == "0") {
                                                                                                    $orden = str_replace("0", "", $orden);
                                                                                                }
                                                                                            } else {
                                                                                                if (substr($orden, 0, 1) == "0") {
                                                                                                    $orden = substr_replace($orden, "", 0, 1);
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        ?>
                                                                                        <?php
                                                                                        if ($row['vlv']) {
                                                                                            $cadena = $row['vlv'];
                                                                                        } else {
                                                                                            $cadena = $row['video'];
                                                                                        }
                                                                                        $maximo = strlen($cadena);
                                                                                        if ($maximo > 3) {
                                                                                            $cadena_comienzo = "v=";
                                                                                            $totalstr = strpos($cadena, $cadena_comienzo);
                                                                                            $varURLYT = substr($cadena, ($totalstr + 2), strlen($cadena));
                                                                                        } else {
                                                                                            $varURLYT = '';
                                                                                        }
                                                                                        if($cuentatotal==1){
                                                                                            $fdobtnlote='sel';
                                                                                            ?>
                                                                                        <script type="text/javascript">
                                                                                            function abrePrimero(){
                                                                                            //alert('abreee');
                                                                                            abreCatalogoNew('<?php echo $row['loteid'] ?>', '<?php echo $row['codigo'] ?>','catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>', '<?php echo utf8_encode($row['plazo'])?>', 'divflecha<?php echo $row['loteid']?>');
                                                                                            btnloteSel='divlote<?php echo $lid ?>';
                                                                                            }

                                                                                        </script>
                                                                                            <?php
                                                                                        }else{
                                                                                            $fdobtnlote='off';
                                                                                            }
                                                                                        ?>
                                                                                        <td width="401" height="179" style="cursor:pointer;" onclick="abreCatalogoNew('<?php echo $row['loteid'] ?>','<?php echo $row['codigo'] ?>','catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>','<?php echo utf8_encode($row['plazo'])?>', 'divflecha<?php echo $row['loteid']?>');">
                                                                                            <div id="divlote<?php echo $lid ?>" style="border:solid 0px;width:401px; height: 179px; background-image:url(images/fdo-lote-part-<?php echo $fdobtnlote ?>.png)" onmouseover="mouseOverout(this.id,'on')" onmouseout="mouseOverout(this.id,'off')" onclick="mouseOverout(this.id,'sel')">
                                                                                                <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                    <tr>
                                                                                                        <td width="18"></td>
                                                                                                        <td valign="top">
                                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="100%">
                                                                                                                <tr style="display:<?php echo $displayCode ?>;">
                                                                                                                    <td class="txtCodilote" height="1" valign="top" style="display:<?php echo $displayCode ?>;">
                                                                                                                        <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="1">
                                                                                                                            <tr>
                                                                                                                                <td height="28"></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td class="txtCodilote" align="left" valign="top" height="1" class="txtLotecertif2" style="text-align: left;"><span style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:14pt; color: #383838; text-align:left;">&nbsp;Lote </span><span style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:15pt; color: #383838; text-align:left;"><?php echo $row['codigo'] ?></span></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="14"></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:13pt; color: #383838; text-align:left;"><?php echo $tituLote ?></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="10"></td>
                                                                                                                            </tr>
                                                                                                                            <?php
                                                                                                                            if ($row['peso'] == '-' || $row['peso'] == '' || $row['peso'] == 'S / P') {
                                                                                                                                $pesolote = '';
                                                                                                                            } else {
                                                                                                                                $pesolote = utf8_encode($row['peso'] . ' Kgs.');
                                                                                                                            }
                                                                                                                            ?>
                                                                                                                            <tr>
                                                                                                                                <td height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:13pt; color: #383838; text-align:left;"><?php echo $pesolote ?></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="10"></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td height="1" class="txtLotecertif2" style="font-family: 'Roboto', sans-serif; font-weight: bolder; font-size:13pt; color: #383838; text-align:left;"><?php echo utf8_encode($row['locanombre']) ?>, <?php echo utf8_encode($row['provinombre']) ?></td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td></td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td width="10"></td>
                                                                                                        <td width="150">
                                                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="152" height="160">
                                                                                                                <tr>
                                                                                                                    <td height="8"></td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <?php
                                                                                                                    $fotoCH = '';
                                                                                                                    if ($row['foto_1']!='' || $row['foto_2']!='' || $row['foto_3']!='' || $row['foto_4']!=''){
                                                                                                                        if($row['foto_1']!=''){
                                                                                                                            $fotoCH = $row['foto_1'];
                                                                                                                        }else if ($row['foto_2']!=''){
                                                                                                                            $fotoCH = $row['foto_2'];
                                                                                                                        }else if ($row['foto_3']!=''){
                                                                                                                            $fotoCH = $row['foto_3'];
                                                                                                                        }else if ($row['foto_4']!=''){
                                                                                                                            $fotoCH = $row['foto_4'];
                                                                                                                        }
                                                                                                                        //echo $row['foto_1'];
                                                                                                                ?>
                                                                                                                    <td height="1"><div style="overflow: hidden; width: 146px; height: 146px; top: 8px; border: solid 2px #fff;"><img src="repositorio/<?php echo $fotoCH ?>" height="150"></div></td>
                                                                                                                <?php }else{ ?>
                                                                                                                    <td height="1"><div style="overflow: hidden; width: 146px; height: 146px; top: 8px; border: solid 2px #fff;"><img src="images/no-foto.jpg" height="150"></div></td>
                                                                                                                <?php } ?>
                                                                                                                </tr>
                                                                                                            </table>

                                                                                                        </td>
                                                                                                        <td width="14"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td width="28"><div id="divflecha<?php echo $row['loteid'] ?>" style="display:none;"><img src="images/flecha-lote.jpg" id="imgflecha"></div></td>
                                                                                        <?php
                                                                                        if ($cuentatres == 1) {
                                                                                            $cuentatres = 0;
                                                                                            ?>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="7" height="36"></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                        if ($cuentatotal == $total) {

                                                                                        } else {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <script type="text/javascript">
                                                                                    </script>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="7" height="20" align="center"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="7"></td>
                                                                                </tr>
                                                                            </table>
                                                                            </div>
                                                                        </td>
                                                                        <td bgcolor="#e6e6e6" width="440" valign="top" style="border: solid 0px;">
                                                                            <div align="center" id="catalogDiv" style="position:relative; top:0; z-index:30; text-align:center;">
                                                                                <div id="conteVideoylote" style="display:none; border:solid 0px; height: 648px;">
                                                                                    <?php include 'videoinclude_part.php' ?>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td bgcolor="#FFFFFF" colspan="4" height="20"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" height="20" align="center">
                                                                            <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="20">
                                                                                <tr>
                                                                                    <?php
                                                                                    $totalPag = ceil($total / $limit);
                                                                                    ?>
                                                                                    <script type="text/javascript">

                                                                                            function utf8_encode(argString) {
                                                                                                // Encodes an ISO-8859-1 string to UTF-8
                                                                                                //
                                                                                                // version: 1109.2015
                                                                                                // discuss at: http://phpjs.org/functions/utf8_encode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                                                // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                                                // +   improved by: sowberry
                                                                                                // +    tweaked by: Jack
                                                                                                // +   bugfixed by: Onno Marsman    // +   improved by: Yves Sucaet
                                                                                                // +   bugfixed by: Onno Marsman
                                                                                                // +   bugfixed by: Ulrich
                                                                                                // +   bugfixed by: Rafal Kukawski
                                                                                                // *     example 1: utf8_encode('Kevin van Zonneveld');    // *     returns 1: 'Kevin van Zonneveld'
                                                                                                if (argString === null || typeof argString === "undefined") {
                                                                                                    return "";
                                                                                                }
                                                                                                var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
                                                                                                var utftext = "",
                                                                                                        start, end, stringl = 0;

                                                                                                start = end = 0;
                                                                                                stringl = string.length;
                                                                                                for (var n = 0; n < stringl; n++) {
                                                                                                    var c1 = string.charCodeAt(n);
                                                                                                    var enc = null;
                                                                                                    if (c1 < 128) {
                                                                                                        end++;
                                                                                                    } else if (c1 > 127 && c1 < 2048) {
                                                                                                        enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
                                                                                                    } else {
                                                                                                        enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
                                                                                                    }
                                                                                                    if (enc !== null) {
                                                                                                        if (end > start) {
                                                                                                            utftext += string.slice(start, end);
                                                                                                        }
                                                                                                        utftext += enc;
                                                                                                        start = end = n + 1;
                                                                                                    }
                                                                                                }
                                                                                                if (end > start) {
                                                                                                    utftext += string.slice(start, stringl);
                                                                                                }
                                                                                                return utftext;
                                                                                            }


                                                                                            function utf8_decode(str_data) {
                                                                                                // Converts a UTF-8 encoded string to ISO-8859-1
                                                                                                //
                                                                                                // version: 1109.2015
                                                                                                // discuss at: http://phpjs.org/functions/utf8_decode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                                                // +      input by: Aman Gupta
                                                                                                // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                                                // +   improved by: Norman "zEh" Fuchs
                                                                                                // +   bugfixed by: hitwork    // +   bugfixed by: Onno Marsman
                                                                                                // +      input by: Brett Zamir (http://brett-zamir.me)
                                                                                                // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                                                // *     example 1: utf8_decode('Kevin van Zonneveld');
                                                                                                // *     returns 1: 'Kevin van Zonneveld'
                                                                                                var tmp_arr = [],
                                                                                                        i = 0,
                                                                                                        ac = 0,
                                                                                                        c1 = 0,
                                                                                                        c2 = 0, c3 = 0;

                                                                                                str_data += '';

                                                                                                while (i < str_data.length) {
                                                                                                    c1 = str_data.charCodeAt(i);
                                                                                                    if (c1 < 128) {
                                                                                                        tmp_arr[ac++] = String.fromCharCode(c1);
                                                                                                        i++;
                                                                                                    } else if (c1 > 191 && c1 < 224) {
                                                                                                        c2 = str_data.charCodeAt(i + 1);
                                                                                                        tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                                                                                                        i += 2;
                                                                                                    } else {
                                                                                                        c2 = str_data.charCodeAt(i + 1);
                                                                                                        c3 = str_data.charCodeAt(i + 2);
                                                                                                        tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                                                                                                        i += 3;
                                                                                                    }
                                                                                                }
                                                                                                return tmp_arr.join('');
                                                                                            }


                                                                                            function urlencode(str) {
                                                                                                str = escape(str);
                                                                                                str = str.replace('+', '%2B');
                                                                                                str = str.replace('%20', '+');
                                                                                                str = str.replace('*', '%2A');
                                                                                                str = str.replace('/', '%2F');
                                                                                                str = str.replace('@', '%40');
                                                                                                return str;
                                                                                            }

                                                                                            function urldecode(str) {
                                                                                                str = str.replace('+', ' ');
                                                                                                str = unescape(str);
                                                                                                return str;
                                                                                            }

                                                                                            pagSig = <?php echo $pag ?> + 1
                                                                                            pagAnt = <?php echo $pag ?> - 1
                                                                                            function cambiaPag(pPag) {
                                                                                                if (pPag > <?php echo $totalPag ?> || pPag < 1) {

                                                                                                } else {
                                                                                                    url = '<?php echo $miURL ?>';
                                                                                                    url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=' + pPag;
                                                                                                    window.location = url;

                                                                                                }

                                                                                            }



                                                                                            function cambiaPagFiltro(pPar, pFiltro, pVar) {
                                                                                                if (pPar == 'lotenum') {
                                                                                                    if (pFiltro == '') {
                                                                                                        url = '<?php echo $miURLsinP ?>?;
                                                                                                    } else {
                                                                                                        url = '<?php echo $miURLsinP ?>?lotenum=' + pFiltro;
                                                                                                    }
                                                                                                } else {
                                                                                                    url = '<?php echo $miURL ?>';
                                                                                                    url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=1';
                                                                                                    url = url.replace('&lotenum=<?php echo $lotesel ?>', '');
                                                                                                    if (pFiltro == '') {
                                                                                                        url = url.replace('&' + pPar + '=' + pVar, '');
                                                                                                    } else {
                                                                                                        url = url.replace('&' + pPar + '=' + pVar, '') + '&' + pPar + '=' + pFiltro;
                                                                                                    }
                                                                                                }

                                                                                                window.location = url;
                                                                                            }
                                                                                            function enviar_formulario() {
                                                                                                document.selUbic.submit();
                                                                                            }

                                                                                    </script>
                                                                                    <td width="30">&nbsp;</td>
                                                                                    <td colspan="3" bgcolor="#ffffff">
                                                                                        <table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="20">
                                                                                            <tr>
                                                                                                <td>&nbsp;</td>
                                                                                                <td width="1"><img src="images/ico-pag-ret.jpg" style="cursor:pointer;" onclick="cambiaPag(pagAnt)"></td>
                                                                                                <td width="10">&nbsp;</td>
                                                                                                <td width="1" class="txtNumepagiOff">P&aacute;gina&nbsp;&nbsp;</td>
                                                                                                <?php
                                                                                                $links = array();
                                                                                                for ($i = 1; $i <= $totalPag; $i++) {
                                                                                                    if ($pag == $i) {
                                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagi\" valign=\"middle\" bgcolor=\"#b44422;\">" . $i . "</td>";
                                                                                                    } else {
                                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagiOff\" valign=\"middle\" style=\"cursor:pointer;\" onclick=\"cambiaPag(" . $i . ")\"><a href=\"javascript:cambiaPag(" . $i . ")\">" . $i . "</a></td>";
                                                                                                    }
                                                                                                }
                                                                                                echo implode("<td width=\"1\"></td>", $links);
                                                                                                ?>
                                                                                                <td width="10">&nbsp;</td>
                                                                                                <td width="1"><img src="images/ico-pag-av.jpg" style="cursor:pointer;" onclick="cambiaPag(pagSig)"></td>
                                                                                                <td>&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="30">&nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20"></td>
                                                                        <td bgcolor="#FFFFFF" colspan="4" height="20"></td>
                                                                        <td width="20"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                                <td></td>
                                                            </tr>
                                                                </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="40"></td>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr><td></td></tr>
                                            </table>
                                        </div>
                                        <?php
                                            //include 'footer.php';
                                        ?>
                                    </td>
                                </tr>
                                </table>
                            </div>
                                        </div>
                                        </body>
                                        </html>
