<?php
//header('Content-Type:text/html; charset=UTF-8');
$activeIndex = '';
$activeRema = '';
$activePart = '';
$activeNos = '';
$activeConta = '';
$activePro = '';
if ($pageID == 'index') {
    $activeIndex = 'class="active"';
} else if ($pageID == 'rema') {
    $activeRema = 'class="active"';
} else if ($pageID == 'part') {
    $activePart = 'class="active"';
} else if ($pageID == 'nos') {
    $activeNos = 'class="active"';
} else if ($pageID == 'conta') {
    $activeConta = 'class="active"';
} else if ($pageID == 'lotespart') {
    $activePart = 'class="active"';
} else if ($pageID == 'pro') {
    $activePro = 'class="active"';
}


?>
<nav class="navbar-fixed-top navbar-inverse" style="">
    <div style="height: 90px;" class="hhh1"></div>
    <div class="container-fluid padding-0" class="hhh1">
        <div style="border: solid 0px #c3c1bf; text-align:center; z-index: 20; margin-top: -90px;"><img
                    src="images/logo2.png" class="image-responsive logo"></div>
        <div class="navbar-header" style="">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar" style="background-color: #c3c1bf;">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="social-head">
            <div class="hidden-xs"
                 style="position: absolute; left: 50px; margin: 10px; font-size: 25px; color: #6d6d6d;"
                 onclick="window.open('https://www.facebook.com/umcnegociosagropecuarios');"><i
                        class="fa fa-facebook-square" aria-hidden="true" style="cursor:pointer;"></i></div>
            <div class="hidden-xs"
                 style="position: absolute; left: 80px; margin: 10px; font-size: 25px; color: #6d6d6d;"
                 onclick="window.open('https://www.instagram.com/umc_sa/');"><i class="fa fa-instagram"
                                                                                aria-hidden="true"
                                                                                style="cursor:pointer;"></i></div>
        </div>

        <div id="navbar" class="collapse navbar-collapse center" style="">
            <div class="navbar-inner text-center">
                <ul class="nav navbar-nav navbar">
                    <!-- <li <?php echo $activeIndex ?>><div style="width:420px;">&nbsp;</div></li> -->
                    <li <?php echo $activeIndex ?>><a href="index.php" class="txtMenu"><b>INICIO</b></a></li>
                    <li <?php echo $activeRema ?>><a href="remates.php" class="txtMenu"><b>REMATES</b></a></li>
                    <li <?php echo $activePart ?>><a href="lotespart.php" class="txtMenu"><b>LOTES PARTICULARES</b></a>
                    </li>
                    <li <?php echo $activeNos ?>><a href="umc.php" class="txtMenu"><b>NOSOTROS</b></a></li>
                    <li <?php echo $activeConta ?>><a href="contacto.php" class="txtMenu"><b>CONTACTO</b></a></li>
                    <? if ($auth->islogged()) : ?>
                        <li <?php echo $activePro ?>><a href="profile.php" class="txtMenu"><b>MI CUENTA</b></a></li>
                        <li><a href="users/logout.php" class="txtMenu"><b>SALIR</b></a></li>
                    <? else : ?>
                        <li><a href="#login-modal" class="txtMenu" data-toggle="modal" data-target="#login-modal"><b>INGRESAR</b></a>
                        </li>
                    <? endif; ?>
                </ul>
            </div>
            <div class="hidden-xs textcc"
                 style="position: absolute; z-index: 10; text-align: right; right: 10%; top: 20px;">C.A.B.A.: (+5411)
                4780-1278<br>Villaguay (E.R.) <b>(03455) 421050</b></div>
            <div class="hidden-xs" style="position: absolute; z-index: 9; text-align: right; right: 0px; top: 0px;"><img
                        class="gradhead hidden" src="images/gradient-header.png"></div>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

<? if (!$auth->islogged()) : ?>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="../app/css/login.css"/>
    <div class="modal fade" id="login-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" class="class pull-right icon">
                        <i class="fa fa-times"></i>
                    </a>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <? include_once __path("/src/views/auth/login-form.php"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>


<script type="text/javascript">
    $(document).ready(function () {
        $(".contactogral").on("click", function (e) {
            e.preventDefault(); // avoids calling preview.php
            //var datos = $(this).attr('href');
            $.ajax({
                type: "POST",
                cache: false,
                url: "contacto.php", // preview.php
                //data: datos, // all form fields
                success: function (data) {
                    // on success, post (preview) returned data in fancybox
                    $.fancybox(data, {
                        // fancybox API options
                        fitToView: false,

                        autoSize: true,
                        padding: 50,
                        closeClick: false,
                        openEffect: 'none',
                        closeEffect: 'none',

                    }); // fancybox
                } // success
            }); // ajax
        }); // on

    })
</script>