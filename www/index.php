<?php
require_once("app/config/bootstrap.php");

use Umc\App\Services\Auth\Auth;

$auth = new Auth();
?>
<!DOCTYPE html>
<?php
include 'includes/MySQL.php';
$link = Conectarse();

include 'includes/funciones.php';

$pageID = 'index';

$result = mysql_query("SELECT
remates.banner_g,
remates.banner_ch,
remates.id,
remates.archivo,
remates.numero
FROM
remates

WHERE
(remates.banner_g IS NOT NULL  OR
remates.banner_ch IS NOT NULL)  AND
remates.fecha >=  curdate() 

ORDER BY
remates.fecha ASC", $link);

$innerBanners = '';
$posIndex = 50;
$controw = 0;

while ($row = mysql_fetch_array($result)) {

    if ($row['archivo']) {
        $comparchivo = 1;
    } else {
        $comparchivo = 2;
    }

    $controw += 1;

    if ($controw == 1) {
        if ($row['banner_ch']) {
            $innerBanners = $row['banner_ch'];
            $bannerdoble = 'SI';
        } else {
            $innerBanners = $row['banner_g'];
            $bannerdoble = 'NO';
        }
        $linkrema = 'rematedeta.php?remaid=' . $row['id'];
        //---hard coded por Diego--//
        $linkremaTEMP = 'rematedeta.php?remaid=13';
        //------------------------//
        $linkarchivo = 'repositorio/' . $row['archivo'];
    }


    $posIndex = -1;
}

// Include and instantiate the class.
require_once 'includes/mobiledetect/Mobile_Detect.php';
$detect = new Mobile_Detect;
?>


<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title>:: UMC S.A. Negocios Agropecuarios ::</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!--<body onload="functionClickBanner('clickrematepopup');">-->
<body onload="">
<?php
    include 'header.php';
    if ($detect->isMobile()) {
    ?>
    <div style="display: none;"><a id="clickrematepopup" href="javascript:void(0);"></a></div>
    <?php
} else {
    ?>
    <div style="display: none;"><a id="clickrematepopup"
                                   href="ifr-afiche.php?varimg=delfabro-2021-vivo-ch.png&vardest=https://remates.elrural.com/remate/1521&w=450&h=450"
                                   data-fancybox
                                   data-options='{"type" : "iframe", "iframe" : {"preload" : false, "scrolling": "no", "css" : {"width" : "458px", "height" : "458px", "scrolling" : "no"}}}'></a>
    </div>
    <!--<div style="display: none;"><a id="clickrematepopup" href="ifr-afiche.php?varimg=remate-7-8-2021.jpeg&vardest=javascript:void(0);" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "scrolling": "no", "css" : {"width" : "458px", "height" : "458px", "scrolling" : "no"}}}'></a></div>-->
    <?php
}
?>

<div class="container" style="margin-top:240px;">

    <div class="container-fluid">
        <div style="margin-bottom:50px; display: none;">
            <img src="images/saludo-umc-2020-21.jpg" class="img-responsive center-block d-block mx-auto"
                 alt="FELICIDADES">
        </div>


        <div style="margin-bottom:40px; padding: auto; display:none;">
            <!--<a data-fancybox title="REMATE EN VIVO" href="https://www.youtube.com/watch?v=E6ngpVGcGe0">-->
            <a title="click para PRE OFERTAR"
               href="https://remates.elrural.com/remate/1935-umc-y-haciendas-villaguay-genetica-del-mercosur-online"
               target="_blank">
                <img src="images/banner-mercosur-vivo-ch.png" border="0" class="hidden-md hidden-sm hidden-lg"
                     style="max-width: 100%; width: 100%; left: 0px;">
                <img src="images/banner-mercosur-vivo-g.png" border="0" class="hidden-xs"
                     style="max-width: 100%; width: 100%;">
            </a>
        </div>
        <div style="margin-bottom:10px; padding: auto; display:none;">

            <a href="https://remates.elrural.com/remate/1521" target="_blank"><img src="images/delfabro-2021-vivo-g.png"
                                                                                   border="0" class="hidden-xs"
                                                                                   style="max-width: 100%; width: 100%;">
                <img src="images/delfabro-2021-vivo-ch.png" border="0" class="hidden-md hidden-sm hidden-lg"
                     style="max-width: 100%; width: 100%; left: 0px;">
            </a>
        </div>
        <div style="margin-bottom:30px; margin-top:10px;left: 0px; margin-right: 0px; text-align: right; display:none;">
            <a href="pdf/catalogo-genetica-del-mercosur-2021.pdf" target="_blank"><img src="images/btn-catalogo.png"
                                                                                       border="0" class=""
                                                                                       style="max-width: 200px; width: 200px;"></a>
        </div>
        <div style="">
            <?php include 'bannerremate.php' ?>
        </div>
        <div style="display: ; position: absolute; txt-align: center; margin-bottom: 40px; padding: 30px; left: 0px; right: 0px; width: auto; height: auto;">
            <div class="txtMenu"
                 style="color:#000; font-size: 14pt; margin-bottom: 20px; margin-top: -8px; background-color: #fbf3be; padding: 10px;">
                <b>INFO LINIERS</b></div>
            <div>
                <table border="0" width="100%">
                    <tbody>
                    <tr>
                        <td height="1">
                            <a class="variousLiniers" data-fancybox data-type="iframe" border="0"
                               title="CLICK PARA ABRIR" data-src="ifr-liniers.php" href="javascript:;">
                                <div class="hidden-md hidden-lg hidden-sm"
                                     style="margin: auto; margin-bottom: 10px; padding: 8px; color:#fff; font-size: 11pt; font-weight: 700; background-color: #3e3e3e; border-radius: 10px; width: 310px;">
                                    NUESTRAS VENTAS EN LINIERS&nbsp;<span style="font-size: 16pt;">+</span></div>
                            </a>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="380" bgcolor="#ffffff">
                            <iframe style="margin:auto;" src="http://www.delsector.com/ifr-umc.php" frameborder="0"
                                    width="315" height="250"></iframe>
            </div>
            </td>
            <td>
                <div class="hidden-xs"
                     style="position: relative; overflow: hidden; height: 250px; right: 20px; top: 0px; margin-top: 0px;">
                    <a class="variousLiniers" data-fancybox data-type="iframe" title="CLICK PARA ABRIR"
                       data-src="ifr-liniers.php" href="javascript:;">
                        <div style="position:absolute; top:10px; left: 10px; padding: 8px; color:#fff; font-size: 13pt; font-weight: 700; background-color: #3e3e3e; border-radius: 10px;">
                            NUESTRAS VENTAS EN LINIERS&nbsp;<span style="font-size: 16pt;">+</span></div>
                    </a><img src="images/liniers-grande.jpg" class="img-responsive center-block d-block mx-auto"></div>
            </td>
            </tr>
            </tbody>
            </table>


            <div style="height: 20px;"></div>

        </div>
    </div>
</div>

<div id="divfooter"
     style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
    <?php include 'footer.php' ?>
</div>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>