<!DOCTYPE html>
<?php
include 'includes/MySQL.php';
$link = Conectarse();

include 'includes/funciones.php';

$pageID = 'rema';

if (isset($_GET["extappid"])) {
$extappid= $_GET["extappid"];   
$lotesruralall = json_decode(file_get_contents('https://www.api.rural.ag/umc/lotes_a_remate/'.$extappid.'?'));
//$cuenta = count($lotesrural);
} else {
    ?>
    <script type="text/javascript">
        location.href = 'index.php'
    </script>
    <?php
}

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $locaselected = '';
} else {
    $locaId = '';
    $locaselected = 'selected';
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
} else {
    $traza = '';
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
} else {
    $garpta = '';
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
} else {
    $miomio = '';
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
} else {
    $cate = '';
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
} else {
    $lotesel = '';
}



$miURLsinP = $_SERVER['SCRIPT_NAME'];

$miURL = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];

//echo $miURL;
// maximo por pagina

$limit = 14;



// pagina pedida
if (isset($_GET["pag"])) {
    $pag = (int) $_GET["pag"];
} else {
    $pag = (int) 0;
}
if ($pag < 1) {
    $pag = 1;
}
$offset = ($pag - 1) * $limit;

if (isset($_GET["remaid"])) {
    $remaid = (int) $_GET["remaid"];
} else {
    ?>
    <script type="text/javascript">
        location.href = 'index.php'
    </script>
    <?php
}

$arrlotesfitrado = array ();

foreach($lotesruralall as $arrlotes){
    $varfil1='';
    $varfil2='';
    if($locaId != ''){
    $varfil1=$locaId;
    $varfil2=$arrlotes->localidad_id;    
    } else if($cate != ''){
    $varfil1=$cate;
    $varfil2=$arrlotes->categoria;    
    } else if($lotesel != ''){
    $varfil1=$lotesel;
    $varfil2=$arrlotes->id;    
    }
    if($varfil1==$varfil2){
        $arrlotesfitrado[] = [
        'id' => $arrlotes->id,
        'nro_lote' => $arrlotes->nro_lote,
        'cert' => $arrlotes->cert,
        'localidad_texto' => $arrlotes->localidad_texto,
        'localidad_id' => $arrlotes->localidad_id,    
        'titulo' => $arrlotes->titulo,
        'detalle' => $arrlotes->detalle,
        'dpto' => $arrlotes->dpto,
        'cantidad' => $arrlotes->cantidad,     
        'peso' => $arrlotes->peso,
        'mail_contacto' => $arrlotes->mail_contacto,     
        'video' => $arrlotes->video,
        'categoria' => $arrlotes->categoria,
        'raza' => $arrlotes->raza    
];
    }
    
    
    
}

$jsonfiltrado = json_encode($arrlotesfitrado);
$lotesrural = json_decode($jsonfiltrado);
$cuenta = count($lotesrural);


//Busqueda consultas--------

$mesesArr = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
$diasArr = array("DOMINGO", "LUNES", "MARTES", "MI&Eacute;RCOLES", "JUEVES", "VIERNES", "S&Aacute;BADO");


//---------------------------

$sqlwhere = "";
$sqlwherecl = "";
if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
    $sqlwhere = $sqlwhere . "AND lotes.conjunto_lote_id =  '$conjuntolote' ";
    $sqlwherecl = $sqlwherecl . "AND lts.conjunto_lote_id =  '$conjuntolote' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $sqlwhere = $sqlwhere . "AND lotes.localidad =  '$locaId' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
    $sqlwhere = $sqlwhere . "AND lotes.id =  '$lotesel' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
    $sqlwhere = $sqlwhere . "AND lotes.trazabilidad =  '$traza' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
    $sqlwhere = $sqlwhere . "AND lotes.categoria =  '$cate' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
    if ($garpta == 'SI') {
        $sqlwhere = $sqlwhere . "AND (lotes.garrapata =  '$garpta' OR lotes.garrapata =  'ZONA DE LUCHA')";
    } else {
        $sqlwhere = $sqlwhere . "AND lotes.garrapata =  '$garpta'";
    }
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
    $sqlwhere = $sqlwhere . "AND lotes.mio_mio =  '$miomio'";
} else {
    $sqlwhere = $sqlwhere . "";
}

$rsLT = mysql_query($sqlLT);

$innerselLotes = '';
$lotescuenta = 0;
foreach($lotesrural as $lotesjson){ 
    $lotescuenta+=1;
    if ($lotescuenta == 1) {
        if ($lotesel == '') {

            $innerselLotes = $innerselLotes . '<option value="" selected>TODOS</option>';
        } else {

            $innerselLotes = $innerselLotes . '<option value="">TODOS</option>';
        }
    }

    $ordenLT = $lotesjson->nro_lote;
    if ($ordenLT != '') {
        if (substr($ordenLT, 0, 2) == "00") {
            $ordenLT = str_replace("00", "", $ordenLT);
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = str_replace("0", "", $ordenLT);
            }
        } else {
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = substr_replace($ordenLT, "", 0, 1);
            }
        }
    }

    $ordenInner = '';
    $ordenInner = 'N&ordm; ' . $ordenLT;
    if ($lotesjson->id == $lotesel) {

        $innerselLotes = $innerselLotes . '<option value="' . $lotesjson->id . '" selected>' . $ordenInner . '</option>';
    } else {

        $innerselLotes = $innerselLotes . '<option value="' . $lotesjson->id . '">' . $ordenInner . '</option>';
    }
}



$sqlCAT = "SELECT DISTINCT
SQL_CALC_FOUND_ROWS categorias_ganado.id,
categorias_ganado.id AS cateidselect,
categorias_ganado.nombre AS catenomselect,
categorias_ganado.descripcion,
lotes.categoria,
lotes.remate_id
FROM
categorias_ganado
Inner Join lotes ON lotes.categoria = categorias_ganado.id
WHERE
lotes.remate_id =  '$remaid' AND
categorias_ganado.id IN  ( lotes.categoria)";

$sqlRema = "SELECT
remates.id,
remates.numero,
remates.titulo,
remates.banner_g,
remates.banner_ch,
remates.fecha,
remates.hora_inicio,
remates.hora_fin,
remates.zona,
remates.tipo,
remates.lugar,
remates.locacion,
remates.descripcion,
remates.yacare_visible,
remates.banner_g,
remates.banner_g_visible,
remates.banner_g_z_index,
remates.banner_g_pos_x,
remates.banner_g_pos_y,
remates.banner_g_link,
remates.cantidad,
remates.activo,
remates.video_final_1,
remates.video_titu_1,
remates.video_final_2,
remates.video_titu_2,
remates.video_final_3,
remates.video_titu_3,
remates.video_final_4,
remates.video_titu_4,
remates.video_final_5,
remates.video_titu_5,
remates.archivo,
remates.precios,
remates.mapa_img
FROM
remates
WHERE
remates.id =  '$remaid'";

$sqlCompr = "SELECT
lotes.orden
FROM
lotes
WHERE
remate_id = '$remaid' AND
orden IS NOT NULL";

$rsCompr = mysql_query($sqlCompr);
$cuentaCompr = mysql_num_rows($rsCompr);

if ($cuentaCompr > 0) {
    $consulorder = "ORDER BY ifnull(lotes.orden, catenombre)";
} else {
    $consulorder = "ORDER BY catenombre";
}


$sqlLugar = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.id,
lotes.provincia,
lotes.lugar,
provincias.id AS provid,
provincias.nombre AS provinombre,
localidades.id as locaid,
localidades.nombre as locanombre
FROM
lotes
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
WHERE
lotes.remate_id =  '$remaid'
GROUP BY lotes.localidad
ORDER BY provinombre ASC, locanombre ASC";




$lugarcombo = '';
$countRowlugar = 0;


/*while ($rowLugar = mysql_fetch_array($rsLugar)) {
    if ($locaId == $rowLugar['locaid']) {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '" selected>' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    } else {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '">' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    }
}*/

$sql = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.orden,
lotes.id AS loteid,
lotes.remate_id,
lotes.conjunto_lote_id,
lotes.tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.codigo,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.provincia,
lotes.localidad,
lotes.lugar,
lotes.video,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
localidades.id,
localidades.nombre AS locanombre,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
remates.id AS remaid,
remates.fecha,
remates.tipo,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre,
videos_lotes.id AS vlid,
videos_lotes.remate_id AS vlrid,
videos_lotes.lote_id AS vllid,
videos_lotes.video AS vlv
FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Left Join videos_lotes ON videos_lotes.lote_id = lotes.id
WHERE
lotes.remate_id =  '$remaid'
" . $sqlwhere . "

" . $consulorder . "

LIMIT $offset, $limit";

//echo $sql;


// Total de registros sin limit
$total = $cuenta;
$totalresto = $cuenta - (($pag-1) * 14);
if($totalresto<14){
$totalcount = $totalresto;
}else{
    $totalcount = 14;
}
//echo "totalllll= ".$total." / ". $totalresto;


$rsCAT = mysql_query($sqlCAT);

$sqlCATTotal = "SELECT FOUND_ROWS() as total";
$rsCATTotal = mysql_query($sqlCATTotal);
$rowCATTotal = mysql_fetch_assoc($rsCATTotal);
// Total de registros sin limit
$totalCAT = $rowCATTotal["total"];


$rsRema = mysql_query($sqlRema);

$resultHomePauta=mysql_query("SELECT
home_pautas.id,
home_pautas.activa,
home_pautas.fecha_desde,
home_pautas.fecha_hasta,
home_pautas.vivolink
FROM
home_pautas
WHERE
home_pautas.activa =  'SI' AND
curdate() BETWEEN  home_pautas.fecha_desde AND home_pautas.fecha_hasta 
ORDER BY
home_pautas.id DESC
LIMIT 0, 1", $link);
$cuentaHP = mysql_num_rows($resultHomePauta);
$forBanncount=0;
while ($rowHP=mysql_fetch_array($resultHomePauta)){
	if ($rowHP['vivolink']) {
        $vivolink = $rowHP['vivolink'];
    }
}
?>
   


<script type="text/javascript">

    function cambiaFondo(pId, pColor) {
        document.getElementById(pId).style.backgroundColor = pColor;
    }
    var divflecha = '';
    function abreCatalogoNew(pDivqueabre, pVideo, pTitulote, pDetalote1, pDetalote2, pDetalote3, pDivflecha, pServ) {
        //document.getElementById('todoDiv').style.zIndex=20;
        innerVideo = pVideo;
        if(pServ=='APP'){
            if(pVideo.indexOf('mp4') !== -1){
            embedVideo = '<div align="center" class="embed-responsive embed-responsive-16by9"><video autoplay loop  controls muted class="embed-responsive-item"><source src="http://rural-ftp.com/ofertas/videos/' + innerVideo + '" type="video/mp4"></video></div>'
        }else{
            embedVideo = '<iframe class="youtube-player" type="text/html" width="100%" height="100%" src="http://www.youtube.com/embed/' + innerVideo + '?rel=0&ps=docs&showinfo=0&controls=1" allowfullscreen frameborder="0"></iframe>';
        }    
        }else{
            embedVideo = '<iframe class="youtube-player" type="text/html" width="100%" height="100%" src="http://www.youtube.com/embed/' + innerVideo + '?rel=0&ps=docs&showinfo=0&controls=1" allowfullscreen frameborder="0"></iframe>';
        }
        if (innerVideo == '') {
            embedVideo = '<img src="images/no-video.jpg" width="500" height="416">';

        }
        if (divflecha != '') {
            jQuery('#' + divflecha).fadeOut(200);
        }
        jQuery('#' + pDivqueabre).fadeOut(200, function () {
            jQuery('#conteVideoylote').css('display', '');
            jQuery('.tdContevideo').html(embedVideo);
            jQuery('#conteTitu').html(pTitulote);
            jQuery('.conteTitu').html(pTitulote);
            jQuery('.detaLote1').html(pDetalote1);
            jQuery('.detaLote2').html(pDetalote2);
            jQuery('.detaLote3').html(pDetalote3);

            jQuery('#' + pDivqueabre).fadeIn(400);
            //jQuery('#' + pDivflecha).fadeIn(400);
            //divflecha = pDivflecha;
        });

        tempAlpha = setInterval('setAlphaUp()', 5);
    }
    var btnloteSel = '';
    function mouseOverout(pDivid, pInst) {
        if (pInst == 'sel') {
            document.getElementById(btnloteSel).style.backgroundColor = "#fefefe";
            btnloteSel = pDivid;
            document.getElementById(pDivid).style.backgroundColor = "#fff0ce";
        } else {
            if (btnloteSel != pDivid) {
                document.getElementById(pDivid).style.backgroundColor = "#fefefe";
            }
        }




    }
    ;

</script>



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>:: UMC S.A. Negocios Agropecuarios ::</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body onload=" abrePrimero();">

<?php
include 'header.php';
?>
<!-- BANNER -->
<?php
    while ($rowRema = mysql_fetch_array($rsRema)) {
        $precioSINO = $rowRema['precios'];

        $botArchivoOff = 'images/bot_desc-cat-off.png';
        $botArchivoOn = 'images/bot_desc-cat-on.png';

        if ($rowRema['archivo']) {
            $comparchivo = 1;
            $descCatalogoDisplay = '';
            $archivoCat = 'repositorio/' . $rowRema['archivo'];
        } else {
            $comparchivo = 2;
            $descCatalogoDisplay = 'none';
            $archivoCat = '';
        }

        if ($rowRema['precios'] == 'SI') {
            $botArchivoOff = 'images/bot_desc-precios-off.png';
            $botArchivoOn = 'images/bot_desc-precios-on.png';
            $archivoCat = 'repositorio/' . $rowRema['archivo'];
        }

        if ($rowRema['zona'] == 'CORR') {
            $colorFont = '#a92000';
            $imgBannerTop = '#3f628a';
            $imgBannerBottom = 'tira_corrientes.png';
        }
        if ($rowRema['zona'] == 'MIS') {
            $colorFont = '#2b6f32';
            $imgBannerTop = '#b22423';
            $imgBannerBottom = 'tira_misiones.png';
        }
        if ($rowRema['zona'] == 'BUE') {
            $colorFont = '#006788';
            $imgBannerTop = '#2b6f32';
            $imgBannerBottom = 'tira_bsas.png';
        }

        if ($rowRema['cantidad']) {
            $remaCanti = number_format($rowRema['cantidad'], 0, ',', '.');
            $displayVacunos = '';
        } else {
            $remaCanti = '<span onclick="location.href=\'umc.php#quieroconsignar\'" style="cursor:pointer;"><consigneBanner>CONSIGNE SU HACIENDA</consigneBanner></span>';
            $displayVacunos = 'none';
        }
?>
        
<?php
    $btnexcel = '<a href="remaplanilla.php?remaid=' . $remaid . '&xlsname=' . utf8_encode($rowRema['titulo']) . '"><img src="images/ico-excel.png" border="0"></a>';
    if ($rowRema['archivo']) {
        if ($rowRema['precios'] == 'SI') {
            $btnpdf = '<a href="repositorio/' . $rowRema['archivo'] . '"><img src="images/icono_pdf_precios.png" border="0"></a>';
        } else {
            $btnpdf = '';
        }
        if ($rowRema['precios'] == 'SI') {
            $textobtnpdf = '<a href="repositorio/' . $rowRema['archivo'] . '" class="txtFiltro">DESCARGAR RESULTADOS</a>';
        } else {
            $textobtnpdf = '';
        }
    } else {
        $btnpdf = '';
    }
?>
        
<div class="container banner mb">
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <?php
                //---------Mapa--------//
                if ($rowRema['mapa_img']) {
                    $mapaDisplay = '';
                    $mapa = 'repositorio/' . $rowRema['mapa_img'];
                    $mapaLink = '<a class="fancybox" rel="groupmapa" href="' . $mapa . '" title="Mapa del lugar del Remate">CÓMO LLEGAR</a>';
                } else {
                    $mapaDisplay = 'none';
                    $mapaLink = '';
                }
                //----------Mapa-------//

                $fechaRem = strtotime($rowRema['fecha']);
                //--------función Hora remate----//
                $remaHoraIni = strtotime($rowRema['hora_inicio']);
                $remaHoraFin = strtotime($rowRema['hora_fin']);
                $hora_actual = strtotime(date("H:i:s"));
                $remaFecha = date('d/m/Y', $fechaRem);
                $fechaHoy = date('d/m/Y');
                $veaRemateDisplay = 'none';
                
                //echo $remaFecha.' / '.$fechaHoy.'<br><br><br>';
                //echo $remaHoraIni.' / '.$remaHoraFin.' - '.$hora_actual.'<br><br><br>';
                if ($remaFecha == $fechaHoy) {
                    //echo $remaFecha.' / '.$fechaHoy;
                    if ($hora_actual >= $remaHoraIni && $hora_actual <= $remaHoraFin) {
                        $veaRemateDisplay = '';
                    } else {
                        $veaRemateDisplay = 'none';
                    }
                }
                if($rowRema['tipo']=='TV'){
            
                $txtTipoRema = 'TELEVISADO <img src="images/logo-rural-banner.png">';

                }else{

                    $txtTipoRema = "F&Iacute;SICO";
                }
            ?>
            <div style="position:relative;">
                <div style="position:relative; z-index:10;">
                    <?php
                        $yacarevisible= str_replace('SI','',$rowRema['yacare_visible']);
                        $yacarevisible= str_replace('NO','none',$yacarevisible);
                        if($rowRema['banner_g_visible']=='SI' && $rowRema['banner_g']){ 
                        $foto='repositorio/'.$rowRema['banner_g'];
                        if (file_exists($foto)) {
                            $size = GetImageSize("$foto"); 
                            $ancho=$size[0]; 
                            $alto=$size[1]; 
                            if($rowRema['banner_g_link']!=''){
                    ?>  
                    <!--<div style="position:absolute; z-index:<?php echo $rowRema['banner_g_z_index'] ?>; left:<?php echo $rowRema['banner_g_pos_x'] ?>px; top:<?php echo $rowRema['banner_g_pos_y'] ?>px; display:<?php echo $rowRema['banner_g_visible'] ?>; width:<?php echo $ancho?>px; height:<?php echo $alto?>px;"><a href="<?php echo $rowRema['banner_g_link'] ?>" target="_blank"><img src="<?php echo $foto ?>" border="0"></a></div>-->  
                    <?php
                            }else{
                    ?>
                    <!--<div style="position:absolute; z-index:<?php echo $rowRema['banner_g_z_index'] ?>; left:<?php echo $rowRema['banner_g_pos_x'] ?>px; top:<?php echo $rowRema['banner_g_pos_y'] ?>px; display:<?php echo $rowRema['banner_g_visible'] ?>; width:<?php echo $ancho?>px; height:<?php echo $alto ?>px;"><img src="<?php echo $foto ?>" border="0"></div>-->  
                    <?php
                            }    
                        }
                    }
                    ?>
                    
                    <div class="banner-borders" style="padding: 0px;">
                        <div class="tirita" style="background-color:<?php echo $imgBannerTop ?>;">
                            <div class="txtRobotobanner" style="position: relative; color: #fff; width:100%; letter-spacing:1pt; font-size: 22pt; padding-left: auto; padding-right: auto; top:2px; z-index:12; border: 0px solid #b1b0b0;font-weight:700;"><fechaRema><?php echo utf8_encode($diasArr[date('w', $fechaRem)]) . ' ' . date('d', $fechaRem) . ' DE ' . utf8_encode($mesesArr[(int) date('m', $fechaRem)    ]) ?> | <?php echo str_replace(':00', '', $rowRema['hora_inicio']) . ' hs' ?></fechaRema></div>
                        </div>
                        <div style="border: solid 0px; position: relative; top:60px;">
                            <div id="bannerNumerema" class="txtRobotobanner"><b><?php echo $rowRema['numero'] ?>&ordm; REMATE <?php echo $txtTipoRema ?></b><span style="color: #5e5e5e; font-weight: normal;"> desde <?php echo $rowRema['lugar'] ?></span></div>
                            <div id="bannerCantivac" class="txtRobotobanner" style="color: <?php echo $colorFont ?>;">
                                <cantiBanner><?php echo $remaCanti ?>  
                                    <!-- mostrar solo si hay cantidad -->
                                    <div class="vacunostext" style="display:<?php echo $displayVacunos ?> ">VACUNOS</div>
                                    <!-- mostrar solo si destaca -->
                                    <div class="detallestext" style="display:<?php echo $displayDestaca ?> "></div> 
                                </cantiBanner>
                            </div>
                            <div id="bannerDetarema" class="txtRobotobanner" style="position: relative; border: solid 0px"><?php echo utf8_encode($rowRema['descripcion']) ?></div>
                        </div>
                        <div class="linkcont" style="position: relative; padding-left: 10px; height: 40px; background-color: #c5c5c5; margin-top: 100px;z-index: 1000;">
                            <?php
                                if($vivolink != 'no'){
                            ?>
                            <div id="bannerRemateenVivo" class="" style="display:<?php echo $veaRemateDisplay ?>;"><a data-fancybox href="https://www.youtube.com/embed/<?php echo $vivolink ?>?rel=0&fs=1&autoplay=1">EN VIVO</a></div>
                            <?php
                                }
                            ?>
                            <div id="bannerDescCat" class="" style="display:<?php echo $descCatalogoDisplay ?>; "><a href="<?php echo $archivoCat ?>" target="_blank">CATÁLOGO</a></div>
                            <div id="bannerMapaM" class="" style="display:<?php echo $mapaDisplay ?>;"><?php echo $mapaLink ?></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
             
<!-- FINAL DEL BANNER -->

<div class="container buscador mb">
    <div class="row">
        <div class="col-xs-12">
            <?php
                if ($rowRema['video_final_1']) {
            ?>
            <div class="txtTituremates">
                <b>VIDEOS</b>
                <span style="color: #7a7a7a;"> DE LA TRANSMISI&Oacute;N</span>
                <div class="col-xs-12 text-center" style="border-bottom: 1px solid #e1e1e1;">                                                   
                    <?php
                        if ($rowRema['video_final_1']) {
                    ?>
                                                        
                    <div  class="txtVideolink"><a class="vimeo" title="<?php echo utf8_encode($rowRema['video_titu_1']) ?>" href="http://player.vimeo.com/video/<?php echo $rowRema['video_final_1'] ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=d8d453"><span class="txtVideolink"><b>VIDEO 1: </b><?php echo utf8_encode($rowRema['video_titu_1']) ?></span></a></div>
                                                                
                    <?php
                        }
                        if ($rowRema['video_final_2']) {
                    ?>
                                                        
                    <div class="txtVideolink"><a class="vimeo" title="<?php echo utf8_encode($rowRema['video_titu_2']) ?>" href="http://player.vimeo.com/video/<?php echo $rowRema['video_final_2'] ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=d8d453"><span class="txtVideolink"><b>VIDEO 2: </b><?php echo utf8_encode($rowRema['video_titu_2']) ?></span></a></div>
                            
                    <?php
                        }
                        if ($rowRema['video_final_3']) {
                    ?>
                                                        
                    <div class="txtVideolink"><a class="vimeo" title="<?php echo utf8_encode($rowRema['video_titu_3']) ?>" href="http://player.vimeo.com/video/<?php echo $rowRema['video_final_3'] ?>?title=0&amp;byline=0&amp;portrait1=0&amp;color=d8d453"><span class="txtVideolink"><b>VIDEO 3: </b><?php echo utf8_encode($rowRema['video_titu_3']) ?></span></a></div>

                    <?php
                        }
                        if ($rowRema['video_final_4']) {
                    ?>
                                                        
                    <div  class="txtVideolink"><a class="vimeo" title="<?php echo utf8_encode($rowRema['video_titu_4']) ?>" href="http://player.vimeo.com/video/<?php echo $rowRema['video_final_4'] ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=d8d453"><span class="txtVideolink"><b>VIDEO 4: </b><?php echo utf8_encode($rowRema['video_titu_4']) ?></span></a></div>
                
                    <?php
                        }
                        if ($rowRema['video_final_5']) {
                    ?>
                                                        
                    <div  class="txtVideolink"><a class="vimeo" title="<?php echo utf8_encode($rowRema['video_titu_5']) ?>" href="http://player.vimeo.com/video/<?php echo $rowRema['video_final_5'] ?>?title=0&amp;byline=0&amp;portrait=0&amp;color=d8d453"><span class="txtVideolink"><b>VIDEO 5: </b><?php echo utf8_encode($rowRema['video_titu_5']) ?></span></a></div>
                
                    <?php
                    }
                    ?>        
                </div>
            </div>
        </div>
        
    </div>
</div>



<!-- BARRA DE BUSQUEDA -->
<div class="container mb">
    <div class="row">
        <div class="col-xs-12">
            <span class="txtTituremates" style="border: none;"><b>LOTES</b><span style="color: #7a7a7a;"> A LA VENTA</span></span>
            
        </div>
        <div class="col-xs-12 hidden">
            <b>FILTROS DE B&Uacute;SQUEDA</b>                                
        </div>
        <div class="col-xs-12 filtros">
            <b>UBICACI&Oacute;N DEL LOTE</b>
            <div id="comboLocalidades">
                <select id="lotesLoca" class="select" name="lotesLoca" onchange="cambiaPagFiltro('loca', this.value, '<?php echo $locaId ?>');"
                        <option value="5" <?php echo $locaselected ?>>TODOS</option>
                            <?php echo '<option value="" ' . $locaselected . '>TODOS</option>' . $lugarcombo; ?>
                </select>
            </div>
            <div id="">
                <b>LOTE</b>
                <select id="lotesSel" class="select" name="categorias" onchange="cambiaPagFiltro('lotenum', this.value, '<?php echo $lotesel ?>');">
                    <?php echo "VER" . $innerselLotes ?>
                </select>
            </div>
            <div id="">
                <b>CATEG.</b>
                <select class="select" name="categorias" onchange="cambiaPagFiltro('cate', this.value, '<?php echo $cate ?>', '');">
                    <?php
                    $cuentarowscate = 0;
                    if ($totalCAT > 0) {
                        while ($rowCAT = mysql_fetch_array($rsCAT)) {
                            $cuentarowscate+=1;
                            if ($cuentarowscate == 1) {
                                if ($cate == "") {
                                    ?>
                                    <option value="" selected>TODOS</option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="">TODOS</option>
                                    <?php
                                }
                            }

                            if ($cate == $rowCAT['cateidselect']) {
                                ?>
                                <option value="<?php echo $rowCAT['cateidselect'] ?>" selected ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $rowCAT['cateidselect'] ?>" ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                <?php
                            }
                        }
                    } else {
                        ?>
                        <option value="" selected>TODOS</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>TRAZ.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('tr', this.value, '<?php echo $traza ?>', '');">
                    <?php
                    if ($traza == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($traza == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($traza == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>GARPTA.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('gr', this.value, '<?php echo $garpta ?>', '');">
                    <?php
                    if ($garpta == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($garpta == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($garpta == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>MIO-MIO.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('mm', this.value, '<?php echo $miomio ?>', '');">
                    <?php
                    if ($miomio == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($miomio == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($miomio == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
<?php
    }                            
    }
?>


      <!-- HEADER CON LINKS VIDEOS -->

























<!-- CONTENIDO CUADRITOS -->
<div class="container">
    <div class="row">
        <?php
        $cuentainicial = ($pag-1) * 14;
        $cuentatres = 0;
        $cuentacatorce= 0;
        $cuentatotal = 0;
        $videoserv="YT";
        // echo 'VERRRRRRRRRRRR'.$totalcount;
        foreach($lotesrural as $item){ 
            
            if(strpos($item->video, '.mp4') !== false){
              $videoserv="APP";  
            }
            $id = $item->id;
            $lid = $item->id;
            $name = $item->nro_lote;
            $cuentatres+=1;
            $cuentatotal+=1;
            
            if($cuentatotal > $cuentainicial){
            $cuentacatorce+=1;    
            $detalote1 = '<b>Cantidad: <b>' . $item->cantidad . '</b><br>Tipo: <b>' . $item->raza . '</b><br>Categor&iacute;a: <b>' . $item->categoria . '</b><br>Peso.: <b>' . $item->peso . ' Kgs.</b><br>Localidad: <b>' . $item->localidad_texto;
            $detalote2 = '</b>Provincia: <b>' . $item->dpto . '</b><br>Trazabilidad: <b></b><br>Garrapata: <b></b><br>MIO-MIO: <b></b>';
            $detalle= str_replace (array("\r\n", "\n", "\r"), ' ', $item->detalle);
            $detalote3 = 'Caracter&iacute;sticas: <b>' . $detalle . '</b>';



            $orden = $item->nro_lote;
            $displayCode = 'none';
            $displayOrden = '';
            $titulo= str_replace('\n','',$item->titulo);
            $tituLote = 'LOTE Nº ' . $orden . ' | ' . $titulo;
            $varURLYT = str_replace("https://www.youtube.com/watch?v=", "", $item->video);
            
            if ($cuentacatorce== 1) {
                $fdobtnlote = '#fff0ce';
        ?>

        <div class="col-xs-6 col-sm-3 primero">
            <table cellpadding="0" cellspacing="0" border="0" style="width:250px; height: 1px; margin: 0 auto;">
                    <script type="text/javascript">
                        function abrePrimero() {
                            abreCatalogoNew('catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>', '<?php echo $detalote3 ?>', 'divflecha<?php echo $item->id ?>','<?php echo $videoserv ?>');
                            btnloteSel = 'divlote<?php echo $lid ?>';
                        }
                    </script>
                    <?php
                        } else {
                            $fdobtnlote = '#fefefe';
                        }
                    ?>
                <tr>
                        <?php
                    if($cuentacatorce > 7){
                        ?>
                        <!-- <td style="width: 16px;">
                            <div id="divflecha<?php echo $item->id ?>" style="display:none; width: 16px;"><img src="images/flecha-lote-der.jpg" id="imgflecha"></div>
                        </td> -->
                        <?php
                    }
                    ?>
                    <td width="250" height="80" border="0" style="cursor:pointer; background-color: #fff;" onclick="abreCatalogoNew('catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>', '<?php echo $detalote3 ?>', 'divflecha<?php echo $item->id ?>','<?php echo $videoserv ?>');">
                        <div id="divlote<?php echo $lid ?>" style="border:1px solid #e4e4e4;width:250px; height: 80px; background-color:<?php echo $fdobtnlote ?>" onmouseover="mouseOverout(this.id, 'on')" onmouseout="mouseOverout(this.id, 'off')" onclick="mouseOverout(this.id, 'sel')">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                <tr>
                                    <td width="1"></td>
                                    <td width="100" valign="top">
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 1px;">
                                            <tr style="display:<?php echo $displayCode ?>;">
                                                <td class="txtCodilote" height="1" valign="top" style="display:<?php echo $displayCode ?>;">
                                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 1px;">
                                                        <tr>
                                                            <td height="8"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2" style="font-size:11pt; color: #383838;">Certif.</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txtCodilote" valign="top" height="35" style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff; font-size:17pt;"><?php echo $item->cert ?></td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="display:<?php echo $displayOrden ?>;">
                                                <td height="1" valign="middle">
                                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                                        <tr>
                                                            <td height="8"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2" style="font-size:11pt; color: #383838;">LOTE</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txtNumelote" style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;" height="1" valign="middle"><?php echo $orden ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2">CERT: <?php echo $item->cert ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="1"></td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                            <tr>
                                                <td height="6"></td>
                                            </tr>
                                            <tr>
                                                <td height="1" class="txtTextolote"><?php echo $item->cantidad ?> cabezas</td>
                                            </tr>
                                            <?php
                                            
                                            $pesolote = '<br>' . $item->peso . ' Kgs.';
                                            //echo " totalcount= ".$totalresto."/".$cuenta;
                                            ?>
                                            <tr>
                                                <td class="txtLotecate" valign="top"><?php echo $item->categoria ?><?php echo $pesolote ?><br><?php echo $item->localidad_texto ?>, <?php echo $item->dpto ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div type="button" id="divlote<?php echo $lid ?>" class="btn btn-primary visible-xs triggerv" data-toggle="modal" data-target="#verVid" onclick="mouseOverout(this.id, 'sel')"></div>
                         
                        <!-- <div id="divi<?php echo $lid ?>" style="width: 100%;position: absolute;left: 0;">
                            <div id="catalogDiv" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                                <div id="conteVideoylote" style="display:;">
                                    <?php include 'videoinclude_n.php' ?>
                                </div>
                            </div>
                        </div> -->
                    </td>





                    <?php
                    if($cuentacatorce <= 7){
                    ?>
                    <!-- <td style="width: 16px;" style="border: solid 0px;">
                        <div id="divflecha" style="display: none;">
                            <img src="images/flecha-lote.jpg" id="imgflecha">
                        </div>
                    </td> -->
                    <?php
                    }
                    ?>
                </tr>

                <?php
                if(($cuentacatorce<7) || ($cuentacatorce>7 and ($cuentacatorce<14|| $cuentacatorce< $totalcount))){
                    ?>
                <tr>
                    <td style="height: 6px;"></td>
                </tr>
                <?php
                }
                $midLotes=7;
                if ($totalcount<7){

                    $midLotes=$totalcount;
                }
                if($cuentacatorce == $midLotes || $cuentacatorce == $totalcount){
                ?>

            </table>
        </div>


        <?php
        if($cuentacatorce == $midLotes){
        ?>

        
        <div class="hidden-xs col-sm-6 segundo" style="border: solid 0px;">
            <div id="catalogDiv" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                <div id="conteVideoylote" style="display:;">
                    <?php include 'videoinclude_n.php' ?>
                </div>
            </div>
        </div>


        <div class="col-xs-6 col-sm-3 tercero">
            <table cellpadding="0" cellspacing="0" style="width: 250px; height: 100%; margin: 0 auto;">
                <?php
                    }
                }
                //echo "VERRR:".$totalresto."/".$cuentatotal;
                if ($cuentatotal == ($cuentainicial + 14) || ($cuentatotal==$totalresto)){
                    
                ?>

            </table>
        </div>
        <?php
        break;
        }
        }
        }
        ?>



    </div>
</div>
<!-- FIN LOTES -->












<div class="modal fade product_view" id="verVid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                <h3 class="modal-title conteTitu"></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 product_img">

                            <div id="catalogDiv2" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                                <div class="conteVideoylote2" style="display:;">
                                    <?php include 'videoinclude_n.php' ?>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>













                                                        <!-- PAGINADO -->

                                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 20px;">
                                                                    <tr>
                                                                        <?php
                                                                        $totalPag = ceil($total / $limit);
                                                                        ?>
                                                                    <script type="text/javascript">

                                                                        function utf8_encode(argString) {
                                                                            // Encodes an ISO-8859-1 string to UTF-8
                                                                            //
                                                                            // version: 1109.2015
                                                                            // discuss at: http://phpjs.org/functions/utf8_encode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // +   improved by: sowberry
                                                                            // +    tweaked by: Jack
                                                                            // +   bugfixed by: Onno Marsman    // +   improved by: Yves Sucaet
                                                                            // +   bugfixed by: Onno Marsman
                                                                            // +   bugfixed by: Ulrich
                                                                            // +   bugfixed by: Rafal Kukawski
                                                                            // *     example 1: utf8_encode('Kevin van Zonneveld');    // *     returns 1: 'Kevin van Zonneveld'
                                                                            if (argString === null || typeof argString === "undefined") {
                                                                                return "";
                                                                            }
                                                                            var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
                                                                            var utftext = "",
                                                                                    start, end, stringl = 0;

                                                                            start = end = 0;
                                                                            stringl = string.length;
                                                                            for (var n = 0; n < stringl; n++) {
                                                                                var c1 = string.charCodeAt(n);
                                                                                var enc = null;
                                                                                if (c1 < 128) {
                                                                                    end++;
                                                                                } else if (c1 > 127 && c1 < 2048) {
                                                                                    enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
                                                                                } else {
                                                                                    enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
                                                                                }
                                                                                if (enc !== null) {
                                                                                    if (end > start) {
                                                                                        utftext += string.slice(start, end);
                                                                                    }
                                                                                    utftext += enc;
                                                                                    start = end = n + 1;
                                                                                }
                                                                            }
                                                                            if (end > start) {
                                                                                utftext += string.slice(start, stringl);
                                                                            }
                                                                            return utftext;
                                                                        }


                                                                        function utf8_decode(str_data) {
                                                                            // Converts a UTF-8 encoded string to ISO-8859-1
                                                                            //
                                                                            // version: 1109.2015
                                                                            // discuss at: http://phpjs.org/functions/utf8_decode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                            // +      input by: Aman Gupta
                                                                            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // +   improved by: Norman "zEh" Fuchs
                                                                            // +   bugfixed by: hitwork    // +   bugfixed by: Onno Marsman
                                                                            // +      input by: Brett Zamir (http://brett-zamir.me)
                                                                            // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // *     example 1: utf8_decode('Kevin van Zonneveld');
                                                                            // *     returns 1: 'Kevin van Zonneveld'
                                                                            var tmp_arr = [],
                                                                                    i = 0,
                                                                                    ac = 0,
                                                                                    c1 = 0,
                                                                                    c2 = 0, c3 = 0;

                                                                            str_data += '';

                                                                            while (i < str_data.length) {
                                                                                c1 = str_data.charCodeAt(i);
                                                                                if (c1 < 128) {
                                                                                    tmp_arr[ac++] = String.fromCharCode(c1);
                                                                                    i++;
                                                                                } else if (c1 > 191 && c1 < 224) {
                                                                                    c2 = str_data.charCodeAt(i + 1);
                                                                                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                                                                                    i += 2;
                                                                                } else {
                                                                                    c2 = str_data.charCodeAt(i + 1);
                                                                                    c3 = str_data.charCodeAt(i + 2);
                                                                                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                                                                                    i += 3;
                                                                                }
                                                                            }
                                                                            return tmp_arr.join('');
                                                                        }


                                                                        function urlencode(str) {
                                                                            str = escape(str);
                                                                            str = str.replace('+', '%2B');
                                                                            str = str.replace('%20', '+');
                                                                            str = str.replace('*', '%2A');
                                                                            str = str.replace('/', '%2F');
                                                                            str = str.replace('@', '%40');
                                                                            return str;
                                                                        }

                                                                        function urldecode(str) {
                                                                            str = str.replace('+', ' ');
                                                                            str = unescape(str);
                                                                            return str;
                                                                        }

                                                                        pagSig = <?php echo $pag ?> + 1
                                                                        pagAnt = <?php echo $pag ?> - 1
                                                                        function cambiaPag(pPag) {
                                                                            if (pPag > <?php echo $totalPag ?> || pPag < 1) {

                                                                            } else {
                                                                                url = '<?php echo $miURL ?>';
                                                                                url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=' + pPag;
                                                                                window.location = url;

                                                                            }

                                                                        }



                                                                        function cambiaPagFiltro(pPar, pFiltro, pVar, pRemaid) {
                                                                            if (pPar == 'lotenum') {
                                                                                if (pFiltro == '') {
                                                                                    url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid;
                                                                                } else {
                                                                                    url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid + '&lotenum=' + pFiltro;
                                                                                }
                                                                            } else {
                                                                                url = '<?php echo $miURL ?>';
                                                                                url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=1';
                                                                                url = url.replace('&lotenum=<?php echo $lotesel ?>', '');
                                                                                if (pFiltro == '') {
                                                                                    url = url.replace('&' + pPar + '=' + pVar, '');
                                                                                } else {
                                                                                    url = url.replace('&' + pPar + '=' + pVar, '') + '&' + pPar + '=' + pFiltro;
                                                                                }
                                                                            }

                                                                            window.location = url;
                                                                        }
                                                                        function enviar_formulario() {
                                                                            document.selUbic.submit();
                                                                        }

                                                                    </script>
                                                                    <td colspan="3" bgcolor="#ffffff">
                                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" height="20">
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td width="1"><img src="images/ico-pag-ret.jpg" style="cursor:pointer;" onclick="cambiaPag(pagAnt)"></td>
                                                                                <td width="10">&nbsp;</td>
                                                                                <td width="1" class="txtNumepagiOff">&nbsp;&nbsp;</td>
                                                                                <?php
                                                                                $links = array();
                                                                                for ($i = 1; $i <= $totalPag; $i++) {
                                                                                    if ($pag == $i) {
                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagi\" valign=\"middle\" bgcolor=\"#b44422;\">" . $i . "</td>";
                                                                                    } else {
                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagiOff\" valign=\"middle\" style=\"cursor:pointer;\" onclick=\"cambiaPag(" . $i . ")\"><a href=\"javascript:cambiaPag(" . $i . ")\">" . $i . "</a></td>";
                                                                                    }
                                                                                }
                                                                                echo implode("<td width=\"1\"></td>", $links);
                                                                                ?>
                                                                                <td width="10">&nbsp;</td>
                                                                                <td width="1"><img src="images/ico-pag-av.jpg" style="cursor:pointer;" onclick="cambiaPag(pagSig)"></td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!-- FIN PAGINADO -->
                                                    </table>




                    </div>
                    <!--fin contenido remates-->



<div style="height: 60px;"></div>
<div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
        <?php include 'footer.php'?>
    </div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>
