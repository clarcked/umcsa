"use strict";
import config from "../config.json";
import Preofertar from "./preofertar";

export default function Preofertas(props) {
  const [preofertas, setPreofertas] = React.useState();
  const [pautas, setPautas] = React.useState();
  const [status, setStatus] = React.useState();
  const [FORMAT] = React.useState("DD/MM/YYYY HH:mm");
  const getPreofertas = () => {
    let { remate_id, id } = JSON.parse(sessionStorage.getItem("umc-data-lote"));
    let query = {
      limit: "3",
      order: "valor,desc",
      fields: "remate_id,lote_id",
      val: `${remate_id},${id}`,
    };
    let url = new URL(
      `${config.api.baseUrl}${config.api.endpoints.preofertas}`
    );
    for (let k in query) url.searchParams.append(k, query[k]);

    setStatus("loading");
    fetch(url, {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
    })
      .then(
        (res) =>
          res.json().then((data) => {
            setStatus("loaded");
            setPreofertas(data);
          }),
        (err) => {
          setStatus("error");
          console.log(err, "rejected");
        }
      )
      .catch((err) => setStatus("error"));
  };
  const getPautas = () => {
    let { remate_id, id } = JSON.parse(sessionStorage.getItem("umc-data-lote"));
    let query = {
      filter: "one",
      fields: "remate_id",
      val: `${remate_id}`,
    };
    let url = new URL(`${config.api.baseUrl}${config.api.endpoints.pautas}`);
    for (let k in query) url.searchParams.append(k, query[k]);

    setStatus("loading");
    fetch(url, {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
    })
      .then(
        (res) =>
          res.json().then((data) => {
            setStatus("loaded");
            setPautas(data);
          }),
        (err) => {
          setStatus("error");
          console.log(err, "rejected");
        }
      )
      .catch((err) => setStatus("error"));
  };

  const init = () => {
    setStatus("loading");
    setTimeout(() => {
      getPreofertas();
      getPautas();
    }, 2000);
  };

  React.useEffect(() => {
    init();
    let targets = document.querySelectorAll("td");
    targets?.forEach((t) => t.addEventListener("click", () => init()));
    return () => {
      window.removeEventListener("click");
      let targets = document.querySelectorAll("td");
      targets?.forEach((t) => t.removeEventListener("click"));
    };
  }, []);
  // if (pautas) console.log(pautas);
  // if (preofertas) console.log(preofertas);
  if (status == "loading") {
    return <div className="loader">verificando...</div>;
  } else {
    if (pautas) {
      return (
        <>
          <div className="last-preofertas">
            <div className="title">Ultimas preofertas</div>
            <div className="items">
              {Array.isArray(preofertas) && preofertas?.length > 0 ? (
                preofertas?.map(({ fecha_y_hora, valor }, k) => (
                  <div key={k} className="item">
                    <div className="icon">
                      <i className="ico-price-ribbon"></i>
                    </div>
                    <div className="details">
                      <div className="preoferta-date">
                        {moment(fecha_y_hora?.date).format(FORMAT)}
                      </div>
                      <div className="preoferta-value">$ {valor}</div>
                    </div>
                  </div>
                ))
              ) : (
                <div className="notes">
                  <div> Aprovechá! Hace tu preoferta ahora!!!</div>
                </div>
              )}
            </div>
            <div className="dates">
              <div className="opening-date">
                Apertura: {moment(pautas?.fecha_desde?.date).format(FORMAT)}
              </div>
              <div className="closing-date">
                Cierre: {moment(pautas?.fecha_hasta?.date).format(FORMAT)}
              </div>
            </div>
            <div className="notes">{pautas?.leyenda_espacial}</div>
          </div>
          <Preofertar
            pautas={pautas}
            preofertas={preofertas}
            format={FORMAT}
            status={status}
            lote={JSON.parse(sessionStorage.getItem("umc-data-lote"))}
          />
        </>
      );
    } else {
      return <div></div>;
    }
  }
}
