"use strict";

export default function Preofertar(props) {
  const { pautas, preofertas, status, lote } = props;
  const [valor, setValor] = React.useState(pautas?.valor_suma);
  const [valor_pautas] = React.useState(pautas?.valor_suma);
  const [ultima_preo] = React.useState(preofertas[0]);
  const [valor_ultima_preo] = React.useState(preofertas[0]?.valor);
  const increment = () => {
    setValor(() => valor + valor_pautas);
  };
  const decrement = () => {
    let prox_valor = valor - valor_pautas;
    if (prox_valor < valor_ultima_preo || prox_valor === 0)
      return alert("¡Por favor aumenta el valor para seguir la preoferta!");
    setValor(() => prox_valor);
  };
  const save = () => {
    if (valor <= valor_ultima_preo || valor === 0)
      return alert("¡Por favor aumenta el valor para seguir la preoferta!");
  };
  React.useEffect(() => {
    if (valor_ultima_preo) setValor(valor_ultima_preo);
  }, []);
  if (pautas) console.log(pautas, "pautas");
  if (preofertas) console.log(preofertas, "preofertas");
  return (
    <div className="preofertar">
      <div className="controls">
        <div className="action-preofertar">
          <button type="button" onClick={save}>
            Preofertar
          </button>
        </div>
        <div className="control">
          <div className="left">
            <button type="button" onClick={decrement}>
              <i className="ico-minus"></i>
            </button>
          </div>
          <div className="valor">{valor}</div>
          <div className="right">
            <button type="button" onClick={increment}>
              <i className="ico-plus"></i>
            </button>
          </div>
        </div>
      </div>
      {ultima_preo?.user?.id === lote?.user && (
        <div className="notes">
          <div className="icon">
            <i className="ico-smile"></i>
          </div>
          <div className="note">¡Su oferta es la más alta!</div>
        </div>
      )}
    </div>
  );
}
