"use strict";

import Preofertas from "./preofertas";

class Index extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="preofertas-container">
        <Preofertas />
      </div>
    );
  }
}

let domContainer = document.querySelector("#preofertas");
let domContainerModal = document.querySelector("#preofertas-modal");
if (domContainer) {
  const root = ReactDOM.createRoot(domContainer);
  root.render(<Index />);
}
if (domContainerModal) {
  const rootModal = ReactDOM.createRoot(domContainerModal);
  rootModal.render(<Index />);
}
