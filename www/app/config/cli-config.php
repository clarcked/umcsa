<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Umc\App\Services\ORM\DB;

require_once 'bootstrap.php'; 

return ConsoleRunner::createHelperSet(DB::Manager());