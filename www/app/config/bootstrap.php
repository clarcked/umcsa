<?php

define("__APPROOT__", __DIR__ . "/../..");
define("__APPDIR__", __DIR__ . "/..");
define("__TEMPLATEDIR__", __APPDIR__ . "/src/views");
define("__CONFIGDIR__", __DIR__);

require_once __CONFIGDIR__ . "/env.php";
require_once __APPDIR__ . "/vendor/autoload.php";
require_once __APPDIR__ . "/src/services/helpers.php";
require_once __APPDIR__ . "/src/services/orm/db.php";

date_default_timezone_set("America/Argentina/Buenos_Aires");
session_start();