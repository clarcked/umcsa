<?php

define("__DBHOST__", "localhost");
define("__DBPORT__", "3366");
define("__DBUSER__", "");
define("__DBPASSWORD__", "");
define("__DBNAME__", "");

// define("__MAILHOST__", "smtp.mailtrap.io");
// define("__MAILPORT__", "2525");
// define("__MAILUSERNAME__", "");
// define("__MAILPASSWORD__", "");

define("__SENDGRID_APIKEY__", "");

define("__DEFAULTEMAIL__", "");
define("__NOREPLYEMAIL__", "");
define("__SUPPORTMAIL__", "");
define("__ADMINMAIL__", "");

define("__SALT__", '!kQm*fF3pXe1Kbm%9');
define("__COST__", 14);
define("__DEVMODE__", true);
define("__SITEBASE__","/");
