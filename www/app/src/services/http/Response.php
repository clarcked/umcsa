<?php

namespace Umc\App\Services\Http;

class Response
{
    public static function redirect404()
    {
        header("Location: /error404.php");
    }

    public static function redirect($location)
    {
        header("Location: $location");
    }
}
