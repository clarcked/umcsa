<?php

namespace Umc\App\Services\Http;

class Request
{

    private $_get = [];
    private $_post = [];
    private $_session = [];

    public function __construct()
    {
        $this->_get = isset($_GET) ? $_GET : [];
        $this->_post = isset($_POST) ? $_POST : [];
        $this->_session = isset($_SESSION) ? $_SESSION : [];
    }

    private function sanitize($arg)
    {
        if (is_string($arg)) return strip_tags(stripslashes($arg));
        return $arg;
    }
    public function isMethod($arg)
    {
        return strtolower($this->method()) == strtolower($arg);
    }
    public function method()
    {
        return $_SERVER["REQUEST_METHOD"];
    }
    public function agent()
    {
        return isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "";
    }

    public function host()
    {
        return isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : "/";
    }

    public function uri()
    {
        return isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : "/";
    }

    public function referer($fallback = "/")
    {
        return isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : $fallback;
    }

    public function sessions()
    {
        return isset($this->_session) ? $this->_session : [];
    }

    public function session($key,$fallback=[])
    {
        return isset($this->_session[$key]) ? $this->sanitize($this->_session[$key]) : $fallback;
    }

    public function body()
    {
        $content = file_get_contents("php://input");
        return json_decode($content, true);
    }

    public function params()
    {
        return isset($this->_post) ?  $this->_post : [];
    }

    public function param($key,$fallback=[])
    {
        return isset($this->_post[$key]) ? $this->sanitize($this->_post[$key]) : $fallback;
    }

    public function queries()
    {
        return isset($this->_get) ? $this->_get : [];
    }

    public function query($key, $fallback=[])
    {
        return isset($this->_get[$key]) ? $this->sanitize($this->_get[$key]) : $fallback;
    }
}
