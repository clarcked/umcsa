<?php

namespace Umc\App\Services\Http;

/**
 *  - /?filter=[one,all]&fields=[key1,key2]&val=[val1,val2]&join=[table,left]&limit=[10]&offset=[10]&mode=[escalar,normal]&order=[key1,asc]
 */
class RequestFilter
{
    const MAX_LIMIT = 100;
    private $request;
    private $allowed_filters = ["one" => "find", "all" => "fetch"];

    public function __construct()
    {
        $this->request = new Request();
    }

    public function isFiltered()
    {
        return $this->request->query("filter", false);
    }

    public function hasJoin()
    {
        return $this->request->query("join", false);
    }

    public function hasProperties()
    {
        return $this->request->query("fields", false);
    }

    public function hasOrder()
    {
        return $this->request->query("order", false);
    }

    public function getJoinTable()
    {
        $join = explode(",", $this->request->query("join", ""));
        return $join[0];
    }

    public function getJoinType()
    {
        $join = explode(",", $this->request->query("join", ""));
        return isset($join[1]) ? $join[1] : "left";
    }

    public function getFilterType()
    {
        $filter = $this->request->query("filter", "all");
        return array_key_exists($filter, $this->allowed_filters) ? $this->allowed_filters[$filter] : $this->allowed_filters["all"];
    }

    public function getProperties()
    {
        $properties = [];
        if ($this->hasProperties()) {
            $fields = explode(",", $this->request->query("fields", ""));
            $values = explode(",", $this->request->query("val", ""));
            foreach ($fields as $k => $field) $properties = array_merge($properties, [$field => $values[$k]]);
        }
        return $properties;
    }

    public function getLimit()
    {
        return $this->request->query("limit", self::MAX_LIMIT);
    }

    public function getOffset()
    {
        return $this->request->query("offset", 0);
    }

    public function getResponseType()
    {
        return "normal";
    }

    public function getOrder()
    {
        $orderBy = [];
        if ($this->hasOrder()) {
            $orderBy = explode(",", $this->request->query("order", ""));
            if (count($orderBy) == 2) {
                $orderBy = [$orderBy[0] => $orderBy[1]];
            } else {
                $orderBy = [];
            }
        }
        return $orderBy;
    }

}