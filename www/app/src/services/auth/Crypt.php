<?php
namespace Umc\App\Services\Auth;

class Crypt
{
    private $options = ['cost' => __COST__];

    public function hash($plain)
    {
        return \password_hash($plain, PASSWORD_BCRYPT, $this->options);
    }

    public function check($plain, $hash)
    {
        return \password_verify($plain, $hash);
    }
}
