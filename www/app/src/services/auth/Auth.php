<?php

namespace Umc\App\Services\Auth;

use Umc\App\Model\Entity\User;
use Umc\App\Services\ORM\DB;

class Auth
{
    private $db;
    public function __construct()
    {
        $this->db = DB::Manager();
    }

    /**
     * @return User|null
     */
    public function authenticate($token)
    {
        $expected = ["email", "password"];
        $missing = array_diff($expected, array_keys($token));
        if ($missing) return null;

        /** @var User */
        $user = $this->db->getRepository("Umc\App\Model\Entity\User")
            ->findOneBy([
                "email" => $token["email"],
                "activo" => "SI"
            ]);
        if ($user && ($user->getPassword() == $token["password"])) {
            //TODO password encryption
            return $this->logging($user); 
        }
        return null;
    }



    public function user($key = "id")
    {
        if (isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
            if ($user instanceof User) {
                if (method_exists($user, "get" . ucfirst($key))) {
                    return call_user_func([$user, "get" . ucfirst($key)]);
                }
            }
        }
        return  null;
    }


    public function islogged()
    {
        return isset($_SESSION['user']) && ($_SESSION["user"] instanceof User);
    }

    public function logging($arg)
    {
        if ($arg instanceof User) {
            $_SESSION["user"] = $arg;
            return $arg;
        }
        return null;
    }

    public function logout($arg)
    {
        if (isset($_SESSION["user"])) {
            unset($_SESSION["user"]);
        }
    }
}
