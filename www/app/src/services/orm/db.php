<?php
namespace Umc\App\Services\ORM;

use Doctrine\ORM\EntityManager; 
use Doctrine\ORM\Tools\Setup; 


class DB{
    /**
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    public static function Manager(){
        $config = Setup::createYAMLMetadataConfiguration(array(__APPDIR__ . "/config/doctrine"), __DEVMODE__);
        $conn = [
            'driver'   => 'pdo_mysql',
            'host' => __DBHOST__,
            'user'     => __DBUSER__,
            'password' => __DBPASSWORD__,
            'dbname'   => __DBNAME__,
        ]; 
        return EntityManager::create($conn, $config);
    }
}