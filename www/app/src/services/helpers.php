<?php

function __($arg)
{
    echo $arg;
}

function pr($arg)
{
    echo "<pre>";
    if ($arg) print_r($arg);
    echo "</pre>";
}

function curl_get($url, array $get = NULL, array $options = array())
{
    $defaults = array(
        CURLOPT_URL => $url . (strpos($url, '?') === FALSE ? '?' : '') . http_build_query($get),
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 4
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if (!$result = curl_exec($ch)) {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function __path($path)
{
    return __APPDIR__ . $path;
}

function __link($path)
{
    return __SITEBASE__ ? __SITEBASE__ . $path: $path;
}
