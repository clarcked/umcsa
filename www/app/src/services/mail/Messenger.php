<?php

namespace Umc\App\Services\Mail;


class Messenger
{
    public static function nuevoUsuario($mailer, $user)
    {
        return $mailer->send(
            "Nueva solicitud de alta de usuario.",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            "new-user.php",
            [
                "user" => $user,
            ]
        );
    }
    public static function nuevaOferta($mailer, $oferta, $auth)
    {
        return $mailer->send(
            "Nueva oferta registrada!",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            "new-oferta.php",
            [
                "preoferta" => $oferta,
                "auth" => $auth,
            ]
        );
    }
    public static function ofertaGanadora($mailer, $oferta, $auth)
    {
        return $mailer->send(
            "Su oferta es la ganadora!",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            // $auth->user("email"),
            "preoferta-winner.php",
            [
                "preoferta" => $oferta,
                "auth" => $auth,
            ]
        );
    }
    public static function ofertaSuperada($mailer, $oferta, $user)
    {
        return $mailer->send(
            "Su oferta ha sido superada!",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            // $user->getEmail(),
            "preoferta-loser.php",
            [
                "preoferta" => $oferta,
                "user" => $user,
            ]
        );
    }
    public static function dadoDeAltaUsuario($mailer, $user)
    {
        return $mailer->send(
            "Su usuario ha sido dado de alta.",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            // $user->getEmail(),
            "registration.php",
            [
                "user" => $user,
            ]
        );
    }
    public static function blanqueoClave($mailer, $user, $token)
    {
        return $mailer->send(
            "Solicitud de blanqueo de clave.",
            __NOREPLYEMAIL__,
            __ADMINMAIL__,
            // $user->getEmail(),
            "recover.php",
            [
                "user" => $user,
                "token" => $token
            ]
        );
    }
}
