<?php

namespace Umc\App\Services\Mail;


class Mailer
{

    private $_mailer;

    public function __construct()
    {
        try {
            $this->_mailer = new \SendGrid(__SENDGRID_APIKEY__);
        } catch (\Exception $th) {
            die($th->getMessage());
        }
    }

    public function getTemplate($file, $vars)
    {
        $path = __TEMPLATEDIR__ . "/emails/$file";
        extract($vars);
        if (!file_exists($path)) return false;
        ob_start();
        include_once($path);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function send($subject, $from, $to, $template, $vars)
    {
        try {
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($from, "UMC S.A");
            $email->setSubject($subject);
            $email->addTo($to);
            $email->addContent('text/html', $this->getTemplate($template, $vars));
            $response = $this->_mailer->send($email);
            // __(var_dump($response));
            return $response;
        } catch (\Exception $th) {
            die($th->getMessage());
        }
        return false;
    }
}
