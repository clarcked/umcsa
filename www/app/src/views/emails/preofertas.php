<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>UMCSA</title>
    <style>
      * {
        box-sizing: border-box;
        text-decoration: none;
      }

      html {
        width: 100vw;
        height: 100vh;
        overflow: hidden;
      }

      body {
        width: 100%;
        height: 100%;
        overflow: auto;
      }

      html,
      body {
        background-color: #dfe6e9;
        margin: 0;
        padding: 0;
      }

      main {
        max-width: 80%;
        min-width: 300px;
        min-height: 70%;
        margin: 0 auto;
        padding: 40px;
        background-color: #fff;
        border-radius: 5px;
      }

      footer {
        width: 100%;
        font-size: 10px;
        text-align: center;
        padding: 20px;
      }

      .btn {
        text-decoration: none;
        color: #fff;
        padding: 10px 40px;
        text-align: center;
        background-color: #00b894;
        border-radius: 3px;
        font-weight: bold;
        font-size: 14px;
      }

      .btn:hover {
        background-color: #55efc4;
      }

      .txt-c {
        text-align: center;
      }

      .pad-sm {
        padding: 20px;
      }
      .oferta {
        padding: 20px 0;
      }
      .oferta .row {
        display: flex;
        flex-direction: row;
        align-items: center;
      }
      .oferta .row .key {
        font-weight: bold;
      }
      .oferta .row .val {
        padding: 0 20px;
      }
    </style>
  </head>

  <body>
    <main>
      <header class="txt-c pad-sm">
        <img
          src="http://umcsa.net/images/logo2.png"
          alt="UMCSA-LOGO"
          width="150"
        />
      </header>
      <section>
        <h1>Su oferta es la ganadora!!!</h1>
        <article class="oferta">
          <div class="row">
            <div class="key">Remate:</div>
            <div class="val">No 198</div>
          </div>
          <div class="row">
            <div class="key">Fecha:</div>
            <div class="val">05/12/2020 11:30:00</div>
          </div>
          <div class="row">
            <div class="key">Fecha cierre:</div>
            <div class="val">05/12/2020 17:30:00</div>
          </div>
          <div class="row">
            <div class="key">Titulo:</div>
            <div class="val">
              123° Remate Televisado desde Gdor. Virasoro, Corrientes
            </div>
          </div>
          <div class="row">
            <div class="key">Valor en pesos:</div>
            <div class="val">35000</div>
          </div>
        </article>
      </section>
      <section class="txt-c pad-sm">
        <a href="http://umcsa.net/profile.php" class="btn">Mis Ofertas</a>
      </section>
    </main>
    <footer>
      <div>UMC S.A - Negocios Agropecuarios</div>
      <div>2021 &copy; Todo derecho reservado.</div>
    </footer>
  </body>
</html>
