<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>UMCSA</title>
    <style>
      * {
        box-sizing: border-box;
        text-decoration: none;
      }

      html {
        width: 100vw;
        height: 100vh;
        overflow: hidden;
      }

      body {
        width: 100%;
        height: 100%;
        overflow: auto;
      }

      html,
      body {
        background-color: #dfe6e9;
        margin: 0;
        padding: 0;
      }

      main {
        max-width: 80%;
        min-width: 300px;
        min-height: 70%;
        margin: 0 auto;
        padding: 40px;
        background-color: #fff;
        border-radius: 5px;
      }

      footer {
        width: 100%;
        font-size: 10px;
        text-align: center;
        padding: 20px;
      }

      .btn {
        text-decoration: none;
        color: #fff;
        padding: 10px 40px;
        text-align: center;
        background-color: #00b894;
        border-radius: 3px;
        font-weight: bold;
        font-size: 14px;
      }

      .btn:hover {
        background-color: #55efc4;
      }

      .txt-c {
        text-align: center;
      }

      .pad-sm {
        padding: 20px;
      }
      .rows {
        padding: 20px 0;
      }
      .rows .row {
        display: flex;
        flex-direction: row;
        align-items: center;
      }
      .rows .row .key {
        font-weight: bold;
      }
      .rows .row .val {
        padding: 0 20px;
      }
    </style>
  </head>

  <body>
    <main>
      <header class="txt-c">
        <img
          src="http://umcsa.net/images/logo2.png"
          alt="UMCSA-LOGO"
          width="150"
        />
      </header>
      <section>
        <h1>Nueva preoferta registrada!</h1>
        <article class="rows">
          <div class="row">
            <div class="key">Nombre:</div>
            <div class="val">Diego</div>
          </div>
          <div class="row">
            <div class="key">Apellido:</div>
            <div class="val">Montenegro</div>
          </div>
          <div class="row">
            <div class="key">Email:</div>
            <div class="val">email@email.com</div>
          </div>
          <div class="row">
            <div class="key">Tel:</div>
            <div class="val">1122111111</div>
          </div>
        </article>
        <article class="rows">
          <div class="row">
            <div class="key">Remate:</div>
            <div class="val">No 198</div>
          </div>
          <div class="row">
            <div class="key">Fecha:</div>
            <div class="val">05/12/2020 11:30:00</div>
          </div>
          <div class="row">
            <div class="key">Fecha cierre:</div>
            <div class="val">05/12/2020 17:30:00</div>
          </div>
          <div class="row">
            <div class="key">Lote:</div>
            <div class="val">11400</div>
          </div>
          <div class="row">
            <div class="key">Orden Lote:</div>
            <div class="val">001</div>
          </div>
          <div class="row">
            <div class="key">Titulo:</div>
            <div class="val">
              123° Remate Televisado desde Gdor. Virasoro, Corrientes
            </div>
          </div>
          <div class="row">
            <div class="key">Valor:</div>
            <div class="val">35000 $</div>
          </div>
        </article>
      </section>
    </main>
    <footer>
      <div>
        <div class="">
          <div>UMC &#8226; Haciendas Villaguay</div>
          <p>
            Nuestras empresas tienen distintos orígenes, similares historias y
            muchas cosas en común, Santa Fé, Entre Ríos, Corrientes, Buenos
            Aires, La Pampa. No importa, da igual. Nuestro ADN es el campo,
            somos productores y nuestra pasión los negocios ganaderos. Nuestro
            principal valor: la palabra. A estos conceptos -transmitidos de
            generaciones anteriores- le sumamos el esfuerzo y el profesionalismo
            que exige la mecánica del mercado agro-ganadero actual. El resultado
            es lo que somos. Confiabilidad, seriedad, flexibilidad, capacidad
            resolutiva, cumplimiento. Nada distinto a lo que nuestros clientes
            esperan de nosotros. Pero nosotros se lo brindamos.
            <b>UMC & Haciendas Villaguay <br />La diferencia es el equipo.</b>
          </p>
        </div>
        <div>
          <a href="http://umcsa.net/contacto.php" class="">Contactanos aqui!</a>
        </div>
      </div>
      <div>
        Usted recibio este mensaje porque este email ha sido registrado en
        umcsa.net, si estima que fue un error por favor ignora el mensaje.
      </div>
      <div>2021 &copy; Todo derecho reservado.</div>
    </footer>
  </body>
</html>
