<!DOCTYPE html>
<?php
include 'includes/funciones.php';
$pageID = 'pro';
?>


<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="app/css/loader.css">
    <link rel="stylesheet" href="app/css/profile.css">

    <title><? __($auth->user("nombre")) ?> :: UMC S.A. Negocios Agropecuarios ::</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php
    include 'header.php';
    ?>

    <div class="container" style="padding-top:230px;">
        <div class="container-fluid">
            <div style="" class="row">
                <!--contenido remates-->
                <div id="no-more-tables">
                    <div class="col-xs-12 txtTituremates" style="margin-top: 20px;">
                        <span>MIS OFERTAS</span> <b style="color: #7a7a7a;">ACTIVAS</b>
                    </div>
                    <div id="ofertas" data-user="<? __($auth->user()) ?>"></div>
                </div>
                <!--fin contenido remates-->
            </div>
        </div>
    </div>
    <div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
        <?php include 'footer.php' ?>
    </div>

    <!-- Bootstrap core JavaScript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- clarcked -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
</body>

</html>