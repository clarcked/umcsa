<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingresar | UMC S.A - Negocios Agropecuarios</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="../app/css/login.css" />
</head>

<body>
    <div id="login">
        <form class="login form" action="" method="post">
            <div class="brand">
                <img src="../images/logo2.png" alt="umc-logo" width="150" />
            </div>
            <div style="text-align: center;padding:20px 0 5px 0;">
                <? if (isset($flash)) : ?><div class="flash <? __($flash['level']); ?>"><? __($flash['message']); ?></div><? endif; ?>
            </div>
            <div class="fields">
                <label class="field">
                    <div class="label">Nombre</div>
                    <input type="text" name="user[nombre]" id="nombre" required />
                </label>
                <label class="field">
                    <div class="label">Apellido</div>
                    <input type="text" name="user[apellido]" id="apellido" required />
                </label>
                <label class="field">
                    <div class="label">Tel</div>
                    <input type="text" name="user[tel]" id="tel" required />
                </label>
                <label class="field">
                    <div class="label">Email</div>
                    <input type="text" name="user[email]" id="email" required />
                </label>
                <label class="field">
                    <div class="label">Clave</div>
                    <input type="password" name="user[password]" id="password" required />
                </label>
                <label class="field">
                    <div class="label">Confirmar Clave</div>
                    <input type="password" name="user[vpassword]" id="vpassword" required />
                </label>
                <div class="field submit">
                    <button type="submit" class="btn">Registrar</button>
                </div>
                <div class="field register-msg">
                    <a href="<? __(__link("/users/login.php")) ?>">Ya tienes cuenta ? Ingresar</a>
                </div>
                <div class="field recover-msg">
                    <a href="<? __(__link("/users/recover.php")) ?>">Olvide mi Usuario/Clave</a>
                </div>
            </div>
        </form>
    </div>

</body>

</html>