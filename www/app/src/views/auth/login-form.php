<!--login-form-->
<div id="login">
    <form class="login form" action="<? __(__link("users/login.php")) ?>" method="post">
        <div class="brand">
            <a href="<? __(__link("")) ?>">
                <img src="<? __(__link("../images/logo2.png")) ?>" alt="umc-logo" width="150"/>
            </a>
        </div>
        <div style="text-align: center;padding:20px 0 5px 0;">
            <? if (isset($flash)) : ?>
                <div class="flash <? __($flash['level']); ?>"><? __($flash['message']); ?></div><? endif; ?>
        </div>
        <div class="fields">
            <label class="field">
                <div class="label">Email</div>
                <input type="text" name="login[email]" id="email" required/>
            </label>
            <label class="field">
                <div class="label">Clave</div>
                <input type="password" name="login[password]" id="password" required/>
            </label>
            <div class="field submit">
                <button type="submit" class="btn">Ingresar</button>
            </div>
            <div class="field register-msg">
                <a href="<? __(__link("users/register.php")) ?>">Registráte!</a>
            </div>
            <div class="field recover-msg">
                <a href="<? __(__link("users/recover.php")) ?>">Olvidé mi Usuario/Clave</a>
            </div>
        </div>
    </form>
</div>
<!--login-form-->
