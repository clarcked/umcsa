<? if (isset($auth)) _($auth->islogged()) ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingresar | UMC S.A - Negocios Agropecuarios</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="../app/css/login.css" />
</head>

<body>
    <? include_once __path("/src/views/auth/login-form.php"); ?>
</body>

</html>