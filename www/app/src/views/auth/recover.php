<!-- <? pr($flash); ?> -->
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blanquear clave | UMC S.A - Negocios Agropecuarios</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="../app/css/login.css" />
</head>

<body>
    <div id="login">
        <form class="login form" action="" method="post">
            <div class="brand">
                <img src="../images/logo2.png" alt="umc-logo" width="150" />
            </div>
            <div class="fields">
                <label class="field">
                    <div class="label">Email</div>
                    <input type="email" name="recover[email]" id="email" required />
                    <? if ($flash["level"] == "error") : ?><div class="flash error"><? if ($flash) __($flash["message"]); ?></div><? endif; ?>
                    <? if ($flash["level"] == "success") : ?><div class="flash"><? if ($flash) __($flash["message"]); ?></div><? endif; ?>
                </label>
                <div class="field submit">
                    <button type="submit" class="btn">Blanquear mi clave</button>
                </div>
                <div class="field recover-msg">
                    <a href="<? __(__link("/users/login.php")) ?>">Ingresar</a>
                </div>
            </div>
        </form>
    </div>
</body>

</html>