<?php

namespace Umc\App\Model\Entity;

/**
 * PreofertaPauta
 */
class PreofertaPauta
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $remate_id;

    /**
     * @var \DateTime
     */
    public $fecha_desde;

    /**
     * @var \DateTime
     */
    public $fecha_hasta;

    /**
     * @var \DateTime
     */
    public $hora_desde;

    /**
     * @var \DateTime
     */
    public $hora_hasta;

    /**
     * @var integer
     */
    public $valor_suma;

    /**
     * @var string
     */
    public $leyenda_espacial;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remateId
     *
     * @param integer $remateId
     *
     * @return PreofertaPauta
     */
    public function setRemateId($remateId)
    {
        $this->remate_id = $remateId;

        return $this;
    }

    /**
     * Get remateId
     *
     * @return integer
     */
    public function getRemateId()
    {
        return $this->remate_id;
    }

    /**
     * Set fechaDesde
     *
     * @param \DateTime $fechaDesde
     *
     * @return PreofertaPauta
     */
    public function setFechaDesde($fechaDesde)
    {
        $this->fecha_desde = $fechaDesde;

        return $this;
    }

    /**
     * Get fechaDesde
     *
     * @return \DateTime
     */
    public function getFechaDesde()
    {
        return $this->fecha_desde;
    }

    /**
     * Set fechaHasta
     *
     * @param \DateTime $fechaHasta
     *
     * @return PreofertaPauta
     */
    public function setFechaHasta($fechaHasta)
    {
        $this->fecha_hasta = $fechaHasta;

        return $this;
    }

    /**
     * Get fechaHasta
     *
     * @return \DateTime
     */
    public function getFechaHasta()
    {
        return $this->fecha_hasta;
    }

    /**
     * Set horaDesde
     *
     * @param \DateTime $horaDesde
     *
     * @return PreofertaPauta
     */
    public function setHoraDesde($horaDesde)
    {
        $this->hora_desde = $horaDesde;

        return $this;
    }

    /**
     * Get horaDesde
     *
     * @return \DateTime
     */
    public function getHoraDesde()
    {
        return $this->hora_desde;
    }

    /**
     * Set horaHasta
     *
     * @param \DateTime $horaHasta
     *
     * @return PreofertaPauta
     */
    public function setHoraHasta($horaHasta)
    {
        $this->hora_hasta = $horaHasta;

        return $this;
    }

    /**
     * Get horaHasta
     *
     * @return \DateTime
     */
    public function getHoraHasta()
    {
        return $this->hora_hasta;
    }

    /**
     * Set valorSuma
     *
     * @param integer $valorSuma
     *
     * @return PreofertaPauta
     */
    public function setValorSuma($valorSuma)
    {
        $this->valor_suma = $valorSuma;

        return $this;
    }

    /**
     * Get valorSuma
     *
     * @return integer
     */
    public function getValorSuma()
    {
        return $this->valor_suma;
    }

    /**
     * Set leyendaEspacial
     *
     * @param string $leyendaEspacial
     *
     * @return PreofertaPauta
     */
    public function setLeyendaEspacial($leyendaEspacial)
    {
        $this->leyenda_espacial = $leyendaEspacial;

        return $this;
    }

    /**
     * Get leyendaEspacial
     *
     * @return string
     */
    public function getLeyendaEspacial()
    {
        return $this->leyenda_espacial;
    }
}

