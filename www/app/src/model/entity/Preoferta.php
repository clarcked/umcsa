<?php

namespace Umc\App\Model\Entity;

/**
 * Preoferta
 */
class Preoferta
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $remate_id;

    /**
     * @var integer
     */
    public $usr_id;

    /**
     * @var \DateTime
     */
    public $fecha_y_hora;

    /**
     * @var integer
     */
    public $valor;

    /**
     * @var integer
     */
    public $lote_id;

    /**
     * @var string
     */
    public $lote_orden;

    /**
     * @var string
     */
    public $estado;


    /**
     * @var Remate
     */
    public $remate;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Lote
     */
    public $lote;

    /**
     * @return Lote
     */
    public function getLote()
    {
        return $this->lote;
    }

    /**
     * @param Lote $lote
     * @return Preoferta
     */
    public function setLote($lote)
    {
        $this->lote = $lote;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Preoferta
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


    /**
     * @return Remate
     */
    public function getRemate()
    {
        return $this->remate;
    }

    /**
     * @param Remate $remate
     * @return Preoferta
     */
    public function setRemate(Remate $remate)
    {
        $this->remate = $remate;
        return $this;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remateId
     *
     * @param integer $remateId
     *
     * @return Preoferta
     */
    public function setRemateId($remateId)
    {
        $this->remate_id = $remateId;

        return $this;
    }

    /**
     * Get remateId
     *
     * @return integer
     */
    public function getRemateId()
    {
        return $this->remate_id;
    }

    /**
     * Set usrId
     *
     * @param integer $usrId
     *
     * @return Preoferta
     */
    public function setUsrId($usrId)
    {
        $this->usr_id = $usrId;

        return $this;
    }

    /**
     * Get usrId
     *
     * @return integer
     */
    public function getUsrId()
    {
        return $this->usr_id;
    }

    /**
     * Set fechaYHora
     *
     * @param \DateTime $fechaYHora
     *
     * @return Preoferta
     */
    public function setFechaYHora($fechaYHora)
    {
        $this->fecha_y_hora = $fechaYHora;

        return $this;
    }

    /**
     * Get fechaYHora
     *
     * @return \DateTime
     */
    public function getFechaYHora()
    {
        return $this->fecha_y_hora;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     *
     * @return Preoferta
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return integer
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set loteId
     *
     * @param integer $loteId
     *
     * @return Preoferta
     */
    public function setLoteId($loteId)
    {
        $this->lote_id = $loteId;

        return $this;
    }

    /**
     * Get loteId
     *
     * @return integer
     */
    public function getLoteId()
    {
        return $this->lote_id;
    }

    /**
     * Set loteOrden
     *
     * @param string $loteOrden
     *
     * @return Preoferta
     */
    public function setLoteOrden($loteOrden)
    {
        $this->lote_orden = $loteOrden;

        return $this;
    }

    /**
     * Get loteOrden
     *
     * @return string
     */
    public function getLoteOrden()
    {
        return $this->lote_orden;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Preoferta
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }
}

