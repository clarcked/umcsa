<?php

namespace Umc\App\Model\Entity;

/**
 * Lote
 */
class Lote
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $activo;

    /**
     * @var string
     */
    public $venta_tipo;

    /**
     * @var string
     */
    public $vendido;

    /**
     * @var integer
     */
    public $remate_id;

    /**
     * @var integer
     */
    public $conjunto_lote_id;

    /**
     * @var \DateTime
     */
    public $fecha_film;

    /**
     * @var string
     */
    public $establecimiento;

    /**
     * @var string
     */
    public $remitente;

    /**
     * @var integer
     */
    public $tipo;

    /**
     * @var integer
     */
    public $categoria;

    /**
     * @var integer
     */
    public $sub_categoria;

    /**
     * @var string
     */
    public $codigo;

    /**
     * @var string
     */
    public $orden;

    /**
     * @var string
     */
    public $titulo;

    /**
     * @var integer
     */
    public $base;

    /**
     * @var string
     */
    public $precio;

    /**
     * @var integer
     */
    public $cantidad;

    /**
     * @var string
     */
    public $peso;

    /**
     * @var string
     */
    public $edad_aprox;

    /**
     * @var string
     */
    public $trazabilidad;

    /**
     * @var string
     */
    public $mio_mio;

    /**
     * @var string
     */
    public $garrapata;

    /**
     * @var string
     */
    public $caracteristicas;

    /**
     * @var integer
     */
    public $provincia;

    /**
     * @var integer
     */
    public $localidad;

    /**
     * @var string
     */
    public $lugar;

    /**
     * @var string
     */
    public $video;

    /**
     * @var string
     */
    public $foto_1;

    /**
     * @var string
     */
    public $foto_2;

    /**
     * @var string
     */
    public $foto_3;

    /**
     * @var string
     */
    public $foto_4;

    /**
     * @var string
     */
    public $plazo;

    /**
     * @var string
     */
    public $observaciones;

    /**
     * @var integer
     */
    public $visitas;

    /**
     * @var integer
     */
    public $base_preoferta;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activo
     *
     * @param string $activo
     *
     * @return Lote
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set ventaTipo
     *
     * @param string $ventaTipo
     *
     * @return Lote
     */
    public function setVentaTipo($ventaTipo)
    {
        $this->venta_tipo = $ventaTipo;

        return $this;
    }

    /**
     * Get ventaTipo
     *
     * @return string
     */
    public function getVentaTipo()
    {
        return $this->venta_tipo;
    }

    /**
     * Set vendido
     *
     * @param string $vendido
     *
     * @return Lote
     */
    public function setVendido($vendido)
    {
        $this->vendido = $vendido;

        return $this;
    }

    /**
     * Get vendido
     *
     * @return string
     */
    public function getVendido()
    {
        return $this->vendido;
    }

    /**
     * Set remateId
     *
     * @param integer $remateId
     *
     * @return Lote
     */
    public function setRemateId($remateId)
    {
        $this->remate_id = $remateId;

        return $this;
    }

    /**
     * Get remateId
     *
     * @return integer
     */
    public function getRemateId()
    {
        return $this->remate_id;
    }

    /**
     * Set conjuntoLoteId
     *
     * @param integer $conjuntoLoteId
     *
     * @return Lote
     */
    public function setConjuntoLoteId($conjuntoLoteId)
    {
        $this->conjunto_lote_id = $conjuntoLoteId;

        return $this;
    }

    /**
     * Get conjuntoLoteId
     *
     * @return integer
     */
    public function getConjuntoLoteId()
    {
        return $this->conjunto_lote_id;
    }

    /**
     * Set fechaFilm
     *
     * @param \DateTime $fechaFilm
     *
     * @return Lote
     */
    public function setFechaFilm($fechaFilm)
    {
        $this->fecha_film = $fechaFilm;

        return $this;
    }

    /**
     * Get fechaFilm
     *
     * @return \DateTime
     */
    public function getFechaFilm()
    {
        return $this->fecha_film;
    }

    /**
     * Set establecimiento
     *
     * @param string $establecimiento
     *
     * @return Lote
     */
    public function setEstablecimiento($establecimiento)
    {
        $this->establecimiento = $establecimiento;

        return $this;
    }

    /**
     * Get establecimiento
     *
     * @return string
     */
    public function getEstablecimiento()
    {
        return $this->establecimiento;
    }

    /**
     * Set remitente
     *
     * @param string $remitente
     *
     * @return Lote
     */
    public function setRemitente($remitente)
    {
        $this->remitente = $remitente;

        return $this;
    }

    /**
     * Get remitente
     *
     * @return string
     */
    public function getRemitente()
    {
        return $this->remitente;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return Lote
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set categoria
     *
     * @param integer $categoria
     *
     * @return Lote
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return integer
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set subCategoria
     *
     * @param integer $subCategoria
     *
     * @return Lote
     */
    public function setSubCategoria($subCategoria)
    {
        $this->sub_categoria = $subCategoria;

        return $this;
    }

    /**
     * Get subCategoria
     *
     * @return integer
     */
    public function getSubCategoria()
    {
        return $this->sub_categoria;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Lote
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set orden
     *
     * @param string $orden
     *
     * @return Lote
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return string
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Lote
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set base
     *
     * @param integer $base
     *
     * @return Lote
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return integer
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Lote
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Lote
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set peso
     *
     * @param string $peso
     *
     * @return Lote
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set edadAprox
     *
     * @param string $edadAprox
     *
     * @return Lote
     */
    public function setEdadAprox($edadAprox)
    {
        $this->edad_aprox = $edadAprox;

        return $this;
    }

    /**
     * Get edadAprox
     *
     * @return string
     */
    public function getEdadAprox()
    {
        return $this->edad_aprox;
    }

    /**
     * Set trazabilidad
     *
     * @param string $trazabilidad
     *
     * @return Lote
     */
    public function setTrazabilidad($trazabilidad)
    {
        $this->trazabilidad = $trazabilidad;

        return $this;
    }

    /**
     * Get trazabilidad
     *
     * @return string
     */
    public function getTrazabilidad()
    {
        return $this->trazabilidad;
    }

    /**
     * Set mioMio
     *
     * @param string $mioMio
     *
     * @return Lote
     */
    public function setMioMio($mioMio)
    {
        $this->mio_mio = $mioMio;

        return $this;
    }

    /**
     * Get mioMio
     *
     * @return string
     */
    public function getMioMio()
    {
        return $this->mio_mio;
    }

    /**
     * Set garrapata
     *
     * @param string $garrapata
     *
     * @return Lote
     */
    public function setGarrapata($garrapata)
    {
        $this->garrapata = $garrapata;

        return $this;
    }

    /**
     * Get garrapata
     *
     * @return string
     */
    public function getGarrapata()
    {
        return $this->garrapata;
    }

    /**
     * Set caracteristicas
     *
     * @param string $caracteristicas
     *
     * @return Lote
     */
    public function setCaracteristicas($caracteristicas)
    {
        $this->caracteristicas = $caracteristicas;

        return $this;
    }

    /**
     * Get caracteristicas
     *
     * @return string
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Set provincia
     *
     * @param integer $provincia
     *
     * @return Lote
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return integer
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set localidad
     *
     * @param integer $localidad
     *
     * @return Lote
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return integer
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     *
     * @return Lote
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;

        return $this;
    }

    /**
     * Get lugar
     *
     * @return string
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Lote
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set foto1
     *
     * @param string $foto1
     *
     * @return Lote
     */
    public function setFoto1($foto1)
    {
        $this->foto_1 = $foto1;

        return $this;
    }

    /**
     * Get foto1
     *
     * @return string
     */
    public function getFoto1()
    {
        return $this->foto_1;
    }

    /**
     * Set foto2
     *
     * @param string $foto2
     *
     * @return Lote
     */
    public function setFoto2($foto2)
    {
        $this->foto_2 = $foto2;

        return $this;
    }

    /**
     * Get foto2
     *
     * @return string
     */
    public function getFoto2()
    {
        return $this->foto_2;
    }

    /**
     * Set foto3
     *
     * @param string $foto3
     *
     * @return Lote
     */
    public function setFoto3($foto3)
    {
        $this->foto_3 = $foto3;

        return $this;
    }

    /**
     * Get foto3
     *
     * @return string
     */
    public function getFoto3()
    {
        return $this->foto_3;
    }

    /**
     * Set foto4
     *
     * @param string $foto4
     *
     * @return Lote
     */
    public function setFoto4($foto4)
    {
        $this->foto_4 = $foto4;

        return $this;
    }

    /**
     * Get foto4
     *
     * @return string
     */
    public function getFoto4()
    {
        return $this->foto_4;
    }

    /**
     * Set plazo
     *
     * @param string $plazo
     *
     * @return Lote
     */
    public function setPlazo($plazo)
    {
        $this->plazo = $plazo;

        return $this;
    }

    /**
     * Get plazo
     *
     * @return string
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     *
     * @return Lote
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set visitas
     *
     * @param integer $visitas
     *
     * @return Lote
     */
    public function setVisitas($visitas)
    {
        $this->visitas = $visitas;

        return $this;
    }

    /**
     * Get visitas
     *
     * @return integer
     */
    public function getVisitas()
    {
        return $this->visitas;
    }

    /**
     * Set basePreoferta
     *
     * @param integer $basePreoferta
     *
     * @return Lote
     */
    public function setBasePreoferta($basePreoferta)
    {
        $this->base_preoferta = $basePreoferta;

        return $this;
    }

    /**
     * Get basePreoferta
     *
     * @return integer
     */
    public function getBasePreoferta()
    {
        return $this->base_preoferta;
    }
}

