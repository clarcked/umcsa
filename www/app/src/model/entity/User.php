<?php

namespace Umc\App\Model\Entity;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $nombre;

    /**
     * @var string
     */
    public $apellido;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $password_hash;

    /**
     * @var string
     */
    public $notas;

    /**
     * @var string
     */
    public $activo;

    /**
     * @var string
     */
    public $empresa_id;

    /**
     * @var string
     */
    public $cargo;

    /**
     * @var string
     */
    public $tel_movil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return User
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set passwordHash
     *
     * @param string $passwordHash
     *
     * @return User
     */
    public function setPasswordHash($passwordHash)
    {
        $this->password_hash = $passwordHash;

        return $this;
    }

    /**
     * Get passwordHash
     *
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->password_hash;
    }

    /**
     * Set notas
     *
     * @param string $notas
     *
     * @return User
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * Set activo
     *
     * @param string $activo
     *
     * @return User
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set empresaId
     *
     * @param string $empresaId
     *
     * @return User
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresa_id = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return string
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return User
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set telMovil
     *
     * @param string $telMovil
     *
     * @return User
     */
    public function setTelMovil($telMovil)
    {
        $this->tel_movil = $telMovil;

        return $this;
    }

    /**
     * Get telMovil
     *
     * @return string
     */
    public function getTelMovil()
    {
        return $this->tel_movil;
    }
}

