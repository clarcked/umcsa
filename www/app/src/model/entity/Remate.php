<?php

namespace Umc\App\Model\Entity;

/**
 * Remate
 */
class Remate
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $tipo;

    /**
     * @var string
     */
    public $plataforma;

    /**
     * @var integer
     */
    public $ext_id;

    /**
     * @var \DateTime
     */
    public $fecha;

    /**
     * @var \DateTime
     */
    public $hora_inicio;

    /**
     * @var \DateTime
     */
    public $hora_fin;

    /**
     * @var string
     */
    public $zona;

    /**
     * @var string
     */
    public $lugar;

    /**
     * @var string
     */
    public $locacion;

    /**
     * @var integer
     */
    public $numero;

    /**
     * @var string
     */
    public $titulo;

    /**
     * @var string
     */
    public $descripcion;

    /**
     * @var integer
     */
    public $cantidad;

    /**
     * @var string
     */
    public $banner_g;

    /**
     * @var string
     */
    public $banner_g_visible;

    /**
     * @var integer
     */
    public $banner_g_z_index;

    /**
     * @var integer
     */
    public $banner_g_pos_x;

    /**
     * @var integer
     */
    public $banner_g_pos_y;

    /**
     * @var string
     */
    public $banner_g_link;

    /**
     * @var string
     */
    public $mapa_img;

    /**
     * @var string
     */
    public $yacare_visible;

    /**
     * @var string
     */
    public $banner_ch;

    /**
     * @var string
     */
    public $video_final_servidor;

    /**
     * @var string
     */
    public $video_final_1;

    /**
     * @var string
     */
    public $video_titu_1;

    /**
     * @var string
     */
    public $video_final_2;

    /**
     * @var string
     */
    public $video_titu_2;

    /**
     * @var string
     */
    public $video_final_3;

    /**
     * @var string
     */
    public $video_titu_3;

    /**
     * @var string
     */
    public $video_final_4;

    /**
     * @var string
     */
    public $video_titu_4;

    /**
     * @var string
     */
    public $video_final_5;

    /**
     * @var string
     */
    public $video_titu_5;

    /**
     * @var string
     */
    public $activo;

    /**
     * @var string
     */
    public $archivo;

    /**
     * @var string
     */
    public $precios;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Remate
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set plataforma
     *
     * @param string $plataforma
     *
     * @return Remate
     */
    public function setPlataforma($plataforma)
    {
        $this->plataforma = $plataforma;

        return $this;
    }

    /**
     * Get plataforma
     *
     * @return string
     */
    public function getPlataforma()
    {
        return $this->plataforma;
    }

    /**
     * Set extId
     *
     * @param integer $extId
     *
     * @return Remate
     */
    public function setExtId($extId)
    {
        $this->ext_id = $extId;

        return $this;
    }

    /**
     * Get extId
     *
     * @return integer
     */
    public function getExtId()
    {
        return $this->ext_id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Remate
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set horaInicio
     *
     * @param \DateTime $horaInicio
     *
     * @return Remate
     */
    public function setHoraInicio($horaInicio)
    {
        $this->hora_inicio = $horaInicio;

        return $this;
    }

    /**
     * Get horaInicio
     *
     * @return \DateTime
     */
    public function getHoraInicio()
    {
        return $this->hora_inicio;
    }

    /**
     * Set horaFin
     *
     * @param \DateTime $horaFin
     *
     * @return Remate
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;

        return $this;
    }

    /**
     * Get horaFin
     *
     * @return \DateTime
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set zona
     *
     * @param string $zona
     *
     * @return Remate
     */
    public function setZona($zona)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     *
     * @return Remate
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;

        return $this;
    }

    /**
     * Get lugar
     *
     * @return string
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set locacion
     *
     * @param string $locacion
     *
     * @return Remate
     */
    public function setLocacion($locacion)
    {
        $this->locacion = $locacion;

        return $this;
    }

    /**
     * Get locacion
     *
     * @return string
     */
    public function getLocacion()
    {
        return $this->locacion;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Remate
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Remate
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Remate
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Remate
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set bannerG
     *
     * @param string $bannerG
     *
     * @return Remate
     */
    public function setBannerG($bannerG)
    {
        $this->banner_g = $bannerG;

        return $this;
    }

    /**
     * Get bannerG
     *
     * @return string
     */
    public function getBannerG()
    {
        return $this->banner_g;
    }

    /**
     * Set bannerGVisible
     *
     * @param string $bannerGVisible
     *
     * @return Remate
     */
    public function setBannerGVisible($bannerGVisible)
    {
        $this->banner_g_visible = $bannerGVisible;

        return $this;
    }

    /**
     * Get bannerGVisible
     *
     * @return string
     */
    public function getBannerGVisible()
    {
        return $this->banner_g_visible;
    }

    /**
     * Set bannerGZIndex
     *
     * @param integer $bannerGZIndex
     *
     * @return Remate
     */
    public function setBannerGZIndex($bannerGZIndex)
    {
        $this->banner_g_z_index = $bannerGZIndex;

        return $this;
    }

    /**
     * Get bannerGZIndex
     *
     * @return integer
     */
    public function getBannerGZIndex()
    {
        return $this->banner_g_z_index;
    }

    /**
     * Set bannerGPosX
     *
     * @param integer $bannerGPosX
     *
     * @return Remate
     */
    public function setBannerGPosX($bannerGPosX)
    {
        $this->banner_g_pos_x = $bannerGPosX;

        return $this;
    }

    /**
     * Get bannerGPosX
     *
     * @return integer
     */
    public function getBannerGPosX()
    {
        return $this->banner_g_pos_x;
    }

    /**
     * Set bannerGPosY
     *
     * @param integer $bannerGPosY
     *
     * @return Remate
     */
    public function setBannerGPosY($bannerGPosY)
    {
        $this->banner_g_pos_y = $bannerGPosY;

        return $this;
    }

    /**
     * Get bannerGPosY
     *
     * @return integer
     */
    public function getBannerGPosY()
    {
        return $this->banner_g_pos_y;
    }

    /**
     * Set bannerGLink
     *
     * @param string $bannerGLink
     *
     * @return Remate
     */
    public function setBannerGLink($bannerGLink)
    {
        $this->banner_g_link = $bannerGLink;

        return $this;
    }

    /**
     * Get bannerGLink
     *
     * @return string
     */
    public function getBannerGLink()
    {
        return $this->banner_g_link;
    }

    /**
     * Set mapaImg
     *
     * @param string $mapaImg
     *
     * @return Remate
     */
    public function setMapaImg($mapaImg)
    {
        $this->mapa_img = $mapaImg;

        return $this;
    }

    /**
     * Get mapaImg
     *
     * @return string
     */
    public function getMapaImg()
    {
        return $this->mapa_img;
    }

    /**
     * Set yacareVisible
     *
     * @param string $yacareVisible
     *
     * @return Remate
     */
    public function setYacareVisible($yacareVisible)
    {
        $this->yacare_visible = $yacareVisible;

        return $this;
    }

    /**
     * Get yacareVisible
     *
     * @return string
     */
    public function getYacareVisible()
    {
        return $this->yacare_visible;
    }

    /**
     * Set bannerCh
     *
     * @param string $bannerCh
     *
     * @return Remate
     */
    public function setBannerCh($bannerCh)
    {
        $this->banner_ch = $bannerCh;

        return $this;
    }

    /**
     * Get bannerCh
     *
     * @return string
     */
    public function getBannerCh()
    {
        return $this->banner_ch;
    }

    /**
     * Set videoFinalServidor
     *
     * @param string $videoFinalServidor
     *
     * @return Remate
     */
    public function setVideoFinalServidor($videoFinalServidor)
    {
        $this->video_final_servidor = $videoFinalServidor;

        return $this;
    }

    /**
     * Get videoFinalServidor
     *
     * @return string
     */
    public function getVideoFinalServidor()
    {
        return $this->video_final_servidor;
    }

    /**
     * Set videoFinal1
     *
     * @param string $videoFinal1
     *
     * @return Remate
     */
    public function setVideoFinal1($videoFinal1)
    {
        $this->video_final_1 = $videoFinal1;

        return $this;
    }

    /**
     * Get videoFinal1
     *
     * @return string
     */
    public function getVideoFinal1()
    {
        return $this->video_final_1;
    }

    /**
     * Set videoTitu1
     *
     * @param string $videoTitu1
     *
     * @return Remate
     */
    public function setVideoTitu1($videoTitu1)
    {
        $this->video_titu_1 = $videoTitu1;

        return $this;
    }

    /**
     * Get videoTitu1
     *
     * @return string
     */
    public function getVideoTitu1()
    {
        return $this->video_titu_1;
    }

    /**
     * Set videoFinal2
     *
     * @param string $videoFinal2
     *
     * @return Remate
     */
    public function setVideoFinal2($videoFinal2)
    {
        $this->video_final_2 = $videoFinal2;

        return $this;
    }

    /**
     * Get videoFinal2
     *
     * @return string
     */
    public function getVideoFinal2()
    {
        return $this->video_final_2;
    }

    /**
     * Set videoTitu2
     *
     * @param string $videoTitu2
     *
     * @return Remate
     */
    public function setVideoTitu2($videoTitu2)
    {
        $this->video_titu_2 = $videoTitu2;

        return $this;
    }

    /**
     * Get videoTitu2
     *
     * @return string
     */
    public function getVideoTitu2()
    {
        return $this->video_titu_2;
    }

    /**
     * Set videoFinal3
     *
     * @param string $videoFinal3
     *
     * @return Remate
     */
    public function setVideoFinal3($videoFinal3)
    {
        $this->video_final_3 = $videoFinal3;

        return $this;
    }

    /**
     * Get videoFinal3
     *
     * @return string
     */
    public function getVideoFinal3()
    {
        return $this->video_final_3;
    }

    /**
     * Set videoTitu3
     *
     * @param string $videoTitu3
     *
     * @return Remate
     */
    public function setVideoTitu3($videoTitu3)
    {
        $this->video_titu_3 = $videoTitu3;

        return $this;
    }

    /**
     * Get videoTitu3
     *
     * @return string
     */
    public function getVideoTitu3()
    {
        return $this->video_titu_3;
    }

    /**
     * Set videoFinal4
     *
     * @param string $videoFinal4
     *
     * @return Remate
     */
    public function setVideoFinal4($videoFinal4)
    {
        $this->video_final_4 = $videoFinal4;

        return $this;
    }

    /**
     * Get videoFinal4
     *
     * @return string
     */
    public function getVideoFinal4()
    {
        return $this->video_final_4;
    }

    /**
     * Set videoTitu4
     *
     * @param string $videoTitu4
     *
     * @return Remate
     */
    public function setVideoTitu4($videoTitu4)
    {
        $this->video_titu_4 = $videoTitu4;

        return $this;
    }

    /**
     * Get videoTitu4
     *
     * @return string
     */
    public function getVideoTitu4()
    {
        return $this->video_titu_4;
    }

    /**
     * Set videoFinal5
     *
     * @param string $videoFinal5
     *
     * @return Remate
     */
    public function setVideoFinal5($videoFinal5)
    {
        $this->video_final_5 = $videoFinal5;

        return $this;
    }

    /**
     * Get videoFinal5
     *
     * @return string
     */
    public function getVideoFinal5()
    {
        return $this->video_final_5;
    }

    /**
     * Set videoTitu5
     *
     * @param string $videoTitu5
     *
     * @return Remate
     */
    public function setVideoTitu5($videoTitu5)
    {
        $this->video_titu_5 = $videoTitu5;

        return $this;
    }

    /**
     * Get videoTitu5
     *
     * @return string
     */
    public function getVideoTitu5()
    {
        return $this->video_titu_5;
    }

    /**
     * Set activo
     *
     * @param string $activo
     *
     * @return Remate
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set archivo
     *
     * @param string $archivo
     *
     * @return Remate
     */
    public function setArchivo($archivo)
    {
        $this->archivo = $archivo;

        return $this;
    }

    /**
     * Get archivo
     *
     * @return string
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    /**
     * Set precios
     *
     * @param string $precios
     *
     * @return Remate
     */
    public function setPrecios($precios)
    {
        $this->precios = $precios;

        return $this;
    }

    /**
     * Get precios
     *
     * @return string
     */
    public function getPrecios()
    {
        return $this->precios;
    }
}

