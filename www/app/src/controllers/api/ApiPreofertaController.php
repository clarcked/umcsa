<?php

namespace Umc\App\Controllers\Api;

use DateTime;
use Umc\App\Model\Entity\Preoferta;
use Umc\App\Model\Repository\PreofertaRepository;
use Umc\App\Services\Http\Response;
use Umc\App\Services\Mail\Messenger;

class ApiPreofertaController extends ApiController
{
    protected $entityClass = "Umc\App\Model\Entity\Preoferta";


    public function create()
    {
        $params = $this->request->body();
        $allowed = ["usr_id", "remate_id", "lote_id", "lote_orden", "valor"];
        $missing = array_diff($allowed, array_keys($params));
        if ($missing) return Response::redirect404();

        /** @var PreofertaRepository */
        $repo = $this->db->getRepository($this->entityClass);
        $preoferta = new Preoferta();
        $preoferta->setUsrId($params["usr_id"]);
        $preoferta->setLoteId($params["lote_id"]);
        $preoferta->setLoteOrden($params["lote_orden"]);
        $preoferta->setRemateId($params["remate_id"]);
        $preoferta->setValor($params["valor"]);
        $preoferta->setFechaYHora(new DateTime());

        /**
         * Al guadar la oferta asigna todas las preofertas de este lote
         * anteriores como perdedores
         * y a esta nueva como ganador
         */
        $repo->makeLoser($preoferta->getLoteId());

        /** 
         * Si el usuario anteriormente tenia preoferta para este lote
         * cambiamos el estado a "-1" ( cancelado )
         * esto te salva la vida cuando solo quieras 
         * listar las ofertas mas alta de este usuario
         * sin tener lote duplicado.
         */
        $repo->cancelUserOffer($preoferta->getUsrId(), $preoferta->getLoteId());
        $preoferta->setEstado("1");

        //------------------
        $this->db->persist($preoferta);
        if ($this->db->flush()) {
            Messenger::nuevaOferta($this->mailer, $preoferta, $this->auth);
            Messenger::ofertaGanadora($this->mailer, $preoferta, $this->auth);
            // Messenger::ofertaSuperada($this->mailer, $preoferta, $this->auth);
        }

        return $this->fetch();
    }
}
