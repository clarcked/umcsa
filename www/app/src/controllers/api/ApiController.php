<?php

namespace Umc\App\Controllers\Api;

use Umc\App\Controllers\Controller;
use Umc\App\Services\Http\Response;

abstract class ApiController extends Controller
{
    protected $entityClass;

    protected function json($args)
    {
        header("Content-Type: application/json");
        $content = $this->serialize($args);
        if (!is_null($content)) {
            echo $content;
            exit;
        }
        Response::redirect404();
    }

    public function handle()
    {
        if ($this->request->isMethod("post")) {
            $this->create();
        } else {
            if ($this->filter->isFiltered()) {
                $filter_method = $this->filter->getFilterType();
                if (method_exists($this, $filter_method)) {
                    $this->$filter_method();
                } else {
                    $this->fetch();
                }
            } else {
                $this->fetch();
            }
        }
    }

    public function fetch()
    {
        $properties = $this->filter->getProperties();
        $orderBy = $this->filter->getOrder();
        $limit = $this->filter->getLimit();
        $offset = $this->filter->getOffset();
        $data = $this->db->getRepository($this->entityClass)->findBy($properties, $orderBy, $limit, $offset);
        $this->json($data);
    }

    public function find()
    {
        $properties = $this->filter->getProperties();
        $data = $this->db->getRepository($this->entityClass)->findOneBy($properties);
        $this->json($data);
    }

    public function create()
    {
        Response::redirect404();
    }
}
