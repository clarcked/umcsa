<?php

namespace Umc\App\Controllers\User;

use Umc\App\Controllers\Controller;
use Umc\App\Model\Entity\User;
use Umc\App\Services\Auth\Crypt;
use Umc\App\Services\Http\Response;
use Umc\App\Services\Mail\Messenger;

class UserController extends Controller
{

    public function profile()
    {
        if (!$this->auth->islogged()) return Response::redirect(__link("/"));
        $ctx = [
            "auth" => $this->auth
        ];
        return $this->render("user/profile.php", $ctx);
    }

    public function register()
    {
        $flash = ["level" => "", "message" => ""];
        if ($this->request->isMethod("post")) {
            $token = $this->request->param("user");
            $allowed = ["nombre", "apellido", "tel", "email", "password", "vpassword"];
            $missing = array_diff($allowed, array_keys($token));
            if ($missing) {
                $flash["level"] = "error";
                $flash["message"] = "Por favor completa todo el formulario!";
            } else {
                if ($token["password"] != $token["vpassword"]) {
                    $flash["level"] = "error";
                    $flash["message"] = "Las claves deben ser iguales!";
                } else {
                    # 549 11 00 00 00 00
                    if (strlen($token["tel"]) < 10 || strlen($token["tel"]) > 13) {
                        $flash["level"] = "error";
                        $flash["message"] = "Por favor entra un tel valido!";
                    } else {
                        if (strlen($token["nombre"]) <= 3 || strlen($token["apellido"]) <= 3) {
                            $flash["level"] = "error";
                            $flash["message"] = "Por favor entra un nombre valido!";
                        } else {
                            if (!filter_var($token["email"], FILTER_VALIDATE_EMAIL)) {
                                $flash["level"] = "error";
                                $flash["message"] = "Por favor entra un email valido!";
                            } else {
                                $exists = $this->db->getRepository("Umc\App\Model\Entity\User")->findBy(["email" => $token["email"]]);
                                if ($exists) {
                                    $flash["level"] = "error";
                                    $flash["message"] = "Este usuario ya existe!";
                                } else {
                                    $user = new User();
                                    $user
                                        ->setNombre($token["nombre"])
                                        ->setEmpresaId(123)
                                        ->setCargo("Gerente")
                                        ->setNotas("")
                                        ->setActivo("NO")
                                        ->setApellido($token["apellido"])
                                        ->setTelMovil($token["tel"])
                                        ->setPassword($token["password"])
                                        ->setPasswordHash((new Crypt())->hash($token["password"]))
                                        ->setEmail($token["email"]);
                                    $this->db->persist($user);
                                    $this->db->flush();
                                    Messenger::nuevoUsuario($this->mailer, $user);
                                    $flash["level"] = "success";
                                    $flash["message"] = "Solicitud de alta enviado!";
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this->render("user/register.php", ["flash" => $flash]);
    }
}
