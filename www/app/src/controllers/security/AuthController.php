<?php

namespace Umc\App\Controllers\Security;

use Umc\App\Controllers\Controller;
use Umc\App\Services\Http\Response;
use Umc\App\Services\Mail\Messenger;
use Umc\App\Controllers\User;

class AuthController extends Controller
{
    public function login()
    {
        $data = [];
        if ($this->request->isMethod("post")) {
            $token = $this->request->param("login");
            $user = $this->auth->authenticate($token);
            if ($user) {
                $referer = $this->request->query("ref");
                if ($this->auth->islogged()) return Response::redirect(!empty($referer) ? $referer : __link("/profile.php?no-referer="));
                $data["user"] = $user;
                $data["auth"] = $this->auth;
            }
            $data["token"] = $token;
        }
        return $this->render("auth/login.php", $data);
    }


    public function recover()
    {
        $flash = ["level" => "", "message" => ""];
        if ($this->request->isMethod("post")) {
            $token = $this->request->param("recover");
            $email = isset($token["email"]) ? $token["email"] : null;
            $flash = [
                "message" => "No se pudo enviar el correo, porfavor vuelve a intentar otra vez.",
                "level" => null
            ];
            try {
                if (!is_null($email)) {
                    /** @var User */
                    $user = $this->db->getRepository("Umc\App\Model\Entity\User")->findOneBy(["email" => $email]);
                    if ($user) {
                        $recoverToken = \md5($user->getEmail());
                        Messenger::blanqueoClave($this->mailer, $user, $recoverToken);
                        $flash["level"] = "success";
                        $flash["message"] = "Solicitud de blanqueo de clave, enviado!";
                    }
                }
            } catch (\Exception $th) {
                $flash["level"] = "error";
                $flash["message"] = $th->getMessage();
            }
        }

        return $this->render("auth/recover.php", ["flash" => $flash]);
    }

    public function logout()
    {
        session_destroy();
        Response::redirect($this->request->referer(__link("/")));
    }
}
