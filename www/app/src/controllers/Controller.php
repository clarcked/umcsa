<?php

namespace Umc\App\Controllers;

use Umc\App\Services\Auth\Auth;
use Umc\App\Services\Http\Request;
use Umc\App\Services\Http\RequestFilter;
use Umc\App\Services\Mail\Mailer;
use Umc\App\Services\ORM\DB;

abstract class Controller
{
    /* @var Request */
    protected $request;
    protected $auth;
    protected $mailer;
    protected $db;
    protected $serializer;
    protected $filter;



    public function __construct()
    {
        $this->request = new Request();
        $this->auth = new Auth();
        $this->mailer = new Mailer();
        $this->db  = DB::Manager();
        $this->filter = new RequestFilter();
    }

    protected function serialize($data)
    {
        $result = json_encode($data, JSON_PRETTY_PRINT);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $result;
            default:
                return json_encode(["error" => "An error has occured: " . json_last_error_msg()]);
        }
    }

    protected function render($template, $context = [])
    {
        $file = __TEMPLATEDIR__ . "/$template";
        if (!file_exists($file)) {
            echo "File: $file does'nt exist";
            exit(1);
        }
        extract($context);
        ob_start();
        include_once __TEMPLATEDIR__ . "/$template";
        $body = ob_get_contents();
        ob_end_clean();
        __($body);
        
    }
}
