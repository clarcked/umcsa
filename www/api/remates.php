<?php
require_once "../app/config/bootstrap.php";

use Umc\App\Controllers\Api\ApiRemateController;

$controller = new ApiRemateController();
$controller->handle();