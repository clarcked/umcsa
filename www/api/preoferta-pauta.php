<?php
require_once "../app/config/bootstrap.php";

use Umc\App\Controllers\Api\ApiPreofertaPautaController;

$controller = new ApiPreofertaPautaController();
$controller->handle();