<?php
require_once "../app/config/bootstrap.php";

use Umc\App\Controllers\Api\ApiPreofertaController;

$controller = new ApiPreofertaController();
$controller->handle();