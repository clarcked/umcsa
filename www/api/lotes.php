<?php
require_once "../app/config/bootstrap.php";

use Umc\App\Controllers\Api\ApiLoteController;

$controller = new ApiLoteController();
$controller->handle();