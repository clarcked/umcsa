<?php
require_once("app/config/bootstrap.php");
use Umc\App\Services\Auth\Auth;
$auth = new Auth();
?>
<!DOCTYPE html>
<?php
include 'includes/MySQL.php';
$link = Conectarse();

include 'includes/funciones.php';

$pageID = 'part';

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $locaselected = '';
} else {
    $locaId = '';
    $locaselected = 'selected';
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
} else {
    $traza = '';
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
} else {
    $garpta = '';
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
} else {
    $miomio = '';
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
} else {
    $cate = '';
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
} else {
    $lotesel = '';
}

if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
} else {
    $conjuntolote = '';
}

$miURLsinP = $_SERVER['SCRIPT_NAME'];

$miURL = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];

//echo $miURL;
// maximo por pagina

$limit = 12;

// pagina pedida
if (isset($_GET["pag"])) {
    $pag = (int) $_GET["pag"];
} else {
    $pag = (int) 0;
}
if ($pag < 1) {
    $pag = 1;
}
$offset = ($pag - 1) * $limit;



//Busqueda consultas--------

$mesesArr = array("", "ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");
$diasArr = array("DOMINGO", "LUNES", "MARTES", "MI&Eacute;RCOLES", "JUEVES", "VIERNES", "S&Aacute;BADO");


//---------------------------

$sqlwhere = "";
$sqlwherecl = "";
if (isset($_GET["cl"])) {
    $conjuntolote = $_GET["cl"];
    $sqlwhere = $sqlwhere . "AND lotes.conjunto_lote_id =  '$conjuntolote' ";
    $sqlwherecl = $sqlwherecl . "AND lts.conjunto_lote_id =  '$conjuntolote' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["loca"])) {
    $locaId = $_GET["loca"];
    $sqlwhere = $sqlwhere . "AND lotes.localidad =  '$locaId' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["lotenum"])) {
    $lotesel = $_GET["lotenum"];
    $sqlwhere = $sqlwhere . "AND lotes.id =  '$lotesel' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["tr"])) {
    $traza = $_GET["tr"];
    $sqlwhere = $sqlwhere . "AND lotes.trazabilidad =  '$traza' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["cate"])) {
    $cate = $_GET["cate"];
    $sqlwhere = $sqlwhere . "AND lotes.categoria =  '$cate' ";
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["gr"])) {
    $garpta = $_GET["gr"];
    if ($garpta == 'SI') {
        $sqlwhere = $sqlwhere . "AND (lotes.garrapata =  '$garpta' OR lotes.garrapata =  'ZONA DE LUCHA')";
    } else {
        $sqlwhere = $sqlwhere . "AND lotes.garrapata =  '$garpta'";
    }
} else {
    $sqlwhere = $sqlwhere . "";
}

if (isset($_GET["mm"])) {
    $miomio = $_GET["mm"];
    $sqlwhere = $sqlwhere . "AND lotes.mio_mio =  '$miomio'";
} else {
    $sqlwhere = $sqlwhere . "";
}




$sqlLT = "SELECT
lts.remate_id AS rmtid,
lts.venta_tipo,
lts.conjunto_lote_id,
lts.orden AS ltorden,
lts.codigo AS ltcert,
lts.id AS ltid,
lts.categoria AS ltcate
FROM
lotes AS lts
WHERE
lts.venta_tipo = 'PARTICULAR' " . $sqlwherecl . " ORDER BY ifnull(ltorden, ltcert)";

//echo $sqlLT;

$rsLT = mysql_query($sqlLT);
$cuenta = mysql_num_rows($rsLT);
//echo $cuenta;
$innerselLotes = '';
$lotescuenta = 0;
while ($rowLT = mysql_fetch_array($rsLT)) {
    $lotescuenta+=1;
    if ($lotescuenta == 1) {
        if ($lotesel == '') {

            $innerselLotes = $innerselLotes . '<option value="" selected>TODOS</option>';
        } else {

            $innerselLotes = $innerselLotes . '<option value="">TODOS</option>';
        }
    }

    $ordenLT = $rowLT['ltorden'];
    if ($ordenLT != '') {
        if (substr($ordenLT, 0, 2) == "00") {
            $ordenLT = str_replace("00", "", $ordenLT);
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = str_replace("0", "", $ordenLT);
            }
        } else {
            if (substr($ordenLT, 0, 1) == "0") {
                $ordenLT = substr_replace($ordenLT, "", 0, 1);
            }
        }
    }

    $ordenInner = '';

    if ($rowLT['ltorden'] == '') {
        $ordenInner = 'Certif. N&ordm; ' . $rowLT['ltcert'];
    } else {
        $ordenInner = 'N&ordm; ' . $ordenLT;
    }

    if ($rowLT['ltid'] == $lotesel) {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '" selected>' . $ordenInner . '</option>';
    } else {

        $innerselLotes = $innerselLotes . '<option value="' . $rowLT['ltid'] . '">' . $ordenInner . '</option>';
    }
}



$sqlCAT = "SELECT DISTINCT
SQL_CALC_FOUND_ROWS categorias_ganado.id,
categorias_ganado.id AS cateidselect,
categorias_ganado.nombre AS catenomselect,
categorias_ganado.descripcion,
lotes.categoria,
lotes.venta_tipo,
lotes.remate_id
FROM
categorias_ganado
Inner Join lotes ON lotes.categoria = categorias_ganado.id
WHERE
lotes.venta_tipo =  'PARTICULAR' AND
categorias_ganado.id IN  ( lotes.categoria)";








$sqlLugar = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.id,
lotes.provincia,
lotes.lugar,
provincias.id AS provid,
provincias.nombre AS provinombre,
localidades.id as locaid,
localidades.nombre as locanombre
FROM
lotes
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
WHERE
lotes.venta_tipo =  'PARTICULAR'
GROUP BY lotes.localidad
ORDER BY provinombre ASC, locanombre ASC";



$rsLugar = mysql_query($sqlLugar);
$cuentaLugar = mysql_num_rows($rsLugar);
$lugarcombo = '';
$countRowlugar = 0;
while ($rowLugar = mysql_fetch_array($rsLugar)) {
    if ($locaId == $rowLugar['locaid']) {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '" selected>' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    } else {
        $lugarcombo = $lugarcombo . '<option value="' . $rowLugar['locaid'] . '">' . utf8_encode($rowLugar['locanombre']) . ', ' . utf8_encode(strtoupper($rowLugar['provinombre'])) . '</option>';
    }
}

$sql = "SELECT
SQL_CALC_FOUND_ROWS lotes.id,
lotes.orden,
lotes.id AS loteid,
lotes.remate_id,
lotes.conjunto_lote_id,
lotes.tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.codigo,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.provincia,
lotes.localidad,
lotes.lugar,
lotes.video,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
localidades.id,
localidades.nombre AS locanombre,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
remates.id AS remaid,
remates.fecha,
remates.tipo,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre,
videos_lotes.id AS vlid,
videos_lotes.remate_id AS vlrid,
videos_lotes.lote_id AS vllid,
videos_lotes.video AS vlv
FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Left Join videos_lotes ON videos_lotes.lote_id = lotes.id
WHERE
lotes.venta_tipo =  'PARTICULAR'
" . $sqlwhere . "

" . $consulorder . "

LIMIT $offset, $limit";

//echo $sql;

$rs = mysql_query($sql);
$sqlTotal = "SELECT FOUND_ROWS() as total";
$rsTotal = mysql_query($sqlTotal);
$rowTotal = mysql_fetch_assoc($rsTotal);
// Total de registros sin limit
$total = $rowTotal["total"];
$totalcount = mysql_num_rows($rs);


$rsCAT = mysql_query($sqlCAT);

$sqlCATTotal = "SELECT FOUND_ROWS() as total";
$rsCATTotal = mysql_query($sqlCATTotal);
$rowCATTotal = mysql_fetch_assoc($rsCATTotal);
// Total de registros sin limit
$totalCAT = $rowCATTotal["total"];



?>


<script type="text/javascript">

    function cambiaFondo(pId, pColor) {
        document.getElementById(pId).style.backgroundColor = pColor;
    }
    var divflecha = '';
    function abreCatalogoNew(pDivqueabre, pVideo, pTitulote, pDetalote1, pDetalote2, pDivflecha) {
        //document.getElementById('todoDiv').style.zIndex=20;
        innerVideo = pVideo;
        embedVideo = '<iframe class="youtube-player" type="text/html" width="100%" height="100%" src="http://www.youtube.com/embed/' + innerVideo + '?rel=0&ps=docs&showinfo=0&controls=1" allowfullscreen frameborder="0"></iframe>';
        if (innerVideo == '') {
            embedVideo = '<img src="images/no-video.jpg" width="500" height="416">';

        }
        if (divflecha != '') {
            jQuery('#' + divflecha).fadeOut(200);
        }
        jQuery('#' + pDivqueabre).fadeOut(200, function () {
            jQuery('#conteVideoylote').css('display', '');
            jQuery('.tdContevideo').html(embedVideo);
            jQuery('.conteTitu').html(pTitulote);
            jQuery('.detaLote1').html(pDetalote1);
            jQuery('.detaLote2').html(pDetalote2);

            jQuery('#' + pDivqueabre).fadeIn(400);
            //jQuery('#' + pDivflecha).fadeIn(400);
            //divflecha = pDivflecha;
        });

        tempAlpha = setInterval('setAlphaUp()', 5);
    }
    var btnloteSel = '';
    function mouseOverout(pDivid, pInst) {
        if (pInst == 'sel') {
            document.getElementById(btnloteSel).style.backgroundColor = "#fefefe";
            btnloteSel = pDivid;
            document.getElementById(pDivid).style.backgroundColor = "#fff0ce";
        } else {
            if (btnloteSel != pDivid) {
                document.getElementById(pDivid).style.backgroundColor = "#fefefe";
            }
        }




    }
    ;

</script>



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>:: UMC S.A. Negocios Agropecuarios ::</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body onload=" abrePrimero();">

<?php
include 'header.php';
?>
    <div style="height:230px;">
        
    </div><!-- BARRA DE BUSQUEDA -->
<div class="container mb">
    <div class="row">
        <div class="col-xs-12">
            <span class="txtTituremates" style="border: none;"><b>LOTES</b><span style="color: #7a7a7a;"> A LA VENTA</span></span>
        </div>
        <div class="col-xs-12" style=" height:10px;"></div>
        <div class="col-xs-12 hidden">
            <b>FILTROS DE B&Uacute;SQUEDA</b>                                
        </div>
        <div class="col-xs-12 filtros">
            <b>UBICACI&Oacute;N DEL LOTE</b>
            <div id="comboLocalidades">
                <select id="lotesLoca" class="select" name="lotesLoca" onchange="cambiaPagFiltro('loca', this.value, '<?php echo $locaId ?>');"
                        <option value="5" <?php echo $locaselected ?>>TODOS</option>
                            <?php echo '<option value="" ' . $locaselected . '>TODOS</option>' . $lugarcombo; ?>
                </select>
            </div>
            <div id="">
                <b>LOTE</b>
                <select id="lotesSel" class="select" name="categorias" onchange="cambiaPagFiltro('lotenum', this.value, '<?php echo $lotesel ?>');">
                    <?php echo "VER" . $innerselLotes ?>
                </select>
            </div>
            <div id="">
                <b>CATEG.</b>
                <select class="select" name="categorias" onchange="cambiaPagFiltro('cate', this.value, '<?php echo $cate ?>', '');">
                    <?php
                    $cuentarowscate = 0;
                    if ($totalCAT > 0) {
                        while ($rowCAT = mysql_fetch_array($rsCAT)) {
                            $cuentarowscate+=1;
                            if ($cuentarowscate == 1) {
                                if ($cate == "") {
                                    ?>
                                    <option value="" selected>TODOS</option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="">TODOS</option>
                                    <?php
                                }
                            }

                            if ($cate == $rowCAT['cateidselect']) {
                                ?>
                                <option value="<?php echo $rowCAT['cateidselect'] ?>" selected ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $rowCAT['cateidselect'] ?>" ><?php echo strtoupper(utf8_encode($rowCAT['catenomselect'])) ?></option>
                                <?php
                            }
                        }
                    } else {
                        ?>
                        <option value="" selected>TODOS</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>TRAZ.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('tr', this.value, '<?php echo $traza ?>', '');">
                    <?php
                    if ($traza == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($traza == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($traza == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>GARPTA.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('gr', this.value, '<?php echo $garpta ?>', '');">
                    <?php
                    if ($garpta == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($garpta == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($garpta == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div id="">
                <b>MIO-MIO.</b>
                <select class="select" name="colores" onchange="cambiaPagFiltro('mm', this.value, '<?php echo $miomio ?>', '');">
                    <?php
                    if ($miomio == '') {
                        ?>
                        <option value="" selected>TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }

                    if ($miomio == 'NO') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI">SI</option>
                        <option value="NO" selected>NO</option>
                        <?php
                    }
                    if ($miomio == 'SI') {
                        ?>
                        <option value="">TODOS</option>
                        <option value="SI"selected>SI</option>
                        <option value="NO">NO</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>



      <!-- HEADER CON LINKS VIDEOS -->

























<!-- CONTENIDO CUADRITOS -->
<div class="container">
    <div class="row">
        <?php
        $cuentatres = 0;
        $cuentatotal = 0;
        // echo 'VERRRRRRRRRRRR'.$totalcount;
        while ($row = mysql_fetch_assoc($rs)) {
            $rematitu = utf8_encode($row['rematitu']);
            $id = $row["id"];
            $lid = $row["loteid"];
            $name = utf8_encode($row["orden"]);
            $cuentatres+=1;
            $cuentatotal+=1;
            $detalote1 = 'Cantidad: <b>' . utf8_encode($row['cantidad']) . ' vacunos</b><br>Tipo: <b>' . utf8_encode($row['tiponombre']) . '</b><br>Categor&iacute;a: <b>' . utf8_encode($row['catenombre']) . '</b><br>Peso.: <b>' . utf8_encode($row['peso']) . ' Kgs.</b><br>Localidad: <b>' . utf8_encode($row['locanombre']);
            $detalote2 = '</b>Provincia: <b>' . utf8_encode($row['provinombre']) . '</b><br>Trazabilidad: <b>' . $row['trazabilidad'] . '</b><br>Garrapata: <b>' . $row['garrapata'] . '</b><br>MIO-MIO: <b>' . $row['mio_mio'] . '</b><br>Caracter&iacute;sticas: <b>' . utf8_encode($row['caracteristicas']) . '</b>';
            //$detalote= "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">";


            $orden = $row['orden'];
            if ($orden != '') {
                if (substr($orden, 0, 2) == "00") {
                    $orden = str_replace("00", "", $orden);
                    if (substr($orden, 0, 1) == "0") {
                        $orden = str_replace("0", "", $orden);
                    }
                } else {
                    if (substr($orden, 0, 1) == "0") {
                        $orden = substr_replace($orden, "", 0, 1);
                    }
                }
            }
            if ($row['orden'] == '') {
                $displayCode = '';
                $displayOrden = 'none';
                $tituLote = 'Certif. Nº' . $row['codigo'] . ' | ' . utf8_encode($row['titulo']);
            } else {
                $displayCode = 'none';
                $displayOrden = '';
                $tituLote = 'LOTE Nº ' . $orden . ' | ' . utf8_encode($row['titulo']);
            }

            if ($row['vlv']) {
                $cadena = $row['vlv'];
            } else {
                $cadena = $row['video'];
            }
            $maximo = strlen($cadena);
            if ($maximo > 3) {
                $cadena_comienzo = "v=";
                $totalstr = strpos($cadena, $cadena_comienzo);
                $varURLYT = substr($cadena, ($totalstr + 2), strlen($cadena));
            } else {
                $varURLYT = '';
            }
            if ($cuentatotal == 1) {
                $fdobtnlote = '#fff0ce';
        ?>

        <div class="col-xs-6 col-sm-3 primero">
            <table cellpadding="0" cellspacing="0" border="0" style="width:250px; height: 1px; margin: 0 auto;">
                    <script type="text/javascript">
                        function abrePrimero() {
                            abreCatalogoNew('catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>', 'divflecha<?php echo $row['loteid'] ?>');
                            btnloteSel = 'divlote<?php echo $lid ?>';
                        }
                    </script>
                    <?php
                        } else {
                            $fdobtnlote = '#fefefe';
                        }
                    ?>
                <tr>
                        <?php
                    if($cuentatotal > 6){
                        ?>
                        <!-- <td style="width: 16px;">
                            <div id="divflecha<?php echo $row['loteid'] ?>" style="display:none; width: 16px;"><img src="images/flecha-lote-der.jpg" id="imgflecha"></div>
                        </td> -->
                        <?php
                    }
                    ?>
                    <td width="250" height="80" border="0" style="cursor:pointer; background-color: #fff;" onclick="abreCatalogoNew('catalogDiv', '<?php echo $varURLYT ?>', '<?php echo $tituLote ?>', '<?php echo $detalote1 ?>', '<?php echo $detalote2 ?>', 'divflecha<?php echo $row['loteid'] ?>');">
                        <div id="divlote<?php echo $lid ?>" style="border:1px solid #e4e4e4;width:250px; height: 80px; background-color:<?php echo $fdobtnlote ?>" onmouseover="mouseOverout(this.id, 'on')" onmouseout="mouseOverout(this.id, 'off')" onclick="mouseOverout(this.id, 'sel')">
                            <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                <tr>
                                    <td width="1"></td>
                                    <td width="100" valign="top">
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 1px;">
                                            <tr style="display:none;">
                                                <td class="txtCodilote" height="1" valign="top" style="display:<?php echo $displayCode ?>;">
                                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 1px;">
                                                        <tr>
                                                            <td height="8"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2" style="font-size:11pt; color: #383838;">Certif.</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txtCodilote" valign="top" height="35" style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff; font-size:17pt;"><?php echo $row['codigo'] ?></td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="display:">
                                                <td height="1" valign="middle">
                                                    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                                        <tr>
                                                            <td height="8"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2" style="font-size:11pt; color: #383838;">LOTE</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="txtNumelote" style="text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;" height="1" valign="middle"><?php echo $orden ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="1" align="center" class="txtLotecertif2">CERT: <?php echo $row['codigo'] ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="1"></td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 100%;">
                                            <tr>
                                                <td height="6"></td>
                                            </tr>
                                            <tr>
                                                <td height="1" class="txtTextolote"><?php echo $row['cantidad'] ?> cabezas</td>
                                            </tr>
                                            <?php
                                            if ($row['peso'] == '-' || $row['peso'] == '' || $row['peso'] == 'S / P') {
                                                $pesolote = '';
                                            } else {
                                                $pesolote = '<br>' . utf8_encode($row['peso'] . ' Kgs.');
                                            }
                                            ?>
                                            <tr>
                                                <td class="txtLotecate" valign="top"><?php echo utf8_encode($row['catenombre']) ?><?php echo $pesolote ?><br><?php echo utf8_encode($row['locanombre']) ?>, <?php echo utf8_encode($row['provinombre']) ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div type="button" id="divlote<?php echo $lid ?>" class="btn btn-primary visible-xs triggerv" data-toggle="modal" data-target="#verVid" onclick="mouseOverout(this.id, 'sel')"></div>

                        <!-- <div id="divi<?php echo $lid ?>" style="width: 100%;position: absolute;left: 0;">
                            <div id="catalogDiv" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                                <div id="conteVideoylote" style="display:;">
                                    <?php include 'videoinclude_n.php' ?>
                                </div>
                            </div>
                        </div> -->
                    </td>





                    <?php
                    if($cuentatotal <= 6){
                    ?>
                    <!-- <td style="width: 16px;" style="border: solid 0px;">
                        <div id="divflecha<?php echo $row['loteid'] ?>" style="display: none;">
                            <img src="images/flecha-lote.jpg" id="imgflecha">
                        </div>
                    </td> -->
                    <?php
                    }
                    ?>
                </tr>

                <?php
                if(($cuentatotal<6) || ($cuentatotal>6 and ($cuentatotal<12|| $cuentatotal< $totalcount))){
                    ?>
                <tr>
                    <td style="height: 6px;"></td>
                </tr>
                <?php
                }
                $midLotes=6;
                if ($totalcount<6){

                    $midLotes=$totalcount;
                }
                if($cuentatotal == $midLotes || $cuentatotal == $totalcount){
                ?>

            </table>
        </div>


        <?php
        if($cuentatotal == $midLotes){
        ?>

        
        <div class="hidden-xs col-sm-6 segundo" style="border: solid 0px;">
            <div id="catalogDiv" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                <div id="conteVideoylote" style="display:;">
                    <?php include 'videoinclude_n.php' ?>
                </div>
            </div>
        </div>


        <div class="col-xs-6 col-sm-3 tercero">
            <table cellpadding="0" cellspacing="0" style="width: 250px; height: 100%; margin: 0 auto;">
                <?php
                    }
                }
                if ($cuentatotal == 12 || $cuentatotal == $totalcount){
                ?>

            </table>
        </div>
        <?php
        }
        }
        ?>



    </div>
</div>
<!-- FIN LOTES -->












<div class="modal fade product_view" id="verVid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                <h3 class="modal-title conteTitu"></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 product_img">

                            <div id="catalogDiv2" style="top:0; z-index:30; text-align:center; background-color: #000ggg">
                                <div class="conteVideoylote2" style="display:;">
                                    <?php include 'videoinclude_n.php' ?>
                                </div>
                            </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>













                                                        <!-- PAGINADO -->

                                                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; height: 20px;">
                                                                    <tr>
                                                                        <?php
                                                                        $totalPag = ceil($total / $limit);
                                                                        ?>
                                                                    <script type="text/javascript">

                                                                        function utf8_encode(argString) {
                                                                            // Encodes an ISO-8859-1 string to UTF-8
                                                                            //
                                                                            // version: 1109.2015
                                                                            // discuss at: http://phpjs.org/functions/utf8_encode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // +   improved by: sowberry
                                                                            // +    tweaked by: Jack
                                                                            // +   bugfixed by: Onno Marsman    // +   improved by: Yves Sucaet
                                                                            // +   bugfixed by: Onno Marsman
                                                                            // +   bugfixed by: Ulrich
                                                                            // +   bugfixed by: Rafal Kukawski
                                                                            // *     example 1: utf8_encode('Kevin van Zonneveld');    // *     returns 1: 'Kevin van Zonneveld'
                                                                            if (argString === null || typeof argString === "undefined") {
                                                                                return "";
                                                                            }
                                                                            var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
                                                                            var utftext = "",
                                                                                    start, end, stringl = 0;

                                                                            start = end = 0;
                                                                            stringl = string.length;
                                                                            for (var n = 0; n < stringl; n++) {
                                                                                var c1 = string.charCodeAt(n);
                                                                                var enc = null;
                                                                                if (c1 < 128) {
                                                                                    end++;
                                                                                } else if (c1 > 127 && c1 < 2048) {
                                                                                    enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
                                                                                } else {
                                                                                    enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
                                                                                }
                                                                                if (enc !== null) {
                                                                                    if (end > start) {
                                                                                        utftext += string.slice(start, end);
                                                                                    }
                                                                                    utftext += enc;
                                                                                    start = end = n + 1;
                                                                                }
                                                                            }
                                                                            if (end > start) {
                                                                                utftext += string.slice(start, stringl);
                                                                            }
                                                                            return utftext;
                                                                        }


                                                                        function utf8_decode(str_data) {
                                                                            // Converts a UTF-8 encoded string to ISO-8859-1
                                                                            //
                                                                            // version: 1109.2015
                                                                            // discuss at: http://phpjs.org/functions/utf8_decode    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
                                                                            // +      input by: Aman Gupta
                                                                            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // +   improved by: Norman "zEh" Fuchs
                                                                            // +   bugfixed by: hitwork    // +   bugfixed by: Onno Marsman
                                                                            // +      input by: Brett Zamir (http://brett-zamir.me)
                                                                            // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                                                                            // *     example 1: utf8_decode('Kevin van Zonneveld');
                                                                            // *     returns 1: 'Kevin van Zonneveld'
                                                                            var tmp_arr = [],
                                                                                    i = 0,
                                                                                    ac = 0,
                                                                                    c1 = 0,
                                                                                    c2 = 0, c3 = 0;

                                                                            str_data += '';

                                                                            while (i < str_data.length) {
                                                                                c1 = str_data.charCodeAt(i);
                                                                                if (c1 < 128) {
                                                                                    tmp_arr[ac++] = String.fromCharCode(c1);
                                                                                    i++;
                                                                                } else if (c1 > 191 && c1 < 224) {
                                                                                    c2 = str_data.charCodeAt(i + 1);
                                                                                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                                                                                    i += 2;
                                                                                } else {
                                                                                    c2 = str_data.charCodeAt(i + 1);
                                                                                    c3 = str_data.charCodeAt(i + 2);
                                                                                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                                                                                    i += 3;
                                                                                }
                                                                            }
                                                                            return tmp_arr.join('');
                                                                        }


                                                                        function urlencode(str) {
                                                                            str = escape(str);
                                                                            str = str.replace('+', '%2B');
                                                                            str = str.replace('%20', '+');
                                                                            str = str.replace('*', '%2A');
                                                                            str = str.replace('/', '%2F');
                                                                            str = str.replace('@', '%40');
                                                                            return str;
                                                                        }

                                                                        function urldecode(str) {
                                                                            str = str.replace('+', ' ');
                                                                            str = unescape(str);
                                                                            return str;
                                                                        }

                                                                        pagSig = <?php echo $pag ?> + 1
                                                                        pagAnt = <?php echo $pag ?> - 1
                                                                        function cambiaPag(pPag) {
                                                                            if (pPag > <?php echo $totalPag ?> || pPag < 1) {

                                                                            } else {
                                                                                url = '<?php echo $miURL ?>';
                                                                                url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=' + pPag;
                                                                                window.location = url;

                                                                            }

                                                                        }



                                                                        function cambiaPagFiltro(pPar, pFiltro, pVar, pRemaid) {
                                                                            if (pPar == 'lotenum') {
                                                                                if (pFiltro == '') {
                                                                                    url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid;
                                                                                } else {
                                                                                    url = '<?php echo $miURLsinP ?>?remaid=' + pRemaid + '&lotenum=' + pFiltro;
                                                                                }
                                                                            } else {
                                                                                url = '<?php echo $miURL ?>';
                                                                                url = url.replace('&pag=<?php echo $pag ?>', '') + '&pag=1';
                                                                                url = url.replace('&lotenum=<?php echo $lotesel ?>', '');
                                                                                if (pFiltro == '') {
                                                                                    url = url.replace('&' + pPar + '=' + pVar, '');
                                                                                } else {
                                                                                    url = url.replace('&' + pPar + '=' + pVar, '') + '&' + pPar + '=' + pFiltro;
                                                                                }
                                                                            }

                                                                            window.location = url;
                                                                        }
                                                                        function enviar_formulario() {
                                                                            document.selUbic.submit();
                                                                        }

                                                                    </script>
                                                                    <td colspan="3" bgcolor="#ffffff">
                                                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" height="20">
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                                <td width="1"><img src="images/ico-pag-ret.jpg" style="cursor:pointer;" onclick="cambiaPag(pagAnt)"></td>
                                                                                <td width="10">&nbsp;</td>
                                                                                <td width="1" class="txtNumepagiOff">&nbsp;&nbsp;</td>
                                                                                <?php
                                                                                $links = array();
                                                                                for ($i = 1; $i <= $totalPag; $i++) {
                                                                                    if ($pag == $i) {
                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagi\" valign=\"middle\" bgcolor=\"#b44422;\">" . $i . "</td>";
                                                                                    } else {
                                                                                        $links[] = "<td height=\"20\" width=\"20\" class=\"txtNumepagiOff\" valign=\"middle\" style=\"cursor:pointer;\" onclick=\"cambiaPag(" . $i . ")\"><a href=\"javascript:cambiaPag(" . $i . ")\">" . $i . "</a></td>";
                                                                                    }
                                                                                }
                                                                                echo implode("<td width=\"1\"></td>", $links);
                                                                                ?>
                                                                                <td width="10">&nbsp;</td>
                                                                                <td width="1"><img src="images/ico-pag-av.jpg" style="cursor:pointer;" onclick="cambiaPag(pagSig)"></td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!-- FIN PAGINADO -->
                                                    </table>




                    </div>
                    <!--fin contenido remates-->



<div style="height: 60px;"></div>
<div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
        <?php include 'footer.php'?>
    </div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</html>
