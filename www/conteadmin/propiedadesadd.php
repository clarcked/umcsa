<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "propiedadesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$propiedades_add = NULL; // Initialize page object first

class cpropiedades_add extends cpropiedades {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{1B8CF9BA-91E4-4950-9047-342B30417538}";

	// Table name
	var $TableName = 'propiedades';

	// Page object name
	var $PageObjName = 'propiedades_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (propiedades)
		if (!isset($GLOBALS["propiedades"]) || get_class($GLOBALS["propiedades"]) == "cpropiedades") {
			$GLOBALS["propiedades"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["propiedades"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'propiedades', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("propiedadeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->prov_id->SetVisibility();
		$this->loca_id->SetVisibility();
		$this->titulo->SetVisibility();
		$this->tipo_id->SetVisibility();
		$this->actividad->SetVisibility();
		$this->oper_tipo->SetVisibility();
		$this->descripcion->SetVisibility();
		$this->img_1->SetVisibility();
		$this->img_2->SetVisibility();
		$this->img_3->SetVisibility();
		$this->img_4->SetVisibility();
		$this->img_5->SetVisibility();
		$this->img_6->SetVisibility();
		$this->img_7->SetVisibility();
		$this->img_8->SetVisibility();
		$this->img_9->SetVisibility();
		$this->img_10->SetVisibility();
		$this->video_servidor->SetVisibility();
		$this->video->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $propiedades;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($propiedades);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $IsModal = FALSE;
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["id"] != "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		} else {
			if ($this->CurrentAction == "I") // Load default values for blank record
				$this->LoadDefaultValues();
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("propiedadeslist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "propiedadeslist.php")
						$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
					elseif (ew_GetPageName($sReturnUrl) == "propiedadesview.php")
						$sReturnUrl = $this->GetViewUrl(); // View page, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->img_1->Upload->Index = $objForm->Index;
		$this->img_1->Upload->UploadFile();
		$this->img_1->CurrentValue = $this->img_1->Upload->FileName;
		$this->img_2->Upload->Index = $objForm->Index;
		$this->img_2->Upload->UploadFile();
		$this->img_2->CurrentValue = $this->img_2->Upload->FileName;
		$this->img_3->Upload->Index = $objForm->Index;
		$this->img_3->Upload->UploadFile();
		$this->img_3->CurrentValue = $this->img_3->Upload->FileName;
		$this->img_4->Upload->Index = $objForm->Index;
		$this->img_4->Upload->UploadFile();
		$this->img_4->CurrentValue = $this->img_4->Upload->FileName;
		$this->img_5->Upload->Index = $objForm->Index;
		$this->img_5->Upload->UploadFile();
		$this->img_5->CurrentValue = $this->img_5->Upload->FileName;
		$this->img_6->Upload->Index = $objForm->Index;
		$this->img_6->Upload->UploadFile();
		$this->img_6->CurrentValue = $this->img_6->Upload->FileName;
		$this->img_7->Upload->Index = $objForm->Index;
		$this->img_7->Upload->UploadFile();
		$this->img_7->CurrentValue = $this->img_7->Upload->FileName;
		$this->img_8->Upload->Index = $objForm->Index;
		$this->img_8->Upload->UploadFile();
		$this->img_8->CurrentValue = $this->img_8->Upload->FileName;
		$this->img_9->Upload->Index = $objForm->Index;
		$this->img_9->Upload->UploadFile();
		$this->img_9->CurrentValue = $this->img_9->Upload->FileName;
		$this->img_10->Upload->Index = $objForm->Index;
		$this->img_10->Upload->UploadFile();
		$this->img_10->CurrentValue = $this->img_10->Upload->FileName;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->prov_id->CurrentValue = NULL;
		$this->prov_id->OldValue = $this->prov_id->CurrentValue;
		$this->loca_id->CurrentValue = NULL;
		$this->loca_id->OldValue = $this->loca_id->CurrentValue;
		$this->titulo->CurrentValue = NULL;
		$this->titulo->OldValue = $this->titulo->CurrentValue;
		$this->tipo_id->CurrentValue = NULL;
		$this->tipo_id->OldValue = $this->tipo_id->CurrentValue;
		$this->actividad->CurrentValue = "GANADERO";
		$this->oper_tipo->CurrentValue = "VENTA";
		$this->descripcion->CurrentValue = NULL;
		$this->descripcion->OldValue = $this->descripcion->CurrentValue;
		$this->img_1->Upload->DbValue = NULL;
		$this->img_1->OldValue = $this->img_1->Upload->DbValue;
		$this->img_1->CurrentValue = NULL; // Clear file related field
		$this->img_2->Upload->DbValue = NULL;
		$this->img_2->OldValue = $this->img_2->Upload->DbValue;
		$this->img_2->CurrentValue = NULL; // Clear file related field
		$this->img_3->Upload->DbValue = NULL;
		$this->img_3->OldValue = $this->img_3->Upload->DbValue;
		$this->img_3->CurrentValue = NULL; // Clear file related field
		$this->img_4->Upload->DbValue = NULL;
		$this->img_4->OldValue = $this->img_4->Upload->DbValue;
		$this->img_4->CurrentValue = NULL; // Clear file related field
		$this->img_5->Upload->DbValue = NULL;
		$this->img_5->OldValue = $this->img_5->Upload->DbValue;
		$this->img_5->CurrentValue = NULL; // Clear file related field
		$this->img_6->Upload->DbValue = NULL;
		$this->img_6->OldValue = $this->img_6->Upload->DbValue;
		$this->img_6->CurrentValue = NULL; // Clear file related field
		$this->img_7->Upload->DbValue = NULL;
		$this->img_7->OldValue = $this->img_7->Upload->DbValue;
		$this->img_7->CurrentValue = NULL; // Clear file related field
		$this->img_8->Upload->DbValue = NULL;
		$this->img_8->OldValue = $this->img_8->Upload->DbValue;
		$this->img_8->CurrentValue = NULL; // Clear file related field
		$this->img_9->Upload->DbValue = NULL;
		$this->img_9->OldValue = $this->img_9->Upload->DbValue;
		$this->img_9->CurrentValue = NULL; // Clear file related field
		$this->img_10->Upload->DbValue = NULL;
		$this->img_10->OldValue = $this->img_10->Upload->DbValue;
		$this->img_10->CurrentValue = NULL; // Clear file related field
		$this->video_servidor->CurrentValue = "YOUTUBE";
		$this->video->CurrentValue = NULL;
		$this->video->OldValue = $this->video->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->prov_id->FldIsDetailKey) {
			$this->prov_id->setFormValue($objForm->GetValue("x_prov_id"));
		}
		if (!$this->loca_id->FldIsDetailKey) {
			$this->loca_id->setFormValue($objForm->GetValue("x_loca_id"));
		}
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		if (!$this->tipo_id->FldIsDetailKey) {
			$this->tipo_id->setFormValue($objForm->GetValue("x_tipo_id"));
		}
		if (!$this->actividad->FldIsDetailKey) {
			$this->actividad->setFormValue($objForm->GetValue("x_actividad"));
		}
		if (!$this->oper_tipo->FldIsDetailKey) {
			$this->oper_tipo->setFormValue($objForm->GetValue("x_oper_tipo"));
		}
		if (!$this->descripcion->FldIsDetailKey) {
			$this->descripcion->setFormValue($objForm->GetValue("x_descripcion"));
		}
		if (!$this->video_servidor->FldIsDetailKey) {
			$this->video_servidor->setFormValue($objForm->GetValue("x_video_servidor"));
		}
		if (!$this->video->FldIsDetailKey) {
			$this->video->setFormValue($objForm->GetValue("x_video"));
		}
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->prov_id->CurrentValue = $this->prov_id->FormValue;
		$this->loca_id->CurrentValue = $this->loca_id->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->tipo_id->CurrentValue = $this->tipo_id->FormValue;
		$this->actividad->CurrentValue = $this->actividad->FormValue;
		$this->oper_tipo->CurrentValue = $this->oper_tipo->FormValue;
		$this->descripcion->CurrentValue = $this->descripcion->FormValue;
		$this->video_servidor->CurrentValue = $this->video_servidor->FormValue;
		$this->video->CurrentValue = $this->video->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->prov_id->setDbValue($rs->fields('prov_id'));
		if (array_key_exists('EV__prov_id', $rs->fields)) {
			$this->prov_id->VirtualValue = $rs->fields('EV__prov_id'); // Set up virtual field value
		} else {
			$this->prov_id->VirtualValue = ""; // Clear value
		}
		$this->loca_id->setDbValue($rs->fields('loca_id'));
		if (array_key_exists('EV__loca_id', $rs->fields)) {
			$this->loca_id->VirtualValue = $rs->fields('EV__loca_id'); // Set up virtual field value
		} else {
			$this->loca_id->VirtualValue = ""; // Clear value
		}
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->tipo_id->setDbValue($rs->fields('tipo_id'));
		$this->actividad->setDbValue($rs->fields('actividad'));
		$this->oper_tipo->setDbValue($rs->fields('oper_tipo'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->img_1->Upload->DbValue = $rs->fields('img_1');
		$this->img_1->CurrentValue = $this->img_1->Upload->DbValue;
		$this->img_2->Upload->DbValue = $rs->fields('img_2');
		$this->img_2->CurrentValue = $this->img_2->Upload->DbValue;
		$this->img_3->Upload->DbValue = $rs->fields('img_3');
		$this->img_3->CurrentValue = $this->img_3->Upload->DbValue;
		$this->img_4->Upload->DbValue = $rs->fields('img_4');
		$this->img_4->CurrentValue = $this->img_4->Upload->DbValue;
		$this->img_5->Upload->DbValue = $rs->fields('img_5');
		$this->img_5->CurrentValue = $this->img_5->Upload->DbValue;
		$this->img_6->Upload->DbValue = $rs->fields('img_6');
		$this->img_6->CurrentValue = $this->img_6->Upload->DbValue;
		$this->img_7->Upload->DbValue = $rs->fields('img_7');
		$this->img_7->CurrentValue = $this->img_7->Upload->DbValue;
		$this->img_8->Upload->DbValue = $rs->fields('img_8');
		$this->img_8->CurrentValue = $this->img_8->Upload->DbValue;
		$this->img_9->Upload->DbValue = $rs->fields('img_9');
		$this->img_9->CurrentValue = $this->img_9->Upload->DbValue;
		$this->img_10->Upload->DbValue = $rs->fields('img_10');
		$this->img_10->CurrentValue = $this->img_10->Upload->DbValue;
		$this->video_servidor->setDbValue($rs->fields('video_servidor'));
		$this->video->setDbValue($rs->fields('video'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->prov_id->DbValue = $row['prov_id'];
		$this->loca_id->DbValue = $row['loca_id'];
		$this->titulo->DbValue = $row['titulo'];
		$this->tipo_id->DbValue = $row['tipo_id'];
		$this->actividad->DbValue = $row['actividad'];
		$this->oper_tipo->DbValue = $row['oper_tipo'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->img_1->Upload->DbValue = $row['img_1'];
		$this->img_2->Upload->DbValue = $row['img_2'];
		$this->img_3->Upload->DbValue = $row['img_3'];
		$this->img_4->Upload->DbValue = $row['img_4'];
		$this->img_5->Upload->DbValue = $row['img_5'];
		$this->img_6->Upload->DbValue = $row['img_6'];
		$this->img_7->Upload->DbValue = $row['img_7'];
		$this->img_8->Upload->DbValue = $row['img_8'];
		$this->img_9->Upload->DbValue = $row['img_9'];
		$this->img_10->Upload->DbValue = $row['img_10'];
		$this->video_servidor->DbValue = $row['video_servidor'];
		$this->video->DbValue = $row['video'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// prov_id
		// loca_id
		// titulo
		// tipo_id
		// actividad
		// oper_tipo
		// descripcion
		// img_1
		// img_2
		// img_3
		// img_4
		// img_5
		// img_6
		// img_7
		// img_8
		// img_9
		// img_10
		// video_servidor
		// video

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		if ($this->prov_id->VirtualValue <> "") {
			$this->prov_id->ViewValue = $this->prov_id->VirtualValue;
		} else {
		if (strval($this->prov_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->prov_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prov_id->ViewValue = $this->prov_id->CurrentValue;
			}
		} else {
			$this->prov_id->ViewValue = NULL;
		}
		}
		$this->prov_id->ViewCustomAttributes = "";

		// loca_id
		if ($this->loca_id->VirtualValue <> "") {
			$this->loca_id->ViewValue = $this->loca_id->VirtualValue;
		} else {
		if (strval($this->loca_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->loca_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->loca_id->ViewValue = $this->loca_id->CurrentValue;
			}
		} else {
			$this->loca_id->ViewValue = NULL;
		}
		}
		$this->loca_id->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// tipo_id
		$this->tipo_id->ViewValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->ViewCustomAttributes = "";

		// actividad
		if (strval($this->actividad->CurrentValue) <> "") {
			$this->actividad->ViewValue = $this->actividad->OptionCaption($this->actividad->CurrentValue);
		} else {
			$this->actividad->ViewValue = NULL;
		}
		$this->actividad->ViewCustomAttributes = "";

		// oper_tipo
		if (strval($this->oper_tipo->CurrentValue) <> "") {
			$this->oper_tipo->ViewValue = $this->oper_tipo->OptionCaption($this->oper_tipo->CurrentValue);
		} else {
			$this->oper_tipo->ViewValue = NULL;
		}
		$this->oper_tipo->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// img_1
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->ViewValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->ViewValue = "";
		}
		$this->img_1->ViewCustomAttributes = "";

		// img_2
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->ViewValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->ViewValue = "";
		}
		$this->img_2->ViewCustomAttributes = "";

		// img_3
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->ViewValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->ViewValue = "";
		}
		$this->img_3->ViewCustomAttributes = "";

		// img_4
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->ViewValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->ViewValue = "";
		}
		$this->img_4->ViewCustomAttributes = "";

		// img_5
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->ViewValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->ViewValue = "";
		}
		$this->img_5->ViewCustomAttributes = "";

		// img_6
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->ViewValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->ViewValue = "";
		}
		$this->img_6->ViewCustomAttributes = "";

		// img_7
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->ViewValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->ViewValue = "";
		}
		$this->img_7->ViewCustomAttributes = "";

		// img_8
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->ViewValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->ViewValue = "";
		}
		$this->img_8->ViewCustomAttributes = "";

		// img_9
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->ViewValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->ViewValue = "";
		}
		$this->img_9->ViewCustomAttributes = "";

		// img_10
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->ViewValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->ViewValue = "";
		}
		$this->img_10->ViewCustomAttributes = "";

		// video_servidor
		if (strval($this->video_servidor->CurrentValue) <> "") {
			$this->video_servidor->ViewValue = $this->video_servidor->OptionCaption($this->video_servidor->CurrentValue);
		} else {
			$this->video_servidor->ViewValue = NULL;
		}
		$this->video_servidor->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

			// prov_id
			$this->prov_id->LinkCustomAttributes = "";
			$this->prov_id->HrefValue = "";
			$this->prov_id->TooltipValue = "";

			// loca_id
			$this->loca_id->LinkCustomAttributes = "";
			$this->loca_id->HrefValue = "";
			$this->loca_id->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// tipo_id
			$this->tipo_id->LinkCustomAttributes = "";
			$this->tipo_id->HrefValue = "";
			$this->tipo_id->TooltipValue = "";

			// actividad
			$this->actividad->LinkCustomAttributes = "";
			$this->actividad->HrefValue = "";
			$this->actividad->TooltipValue = "";

			// oper_tipo
			$this->oper_tipo->LinkCustomAttributes = "";
			$this->oper_tipo->HrefValue = "";
			$this->oper_tipo->TooltipValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";
			$this->descripcion->TooltipValue = "";

			// img_1
			$this->img_1->LinkCustomAttributes = "";
			$this->img_1->HrefValue = "";
			$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;
			$this->img_1->TooltipValue = "";

			// img_2
			$this->img_2->LinkCustomAttributes = "";
			$this->img_2->HrefValue = "";
			$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;
			$this->img_2->TooltipValue = "";

			// img_3
			$this->img_3->LinkCustomAttributes = "";
			$this->img_3->HrefValue = "";
			$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;
			$this->img_3->TooltipValue = "";

			// img_4
			$this->img_4->LinkCustomAttributes = "";
			$this->img_4->HrefValue = "";
			$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;
			$this->img_4->TooltipValue = "";

			// img_5
			$this->img_5->LinkCustomAttributes = "";
			$this->img_5->HrefValue = "";
			$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;
			$this->img_5->TooltipValue = "";

			// img_6
			$this->img_6->LinkCustomAttributes = "";
			$this->img_6->HrefValue = "";
			$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;
			$this->img_6->TooltipValue = "";

			// img_7
			$this->img_7->LinkCustomAttributes = "";
			$this->img_7->HrefValue = "";
			$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;
			$this->img_7->TooltipValue = "";

			// img_8
			$this->img_8->LinkCustomAttributes = "";
			$this->img_8->HrefValue = "";
			$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;
			$this->img_8->TooltipValue = "";

			// img_9
			$this->img_9->LinkCustomAttributes = "";
			$this->img_9->HrefValue = "";
			$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;
			$this->img_9->TooltipValue = "";

			// img_10
			$this->img_10->LinkCustomAttributes = "";
			$this->img_10->HrefValue = "";
			$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;
			$this->img_10->TooltipValue = "";

			// video_servidor
			$this->video_servidor->LinkCustomAttributes = "";
			$this->video_servidor->HrefValue = "";
			$this->video_servidor->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// prov_id
			$this->prov_id->EditCustomAttributes = "";
			if (trim(strval($this->prov_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->prov_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
			} else {
				$this->prov_id->ViewValue = $Language->Phrase("PleaseSelect");
			}
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->prov_id->EditValue = $arwrk;

			// loca_id
			$this->loca_id->EditCustomAttributes = "";
			if (trim(strval($this->loca_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `provincia_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `localidades`";
			$sWhereWrk = "";
			$this->loca_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
			} else {
				$this->loca_id->ViewValue = $Language->Phrase("PleaseSelect");
			}
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->loca_id->EditValue = $arwrk;

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// tipo_id
			$this->tipo_id->EditAttrs["class"] = "form-control";
			$this->tipo_id->EditCustomAttributes = "";
			$this->tipo_id->EditValue = ew_HtmlEncode($this->tipo_id->CurrentValue);
			$this->tipo_id->PlaceHolder = ew_RemoveHtml($this->tipo_id->FldCaption());

			// actividad
			$this->actividad->EditCustomAttributes = "";
			$this->actividad->EditValue = $this->actividad->Options(FALSE);

			// oper_tipo
			$this->oper_tipo->EditCustomAttributes = "";
			$this->oper_tipo->EditValue = $this->oper_tipo->Options(FALSE);

			// descripcion
			$this->descripcion->EditAttrs["class"] = "form-control";
			$this->descripcion->EditCustomAttributes = "";
			$this->descripcion->EditValue = ew_HtmlEncode($this->descripcion->CurrentValue);
			$this->descripcion->PlaceHolder = ew_RemoveHtml($this->descripcion->FldCaption());

			// img_1
			$this->img_1->EditAttrs["class"] = "form-control";
			$this->img_1->EditCustomAttributes = "";
			if (!ew_Empty($this->img_1->Upload->DbValue)) {
				$this->img_1->EditValue = $this->img_1->Upload->DbValue;
			} else {
				$this->img_1->EditValue = "";
			}
			if (!ew_Empty($this->img_1->CurrentValue))
				$this->img_1->Upload->FileName = $this->img_1->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_1);

			// img_2
			$this->img_2->EditAttrs["class"] = "form-control";
			$this->img_2->EditCustomAttributes = "";
			if (!ew_Empty($this->img_2->Upload->DbValue)) {
				$this->img_2->EditValue = $this->img_2->Upload->DbValue;
			} else {
				$this->img_2->EditValue = "";
			}
			if (!ew_Empty($this->img_2->CurrentValue))
				$this->img_2->Upload->FileName = $this->img_2->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_2);

			// img_3
			$this->img_3->EditAttrs["class"] = "form-control";
			$this->img_3->EditCustomAttributes = "";
			if (!ew_Empty($this->img_3->Upload->DbValue)) {
				$this->img_3->EditValue = $this->img_3->Upload->DbValue;
			} else {
				$this->img_3->EditValue = "";
			}
			if (!ew_Empty($this->img_3->CurrentValue))
				$this->img_3->Upload->FileName = $this->img_3->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_3);

			// img_4
			$this->img_4->EditAttrs["class"] = "form-control";
			$this->img_4->EditCustomAttributes = "";
			if (!ew_Empty($this->img_4->Upload->DbValue)) {
				$this->img_4->EditValue = $this->img_4->Upload->DbValue;
			} else {
				$this->img_4->EditValue = "";
			}
			if (!ew_Empty($this->img_4->CurrentValue))
				$this->img_4->Upload->FileName = $this->img_4->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_4);

			// img_5
			$this->img_5->EditAttrs["class"] = "form-control";
			$this->img_5->EditCustomAttributes = "";
			if (!ew_Empty($this->img_5->Upload->DbValue)) {
				$this->img_5->EditValue = $this->img_5->Upload->DbValue;
			} else {
				$this->img_5->EditValue = "";
			}
			if (!ew_Empty($this->img_5->CurrentValue))
				$this->img_5->Upload->FileName = $this->img_5->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_5);

			// img_6
			$this->img_6->EditAttrs["class"] = "form-control";
			$this->img_6->EditCustomAttributes = "";
			if (!ew_Empty($this->img_6->Upload->DbValue)) {
				$this->img_6->EditValue = $this->img_6->Upload->DbValue;
			} else {
				$this->img_6->EditValue = "";
			}
			if (!ew_Empty($this->img_6->CurrentValue))
				$this->img_6->Upload->FileName = $this->img_6->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_6);

			// img_7
			$this->img_7->EditAttrs["class"] = "form-control";
			$this->img_7->EditCustomAttributes = "";
			if (!ew_Empty($this->img_7->Upload->DbValue)) {
				$this->img_7->EditValue = $this->img_7->Upload->DbValue;
			} else {
				$this->img_7->EditValue = "";
			}
			if (!ew_Empty($this->img_7->CurrentValue))
				$this->img_7->Upload->FileName = $this->img_7->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_7);

			// img_8
			$this->img_8->EditAttrs["class"] = "form-control";
			$this->img_8->EditCustomAttributes = "";
			if (!ew_Empty($this->img_8->Upload->DbValue)) {
				$this->img_8->EditValue = $this->img_8->Upload->DbValue;
			} else {
				$this->img_8->EditValue = "";
			}
			if (!ew_Empty($this->img_8->CurrentValue))
				$this->img_8->Upload->FileName = $this->img_8->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_8);

			// img_9
			$this->img_9->EditAttrs["class"] = "form-control";
			$this->img_9->EditCustomAttributes = "";
			if (!ew_Empty($this->img_9->Upload->DbValue)) {
				$this->img_9->EditValue = $this->img_9->Upload->DbValue;
			} else {
				$this->img_9->EditValue = "";
			}
			if (!ew_Empty($this->img_9->CurrentValue))
				$this->img_9->Upload->FileName = $this->img_9->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_9);

			// img_10
			$this->img_10->EditAttrs["class"] = "form-control";
			$this->img_10->EditCustomAttributes = "";
			if (!ew_Empty($this->img_10->Upload->DbValue)) {
				$this->img_10->EditValue = $this->img_10->Upload->DbValue;
			} else {
				$this->img_10->EditValue = "";
			}
			if (!ew_Empty($this->img_10->CurrentValue))
				$this->img_10->Upload->FileName = $this->img_10->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->img_10);

			// video_servidor
			$this->video_servidor->EditCustomAttributes = "";
			$this->video_servidor->EditValue = $this->video_servidor->Options(FALSE);

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->CurrentValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// Add refer script
			// prov_id

			$this->prov_id->LinkCustomAttributes = "";
			$this->prov_id->HrefValue = "";

			// loca_id
			$this->loca_id->LinkCustomAttributes = "";
			$this->loca_id->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// tipo_id
			$this->tipo_id->LinkCustomAttributes = "";
			$this->tipo_id->HrefValue = "";

			// actividad
			$this->actividad->LinkCustomAttributes = "";
			$this->actividad->HrefValue = "";

			// oper_tipo
			$this->oper_tipo->LinkCustomAttributes = "";
			$this->oper_tipo->HrefValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";

			// img_1
			$this->img_1->LinkCustomAttributes = "";
			$this->img_1->HrefValue = "";
			$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;

			// img_2
			$this->img_2->LinkCustomAttributes = "";
			$this->img_2->HrefValue = "";
			$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;

			// img_3
			$this->img_3->LinkCustomAttributes = "";
			$this->img_3->HrefValue = "";
			$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;

			// img_4
			$this->img_4->LinkCustomAttributes = "";
			$this->img_4->HrefValue = "";
			$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;

			// img_5
			$this->img_5->LinkCustomAttributes = "";
			$this->img_5->HrefValue = "";
			$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;

			// img_6
			$this->img_6->LinkCustomAttributes = "";
			$this->img_6->HrefValue = "";
			$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;

			// img_7
			$this->img_7->LinkCustomAttributes = "";
			$this->img_7->HrefValue = "";
			$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;

			// img_8
			$this->img_8->LinkCustomAttributes = "";
			$this->img_8->HrefValue = "";
			$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;

			// img_9
			$this->img_9->LinkCustomAttributes = "";
			$this->img_9->HrefValue = "";
			$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;

			// img_10
			$this->img_10->LinkCustomAttributes = "";
			$this->img_10->HrefValue = "";
			$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;

			// video_servidor
			$this->video_servidor->LinkCustomAttributes = "";
			$this->video_servidor->HrefValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->prov_id->FldIsDetailKey && !is_null($this->prov_id->FormValue) && $this->prov_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->prov_id->FldCaption(), $this->prov_id->ReqErrMsg));
		}
		if (!$this->loca_id->FldIsDetailKey && !is_null($this->loca_id->FormValue) && $this->loca_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->loca_id->FldCaption(), $this->loca_id->ReqErrMsg));
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!$this->tipo_id->FldIsDetailKey && !is_null($this->tipo_id->FormValue) && $this->tipo_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->tipo_id->FldCaption(), $this->tipo_id->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->tipo_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->tipo_id->FldErrMsg());
		}
		if ($this->actividad->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->actividad->FldCaption(), $this->actividad->ReqErrMsg));
		}
		if ($this->oper_tipo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->oper_tipo->FldCaption(), $this->oper_tipo->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// prov_id
		$this->prov_id->SetDbValueDef($rsnew, $this->prov_id->CurrentValue, 0, FALSE);

		// loca_id
		$this->loca_id->SetDbValueDef($rsnew, $this->loca_id->CurrentValue, 0, FALSE);

		// titulo
		$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", FALSE);

		// tipo_id
		$this->tipo_id->SetDbValueDef($rsnew, $this->tipo_id->CurrentValue, 0, FALSE);

		// actividad
		$this->actividad->SetDbValueDef($rsnew, $this->actividad->CurrentValue, "", strval($this->actividad->CurrentValue) == "");

		// oper_tipo
		$this->oper_tipo->SetDbValueDef($rsnew, $this->oper_tipo->CurrentValue, "", strval($this->oper_tipo->CurrentValue) == "");

		// descripcion
		$this->descripcion->SetDbValueDef($rsnew, $this->descripcion->CurrentValue, NULL, FALSE);

		// img_1
		if ($this->img_1->Visible && !$this->img_1->Upload->KeepFile) {
			$this->img_1->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_1->Upload->FileName == "") {
				$rsnew['img_1'] = NULL;
			} else {
				$rsnew['img_1'] = $this->img_1->Upload->FileName;
			}
			$this->img_1->ImageWidth = 960; // Resize width
			$this->img_1->ImageHeight = 0; // Resize height
		}

		// img_2
		if ($this->img_2->Visible && !$this->img_2->Upload->KeepFile) {
			$this->img_2->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_2->Upload->FileName == "") {
				$rsnew['img_2'] = NULL;
			} else {
				$rsnew['img_2'] = $this->img_2->Upload->FileName;
			}
		}

		// img_3
		if ($this->img_3->Visible && !$this->img_3->Upload->KeepFile) {
			$this->img_3->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_3->Upload->FileName == "") {
				$rsnew['img_3'] = NULL;
			} else {
				$rsnew['img_3'] = $this->img_3->Upload->FileName;
			}
		}

		// img_4
		if ($this->img_4->Visible && !$this->img_4->Upload->KeepFile) {
			$this->img_4->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_4->Upload->FileName == "") {
				$rsnew['img_4'] = NULL;
			} else {
				$rsnew['img_4'] = $this->img_4->Upload->FileName;
			}
		}

		// img_5
		if ($this->img_5->Visible && !$this->img_5->Upload->KeepFile) {
			$this->img_5->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_5->Upload->FileName == "") {
				$rsnew['img_5'] = NULL;
			} else {
				$rsnew['img_5'] = $this->img_5->Upload->FileName;
			}
		}

		// img_6
		if ($this->img_6->Visible && !$this->img_6->Upload->KeepFile) {
			$this->img_6->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_6->Upload->FileName == "") {
				$rsnew['img_6'] = NULL;
			} else {
				$rsnew['img_6'] = $this->img_6->Upload->FileName;
			}
		}

		// img_7
		if ($this->img_7->Visible && !$this->img_7->Upload->KeepFile) {
			$this->img_7->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_7->Upload->FileName == "") {
				$rsnew['img_7'] = NULL;
			} else {
				$rsnew['img_7'] = $this->img_7->Upload->FileName;
			}
		}

		// img_8
		if ($this->img_8->Visible && !$this->img_8->Upload->KeepFile) {
			$this->img_8->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_8->Upload->FileName == "") {
				$rsnew['img_8'] = NULL;
			} else {
				$rsnew['img_8'] = $this->img_8->Upload->FileName;
			}
		}

		// img_9
		if ($this->img_9->Visible && !$this->img_9->Upload->KeepFile) {
			$this->img_9->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_9->Upload->FileName == "") {
				$rsnew['img_9'] = NULL;
			} else {
				$rsnew['img_9'] = $this->img_9->Upload->FileName;
			}
		}

		// img_10
		if ($this->img_10->Visible && !$this->img_10->Upload->KeepFile) {
			$this->img_10->Upload->DbValue = ""; // No need to delete old file
			if ($this->img_10->Upload->FileName == "") {
				$rsnew['img_10'] = NULL;
			} else {
				$rsnew['img_10'] = $this->img_10->Upload->FileName;
			}
		}

		// video_servidor
		$this->video_servidor->SetDbValueDef($rsnew, $this->video_servidor->CurrentValue, NULL, strval($this->video_servidor->CurrentValue) == "");

		// video
		$this->video->SetDbValueDef($rsnew, $this->video->CurrentValue, NULL, FALSE);
		if ($this->img_1->Visible && !$this->img_1->Upload->KeepFile) {
			if (!ew_Empty($this->img_1->Upload->Value)) {
				$rsnew['img_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_1->UploadPath), $rsnew['img_1']); // Get new file name
			}
		}
		if ($this->img_2->Visible && !$this->img_2->Upload->KeepFile) {
			if (!ew_Empty($this->img_2->Upload->Value)) {
				$rsnew['img_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_2->UploadPath), $rsnew['img_2']); // Get new file name
			}
		}
		if ($this->img_3->Visible && !$this->img_3->Upload->KeepFile) {
			if (!ew_Empty($this->img_3->Upload->Value)) {
				$rsnew['img_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_3->UploadPath), $rsnew['img_3']); // Get new file name
			}
		}
		if ($this->img_4->Visible && !$this->img_4->Upload->KeepFile) {
			if (!ew_Empty($this->img_4->Upload->Value)) {
				$rsnew['img_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_4->UploadPath), $rsnew['img_4']); // Get new file name
			}
		}
		if ($this->img_5->Visible && !$this->img_5->Upload->KeepFile) {
			if (!ew_Empty($this->img_5->Upload->Value)) {
				$rsnew['img_5'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_5->UploadPath), $rsnew['img_5']); // Get new file name
			}
		}
		if ($this->img_6->Visible && !$this->img_6->Upload->KeepFile) {
			if (!ew_Empty($this->img_6->Upload->Value)) {
				$rsnew['img_6'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_6->UploadPath), $rsnew['img_6']); // Get new file name
			}
		}
		if ($this->img_7->Visible && !$this->img_7->Upload->KeepFile) {
			if (!ew_Empty($this->img_7->Upload->Value)) {
				$rsnew['img_7'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_7->UploadPath), $rsnew['img_7']); // Get new file name
			}
		}
		if ($this->img_8->Visible && !$this->img_8->Upload->KeepFile) {
			if (!ew_Empty($this->img_8->Upload->Value)) {
				$rsnew['img_8'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_8->UploadPath), $rsnew['img_8']); // Get new file name
			}
		}
		if ($this->img_9->Visible && !$this->img_9->Upload->KeepFile) {
			if (!ew_Empty($this->img_9->Upload->Value)) {
				$rsnew['img_9'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_9->UploadPath), $rsnew['img_9']); // Get new file name
			}
		}
		if ($this->img_10->Visible && !$this->img_10->Upload->KeepFile) {
			if (!ew_Empty($this->img_10->Upload->Value)) {
				$rsnew['img_10'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->img_10->UploadPath), $rsnew['img_10']); // Get new file name
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);

		// Check if key value entered
		if ($bInsertRow && $this->ValidateKey && strval($rsnew['id']) == "") {
			$this->setFailureMessage($Language->Phrase("InvalidKeyValue"));
			$bInsertRow = FALSE;
		}

		// Check for duplicate key
		if ($bInsertRow && $this->ValidateKey) {
			$sFilter = $this->KeyFilter();
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sKeyErrMsg = str_replace("%f", $sFilter, $Language->Phrase("DupKey"));
				$this->setFailureMessage($sKeyErrMsg);
				$rsChk->Close();
				$bInsertRow = FALSE;
			}
		}
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
				if ($this->img_1->Visible && !$this->img_1->Upload->KeepFile) {
					if (!ew_Empty($this->img_1->Upload->Value)) {
						$this->img_1->Upload->Resize($this->img_1->ImageWidth, $this->img_1->ImageHeight);
						if (!$this->img_1->Upload->SaveToFile($this->img_1->UploadPath, $rsnew['img_1'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_2->Visible && !$this->img_2->Upload->KeepFile) {
					if (!ew_Empty($this->img_2->Upload->Value)) {
						if (!$this->img_2->Upload->SaveToFile($this->img_2->UploadPath, $rsnew['img_2'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_3->Visible && !$this->img_3->Upload->KeepFile) {
					if (!ew_Empty($this->img_3->Upload->Value)) {
						if (!$this->img_3->Upload->SaveToFile($this->img_3->UploadPath, $rsnew['img_3'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_4->Visible && !$this->img_4->Upload->KeepFile) {
					if (!ew_Empty($this->img_4->Upload->Value)) {
						if (!$this->img_4->Upload->SaveToFile($this->img_4->UploadPath, $rsnew['img_4'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_5->Visible && !$this->img_5->Upload->KeepFile) {
					if (!ew_Empty($this->img_5->Upload->Value)) {
						if (!$this->img_5->Upload->SaveToFile($this->img_5->UploadPath, $rsnew['img_5'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_6->Visible && !$this->img_6->Upload->KeepFile) {
					if (!ew_Empty($this->img_6->Upload->Value)) {
						if (!$this->img_6->Upload->SaveToFile($this->img_6->UploadPath, $rsnew['img_6'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_7->Visible && !$this->img_7->Upload->KeepFile) {
					if (!ew_Empty($this->img_7->Upload->Value)) {
						if (!$this->img_7->Upload->SaveToFile($this->img_7->UploadPath, $rsnew['img_7'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_8->Visible && !$this->img_8->Upload->KeepFile) {
					if (!ew_Empty($this->img_8->Upload->Value)) {
						if (!$this->img_8->Upload->SaveToFile($this->img_8->UploadPath, $rsnew['img_8'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_9->Visible && !$this->img_9->Upload->KeepFile) {
					if (!ew_Empty($this->img_9->Upload->Value)) {
						if (!$this->img_9->Upload->SaveToFile($this->img_9->UploadPath, $rsnew['img_9'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->img_10->Visible && !$this->img_10->Upload->KeepFile) {
					if (!ew_Empty($this->img_10->Upload->Value)) {
						if (!$this->img_10->Upload->SaveToFile($this->img_10->UploadPath, $rsnew['img_10'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// img_1
		ew_CleanUploadTempPath($this->img_1, $this->img_1->Upload->Index);

		// img_2
		ew_CleanUploadTempPath($this->img_2, $this->img_2->Upload->Index);

		// img_3
		ew_CleanUploadTempPath($this->img_3, $this->img_3->Upload->Index);

		// img_4
		ew_CleanUploadTempPath($this->img_4, $this->img_4->Upload->Index);

		// img_5
		ew_CleanUploadTempPath($this->img_5, $this->img_5->Upload->Index);

		// img_6
		ew_CleanUploadTempPath($this->img_6, $this->img_6->Upload->Index);

		// img_7
		ew_CleanUploadTempPath($this->img_7, $this->img_7->Upload->Index);

		// img_8
		ew_CleanUploadTempPath($this->img_8, $this->img_8->Upload->Index);

		// img_9
		ew_CleanUploadTempPath($this->img_9, $this->img_9->Upload->Index);

		// img_10
		ew_CleanUploadTempPath($this->img_10, $this->img_10->Upload->Index);
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("propiedadeslist.php"), "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_prov_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
			$sWhereWrk = "";
			$this->prov_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` ASC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_loca_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
			$sWhereWrk = "{filter}";
			$this->loca_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`provincia_id` IN ({filter_value})', "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` ASC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($propiedades_add)) $propiedades_add = new cpropiedades_add();

// Page init
$propiedades_add->Page_Init();

// Page main
$propiedades_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$propiedades_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = fpropiedadesadd = new ew_Form("fpropiedadesadd", "add");

// Validate form
fpropiedadesadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_prov_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->prov_id->FldCaption(), $propiedades->prov_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_loca_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->loca_id->FldCaption(), $propiedades->loca_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->titulo->FldCaption(), $propiedades->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_tipo_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->tipo_id->FldCaption(), $propiedades->tipo_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_tipo_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($propiedades->tipo_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_actividad");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->actividad->FldCaption(), $propiedades->actividad->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_oper_tipo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $propiedades->oper_tipo->FldCaption(), $propiedades->oper_tipo->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fpropiedadesadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fpropiedadesadd.ValidateRequired = true;
<?php } else { ?>
fpropiedadesadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fpropiedadesadd.Lists["x_prov_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_loca_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
fpropiedadesadd.Lists["x_loca_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":["x_prov_id"],"ChildFields":[],"FilterFields":["x_provincia_id"],"Options":[],"Template":"","LinkTable":"localidades"};
fpropiedadesadd.Lists["x_actividad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesadd.Lists["x_actividad"].Options = <?php echo json_encode($propiedades->actividad->Options()) ?>;
fpropiedadesadd.Lists["x_oper_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesadd.Lists["x_oper_tipo"].Options = <?php echo json_encode($propiedades->oper_tipo->Options()) ?>;
fpropiedadesadd.Lists["x_video_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesadd.Lists["x_video_servidor"].Options = <?php echo json_encode($propiedades->video_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$propiedades_add->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $propiedades_add->ShowPageHeader(); ?>
<?php
$propiedades_add->ShowMessage();
?>
<form name="fpropiedadesadd" id="fpropiedadesadd" class="<?php echo $propiedades_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($propiedades_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $propiedades_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="propiedades">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($propiedades_add->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
	<div id="r_prov_id" class="form-group">
		<label id="elh_propiedades_prov_id" for="x_prov_id" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->prov_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->prov_id->CellAttributes() ?>>
<span id="el_propiedades_prov_id">
<?php $propiedades->prov_id->EditAttrs["onclick"] = "ew_UpdateOpt.call(this); " . @$propiedades->prov_id->EditAttrs["onclick"]; ?>
<div class="ewDropdownList has-feedback">
	<span onclick="" class="form-control dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		<?php echo $propiedades->prov_id->ViewValue ?>
	</span>
	<span class="glyphicon glyphicon-remove form-control-feedback ewDropdownListClear"></span>
	<span class="form-control-feedback"><span class="caret"></span></span>
	<div id="dsl_x_prov_id" data-repeatcolumn="1" class="dropdown-menu">
		<div class="ewItems" style="position: relative; overflow-x: hidden;">
<?php echo $propiedades->prov_id->RadioButtonListHtml(TRUE, "x_prov_id") ?>
		</div>
	</div>
	<div id="tp_x_prov_id" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_prov_id" data-value-separator="<?php echo $propiedades->prov_id->DisplayValueSeparatorAttribute() ?>" name="x_prov_id" id="x_prov_id" value="{value}"<?php echo $propiedades->prov_id->EditAttributes() ?>></div>
</div>
<input type="hidden" name="s_x_prov_id" id="s_x_prov_id" value="<?php echo $propiedades->prov_id->LookupFilterQuery() ?>">
</span>
<?php echo $propiedades->prov_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
	<div id="r_loca_id" class="form-group">
		<label id="elh_propiedades_loca_id" for="x_loca_id" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->loca_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->loca_id->CellAttributes() ?>>
<span id="el_propiedades_loca_id">
<div class="ewDropdownList has-feedback">
	<span onclick="" class="form-control dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		<?php echo $propiedades->loca_id->ViewValue ?>
	</span>
	<span class="glyphicon glyphicon-remove form-control-feedback ewDropdownListClear"></span>
	<span class="form-control-feedback"><span class="caret"></span></span>
	<div id="dsl_x_loca_id" data-repeatcolumn="1" class="dropdown-menu">
		<div class="ewItems" style="position: relative; overflow-x: hidden;">
<?php echo $propiedades->loca_id->RadioButtonListHtml(TRUE, "x_loca_id") ?>
		</div>
	</div>
	<div id="tp_x_loca_id" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_loca_id" data-value-separator="<?php echo $propiedades->loca_id->DisplayValueSeparatorAttribute() ?>" name="x_loca_id" id="x_loca_id" value="{value}"<?php echo $propiedades->loca_id->EditAttributes() ?>></div>
</div>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $propiedades->loca_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_loca_id',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_loca_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $propiedades->loca_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_loca_id" id="s_x_loca_id" value="<?php echo $propiedades->loca_id->LookupFilterQuery() ?>">
</span>
<?php echo $propiedades->loca_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->titulo->Visible) { // titulo ?>
	<div id="r_titulo" class="form-group">
		<label id="elh_propiedades_titulo" for="x_titulo" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->titulo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->titulo->CellAttributes() ?>>
<span id="el_propiedades_titulo">
<input type="text" data-table="propiedades" data-field="x_titulo" name="x_titulo" id="x_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($propiedades->titulo->getPlaceHolder()) ?>" value="<?php echo $propiedades->titulo->EditValue ?>"<?php echo $propiedades->titulo->EditAttributes() ?>>
</span>
<?php echo $propiedades->titulo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
	<div id="r_tipo_id" class="form-group">
		<label id="elh_propiedades_tipo_id" for="x_tipo_id" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->tipo_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->tipo_id->CellAttributes() ?>>
<span id="el_propiedades_tipo_id">
<input type="text" data-table="propiedades" data-field="x_tipo_id" name="x_tipo_id" id="x_tipo_id" size="30" placeholder="<?php echo ew_HtmlEncode($propiedades->tipo_id->getPlaceHolder()) ?>" value="<?php echo $propiedades->tipo_id->EditValue ?>"<?php echo $propiedades->tipo_id->EditAttributes() ?>>
</span>
<?php echo $propiedades->tipo_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->actividad->Visible) { // actividad ?>
	<div id="r_actividad" class="form-group">
		<label id="elh_propiedades_actividad" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->actividad->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->actividad->CellAttributes() ?>>
<span id="el_propiedades_actividad">
<div id="tp_x_actividad" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_actividad" data-value-separator="<?php echo $propiedades->actividad->DisplayValueSeparatorAttribute() ?>" name="x_actividad" id="x_actividad" value="{value}"<?php echo $propiedades->actividad->EditAttributes() ?>></div>
<div id="dsl_x_actividad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->actividad->RadioButtonListHtml(FALSE, "x_actividad") ?>
</div></div>
</span>
<?php echo $propiedades->actividad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
	<div id="r_oper_tipo" class="form-group">
		<label id="elh_propiedades_oper_tipo" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->oper_tipo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->oper_tipo->CellAttributes() ?>>
<span id="el_propiedades_oper_tipo">
<div id="tp_x_oper_tipo" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_oper_tipo" data-value-separator="<?php echo $propiedades->oper_tipo->DisplayValueSeparatorAttribute() ?>" name="x_oper_tipo" id="x_oper_tipo" value="{value}"<?php echo $propiedades->oper_tipo->EditAttributes() ?>></div>
<div id="dsl_x_oper_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->oper_tipo->RadioButtonListHtml(FALSE, "x_oper_tipo") ?>
</div></div>
</span>
<?php echo $propiedades->oper_tipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->descripcion->Visible) { // descripcion ?>
	<div id="r_descripcion" class="form-group">
		<label id="elh_propiedades_descripcion" for="x_descripcion" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->descripcion->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->descripcion->CellAttributes() ?>>
<span id="el_propiedades_descripcion">
<textarea data-table="propiedades" data-field="x_descripcion" name="x_descripcion" id="x_descripcion" cols="60" rows="4" placeholder="<?php echo ew_HtmlEncode($propiedades->descripcion->getPlaceHolder()) ?>"<?php echo $propiedades->descripcion->EditAttributes() ?>><?php echo $propiedades->descripcion->EditValue ?></textarea>
</span>
<?php echo $propiedades->descripcion->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_1->Visible) { // img_1 ?>
	<div id="r_img_1" class="form-group">
		<label id="elh_propiedades_img_1" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_1->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_1->CellAttributes() ?>>
<span id="el_propiedades_img_1">
<div id="fd_x_img_1">
<span title="<?php echo $propiedades->img_1->FldTitle() ? $propiedades->img_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_1->ReadOnly || $propiedades->img_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_1" name="x_img_1" id="x_img_1"<?php echo $propiedades->img_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_1" id= "fn_x_img_1" value="<?php echo $propiedades->img_1->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_1" id= "fa_x_img_1" value="0">
<input type="hidden" name="fs_x_img_1" id= "fs_x_img_1" value="255">
<input type="hidden" name="fx_x_img_1" id= "fx_x_img_1" value="<?php echo $propiedades->img_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_1" id= "fm_x_img_1" value="<?php echo $propiedades->img_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_2->Visible) { // img_2 ?>
	<div id="r_img_2" class="form-group">
		<label id="elh_propiedades_img_2" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_2->CellAttributes() ?>>
<span id="el_propiedades_img_2">
<div id="fd_x_img_2">
<span title="<?php echo $propiedades->img_2->FldTitle() ? $propiedades->img_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_2->ReadOnly || $propiedades->img_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_2" name="x_img_2" id="x_img_2"<?php echo $propiedades->img_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_2" id= "fn_x_img_2" value="<?php echo $propiedades->img_2->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_2" id= "fa_x_img_2" value="0">
<input type="hidden" name="fs_x_img_2" id= "fs_x_img_2" value="255">
<input type="hidden" name="fx_x_img_2" id= "fx_x_img_2" value="<?php echo $propiedades->img_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_2" id= "fm_x_img_2" value="<?php echo $propiedades->img_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_3->Visible) { // img_3 ?>
	<div id="r_img_3" class="form-group">
		<label id="elh_propiedades_img_3" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_3->CellAttributes() ?>>
<span id="el_propiedades_img_3">
<div id="fd_x_img_3">
<span title="<?php echo $propiedades->img_3->FldTitle() ? $propiedades->img_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_3->ReadOnly || $propiedades->img_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_3" name="x_img_3" id="x_img_3"<?php echo $propiedades->img_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_3" id= "fn_x_img_3" value="<?php echo $propiedades->img_3->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_3" id= "fa_x_img_3" value="0">
<input type="hidden" name="fs_x_img_3" id= "fs_x_img_3" value="255">
<input type="hidden" name="fx_x_img_3" id= "fx_x_img_3" value="<?php echo $propiedades->img_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_3" id= "fm_x_img_3" value="<?php echo $propiedades->img_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_4->Visible) { // img_4 ?>
	<div id="r_img_4" class="form-group">
		<label id="elh_propiedades_img_4" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_4->CellAttributes() ?>>
<span id="el_propiedades_img_4">
<div id="fd_x_img_4">
<span title="<?php echo $propiedades->img_4->FldTitle() ? $propiedades->img_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_4->ReadOnly || $propiedades->img_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_4" name="x_img_4" id="x_img_4"<?php echo $propiedades->img_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_4" id= "fn_x_img_4" value="<?php echo $propiedades->img_4->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_4" id= "fa_x_img_4" value="0">
<input type="hidden" name="fs_x_img_4" id= "fs_x_img_4" value="255">
<input type="hidden" name="fx_x_img_4" id= "fx_x_img_4" value="<?php echo $propiedades->img_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_4" id= "fm_x_img_4" value="<?php echo $propiedades->img_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_5->Visible) { // img_5 ?>
	<div id="r_img_5" class="form-group">
		<label id="elh_propiedades_img_5" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_5->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_5->CellAttributes() ?>>
<span id="el_propiedades_img_5">
<div id="fd_x_img_5">
<span title="<?php echo $propiedades->img_5->FldTitle() ? $propiedades->img_5->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_5->ReadOnly || $propiedades->img_5->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_5" name="x_img_5" id="x_img_5"<?php echo $propiedades->img_5->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_5" id= "fn_x_img_5" value="<?php echo $propiedades->img_5->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_5" id= "fa_x_img_5" value="0">
<input type="hidden" name="fs_x_img_5" id= "fs_x_img_5" value="255">
<input type="hidden" name="fx_x_img_5" id= "fx_x_img_5" value="<?php echo $propiedades->img_5->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_5" id= "fm_x_img_5" value="<?php echo $propiedades->img_5->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_5" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_5->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_6->Visible) { // img_6 ?>
	<div id="r_img_6" class="form-group">
		<label id="elh_propiedades_img_6" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_6->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_6->CellAttributes() ?>>
<span id="el_propiedades_img_6">
<div id="fd_x_img_6">
<span title="<?php echo $propiedades->img_6->FldTitle() ? $propiedades->img_6->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_6->ReadOnly || $propiedades->img_6->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_6" name="x_img_6" id="x_img_6"<?php echo $propiedades->img_6->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_6" id= "fn_x_img_6" value="<?php echo $propiedades->img_6->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_6" id= "fa_x_img_6" value="0">
<input type="hidden" name="fs_x_img_6" id= "fs_x_img_6" value="255">
<input type="hidden" name="fx_x_img_6" id= "fx_x_img_6" value="<?php echo $propiedades->img_6->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_6" id= "fm_x_img_6" value="<?php echo $propiedades->img_6->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_6" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_6->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_7->Visible) { // img_7 ?>
	<div id="r_img_7" class="form-group">
		<label id="elh_propiedades_img_7" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_7->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_7->CellAttributes() ?>>
<span id="el_propiedades_img_7">
<div id="fd_x_img_7">
<span title="<?php echo $propiedades->img_7->FldTitle() ? $propiedades->img_7->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_7->ReadOnly || $propiedades->img_7->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_7" name="x_img_7" id="x_img_7"<?php echo $propiedades->img_7->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_7" id= "fn_x_img_7" value="<?php echo $propiedades->img_7->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_7" id= "fa_x_img_7" value="0">
<input type="hidden" name="fs_x_img_7" id= "fs_x_img_7" value="255">
<input type="hidden" name="fx_x_img_7" id= "fx_x_img_7" value="<?php echo $propiedades->img_7->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_7" id= "fm_x_img_7" value="<?php echo $propiedades->img_7->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_7" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_7->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_8->Visible) { // img_8 ?>
	<div id="r_img_8" class="form-group">
		<label id="elh_propiedades_img_8" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_8->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_8->CellAttributes() ?>>
<span id="el_propiedades_img_8">
<div id="fd_x_img_8">
<span title="<?php echo $propiedades->img_8->FldTitle() ? $propiedades->img_8->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_8->ReadOnly || $propiedades->img_8->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_8" name="x_img_8" id="x_img_8"<?php echo $propiedades->img_8->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_8" id= "fn_x_img_8" value="<?php echo $propiedades->img_8->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_8" id= "fa_x_img_8" value="0">
<input type="hidden" name="fs_x_img_8" id= "fs_x_img_8" value="255">
<input type="hidden" name="fx_x_img_8" id= "fx_x_img_8" value="<?php echo $propiedades->img_8->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_8" id= "fm_x_img_8" value="<?php echo $propiedades->img_8->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_8" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_8->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_9->Visible) { // img_9 ?>
	<div id="r_img_9" class="form-group">
		<label id="elh_propiedades_img_9" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_9->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_9->CellAttributes() ?>>
<span id="el_propiedades_img_9">
<div id="fd_x_img_9">
<span title="<?php echo $propiedades->img_9->FldTitle() ? $propiedades->img_9->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_9->ReadOnly || $propiedades->img_9->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_9" name="x_img_9" id="x_img_9"<?php echo $propiedades->img_9->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_9" id= "fn_x_img_9" value="<?php echo $propiedades->img_9->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_9" id= "fa_x_img_9" value="0">
<input type="hidden" name="fs_x_img_9" id= "fs_x_img_9" value="255">
<input type="hidden" name="fx_x_img_9" id= "fx_x_img_9" value="<?php echo $propiedades->img_9->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_9" id= "fm_x_img_9" value="<?php echo $propiedades->img_9->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_9" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_9->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->img_10->Visible) { // img_10 ?>
	<div id="r_img_10" class="form-group">
		<label id="elh_propiedades_img_10" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->img_10->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->img_10->CellAttributes() ?>>
<span id="el_propiedades_img_10">
<div id="fd_x_img_10">
<span title="<?php echo $propiedades->img_10->FldTitle() ? $propiedades->img_10->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($propiedades->img_10->ReadOnly || $propiedades->img_10->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="propiedades" data-field="x_img_10" name="x_img_10" id="x_img_10"<?php echo $propiedades->img_10->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_img_10" id= "fn_x_img_10" value="<?php echo $propiedades->img_10->Upload->FileName ?>">
<input type="hidden" name="fa_x_img_10" id= "fa_x_img_10" value="0">
<input type="hidden" name="fs_x_img_10" id= "fs_x_img_10" value="255">
<input type="hidden" name="fx_x_img_10" id= "fx_x_img_10" value="<?php echo $propiedades->img_10->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_img_10" id= "fm_x_img_10" value="<?php echo $propiedades->img_10->UploadMaxFileSize ?>">
</div>
<table id="ft_x_img_10" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $propiedades->img_10->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
	<div id="r_video_servidor" class="form-group">
		<label id="elh_propiedades_video_servidor" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->video_servidor->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->video_servidor->CellAttributes() ?>>
<span id="el_propiedades_video_servidor">
<div id="tp_x_video_servidor" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_video_servidor" data-value-separator="<?php echo $propiedades->video_servidor->DisplayValueSeparatorAttribute() ?>" name="x_video_servidor" id="x_video_servidor" value="{value}"<?php echo $propiedades->video_servidor->EditAttributes() ?>></div>
<div id="dsl_x_video_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->video_servidor->RadioButtonListHtml(FALSE, "x_video_servidor") ?>
</div></div>
</span>
<?php echo $propiedades->video_servidor->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($propiedades->video->Visible) { // video ?>
	<div id="r_video" class="form-group">
		<label id="elh_propiedades_video" for="x_video" class="col-sm-2 control-label ewLabel"><?php echo $propiedades->video->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $propiedades->video->CellAttributes() ?>>
<span id="el_propiedades_video">
<input type="text" data-table="propiedades" data-field="x_video" name="x_video" id="x_video" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($propiedades->video->getPlaceHolder()) ?>" value="<?php echo $propiedades->video->EditValue ?>"<?php echo $propiedades->video->EditAttributes() ?>>
</span>
<?php echo $propiedades->video->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php if (!$propiedades_add->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $propiedades_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fpropiedadesadd.Init();
</script>
<?php
$propiedades_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$propiedades_add->Page_Terminate();
?>
