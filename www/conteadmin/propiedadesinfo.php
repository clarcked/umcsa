<?php

// Global variable for table object
$propiedades = NULL;

//
// Table class for propiedades
//
class cpropiedades extends cTable {
	var $id;
	var $prov_id;
	var $loca_id;
	var $titulo;
	var $tipo_id;
	var $actividad;
	var $oper_tipo;
	var $descripcion;
	var $img_1;
	var $img_2;
	var $img_3;
	var $img_4;
	var $img_5;
	var $img_6;
	var $img_7;
	var $img_8;
	var $img_9;
	var $img_10;
	var $video_servidor;
	var $video;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'propiedades';
		$this->TableName = 'propiedades';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`propiedades`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 20;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('propiedades', 'propiedades', 'x_id', 'id', '`id`', '`id`', 3, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// prov_id
		$this->prov_id = new cField('propiedades', 'propiedades', 'x_prov_id', 'prov_id', '`prov_id`', '`prov_id`', 3, -1, FALSE, '`EV__prov_id`', TRUE, TRUE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->prov_id->Sortable = TRUE; // Allow sort
		$this->prov_id->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->prov_id->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->prov_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prov_id'] = &$this->prov_id;

		// loca_id
		$this->loca_id = new cField('propiedades', 'propiedades', 'x_loca_id', 'loca_id', '`loca_id`', '`loca_id`', 3, -1, FALSE, '`EV__loca_id`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->loca_id->Sortable = TRUE; // Allow sort
		$this->loca_id->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->loca_id->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->loca_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['loca_id'] = &$this->loca_id;

		// titulo
		$this->titulo = new cField('propiedades', 'propiedades', 'x_titulo', 'titulo', '`titulo`', '`titulo`', 200, -1, FALSE, '`titulo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->titulo->Sortable = TRUE; // Allow sort
		$this->fields['titulo'] = &$this->titulo;

		// tipo_id
		$this->tipo_id = new cField('propiedades', 'propiedades', 'x_tipo_id', 'tipo_id', '`tipo_id`', '`tipo_id`', 3, -1, FALSE, '`tipo_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->tipo_id->Sortable = TRUE; // Allow sort
		$this->tipo_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['tipo_id'] = &$this->tipo_id;

		// actividad
		$this->actividad = new cField('propiedades', 'propiedades', 'x_actividad', 'actividad', '`actividad`', '`actividad`', 202, -1, FALSE, '`actividad`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->actividad->Sortable = TRUE; // Allow sort
		$this->actividad->OptionCount = 3;
		$this->fields['actividad'] = &$this->actividad;

		// oper_tipo
		$this->oper_tipo = new cField('propiedades', 'propiedades', 'x_oper_tipo', 'oper_tipo', '`oper_tipo`', '`oper_tipo`', 202, -1, FALSE, '`oper_tipo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->oper_tipo->Sortable = TRUE; // Allow sort
		$this->oper_tipo->OptionCount = 2;
		$this->fields['oper_tipo'] = &$this->oper_tipo;

		// descripcion
		$this->descripcion = new cField('propiedades', 'propiedades', 'x_descripcion', 'descripcion', '`descripcion`', '`descripcion`', 201, -1, FALSE, '`descripcion`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->descripcion->Sortable = TRUE; // Allow sort
		$this->fields['descripcion'] = &$this->descripcion;

		// img_1
		$this->img_1 = new cField('propiedades', 'propiedades', 'x_img_1', 'img_1', '`img_1`', '`img_1`', 200, -1, TRUE, '`img_1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_1->Sortable = TRUE; // Allow sort
		$this->fields['img_1'] = &$this->img_1;

		// img_2
		$this->img_2 = new cField('propiedades', 'propiedades', 'x_img_2', 'img_2', '`img_2`', '`img_2`', 200, -1, TRUE, '`img_2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_2->Sortable = TRUE; // Allow sort
		$this->fields['img_2'] = &$this->img_2;

		// img_3
		$this->img_3 = new cField('propiedades', 'propiedades', 'x_img_3', 'img_3', '`img_3`', '`img_3`', 200, -1, TRUE, '`img_3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_3->Sortable = TRUE; // Allow sort
		$this->fields['img_3'] = &$this->img_3;

		// img_4
		$this->img_4 = new cField('propiedades', 'propiedades', 'x_img_4', 'img_4', '`img_4`', '`img_4`', 200, -1, TRUE, '`img_4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_4->Sortable = TRUE; // Allow sort
		$this->fields['img_4'] = &$this->img_4;

		// img_5
		$this->img_5 = new cField('propiedades', 'propiedades', 'x_img_5', 'img_5', '`img_5`', '`img_5`', 200, -1, TRUE, '`img_5`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_5->Sortable = TRUE; // Allow sort
		$this->fields['img_5'] = &$this->img_5;

		// img_6
		$this->img_6 = new cField('propiedades', 'propiedades', 'x_img_6', 'img_6', '`img_6`', '`img_6`', 200, -1, TRUE, '`img_6`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_6->Sortable = TRUE; // Allow sort
		$this->fields['img_6'] = &$this->img_6;

		// img_7
		$this->img_7 = new cField('propiedades', 'propiedades', 'x_img_7', 'img_7', '`img_7`', '`img_7`', 200, -1, TRUE, '`img_7`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_7->Sortable = TRUE; // Allow sort
		$this->fields['img_7'] = &$this->img_7;

		// img_8
		$this->img_8 = new cField('propiedades', 'propiedades', 'x_img_8', 'img_8', '`img_8`', '`img_8`', 200, -1, TRUE, '`img_8`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_8->Sortable = TRUE; // Allow sort
		$this->fields['img_8'] = &$this->img_8;

		// img_9
		$this->img_9 = new cField('propiedades', 'propiedades', 'x_img_9', 'img_9', '`img_9`', '`img_9`', 200, -1, TRUE, '`img_9`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_9->Sortable = TRUE; // Allow sort
		$this->fields['img_9'] = &$this->img_9;

		// img_10
		$this->img_10 = new cField('propiedades', 'propiedades', 'x_img_10', 'img_10', '`img_10`', '`img_10`', 200, -1, TRUE, '`img_10`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->img_10->Sortable = TRUE; // Allow sort
		$this->fields['img_10'] = &$this->img_10;

		// video_servidor
		$this->video_servidor = new cField('propiedades', 'propiedades', 'x_video_servidor', 'video_servidor', '`video_servidor`', '`video_servidor`', 202, -1, FALSE, '`video_servidor`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->video_servidor->Sortable = TRUE; // Allow sort
		$this->video_servidor->OptionCount = 2;
		$this->fields['video_servidor'] = &$this->video_servidor;

		// video
		$this->video = new cField('propiedades', 'propiedades', 'x_video', 'video', '`video`', '`video`', 200, -1, FALSE, '`video`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video->Sortable = TRUE; // Allow sort
		$this->fields['video'] = &$this->video;
	}

	// Set Field Visibility
	function SetFieldVisibility($fldparm) {
		global $Security;
		return $this->$fldparm->Visible; // Returns original value
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			$sSortFieldList = ($ofld->FldVirtualExpression <> "") ? $ofld->FldVirtualExpression : $sSortField;
			$this->setSessionOrderByList($sSortFieldList . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Session ORDER BY for List page
	function getSessionOrderByList() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY_LIST];
	}

	function setSessionOrderByList($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY_LIST] = $v;
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`propiedades`";
	}

	function SqlFrom() { // For backward compatibility
		return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
		$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
		return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
		$this->_SqlSelect = $v;
	}
	var $_SqlSelectList = "";

	function getSqlSelectList() { // Select for List page
		$select = "";
		$select = "SELECT * FROM (" .
			"SELECT *, (SELECT `nombre` FROM `provincias` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `propiedades`.`prov_id` LIMIT 1) AS `EV__prov_id`, (SELECT `nombre` FROM `localidades` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `propiedades`.`loca_id` LIMIT 1) AS `EV__loca_id` FROM `propiedades`" .
			") `EW_TMP_TABLE`";
		return ($this->_SqlSelectList <> "") ? $this->_SqlSelectList : $select;
	}

	function SqlSelectList() { // For backward compatibility
		return $this->getSqlSelectList();
	}

	function setSqlSelectList($v) {
		$this->_SqlSelectList = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
		return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
		$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
		return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
		$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
		return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
		$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
		return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
		$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 8) == 8);
			case "search":
				return (($allow & 8) == 8);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		if ($this->UseVirtualFields()) {
			$sSort = $this->getSessionOrderByList();
			return ew_BuildSelectSql($this->getSqlSelectList(), $this->getSqlWhere(), $this->getSqlGroupBy(),
				$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
		} else {
			$sSort = $this->getSessionOrderBy();
			return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
				$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
		}
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = ($this->UseVirtualFields()) ? $this->getSessionOrderByList() : $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Check if virtual fields is used in SQL
	function UseVirtualFields() {
		$sWhere = $this->getSessionWhere();
		$sOrderBy = $this->getSessionOrderByList();
		if ($sWhere <> "")
			$sWhere = " " . str_replace(array("(",")"), array("",""), $sWhere) . " ";
		if ($sOrderBy <> "")
			$sOrderBy = " " . str_replace(array("(",")"), array("",""), $sOrderBy) . " ";
		if (strpos($sOrderBy, " " . $this->prov_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->loca_id->AdvancedSearch->SearchValue <> "" ||
			$this->loca_id->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->loca_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->loca_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		return FALSE;
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id', $this->DBID) . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "propiedadeslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "propiedadeslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("propiedadesview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("propiedadesview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "propiedadesadd.php?" . $this->UrlParm($parm);
		else
			$url = "propiedadesadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("propiedadesedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("propiedadesadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("propiedadesdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "id:" . ew_VarToJson($this->id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return $this->AddMasterUrl(ew_CurrentPage() . "?" . $sUrlParm);
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["id"]))
				$arKeys[] = ew_StripSlashes($_POST["id"]);
			elseif (isset($_GET["id"]))
				$arKeys[] = ew_StripSlashes($_GET["id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->prov_id->setDbValue($rs->fields('prov_id'));
		$this->loca_id->setDbValue($rs->fields('loca_id'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->tipo_id->setDbValue($rs->fields('tipo_id'));
		$this->actividad->setDbValue($rs->fields('actividad'));
		$this->oper_tipo->setDbValue($rs->fields('oper_tipo'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->img_1->Upload->DbValue = $rs->fields('img_1');
		$this->img_2->Upload->DbValue = $rs->fields('img_2');
		$this->img_3->Upload->DbValue = $rs->fields('img_3');
		$this->img_4->Upload->DbValue = $rs->fields('img_4');
		$this->img_5->Upload->DbValue = $rs->fields('img_5');
		$this->img_6->Upload->DbValue = $rs->fields('img_6');
		$this->img_7->Upload->DbValue = $rs->fields('img_7');
		$this->img_8->Upload->DbValue = $rs->fields('img_8');
		$this->img_9->Upload->DbValue = $rs->fields('img_9');
		$this->img_10->Upload->DbValue = $rs->fields('img_10');
		$this->video_servidor->setDbValue($rs->fields('video_servidor'));
		$this->video->setDbValue($rs->fields('video'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// prov_id
		// loca_id
		// titulo
		// tipo_id
		// actividad
		// oper_tipo
		// descripcion
		// img_1
		// img_2
		// img_3
		// img_4
		// img_5
		// img_6
		// img_7
		// img_8
		// img_9
		// img_10
		// video_servidor
		// video
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		if ($this->prov_id->VirtualValue <> "") {
			$this->prov_id->ViewValue = $this->prov_id->VirtualValue;
		} else {
		if (strval($this->prov_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->prov_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prov_id->ViewValue = $this->prov_id->CurrentValue;
			}
		} else {
			$this->prov_id->ViewValue = NULL;
		}
		}
		$this->prov_id->ViewCustomAttributes = "";

		// loca_id
		if ($this->loca_id->VirtualValue <> "") {
			$this->loca_id->ViewValue = $this->loca_id->VirtualValue;
		} else {
		if (strval($this->loca_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->loca_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->loca_id->ViewValue = $this->loca_id->CurrentValue;
			}
		} else {
			$this->loca_id->ViewValue = NULL;
		}
		}
		$this->loca_id->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// tipo_id
		$this->tipo_id->ViewValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->ViewCustomAttributes = "";

		// actividad
		if (strval($this->actividad->CurrentValue) <> "") {
			$this->actividad->ViewValue = $this->actividad->OptionCaption($this->actividad->CurrentValue);
		} else {
			$this->actividad->ViewValue = NULL;
		}
		$this->actividad->ViewCustomAttributes = "";

		// oper_tipo
		if (strval($this->oper_tipo->CurrentValue) <> "") {
			$this->oper_tipo->ViewValue = $this->oper_tipo->OptionCaption($this->oper_tipo->CurrentValue);
		} else {
			$this->oper_tipo->ViewValue = NULL;
		}
		$this->oper_tipo->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// img_1
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->ViewValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->ViewValue = "";
		}
		$this->img_1->ViewCustomAttributes = "";

		// img_2
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->ViewValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->ViewValue = "";
		}
		$this->img_2->ViewCustomAttributes = "";

		// img_3
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->ViewValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->ViewValue = "";
		}
		$this->img_3->ViewCustomAttributes = "";

		// img_4
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->ViewValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->ViewValue = "";
		}
		$this->img_4->ViewCustomAttributes = "";

		// img_5
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->ViewValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->ViewValue = "";
		}
		$this->img_5->ViewCustomAttributes = "";

		// img_6
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->ViewValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->ViewValue = "";
		}
		$this->img_6->ViewCustomAttributes = "";

		// img_7
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->ViewValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->ViewValue = "";
		}
		$this->img_7->ViewCustomAttributes = "";

		// img_8
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->ViewValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->ViewValue = "";
		}
		$this->img_8->ViewCustomAttributes = "";

		// img_9
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->ViewValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->ViewValue = "";
		}
		$this->img_9->ViewCustomAttributes = "";

		// img_10
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->ViewValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->ViewValue = "";
		}
		$this->img_10->ViewCustomAttributes = "";

		// video_servidor
		if (strval($this->video_servidor->CurrentValue) <> "") {
			$this->video_servidor->ViewValue = $this->video_servidor->OptionCaption($this->video_servidor->CurrentValue);
		} else {
			$this->video_servidor->ViewValue = NULL;
		}
		$this->video_servidor->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// prov_id
		$this->prov_id->LinkCustomAttributes = "";
		$this->prov_id->HrefValue = "";
		$this->prov_id->TooltipValue = "";

		// loca_id
		$this->loca_id->LinkCustomAttributes = "";
		$this->loca_id->HrefValue = "";
		$this->loca_id->TooltipValue = "";

		// titulo
		$this->titulo->LinkCustomAttributes = "";
		$this->titulo->HrefValue = "";
		$this->titulo->TooltipValue = "";

		// tipo_id
		$this->tipo_id->LinkCustomAttributes = "";
		$this->tipo_id->HrefValue = "";
		$this->tipo_id->TooltipValue = "";

		// actividad
		$this->actividad->LinkCustomAttributes = "";
		$this->actividad->HrefValue = "";
		$this->actividad->TooltipValue = "";

		// oper_tipo
		$this->oper_tipo->LinkCustomAttributes = "";
		$this->oper_tipo->HrefValue = "";
		$this->oper_tipo->TooltipValue = "";

		// descripcion
		$this->descripcion->LinkCustomAttributes = "";
		$this->descripcion->HrefValue = "";
		$this->descripcion->TooltipValue = "";

		// img_1
		$this->img_1->LinkCustomAttributes = "";
		$this->img_1->HrefValue = "";
		$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;
		$this->img_1->TooltipValue = "";

		// img_2
		$this->img_2->LinkCustomAttributes = "";
		$this->img_2->HrefValue = "";
		$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;
		$this->img_2->TooltipValue = "";

		// img_3
		$this->img_3->LinkCustomAttributes = "";
		$this->img_3->HrefValue = "";
		$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;
		$this->img_3->TooltipValue = "";

		// img_4
		$this->img_4->LinkCustomAttributes = "";
		$this->img_4->HrefValue = "";
		$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;
		$this->img_4->TooltipValue = "";

		// img_5
		$this->img_5->LinkCustomAttributes = "";
		$this->img_5->HrefValue = "";
		$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;
		$this->img_5->TooltipValue = "";

		// img_6
		$this->img_6->LinkCustomAttributes = "";
		$this->img_6->HrefValue = "";
		$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;
		$this->img_6->TooltipValue = "";

		// img_7
		$this->img_7->LinkCustomAttributes = "";
		$this->img_7->HrefValue = "";
		$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;
		$this->img_7->TooltipValue = "";

		// img_8
		$this->img_8->LinkCustomAttributes = "";
		$this->img_8->HrefValue = "";
		$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;
		$this->img_8->TooltipValue = "";

		// img_9
		$this->img_9->LinkCustomAttributes = "";
		$this->img_9->HrefValue = "";
		$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;
		$this->img_9->TooltipValue = "";

		// img_10
		$this->img_10->LinkCustomAttributes = "";
		$this->img_10->HrefValue = "";
		$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;
		$this->img_10->TooltipValue = "";

		// video_servidor
		$this->video_servidor->LinkCustomAttributes = "";
		$this->video_servidor->HrefValue = "";
		$this->video_servidor->TooltipValue = "";

		// video
		$this->video->LinkCustomAttributes = "";
		$this->video->HrefValue = "";
		$this->video->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		$this->prov_id->EditCustomAttributes = "";

		// loca_id
		$this->loca_id->EditCustomAttributes = "";

		// titulo
		$this->titulo->EditAttrs["class"] = "form-control";
		$this->titulo->EditCustomAttributes = "";
		$this->titulo->EditValue = $this->titulo->CurrentValue;
		$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

		// tipo_id
		$this->tipo_id->EditAttrs["class"] = "form-control";
		$this->tipo_id->EditCustomAttributes = "";
		$this->tipo_id->EditValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->PlaceHolder = ew_RemoveHtml($this->tipo_id->FldCaption());

		// actividad
		$this->actividad->EditCustomAttributes = "";
		$this->actividad->EditValue = $this->actividad->Options(FALSE);

		// oper_tipo
		$this->oper_tipo->EditCustomAttributes = "";
		$this->oper_tipo->EditValue = $this->oper_tipo->Options(FALSE);

		// descripcion
		$this->descripcion->EditAttrs["class"] = "form-control";
		$this->descripcion->EditCustomAttributes = "";
		$this->descripcion->EditValue = $this->descripcion->CurrentValue;
		$this->descripcion->PlaceHolder = ew_RemoveHtml($this->descripcion->FldCaption());

		// img_1
		$this->img_1->EditAttrs["class"] = "form-control";
		$this->img_1->EditCustomAttributes = "";
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->EditValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->EditValue = "";
		}
		if (!ew_Empty($this->img_1->CurrentValue))
			$this->img_1->Upload->FileName = $this->img_1->CurrentValue;

		// img_2
		$this->img_2->EditAttrs["class"] = "form-control";
		$this->img_2->EditCustomAttributes = "";
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->EditValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->EditValue = "";
		}
		if (!ew_Empty($this->img_2->CurrentValue))
			$this->img_2->Upload->FileName = $this->img_2->CurrentValue;

		// img_3
		$this->img_3->EditAttrs["class"] = "form-control";
		$this->img_3->EditCustomAttributes = "";
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->EditValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->EditValue = "";
		}
		if (!ew_Empty($this->img_3->CurrentValue))
			$this->img_3->Upload->FileName = $this->img_3->CurrentValue;

		// img_4
		$this->img_4->EditAttrs["class"] = "form-control";
		$this->img_4->EditCustomAttributes = "";
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->EditValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->EditValue = "";
		}
		if (!ew_Empty($this->img_4->CurrentValue))
			$this->img_4->Upload->FileName = $this->img_4->CurrentValue;

		// img_5
		$this->img_5->EditAttrs["class"] = "form-control";
		$this->img_5->EditCustomAttributes = "";
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->EditValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->EditValue = "";
		}
		if (!ew_Empty($this->img_5->CurrentValue))
			$this->img_5->Upload->FileName = $this->img_5->CurrentValue;

		// img_6
		$this->img_6->EditAttrs["class"] = "form-control";
		$this->img_6->EditCustomAttributes = "";
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->EditValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->EditValue = "";
		}
		if (!ew_Empty($this->img_6->CurrentValue))
			$this->img_6->Upload->FileName = $this->img_6->CurrentValue;

		// img_7
		$this->img_7->EditAttrs["class"] = "form-control";
		$this->img_7->EditCustomAttributes = "";
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->EditValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->EditValue = "";
		}
		if (!ew_Empty($this->img_7->CurrentValue))
			$this->img_7->Upload->FileName = $this->img_7->CurrentValue;

		// img_8
		$this->img_8->EditAttrs["class"] = "form-control";
		$this->img_8->EditCustomAttributes = "";
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->EditValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->EditValue = "";
		}
		if (!ew_Empty($this->img_8->CurrentValue))
			$this->img_8->Upload->FileName = $this->img_8->CurrentValue;

		// img_9
		$this->img_9->EditAttrs["class"] = "form-control";
		$this->img_9->EditCustomAttributes = "";
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->EditValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->EditValue = "";
		}
		if (!ew_Empty($this->img_9->CurrentValue))
			$this->img_9->Upload->FileName = $this->img_9->CurrentValue;

		// img_10
		$this->img_10->EditAttrs["class"] = "form-control";
		$this->img_10->EditCustomAttributes = "";
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->EditValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->EditValue = "";
		}
		if (!ew_Empty($this->img_10->CurrentValue))
			$this->img_10->Upload->FileName = $this->img_10->CurrentValue;

		// video_servidor
		$this->video_servidor->EditCustomAttributes = "";
		$this->video_servidor->EditValue = $this->video_servidor->Options(FALSE);

		// video
		$this->video->EditAttrs["class"] = "form-control";
		$this->video->EditCustomAttributes = "";
		$this->video->EditValue = $this->video->CurrentValue;
		$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->prov_id->Exportable) $Doc->ExportCaption($this->prov_id);
					if ($this->loca_id->Exportable) $Doc->ExportCaption($this->loca_id);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->tipo_id->Exportable) $Doc->ExportCaption($this->tipo_id);
					if ($this->actividad->Exportable) $Doc->ExportCaption($this->actividad);
					if ($this->oper_tipo->Exportable) $Doc->ExportCaption($this->oper_tipo);
					if ($this->descripcion->Exportable) $Doc->ExportCaption($this->descripcion);
					if ($this->img_1->Exportable) $Doc->ExportCaption($this->img_1);
					if ($this->img_2->Exportable) $Doc->ExportCaption($this->img_2);
					if ($this->img_3->Exportable) $Doc->ExportCaption($this->img_3);
					if ($this->img_4->Exportable) $Doc->ExportCaption($this->img_4);
					if ($this->img_5->Exportable) $Doc->ExportCaption($this->img_5);
					if ($this->img_6->Exportable) $Doc->ExportCaption($this->img_6);
					if ($this->img_7->Exportable) $Doc->ExportCaption($this->img_7);
					if ($this->img_8->Exportable) $Doc->ExportCaption($this->img_8);
					if ($this->img_9->Exportable) $Doc->ExportCaption($this->img_9);
					if ($this->img_10->Exportable) $Doc->ExportCaption($this->img_10);
					if ($this->video_servidor->Exportable) $Doc->ExportCaption($this->video_servidor);
					if ($this->video->Exportable) $Doc->ExportCaption($this->video);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->prov_id->Exportable) $Doc->ExportCaption($this->prov_id);
					if ($this->loca_id->Exportable) $Doc->ExportCaption($this->loca_id);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->tipo_id->Exportable) $Doc->ExportCaption($this->tipo_id);
					if ($this->actividad->Exportable) $Doc->ExportCaption($this->actividad);
					if ($this->oper_tipo->Exportable) $Doc->ExportCaption($this->oper_tipo);
					if ($this->img_1->Exportable) $Doc->ExportCaption($this->img_1);
					if ($this->img_2->Exportable) $Doc->ExportCaption($this->img_2);
					if ($this->img_3->Exportable) $Doc->ExportCaption($this->img_3);
					if ($this->img_4->Exportable) $Doc->ExportCaption($this->img_4);
					if ($this->img_5->Exportable) $Doc->ExportCaption($this->img_5);
					if ($this->img_6->Exportable) $Doc->ExportCaption($this->img_6);
					if ($this->img_7->Exportable) $Doc->ExportCaption($this->img_7);
					if ($this->img_8->Exportable) $Doc->ExportCaption($this->img_8);
					if ($this->img_9->Exportable) $Doc->ExportCaption($this->img_9);
					if ($this->img_10->Exportable) $Doc->ExportCaption($this->img_10);
					if ($this->video_servidor->Exportable) $Doc->ExportCaption($this->video_servidor);
					if ($this->video->Exportable) $Doc->ExportCaption($this->video);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->prov_id->Exportable) $Doc->ExportField($this->prov_id);
						if ($this->loca_id->Exportable) $Doc->ExportField($this->loca_id);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->tipo_id->Exportable) $Doc->ExportField($this->tipo_id);
						if ($this->actividad->Exportable) $Doc->ExportField($this->actividad);
						if ($this->oper_tipo->Exportable) $Doc->ExportField($this->oper_tipo);
						if ($this->descripcion->Exportable) $Doc->ExportField($this->descripcion);
						if ($this->img_1->Exportable) $Doc->ExportField($this->img_1);
						if ($this->img_2->Exportable) $Doc->ExportField($this->img_2);
						if ($this->img_3->Exportable) $Doc->ExportField($this->img_3);
						if ($this->img_4->Exportable) $Doc->ExportField($this->img_4);
						if ($this->img_5->Exportable) $Doc->ExportField($this->img_5);
						if ($this->img_6->Exportable) $Doc->ExportField($this->img_6);
						if ($this->img_7->Exportable) $Doc->ExportField($this->img_7);
						if ($this->img_8->Exportable) $Doc->ExportField($this->img_8);
						if ($this->img_9->Exportable) $Doc->ExportField($this->img_9);
						if ($this->img_10->Exportable) $Doc->ExportField($this->img_10);
						if ($this->video_servidor->Exportable) $Doc->ExportField($this->video_servidor);
						if ($this->video->Exportable) $Doc->ExportField($this->video);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->prov_id->Exportable) $Doc->ExportField($this->prov_id);
						if ($this->loca_id->Exportable) $Doc->ExportField($this->loca_id);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->tipo_id->Exportable) $Doc->ExportField($this->tipo_id);
						if ($this->actividad->Exportable) $Doc->ExportField($this->actividad);
						if ($this->oper_tipo->Exportable) $Doc->ExportField($this->oper_tipo);
						if ($this->img_1->Exportable) $Doc->ExportField($this->img_1);
						if ($this->img_2->Exportable) $Doc->ExportField($this->img_2);
						if ($this->img_3->Exportable) $Doc->ExportField($this->img_3);
						if ($this->img_4->Exportable) $Doc->ExportField($this->img_4);
						if ($this->img_5->Exportable) $Doc->ExportField($this->img_5);
						if ($this->img_6->Exportable) $Doc->ExportField($this->img_6);
						if ($this->img_7->Exportable) $Doc->ExportField($this->img_7);
						if ($this->img_8->Exportable) $Doc->ExportField($this->img_8);
						if ($this->img_9->Exportable) $Doc->ExportField($this->img_9);
						if ($this->img_10->Exportable) $Doc->ExportField($this->img_10);
						if ($this->video_servidor->Exportable) $Doc->ExportField($this->video_servidor);
						if ($this->video->Exportable) $Doc->ExportField($this->video);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
