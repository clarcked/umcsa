<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$staff_search = NULL; // Initialize page object first

class cstaff_search extends cstaff {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'staff';

	// Page object name
	var $PageObjName = 'staff_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (staff)
		if (!isset($GLOBALS["staff"]) || get_class($GLOBALS["staff"]) == "cstaff") {
			$GLOBALS["staff"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["staff"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'staff', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("stafflist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->nombre->SetVisibility();
		$this->grupo1->SetVisibility();
		$this->grupo2->SetVisibility();
		$this->grupo3->SetVisibility();
		$this->grupo4->SetVisibility();
		$this->subgrupo->SetVisibility();
		$this->foto->SetVisibility();
		$this->cv->SetVisibility();
		$this->_email->SetVisibility();
		$this->tel->SetVisibility();
		$this->orden->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $staff;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($staff);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "stafflist.php" . "?" . $sSrchStr;
						$this->Page_Terminate($sSrchStr); // Go to list page
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->id); // id
		$this->BuildSearchUrl($sSrchUrl, $this->nombre); // nombre
		$this->BuildSearchUrl($sSrchUrl, $this->grupo1); // grupo1
		$this->BuildSearchUrl($sSrchUrl, $this->grupo2); // grupo2
		$this->BuildSearchUrl($sSrchUrl, $this->grupo3); // grupo3
		$this->BuildSearchUrl($sSrchUrl, $this->grupo4); // grupo4
		$this->BuildSearchUrl($sSrchUrl, $this->subgrupo); // subgrupo
		$this->BuildSearchUrl($sSrchUrl, $this->foto); // foto
		$this->BuildSearchUrl($sSrchUrl, $this->cv); // cv
		$this->BuildSearchUrl($sSrchUrl, $this->_email); // email
		$this->BuildSearchUrl($sSrchUrl, $this->tel); // tel
		$this->BuildSearchUrl($sSrchUrl, $this->orden); // orden
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_id"));
		$this->id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_id");

		// nombre
		$this->nombre->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_nombre"));
		$this->nombre->AdvancedSearch->SearchOperator = $objForm->GetValue("z_nombre");

		// grupo1
		$this->grupo1->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_grupo1"));
		$this->grupo1->AdvancedSearch->SearchOperator = $objForm->GetValue("z_grupo1");

		// grupo2
		$this->grupo2->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_grupo2"));
		$this->grupo2->AdvancedSearch->SearchOperator = $objForm->GetValue("z_grupo2");

		// grupo3
		$this->grupo3->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_grupo3"));
		$this->grupo3->AdvancedSearch->SearchOperator = $objForm->GetValue("z_grupo3");

		// grupo4
		$this->grupo4->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_grupo4"));
		$this->grupo4->AdvancedSearch->SearchOperator = $objForm->GetValue("z_grupo4");

		// subgrupo
		$this->subgrupo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_subgrupo"));
		$this->subgrupo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_subgrupo");

		// foto
		$this->foto->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_foto"));
		$this->foto->AdvancedSearch->SearchOperator = $objForm->GetValue("z_foto");

		// cv
		$this->cv->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_cv"));
		$this->cv->AdvancedSearch->SearchOperator = $objForm->GetValue("z_cv");

		// email
		$this->_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x__email"));
		$this->_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z__email");

		// tel
		$this->tel->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_tel"));
		$this->tel->AdvancedSearch->SearchOperator = $objForm->GetValue("z_tel");

		// orden
		$this->orden->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_orden"));
		$this->orden->AdvancedSearch->SearchOperator = $objForm->GetValue("z_orden");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// nombre
		// grupo1
		// grupo2
		// grupo3
		// grupo4
		// subgrupo
		// foto
		// cv
		// email
		// tel
		// orden

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// nombre
		$this->nombre->ViewValue = $this->nombre->CurrentValue;
		$this->nombre->ViewCustomAttributes = "";

		// grupo1
		if (strval($this->grupo1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo1->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo1->ViewValue = $this->grupo1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo1->ViewValue = $this->grupo1->CurrentValue;
			}
		} else {
			$this->grupo1->ViewValue = NULL;
		}
		$this->grupo1->ViewCustomAttributes = "";

		// grupo2
		if (strval($this->grupo2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo2->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo2->ViewValue = $this->grupo2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo2->ViewValue = $this->grupo2->CurrentValue;
			}
		} else {
			$this->grupo2->ViewValue = NULL;
		}
		$this->grupo2->ViewCustomAttributes = "";

		// grupo3
		if (strval($this->grupo3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo3->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo3->ViewValue = $this->grupo3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo3->ViewValue = $this->grupo3->CurrentValue;
			}
		} else {
			$this->grupo3->ViewValue = NULL;
		}
		$this->grupo3->ViewCustomAttributes = "";

		// grupo4
		if (strval($this->grupo4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo4->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo4->ViewValue = $this->grupo4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo4->ViewValue = $this->grupo4->CurrentValue;
			}
		} else {
			$this->grupo4->ViewValue = NULL;
		}
		$this->grupo4->ViewCustomAttributes = "";

		// subgrupo
		if (strval($this->subgrupo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->subgrupo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_subgrupos`";
		$sWhereWrk = "";
		$this->subgrupo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->subgrupo->ViewValue = $this->subgrupo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->subgrupo->ViewValue = $this->subgrupo->CurrentValue;
			}
		} else {
			$this->subgrupo->ViewValue = NULL;
		}
		$this->subgrupo->ViewCustomAttributes = "";

		// foto
		if (!ew_Empty($this->foto->Upload->DbValue)) {
			$this->foto->ViewValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->ViewValue = "";
		}
		$this->foto->ViewCustomAttributes = "";

		// cv
		if (!ew_Empty($this->cv->Upload->DbValue)) {
			$this->cv->ViewValue = $this->cv->Upload->DbValue;
		} else {
			$this->cv->ViewValue = "";
		}
		$this->cv->ViewCustomAttributes = "";

		// email
		$this->_email->ViewValue = $this->_email->CurrentValue;
		$this->_email->ViewCustomAttributes = "";

		// tel
		$this->tel->ViewValue = $this->tel->CurrentValue;
		$this->tel->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// nombre
			$this->nombre->LinkCustomAttributes = "";
			$this->nombre->HrefValue = "";
			$this->nombre->TooltipValue = "";

			// grupo1
			$this->grupo1->LinkCustomAttributes = "";
			$this->grupo1->HrefValue = "";
			$this->grupo1->TooltipValue = "";

			// grupo2
			$this->grupo2->LinkCustomAttributes = "";
			$this->grupo2->HrefValue = "";
			$this->grupo2->TooltipValue = "";

			// grupo3
			$this->grupo3->LinkCustomAttributes = "";
			$this->grupo3->HrefValue = "";
			$this->grupo3->TooltipValue = "";

			// grupo4
			$this->grupo4->LinkCustomAttributes = "";
			$this->grupo4->HrefValue = "";
			$this->grupo4->TooltipValue = "";

			// subgrupo
			$this->subgrupo->LinkCustomAttributes = "";
			$this->subgrupo->HrefValue = "";
			$this->subgrupo->TooltipValue = "";

			// foto
			$this->foto->LinkCustomAttributes = "";
			$this->foto->HrefValue = "";
			$this->foto->HrefValue2 = $this->foto->UploadPath . $this->foto->Upload->DbValue;
			$this->foto->TooltipValue = "";

			// cv
			$this->cv->LinkCustomAttributes = "";
			$this->cv->HrefValue = "";
			$this->cv->HrefValue2 = $this->cv->UploadPath . $this->cv->Upload->DbValue;
			$this->cv->TooltipValue = "";

			// email
			$this->_email->LinkCustomAttributes = "";
			$this->_email->HrefValue = "";
			$this->_email->TooltipValue = "";

			// tel
			$this->tel->LinkCustomAttributes = "";
			$this->tel->HrefValue = "";
			$this->tel->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// nombre
			$this->nombre->EditAttrs["class"] = "form-control";
			$this->nombre->EditCustomAttributes = "";
			$this->nombre->EditValue = ew_HtmlEncode($this->nombre->AdvancedSearch->SearchValue);
			$this->nombre->PlaceHolder = ew_RemoveHtml($this->nombre->FldCaption());

			// grupo1
			$this->grupo1->EditAttrs["class"] = "form-control";
			$this->grupo1->EditCustomAttributes = "";
			if (trim(strval($this->grupo1->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo1->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo1->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo1->EditValue = $arwrk;

			// grupo2
			$this->grupo2->EditAttrs["class"] = "form-control";
			$this->grupo2->EditCustomAttributes = "";
			if (trim(strval($this->grupo2->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo2->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo2->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo2->EditValue = $arwrk;

			// grupo3
			$this->grupo3->EditAttrs["class"] = "form-control";
			$this->grupo3->EditCustomAttributes = "";
			if (trim(strval($this->grupo3->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo3->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo3->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo3->EditValue = $arwrk;

			// grupo4
			$this->grupo4->EditAttrs["class"] = "form-control";
			$this->grupo4->EditCustomAttributes = "";
			if (trim(strval($this->grupo4->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo4->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo4->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo4->EditValue = $arwrk;

			// subgrupo
			$this->subgrupo->EditAttrs["class"] = "form-control";
			$this->subgrupo->EditCustomAttributes = "";
			if (trim(strval($this->subgrupo->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->subgrupo->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_subgrupos`";
			$sWhereWrk = "";
			$this->subgrupo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->subgrupo->EditValue = $arwrk;

			// foto
			$this->foto->EditAttrs["class"] = "form-control";
			$this->foto->EditCustomAttributes = "";
			$this->foto->EditValue = ew_HtmlEncode($this->foto->AdvancedSearch->SearchValue);
			$this->foto->PlaceHolder = ew_RemoveHtml($this->foto->FldCaption());

			// cv
			$this->cv->EditAttrs["class"] = "form-control";
			$this->cv->EditCustomAttributes = "";
			$this->cv->EditValue = ew_HtmlEncode($this->cv->AdvancedSearch->SearchValue);
			$this->cv->PlaceHolder = ew_RemoveHtml($this->cv->FldCaption());

			// email
			$this->_email->EditAttrs["class"] = "form-control";
			$this->_email->EditCustomAttributes = "";
			$this->_email->EditValue = ew_HtmlEncode($this->_email->AdvancedSearch->SearchValue);
			$this->_email->PlaceHolder = ew_RemoveHtml($this->_email->FldCaption());

			// tel
			$this->tel->EditAttrs["class"] = "form-control";
			$this->tel->EditCustomAttributes = "";
			$this->tel->EditValue = ew_HtmlEncode($this->tel->AdvancedSearch->SearchValue);
			$this->tel->PlaceHolder = ew_RemoveHtml($this->tel->FldCaption());

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->AdvancedSearch->SearchValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->orden->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->orden->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->nombre->AdvancedSearch->Load();
		$this->grupo1->AdvancedSearch->Load();
		$this->grupo2->AdvancedSearch->Load();
		$this->grupo3->AdvancedSearch->Load();
		$this->grupo4->AdvancedSearch->Load();
		$this->subgrupo->AdvancedSearch->Load();
		$this->foto->AdvancedSearch->Load();
		$this->cv->AdvancedSearch->Load();
		$this->_email->AdvancedSearch->Load();
		$this->tel->AdvancedSearch->Load();
		$this->orden->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("stafflist.php"), "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_grupo1":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo1->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo2":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo2->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo3":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo3->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo4":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo4->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_subgrupo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_subgrupos`";
			$sWhereWrk = "";
			$this->subgrupo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($staff_search)) $staff_search = new cstaff_search();

// Page init
$staff_search->Page_Init();

// Page main
$staff_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$staff_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($staff_search->IsModal) { ?>
var CurrentAdvancedSearchForm = fstaffsearch = new ew_Form("fstaffsearch", "search");
<?php } else { ?>
var CurrentForm = fstaffsearch = new ew_Form("fstaffsearch", "search");
<?php } ?>

// Form_CustomValidate event
fstaffsearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fstaffsearch.ValidateRequired = true;
<?php } else { ?>
fstaffsearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fstaffsearch.Lists["x_grupo1"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffsearch.Lists["x_grupo2"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffsearch.Lists["x_grupo3"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffsearch.Lists["x_grupo4"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffsearch.Lists["x_subgrupo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_subgrupos"};

// Form object for search
// Validate function for search

fstaffsearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($staff->id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_orden");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($staff->orden->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$staff_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $staff_search->ShowPageHeader(); ?>
<?php
$staff_search->ShowMessage();
?>
<form name="fstaffsearch" id="fstaffsearch" class="<?php echo $staff_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($staff_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $staff_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="staff">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($staff_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($staff->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label for="x_id" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_id"><?php echo $staff->id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_id" id="z_id" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->id->CellAttributes() ?>>
			<span id="el_staff_id">
<input type="text" data-table="staff" data-field="x_id" name="x_id" id="x_id" placeholder="<?php echo ew_HtmlEncode($staff->id->getPlaceHolder()) ?>" value="<?php echo $staff->id->EditValue ?>"<?php echo $staff->id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->nombre->Visible) { // nombre ?>
	<div id="r_nombre" class="form-group">
		<label for="x_nombre" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_nombre"><?php echo $staff->nombre->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_nombre" id="z_nombre" value="LIKE"></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->nombre->CellAttributes() ?>>
			<span id="el_staff_nombre">
<input type="text" data-table="staff" data-field="x_nombre" name="x_nombre" id="x_nombre" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->nombre->getPlaceHolder()) ?>" value="<?php echo $staff->nombre->EditValue ?>"<?php echo $staff->nombre->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo1->Visible) { // grupo1 ?>
	<div id="r_grupo1" class="form-group">
		<label for="x_grupo1" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_grupo1"><?php echo $staff->grupo1->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_grupo1" id="z_grupo1" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->grupo1->CellAttributes() ?>>
			<span id="el_staff_grupo1">
<select data-table="staff" data-field="x_grupo1" data-value-separator="<?php echo $staff->grupo1->DisplayValueSeparatorAttribute() ?>" id="x_grupo1" name="x_grupo1"<?php echo $staff->grupo1->EditAttributes() ?>>
<?php echo $staff->grupo1->SelectOptionListHtml("x_grupo1") ?>
</select>
<input type="hidden" name="s_x_grupo1" id="s_x_grupo1" value="<?php echo $staff->grupo1->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo2->Visible) { // grupo2 ?>
	<div id="r_grupo2" class="form-group">
		<label for="x_grupo2" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_grupo2"><?php echo $staff->grupo2->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_grupo2" id="z_grupo2" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->grupo2->CellAttributes() ?>>
			<span id="el_staff_grupo2">
<select data-table="staff" data-field="x_grupo2" data-value-separator="<?php echo $staff->grupo2->DisplayValueSeparatorAttribute() ?>" id="x_grupo2" name="x_grupo2"<?php echo $staff->grupo2->EditAttributes() ?>>
<?php echo $staff->grupo2->SelectOptionListHtml("x_grupo2") ?>
</select>
<input type="hidden" name="s_x_grupo2" id="s_x_grupo2" value="<?php echo $staff->grupo2->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo3->Visible) { // grupo3 ?>
	<div id="r_grupo3" class="form-group">
		<label for="x_grupo3" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_grupo3"><?php echo $staff->grupo3->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_grupo3" id="z_grupo3" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->grupo3->CellAttributes() ?>>
			<span id="el_staff_grupo3">
<select data-table="staff" data-field="x_grupo3" data-value-separator="<?php echo $staff->grupo3->DisplayValueSeparatorAttribute() ?>" id="x_grupo3" name="x_grupo3"<?php echo $staff->grupo3->EditAttributes() ?>>
<?php echo $staff->grupo3->SelectOptionListHtml("x_grupo3") ?>
</select>
<input type="hidden" name="s_x_grupo3" id="s_x_grupo3" value="<?php echo $staff->grupo3->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo4->Visible) { // grupo4 ?>
	<div id="r_grupo4" class="form-group">
		<label for="x_grupo4" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_grupo4"><?php echo $staff->grupo4->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_grupo4" id="z_grupo4" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->grupo4->CellAttributes() ?>>
			<span id="el_staff_grupo4">
<select data-table="staff" data-field="x_grupo4" data-value-separator="<?php echo $staff->grupo4->DisplayValueSeparatorAttribute() ?>" id="x_grupo4" name="x_grupo4"<?php echo $staff->grupo4->EditAttributes() ?>>
<?php echo $staff->grupo4->SelectOptionListHtml("x_grupo4") ?>
</select>
<input type="hidden" name="s_x_grupo4" id="s_x_grupo4" value="<?php echo $staff->grupo4->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->subgrupo->Visible) { // subgrupo ?>
	<div id="r_subgrupo" class="form-group">
		<label for="x_subgrupo" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_subgrupo"><?php echo $staff->subgrupo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_subgrupo" id="z_subgrupo" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->subgrupo->CellAttributes() ?>>
			<span id="el_staff_subgrupo">
<select data-table="staff" data-field="x_subgrupo" data-value-separator="<?php echo $staff->subgrupo->DisplayValueSeparatorAttribute() ?>" id="x_subgrupo" name="x_subgrupo"<?php echo $staff->subgrupo->EditAttributes() ?>>
<?php echo $staff->subgrupo->SelectOptionListHtml("x_subgrupo") ?>
</select>
<input type="hidden" name="s_x_subgrupo" id="s_x_subgrupo" value="<?php echo $staff->subgrupo->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->foto->Visible) { // foto ?>
	<div id="r_foto" class="form-group">
		<label class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_foto"><?php echo $staff->foto->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_foto" id="z_foto" value="LIKE"></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->foto->CellAttributes() ?>>
			<span id="el_staff_foto">
<input type="text" data-table="staff" data-field="x_foto" name="x_foto" id="x_foto" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->foto->getPlaceHolder()) ?>" value="<?php echo $staff->foto->EditValue ?>"<?php echo $staff->foto->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->cv->Visible) { // cv ?>
	<div id="r_cv" class="form-group">
		<label class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_cv"><?php echo $staff->cv->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_cv" id="z_cv" value="LIKE"></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->cv->CellAttributes() ?>>
			<span id="el_staff_cv">
<input type="text" data-table="staff" data-field="x_cv" name="x_cv" id="x_cv" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->cv->getPlaceHolder()) ?>" value="<?php echo $staff->cv->EditValue ?>"<?php echo $staff->cv->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->_email->Visible) { // email ?>
	<div id="r__email" class="form-group">
		<label for="x__email" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff__email"><?php echo $staff->_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z__email" id="z__email" value="LIKE"></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->_email->CellAttributes() ?>>
			<span id="el_staff__email">
<input type="text" data-table="staff" data-field="x__email" name="x__email" id="x__email" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->_email->getPlaceHolder()) ?>" value="<?php echo $staff->_email->EditValue ?>"<?php echo $staff->_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->tel->Visible) { // tel ?>
	<div id="r_tel" class="form-group">
		<label for="x_tel" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_tel"><?php echo $staff->tel->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_tel" id="z_tel" value="LIKE"></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->tel->CellAttributes() ?>>
			<span id="el_staff_tel">
<input type="text" data-table="staff" data-field="x_tel" name="x_tel" id="x_tel" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->tel->getPlaceHolder()) ?>" value="<?php echo $staff->tel->EditValue ?>"<?php echo $staff->tel->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($staff->orden->Visible) { // orden ?>
	<div id="r_orden" class="form-group">
		<label for="x_orden" class="<?php echo $staff_search->SearchLabelClass ?>"><span id="elh_staff_orden"><?php echo $staff->orden->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_orden" id="z_orden" value="="></p>
		</label>
		<div class="<?php echo $staff_search->SearchRightColumnClass ?>"><div<?php echo $staff->orden->CellAttributes() ?>>
			<span id="el_staff_orden">
<input type="text" data-table="staff" data-field="x_orden" name="x_orden" id="x_orden" size="30" placeholder="<?php echo ew_HtmlEncode($staff->orden->getPlaceHolder()) ?>" value="<?php echo $staff->orden->EditValue ?>"<?php echo $staff->orden->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$staff_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fstaffsearch.Init();
</script>
<?php
$staff_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$staff_search->Page_Terminate();
?>
