<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_delete = NULL; // Initialize page object first

class clotes_delete extends clotes {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("loteslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("loteslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in lotes class, lotesinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} elseif (@$_GET["a_delete"] == "1") {
			$this->CurrentAction = "D"; // Delete record directly
		} else {
			$this->CurrentAction = "D"; // Delete record directly
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("loteslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		if (array_key_exists('EV__remate_id', $rs->fields)) {
			$this->remate_id->VirtualValue = $rs->fields('EV__remate_id'); // Set up virtual field value
		} else {
			$this->remate_id->VirtualValue = ""; // Clear value
		}
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		if (array_key_exists('EV__conjunto_lote_id', $rs->fields)) {
			$this->conjunto_lote_id->VirtualValue = $rs->fields('EV__conjunto_lote_id'); // Set up virtual field value
		} else {
			$this->conjunto_lote_id->VirtualValue = ""; // Clear value
		}
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		if (array_key_exists('EV__tipo', $rs->fields)) {
			$this->tipo->VirtualValue = $rs->fields('EV__tipo'); // Set up virtual field value
		} else {
			$this->tipo->VirtualValue = ""; // Clear value
		}
		$this->categoria->setDbValue($rs->fields('categoria'));
		if (array_key_exists('EV__categoria', $rs->fields)) {
			$this->categoria->VirtualValue = $rs->fields('EV__categoria'); // Set up virtual field value
		} else {
			$this->categoria->VirtualValue = ""; // Clear value
		}
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		if (array_key_exists('EV__sub_categoria', $rs->fields)) {
			$this->sub_categoria->VirtualValue = $rs->fields('EV__sub_categoria'); // Set up virtual field value
		} else {
			$this->sub_categoria->VirtualValue = ""; // Clear value
		}
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		if (array_key_exists('EV__localidad', $rs->fields)) {
			$this->localidad->VirtualValue = $rs->fields('EV__localidad'); // Set up virtual field value
		} else {
			$this->localidad->VirtualValue = ""; // Clear value
		}
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->venta_tipo->DbValue = $row['venta_tipo'];
		$this->activo->DbValue = $row['activo'];
		$this->vendido->DbValue = $row['vendido'];
		$this->codigo->DbValue = $row['codigo'];
		$this->remate_id->DbValue = $row['remate_id'];
		$this->conjunto_lote_id->DbValue = $row['conjunto_lote_id'];
		$this->orden->DbValue = $row['orden'];
		$this->video->DbValue = $row['video'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->precio->DbValue = $row['precio'];
		$this->fecha_film->DbValue = $row['fecha_film'];
		$this->establecimiento->DbValue = $row['establecimiento'];
		$this->remitente->DbValue = $row['remitente'];
		$this->tipo->DbValue = $row['tipo'];
		$this->categoria->DbValue = $row['categoria'];
		$this->sub_categoria->DbValue = $row['sub_categoria'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->peso->DbValue = $row['peso'];
		$this->caracteristicas->DbValue = $row['caracteristicas'];
		$this->provincia->DbValue = $row['provincia'];
		$this->localidad->DbValue = $row['localidad'];
		$this->lugar->DbValue = $row['lugar'];
		$this->titulo->DbValue = $row['titulo'];
		$this->edad_aprox->DbValue = $row['edad_aprox'];
		$this->trazabilidad->DbValue = $row['trazabilidad'];
		$this->mio_mio->DbValue = $row['mio_mio'];
		$this->garrapata->DbValue = $row['garrapata'];
		$this->observaciones->DbValue = $row['observaciones'];
		$this->plazo->DbValue = $row['plazo'];
		$this->visitas->DbValue = $row['visitas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("loteslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_delete)) $lotes_delete = new clotes_delete();

// Page init
$lotes_delete->Page_Init();

// Page main
$lotes_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = flotesdelete = new ew_Form("flotesdelete", "delete");

// Form_CustomValidate event
flotesdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
flotesdelete.ValidateRequired = true;
<?php } else { ?>
flotesdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
flotesdelete.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
flotesdelete.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
flotesdelete.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
flotesdelete.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
flotesdelete.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
flotesdelete.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
flotesdelete.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
flotesdelete.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
flotesdelete.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
flotesdelete.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"localidades"};
flotesdelete.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
flotesdelete.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
flotesdelete.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesdelete.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $lotes_delete->ShowPageHeader(); ?>
<?php
$lotes_delete->ShowMessage();
?>
<form name="flotesdelete" id="flotesdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($lotes_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $lotes->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($lotes->id->Visible) { // id ?>
		<th><span id="elh_lotes_id" class="lotes_id"><?php echo $lotes->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
		<th><span id="elh_lotes_venta_tipo" class="lotes_venta_tipo"><?php echo $lotes->venta_tipo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
		<th><span id="elh_lotes_activo" class="lotes_activo"><?php echo $lotes->activo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
		<th><span id="elh_lotes_vendido" class="lotes_vendido"><?php echo $lotes->vendido->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
		<th><span id="elh_lotes_codigo" class="lotes_codigo"><?php echo $lotes->codigo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
		<th><span id="elh_lotes_remate_id" class="lotes_remate_id"><?php echo $lotes->remate_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
		<th><span id="elh_lotes_conjunto_lote_id" class="lotes_conjunto_lote_id"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
		<th><span id="elh_lotes_orden" class="lotes_orden"><?php echo $lotes->orden->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
		<th><span id="elh_lotes_video" class="lotes_video"><?php echo $lotes->video->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
		<th><span id="elh_lotes_foto_1" class="lotes_foto_1"><?php echo $lotes->foto_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
		<th><span id="elh_lotes_foto_2" class="lotes_foto_2"><?php echo $lotes->foto_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
		<th><span id="elh_lotes_foto_3" class="lotes_foto_3"><?php echo $lotes->foto_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
		<th><span id="elh_lotes_foto_4" class="lotes_foto_4"><?php echo $lotes->foto_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
		<th><span id="elh_lotes_precio" class="lotes_precio"><?php echo $lotes->precio->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
		<th><span id="elh_lotes_fecha_film" class="lotes_fecha_film"><?php echo $lotes->fecha_film->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
		<th><span id="elh_lotes_establecimiento" class="lotes_establecimiento"><?php echo $lotes->establecimiento->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
		<th><span id="elh_lotes_remitente" class="lotes_remitente"><?php echo $lotes->remitente->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
		<th><span id="elh_lotes_tipo" class="lotes_tipo"><?php echo $lotes->tipo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
		<th><span id="elh_lotes_categoria" class="lotes_categoria"><?php echo $lotes->categoria->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
		<th><span id="elh_lotes_sub_categoria" class="lotes_sub_categoria"><?php echo $lotes->sub_categoria->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
		<th><span id="elh_lotes_cantidad" class="lotes_cantidad"><?php echo $lotes->cantidad->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
		<th><span id="elh_lotes_peso" class="lotes_peso"><?php echo $lotes->peso->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
		<th><span id="elh_lotes_caracteristicas" class="lotes_caracteristicas"><?php echo $lotes->caracteristicas->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
		<th><span id="elh_lotes_provincia" class="lotes_provincia"><?php echo $lotes->provincia->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
		<th><span id="elh_lotes_localidad" class="lotes_localidad"><?php echo $lotes->localidad->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
		<th><span id="elh_lotes_lugar" class="lotes_lugar"><?php echo $lotes->lugar->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
		<th><span id="elh_lotes_titulo" class="lotes_titulo"><?php echo $lotes->titulo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
		<th><span id="elh_lotes_edad_aprox" class="lotes_edad_aprox"><?php echo $lotes->edad_aprox->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
		<th><span id="elh_lotes_trazabilidad" class="lotes_trazabilidad"><?php echo $lotes->trazabilidad->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
		<th><span id="elh_lotes_mio_mio" class="lotes_mio_mio"><?php echo $lotes->mio_mio->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
		<th><span id="elh_lotes_garrapata" class="lotes_garrapata"><?php echo $lotes->garrapata->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
		<th><span id="elh_lotes_plazo" class="lotes_plazo"><?php echo $lotes->plazo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
		<th><span id="elh_lotes_visitas" class="lotes_visitas"><?php echo $lotes->visitas->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$lotes_delete->RecCnt = 0;
$i = 0;
while (!$lotes_delete->Recordset->EOF) {
	$lotes_delete->RecCnt++;
	$lotes_delete->RowCnt++;

	// Set row properties
	$lotes->ResetAttrs();
	$lotes->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$lotes_delete->LoadRowValues($lotes_delete->Recordset);

	// Render row
	$lotes_delete->RenderRow();
?>
	<tr<?php echo $lotes->RowAttributes() ?>>
<?php if ($lotes->id->Visible) { // id ?>
		<td<?php echo $lotes->id->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_id" class="lotes_id">
<span<?php echo $lotes->id->ViewAttributes() ?>>
<?php echo $lotes->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
		<td<?php echo $lotes->venta_tipo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_venta_tipo" class="lotes_venta_tipo">
<span<?php echo $lotes->venta_tipo->ViewAttributes() ?>>
<?php echo $lotes->venta_tipo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
		<td<?php echo $lotes->activo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_activo" class="lotes_activo">
<span<?php echo $lotes->activo->ViewAttributes() ?>>
<?php echo $lotes->activo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
		<td<?php echo $lotes->vendido->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_vendido" class="lotes_vendido">
<span<?php echo $lotes->vendido->ViewAttributes() ?>>
<?php echo $lotes->vendido->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
		<td<?php echo $lotes->codigo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_codigo" class="lotes_codigo">
<span<?php echo $lotes->codigo->ViewAttributes() ?>>
<?php echo $lotes->codigo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
		<td<?php echo $lotes->remate_id->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_remate_id" class="lotes_remate_id">
<span<?php echo $lotes->remate_id->ViewAttributes() ?>>
<?php echo $lotes->remate_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
		<td<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_conjunto_lote_id" class="lotes_conjunto_lote_id">
<span<?php echo $lotes->conjunto_lote_id->ViewAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
		<td<?php echo $lotes->orden->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_orden" class="lotes_orden">
<span<?php echo $lotes->orden->ViewAttributes() ?>>
<?php echo $lotes->orden->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
		<td<?php echo $lotes->video->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_video" class="lotes_video">
<span<?php echo $lotes->video->ViewAttributes() ?>>
<?php echo $lotes->video->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
		<td<?php echo $lotes->foto_1->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_foto_1" class="lotes_foto_1">
<span<?php echo $lotes->foto_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_1, $lotes->foto_1->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
		<td<?php echo $lotes->foto_2->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_foto_2" class="lotes_foto_2">
<span<?php echo $lotes->foto_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_2, $lotes->foto_2->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
		<td<?php echo $lotes->foto_3->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_foto_3" class="lotes_foto_3">
<span<?php echo $lotes->foto_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_3, $lotes->foto_3->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
		<td<?php echo $lotes->foto_4->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_foto_4" class="lotes_foto_4">
<span<?php echo $lotes->foto_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_4, $lotes->foto_4->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
		<td<?php echo $lotes->precio->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_precio" class="lotes_precio">
<span<?php echo $lotes->precio->ViewAttributes() ?>>
<?php echo $lotes->precio->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
		<td<?php echo $lotes->fecha_film->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_fecha_film" class="lotes_fecha_film">
<span<?php echo $lotes->fecha_film->ViewAttributes() ?>>
<?php echo $lotes->fecha_film->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
		<td<?php echo $lotes->establecimiento->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_establecimiento" class="lotes_establecimiento">
<span<?php echo $lotes->establecimiento->ViewAttributes() ?>>
<?php echo $lotes->establecimiento->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
		<td<?php echo $lotes->remitente->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_remitente" class="lotes_remitente">
<span<?php echo $lotes->remitente->ViewAttributes() ?>>
<?php echo $lotes->remitente->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
		<td<?php echo $lotes->tipo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_tipo" class="lotes_tipo">
<span<?php echo $lotes->tipo->ViewAttributes() ?>>
<?php echo $lotes->tipo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
		<td<?php echo $lotes->categoria->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_categoria" class="lotes_categoria">
<span<?php echo $lotes->categoria->ViewAttributes() ?>>
<?php echo $lotes->categoria->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
		<td<?php echo $lotes->sub_categoria->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_sub_categoria" class="lotes_sub_categoria">
<span<?php echo $lotes->sub_categoria->ViewAttributes() ?>>
<?php echo $lotes->sub_categoria->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
		<td<?php echo $lotes->cantidad->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_cantidad" class="lotes_cantidad">
<span<?php echo $lotes->cantidad->ViewAttributes() ?>>
<?php echo $lotes->cantidad->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
		<td<?php echo $lotes->peso->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_peso" class="lotes_peso">
<span<?php echo $lotes->peso->ViewAttributes() ?>>
<?php echo $lotes->peso->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
		<td<?php echo $lotes->caracteristicas->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_caracteristicas" class="lotes_caracteristicas">
<span<?php echo $lotes->caracteristicas->ViewAttributes() ?>>
<?php echo $lotes->caracteristicas->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
		<td<?php echo $lotes->provincia->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_provincia" class="lotes_provincia">
<span<?php echo $lotes->provincia->ViewAttributes() ?>>
<?php echo $lotes->provincia->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
		<td<?php echo $lotes->localidad->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_localidad" class="lotes_localidad">
<span<?php echo $lotes->localidad->ViewAttributes() ?>>
<?php echo $lotes->localidad->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
		<td<?php echo $lotes->lugar->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_lugar" class="lotes_lugar">
<span<?php echo $lotes->lugar->ViewAttributes() ?>>
<?php echo $lotes->lugar->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
		<td<?php echo $lotes->titulo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_titulo" class="lotes_titulo">
<span<?php echo $lotes->titulo->ViewAttributes() ?>>
<?php echo $lotes->titulo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
		<td<?php echo $lotes->edad_aprox->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_edad_aprox" class="lotes_edad_aprox">
<span<?php echo $lotes->edad_aprox->ViewAttributes() ?>>
<?php echo $lotes->edad_aprox->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
		<td<?php echo $lotes->trazabilidad->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_trazabilidad" class="lotes_trazabilidad">
<span<?php echo $lotes->trazabilidad->ViewAttributes() ?>>
<?php echo $lotes->trazabilidad->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
		<td<?php echo $lotes->mio_mio->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_mio_mio" class="lotes_mio_mio">
<span<?php echo $lotes->mio_mio->ViewAttributes() ?>>
<?php echo $lotes->mio_mio->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
		<td<?php echo $lotes->garrapata->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_garrapata" class="lotes_garrapata">
<span<?php echo $lotes->garrapata->ViewAttributes() ?>>
<?php echo $lotes->garrapata->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
		<td<?php echo $lotes->plazo->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_plazo" class="lotes_plazo">
<span<?php echo $lotes->plazo->ViewAttributes() ?>>
<?php echo $lotes->plazo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
		<td<?php echo $lotes->visitas->CellAttributes() ?>>
<span id="el<?php echo $lotes_delete->RowCnt ?>_lotes_visitas" class="lotes_visitas">
<span<?php echo $lotes->visitas->ViewAttributes() ?>>
<?php echo $lotes->visitas->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$lotes_delete->Recordset->MoveNext();
}
$lotes_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $lotes_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
flotesdelete.Init();
</script>
<?php
$lotes_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$lotes_delete->Page_Terminate();
?>
