<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "notiliniersinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$notiliniers_list = NULL; // Initialize page object first

class cnotiliniers_list extends cnotiliniers {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'notiliniers';

	// Page object name
	var $PageObjName = 'notiliniers_list';

	// Grid form hidden field names
	var $FormName = 'fnotilinierslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (notiliniers)
		if (!isset($GLOBALS["notiliniers"]) || get_class($GLOBALS["notiliniers"]) == "cnotiliniers") {
			$GLOBALS["notiliniers"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["notiliniers"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "notiliniersadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "notiliniersdelete.php";
		$this->MultiUpdateUrl = "notiliniersupdate.php";

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'notiliniers', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fnotilinierslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->fecha->SetVisibility();
		$this->activa->SetVisibility();
		$this->titulo->SetVisibility();
		$this->bajada->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->epigrafe_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->epigrafe_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->epigrafe_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->epigrafe_4->SetVisibility();
		$this->galeria_id->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $notiliniers;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($notiliniers);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Process filter list
			$this->ProcessFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Get list of filters
	function GetFilterList() {
		global $UserProfile;

		// Load server side filters
		if (EW_SEARCH_FILTER_OPTION == "Server") {
			$sSavedFilterList = $UserProfile->GetSearchFilters(CurrentUserName(), "fnotilinierslistsrch");
		} else {
			$sSavedFilterList = "";
		}

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->id->AdvancedSearch->ToJSON(), ","); // Field id
		$sFilterList = ew_Concat($sFilterList, $this->fecha->AdvancedSearch->ToJSON(), ","); // Field fecha
		$sFilterList = ew_Concat($sFilterList, $this->activa->AdvancedSearch->ToJSON(), ","); // Field activa
		$sFilterList = ew_Concat($sFilterList, $this->titulo->AdvancedSearch->ToJSON(), ","); // Field titulo
		$sFilterList = ew_Concat($sFilterList, $this->bajada->AdvancedSearch->ToJSON(), ","); // Field bajada
		$sFilterList = ew_Concat($sFilterList, $this->cuerpo->AdvancedSearch->ToJSON(), ","); // Field cuerpo
		$sFilterList = ew_Concat($sFilterList, $this->foto_1->AdvancedSearch->ToJSON(), ","); // Field foto_1
		$sFilterList = ew_Concat($sFilterList, $this->epigrafe_1->AdvancedSearch->ToJSON(), ","); // Field epigrafe_1
		$sFilterList = ew_Concat($sFilterList, $this->foto_2->AdvancedSearch->ToJSON(), ","); // Field foto_2
		$sFilterList = ew_Concat($sFilterList, $this->epigrafe_2->AdvancedSearch->ToJSON(), ","); // Field epigrafe_2
		$sFilterList = ew_Concat($sFilterList, $this->foto_3->AdvancedSearch->ToJSON(), ","); // Field foto_3
		$sFilterList = ew_Concat($sFilterList, $this->epigrafe_3->AdvancedSearch->ToJSON(), ","); // Field epigrafe_3
		$sFilterList = ew_Concat($sFilterList, $this->foto_4->AdvancedSearch->ToJSON(), ","); // Field foto_4
		$sFilterList = ew_Concat($sFilterList, $this->epigrafe_4->AdvancedSearch->ToJSON(), ","); // Field epigrafe_4
		$sFilterList = ew_Concat($sFilterList, $this->galeria_id->AdvancedSearch->ToJSON(), ","); // Field galeria_id
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}
		$sFilterList = preg_replace('/,$/', "", $sFilterList);

		// Return filter list in json
		if ($sFilterList <> "")
			$sFilterList = "\"data\":{" . $sFilterList . "}";
		if ($sSavedFilterList <> "") {
			if ($sFilterList <> "")
				$sFilterList .= ",";
			$sFilterList .= "\"filters\":" . $sSavedFilterList;
		}
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Process filter list
	function ProcessFilterList() {
		global $UserProfile;
		if (@$_POST["ajax"] == "savefilters") { // Save filter request (Ajax)
			$filters = ew_StripSlashes(@$_POST["filters"]);
			$UserProfile->SetSearchFilters(CurrentUserName(), "fnotilinierslistsrch", $filters);

			// Clean output buffer
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			echo ew_ArrayToJson(array(array("success" => TRUE))); // Success
			$this->Page_Terminate();
			exit();
		} elseif (@$_POST["cmd"] == "resetfilter") {
			$this->RestoreFilterList();
		}
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field id
		$this->id->AdvancedSearch->SearchValue = @$filter["x_id"];
		$this->id->AdvancedSearch->SearchOperator = @$filter["z_id"];
		$this->id->AdvancedSearch->SearchCondition = @$filter["v_id"];
		$this->id->AdvancedSearch->SearchValue2 = @$filter["y_id"];
		$this->id->AdvancedSearch->SearchOperator2 = @$filter["w_id"];
		$this->id->AdvancedSearch->Save();

		// Field fecha
		$this->fecha->AdvancedSearch->SearchValue = @$filter["x_fecha"];
		$this->fecha->AdvancedSearch->SearchOperator = @$filter["z_fecha"];
		$this->fecha->AdvancedSearch->SearchCondition = @$filter["v_fecha"];
		$this->fecha->AdvancedSearch->SearchValue2 = @$filter["y_fecha"];
		$this->fecha->AdvancedSearch->SearchOperator2 = @$filter["w_fecha"];
		$this->fecha->AdvancedSearch->Save();

		// Field activa
		$this->activa->AdvancedSearch->SearchValue = @$filter["x_activa"];
		$this->activa->AdvancedSearch->SearchOperator = @$filter["z_activa"];
		$this->activa->AdvancedSearch->SearchCondition = @$filter["v_activa"];
		$this->activa->AdvancedSearch->SearchValue2 = @$filter["y_activa"];
		$this->activa->AdvancedSearch->SearchOperator2 = @$filter["w_activa"];
		$this->activa->AdvancedSearch->Save();

		// Field titulo
		$this->titulo->AdvancedSearch->SearchValue = @$filter["x_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator = @$filter["z_titulo"];
		$this->titulo->AdvancedSearch->SearchCondition = @$filter["v_titulo"];
		$this->titulo->AdvancedSearch->SearchValue2 = @$filter["y_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator2 = @$filter["w_titulo"];
		$this->titulo->AdvancedSearch->Save();

		// Field bajada
		$this->bajada->AdvancedSearch->SearchValue = @$filter["x_bajada"];
		$this->bajada->AdvancedSearch->SearchOperator = @$filter["z_bajada"];
		$this->bajada->AdvancedSearch->SearchCondition = @$filter["v_bajada"];
		$this->bajada->AdvancedSearch->SearchValue2 = @$filter["y_bajada"];
		$this->bajada->AdvancedSearch->SearchOperator2 = @$filter["w_bajada"];
		$this->bajada->AdvancedSearch->Save();

		// Field cuerpo
		$this->cuerpo->AdvancedSearch->SearchValue = @$filter["x_cuerpo"];
		$this->cuerpo->AdvancedSearch->SearchOperator = @$filter["z_cuerpo"];
		$this->cuerpo->AdvancedSearch->SearchCondition = @$filter["v_cuerpo"];
		$this->cuerpo->AdvancedSearch->SearchValue2 = @$filter["y_cuerpo"];
		$this->cuerpo->AdvancedSearch->SearchOperator2 = @$filter["w_cuerpo"];
		$this->cuerpo->AdvancedSearch->Save();

		// Field foto_1
		$this->foto_1->AdvancedSearch->SearchValue = @$filter["x_foto_1"];
		$this->foto_1->AdvancedSearch->SearchOperator = @$filter["z_foto_1"];
		$this->foto_1->AdvancedSearch->SearchCondition = @$filter["v_foto_1"];
		$this->foto_1->AdvancedSearch->SearchValue2 = @$filter["y_foto_1"];
		$this->foto_1->AdvancedSearch->SearchOperator2 = @$filter["w_foto_1"];
		$this->foto_1->AdvancedSearch->Save();

		// Field epigrafe_1
		$this->epigrafe_1->AdvancedSearch->SearchValue = @$filter["x_epigrafe_1"];
		$this->epigrafe_1->AdvancedSearch->SearchOperator = @$filter["z_epigrafe_1"];
		$this->epigrafe_1->AdvancedSearch->SearchCondition = @$filter["v_epigrafe_1"];
		$this->epigrafe_1->AdvancedSearch->SearchValue2 = @$filter["y_epigrafe_1"];
		$this->epigrafe_1->AdvancedSearch->SearchOperator2 = @$filter["w_epigrafe_1"];
		$this->epigrafe_1->AdvancedSearch->Save();

		// Field foto_2
		$this->foto_2->AdvancedSearch->SearchValue = @$filter["x_foto_2"];
		$this->foto_2->AdvancedSearch->SearchOperator = @$filter["z_foto_2"];
		$this->foto_2->AdvancedSearch->SearchCondition = @$filter["v_foto_2"];
		$this->foto_2->AdvancedSearch->SearchValue2 = @$filter["y_foto_2"];
		$this->foto_2->AdvancedSearch->SearchOperator2 = @$filter["w_foto_2"];
		$this->foto_2->AdvancedSearch->Save();

		// Field epigrafe_2
		$this->epigrafe_2->AdvancedSearch->SearchValue = @$filter["x_epigrafe_2"];
		$this->epigrafe_2->AdvancedSearch->SearchOperator = @$filter["z_epigrafe_2"];
		$this->epigrafe_2->AdvancedSearch->SearchCondition = @$filter["v_epigrafe_2"];
		$this->epigrafe_2->AdvancedSearch->SearchValue2 = @$filter["y_epigrafe_2"];
		$this->epigrafe_2->AdvancedSearch->SearchOperator2 = @$filter["w_epigrafe_2"];
		$this->epigrafe_2->AdvancedSearch->Save();

		// Field foto_3
		$this->foto_3->AdvancedSearch->SearchValue = @$filter["x_foto_3"];
		$this->foto_3->AdvancedSearch->SearchOperator = @$filter["z_foto_3"];
		$this->foto_3->AdvancedSearch->SearchCondition = @$filter["v_foto_3"];
		$this->foto_3->AdvancedSearch->SearchValue2 = @$filter["y_foto_3"];
		$this->foto_3->AdvancedSearch->SearchOperator2 = @$filter["w_foto_3"];
		$this->foto_3->AdvancedSearch->Save();

		// Field epigrafe_3
		$this->epigrafe_3->AdvancedSearch->SearchValue = @$filter["x_epigrafe_3"];
		$this->epigrafe_3->AdvancedSearch->SearchOperator = @$filter["z_epigrafe_3"];
		$this->epigrafe_3->AdvancedSearch->SearchCondition = @$filter["v_epigrafe_3"];
		$this->epigrafe_3->AdvancedSearch->SearchValue2 = @$filter["y_epigrafe_3"];
		$this->epigrafe_3->AdvancedSearch->SearchOperator2 = @$filter["w_epigrafe_3"];
		$this->epigrafe_3->AdvancedSearch->Save();

		// Field foto_4
		$this->foto_4->AdvancedSearch->SearchValue = @$filter["x_foto_4"];
		$this->foto_4->AdvancedSearch->SearchOperator = @$filter["z_foto_4"];
		$this->foto_4->AdvancedSearch->SearchCondition = @$filter["v_foto_4"];
		$this->foto_4->AdvancedSearch->SearchValue2 = @$filter["y_foto_4"];
		$this->foto_4->AdvancedSearch->SearchOperator2 = @$filter["w_foto_4"];
		$this->foto_4->AdvancedSearch->Save();

		// Field epigrafe_4
		$this->epigrafe_4->AdvancedSearch->SearchValue = @$filter["x_epigrafe_4"];
		$this->epigrafe_4->AdvancedSearch->SearchOperator = @$filter["z_epigrafe_4"];
		$this->epigrafe_4->AdvancedSearch->SearchCondition = @$filter["v_epigrafe_4"];
		$this->epigrafe_4->AdvancedSearch->SearchValue2 = @$filter["y_epigrafe_4"];
		$this->epigrafe_4->AdvancedSearch->SearchOperator2 = @$filter["w_epigrafe_4"];
		$this->epigrafe_4->AdvancedSearch->Save();

		// Field galeria_id
		$this->galeria_id->AdvancedSearch->SearchValue = @$filter["x_galeria_id"];
		$this->galeria_id->AdvancedSearch->SearchOperator = @$filter["z_galeria_id"];
		$this->galeria_id->AdvancedSearch->SearchCondition = @$filter["v_galeria_id"];
		$this->galeria_id->AdvancedSearch->SearchValue2 = @$filter["y_galeria_id"];
		$this->galeria_id->AdvancedSearch->SearchOperator2 = @$filter["w_galeria_id"];
		$this->galeria_id->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->id, $Default, FALSE); // id
		$this->BuildSearchSql($sWhere, $this->fecha, $Default, FALSE); // fecha
		$this->BuildSearchSql($sWhere, $this->activa, $Default, FALSE); // activa
		$this->BuildSearchSql($sWhere, $this->titulo, $Default, FALSE); // titulo
		$this->BuildSearchSql($sWhere, $this->bajada, $Default, FALSE); // bajada
		$this->BuildSearchSql($sWhere, $this->cuerpo, $Default, FALSE); // cuerpo
		$this->BuildSearchSql($sWhere, $this->foto_1, $Default, FALSE); // foto_1
		$this->BuildSearchSql($sWhere, $this->epigrafe_1, $Default, FALSE); // epigrafe_1
		$this->BuildSearchSql($sWhere, $this->foto_2, $Default, FALSE); // foto_2
		$this->BuildSearchSql($sWhere, $this->epigrafe_2, $Default, FALSE); // epigrafe_2
		$this->BuildSearchSql($sWhere, $this->foto_3, $Default, FALSE); // foto_3
		$this->BuildSearchSql($sWhere, $this->epigrafe_3, $Default, FALSE); // epigrafe_3
		$this->BuildSearchSql($sWhere, $this->foto_4, $Default, FALSE); // foto_4
		$this->BuildSearchSql($sWhere, $this->epigrafe_4, $Default, FALSE); // epigrafe_4
		$this->BuildSearchSql($sWhere, $this->galeria_id, $Default, FALSE); // galeria_id

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->id->AdvancedSearch->Save(); // id
			$this->fecha->AdvancedSearch->Save(); // fecha
			$this->activa->AdvancedSearch->Save(); // activa
			$this->titulo->AdvancedSearch->Save(); // titulo
			$this->bajada->AdvancedSearch->Save(); // bajada
			$this->cuerpo->AdvancedSearch->Save(); // cuerpo
			$this->foto_1->AdvancedSearch->Save(); // foto_1
			$this->epigrafe_1->AdvancedSearch->Save(); // epigrafe_1
			$this->foto_2->AdvancedSearch->Save(); // foto_2
			$this->epigrafe_2->AdvancedSearch->Save(); // epigrafe_2
			$this->foto_3->AdvancedSearch->Save(); // foto_3
			$this->epigrafe_3->AdvancedSearch->Save(); // epigrafe_3
			$this->foto_4->AdvancedSearch->Save(); // foto_4
			$this->epigrafe_4->AdvancedSearch->Save(); // epigrafe_4
			$this->galeria_id->AdvancedSearch->Save(); // galeria_id
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1)
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE || $Fld->FldDataType == EW_DATATYPE_TIME) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->titulo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->bajada, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->cuerpo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->epigrafe_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->epigrafe_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->epigrafe_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_4, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->epigrafe_4, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSQL(&$Where, &$Fld, $arKeywords, $type) {
		global $EW_BASIC_SEARCH_IGNORE_PATTERN;
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if ($EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace($EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->fecha->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->activa->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->titulo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->bajada->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->cuerpo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->epigrafe_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->epigrafe_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->epigrafe_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->epigrafe_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->galeria_id->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->id->AdvancedSearch->UnsetSession();
		$this->fecha->AdvancedSearch->UnsetSession();
		$this->activa->AdvancedSearch->UnsetSession();
		$this->titulo->AdvancedSearch->UnsetSession();
		$this->bajada->AdvancedSearch->UnsetSession();
		$this->cuerpo->AdvancedSearch->UnsetSession();
		$this->foto_1->AdvancedSearch->UnsetSession();
		$this->epigrafe_1->AdvancedSearch->UnsetSession();
		$this->foto_2->AdvancedSearch->UnsetSession();
		$this->epigrafe_2->AdvancedSearch->UnsetSession();
		$this->foto_3->AdvancedSearch->UnsetSession();
		$this->epigrafe_3->AdvancedSearch->UnsetSession();
		$this->foto_4->AdvancedSearch->UnsetSession();
		$this->epigrafe_4->AdvancedSearch->UnsetSession();
		$this->galeria_id->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->id->AdvancedSearch->Load();
		$this->fecha->AdvancedSearch->Load();
		$this->activa->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->bajada->AdvancedSearch->Load();
		$this->cuerpo->AdvancedSearch->Load();
		$this->foto_1->AdvancedSearch->Load();
		$this->epigrafe_1->AdvancedSearch->Load();
		$this->foto_2->AdvancedSearch->Load();
		$this->epigrafe_2->AdvancedSearch->Load();
		$this->foto_3->AdvancedSearch->Load();
		$this->epigrafe_3->AdvancedSearch->Load();
		$this->foto_4->AdvancedSearch->Load();
		$this->epigrafe_4->AdvancedSearch->Load();
		$this->galeria_id->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->fecha); // fecha
			$this->UpdateSort($this->activa); // activa
			$this->UpdateSort($this->titulo); // titulo
			$this->UpdateSort($this->bajada); // bajada
			$this->UpdateSort($this->foto_1); // foto_1
			$this->UpdateSort($this->epigrafe_1); // epigrafe_1
			$this->UpdateSort($this->foto_2); // foto_2
			$this->UpdateSort($this->epigrafe_2); // epigrafe_2
			$this->UpdateSort($this->foto_3); // foto_3
			$this->UpdateSort($this->epigrafe_3); // epigrafe_3
			$this->UpdateSort($this->foto_4); // foto_4
			$this->UpdateSort($this->epigrafe_4); // epigrafe_4
			$this->UpdateSort($this->galeria_id); // galeria_id
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->id->setSort("");
				$this->fecha->setSort("");
				$this->activa->setSort("");
				$this->titulo->setSort("");
				$this->bajada->setSort("");
				$this->foto_1->setSort("");
				$this->epigrafe_1->setSort("");
				$this->foto_2->setSort("");
				$this->epigrafe_2->setSort("");
				$this->foto_3->setSort("");
				$this->epigrafe_3->setSort("");
				$this->foto_4->setSort("");
				$this->epigrafe_4->setSort("");
				$this->galeria_id->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		$viewcaption = ew_HtmlTitle($Language->Phrase("ViewLink"));
		if ($Security->CanView()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . $viewcaption . "\" data-caption=\"" . $viewcaption . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		$editcaption = ew_HtmlTitle($Language->Phrase("EditLink"));
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		$copycaption = ew_HtmlTitle($Language->Phrase("CopyLink"));
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("AddLink"));
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$option = $options["action"];

		// Add multi delete
		$item = &$option->Add("multidelete");
		$item->Body = "<a class=\"ewAction ewMultiDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fnotilinierslist,url:'" . $this->MultiDeleteUrl . "',msg:ewLanguage.Phrase('DeleteConfirmMsg')});return false;\">" . $Language->Phrase("DeleteSelectedLink") . "</a>";
		$item->Visible = ($Security->CanDelete());

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fnotilinierslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fnotilinierslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fnotilinierslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fnotilinierslistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_id"]);
		if ($this->id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->id->AdvancedSearch->SearchOperator = @$_GET["z_id"];

		// fecha
		$this->fecha->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_fecha"]);
		if ($this->fecha->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->fecha->AdvancedSearch->SearchOperator = @$_GET["z_fecha"];

		// activa
		$this->activa->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_activa"]);
		if ($this->activa->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->activa->AdvancedSearch->SearchOperator = @$_GET["z_activa"];

		// titulo
		$this->titulo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_titulo"]);
		if ($this->titulo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->titulo->AdvancedSearch->SearchOperator = @$_GET["z_titulo"];

		// bajada
		$this->bajada->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_bajada"]);
		if ($this->bajada->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->bajada->AdvancedSearch->SearchOperator = @$_GET["z_bajada"];

		// cuerpo
		$this->cuerpo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_cuerpo"]);
		if ($this->cuerpo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->cuerpo->AdvancedSearch->SearchOperator = @$_GET["z_cuerpo"];

		// foto_1
		$this->foto_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_1"]);
		if ($this->foto_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_1->AdvancedSearch->SearchOperator = @$_GET["z_foto_1"];

		// epigrafe_1
		$this->epigrafe_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_epigrafe_1"]);
		if ($this->epigrafe_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->epigrafe_1->AdvancedSearch->SearchOperator = @$_GET["z_epigrafe_1"];

		// foto_2
		$this->foto_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_2"]);
		if ($this->foto_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_2->AdvancedSearch->SearchOperator = @$_GET["z_foto_2"];

		// epigrafe_2
		$this->epigrafe_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_epigrafe_2"]);
		if ($this->epigrafe_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->epigrafe_2->AdvancedSearch->SearchOperator = @$_GET["z_epigrafe_2"];

		// foto_3
		$this->foto_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_3"]);
		if ($this->foto_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_3->AdvancedSearch->SearchOperator = @$_GET["z_foto_3"];

		// epigrafe_3
		$this->epigrafe_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_epigrafe_3"]);
		if ($this->epigrafe_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->epigrafe_3->AdvancedSearch->SearchOperator = @$_GET["z_epigrafe_3"];

		// foto_4
		$this->foto_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_4"]);
		if ($this->foto_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_4->AdvancedSearch->SearchOperator = @$_GET["z_foto_4"];

		// epigrafe_4
		$this->epigrafe_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_epigrafe_4"]);
		if ($this->epigrafe_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->epigrafe_4->AdvancedSearch->SearchOperator = @$_GET["z_epigrafe_4"];

		// galeria_id
		$this->galeria_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_galeria_id"]);
		if ($this->galeria_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->galeria_id->AdvancedSearch->SearchOperator = @$_GET["z_galeria_id"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->bajada->setDbValue($rs->fields('bajada'));
		$this->cuerpo->setDbValue($rs->fields('cuerpo'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->epigrafe_1->setDbValue($rs->fields('epigrafe_1'));
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->epigrafe_2->setDbValue($rs->fields('epigrafe_2'));
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->epigrafe_3->setDbValue($rs->fields('epigrafe_3'));
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->epigrafe_4->setDbValue($rs->fields('epigrafe_4'));
		$this->galeria_id->setDbValue($rs->fields('galeria_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->activa->DbValue = $row['activa'];
		$this->titulo->DbValue = $row['titulo'];
		$this->bajada->DbValue = $row['bajada'];
		$this->cuerpo->DbValue = $row['cuerpo'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->epigrafe_1->DbValue = $row['epigrafe_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->epigrafe_2->DbValue = $row['epigrafe_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->epigrafe_3->DbValue = $row['epigrafe_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->epigrafe_4->DbValue = $row['epigrafe_4'];
		$this->galeria_id->DbValue = $row['galeria_id'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// fecha
		// activa
		// titulo
		// bajada
		// cuerpo
		// foto_1
		// epigrafe_1
		// foto_2
		// epigrafe_2
		// foto_3
		// epigrafe_3
		// foto_4
		// epigrafe_4
		// galeria_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// bajada
		$this->bajada->ViewValue = $this->bajada->CurrentValue;
		$this->bajada->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// epigrafe_1
		$this->epigrafe_1->ViewValue = $this->epigrafe_1->CurrentValue;
		$this->epigrafe_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// epigrafe_2
		$this->epigrafe_2->ViewValue = $this->epigrafe_2->CurrentValue;
		$this->epigrafe_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// epigrafe_3
		$this->epigrafe_3->ViewValue = $this->epigrafe_3->CurrentValue;
		$this->epigrafe_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// epigrafe_4
		$this->epigrafe_4->ViewValue = $this->epigrafe_4->CurrentValue;
		$this->epigrafe_4->ViewCustomAttributes = "";

		// galeria_id
		if (strval($this->galeria_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->galeria_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `galerias_imagenes`";
		$sWhereWrk = "";
		$this->galeria_id->LookupFilters = array();
		$lookuptblfilter = "`contenido_tipo`='LINIERS'";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->galeria_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->galeria_id->ViewValue = $this->galeria_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->galeria_id->ViewValue = $this->galeria_id->CurrentValue;
			}
		} else {
			$this->galeria_id->ViewValue = NULL;
		}
		$this->galeria_id->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// bajada
			$this->bajada->LinkCustomAttributes = "";
			$this->bajada->HrefValue = "";
			$this->bajada->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// epigrafe_1
			$this->epigrafe_1->LinkCustomAttributes = "";
			$this->epigrafe_1->HrefValue = "";
			$this->epigrafe_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// epigrafe_2
			$this->epigrafe_2->LinkCustomAttributes = "";
			$this->epigrafe_2->HrefValue = "";
			$this->epigrafe_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// epigrafe_3
			$this->epigrafe_3->LinkCustomAttributes = "";
			$this->epigrafe_3->HrefValue = "";
			$this->epigrafe_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// epigrafe_4
			$this->epigrafe_4->LinkCustomAttributes = "";
			$this->epigrafe_4->HrefValue = "";
			$this->epigrafe_4->TooltipValue = "";

			// galeria_id
			$this->galeria_id->LinkCustomAttributes = "";
			$this->galeria_id->HrefValue = "";
			$this->galeria_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// activa
			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->AdvancedSearch->SearchValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// bajada
			$this->bajada->EditAttrs["class"] = "form-control";
			$this->bajada->EditCustomAttributes = "";
			$this->bajada->EditValue = ew_HtmlEncode($this->bajada->AdvancedSearch->SearchValue);
			$this->bajada->PlaceHolder = ew_RemoveHtml($this->bajada->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			$this->foto_1->EditValue = ew_HtmlEncode($this->foto_1->AdvancedSearch->SearchValue);
			$this->foto_1->PlaceHolder = ew_RemoveHtml($this->foto_1->FldCaption());

			// epigrafe_1
			$this->epigrafe_1->EditAttrs["class"] = "form-control";
			$this->epigrafe_1->EditCustomAttributes = "";
			$this->epigrafe_1->EditValue = ew_HtmlEncode($this->epigrafe_1->AdvancedSearch->SearchValue);
			$this->epigrafe_1->PlaceHolder = ew_RemoveHtml($this->epigrafe_1->FldCaption());

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			$this->foto_2->EditValue = ew_HtmlEncode($this->foto_2->AdvancedSearch->SearchValue);
			$this->foto_2->PlaceHolder = ew_RemoveHtml($this->foto_2->FldCaption());

			// epigrafe_2
			$this->epigrafe_2->EditAttrs["class"] = "form-control";
			$this->epigrafe_2->EditCustomAttributes = "";
			$this->epigrafe_2->EditValue = ew_HtmlEncode($this->epigrafe_2->AdvancedSearch->SearchValue);
			$this->epigrafe_2->PlaceHolder = ew_RemoveHtml($this->epigrafe_2->FldCaption());

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			$this->foto_3->EditValue = ew_HtmlEncode($this->foto_3->AdvancedSearch->SearchValue);
			$this->foto_3->PlaceHolder = ew_RemoveHtml($this->foto_3->FldCaption());

			// epigrafe_3
			$this->epigrafe_3->EditAttrs["class"] = "form-control";
			$this->epigrafe_3->EditCustomAttributes = "";
			$this->epigrafe_3->EditValue = ew_HtmlEncode($this->epigrafe_3->AdvancedSearch->SearchValue);
			$this->epigrafe_3->PlaceHolder = ew_RemoveHtml($this->epigrafe_3->FldCaption());

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			$this->foto_4->EditValue = ew_HtmlEncode($this->foto_4->AdvancedSearch->SearchValue);
			$this->foto_4->PlaceHolder = ew_RemoveHtml($this->foto_4->FldCaption());

			// epigrafe_4
			$this->epigrafe_4->EditAttrs["class"] = "form-control";
			$this->epigrafe_4->EditCustomAttributes = "";
			$this->epigrafe_4->EditValue = ew_HtmlEncode($this->epigrafe_4->AdvancedSearch->SearchValue);
			$this->epigrafe_4->PlaceHolder = ew_RemoveHtml($this->epigrafe_4->FldCaption());

			// galeria_id
			$this->galeria_id->EditAttrs["class"] = "form-control";
			$this->galeria_id->EditCustomAttributes = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->fecha->AdvancedSearch->Load();
		$this->activa->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->bajada->AdvancedSearch->Load();
		$this->cuerpo->AdvancedSearch->Load();
		$this->foto_1->AdvancedSearch->Load();
		$this->epigrafe_1->AdvancedSearch->Load();
		$this->foto_2->AdvancedSearch->Load();
		$this->epigrafe_2->AdvancedSearch->Load();
		$this->foto_3->AdvancedSearch->Load();
		$this->epigrafe_3->AdvancedSearch->Load();
		$this->foto_4->AdvancedSearch->Load();
		$this->epigrafe_4->AdvancedSearch->Load();
		$this->galeria_id->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_notiliniers\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_notiliniers',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fnotilinierslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($notiliniers_list)) $notiliniers_list = new cnotiliniers_list();

// Page init
$notiliniers_list->Page_Init();

// Page main
$notiliniers_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$notiliniers_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($notiliniers->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fnotilinierslist = new ew_Form("fnotilinierslist", "list");
fnotilinierslist.FormKeyCountName = '<?php echo $notiliniers_list->FormKeyCountName ?>';

// Form_CustomValidate event
fnotilinierslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fnotilinierslist.ValidateRequired = true;
<?php } else { ?>
fnotilinierslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fnotilinierslist.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fnotilinierslist.Lists["x_activa"].Options = <?php echo json_encode($notiliniers->activa->Options()) ?>;
fnotilinierslist.Lists["x_galeria_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"galerias_imagenes"};

// Form object for search
var CurrentSearchForm = fnotilinierslistsrch = new ew_Form("fnotilinierslistsrch");

// Validate function for search
fnotilinierslistsrch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
fnotilinierslistsrch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fnotilinierslistsrch.ValidateRequired = true; // Use JavaScript validation
<?php } else { ?>
fnotilinierslistsrch.ValidateRequired = false; // No JavaScript validation
<?php } ?>

// Dynamic selection lists
fnotilinierslistsrch.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fnotilinierslistsrch.Lists["x_activa"].Options = <?php echo json_encode($notiliniers->activa->Options()) ?>;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($notiliniers->Export == "") { ?>
<div class="ewToolbar">
<?php if ($notiliniers->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($notiliniers_list->TotalRecs > 0 && $notiliniers_list->ExportOptions->Visible()) { ?>
<?php $notiliniers_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($notiliniers_list->SearchOptions->Visible()) { ?>
<?php $notiliniers_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($notiliniers_list->FilterOptions->Visible()) { ?>
<?php $notiliniers_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($notiliniers->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
	$bSelectLimit = $notiliniers_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($notiliniers_list->TotalRecs <= 0)
			$notiliniers_list->TotalRecs = $notiliniers->SelectRecordCount();
	} else {
		if (!$notiliniers_list->Recordset && ($notiliniers_list->Recordset = $notiliniers_list->LoadRecordset()))
			$notiliniers_list->TotalRecs = $notiliniers_list->Recordset->RecordCount();
	}
	$notiliniers_list->StartRec = 1;
	if ($notiliniers_list->DisplayRecs <= 0 || ($notiliniers->Export <> "" && $notiliniers->ExportAll)) // Display all records
		$notiliniers_list->DisplayRecs = $notiliniers_list->TotalRecs;
	if (!($notiliniers->Export <> "" && $notiliniers->ExportAll))
		$notiliniers_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$notiliniers_list->Recordset = $notiliniers_list->LoadRecordset($notiliniers_list->StartRec-1, $notiliniers_list->DisplayRecs);

	// Set no record found message
	if ($notiliniers->CurrentAction == "" && $notiliniers_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$notiliniers_list->setWarningMessage(ew_DeniedMsg());
		if ($notiliniers_list->SearchWhere == "0=101")
			$notiliniers_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$notiliniers_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
$notiliniers_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($notiliniers->Export == "" && $notiliniers->CurrentAction == "") { ?>
<form name="fnotilinierslistsrch" id="fnotilinierslistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($notiliniers_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fnotilinierslistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="notiliniers">
	<div class="ewBasicSearch">
<?php
if ($gsSearchError == "")
	$notiliniers_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$notiliniers->RowType = EW_ROWTYPE_SEARCH;

// Render row
$notiliniers->ResetAttrs();
$notiliniers_list->RenderRow();
?>
<div id="xsr_1" class="ewRow">
<?php if ($notiliniers->activa->Visible) { // activa ?>
	<div id="xsc_activa" class="ewCell form-group">
		<label for="x_activa" class="ewSearchCaption ewLabel"><?php echo $notiliniers->activa->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_activa" id="z_activa" value="="></span>
		<span class="ewSearchField">
<select data-table="notiliniers" data-field="x_activa" data-value-separator="<?php echo $notiliniers->activa->DisplayValueSeparatorAttribute() ?>" id="x_activa" name="x_activa"<?php echo $notiliniers->activa->EditAttributes() ?>>
<?php echo $notiliniers->activa->SelectOptionListHtml("x_activa") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($notiliniers_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($notiliniers_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $notiliniers_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($notiliniers_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($notiliniers_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($notiliniers_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($notiliniers_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $notiliniers_list->ShowPageHeader(); ?>
<?php
$notiliniers_list->ShowMessage();
?>
<?php if ($notiliniers_list->TotalRecs > 0 || $notiliniers->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid notiliniers">
<?php if ($notiliniers->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($notiliniers->CurrentAction <> "gridadd" && $notiliniers->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($notiliniers_list->Pager)) $notiliniers_list->Pager = new cPrevNextPager($notiliniers_list->StartRec, $notiliniers_list->DisplayRecs, $notiliniers_list->TotalRecs) ?>
<?php if ($notiliniers_list->Pager->RecordCount > 0 && $notiliniers_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($notiliniers_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($notiliniers_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $notiliniers_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($notiliniers_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($notiliniers_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $notiliniers_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $notiliniers_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $notiliniers_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $notiliniers_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($notiliniers_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fnotilinierslist" id="fnotilinierslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($notiliniers_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $notiliniers_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="notiliniers">
<div id="gmp_notiliniers" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($notiliniers_list->TotalRecs > 0 || $notiliniers->CurrentAction == "gridedit") { ?>
<table id="tbl_notilinierslist" class="table ewTable">
<?php echo $notiliniers->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$notiliniers_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$notiliniers_list->RenderListOptions();

// Render list options (header, left)
$notiliniers_list->ListOptions->Render("header", "left");
?>
<?php if ($notiliniers->id->Visible) { // id ?>
	<?php if ($notiliniers->SortUrl($notiliniers->id) == "") { ?>
		<th data-name="id"><div id="elh_notiliniers_id" class="notiliniers_id"><div class="ewTableHeaderCaption"><?php echo $notiliniers->id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->id) ?>',1);"><div id="elh_notiliniers_id" class="notiliniers_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->fecha->Visible) { // fecha ?>
	<?php if ($notiliniers->SortUrl($notiliniers->fecha) == "") { ?>
		<th data-name="fecha"><div id="elh_notiliniers_fecha" class="notiliniers_fecha"><div class="ewTableHeaderCaption"><?php echo $notiliniers->fecha->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="fecha"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->fecha) ?>',1);"><div id="elh_notiliniers_fecha" class="notiliniers_fecha">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->fecha->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->fecha->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->fecha->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->activa->Visible) { // activa ?>
	<?php if ($notiliniers->SortUrl($notiliniers->activa) == "") { ?>
		<th data-name="activa"><div id="elh_notiliniers_activa" class="notiliniers_activa"><div class="ewTableHeaderCaption"><?php echo $notiliniers->activa->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="activa"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->activa) ?>',1);"><div id="elh_notiliniers_activa" class="notiliniers_activa">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->activa->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->activa->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->activa->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->titulo->Visible) { // titulo ?>
	<?php if ($notiliniers->SortUrl($notiliniers->titulo) == "") { ?>
		<th data-name="titulo"><div id="elh_notiliniers_titulo" class="notiliniers_titulo"><div class="ewTableHeaderCaption"><?php echo $notiliniers->titulo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="titulo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->titulo) ?>',1);"><div id="elh_notiliniers_titulo" class="notiliniers_titulo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->titulo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->titulo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->titulo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->bajada->Visible) { // bajada ?>
	<?php if ($notiliniers->SortUrl($notiliniers->bajada) == "") { ?>
		<th data-name="bajada"><div id="elh_notiliniers_bajada" class="notiliniers_bajada"><div class="ewTableHeaderCaption"><?php echo $notiliniers->bajada->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="bajada"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->bajada) ?>',1);"><div id="elh_notiliniers_bajada" class="notiliniers_bajada">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->bajada->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->bajada->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->bajada->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->foto_1->Visible) { // foto_1 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->foto_1) == "") { ?>
		<th data-name="foto_1"><div id="elh_notiliniers_foto_1" class="notiliniers_foto_1"><div class="ewTableHeaderCaption"><?php echo $notiliniers->foto_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->foto_1) ?>',1);"><div id="elh_notiliniers_foto_1" class="notiliniers_foto_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->foto_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->foto_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->foto_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->epigrafe_1->Visible) { // epigrafe_1 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->epigrafe_1) == "") { ?>
		<th data-name="epigrafe_1"><div id="elh_notiliniers_epigrafe_1" class="notiliniers_epigrafe_1"><div class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="epigrafe_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->epigrafe_1) ?>',1);"><div id="elh_notiliniers_epigrafe_1" class="notiliniers_epigrafe_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->epigrafe_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->epigrafe_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->foto_2->Visible) { // foto_2 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->foto_2) == "") { ?>
		<th data-name="foto_2"><div id="elh_notiliniers_foto_2" class="notiliniers_foto_2"><div class="ewTableHeaderCaption"><?php echo $notiliniers->foto_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->foto_2) ?>',1);"><div id="elh_notiliniers_foto_2" class="notiliniers_foto_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->foto_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->foto_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->foto_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->epigrafe_2->Visible) { // epigrafe_2 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->epigrafe_2) == "") { ?>
		<th data-name="epigrafe_2"><div id="elh_notiliniers_epigrafe_2" class="notiliniers_epigrafe_2"><div class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="epigrafe_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->epigrafe_2) ?>',1);"><div id="elh_notiliniers_epigrafe_2" class="notiliniers_epigrafe_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->epigrafe_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->epigrafe_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->foto_3->Visible) { // foto_3 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->foto_3) == "") { ?>
		<th data-name="foto_3"><div id="elh_notiliniers_foto_3" class="notiliniers_foto_3"><div class="ewTableHeaderCaption"><?php echo $notiliniers->foto_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->foto_3) ?>',1);"><div id="elh_notiliniers_foto_3" class="notiliniers_foto_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->foto_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->foto_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->foto_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->epigrafe_3->Visible) { // epigrafe_3 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->epigrafe_3) == "") { ?>
		<th data-name="epigrafe_3"><div id="elh_notiliniers_epigrafe_3" class="notiliniers_epigrafe_3"><div class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="epigrafe_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->epigrafe_3) ?>',1);"><div id="elh_notiliniers_epigrafe_3" class="notiliniers_epigrafe_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->epigrafe_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->epigrafe_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->foto_4->Visible) { // foto_4 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->foto_4) == "") { ?>
		<th data-name="foto_4"><div id="elh_notiliniers_foto_4" class="notiliniers_foto_4"><div class="ewTableHeaderCaption"><?php echo $notiliniers->foto_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->foto_4) ?>',1);"><div id="elh_notiliniers_foto_4" class="notiliniers_foto_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->foto_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->foto_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->foto_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->epigrafe_4->Visible) { // epigrafe_4 ?>
	<?php if ($notiliniers->SortUrl($notiliniers->epigrafe_4) == "") { ?>
		<th data-name="epigrafe_4"><div id="elh_notiliniers_epigrafe_4" class="notiliniers_epigrafe_4"><div class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="epigrafe_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->epigrafe_4) ?>',1);"><div id="elh_notiliniers_epigrafe_4" class="notiliniers_epigrafe_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->epigrafe_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->epigrafe_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->epigrafe_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($notiliniers->galeria_id->Visible) { // galeria_id ?>
	<?php if ($notiliniers->SortUrl($notiliniers->galeria_id) == "") { ?>
		<th data-name="galeria_id"><div id="elh_notiliniers_galeria_id" class="notiliniers_galeria_id"><div class="ewTableHeaderCaption"><?php echo $notiliniers->galeria_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="galeria_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $notiliniers->SortUrl($notiliniers->galeria_id) ?>',1);"><div id="elh_notiliniers_galeria_id" class="notiliniers_galeria_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $notiliniers->galeria_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($notiliniers->galeria_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($notiliniers->galeria_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$notiliniers_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($notiliniers->ExportAll && $notiliniers->Export <> "") {
	$notiliniers_list->StopRec = $notiliniers_list->TotalRecs;
} else {

	// Set the last record to display
	if ($notiliniers_list->TotalRecs > $notiliniers_list->StartRec + $notiliniers_list->DisplayRecs - 1)
		$notiliniers_list->StopRec = $notiliniers_list->StartRec + $notiliniers_list->DisplayRecs - 1;
	else
		$notiliniers_list->StopRec = $notiliniers_list->TotalRecs;
}
$notiliniers_list->RecCnt = $notiliniers_list->StartRec - 1;
if ($notiliniers_list->Recordset && !$notiliniers_list->Recordset->EOF) {
	$notiliniers_list->Recordset->MoveFirst();
	$bSelectLimit = $notiliniers_list->UseSelectLimit;
	if (!$bSelectLimit && $notiliniers_list->StartRec > 1)
		$notiliniers_list->Recordset->Move($notiliniers_list->StartRec - 1);
} elseif (!$notiliniers->AllowAddDeleteRow && $notiliniers_list->StopRec == 0) {
	$notiliniers_list->StopRec = $notiliniers->GridAddRowCount;
}

// Initialize aggregate
$notiliniers->RowType = EW_ROWTYPE_AGGREGATEINIT;
$notiliniers->ResetAttrs();
$notiliniers_list->RenderRow();
while ($notiliniers_list->RecCnt < $notiliniers_list->StopRec) {
	$notiliniers_list->RecCnt++;
	if (intval($notiliniers_list->RecCnt) >= intval($notiliniers_list->StartRec)) {
		$notiliniers_list->RowCnt++;

		// Set up key count
		$notiliniers_list->KeyCount = $notiliniers_list->RowIndex;

		// Init row class and style
		$notiliniers->ResetAttrs();
		$notiliniers->CssClass = "";
		if ($notiliniers->CurrentAction == "gridadd") {
		} else {
			$notiliniers_list->LoadRowValues($notiliniers_list->Recordset); // Load row values
		}
		$notiliniers->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$notiliniers->RowAttrs = array_merge($notiliniers->RowAttrs, array('data-rowindex'=>$notiliniers_list->RowCnt, 'id'=>'r' . $notiliniers_list->RowCnt . '_notiliniers', 'data-rowtype'=>$notiliniers->RowType));

		// Render row
		$notiliniers_list->RenderRow();

		// Render list options
		$notiliniers_list->RenderListOptions();
?>
	<tr<?php echo $notiliniers->RowAttributes() ?>>
<?php

// Render list options (body, left)
$notiliniers_list->ListOptions->Render("body", "left", $notiliniers_list->RowCnt);
?>
	<?php if ($notiliniers->id->Visible) { // id ?>
		<td data-name="id"<?php echo $notiliniers->id->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_id" class="notiliniers_id">
<span<?php echo $notiliniers->id->ViewAttributes() ?>>
<?php echo $notiliniers->id->ListViewValue() ?></span>
</span>
<a id="<?php echo $notiliniers_list->PageObjName . "_row_" . $notiliniers_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($notiliniers->fecha->Visible) { // fecha ?>
		<td data-name="fecha"<?php echo $notiliniers->fecha->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_fecha" class="notiliniers_fecha">
<span<?php echo $notiliniers->fecha->ViewAttributes() ?>>
<?php echo $notiliniers->fecha->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->activa->Visible) { // activa ?>
		<td data-name="activa"<?php echo $notiliniers->activa->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_activa" class="notiliniers_activa">
<span<?php echo $notiliniers->activa->ViewAttributes() ?>>
<?php echo $notiliniers->activa->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->titulo->Visible) { // titulo ?>
		<td data-name="titulo"<?php echo $notiliniers->titulo->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_titulo" class="notiliniers_titulo">
<span<?php echo $notiliniers->titulo->ViewAttributes() ?>>
<?php echo $notiliniers->titulo->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->bajada->Visible) { // bajada ?>
		<td data-name="bajada"<?php echo $notiliniers->bajada->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_bajada" class="notiliniers_bajada">
<span<?php echo $notiliniers->bajada->ViewAttributes() ?>>
<?php echo $notiliniers->bajada->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->foto_1->Visible) { // foto_1 ?>
		<td data-name="foto_1"<?php echo $notiliniers->foto_1->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_foto_1" class="notiliniers_foto_1">
<span<?php echo $notiliniers->foto_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_1, $notiliniers->foto_1->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->epigrafe_1->Visible) { // epigrafe_1 ?>
		<td data-name="epigrafe_1"<?php echo $notiliniers->epigrafe_1->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_epigrafe_1" class="notiliniers_epigrafe_1">
<span<?php echo $notiliniers->epigrafe_1->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_1->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->foto_2->Visible) { // foto_2 ?>
		<td data-name="foto_2"<?php echo $notiliniers->foto_2->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_foto_2" class="notiliniers_foto_2">
<span<?php echo $notiliniers->foto_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_2, $notiliniers->foto_2->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->epigrafe_2->Visible) { // epigrafe_2 ?>
		<td data-name="epigrafe_2"<?php echo $notiliniers->epigrafe_2->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_epigrafe_2" class="notiliniers_epigrafe_2">
<span<?php echo $notiliniers->epigrafe_2->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_2->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->foto_3->Visible) { // foto_3 ?>
		<td data-name="foto_3"<?php echo $notiliniers->foto_3->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_foto_3" class="notiliniers_foto_3">
<span<?php echo $notiliniers->foto_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_3, $notiliniers->foto_3->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->epigrafe_3->Visible) { // epigrafe_3 ?>
		<td data-name="epigrafe_3"<?php echo $notiliniers->epigrafe_3->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_epigrafe_3" class="notiliniers_epigrafe_3">
<span<?php echo $notiliniers->epigrafe_3->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_3->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->foto_4->Visible) { // foto_4 ?>
		<td data-name="foto_4"<?php echo $notiliniers->foto_4->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_foto_4" class="notiliniers_foto_4">
<span<?php echo $notiliniers->foto_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_4, $notiliniers->foto_4->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->epigrafe_4->Visible) { // epigrafe_4 ?>
		<td data-name="epigrafe_4"<?php echo $notiliniers->epigrafe_4->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_epigrafe_4" class="notiliniers_epigrafe_4">
<span<?php echo $notiliniers->epigrafe_4->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_4->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($notiliniers->galeria_id->Visible) { // galeria_id ?>
		<td data-name="galeria_id"<?php echo $notiliniers->galeria_id->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_list->RowCnt ?>_notiliniers_galeria_id" class="notiliniers_galeria_id">
<span<?php echo $notiliniers->galeria_id->ViewAttributes() ?>>
<?php echo $notiliniers->galeria_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$notiliniers_list->ListOptions->Render("body", "right", $notiliniers_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($notiliniers->CurrentAction <> "gridadd")
		$notiliniers_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($notiliniers->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($notiliniers_list->Recordset)
	$notiliniers_list->Recordset->Close();
?>
<?php if ($notiliniers->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($notiliniers->CurrentAction <> "gridadd" && $notiliniers->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($notiliniers_list->Pager)) $notiliniers_list->Pager = new cPrevNextPager($notiliniers_list->StartRec, $notiliniers_list->DisplayRecs, $notiliniers_list->TotalRecs) ?>
<?php if ($notiliniers_list->Pager->RecordCount > 0 && $notiliniers_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($notiliniers_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($notiliniers_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $notiliniers_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($notiliniers_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($notiliniers_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $notiliniers_list->PageUrl() ?>start=<?php echo $notiliniers_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $notiliniers_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $notiliniers_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $notiliniers_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $notiliniers_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($notiliniers_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($notiliniers_list->TotalRecs == 0 && $notiliniers->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($notiliniers_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($notiliniers->Export == "") { ?>
<script type="text/javascript">
fnotilinierslistsrch.FilterList = <?php echo $notiliniers_list->GetFilterList() ?>;
fnotilinierslistsrch.Init();
fnotilinierslist.Init();
</script>
<?php } ?>
<?php
$notiliniers_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($notiliniers->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$notiliniers_list->Page_Terminate();
?>
