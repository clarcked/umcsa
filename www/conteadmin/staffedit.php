<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$staff_edit = NULL; // Initialize page object first

class cstaff_edit extends cstaff {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'staff';

	// Page object name
	var $PageObjName = 'staff_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (staff)
		if (!isset($GLOBALS["staff"]) || get_class($GLOBALS["staff"]) == "cstaff") {
			$GLOBALS["staff"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["staff"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'staff', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("stafflist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->nombre->SetVisibility();
		$this->grupo1->SetVisibility();
		$this->grupo2->SetVisibility();
		$this->grupo3->SetVisibility();
		$this->grupo4->SetVisibility();
		$this->subgrupo->SetVisibility();
		$this->foto->SetVisibility();
		$this->cv->SetVisibility();
		$this->_email->SetVisibility();
		$this->tel->SetVisibility();
		$this->orden->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $staff;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($staff);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $IsModal = FALSE;
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "") {
			$this->Page_Terminate("stafflist.php"); // Invalid key, return to list
		}

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("stafflist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "stafflist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->foto->Upload->Index = $objForm->Index;
		$this->foto->Upload->UploadFile();
		$this->foto->CurrentValue = $this->foto->Upload->FileName;
		$this->cv->Upload->Index = $objForm->Index;
		$this->cv->Upload->UploadFile();
		$this->cv->CurrentValue = $this->cv->Upload->FileName;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->nombre->FldIsDetailKey) {
			$this->nombre->setFormValue($objForm->GetValue("x_nombre"));
		}
		if (!$this->grupo1->FldIsDetailKey) {
			$this->grupo1->setFormValue($objForm->GetValue("x_grupo1"));
		}
		if (!$this->grupo2->FldIsDetailKey) {
			$this->grupo2->setFormValue($objForm->GetValue("x_grupo2"));
		}
		if (!$this->grupo3->FldIsDetailKey) {
			$this->grupo3->setFormValue($objForm->GetValue("x_grupo3"));
		}
		if (!$this->grupo4->FldIsDetailKey) {
			$this->grupo4->setFormValue($objForm->GetValue("x_grupo4"));
		}
		if (!$this->subgrupo->FldIsDetailKey) {
			$this->subgrupo->setFormValue($objForm->GetValue("x_subgrupo"));
		}
		if (!$this->_email->FldIsDetailKey) {
			$this->_email->setFormValue($objForm->GetValue("x__email"));
		}
		if (!$this->tel->FldIsDetailKey) {
			$this->tel->setFormValue($objForm->GetValue("x_tel"));
		}
		if (!$this->orden->FldIsDetailKey) {
			$this->orden->setFormValue($objForm->GetValue("x_orden"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->nombre->CurrentValue = $this->nombre->FormValue;
		$this->grupo1->CurrentValue = $this->grupo1->FormValue;
		$this->grupo2->CurrentValue = $this->grupo2->FormValue;
		$this->grupo3->CurrentValue = $this->grupo3->FormValue;
		$this->grupo4->CurrentValue = $this->grupo4->FormValue;
		$this->subgrupo->CurrentValue = $this->subgrupo->FormValue;
		$this->_email->CurrentValue = $this->_email->FormValue;
		$this->tel->CurrentValue = $this->tel->FormValue;
		$this->orden->CurrentValue = $this->orden->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->nombre->setDbValue($rs->fields('nombre'));
		$this->grupo1->setDbValue($rs->fields('grupo1'));
		$this->grupo2->setDbValue($rs->fields('grupo2'));
		$this->grupo3->setDbValue($rs->fields('grupo3'));
		$this->grupo4->setDbValue($rs->fields('grupo4'));
		$this->subgrupo->setDbValue($rs->fields('subgrupo'));
		$this->foto->Upload->DbValue = $rs->fields('foto');
		$this->foto->CurrentValue = $this->foto->Upload->DbValue;
		$this->cv->Upload->DbValue = $rs->fields('cv');
		$this->cv->CurrentValue = $this->cv->Upload->DbValue;
		$this->_email->setDbValue($rs->fields('email'));
		$this->tel->setDbValue($rs->fields('tel'));
		$this->orden->setDbValue($rs->fields('orden'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->nombre->DbValue = $row['nombre'];
		$this->grupo1->DbValue = $row['grupo1'];
		$this->grupo2->DbValue = $row['grupo2'];
		$this->grupo3->DbValue = $row['grupo3'];
		$this->grupo4->DbValue = $row['grupo4'];
		$this->subgrupo->DbValue = $row['subgrupo'];
		$this->foto->Upload->DbValue = $row['foto'];
		$this->cv->Upload->DbValue = $row['cv'];
		$this->_email->DbValue = $row['email'];
		$this->tel->DbValue = $row['tel'];
		$this->orden->DbValue = $row['orden'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// nombre
		// grupo1
		// grupo2
		// grupo3
		// grupo4
		// subgrupo
		// foto
		// cv
		// email
		// tel
		// orden

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// nombre
		$this->nombre->ViewValue = $this->nombre->CurrentValue;
		$this->nombre->ViewCustomAttributes = "";

		// grupo1
		if (strval($this->grupo1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo1->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo1->ViewValue = $this->grupo1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo1->ViewValue = $this->grupo1->CurrentValue;
			}
		} else {
			$this->grupo1->ViewValue = NULL;
		}
		$this->grupo1->ViewCustomAttributes = "";

		// grupo2
		if (strval($this->grupo2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo2->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo2->ViewValue = $this->grupo2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo2->ViewValue = $this->grupo2->CurrentValue;
			}
		} else {
			$this->grupo2->ViewValue = NULL;
		}
		$this->grupo2->ViewCustomAttributes = "";

		// grupo3
		if (strval($this->grupo3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo3->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo3->ViewValue = $this->grupo3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo3->ViewValue = $this->grupo3->CurrentValue;
			}
		} else {
			$this->grupo3->ViewValue = NULL;
		}
		$this->grupo3->ViewCustomAttributes = "";

		// grupo4
		if (strval($this->grupo4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo4->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo4->ViewValue = $this->grupo4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo4->ViewValue = $this->grupo4->CurrentValue;
			}
		} else {
			$this->grupo4->ViewValue = NULL;
		}
		$this->grupo4->ViewCustomAttributes = "";

		// subgrupo
		if (strval($this->subgrupo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->subgrupo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_subgrupos`";
		$sWhereWrk = "";
		$this->subgrupo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->subgrupo->ViewValue = $this->subgrupo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->subgrupo->ViewValue = $this->subgrupo->CurrentValue;
			}
		} else {
			$this->subgrupo->ViewValue = NULL;
		}
		$this->subgrupo->ViewCustomAttributes = "";

		// foto
		if (!ew_Empty($this->foto->Upload->DbValue)) {
			$this->foto->ViewValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->ViewValue = "";
		}
		$this->foto->ViewCustomAttributes = "";

		// cv
		if (!ew_Empty($this->cv->Upload->DbValue)) {
			$this->cv->ViewValue = $this->cv->Upload->DbValue;
		} else {
			$this->cv->ViewValue = "";
		}
		$this->cv->ViewCustomAttributes = "";

		// email
		$this->_email->ViewValue = $this->_email->CurrentValue;
		$this->_email->ViewCustomAttributes = "";

		// tel
		$this->tel->ViewValue = $this->tel->CurrentValue;
		$this->tel->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// nombre
			$this->nombre->LinkCustomAttributes = "";
			$this->nombre->HrefValue = "";
			$this->nombre->TooltipValue = "";

			// grupo1
			$this->grupo1->LinkCustomAttributes = "";
			$this->grupo1->HrefValue = "";
			$this->grupo1->TooltipValue = "";

			// grupo2
			$this->grupo2->LinkCustomAttributes = "";
			$this->grupo2->HrefValue = "";
			$this->grupo2->TooltipValue = "";

			// grupo3
			$this->grupo3->LinkCustomAttributes = "";
			$this->grupo3->HrefValue = "";
			$this->grupo3->TooltipValue = "";

			// grupo4
			$this->grupo4->LinkCustomAttributes = "";
			$this->grupo4->HrefValue = "";
			$this->grupo4->TooltipValue = "";

			// subgrupo
			$this->subgrupo->LinkCustomAttributes = "";
			$this->subgrupo->HrefValue = "";
			$this->subgrupo->TooltipValue = "";

			// foto
			$this->foto->LinkCustomAttributes = "";
			$this->foto->HrefValue = "";
			$this->foto->HrefValue2 = $this->foto->UploadPath . $this->foto->Upload->DbValue;
			$this->foto->TooltipValue = "";

			// cv
			$this->cv->LinkCustomAttributes = "";
			$this->cv->HrefValue = "";
			$this->cv->HrefValue2 = $this->cv->UploadPath . $this->cv->Upload->DbValue;
			$this->cv->TooltipValue = "";

			// email
			$this->_email->LinkCustomAttributes = "";
			$this->_email->HrefValue = "";
			$this->_email->TooltipValue = "";

			// tel
			$this->tel->LinkCustomAttributes = "";
			$this->tel->HrefValue = "";
			$this->tel->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// nombre
			$this->nombre->EditAttrs["class"] = "form-control";
			$this->nombre->EditCustomAttributes = "";
			$this->nombre->EditValue = ew_HtmlEncode($this->nombre->CurrentValue);
			$this->nombre->PlaceHolder = ew_RemoveHtml($this->nombre->FldCaption());

			// grupo1
			$this->grupo1->EditAttrs["class"] = "form-control";
			$this->grupo1->EditCustomAttributes = "";
			if (trim(strval($this->grupo1->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo1->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo1->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo1->EditValue = $arwrk;

			// grupo2
			$this->grupo2->EditAttrs["class"] = "form-control";
			$this->grupo2->EditCustomAttributes = "";
			if (trim(strval($this->grupo2->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo2->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo2->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo2->EditValue = $arwrk;

			// grupo3
			$this->grupo3->EditAttrs["class"] = "form-control";
			$this->grupo3->EditCustomAttributes = "";
			if (trim(strval($this->grupo3->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo3->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo3->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo3->EditValue = $arwrk;

			// grupo4
			$this->grupo4->EditAttrs["class"] = "form-control";
			$this->grupo4->EditCustomAttributes = "";
			if (trim(strval($this->grupo4->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo4->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo4->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->grupo4->EditValue = $arwrk;

			// subgrupo
			$this->subgrupo->EditAttrs["class"] = "form-control";
			$this->subgrupo->EditCustomAttributes = "";
			if (trim(strval($this->subgrupo->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->subgrupo->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff_subgrupos`";
			$sWhereWrk = "";
			$this->subgrupo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->subgrupo->EditValue = $arwrk;

			// foto
			$this->foto->EditAttrs["class"] = "form-control";
			$this->foto->EditCustomAttributes = "";
			if (!ew_Empty($this->foto->Upload->DbValue)) {
				$this->foto->EditValue = $this->foto->Upload->DbValue;
			} else {
				$this->foto->EditValue = "";
			}
			if (!ew_Empty($this->foto->CurrentValue))
				$this->foto->Upload->FileName = $this->foto->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->foto);

			// cv
			$this->cv->EditAttrs["class"] = "form-control";
			$this->cv->EditCustomAttributes = "";
			if (!ew_Empty($this->cv->Upload->DbValue)) {
				$this->cv->EditValue = $this->cv->Upload->DbValue;
			} else {
				$this->cv->EditValue = "";
			}
			if (!ew_Empty($this->cv->CurrentValue))
				$this->cv->Upload->FileName = $this->cv->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->cv);

			// email
			$this->_email->EditAttrs["class"] = "form-control";
			$this->_email->EditCustomAttributes = "";
			$this->_email->EditValue = ew_HtmlEncode($this->_email->CurrentValue);
			$this->_email->PlaceHolder = ew_RemoveHtml($this->_email->FldCaption());

			// tel
			$this->tel->EditAttrs["class"] = "form-control";
			$this->tel->EditCustomAttributes = "";
			$this->tel->EditValue = ew_HtmlEncode($this->tel->CurrentValue);
			$this->tel->PlaceHolder = ew_RemoveHtml($this->tel->FldCaption());

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->CurrentValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// nombre
			$this->nombre->LinkCustomAttributes = "";
			$this->nombre->HrefValue = "";

			// grupo1
			$this->grupo1->LinkCustomAttributes = "";
			$this->grupo1->HrefValue = "";

			// grupo2
			$this->grupo2->LinkCustomAttributes = "";
			$this->grupo2->HrefValue = "";

			// grupo3
			$this->grupo3->LinkCustomAttributes = "";
			$this->grupo3->HrefValue = "";

			// grupo4
			$this->grupo4->LinkCustomAttributes = "";
			$this->grupo4->HrefValue = "";

			// subgrupo
			$this->subgrupo->LinkCustomAttributes = "";
			$this->subgrupo->HrefValue = "";

			// foto
			$this->foto->LinkCustomAttributes = "";
			$this->foto->HrefValue = "";
			$this->foto->HrefValue2 = $this->foto->UploadPath . $this->foto->Upload->DbValue;

			// cv
			$this->cv->LinkCustomAttributes = "";
			$this->cv->HrefValue = "";
			$this->cv->HrefValue2 = $this->cv->UploadPath . $this->cv->Upload->DbValue;

			// email
			$this->_email->LinkCustomAttributes = "";
			$this->_email->HrefValue = "";

			// tel
			$this->tel->LinkCustomAttributes = "";
			$this->tel->HrefValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->nombre->FldIsDetailKey && !is_null($this->nombre->FormValue) && $this->nombre->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->nombre->FldCaption(), $this->nombre->ReqErrMsg));
		}
		if (!$this->grupo1->FldIsDetailKey && !is_null($this->grupo1->FormValue) && $this->grupo1->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->grupo1->FldCaption(), $this->grupo1->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->orden->FormValue)) {
			ew_AddMessage($gsFormError, $this->orden->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// nombre
			$this->nombre->SetDbValueDef($rsnew, $this->nombre->CurrentValue, "", $this->nombre->ReadOnly);

			// grupo1
			$this->grupo1->SetDbValueDef($rsnew, $this->grupo1->CurrentValue, 0, $this->grupo1->ReadOnly);

			// grupo2
			$this->grupo2->SetDbValueDef($rsnew, $this->grupo2->CurrentValue, NULL, $this->grupo2->ReadOnly);

			// grupo3
			$this->grupo3->SetDbValueDef($rsnew, $this->grupo3->CurrentValue, NULL, $this->grupo3->ReadOnly);

			// grupo4
			$this->grupo4->SetDbValueDef($rsnew, $this->grupo4->CurrentValue, NULL, $this->grupo4->ReadOnly);

			// subgrupo
			$this->subgrupo->SetDbValueDef($rsnew, $this->subgrupo->CurrentValue, NULL, $this->subgrupo->ReadOnly);

			// foto
			if ($this->foto->Visible && !$this->foto->ReadOnly && !$this->foto->Upload->KeepFile) {
				$this->foto->Upload->DbValue = $rsold['foto']; // Get original value
				if ($this->foto->Upload->FileName == "") {
					$rsnew['foto'] = NULL;
				} else {
					$rsnew['foto'] = $this->foto->Upload->FileName;
				}
			}

			// cv
			if ($this->cv->Visible && !$this->cv->ReadOnly && !$this->cv->Upload->KeepFile) {
				$this->cv->Upload->DbValue = $rsold['cv']; // Get original value
				if ($this->cv->Upload->FileName == "") {
					$rsnew['cv'] = NULL;
				} else {
					$rsnew['cv'] = $this->cv->Upload->FileName;
				}
			}

			// email
			$this->_email->SetDbValueDef($rsnew, $this->_email->CurrentValue, NULL, $this->_email->ReadOnly);

			// tel
			$this->tel->SetDbValueDef($rsnew, $this->tel->CurrentValue, NULL, $this->tel->ReadOnly);

			// orden
			$this->orden->SetDbValueDef($rsnew, $this->orden->CurrentValue, NULL, $this->orden->ReadOnly);
			if ($this->foto->Visible && !$this->foto->Upload->KeepFile) {
				if (!ew_Empty($this->foto->Upload->Value)) {
					$rsnew['foto'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto->UploadPath), $rsnew['foto']); // Get new file name
				}
			}
			if ($this->cv->Visible && !$this->cv->Upload->KeepFile) {
				if (!ew_Empty($this->cv->Upload->Value)) {
					$rsnew['cv'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->cv->UploadPath), $rsnew['cv']); // Get new file name
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->foto->Visible && !$this->foto->Upload->KeepFile) {
						if (!ew_Empty($this->foto->Upload->Value)) {
							if (!$this->foto->Upload->SaveToFile($this->foto->UploadPath, $rsnew['foto'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->cv->Visible && !$this->cv->Upload->KeepFile) {
						if (!ew_Empty($this->cv->Upload->Value)) {
							if (!$this->cv->Upload->SaveToFile($this->cv->UploadPath, $rsnew['cv'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// foto
		ew_CleanUploadTempPath($this->foto, $this->foto->Upload->Index);

		// cv
		ew_CleanUploadTempPath($this->cv, $this->cv->Upload->Index);
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("stafflist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_grupo1":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo1->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo2":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo2->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo3":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo3->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_grupo4":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
			$sWhereWrk = "";
			$this->grupo4->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_subgrupo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_subgrupos`";
			$sWhereWrk = "";
			$this->subgrupo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($staff_edit)) $staff_edit = new cstaff_edit();

// Page init
$staff_edit->Page_Init();

// Page main
$staff_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$staff_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fstaffedit = new ew_Form("fstaffedit", "edit");

// Validate form
fstaffedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_nombre");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $staff->nombre->FldCaption(), $staff->nombre->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_grupo1");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $staff->grupo1->FldCaption(), $staff->grupo1->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_orden");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($staff->orden->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fstaffedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fstaffedit.ValidateRequired = true;
<?php } else { ?>
fstaffedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fstaffedit.Lists["x_grupo1"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffedit.Lists["x_grupo2"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffedit.Lists["x_grupo3"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffedit.Lists["x_grupo4"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_grupos"};
fstaffedit.Lists["x_subgrupo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"staff_subgrupos"};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$staff_edit->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $staff_edit->ShowPageHeader(); ?>
<?php
$staff_edit->ShowMessage();
?>
<form name="fstaffedit" id="fstaffedit" class="<?php echo $staff_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($staff_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $staff_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="staff">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($staff_edit->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($staff->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label id="elh_staff_id" class="col-sm-2 control-label ewLabel"><?php echo $staff->id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->id->CellAttributes() ?>>
<span id="el_staff_id">
<span<?php echo $staff->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $staff->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="staff" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($staff->id->CurrentValue) ?>">
<?php echo $staff->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->nombre->Visible) { // nombre ?>
	<div id="r_nombre" class="form-group">
		<label id="elh_staff_nombre" for="x_nombre" class="col-sm-2 control-label ewLabel"><?php echo $staff->nombre->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $staff->nombre->CellAttributes() ?>>
<span id="el_staff_nombre">
<input type="text" data-table="staff" data-field="x_nombre" name="x_nombre" id="x_nombre" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->nombre->getPlaceHolder()) ?>" value="<?php echo $staff->nombre->EditValue ?>"<?php echo $staff->nombre->EditAttributes() ?>>
</span>
<?php echo $staff->nombre->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo1->Visible) { // grupo1 ?>
	<div id="r_grupo1" class="form-group">
		<label id="elh_staff_grupo1" for="x_grupo1" class="col-sm-2 control-label ewLabel"><?php echo $staff->grupo1->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $staff->grupo1->CellAttributes() ?>>
<span id="el_staff_grupo1">
<select data-table="staff" data-field="x_grupo1" data-value-separator="<?php echo $staff->grupo1->DisplayValueSeparatorAttribute() ?>" id="x_grupo1" name="x_grupo1"<?php echo $staff->grupo1->EditAttributes() ?>>
<?php echo $staff->grupo1->SelectOptionListHtml("x_grupo1") ?>
</select>
<input type="hidden" name="s_x_grupo1" id="s_x_grupo1" value="<?php echo $staff->grupo1->LookupFilterQuery() ?>">
</span>
<?php echo $staff->grupo1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo2->Visible) { // grupo2 ?>
	<div id="r_grupo2" class="form-group">
		<label id="elh_staff_grupo2" for="x_grupo2" class="col-sm-2 control-label ewLabel"><?php echo $staff->grupo2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->grupo2->CellAttributes() ?>>
<span id="el_staff_grupo2">
<select data-table="staff" data-field="x_grupo2" data-value-separator="<?php echo $staff->grupo2->DisplayValueSeparatorAttribute() ?>" id="x_grupo2" name="x_grupo2"<?php echo $staff->grupo2->EditAttributes() ?>>
<?php echo $staff->grupo2->SelectOptionListHtml("x_grupo2") ?>
</select>
<input type="hidden" name="s_x_grupo2" id="s_x_grupo2" value="<?php echo $staff->grupo2->LookupFilterQuery() ?>">
</span>
<?php echo $staff->grupo2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo3->Visible) { // grupo3 ?>
	<div id="r_grupo3" class="form-group">
		<label id="elh_staff_grupo3" for="x_grupo3" class="col-sm-2 control-label ewLabel"><?php echo $staff->grupo3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->grupo3->CellAttributes() ?>>
<span id="el_staff_grupo3">
<select data-table="staff" data-field="x_grupo3" data-value-separator="<?php echo $staff->grupo3->DisplayValueSeparatorAttribute() ?>" id="x_grupo3" name="x_grupo3"<?php echo $staff->grupo3->EditAttributes() ?>>
<?php echo $staff->grupo3->SelectOptionListHtml("x_grupo3") ?>
</select>
<input type="hidden" name="s_x_grupo3" id="s_x_grupo3" value="<?php echo $staff->grupo3->LookupFilterQuery() ?>">
</span>
<?php echo $staff->grupo3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->grupo4->Visible) { // grupo4 ?>
	<div id="r_grupo4" class="form-group">
		<label id="elh_staff_grupo4" for="x_grupo4" class="col-sm-2 control-label ewLabel"><?php echo $staff->grupo4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->grupo4->CellAttributes() ?>>
<span id="el_staff_grupo4">
<select data-table="staff" data-field="x_grupo4" data-value-separator="<?php echo $staff->grupo4->DisplayValueSeparatorAttribute() ?>" id="x_grupo4" name="x_grupo4"<?php echo $staff->grupo4->EditAttributes() ?>>
<?php echo $staff->grupo4->SelectOptionListHtml("x_grupo4") ?>
</select>
<input type="hidden" name="s_x_grupo4" id="s_x_grupo4" value="<?php echo $staff->grupo4->LookupFilterQuery() ?>">
</span>
<?php echo $staff->grupo4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->subgrupo->Visible) { // subgrupo ?>
	<div id="r_subgrupo" class="form-group">
		<label id="elh_staff_subgrupo" for="x_subgrupo" class="col-sm-2 control-label ewLabel"><?php echo $staff->subgrupo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->subgrupo->CellAttributes() ?>>
<span id="el_staff_subgrupo">
<select data-table="staff" data-field="x_subgrupo" data-value-separator="<?php echo $staff->subgrupo->DisplayValueSeparatorAttribute() ?>" id="x_subgrupo" name="x_subgrupo"<?php echo $staff->subgrupo->EditAttributes() ?>>
<?php echo $staff->subgrupo->SelectOptionListHtml("x_subgrupo") ?>
</select>
<input type="hidden" name="s_x_subgrupo" id="s_x_subgrupo" value="<?php echo $staff->subgrupo->LookupFilterQuery() ?>">
</span>
<?php echo $staff->subgrupo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->foto->Visible) { // foto ?>
	<div id="r_foto" class="form-group">
		<label id="elh_staff_foto" class="col-sm-2 control-label ewLabel"><?php echo $staff->foto->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->foto->CellAttributes() ?>>
<span id="el_staff_foto">
<div id="fd_x_foto">
<span title="<?php echo $staff->foto->FldTitle() ? $staff->foto->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($staff->foto->ReadOnly || $staff->foto->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="staff" data-field="x_foto" name="x_foto" id="x_foto"<?php echo $staff->foto->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto" id= "fn_x_foto" value="<?php echo $staff->foto->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto"] == "0") { ?>
<input type="hidden" name="fa_x_foto" id= "fa_x_foto" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto" id= "fa_x_foto" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto" id= "fs_x_foto" value="255">
<input type="hidden" name="fx_x_foto" id= "fx_x_foto" value="<?php echo $staff->foto->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto" id= "fm_x_foto" value="<?php echo $staff->foto->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $staff->foto->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->cv->Visible) { // cv ?>
	<div id="r_cv" class="form-group">
		<label id="elh_staff_cv" class="col-sm-2 control-label ewLabel"><?php echo $staff->cv->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->cv->CellAttributes() ?>>
<span id="el_staff_cv">
<div id="fd_x_cv">
<span title="<?php echo $staff->cv->FldTitle() ? $staff->cv->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($staff->cv->ReadOnly || $staff->cv->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="staff" data-field="x_cv" name="x_cv" id="x_cv"<?php echo $staff->cv->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_cv" id= "fn_x_cv" value="<?php echo $staff->cv->Upload->FileName ?>">
<?php if (@$_POST["fa_x_cv"] == "0") { ?>
<input type="hidden" name="fa_x_cv" id= "fa_x_cv" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_cv" id= "fa_x_cv" value="1">
<?php } ?>
<input type="hidden" name="fs_x_cv" id= "fs_x_cv" value="255">
<input type="hidden" name="fx_x_cv" id= "fx_x_cv" value="<?php echo $staff->cv->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_cv" id= "fm_x_cv" value="<?php echo $staff->cv->UploadMaxFileSize ?>">
</div>
<table id="ft_x_cv" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $staff->cv->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->_email->Visible) { // email ?>
	<div id="r__email" class="form-group">
		<label id="elh_staff__email" for="x__email" class="col-sm-2 control-label ewLabel"><?php echo $staff->_email->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->_email->CellAttributes() ?>>
<span id="el_staff__email">
<input type="text" data-table="staff" data-field="x__email" name="x__email" id="x__email" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->_email->getPlaceHolder()) ?>" value="<?php echo $staff->_email->EditValue ?>"<?php echo $staff->_email->EditAttributes() ?>>
</span>
<?php echo $staff->_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->tel->Visible) { // tel ?>
	<div id="r_tel" class="form-group">
		<label id="elh_staff_tel" for="x_tel" class="col-sm-2 control-label ewLabel"><?php echo $staff->tel->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->tel->CellAttributes() ?>>
<span id="el_staff_tel">
<input type="text" data-table="staff" data-field="x_tel" name="x_tel" id="x_tel" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($staff->tel->getPlaceHolder()) ?>" value="<?php echo $staff->tel->EditValue ?>"<?php echo $staff->tel->EditAttributes() ?>>
</span>
<?php echo $staff->tel->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($staff->orden->Visible) { // orden ?>
	<div id="r_orden" class="form-group">
		<label id="elh_staff_orden" for="x_orden" class="col-sm-2 control-label ewLabel"><?php echo $staff->orden->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $staff->orden->CellAttributes() ?>>
<span id="el_staff_orden">
<input type="text" data-table="staff" data-field="x_orden" name="x_orden" id="x_orden" size="30" placeholder="<?php echo ew_HtmlEncode($staff->orden->getPlaceHolder()) ?>" value="<?php echo $staff->orden->EditValue ?>"<?php echo $staff->orden->EditAttributes() ?>>
</span>
<?php echo $staff->orden->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php if (!$staff_edit->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $staff_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fstaffedit.Init();
</script>
<?php
$staff_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$staff_edit->Page_Terminate();
?>
