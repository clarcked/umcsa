<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "rematesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$remates_delete = NULL; // Initialize page object first

class cremates_delete extends cremates {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'remates';

	// Page object name
	var $PageObjName = 'remates_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (remates)
		if (!isset($GLOBALS["remates"]) || get_class($GLOBALS["remates"]) == "cremates") {
			$GLOBALS["remates"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["remates"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'remates', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("remateslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->tipo->SetVisibility();
		$this->plataforma->SetVisibility();
		$this->ext_id->SetVisibility();
		$this->fecha->SetVisibility();
		$this->hora_inicio->SetVisibility();
		$this->hora_fin->SetVisibility();
		$this->zona->SetVisibility();
		$this->lugar->SetVisibility();
		$this->locacion->SetVisibility();
		$this->numero->SetVisibility();
		$this->titulo->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->yacare_visible->SetVisibility();
		$this->banner_g->SetVisibility();
		$this->banner_g_visible->SetVisibility();
		$this->banner_g_z_index->SetVisibility();
		$this->banner_g_pos_x->SetVisibility();
		$this->banner_g_pos_y->SetVisibility();
		$this->banner_g_link->SetVisibility();
		$this->activo->SetVisibility();
		$this->archivo->SetVisibility();
		$this->precios->SetVisibility();
		$this->mapa_img->SetVisibility();
		$this->video_final_servidor->SetVisibility();
		$this->video_final_1->SetVisibility();
		$this->video_titu_1->SetVisibility();
		$this->video_final_2->SetVisibility();
		$this->video_titu_2->SetVisibility();
		$this->video_final_3->SetVisibility();
		$this->video_titu_3->SetVisibility();
		$this->video_final_4->SetVisibility();
		$this->video_titu_4->SetVisibility();
		$this->video_final_5->SetVisibility();
		$this->video_titu_5->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $remates;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($remates);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("remateslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in remates class, rematesinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} elseif (@$_GET["a_delete"] == "1") {
			$this->CurrentAction = "D"; // Delete record directly
		} else {
			$this->CurrentAction = "D"; // Delete record directly
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("remateslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->plataforma->setDbValue($rs->fields('plataforma'));
		$this->ext_id->setDbValue($rs->fields('ext_id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->hora_inicio->setDbValue($rs->fields('hora_inicio'));
		$this->hora_fin->setDbValue($rs->fields('hora_fin'));
		$this->zona->setDbValue($rs->fields('zona'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->locacion->setDbValue($rs->fields('locacion'));
		$this->numero->setDbValue($rs->fields('numero'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->yacare_visible->setDbValue($rs->fields('yacare_visible'));
		$this->banner_g->Upload->DbValue = $rs->fields('banner_g');
		$this->banner_g->CurrentValue = $this->banner_g->Upload->DbValue;
		$this->banner_g_visible->setDbValue($rs->fields('banner_g_visible'));
		$this->banner_g_z_index->setDbValue($rs->fields('banner_g_z_index'));
		$this->banner_g_pos_x->setDbValue($rs->fields('banner_g_pos_x'));
		$this->banner_g_pos_y->setDbValue($rs->fields('banner_g_pos_y'));
		$this->banner_g_link->setDbValue($rs->fields('banner_g_link'));
		$this->banner_ch->Upload->DbValue = $rs->fields('banner_ch');
		$this->banner_ch->CurrentValue = $this->banner_ch->Upload->DbValue;
		$this->activo->setDbValue($rs->fields('activo'));
		$this->archivo->Upload->DbValue = $rs->fields('archivo');
		$this->archivo->CurrentValue = $this->archivo->Upload->DbValue;
		$this->precios->setDbValue($rs->fields('precios'));
		$this->mapa_img->Upload->DbValue = $rs->fields('mapa_img');
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->DbValue;
		$this->video_final_servidor->setDbValue($rs->fields('video_final_servidor'));
		$this->video_final_1->setDbValue($rs->fields('video_final_1'));
		$this->video_titu_1->setDbValue($rs->fields('video_titu_1'));
		$this->video_final_2->setDbValue($rs->fields('video_final_2'));
		$this->video_titu_2->setDbValue($rs->fields('video_titu_2'));
		$this->video_final_3->setDbValue($rs->fields('video_final_3'));
		$this->video_titu_3->setDbValue($rs->fields('video_titu_3'));
		$this->video_final_4->setDbValue($rs->fields('video_final_4'));
		$this->video_titu_4->setDbValue($rs->fields('video_titu_4'));
		$this->video_final_5->setDbValue($rs->fields('video_final_5'));
		$this->video_titu_5->setDbValue($rs->fields('video_titu_5'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->tipo->DbValue = $row['tipo'];
		$this->plataforma->DbValue = $row['plataforma'];
		$this->ext_id->DbValue = $row['ext_id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->hora_inicio->DbValue = $row['hora_inicio'];
		$this->hora_fin->DbValue = $row['hora_fin'];
		$this->zona->DbValue = $row['zona'];
		$this->lugar->DbValue = $row['lugar'];
		$this->locacion->DbValue = $row['locacion'];
		$this->numero->DbValue = $row['numero'];
		$this->titulo->DbValue = $row['titulo'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->yacare_visible->DbValue = $row['yacare_visible'];
		$this->banner_g->Upload->DbValue = $row['banner_g'];
		$this->banner_g_visible->DbValue = $row['banner_g_visible'];
		$this->banner_g_z_index->DbValue = $row['banner_g_z_index'];
		$this->banner_g_pos_x->DbValue = $row['banner_g_pos_x'];
		$this->banner_g_pos_y->DbValue = $row['banner_g_pos_y'];
		$this->banner_g_link->DbValue = $row['banner_g_link'];
		$this->banner_ch->Upload->DbValue = $row['banner_ch'];
		$this->activo->DbValue = $row['activo'];
		$this->archivo->Upload->DbValue = $row['archivo'];
		$this->precios->DbValue = $row['precios'];
		$this->mapa_img->Upload->DbValue = $row['mapa_img'];
		$this->video_final_servidor->DbValue = $row['video_final_servidor'];
		$this->video_final_1->DbValue = $row['video_final_1'];
		$this->video_titu_1->DbValue = $row['video_titu_1'];
		$this->video_final_2->DbValue = $row['video_final_2'];
		$this->video_titu_2->DbValue = $row['video_titu_2'];
		$this->video_final_3->DbValue = $row['video_final_3'];
		$this->video_titu_3->DbValue = $row['video_titu_3'];
		$this->video_final_4->DbValue = $row['video_final_4'];
		$this->video_titu_4->DbValue = $row['video_titu_4'];
		$this->video_final_5->DbValue = $row['video_final_5'];
		$this->video_titu_5->DbValue = $row['video_titu_5'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// tipo
		// plataforma
		// ext_id
		// fecha
		// hora_inicio
		// hora_fin
		// zona
		// lugar
		// locacion
		// numero
		// titulo
		// cantidad
		// descripcion
		// yacare_visible
		// banner_g
		// banner_g_visible
		// banner_g_z_index
		// banner_g_pos_x
		// banner_g_pos_y
		// banner_g_link
		// banner_ch

		$this->banner_ch->CellCssStyle = "white-space: nowrap;";

		// activo
		// archivo
		// precios
		// mapa_img
		// video_final_servidor
		// video_final_1
		// video_titu_1
		// video_final_2
		// video_titu_2
		// video_final_3
		// video_titu_3
		// video_final_4
		// video_titu_4
		// video_final_5
		// video_titu_5

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		if (strval($this->tipo->CurrentValue) <> "") {
			$this->tipo->ViewValue = $this->tipo->OptionCaption($this->tipo->CurrentValue);
		} else {
			$this->tipo->ViewValue = NULL;
		}
		$this->tipo->ViewCustomAttributes = "";

		// plataforma
		if (strval($this->plataforma->CurrentValue) <> "") {
			$this->plataforma->ViewValue = $this->plataforma->OptionCaption($this->plataforma->CurrentValue);
		} else {
			$this->plataforma->ViewValue = NULL;
		}
		$this->plataforma->ViewCustomAttributes = "";

		// ext_id
		$this->ext_id->ViewValue = $this->ext_id->CurrentValue;
		$this->ext_id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// hora_inicio
		$this->hora_inicio->ViewValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->ViewCustomAttributes = "";

		// hora_fin
		$this->hora_fin->ViewValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->ViewCustomAttributes = "";

		// zona
		if (strval($this->zona->CurrentValue) <> "") {
			$this->zona->ViewValue = $this->zona->OptionCaption($this->zona->CurrentValue);
		} else {
			$this->zona->ViewValue = NULL;
		}
		$this->zona->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewCustomAttributes = "";

		// locacion
		$this->locacion->ViewValue = $this->locacion->CurrentValue;
		$this->locacion->ViewCustomAttributes = "";

		// numero
		$this->numero->ViewValue = $this->numero->CurrentValue;
		$this->numero->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// yacare_visible
		if (strval($this->yacare_visible->CurrentValue) <> "") {
			$this->yacare_visible->ViewValue = $this->yacare_visible->OptionCaption($this->yacare_visible->CurrentValue);
		} else {
			$this->yacare_visible->ViewValue = NULL;
		}
		$this->yacare_visible->ViewCustomAttributes = "";

		// banner_g
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->ViewValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->ViewValue = "";
		}
		$this->banner_g->ViewCustomAttributes = "";

		// banner_g_visible
		if (strval($this->banner_g_visible->CurrentValue) <> "") {
			$this->banner_g_visible->ViewValue = $this->banner_g_visible->OptionCaption($this->banner_g_visible->CurrentValue);
		} else {
			$this->banner_g_visible->ViewValue = NULL;
		}
		$this->banner_g_visible->ViewCustomAttributes = "";

		// banner_g_z_index
		$this->banner_g_z_index->ViewValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->ViewCustomAttributes = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->ViewValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->ViewCustomAttributes = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->ViewValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->ViewCustomAttributes = "";

		// banner_g_link
		$this->banner_g_link->ViewValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// archivo
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->ViewValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->ViewValue = "";
		}
		$this->archivo->ViewCustomAttributes = "";

		// precios
		if (strval($this->precios->CurrentValue) <> "") {
			$this->precios->ViewValue = $this->precios->OptionCaption($this->precios->CurrentValue);
		} else {
			$this->precios->ViewValue = NULL;
		}
		$this->precios->ViewCustomAttributes = "";

		// mapa_img
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->ViewValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->ViewValue = "";
		}
		$this->mapa_img->ViewCustomAttributes = "";

		// video_final_servidor
		if (strval($this->video_final_servidor->CurrentValue) <> "") {
			$this->video_final_servidor->ViewValue = $this->video_final_servidor->OptionCaption($this->video_final_servidor->CurrentValue);
		} else {
			$this->video_final_servidor->ViewValue = NULL;
		}
		$this->video_final_servidor->ViewCustomAttributes = "";

		// video_final_1
		$this->video_final_1->ViewValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->ViewCustomAttributes = "";

		// video_titu_1
		$this->video_titu_1->ViewValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->ViewCustomAttributes = "";

		// video_final_2
		$this->video_final_2->ViewValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->ViewCustomAttributes = "";

		// video_titu_2
		$this->video_titu_2->ViewValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->ViewCustomAttributes = "";

		// video_final_3
		$this->video_final_3->ViewValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->ViewCustomAttributes = "";

		// video_titu_3
		$this->video_titu_3->ViewValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->ViewCustomAttributes = "";

		// video_final_4
		$this->video_final_4->ViewValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->ViewCustomAttributes = "";

		// video_titu_4
		$this->video_titu_4->ViewValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->ViewCustomAttributes = "";

		// video_final_5
		$this->video_final_5->ViewValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->ViewCustomAttributes = "";

		// video_titu_5
		$this->video_titu_5->ViewValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";
			$this->plataforma->TooltipValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";
			$this->ext_id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";
			$this->hora_inicio->TooltipValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";
			$this->hora_fin->TooltipValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";
			$this->zona->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";
			$this->locacion->TooltipValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";
			$this->numero->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";
			$this->yacare_visible->TooltipValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;
			$this->banner_g->TooltipValue = "";

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";
			$this->banner_g_visible->TooltipValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";
			$this->banner_g_z_index->TooltipValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";
			$this->banner_g_pos_x->TooltipValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";
			$this->banner_g_pos_y->TooltipValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";
			$this->banner_g_link->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;
			$this->archivo->TooltipValue = "";

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";
			$this->precios->TooltipValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;
			$this->mapa_img->TooltipValue = "";

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";
			$this->video_final_servidor->TooltipValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";
			$this->video_final_1->TooltipValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";
			$this->video_titu_1->TooltipValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";
			$this->video_final_2->TooltipValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";
			$this->video_titu_2->TooltipValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";
			$this->video_final_3->TooltipValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";
			$this->video_titu_3->TooltipValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";
			$this->video_final_4->TooltipValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";
			$this->video_titu_4->TooltipValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";
			$this->video_final_5->TooltipValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
			$this->video_titu_5->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("remateslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($remates_delete)) $remates_delete = new cremates_delete();

// Page init
$remates_delete->Page_Init();

// Page main
$remates_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$remates_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = frematesdelete = new ew_Form("frematesdelete", "delete");

// Form_CustomValidate event
frematesdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
frematesdelete.ValidateRequired = true;
<?php } else { ?>
frematesdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
frematesdelete.Lists["x_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_tipo"].Options = <?php echo json_encode($remates->tipo->Options()) ?>;
frematesdelete.Lists["x_plataforma"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_plataforma"].Options = <?php echo json_encode($remates->plataforma->Options()) ?>;
frematesdelete.Lists["x_zona"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_zona"].Options = <?php echo json_encode($remates->zona->Options()) ?>;
frematesdelete.Lists["x_yacare_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_yacare_visible"].Options = <?php echo json_encode($remates->yacare_visible->Options()) ?>;
frematesdelete.Lists["x_banner_g_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_banner_g_visible"].Options = <?php echo json_encode($remates->banner_g_visible->Options()) ?>;
frematesdelete.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_activo"].Options = <?php echo json_encode($remates->activo->Options()) ?>;
frematesdelete.Lists["x_precios"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_precios"].Options = <?php echo json_encode($remates->precios->Options()) ?>;
frematesdelete.Lists["x_video_final_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesdelete.Lists["x_video_final_servidor"].Options = <?php echo json_encode($remates->video_final_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $remates_delete->ShowPageHeader(); ?>
<?php
$remates_delete->ShowMessage();
?>
<form name="frematesdelete" id="frematesdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($remates_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $remates_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="remates">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($remates_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $remates->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($remates->id->Visible) { // id ?>
		<th><span id="elh_remates_id" class="remates_id"><?php echo $remates->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->tipo->Visible) { // tipo ?>
		<th><span id="elh_remates_tipo" class="remates_tipo"><?php echo $remates->tipo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->plataforma->Visible) { // plataforma ?>
		<th><span id="elh_remates_plataforma" class="remates_plataforma"><?php echo $remates->plataforma->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->ext_id->Visible) { // ext_id ?>
		<th><span id="elh_remates_ext_id" class="remates_ext_id"><?php echo $remates->ext_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->fecha->Visible) { // fecha ?>
		<th><span id="elh_remates_fecha" class="remates_fecha"><?php echo $remates->fecha->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
		<th><span id="elh_remates_hora_inicio" class="remates_hora_inicio"><?php echo $remates->hora_inicio->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
		<th><span id="elh_remates_hora_fin" class="remates_hora_fin"><?php echo $remates->hora_fin->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->zona->Visible) { // zona ?>
		<th><span id="elh_remates_zona" class="remates_zona"><?php echo $remates->zona->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->lugar->Visible) { // lugar ?>
		<th><span id="elh_remates_lugar" class="remates_lugar"><?php echo $remates->lugar->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->locacion->Visible) { // locacion ?>
		<th><span id="elh_remates_locacion" class="remates_locacion"><?php echo $remates->locacion->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->numero->Visible) { // numero ?>
		<th><span id="elh_remates_numero" class="remates_numero"><?php echo $remates->numero->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->titulo->Visible) { // titulo ?>
		<th><span id="elh_remates_titulo" class="remates_titulo"><?php echo $remates->titulo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->cantidad->Visible) { // cantidad ?>
		<th><span id="elh_remates_cantidad" class="remates_cantidad"><?php echo $remates->cantidad->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
		<th><span id="elh_remates_yacare_visible" class="remates_yacare_visible"><?php echo $remates->yacare_visible->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g->Visible) { // banner_g ?>
		<th><span id="elh_remates_banner_g" class="remates_banner_g"><?php echo $remates->banner_g->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
		<th><span id="elh_remates_banner_g_visible" class="remates_banner_g_visible"><?php echo $remates->banner_g_visible->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
		<th><span id="elh_remates_banner_g_z_index" class="remates_banner_g_z_index"><?php echo $remates->banner_g_z_index->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
		<th><span id="elh_remates_banner_g_pos_x" class="remates_banner_g_pos_x"><?php echo $remates->banner_g_pos_x->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
		<th><span id="elh_remates_banner_g_pos_y" class="remates_banner_g_pos_y"><?php echo $remates->banner_g_pos_y->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
		<th><span id="elh_remates_banner_g_link" class="remates_banner_g_link"><?php echo $remates->banner_g_link->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->activo->Visible) { // activo ?>
		<th><span id="elh_remates_activo" class="remates_activo"><?php echo $remates->activo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->archivo->Visible) { // archivo ?>
		<th><span id="elh_remates_archivo" class="remates_archivo"><?php echo $remates->archivo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->precios->Visible) { // precios ?>
		<th><span id="elh_remates_precios" class="remates_precios"><?php echo $remates->precios->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
		<th><span id="elh_remates_mapa_img" class="remates_mapa_img"><?php echo $remates->mapa_img->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
		<th><span id="elh_remates_video_final_servidor" class="remates_video_final_servidor"><?php echo $remates->video_final_servidor->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
		<th><span id="elh_remates_video_final_1" class="remates_video_final_1"><?php echo $remates->video_final_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
		<th><span id="elh_remates_video_titu_1" class="remates_video_titu_1"><?php echo $remates->video_titu_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
		<th><span id="elh_remates_video_final_2" class="remates_video_final_2"><?php echo $remates->video_final_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
		<th><span id="elh_remates_video_titu_2" class="remates_video_titu_2"><?php echo $remates->video_titu_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
		<th><span id="elh_remates_video_final_3" class="remates_video_final_3"><?php echo $remates->video_final_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
		<th><span id="elh_remates_video_titu_3" class="remates_video_titu_3"><?php echo $remates->video_titu_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
		<th><span id="elh_remates_video_final_4" class="remates_video_final_4"><?php echo $remates->video_final_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
		<th><span id="elh_remates_video_titu_4" class="remates_video_titu_4"><?php echo $remates->video_titu_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
		<th><span id="elh_remates_video_final_5" class="remates_video_final_5"><?php echo $remates->video_final_5->FldCaption() ?></span></th>
<?php } ?>
<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
		<th><span id="elh_remates_video_titu_5" class="remates_video_titu_5"><?php echo $remates->video_titu_5->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$remates_delete->RecCnt = 0;
$i = 0;
while (!$remates_delete->Recordset->EOF) {
	$remates_delete->RecCnt++;
	$remates_delete->RowCnt++;

	// Set row properties
	$remates->ResetAttrs();
	$remates->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$remates_delete->LoadRowValues($remates_delete->Recordset);

	// Render row
	$remates_delete->RenderRow();
?>
	<tr<?php echo $remates->RowAttributes() ?>>
<?php if ($remates->id->Visible) { // id ?>
		<td<?php echo $remates->id->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_id" class="remates_id">
<span<?php echo $remates->id->ViewAttributes() ?>>
<?php echo $remates->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->tipo->Visible) { // tipo ?>
		<td<?php echo $remates->tipo->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_tipo" class="remates_tipo">
<span<?php echo $remates->tipo->ViewAttributes() ?>>
<?php echo $remates->tipo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->plataforma->Visible) { // plataforma ?>
		<td<?php echo $remates->plataforma->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_plataforma" class="remates_plataforma">
<span<?php echo $remates->plataforma->ViewAttributes() ?>>
<?php echo $remates->plataforma->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->ext_id->Visible) { // ext_id ?>
		<td<?php echo $remates->ext_id->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_ext_id" class="remates_ext_id">
<span<?php echo $remates->ext_id->ViewAttributes() ?>>
<?php echo $remates->ext_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->fecha->Visible) { // fecha ?>
		<td<?php echo $remates->fecha->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_fecha" class="remates_fecha">
<span<?php echo $remates->fecha->ViewAttributes() ?>>
<?php echo $remates->fecha->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
		<td<?php echo $remates->hora_inicio->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_hora_inicio" class="remates_hora_inicio">
<span<?php echo $remates->hora_inicio->ViewAttributes() ?>>
<?php echo $remates->hora_inicio->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
		<td<?php echo $remates->hora_fin->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_hora_fin" class="remates_hora_fin">
<span<?php echo $remates->hora_fin->ViewAttributes() ?>>
<?php echo $remates->hora_fin->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->zona->Visible) { // zona ?>
		<td<?php echo $remates->zona->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_zona" class="remates_zona">
<span<?php echo $remates->zona->ViewAttributes() ?>>
<?php echo $remates->zona->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->lugar->Visible) { // lugar ?>
		<td<?php echo $remates->lugar->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_lugar" class="remates_lugar">
<span<?php echo $remates->lugar->ViewAttributes() ?>>
<?php echo $remates->lugar->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->locacion->Visible) { // locacion ?>
		<td<?php echo $remates->locacion->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_locacion" class="remates_locacion">
<span<?php echo $remates->locacion->ViewAttributes() ?>>
<?php echo $remates->locacion->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->numero->Visible) { // numero ?>
		<td<?php echo $remates->numero->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_numero" class="remates_numero">
<span<?php echo $remates->numero->ViewAttributes() ?>>
<?php echo $remates->numero->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->titulo->Visible) { // titulo ?>
		<td<?php echo $remates->titulo->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_titulo" class="remates_titulo">
<span<?php echo $remates->titulo->ViewAttributes() ?>>
<?php echo $remates->titulo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->cantidad->Visible) { // cantidad ?>
		<td<?php echo $remates->cantidad->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_cantidad" class="remates_cantidad">
<span<?php echo $remates->cantidad->ViewAttributes() ?>>
<?php echo $remates->cantidad->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
		<td<?php echo $remates->yacare_visible->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_yacare_visible" class="remates_yacare_visible">
<span<?php echo $remates->yacare_visible->ViewAttributes() ?>>
<?php echo $remates->yacare_visible->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g->Visible) { // banner_g ?>
		<td<?php echo $remates->banner_g->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g" class="remates_banner_g">
<span<?php echo $remates->banner_g->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->banner_g, $remates->banner_g->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
		<td<?php echo $remates->banner_g_visible->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g_visible" class="remates_banner_g_visible">
<span<?php echo $remates->banner_g_visible->ViewAttributes() ?>>
<?php echo $remates->banner_g_visible->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
		<td<?php echo $remates->banner_g_z_index->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g_z_index" class="remates_banner_g_z_index">
<span<?php echo $remates->banner_g_z_index->ViewAttributes() ?>>
<?php echo $remates->banner_g_z_index->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
		<td<?php echo $remates->banner_g_pos_x->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g_pos_x" class="remates_banner_g_pos_x">
<span<?php echo $remates->banner_g_pos_x->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_x->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
		<td<?php echo $remates->banner_g_pos_y->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g_pos_y" class="remates_banner_g_pos_y">
<span<?php echo $remates->banner_g_pos_y->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_y->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
		<td<?php echo $remates->banner_g_link->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_banner_g_link" class="remates_banner_g_link">
<span<?php echo $remates->banner_g_link->ViewAttributes() ?>>
<?php echo $remates->banner_g_link->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->activo->Visible) { // activo ?>
		<td<?php echo $remates->activo->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_activo" class="remates_activo">
<span<?php echo $remates->activo->ViewAttributes() ?>>
<?php echo $remates->activo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->archivo->Visible) { // archivo ?>
		<td<?php echo $remates->archivo->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_archivo" class="remates_archivo">
<span<?php echo $remates->archivo->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->archivo, $remates->archivo->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($remates->precios->Visible) { // precios ?>
		<td<?php echo $remates->precios->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_precios" class="remates_precios">
<span<?php echo $remates->precios->ViewAttributes() ?>>
<?php echo $remates->precios->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
		<td<?php echo $remates->mapa_img->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_mapa_img" class="remates_mapa_img">
<span<?php echo $remates->mapa_img->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->mapa_img, $remates->mapa_img->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
		<td<?php echo $remates->video_final_servidor->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_servidor" class="remates_video_final_servidor">
<span<?php echo $remates->video_final_servidor->ViewAttributes() ?>>
<?php echo $remates->video_final_servidor->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
		<td<?php echo $remates->video_final_1->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_1" class="remates_video_final_1">
<span<?php echo $remates->video_final_1->ViewAttributes() ?>>
<?php echo $remates->video_final_1->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
		<td<?php echo $remates->video_titu_1->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_titu_1" class="remates_video_titu_1">
<span<?php echo $remates->video_titu_1->ViewAttributes() ?>>
<?php echo $remates->video_titu_1->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
		<td<?php echo $remates->video_final_2->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_2" class="remates_video_final_2">
<span<?php echo $remates->video_final_2->ViewAttributes() ?>>
<?php echo $remates->video_final_2->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
		<td<?php echo $remates->video_titu_2->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_titu_2" class="remates_video_titu_2">
<span<?php echo $remates->video_titu_2->ViewAttributes() ?>>
<?php echo $remates->video_titu_2->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
		<td<?php echo $remates->video_final_3->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_3" class="remates_video_final_3">
<span<?php echo $remates->video_final_3->ViewAttributes() ?>>
<?php echo $remates->video_final_3->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
		<td<?php echo $remates->video_titu_3->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_titu_3" class="remates_video_titu_3">
<span<?php echo $remates->video_titu_3->ViewAttributes() ?>>
<?php echo $remates->video_titu_3->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
		<td<?php echo $remates->video_final_4->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_4" class="remates_video_final_4">
<span<?php echo $remates->video_final_4->ViewAttributes() ?>>
<?php echo $remates->video_final_4->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
		<td<?php echo $remates->video_titu_4->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_titu_4" class="remates_video_titu_4">
<span<?php echo $remates->video_titu_4->ViewAttributes() ?>>
<?php echo $remates->video_titu_4->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
		<td<?php echo $remates->video_final_5->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_final_5" class="remates_video_final_5">
<span<?php echo $remates->video_final_5->ViewAttributes() ?>>
<?php echo $remates->video_final_5->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
		<td<?php echo $remates->video_titu_5->CellAttributes() ?>>
<span id="el<?php echo $remates_delete->RowCnt ?>_remates_video_titu_5" class="remates_video_titu_5">
<span<?php echo $remates->video_titu_5->ViewAttributes() ?>>
<?php echo $remates->video_titu_5->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$remates_delete->Recordset->MoveNext();
}
$remates_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $remates_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
frematesdelete.Init();
</script>
<?php
$remates_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$remates_delete->Page_Terminate();
?>
