<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "rematesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$remates_list = NULL; // Initialize page object first

class cremates_list extends cremates {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'remates';

	// Page object name
	var $PageObjName = 'remates_list';

	// Grid form hidden field names
	var $FormName = 'fremateslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (remates)
		if (!isset($GLOBALS["remates"]) || get_class($GLOBALS["remates"]) == "cremates") {
			$GLOBALS["remates"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["remates"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "rematesadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "rematesdelete.php";
		$this->MultiUpdateUrl = "rematesupdate.php";

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'remates', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fremateslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->tipo->SetVisibility();
		$this->plataforma->SetVisibility();
		$this->ext_id->SetVisibility();
		$this->fecha->SetVisibility();
		$this->hora_inicio->SetVisibility();
		$this->hora_fin->SetVisibility();
		$this->zona->SetVisibility();
		$this->lugar->SetVisibility();
		$this->locacion->SetVisibility();
		$this->numero->SetVisibility();
		$this->titulo->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->yacare_visible->SetVisibility();
		$this->banner_g->SetVisibility();
		$this->banner_g_visible->SetVisibility();
		$this->banner_g_z_index->SetVisibility();
		$this->banner_g_pos_x->SetVisibility();
		$this->banner_g_pos_y->SetVisibility();
		$this->banner_g_link->SetVisibility();
		$this->activo->SetVisibility();
		$this->archivo->SetVisibility();
		$this->precios->SetVisibility();
		$this->mapa_img->SetVisibility();
		$this->video_final_servidor->SetVisibility();
		$this->video_final_1->SetVisibility();
		$this->video_titu_1->SetVisibility();
		$this->video_final_2->SetVisibility();
		$this->video_titu_2->SetVisibility();
		$this->video_final_3->SetVisibility();
		$this->video_titu_3->SetVisibility();
		$this->video_final_4->SetVisibility();
		$this->video_titu_4->SetVisibility();
		$this->video_final_5->SetVisibility();
		$this->video_titu_5->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $remates;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($remates);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to inline edit mode
				if ($this->CurrentAction == "edit")
					$this->InlineEditMode();

				// Switch to inline add mode
				if ($this->CurrentAction == "add" || $this->CurrentAction == "copy")
					$this->InlineAddMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Inline Update
					if (($this->CurrentAction == "update" || $this->CurrentAction == "overwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "edit")
						$this->InlineUpdate();

					// Insert Inline
					if ($this->CurrentAction == "insert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "add")
						$this->InlineInsert();

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Process filter list
			$this->ProcessFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->setKey("id", ""); // Clear inline edit key
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Switch to Inline Edit mode
	function InlineEditMode() {
		global $Security, $Language;
		if (!$Security->CanEdit())
			$this->Page_Terminate("login.php"); // Go to login page
		$bInlineEdit = TRUE;
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		} else {
			$bInlineEdit = FALSE;
		}
		if ($bInlineEdit) {
			if ($this->LoadRow()) {
				$this->setKey("id", $this->id->CurrentValue); // Set up inline edit key
				$_SESSION[EW_SESSION_INLINE_MODE] = "edit"; // Enable inline edit
			}
		}
	}

	// Perform update to Inline Edit record
	function InlineUpdate() {
		global $Language, $objForm, $gsFormError;
		$objForm->Index = 1; 
		$this->LoadFormValues(); // Get form values

		// Validate form
		$bInlineUpdate = TRUE;
		if (!$this->ValidateForm()) {	
			$bInlineUpdate = FALSE; // Form error, reset action
			$this->setFailureMessage($gsFormError);
		} else {
			$bInlineUpdate = FALSE;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			if ($this->SetupKeyValues($rowkey)) { // Set up key values
				if ($this->CheckInlineEditKey()) { // Check key
					$this->SendEmail = TRUE; // Send email on update success
					$bInlineUpdate = $this->EditRow(); // Update record
				} else {
					$bInlineUpdate = FALSE;
				}
			}
		}
		if ($bInlineUpdate) { // Update success
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
			$this->EventCancelled = TRUE; // Cancel event
			$this->CurrentAction = "edit"; // Stay in edit mode
		}
	}

	// Check Inline Edit key
	function CheckInlineEditKey() {

		//CheckInlineEditKey = True
		if (strval($this->getKey("id")) <> strval($this->id->CurrentValue))
			return FALSE;
		return TRUE;
	}

	// Switch to Inline Add mode
	function InlineAddMode() {
		global $Security, $Language;
		if (!$Security->CanAdd())
			$this->Page_Terminate("login.php"); // Return to login page
		if ($this->CurrentAction == "copy") {
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CurrentAction = "add";
			}
		}
		$_SESSION[EW_SESSION_INLINE_MODE] = "add"; // Enable inline add
	}

	// Perform update to Inline Add/Copy record
	function InlineInsert() {
		global $Language, $objForm, $gsFormError;
		$this->LoadOldRecord(); // Load old recordset
		$objForm->Index = 0;
		$this->LoadFormValues(); // Get form values

		// Validate form
		if (!$this->ValidateForm()) {
			$this->setFailureMessage($gsFormError); // Set validation error message
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
			return;
		}
		$this->SendEmail = TRUE; // Send email on add success
		if ($this->AddRow($this->OldRecordset)) { // Add record
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up add success message
			$this->ClearInlineMode(); // Clear inline add mode
		} else { // Add failed
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
		}
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_tipo") && $objForm->HasValue("o_tipo") && $this->tipo->CurrentValue <> $this->tipo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_plataforma") && $objForm->HasValue("o_plataforma") && $this->plataforma->CurrentValue <> $this->plataforma->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_ext_id") && $objForm->HasValue("o_ext_id") && $this->ext_id->CurrentValue <> $this->ext_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_fecha") && $objForm->HasValue("o_fecha") && $this->fecha->CurrentValue <> $this->fecha->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_hora_inicio") && $objForm->HasValue("o_hora_inicio") && $this->hora_inicio->CurrentValue <> $this->hora_inicio->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_hora_fin") && $objForm->HasValue("o_hora_fin") && $this->hora_fin->CurrentValue <> $this->hora_fin->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_zona") && $objForm->HasValue("o_zona") && $this->zona->CurrentValue <> $this->zona->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_lugar") && $objForm->HasValue("o_lugar") && $this->lugar->CurrentValue <> $this->lugar->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_locacion") && $objForm->HasValue("o_locacion") && $this->locacion->CurrentValue <> $this->locacion->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_numero") && $objForm->HasValue("o_numero") && $this->numero->CurrentValue <> $this->numero->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_titulo") && $objForm->HasValue("o_titulo") && $this->titulo->CurrentValue <> $this->titulo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_cantidad") && $objForm->HasValue("o_cantidad") && $this->cantidad->CurrentValue <> $this->cantidad->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_yacare_visible") && $objForm->HasValue("o_yacare_visible") && $this->yacare_visible->CurrentValue <> $this->yacare_visible->OldValue)
			return FALSE;
		if (!ew_Empty($this->banner_g->Upload->Value))
			return FALSE;
		if ($objForm->HasValue("x_banner_g_visible") && $objForm->HasValue("o_banner_g_visible") && $this->banner_g_visible->CurrentValue <> $this->banner_g_visible->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_banner_g_z_index") && $objForm->HasValue("o_banner_g_z_index") && $this->banner_g_z_index->CurrentValue <> $this->banner_g_z_index->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_banner_g_pos_x") && $objForm->HasValue("o_banner_g_pos_x") && $this->banner_g_pos_x->CurrentValue <> $this->banner_g_pos_x->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_banner_g_pos_y") && $objForm->HasValue("o_banner_g_pos_y") && $this->banner_g_pos_y->CurrentValue <> $this->banner_g_pos_y->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_banner_g_link") && $objForm->HasValue("o_banner_g_link") && $this->banner_g_link->CurrentValue <> $this->banner_g_link->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_activo") && $objForm->HasValue("o_activo") && $this->activo->CurrentValue <> $this->activo->OldValue)
			return FALSE;
		if (!ew_Empty($this->archivo->Upload->Value))
			return FALSE;
		if ($objForm->HasValue("x_precios") && $objForm->HasValue("o_precios") && $this->precios->CurrentValue <> $this->precios->OldValue)
			return FALSE;
		if (!ew_Empty($this->mapa_img->Upload->Value))
			return FALSE;
		if ($objForm->HasValue("x_video_final_servidor") && $objForm->HasValue("o_video_final_servidor") && $this->video_final_servidor->CurrentValue <> $this->video_final_servidor->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_final_1") && $objForm->HasValue("o_video_final_1") && $this->video_final_1->CurrentValue <> $this->video_final_1->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_titu_1") && $objForm->HasValue("o_video_titu_1") && $this->video_titu_1->CurrentValue <> $this->video_titu_1->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_final_2") && $objForm->HasValue("o_video_final_2") && $this->video_final_2->CurrentValue <> $this->video_final_2->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_titu_2") && $objForm->HasValue("o_video_titu_2") && $this->video_titu_2->CurrentValue <> $this->video_titu_2->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_final_3") && $objForm->HasValue("o_video_final_3") && $this->video_final_3->CurrentValue <> $this->video_final_3->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_titu_3") && $objForm->HasValue("o_video_titu_3") && $this->video_titu_3->CurrentValue <> $this->video_titu_3->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_final_4") && $objForm->HasValue("o_video_final_4") && $this->video_final_4->CurrentValue <> $this->video_final_4->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_titu_4") && $objForm->HasValue("o_video_titu_4") && $this->video_titu_4->CurrentValue <> $this->video_titu_4->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_final_5") && $objForm->HasValue("o_video_final_5") && $this->video_final_5->CurrentValue <> $this->video_final_5->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video_titu_5") && $objForm->HasValue("o_video_titu_5") && $this->video_titu_5->CurrentValue <> $this->video_titu_5->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {
		global $UserProfile;

		// Load server side filters
		if (EW_SEARCH_FILTER_OPTION == "Server") {
			$sSavedFilterList = $UserProfile->GetSearchFilters(CurrentUserName(), "fremateslistsrch");
		} else {
			$sSavedFilterList = "";
		}

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->id->AdvancedSearch->ToJSON(), ","); // Field id
		$sFilterList = ew_Concat($sFilterList, $this->tipo->AdvancedSearch->ToJSON(), ","); // Field tipo
		$sFilterList = ew_Concat($sFilterList, $this->plataforma->AdvancedSearch->ToJSON(), ","); // Field plataforma
		$sFilterList = ew_Concat($sFilterList, $this->ext_id->AdvancedSearch->ToJSON(), ","); // Field ext_id
		$sFilterList = ew_Concat($sFilterList, $this->fecha->AdvancedSearch->ToJSON(), ","); // Field fecha
		$sFilterList = ew_Concat($sFilterList, $this->hora_inicio->AdvancedSearch->ToJSON(), ","); // Field hora_inicio
		$sFilterList = ew_Concat($sFilterList, $this->hora_fin->AdvancedSearch->ToJSON(), ","); // Field hora_fin
		$sFilterList = ew_Concat($sFilterList, $this->zona->AdvancedSearch->ToJSON(), ","); // Field zona
		$sFilterList = ew_Concat($sFilterList, $this->lugar->AdvancedSearch->ToJSON(), ","); // Field lugar
		$sFilterList = ew_Concat($sFilterList, $this->locacion->AdvancedSearch->ToJSON(), ","); // Field locacion
		$sFilterList = ew_Concat($sFilterList, $this->numero->AdvancedSearch->ToJSON(), ","); // Field numero
		$sFilterList = ew_Concat($sFilterList, $this->titulo->AdvancedSearch->ToJSON(), ","); // Field titulo
		$sFilterList = ew_Concat($sFilterList, $this->cantidad->AdvancedSearch->ToJSON(), ","); // Field cantidad
		$sFilterList = ew_Concat($sFilterList, $this->descripcion->AdvancedSearch->ToJSON(), ","); // Field descripcion
		$sFilterList = ew_Concat($sFilterList, $this->yacare_visible->AdvancedSearch->ToJSON(), ","); // Field yacare_visible
		$sFilterList = ew_Concat($sFilterList, $this->banner_g->AdvancedSearch->ToJSON(), ","); // Field banner_g
		$sFilterList = ew_Concat($sFilterList, $this->banner_g_visible->AdvancedSearch->ToJSON(), ","); // Field banner_g_visible
		$sFilterList = ew_Concat($sFilterList, $this->banner_g_z_index->AdvancedSearch->ToJSON(), ","); // Field banner_g_z_index
		$sFilterList = ew_Concat($sFilterList, $this->banner_g_pos_x->AdvancedSearch->ToJSON(), ","); // Field banner_g_pos_x
		$sFilterList = ew_Concat($sFilterList, $this->banner_g_pos_y->AdvancedSearch->ToJSON(), ","); // Field banner_g_pos_y
		$sFilterList = ew_Concat($sFilterList, $this->banner_g_link->AdvancedSearch->ToJSON(), ","); // Field banner_g_link
		$sFilterList = ew_Concat($sFilterList, $this->activo->AdvancedSearch->ToJSON(), ","); // Field activo
		$sFilterList = ew_Concat($sFilterList, $this->archivo->AdvancedSearch->ToJSON(), ","); // Field archivo
		$sFilterList = ew_Concat($sFilterList, $this->precios->AdvancedSearch->ToJSON(), ","); // Field precios
		$sFilterList = ew_Concat($sFilterList, $this->mapa_img->AdvancedSearch->ToJSON(), ","); // Field mapa_img
		$sFilterList = ew_Concat($sFilterList, $this->video_final_servidor->AdvancedSearch->ToJSON(), ","); // Field video_final_servidor
		$sFilterList = ew_Concat($sFilterList, $this->video_final_1->AdvancedSearch->ToJSON(), ","); // Field video_final_1
		$sFilterList = ew_Concat($sFilterList, $this->video_titu_1->AdvancedSearch->ToJSON(), ","); // Field video_titu_1
		$sFilterList = ew_Concat($sFilterList, $this->video_final_2->AdvancedSearch->ToJSON(), ","); // Field video_final_2
		$sFilterList = ew_Concat($sFilterList, $this->video_titu_2->AdvancedSearch->ToJSON(), ","); // Field video_titu_2
		$sFilterList = ew_Concat($sFilterList, $this->video_final_3->AdvancedSearch->ToJSON(), ","); // Field video_final_3
		$sFilterList = ew_Concat($sFilterList, $this->video_titu_3->AdvancedSearch->ToJSON(), ","); // Field video_titu_3
		$sFilterList = ew_Concat($sFilterList, $this->video_final_4->AdvancedSearch->ToJSON(), ","); // Field video_final_4
		$sFilterList = ew_Concat($sFilterList, $this->video_titu_4->AdvancedSearch->ToJSON(), ","); // Field video_titu_4
		$sFilterList = ew_Concat($sFilterList, $this->video_final_5->AdvancedSearch->ToJSON(), ","); // Field video_final_5
		$sFilterList = ew_Concat($sFilterList, $this->video_titu_5->AdvancedSearch->ToJSON(), ","); // Field video_titu_5
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}
		$sFilterList = preg_replace('/,$/', "", $sFilterList);

		// Return filter list in json
		if ($sFilterList <> "")
			$sFilterList = "\"data\":{" . $sFilterList . "}";
		if ($sSavedFilterList <> "") {
			if ($sFilterList <> "")
				$sFilterList .= ",";
			$sFilterList .= "\"filters\":" . $sSavedFilterList;
		}
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Process filter list
	function ProcessFilterList() {
		global $UserProfile;
		if (@$_POST["ajax"] == "savefilters") { // Save filter request (Ajax)
			$filters = ew_StripSlashes(@$_POST["filters"]);
			$UserProfile->SetSearchFilters(CurrentUserName(), "fremateslistsrch", $filters);

			// Clean output buffer
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			echo ew_ArrayToJson(array(array("success" => TRUE))); // Success
			$this->Page_Terminate();
			exit();
		} elseif (@$_POST["cmd"] == "resetfilter") {
			$this->RestoreFilterList();
		}
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field id
		$this->id->AdvancedSearch->SearchValue = @$filter["x_id"];
		$this->id->AdvancedSearch->SearchOperator = @$filter["z_id"];
		$this->id->AdvancedSearch->SearchCondition = @$filter["v_id"];
		$this->id->AdvancedSearch->SearchValue2 = @$filter["y_id"];
		$this->id->AdvancedSearch->SearchOperator2 = @$filter["w_id"];
		$this->id->AdvancedSearch->Save();

		// Field tipo
		$this->tipo->AdvancedSearch->SearchValue = @$filter["x_tipo"];
		$this->tipo->AdvancedSearch->SearchOperator = @$filter["z_tipo"];
		$this->tipo->AdvancedSearch->SearchCondition = @$filter["v_tipo"];
		$this->tipo->AdvancedSearch->SearchValue2 = @$filter["y_tipo"];
		$this->tipo->AdvancedSearch->SearchOperator2 = @$filter["w_tipo"];
		$this->tipo->AdvancedSearch->Save();

		// Field plataforma
		$this->plataforma->AdvancedSearch->SearchValue = @$filter["x_plataforma"];
		$this->plataforma->AdvancedSearch->SearchOperator = @$filter["z_plataforma"];
		$this->plataforma->AdvancedSearch->SearchCondition = @$filter["v_plataforma"];
		$this->plataforma->AdvancedSearch->SearchValue2 = @$filter["y_plataforma"];
		$this->plataforma->AdvancedSearch->SearchOperator2 = @$filter["w_plataforma"];
		$this->plataforma->AdvancedSearch->Save();

		// Field ext_id
		$this->ext_id->AdvancedSearch->SearchValue = @$filter["x_ext_id"];
		$this->ext_id->AdvancedSearch->SearchOperator = @$filter["z_ext_id"];
		$this->ext_id->AdvancedSearch->SearchCondition = @$filter["v_ext_id"];
		$this->ext_id->AdvancedSearch->SearchValue2 = @$filter["y_ext_id"];
		$this->ext_id->AdvancedSearch->SearchOperator2 = @$filter["w_ext_id"];
		$this->ext_id->AdvancedSearch->Save();

		// Field fecha
		$this->fecha->AdvancedSearch->SearchValue = @$filter["x_fecha"];
		$this->fecha->AdvancedSearch->SearchOperator = @$filter["z_fecha"];
		$this->fecha->AdvancedSearch->SearchCondition = @$filter["v_fecha"];
		$this->fecha->AdvancedSearch->SearchValue2 = @$filter["y_fecha"];
		$this->fecha->AdvancedSearch->SearchOperator2 = @$filter["w_fecha"];
		$this->fecha->AdvancedSearch->Save();

		// Field hora_inicio
		$this->hora_inicio->AdvancedSearch->SearchValue = @$filter["x_hora_inicio"];
		$this->hora_inicio->AdvancedSearch->SearchOperator = @$filter["z_hora_inicio"];
		$this->hora_inicio->AdvancedSearch->SearchCondition = @$filter["v_hora_inicio"];
		$this->hora_inicio->AdvancedSearch->SearchValue2 = @$filter["y_hora_inicio"];
		$this->hora_inicio->AdvancedSearch->SearchOperator2 = @$filter["w_hora_inicio"];
		$this->hora_inicio->AdvancedSearch->Save();

		// Field hora_fin
		$this->hora_fin->AdvancedSearch->SearchValue = @$filter["x_hora_fin"];
		$this->hora_fin->AdvancedSearch->SearchOperator = @$filter["z_hora_fin"];
		$this->hora_fin->AdvancedSearch->SearchCondition = @$filter["v_hora_fin"];
		$this->hora_fin->AdvancedSearch->SearchValue2 = @$filter["y_hora_fin"];
		$this->hora_fin->AdvancedSearch->SearchOperator2 = @$filter["w_hora_fin"];
		$this->hora_fin->AdvancedSearch->Save();

		// Field zona
		$this->zona->AdvancedSearch->SearchValue = @$filter["x_zona"];
		$this->zona->AdvancedSearch->SearchOperator = @$filter["z_zona"];
		$this->zona->AdvancedSearch->SearchCondition = @$filter["v_zona"];
		$this->zona->AdvancedSearch->SearchValue2 = @$filter["y_zona"];
		$this->zona->AdvancedSearch->SearchOperator2 = @$filter["w_zona"];
		$this->zona->AdvancedSearch->Save();

		// Field lugar
		$this->lugar->AdvancedSearch->SearchValue = @$filter["x_lugar"];
		$this->lugar->AdvancedSearch->SearchOperator = @$filter["z_lugar"];
		$this->lugar->AdvancedSearch->SearchCondition = @$filter["v_lugar"];
		$this->lugar->AdvancedSearch->SearchValue2 = @$filter["y_lugar"];
		$this->lugar->AdvancedSearch->SearchOperator2 = @$filter["w_lugar"];
		$this->lugar->AdvancedSearch->Save();

		// Field locacion
		$this->locacion->AdvancedSearch->SearchValue = @$filter["x_locacion"];
		$this->locacion->AdvancedSearch->SearchOperator = @$filter["z_locacion"];
		$this->locacion->AdvancedSearch->SearchCondition = @$filter["v_locacion"];
		$this->locacion->AdvancedSearch->SearchValue2 = @$filter["y_locacion"];
		$this->locacion->AdvancedSearch->SearchOperator2 = @$filter["w_locacion"];
		$this->locacion->AdvancedSearch->Save();

		// Field numero
		$this->numero->AdvancedSearch->SearchValue = @$filter["x_numero"];
		$this->numero->AdvancedSearch->SearchOperator = @$filter["z_numero"];
		$this->numero->AdvancedSearch->SearchCondition = @$filter["v_numero"];
		$this->numero->AdvancedSearch->SearchValue2 = @$filter["y_numero"];
		$this->numero->AdvancedSearch->SearchOperator2 = @$filter["w_numero"];
		$this->numero->AdvancedSearch->Save();

		// Field titulo
		$this->titulo->AdvancedSearch->SearchValue = @$filter["x_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator = @$filter["z_titulo"];
		$this->titulo->AdvancedSearch->SearchCondition = @$filter["v_titulo"];
		$this->titulo->AdvancedSearch->SearchValue2 = @$filter["y_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator2 = @$filter["w_titulo"];
		$this->titulo->AdvancedSearch->Save();

		// Field cantidad
		$this->cantidad->AdvancedSearch->SearchValue = @$filter["x_cantidad"];
		$this->cantidad->AdvancedSearch->SearchOperator = @$filter["z_cantidad"];
		$this->cantidad->AdvancedSearch->SearchCondition = @$filter["v_cantidad"];
		$this->cantidad->AdvancedSearch->SearchValue2 = @$filter["y_cantidad"];
		$this->cantidad->AdvancedSearch->SearchOperator2 = @$filter["w_cantidad"];
		$this->cantidad->AdvancedSearch->Save();

		// Field descripcion
		$this->descripcion->AdvancedSearch->SearchValue = @$filter["x_descripcion"];
		$this->descripcion->AdvancedSearch->SearchOperator = @$filter["z_descripcion"];
		$this->descripcion->AdvancedSearch->SearchCondition = @$filter["v_descripcion"];
		$this->descripcion->AdvancedSearch->SearchValue2 = @$filter["y_descripcion"];
		$this->descripcion->AdvancedSearch->SearchOperator2 = @$filter["w_descripcion"];
		$this->descripcion->AdvancedSearch->Save();

		// Field yacare_visible
		$this->yacare_visible->AdvancedSearch->SearchValue = @$filter["x_yacare_visible"];
		$this->yacare_visible->AdvancedSearch->SearchOperator = @$filter["z_yacare_visible"];
		$this->yacare_visible->AdvancedSearch->SearchCondition = @$filter["v_yacare_visible"];
		$this->yacare_visible->AdvancedSearch->SearchValue2 = @$filter["y_yacare_visible"];
		$this->yacare_visible->AdvancedSearch->SearchOperator2 = @$filter["w_yacare_visible"];
		$this->yacare_visible->AdvancedSearch->Save();

		// Field banner_g
		$this->banner_g->AdvancedSearch->SearchValue = @$filter["x_banner_g"];
		$this->banner_g->AdvancedSearch->SearchOperator = @$filter["z_banner_g"];
		$this->banner_g->AdvancedSearch->SearchCondition = @$filter["v_banner_g"];
		$this->banner_g->AdvancedSearch->SearchValue2 = @$filter["y_banner_g"];
		$this->banner_g->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g"];
		$this->banner_g->AdvancedSearch->Save();

		// Field banner_g_visible
		$this->banner_g_visible->AdvancedSearch->SearchValue = @$filter["x_banner_g_visible"];
		$this->banner_g_visible->AdvancedSearch->SearchOperator = @$filter["z_banner_g_visible"];
		$this->banner_g_visible->AdvancedSearch->SearchCondition = @$filter["v_banner_g_visible"];
		$this->banner_g_visible->AdvancedSearch->SearchValue2 = @$filter["y_banner_g_visible"];
		$this->banner_g_visible->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g_visible"];
		$this->banner_g_visible->AdvancedSearch->Save();

		// Field banner_g_z_index
		$this->banner_g_z_index->AdvancedSearch->SearchValue = @$filter["x_banner_g_z_index"];
		$this->banner_g_z_index->AdvancedSearch->SearchOperator = @$filter["z_banner_g_z_index"];
		$this->banner_g_z_index->AdvancedSearch->SearchCondition = @$filter["v_banner_g_z_index"];
		$this->banner_g_z_index->AdvancedSearch->SearchValue2 = @$filter["y_banner_g_z_index"];
		$this->banner_g_z_index->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g_z_index"];
		$this->banner_g_z_index->AdvancedSearch->Save();

		// Field banner_g_pos_x
		$this->banner_g_pos_x->AdvancedSearch->SearchValue = @$filter["x_banner_g_pos_x"];
		$this->banner_g_pos_x->AdvancedSearch->SearchOperator = @$filter["z_banner_g_pos_x"];
		$this->banner_g_pos_x->AdvancedSearch->SearchCondition = @$filter["v_banner_g_pos_x"];
		$this->banner_g_pos_x->AdvancedSearch->SearchValue2 = @$filter["y_banner_g_pos_x"];
		$this->banner_g_pos_x->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g_pos_x"];
		$this->banner_g_pos_x->AdvancedSearch->Save();

		// Field banner_g_pos_y
		$this->banner_g_pos_y->AdvancedSearch->SearchValue = @$filter["x_banner_g_pos_y"];
		$this->banner_g_pos_y->AdvancedSearch->SearchOperator = @$filter["z_banner_g_pos_y"];
		$this->banner_g_pos_y->AdvancedSearch->SearchCondition = @$filter["v_banner_g_pos_y"];
		$this->banner_g_pos_y->AdvancedSearch->SearchValue2 = @$filter["y_banner_g_pos_y"];
		$this->banner_g_pos_y->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g_pos_y"];
		$this->banner_g_pos_y->AdvancedSearch->Save();

		// Field banner_g_link
		$this->banner_g_link->AdvancedSearch->SearchValue = @$filter["x_banner_g_link"];
		$this->banner_g_link->AdvancedSearch->SearchOperator = @$filter["z_banner_g_link"];
		$this->banner_g_link->AdvancedSearch->SearchCondition = @$filter["v_banner_g_link"];
		$this->banner_g_link->AdvancedSearch->SearchValue2 = @$filter["y_banner_g_link"];
		$this->banner_g_link->AdvancedSearch->SearchOperator2 = @$filter["w_banner_g_link"];
		$this->banner_g_link->AdvancedSearch->Save();

		// Field activo
		$this->activo->AdvancedSearch->SearchValue = @$filter["x_activo"];
		$this->activo->AdvancedSearch->SearchOperator = @$filter["z_activo"];
		$this->activo->AdvancedSearch->SearchCondition = @$filter["v_activo"];
		$this->activo->AdvancedSearch->SearchValue2 = @$filter["y_activo"];
		$this->activo->AdvancedSearch->SearchOperator2 = @$filter["w_activo"];
		$this->activo->AdvancedSearch->Save();

		// Field archivo
		$this->archivo->AdvancedSearch->SearchValue = @$filter["x_archivo"];
		$this->archivo->AdvancedSearch->SearchOperator = @$filter["z_archivo"];
		$this->archivo->AdvancedSearch->SearchCondition = @$filter["v_archivo"];
		$this->archivo->AdvancedSearch->SearchValue2 = @$filter["y_archivo"];
		$this->archivo->AdvancedSearch->SearchOperator2 = @$filter["w_archivo"];
		$this->archivo->AdvancedSearch->Save();

		// Field precios
		$this->precios->AdvancedSearch->SearchValue = @$filter["x_precios"];
		$this->precios->AdvancedSearch->SearchOperator = @$filter["z_precios"];
		$this->precios->AdvancedSearch->SearchCondition = @$filter["v_precios"];
		$this->precios->AdvancedSearch->SearchValue2 = @$filter["y_precios"];
		$this->precios->AdvancedSearch->SearchOperator2 = @$filter["w_precios"];
		$this->precios->AdvancedSearch->Save();

		// Field mapa_img
		$this->mapa_img->AdvancedSearch->SearchValue = @$filter["x_mapa_img"];
		$this->mapa_img->AdvancedSearch->SearchOperator = @$filter["z_mapa_img"];
		$this->mapa_img->AdvancedSearch->SearchCondition = @$filter["v_mapa_img"];
		$this->mapa_img->AdvancedSearch->SearchValue2 = @$filter["y_mapa_img"];
		$this->mapa_img->AdvancedSearch->SearchOperator2 = @$filter["w_mapa_img"];
		$this->mapa_img->AdvancedSearch->Save();

		// Field video_final_servidor
		$this->video_final_servidor->AdvancedSearch->SearchValue = @$filter["x_video_final_servidor"];
		$this->video_final_servidor->AdvancedSearch->SearchOperator = @$filter["z_video_final_servidor"];
		$this->video_final_servidor->AdvancedSearch->SearchCondition = @$filter["v_video_final_servidor"];
		$this->video_final_servidor->AdvancedSearch->SearchValue2 = @$filter["y_video_final_servidor"];
		$this->video_final_servidor->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_servidor"];
		$this->video_final_servidor->AdvancedSearch->Save();

		// Field video_final_1
		$this->video_final_1->AdvancedSearch->SearchValue = @$filter["x_video_final_1"];
		$this->video_final_1->AdvancedSearch->SearchOperator = @$filter["z_video_final_1"];
		$this->video_final_1->AdvancedSearch->SearchCondition = @$filter["v_video_final_1"];
		$this->video_final_1->AdvancedSearch->SearchValue2 = @$filter["y_video_final_1"];
		$this->video_final_1->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_1"];
		$this->video_final_1->AdvancedSearch->Save();

		// Field video_titu_1
		$this->video_titu_1->AdvancedSearch->SearchValue = @$filter["x_video_titu_1"];
		$this->video_titu_1->AdvancedSearch->SearchOperator = @$filter["z_video_titu_1"];
		$this->video_titu_1->AdvancedSearch->SearchCondition = @$filter["v_video_titu_1"];
		$this->video_titu_1->AdvancedSearch->SearchValue2 = @$filter["y_video_titu_1"];
		$this->video_titu_1->AdvancedSearch->SearchOperator2 = @$filter["w_video_titu_1"];
		$this->video_titu_1->AdvancedSearch->Save();

		// Field video_final_2
		$this->video_final_2->AdvancedSearch->SearchValue = @$filter["x_video_final_2"];
		$this->video_final_2->AdvancedSearch->SearchOperator = @$filter["z_video_final_2"];
		$this->video_final_2->AdvancedSearch->SearchCondition = @$filter["v_video_final_2"];
		$this->video_final_2->AdvancedSearch->SearchValue2 = @$filter["y_video_final_2"];
		$this->video_final_2->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_2"];
		$this->video_final_2->AdvancedSearch->Save();

		// Field video_titu_2
		$this->video_titu_2->AdvancedSearch->SearchValue = @$filter["x_video_titu_2"];
		$this->video_titu_2->AdvancedSearch->SearchOperator = @$filter["z_video_titu_2"];
		$this->video_titu_2->AdvancedSearch->SearchCondition = @$filter["v_video_titu_2"];
		$this->video_titu_2->AdvancedSearch->SearchValue2 = @$filter["y_video_titu_2"];
		$this->video_titu_2->AdvancedSearch->SearchOperator2 = @$filter["w_video_titu_2"];
		$this->video_titu_2->AdvancedSearch->Save();

		// Field video_final_3
		$this->video_final_3->AdvancedSearch->SearchValue = @$filter["x_video_final_3"];
		$this->video_final_3->AdvancedSearch->SearchOperator = @$filter["z_video_final_3"];
		$this->video_final_3->AdvancedSearch->SearchCondition = @$filter["v_video_final_3"];
		$this->video_final_3->AdvancedSearch->SearchValue2 = @$filter["y_video_final_3"];
		$this->video_final_3->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_3"];
		$this->video_final_3->AdvancedSearch->Save();

		// Field video_titu_3
		$this->video_titu_3->AdvancedSearch->SearchValue = @$filter["x_video_titu_3"];
		$this->video_titu_3->AdvancedSearch->SearchOperator = @$filter["z_video_titu_3"];
		$this->video_titu_3->AdvancedSearch->SearchCondition = @$filter["v_video_titu_3"];
		$this->video_titu_3->AdvancedSearch->SearchValue2 = @$filter["y_video_titu_3"];
		$this->video_titu_3->AdvancedSearch->SearchOperator2 = @$filter["w_video_titu_3"];
		$this->video_titu_3->AdvancedSearch->Save();

		// Field video_final_4
		$this->video_final_4->AdvancedSearch->SearchValue = @$filter["x_video_final_4"];
		$this->video_final_4->AdvancedSearch->SearchOperator = @$filter["z_video_final_4"];
		$this->video_final_4->AdvancedSearch->SearchCondition = @$filter["v_video_final_4"];
		$this->video_final_4->AdvancedSearch->SearchValue2 = @$filter["y_video_final_4"];
		$this->video_final_4->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_4"];
		$this->video_final_4->AdvancedSearch->Save();

		// Field video_titu_4
		$this->video_titu_4->AdvancedSearch->SearchValue = @$filter["x_video_titu_4"];
		$this->video_titu_4->AdvancedSearch->SearchOperator = @$filter["z_video_titu_4"];
		$this->video_titu_4->AdvancedSearch->SearchCondition = @$filter["v_video_titu_4"];
		$this->video_titu_4->AdvancedSearch->SearchValue2 = @$filter["y_video_titu_4"];
		$this->video_titu_4->AdvancedSearch->SearchOperator2 = @$filter["w_video_titu_4"];
		$this->video_titu_4->AdvancedSearch->Save();

		// Field video_final_5
		$this->video_final_5->AdvancedSearch->SearchValue = @$filter["x_video_final_5"];
		$this->video_final_5->AdvancedSearch->SearchOperator = @$filter["z_video_final_5"];
		$this->video_final_5->AdvancedSearch->SearchCondition = @$filter["v_video_final_5"];
		$this->video_final_5->AdvancedSearch->SearchValue2 = @$filter["y_video_final_5"];
		$this->video_final_5->AdvancedSearch->SearchOperator2 = @$filter["w_video_final_5"];
		$this->video_final_5->AdvancedSearch->Save();

		// Field video_titu_5
		$this->video_titu_5->AdvancedSearch->SearchValue = @$filter["x_video_titu_5"];
		$this->video_titu_5->AdvancedSearch->SearchOperator = @$filter["z_video_titu_5"];
		$this->video_titu_5->AdvancedSearch->SearchCondition = @$filter["v_video_titu_5"];
		$this->video_titu_5->AdvancedSearch->SearchValue2 = @$filter["y_video_titu_5"];
		$this->video_titu_5->AdvancedSearch->SearchOperator2 = @$filter["w_video_titu_5"];
		$this->video_titu_5->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->id, $Default, FALSE); // id
		$this->BuildSearchSql($sWhere, $this->tipo, $Default, FALSE); // tipo
		$this->BuildSearchSql($sWhere, $this->plataforma, $Default, FALSE); // plataforma
		$this->BuildSearchSql($sWhere, $this->ext_id, $Default, FALSE); // ext_id
		$this->BuildSearchSql($sWhere, $this->fecha, $Default, FALSE); // fecha
		$this->BuildSearchSql($sWhere, $this->hora_inicio, $Default, FALSE); // hora_inicio
		$this->BuildSearchSql($sWhere, $this->hora_fin, $Default, FALSE); // hora_fin
		$this->BuildSearchSql($sWhere, $this->zona, $Default, FALSE); // zona
		$this->BuildSearchSql($sWhere, $this->lugar, $Default, FALSE); // lugar
		$this->BuildSearchSql($sWhere, $this->locacion, $Default, FALSE); // locacion
		$this->BuildSearchSql($sWhere, $this->numero, $Default, FALSE); // numero
		$this->BuildSearchSql($sWhere, $this->titulo, $Default, FALSE); // titulo
		$this->BuildSearchSql($sWhere, $this->cantidad, $Default, FALSE); // cantidad
		$this->BuildSearchSql($sWhere, $this->descripcion, $Default, FALSE); // descripcion
		$this->BuildSearchSql($sWhere, $this->yacare_visible, $Default, FALSE); // yacare_visible
		$this->BuildSearchSql($sWhere, $this->banner_g, $Default, FALSE); // banner_g
		$this->BuildSearchSql($sWhere, $this->banner_g_visible, $Default, FALSE); // banner_g_visible
		$this->BuildSearchSql($sWhere, $this->banner_g_z_index, $Default, FALSE); // banner_g_z_index
		$this->BuildSearchSql($sWhere, $this->banner_g_pos_x, $Default, FALSE); // banner_g_pos_x
		$this->BuildSearchSql($sWhere, $this->banner_g_pos_y, $Default, FALSE); // banner_g_pos_y
		$this->BuildSearchSql($sWhere, $this->banner_g_link, $Default, FALSE); // banner_g_link
		$this->BuildSearchSql($sWhere, $this->activo, $Default, FALSE); // activo
		$this->BuildSearchSql($sWhere, $this->archivo, $Default, FALSE); // archivo
		$this->BuildSearchSql($sWhere, $this->precios, $Default, FALSE); // precios
		$this->BuildSearchSql($sWhere, $this->mapa_img, $Default, FALSE); // mapa_img
		$this->BuildSearchSql($sWhere, $this->video_final_servidor, $Default, FALSE); // video_final_servidor
		$this->BuildSearchSql($sWhere, $this->video_final_1, $Default, FALSE); // video_final_1
		$this->BuildSearchSql($sWhere, $this->video_titu_1, $Default, FALSE); // video_titu_1
		$this->BuildSearchSql($sWhere, $this->video_final_2, $Default, FALSE); // video_final_2
		$this->BuildSearchSql($sWhere, $this->video_titu_2, $Default, FALSE); // video_titu_2
		$this->BuildSearchSql($sWhere, $this->video_final_3, $Default, FALSE); // video_final_3
		$this->BuildSearchSql($sWhere, $this->video_titu_3, $Default, FALSE); // video_titu_3
		$this->BuildSearchSql($sWhere, $this->video_final_4, $Default, FALSE); // video_final_4
		$this->BuildSearchSql($sWhere, $this->video_titu_4, $Default, FALSE); // video_titu_4
		$this->BuildSearchSql($sWhere, $this->video_final_5, $Default, FALSE); // video_final_5
		$this->BuildSearchSql($sWhere, $this->video_titu_5, $Default, FALSE); // video_titu_5

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->id->AdvancedSearch->Save(); // id
			$this->tipo->AdvancedSearch->Save(); // tipo
			$this->plataforma->AdvancedSearch->Save(); // plataforma
			$this->ext_id->AdvancedSearch->Save(); // ext_id
			$this->fecha->AdvancedSearch->Save(); // fecha
			$this->hora_inicio->AdvancedSearch->Save(); // hora_inicio
			$this->hora_fin->AdvancedSearch->Save(); // hora_fin
			$this->zona->AdvancedSearch->Save(); // zona
			$this->lugar->AdvancedSearch->Save(); // lugar
			$this->locacion->AdvancedSearch->Save(); // locacion
			$this->numero->AdvancedSearch->Save(); // numero
			$this->titulo->AdvancedSearch->Save(); // titulo
			$this->cantidad->AdvancedSearch->Save(); // cantidad
			$this->descripcion->AdvancedSearch->Save(); // descripcion
			$this->yacare_visible->AdvancedSearch->Save(); // yacare_visible
			$this->banner_g->AdvancedSearch->Save(); // banner_g
			$this->banner_g_visible->AdvancedSearch->Save(); // banner_g_visible
			$this->banner_g_z_index->AdvancedSearch->Save(); // banner_g_z_index
			$this->banner_g_pos_x->AdvancedSearch->Save(); // banner_g_pos_x
			$this->banner_g_pos_y->AdvancedSearch->Save(); // banner_g_pos_y
			$this->banner_g_link->AdvancedSearch->Save(); // banner_g_link
			$this->activo->AdvancedSearch->Save(); // activo
			$this->archivo->AdvancedSearch->Save(); // archivo
			$this->precios->AdvancedSearch->Save(); // precios
			$this->mapa_img->AdvancedSearch->Save(); // mapa_img
			$this->video_final_servidor->AdvancedSearch->Save(); // video_final_servidor
			$this->video_final_1->AdvancedSearch->Save(); // video_final_1
			$this->video_titu_1->AdvancedSearch->Save(); // video_titu_1
			$this->video_final_2->AdvancedSearch->Save(); // video_final_2
			$this->video_titu_2->AdvancedSearch->Save(); // video_titu_2
			$this->video_final_3->AdvancedSearch->Save(); // video_final_3
			$this->video_titu_3->AdvancedSearch->Save(); // video_titu_3
			$this->video_final_4->AdvancedSearch->Save(); // video_final_4
			$this->video_titu_4->AdvancedSearch->Save(); // video_titu_4
			$this->video_final_5->AdvancedSearch->Save(); // video_final_5
			$this->video_titu_5->AdvancedSearch->Save(); // video_titu_5
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1)
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE || $Fld->FldDataType == EW_DATATYPE_TIME) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->lugar, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->locacion, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->titulo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->descripcion, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->banner_g, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->banner_g_link, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->archivo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->mapa_img, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_final_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_titu_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_final_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_titu_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_final_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_titu_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_final_4, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_titu_4, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_final_5, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video_titu_5, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSQL(&$Where, &$Fld, $arKeywords, $type) {
		global $EW_BASIC_SEARCH_IGNORE_PATTERN;
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if ($EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace($EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->tipo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->plataforma->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->ext_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->fecha->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->hora_inicio->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->hora_fin->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->zona->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->lugar->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->locacion->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->numero->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->titulo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->cantidad->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->descripcion->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->yacare_visible->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g_visible->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g_z_index->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g_pos_x->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g_pos_y->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->banner_g_link->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->activo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->archivo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->precios->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->mapa_img->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_servidor->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_titu_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_titu_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_titu_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_titu_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_final_5->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_titu_5->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->id->AdvancedSearch->UnsetSession();
		$this->tipo->AdvancedSearch->UnsetSession();
		$this->plataforma->AdvancedSearch->UnsetSession();
		$this->ext_id->AdvancedSearch->UnsetSession();
		$this->fecha->AdvancedSearch->UnsetSession();
		$this->hora_inicio->AdvancedSearch->UnsetSession();
		$this->hora_fin->AdvancedSearch->UnsetSession();
		$this->zona->AdvancedSearch->UnsetSession();
		$this->lugar->AdvancedSearch->UnsetSession();
		$this->locacion->AdvancedSearch->UnsetSession();
		$this->numero->AdvancedSearch->UnsetSession();
		$this->titulo->AdvancedSearch->UnsetSession();
		$this->cantidad->AdvancedSearch->UnsetSession();
		$this->descripcion->AdvancedSearch->UnsetSession();
		$this->yacare_visible->AdvancedSearch->UnsetSession();
		$this->banner_g->AdvancedSearch->UnsetSession();
		$this->banner_g_visible->AdvancedSearch->UnsetSession();
		$this->banner_g_z_index->AdvancedSearch->UnsetSession();
		$this->banner_g_pos_x->AdvancedSearch->UnsetSession();
		$this->banner_g_pos_y->AdvancedSearch->UnsetSession();
		$this->banner_g_link->AdvancedSearch->UnsetSession();
		$this->activo->AdvancedSearch->UnsetSession();
		$this->archivo->AdvancedSearch->UnsetSession();
		$this->precios->AdvancedSearch->UnsetSession();
		$this->mapa_img->AdvancedSearch->UnsetSession();
		$this->video_final_servidor->AdvancedSearch->UnsetSession();
		$this->video_final_1->AdvancedSearch->UnsetSession();
		$this->video_titu_1->AdvancedSearch->UnsetSession();
		$this->video_final_2->AdvancedSearch->UnsetSession();
		$this->video_titu_2->AdvancedSearch->UnsetSession();
		$this->video_final_3->AdvancedSearch->UnsetSession();
		$this->video_titu_3->AdvancedSearch->UnsetSession();
		$this->video_final_4->AdvancedSearch->UnsetSession();
		$this->video_titu_4->AdvancedSearch->UnsetSession();
		$this->video_final_5->AdvancedSearch->UnsetSession();
		$this->video_titu_5->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->id->AdvancedSearch->Load();
		$this->tipo->AdvancedSearch->Load();
		$this->plataforma->AdvancedSearch->Load();
		$this->ext_id->AdvancedSearch->Load();
		$this->fecha->AdvancedSearch->Load();
		$this->hora_inicio->AdvancedSearch->Load();
		$this->hora_fin->AdvancedSearch->Load();
		$this->zona->AdvancedSearch->Load();
		$this->lugar->AdvancedSearch->Load();
		$this->locacion->AdvancedSearch->Load();
		$this->numero->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->cantidad->AdvancedSearch->Load();
		$this->descripcion->AdvancedSearch->Load();
		$this->yacare_visible->AdvancedSearch->Load();
		$this->banner_g->AdvancedSearch->Load();
		$this->banner_g_visible->AdvancedSearch->Load();
		$this->banner_g_z_index->AdvancedSearch->Load();
		$this->banner_g_pos_x->AdvancedSearch->Load();
		$this->banner_g_pos_y->AdvancedSearch->Load();
		$this->banner_g_link->AdvancedSearch->Load();
		$this->activo->AdvancedSearch->Load();
		$this->archivo->AdvancedSearch->Load();
		$this->precios->AdvancedSearch->Load();
		$this->mapa_img->AdvancedSearch->Load();
		$this->video_final_servidor->AdvancedSearch->Load();
		$this->video_final_1->AdvancedSearch->Load();
		$this->video_titu_1->AdvancedSearch->Load();
		$this->video_final_2->AdvancedSearch->Load();
		$this->video_titu_2->AdvancedSearch->Load();
		$this->video_final_3->AdvancedSearch->Load();
		$this->video_titu_3->AdvancedSearch->Load();
		$this->video_final_4->AdvancedSearch->Load();
		$this->video_titu_4->AdvancedSearch->Load();
		$this->video_final_5->AdvancedSearch->Load();
		$this->video_titu_5->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->tipo); // tipo
			$this->UpdateSort($this->plataforma); // plataforma
			$this->UpdateSort($this->ext_id); // ext_id
			$this->UpdateSort($this->fecha); // fecha
			$this->UpdateSort($this->hora_inicio); // hora_inicio
			$this->UpdateSort($this->hora_fin); // hora_fin
			$this->UpdateSort($this->zona); // zona
			$this->UpdateSort($this->lugar); // lugar
			$this->UpdateSort($this->locacion); // locacion
			$this->UpdateSort($this->numero); // numero
			$this->UpdateSort($this->titulo); // titulo
			$this->UpdateSort($this->cantidad); // cantidad
			$this->UpdateSort($this->yacare_visible); // yacare_visible
			$this->UpdateSort($this->banner_g); // banner_g
			$this->UpdateSort($this->banner_g_visible); // banner_g_visible
			$this->UpdateSort($this->banner_g_z_index); // banner_g_z_index
			$this->UpdateSort($this->banner_g_pos_x); // banner_g_pos_x
			$this->UpdateSort($this->banner_g_pos_y); // banner_g_pos_y
			$this->UpdateSort($this->banner_g_link); // banner_g_link
			$this->UpdateSort($this->activo); // activo
			$this->UpdateSort($this->archivo); // archivo
			$this->UpdateSort($this->precios); // precios
			$this->UpdateSort($this->mapa_img); // mapa_img
			$this->UpdateSort($this->video_final_servidor); // video_final_servidor
			$this->UpdateSort($this->video_final_1); // video_final_1
			$this->UpdateSort($this->video_titu_1); // video_titu_1
			$this->UpdateSort($this->video_final_2); // video_final_2
			$this->UpdateSort($this->video_titu_2); // video_titu_2
			$this->UpdateSort($this->video_final_3); // video_final_3
			$this->UpdateSort($this->video_titu_3); // video_titu_3
			$this->UpdateSort($this->video_final_4); // video_final_4
			$this->UpdateSort($this->video_titu_4); // video_titu_4
			$this->UpdateSort($this->video_final_5); // video_final_5
			$this->UpdateSort($this->video_titu_5); // video_titu_5
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
				$this->fecha->setSort("DESC");
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->id->setSort("");
				$this->tipo->setSort("");
				$this->plataforma->setSort("");
				$this->ext_id->setSort("");
				$this->fecha->setSort("");
				$this->hora_inicio->setSort("");
				$this->hora_fin->setSort("");
				$this->zona->setSort("");
				$this->lugar->setSort("");
				$this->locacion->setSort("");
				$this->numero->setSort("");
				$this->titulo->setSort("");
				$this->cantidad->setSort("");
				$this->yacare_visible->setSort("");
				$this->banner_g->setSort("");
				$this->banner_g_visible->setSort("");
				$this->banner_g_z_index->setSort("");
				$this->banner_g_pos_x->setSort("");
				$this->banner_g_pos_y->setSort("");
				$this->banner_g_link->setSort("");
				$this->activo->setSort("");
				$this->archivo->setSort("");
				$this->precios->setSort("");
				$this->mapa_img->setSort("");
				$this->video_final_servidor->setSort("");
				$this->video_final_1->setSort("");
				$this->video_titu_1->setSort("");
				$this->video_final_2->setSort("");
				$this->video_titu_2->setSort("");
				$this->video_final_3->setSort("");
				$this->video_titu_3->setSort("");
				$this->video_final_4->setSort("");
				$this->video_titu_4->setSort("");
				$this->video_final_5->setSort("");
				$this->video_titu_5->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = TRUE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = ($Security->CanDelete() || $Security->CanEdit());
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if (($this->CurrentAction == "add" || $this->CurrentAction == "copy") && $this->RowType == EW_ROWTYPE_ADD) { // Inline Add/Copy
			$this->ListOptions->CustomItem = "copy"; // Show copy column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
			$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
				"<a class=\"ewGridLink ewInlineInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("InsertLink") . "</a>&nbsp;" .
				"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
				"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"insert\"></div>";
			return;
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($this->CurrentAction == "edit" && $this->RowType == EW_ROWTYPE_EDIT) { // Inline-Edit
			$this->ListOptions->CustomItem = "edit"; // Show edit column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
					"<a class=\"ewGridLink ewInlineUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . ew_GetHashUrl($this->PageName(), $this->PageObjName . "_row_" . $this->RowCnt) . "');\">" . $Language->Phrase("UpdateLink") . "</a>&nbsp;" .
					"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
					"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"update\"></div>";
			$oListOpt->Body .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_key\" id=\"k" . $this->RowIndex . "_key\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\">";
			return;
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		$viewcaption = ew_HtmlTitle($Language->Phrase("ViewLink"));
		if ($Security->CanView()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . $viewcaption . "\" data-caption=\"" . $viewcaption . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		$editcaption = ew_HtmlTitle($Language->Phrase("EditLink"));
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" href=\"" . ew_HtmlEncode(ew_GetHashUrl($this->InlineEditUrl, $this->PageObjName . "_row_" . $this->RowCnt)) . "\">" . $Language->Phrase("InlineEditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		$copycaption = ew_HtmlTitle($Language->Phrase("CopyLink"));
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineCopyUrl) . "\">" . $Language->Phrase("InlineCopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->id->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("AddLink"));
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Inline Add
		$item = &$option->Add("inlineadd");
		$item->Body = "<a class=\"ewAddEdit ewInlineAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineAddUrl) . "\">" .$Language->Phrase("InlineAddLink") . "</a>";
		$item->Visible = ($this->InlineAddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Add multi delete
		$item = &$option->Add("multidelete");
		$item->Body = "<a class=\"ewAction ewMultiDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fremateslist,url:'" . $this->MultiDeleteUrl . "',msg:ewLanguage.Phrase('DeleteConfirmMsg')});return false;\">" . $Language->Phrase("DeleteSelectedLink") . "</a>";
		$item->Visible = ($Security->CanDelete());

		// Add multi update
		$item = &$option->Add("multiupdate");
		$item->Body = "<a class=\"ewAction ewMultiUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" data-table=\"remates\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fremateslist,url:'" . $this->MultiUpdateUrl . "'});return false;\">" . $Language->Phrase("UpdateSelectedLink") . "</a>";
		$item->Visible = ($Security->CanEdit());

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fremateslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fremateslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fremateslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fremateslistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->banner_g->Upload->Index = $objForm->Index;
		$this->banner_g->Upload->UploadFile();
		$this->banner_g->CurrentValue = $this->banner_g->Upload->FileName;
		$this->archivo->Upload->Index = $objForm->Index;
		$this->archivo->Upload->UploadFile();
		$this->archivo->CurrentValue = $this->archivo->Upload->FileName;
		$this->mapa_img->Upload->Index = $objForm->Index;
		$this->mapa_img->Upload->UploadFile();
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->FileName;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->id->CurrentValue = NULL;
		$this->id->OldValue = $this->id->CurrentValue;
		$this->tipo->CurrentValue = "TV";
		$this->tipo->OldValue = $this->tipo->CurrentValue;
		$this->plataforma->CurrentValue = "WEB";
		$this->plataforma->OldValue = $this->plataforma->CurrentValue;
		$this->ext_id->CurrentValue = NULL;
		$this->ext_id->OldValue = $this->ext_id->CurrentValue;
		$this->fecha->CurrentValue = NULL;
		$this->fecha->OldValue = $this->fecha->CurrentValue;
		$this->hora_inicio->CurrentValue = NULL;
		$this->hora_inicio->OldValue = $this->hora_inicio->CurrentValue;
		$this->hora_fin->CurrentValue = NULL;
		$this->hora_fin->OldValue = $this->hora_fin->CurrentValue;
		$this->zona->CurrentValue = NULL;
		$this->zona->OldValue = $this->zona->CurrentValue;
		$this->lugar->CurrentValue = NULL;
		$this->lugar->OldValue = $this->lugar->CurrentValue;
		$this->locacion->CurrentValue = NULL;
		$this->locacion->OldValue = $this->locacion->CurrentValue;
		$this->numero->CurrentValue = NULL;
		$this->numero->OldValue = $this->numero->CurrentValue;
		$this->titulo->CurrentValue = NULL;
		$this->titulo->OldValue = $this->titulo->CurrentValue;
		$this->cantidad->CurrentValue = NULL;
		$this->cantidad->OldValue = $this->cantidad->CurrentValue;
		$this->yacare_visible->CurrentValue = "SI";
		$this->yacare_visible->OldValue = $this->yacare_visible->CurrentValue;
		$this->banner_g->Upload->DbValue = NULL;
		$this->banner_g->OldValue = $this->banner_g->Upload->DbValue;
		$this->banner_g_visible->CurrentValue = "SI";
		$this->banner_g_visible->OldValue = $this->banner_g_visible->CurrentValue;
		$this->banner_g_z_index->CurrentValue = NULL;
		$this->banner_g_z_index->OldValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_pos_x->CurrentValue = NULL;
		$this->banner_g_pos_x->OldValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_y->CurrentValue = NULL;
		$this->banner_g_pos_y->OldValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_link->CurrentValue = NULL;
		$this->banner_g_link->OldValue = $this->banner_g_link->CurrentValue;
		$this->activo->CurrentValue = NULL;
		$this->activo->OldValue = $this->activo->CurrentValue;
		$this->archivo->Upload->DbValue = NULL;
		$this->archivo->OldValue = $this->archivo->Upload->DbValue;
		$this->precios->CurrentValue = NULL;
		$this->precios->OldValue = $this->precios->CurrentValue;
		$this->mapa_img->Upload->DbValue = NULL;
		$this->mapa_img->OldValue = $this->mapa_img->Upload->DbValue;
		$this->video_final_servidor->CurrentValue = NULL;
		$this->video_final_servidor->OldValue = $this->video_final_servidor->CurrentValue;
		$this->video_final_1->CurrentValue = NULL;
		$this->video_final_1->OldValue = $this->video_final_1->CurrentValue;
		$this->video_titu_1->CurrentValue = NULL;
		$this->video_titu_1->OldValue = $this->video_titu_1->CurrentValue;
		$this->video_final_2->CurrentValue = NULL;
		$this->video_final_2->OldValue = $this->video_final_2->CurrentValue;
		$this->video_titu_2->CurrentValue = NULL;
		$this->video_titu_2->OldValue = $this->video_titu_2->CurrentValue;
		$this->video_final_3->CurrentValue = NULL;
		$this->video_final_3->OldValue = $this->video_final_3->CurrentValue;
		$this->video_titu_3->CurrentValue = NULL;
		$this->video_titu_3->OldValue = $this->video_titu_3->CurrentValue;
		$this->video_final_4->CurrentValue = NULL;
		$this->video_final_4->OldValue = $this->video_final_4->CurrentValue;
		$this->video_titu_4->CurrentValue = NULL;
		$this->video_titu_4->OldValue = $this->video_titu_4->CurrentValue;
		$this->video_final_5->CurrentValue = NULL;
		$this->video_final_5->OldValue = $this->video_final_5->CurrentValue;
		$this->video_titu_5->CurrentValue = NULL;
		$this->video_titu_5->OldValue = $this->video_titu_5->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_id"]);
		if ($this->id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->id->AdvancedSearch->SearchOperator = @$_GET["z_id"];

		// tipo
		$this->tipo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_tipo"]);
		if ($this->tipo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->tipo->AdvancedSearch->SearchOperator = @$_GET["z_tipo"];

		// plataforma
		$this->plataforma->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_plataforma"]);
		if ($this->plataforma->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->plataforma->AdvancedSearch->SearchOperator = @$_GET["z_plataforma"];

		// ext_id
		$this->ext_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_ext_id"]);
		if ($this->ext_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->ext_id->AdvancedSearch->SearchOperator = @$_GET["z_ext_id"];

		// fecha
		$this->fecha->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_fecha"]);
		if ($this->fecha->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->fecha->AdvancedSearch->SearchOperator = @$_GET["z_fecha"];

		// hora_inicio
		$this->hora_inicio->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_hora_inicio"]);
		if ($this->hora_inicio->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->hora_inicio->AdvancedSearch->SearchOperator = @$_GET["z_hora_inicio"];

		// hora_fin
		$this->hora_fin->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_hora_fin"]);
		if ($this->hora_fin->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->hora_fin->AdvancedSearch->SearchOperator = @$_GET["z_hora_fin"];

		// zona
		$this->zona->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_zona"]);
		if ($this->zona->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->zona->AdvancedSearch->SearchOperator = @$_GET["z_zona"];

		// lugar
		$this->lugar->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_lugar"]);
		if ($this->lugar->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->lugar->AdvancedSearch->SearchOperator = @$_GET["z_lugar"];

		// locacion
		$this->locacion->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_locacion"]);
		if ($this->locacion->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->locacion->AdvancedSearch->SearchOperator = @$_GET["z_locacion"];

		// numero
		$this->numero->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_numero"]);
		if ($this->numero->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->numero->AdvancedSearch->SearchOperator = @$_GET["z_numero"];

		// titulo
		$this->titulo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_titulo"]);
		if ($this->titulo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->titulo->AdvancedSearch->SearchOperator = @$_GET["z_titulo"];

		// cantidad
		$this->cantidad->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_cantidad"]);
		if ($this->cantidad->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->cantidad->AdvancedSearch->SearchOperator = @$_GET["z_cantidad"];

		// descripcion
		$this->descripcion->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_descripcion"]);
		if ($this->descripcion->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->descripcion->AdvancedSearch->SearchOperator = @$_GET["z_descripcion"];

		// yacare_visible
		$this->yacare_visible->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_yacare_visible"]);
		if ($this->yacare_visible->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->yacare_visible->AdvancedSearch->SearchOperator = @$_GET["z_yacare_visible"];

		// banner_g
		$this->banner_g->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g"]);
		if ($this->banner_g->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g->AdvancedSearch->SearchOperator = @$_GET["z_banner_g"];

		// banner_g_visible
		$this->banner_g_visible->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g_visible"]);
		if ($this->banner_g_visible->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g_visible->AdvancedSearch->SearchOperator = @$_GET["z_banner_g_visible"];

		// banner_g_z_index
		$this->banner_g_z_index->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g_z_index"]);
		if ($this->banner_g_z_index->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g_z_index->AdvancedSearch->SearchOperator = @$_GET["z_banner_g_z_index"];

		// banner_g_pos_x
		$this->banner_g_pos_x->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g_pos_x"]);
		if ($this->banner_g_pos_x->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g_pos_x->AdvancedSearch->SearchOperator = @$_GET["z_banner_g_pos_x"];

		// banner_g_pos_y
		$this->banner_g_pos_y->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g_pos_y"]);
		if ($this->banner_g_pos_y->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g_pos_y->AdvancedSearch->SearchOperator = @$_GET["z_banner_g_pos_y"];

		// banner_g_link
		$this->banner_g_link->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_banner_g_link"]);
		if ($this->banner_g_link->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->banner_g_link->AdvancedSearch->SearchOperator = @$_GET["z_banner_g_link"];

		// activo
		$this->activo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_activo"]);
		if ($this->activo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->activo->AdvancedSearch->SearchOperator = @$_GET["z_activo"];

		// archivo
		$this->archivo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_archivo"]);
		if ($this->archivo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->archivo->AdvancedSearch->SearchOperator = @$_GET["z_archivo"];

		// precios
		$this->precios->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_precios"]);
		if ($this->precios->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->precios->AdvancedSearch->SearchOperator = @$_GET["z_precios"];

		// mapa_img
		$this->mapa_img->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_mapa_img"]);
		if ($this->mapa_img->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->mapa_img->AdvancedSearch->SearchOperator = @$_GET["z_mapa_img"];

		// video_final_servidor
		$this->video_final_servidor->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_servidor"]);
		if ($this->video_final_servidor->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_servidor->AdvancedSearch->SearchOperator = @$_GET["z_video_final_servidor"];

		// video_final_1
		$this->video_final_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_1"]);
		if ($this->video_final_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_1->AdvancedSearch->SearchOperator = @$_GET["z_video_final_1"];

		// video_titu_1
		$this->video_titu_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_titu_1"]);
		if ($this->video_titu_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_titu_1->AdvancedSearch->SearchOperator = @$_GET["z_video_titu_1"];

		// video_final_2
		$this->video_final_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_2"]);
		if ($this->video_final_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_2->AdvancedSearch->SearchOperator = @$_GET["z_video_final_2"];

		// video_titu_2
		$this->video_titu_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_titu_2"]);
		if ($this->video_titu_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_titu_2->AdvancedSearch->SearchOperator = @$_GET["z_video_titu_2"];

		// video_final_3
		$this->video_final_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_3"]);
		if ($this->video_final_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_3->AdvancedSearch->SearchOperator = @$_GET["z_video_final_3"];

		// video_titu_3
		$this->video_titu_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_titu_3"]);
		if ($this->video_titu_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_titu_3->AdvancedSearch->SearchOperator = @$_GET["z_video_titu_3"];

		// video_final_4
		$this->video_final_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_4"]);
		if ($this->video_final_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_4->AdvancedSearch->SearchOperator = @$_GET["z_video_final_4"];

		// video_titu_4
		$this->video_titu_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_titu_4"]);
		if ($this->video_titu_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_titu_4->AdvancedSearch->SearchOperator = @$_GET["z_video_titu_4"];

		// video_final_5
		$this->video_final_5->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_final_5"]);
		if ($this->video_final_5->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_final_5->AdvancedSearch->SearchOperator = @$_GET["z_video_final_5"];

		// video_titu_5
		$this->video_titu_5->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_titu_5"]);
		if ($this->video_titu_5->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_titu_5->AdvancedSearch->SearchOperator = @$_GET["z_video_titu_5"];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->tipo->FldIsDetailKey) {
			$this->tipo->setFormValue($objForm->GetValue("x_tipo"));
		}
		$this->tipo->setOldValue($objForm->GetValue("o_tipo"));
		if (!$this->plataforma->FldIsDetailKey) {
			$this->plataforma->setFormValue($objForm->GetValue("x_plataforma"));
		}
		$this->plataforma->setOldValue($objForm->GetValue("o_plataforma"));
		if (!$this->ext_id->FldIsDetailKey) {
			$this->ext_id->setFormValue($objForm->GetValue("x_ext_id"));
		}
		$this->ext_id->setOldValue($objForm->GetValue("o_ext_id"));
		if (!$this->fecha->FldIsDetailKey) {
			$this->fecha->setFormValue($objForm->GetValue("x_fecha"));
			$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		}
		$this->fecha->setOldValue($objForm->GetValue("o_fecha"));
		if (!$this->hora_inicio->FldIsDetailKey) {
			$this->hora_inicio->setFormValue($objForm->GetValue("x_hora_inicio"));
			$this->hora_inicio->CurrentValue = ew_UnFormatDateTime($this->hora_inicio->CurrentValue, 0);
		}
		$this->hora_inicio->setOldValue($objForm->GetValue("o_hora_inicio"));
		if (!$this->hora_fin->FldIsDetailKey) {
			$this->hora_fin->setFormValue($objForm->GetValue("x_hora_fin"));
			$this->hora_fin->CurrentValue = ew_UnFormatDateTime($this->hora_fin->CurrentValue, 0);
		}
		$this->hora_fin->setOldValue($objForm->GetValue("o_hora_fin"));
		if (!$this->zona->FldIsDetailKey) {
			$this->zona->setFormValue($objForm->GetValue("x_zona"));
		}
		$this->zona->setOldValue($objForm->GetValue("o_zona"));
		if (!$this->lugar->FldIsDetailKey) {
			$this->lugar->setFormValue($objForm->GetValue("x_lugar"));
		}
		$this->lugar->setOldValue($objForm->GetValue("o_lugar"));
		if (!$this->locacion->FldIsDetailKey) {
			$this->locacion->setFormValue($objForm->GetValue("x_locacion"));
		}
		$this->locacion->setOldValue($objForm->GetValue("o_locacion"));
		if (!$this->numero->FldIsDetailKey) {
			$this->numero->setFormValue($objForm->GetValue("x_numero"));
		}
		$this->numero->setOldValue($objForm->GetValue("o_numero"));
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		$this->titulo->setOldValue($objForm->GetValue("o_titulo"));
		if (!$this->cantidad->FldIsDetailKey) {
			$this->cantidad->setFormValue($objForm->GetValue("x_cantidad"));
		}
		$this->cantidad->setOldValue($objForm->GetValue("o_cantidad"));
		if (!$this->yacare_visible->FldIsDetailKey) {
			$this->yacare_visible->setFormValue($objForm->GetValue("x_yacare_visible"));
		}
		$this->yacare_visible->setOldValue($objForm->GetValue("o_yacare_visible"));
		if (!$this->banner_g_visible->FldIsDetailKey) {
			$this->banner_g_visible->setFormValue($objForm->GetValue("x_banner_g_visible"));
		}
		$this->banner_g_visible->setOldValue($objForm->GetValue("o_banner_g_visible"));
		if (!$this->banner_g_z_index->FldIsDetailKey) {
			$this->banner_g_z_index->setFormValue($objForm->GetValue("x_banner_g_z_index"));
		}
		$this->banner_g_z_index->setOldValue($objForm->GetValue("o_banner_g_z_index"));
		if (!$this->banner_g_pos_x->FldIsDetailKey) {
			$this->banner_g_pos_x->setFormValue($objForm->GetValue("x_banner_g_pos_x"));
		}
		$this->banner_g_pos_x->setOldValue($objForm->GetValue("o_banner_g_pos_x"));
		if (!$this->banner_g_pos_y->FldIsDetailKey) {
			$this->banner_g_pos_y->setFormValue($objForm->GetValue("x_banner_g_pos_y"));
		}
		$this->banner_g_pos_y->setOldValue($objForm->GetValue("o_banner_g_pos_y"));
		if (!$this->banner_g_link->FldIsDetailKey) {
			$this->banner_g_link->setFormValue($objForm->GetValue("x_banner_g_link"));
		}
		$this->banner_g_link->setOldValue($objForm->GetValue("o_banner_g_link"));
		if (!$this->activo->FldIsDetailKey) {
			$this->activo->setFormValue($objForm->GetValue("x_activo"));
		}
		$this->activo->setOldValue($objForm->GetValue("o_activo"));
		if (!$this->precios->FldIsDetailKey) {
			$this->precios->setFormValue($objForm->GetValue("x_precios"));
		}
		$this->precios->setOldValue($objForm->GetValue("o_precios"));
		if (!$this->video_final_servidor->FldIsDetailKey) {
			$this->video_final_servidor->setFormValue($objForm->GetValue("x_video_final_servidor"));
		}
		$this->video_final_servidor->setOldValue($objForm->GetValue("o_video_final_servidor"));
		if (!$this->video_final_1->FldIsDetailKey) {
			$this->video_final_1->setFormValue($objForm->GetValue("x_video_final_1"));
		}
		$this->video_final_1->setOldValue($objForm->GetValue("o_video_final_1"));
		if (!$this->video_titu_1->FldIsDetailKey) {
			$this->video_titu_1->setFormValue($objForm->GetValue("x_video_titu_1"));
		}
		$this->video_titu_1->setOldValue($objForm->GetValue("o_video_titu_1"));
		if (!$this->video_final_2->FldIsDetailKey) {
			$this->video_final_2->setFormValue($objForm->GetValue("x_video_final_2"));
		}
		$this->video_final_2->setOldValue($objForm->GetValue("o_video_final_2"));
		if (!$this->video_titu_2->FldIsDetailKey) {
			$this->video_titu_2->setFormValue($objForm->GetValue("x_video_titu_2"));
		}
		$this->video_titu_2->setOldValue($objForm->GetValue("o_video_titu_2"));
		if (!$this->video_final_3->FldIsDetailKey) {
			$this->video_final_3->setFormValue($objForm->GetValue("x_video_final_3"));
		}
		$this->video_final_3->setOldValue($objForm->GetValue("o_video_final_3"));
		if (!$this->video_titu_3->FldIsDetailKey) {
			$this->video_titu_3->setFormValue($objForm->GetValue("x_video_titu_3"));
		}
		$this->video_titu_3->setOldValue($objForm->GetValue("o_video_titu_3"));
		if (!$this->video_final_4->FldIsDetailKey) {
			$this->video_final_4->setFormValue($objForm->GetValue("x_video_final_4"));
		}
		$this->video_final_4->setOldValue($objForm->GetValue("o_video_final_4"));
		if (!$this->video_titu_4->FldIsDetailKey) {
			$this->video_titu_4->setFormValue($objForm->GetValue("x_video_titu_4"));
		}
		$this->video_titu_4->setOldValue($objForm->GetValue("o_video_titu_4"));
		if (!$this->video_final_5->FldIsDetailKey) {
			$this->video_final_5->setFormValue($objForm->GetValue("x_video_final_5"));
		}
		$this->video_final_5->setOldValue($objForm->GetValue("o_video_final_5"));
		if (!$this->video_titu_5->FldIsDetailKey) {
			$this->video_titu_5->setFormValue($objForm->GetValue("x_video_titu_5"));
		}
		$this->video_titu_5->setOldValue($objForm->GetValue("o_video_titu_5"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->CurrentValue = $this->id->FormValue;
		$this->tipo->CurrentValue = $this->tipo->FormValue;
		$this->plataforma->CurrentValue = $this->plataforma->FormValue;
		$this->ext_id->CurrentValue = $this->ext_id->FormValue;
		$this->fecha->CurrentValue = $this->fecha->FormValue;
		$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		$this->hora_inicio->CurrentValue = $this->hora_inicio->FormValue;
		$this->hora_inicio->CurrentValue = ew_UnFormatDateTime($this->hora_inicio->CurrentValue, 0);
		$this->hora_fin->CurrentValue = $this->hora_fin->FormValue;
		$this->hora_fin->CurrentValue = ew_UnFormatDateTime($this->hora_fin->CurrentValue, 0);
		$this->zona->CurrentValue = $this->zona->FormValue;
		$this->lugar->CurrentValue = $this->lugar->FormValue;
		$this->locacion->CurrentValue = $this->locacion->FormValue;
		$this->numero->CurrentValue = $this->numero->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->cantidad->CurrentValue = $this->cantidad->FormValue;
		$this->yacare_visible->CurrentValue = $this->yacare_visible->FormValue;
		$this->banner_g_visible->CurrentValue = $this->banner_g_visible->FormValue;
		$this->banner_g_z_index->CurrentValue = $this->banner_g_z_index->FormValue;
		$this->banner_g_pos_x->CurrentValue = $this->banner_g_pos_x->FormValue;
		$this->banner_g_pos_y->CurrentValue = $this->banner_g_pos_y->FormValue;
		$this->banner_g_link->CurrentValue = $this->banner_g_link->FormValue;
		$this->activo->CurrentValue = $this->activo->FormValue;
		$this->precios->CurrentValue = $this->precios->FormValue;
		$this->video_final_servidor->CurrentValue = $this->video_final_servidor->FormValue;
		$this->video_final_1->CurrentValue = $this->video_final_1->FormValue;
		$this->video_titu_1->CurrentValue = $this->video_titu_1->FormValue;
		$this->video_final_2->CurrentValue = $this->video_final_2->FormValue;
		$this->video_titu_2->CurrentValue = $this->video_titu_2->FormValue;
		$this->video_final_3->CurrentValue = $this->video_final_3->FormValue;
		$this->video_titu_3->CurrentValue = $this->video_titu_3->FormValue;
		$this->video_final_4->CurrentValue = $this->video_final_4->FormValue;
		$this->video_titu_4->CurrentValue = $this->video_titu_4->FormValue;
		$this->video_final_5->CurrentValue = $this->video_final_5->FormValue;
		$this->video_titu_5->CurrentValue = $this->video_titu_5->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->plataforma->setDbValue($rs->fields('plataforma'));
		$this->ext_id->setDbValue($rs->fields('ext_id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->hora_inicio->setDbValue($rs->fields('hora_inicio'));
		$this->hora_fin->setDbValue($rs->fields('hora_fin'));
		$this->zona->setDbValue($rs->fields('zona'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->locacion->setDbValue($rs->fields('locacion'));
		$this->numero->setDbValue($rs->fields('numero'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->yacare_visible->setDbValue($rs->fields('yacare_visible'));
		$this->banner_g->Upload->DbValue = $rs->fields('banner_g');
		$this->banner_g->CurrentValue = $this->banner_g->Upload->DbValue;
		$this->banner_g_visible->setDbValue($rs->fields('banner_g_visible'));
		$this->banner_g_z_index->setDbValue($rs->fields('banner_g_z_index'));
		$this->banner_g_pos_x->setDbValue($rs->fields('banner_g_pos_x'));
		$this->banner_g_pos_y->setDbValue($rs->fields('banner_g_pos_y'));
		$this->banner_g_link->setDbValue($rs->fields('banner_g_link'));
		$this->banner_ch->Upload->DbValue = $rs->fields('banner_ch');
		$this->banner_ch->CurrentValue = $this->banner_ch->Upload->DbValue;
		$this->activo->setDbValue($rs->fields('activo'));
		$this->archivo->Upload->DbValue = $rs->fields('archivo');
		$this->archivo->CurrentValue = $this->archivo->Upload->DbValue;
		$this->precios->setDbValue($rs->fields('precios'));
		$this->mapa_img->Upload->DbValue = $rs->fields('mapa_img');
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->DbValue;
		$this->video_final_servidor->setDbValue($rs->fields('video_final_servidor'));
		$this->video_final_1->setDbValue($rs->fields('video_final_1'));
		$this->video_titu_1->setDbValue($rs->fields('video_titu_1'));
		$this->video_final_2->setDbValue($rs->fields('video_final_2'));
		$this->video_titu_2->setDbValue($rs->fields('video_titu_2'));
		$this->video_final_3->setDbValue($rs->fields('video_final_3'));
		$this->video_titu_3->setDbValue($rs->fields('video_titu_3'));
		$this->video_final_4->setDbValue($rs->fields('video_final_4'));
		$this->video_titu_4->setDbValue($rs->fields('video_titu_4'));
		$this->video_final_5->setDbValue($rs->fields('video_final_5'));
		$this->video_titu_5->setDbValue($rs->fields('video_titu_5'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->tipo->DbValue = $row['tipo'];
		$this->plataforma->DbValue = $row['plataforma'];
		$this->ext_id->DbValue = $row['ext_id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->hora_inicio->DbValue = $row['hora_inicio'];
		$this->hora_fin->DbValue = $row['hora_fin'];
		$this->zona->DbValue = $row['zona'];
		$this->lugar->DbValue = $row['lugar'];
		$this->locacion->DbValue = $row['locacion'];
		$this->numero->DbValue = $row['numero'];
		$this->titulo->DbValue = $row['titulo'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->yacare_visible->DbValue = $row['yacare_visible'];
		$this->banner_g->Upload->DbValue = $row['banner_g'];
		$this->banner_g_visible->DbValue = $row['banner_g_visible'];
		$this->banner_g_z_index->DbValue = $row['banner_g_z_index'];
		$this->banner_g_pos_x->DbValue = $row['banner_g_pos_x'];
		$this->banner_g_pos_y->DbValue = $row['banner_g_pos_y'];
		$this->banner_g_link->DbValue = $row['banner_g_link'];
		$this->banner_ch->Upload->DbValue = $row['banner_ch'];
		$this->activo->DbValue = $row['activo'];
		$this->archivo->Upload->DbValue = $row['archivo'];
		$this->precios->DbValue = $row['precios'];
		$this->mapa_img->Upload->DbValue = $row['mapa_img'];
		$this->video_final_servidor->DbValue = $row['video_final_servidor'];
		$this->video_final_1->DbValue = $row['video_final_1'];
		$this->video_titu_1->DbValue = $row['video_titu_1'];
		$this->video_final_2->DbValue = $row['video_final_2'];
		$this->video_titu_2->DbValue = $row['video_titu_2'];
		$this->video_final_3->DbValue = $row['video_final_3'];
		$this->video_titu_3->DbValue = $row['video_titu_3'];
		$this->video_final_4->DbValue = $row['video_final_4'];
		$this->video_titu_4->DbValue = $row['video_titu_4'];
		$this->video_final_5->DbValue = $row['video_final_5'];
		$this->video_titu_5->DbValue = $row['video_titu_5'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// tipo
		// plataforma
		// ext_id
		// fecha
		// hora_inicio
		// hora_fin
		// zona
		// lugar
		// locacion
		// numero
		// titulo
		// cantidad
		// descripcion
		// yacare_visible
		// banner_g
		// banner_g_visible
		// banner_g_z_index
		// banner_g_pos_x
		// banner_g_pos_y
		// banner_g_link
		// banner_ch

		$this->banner_ch->CellCssStyle = "white-space: nowrap;";

		// activo
		// archivo
		// precios
		// mapa_img
		// video_final_servidor
		// video_final_1
		// video_titu_1
		// video_final_2
		// video_titu_2
		// video_final_3
		// video_titu_3
		// video_final_4
		// video_titu_4
		// video_final_5
		// video_titu_5

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		if (strval($this->tipo->CurrentValue) <> "") {
			$this->tipo->ViewValue = $this->tipo->OptionCaption($this->tipo->CurrentValue);
		} else {
			$this->tipo->ViewValue = NULL;
		}
		$this->tipo->ViewCustomAttributes = "";

		// plataforma
		if (strval($this->plataforma->CurrentValue) <> "") {
			$this->plataforma->ViewValue = $this->plataforma->OptionCaption($this->plataforma->CurrentValue);
		} else {
			$this->plataforma->ViewValue = NULL;
		}
		$this->plataforma->ViewCustomAttributes = "";

		// ext_id
		$this->ext_id->ViewValue = $this->ext_id->CurrentValue;
		$this->ext_id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// hora_inicio
		$this->hora_inicio->ViewValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->ViewCustomAttributes = "";

		// hora_fin
		$this->hora_fin->ViewValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->ViewCustomAttributes = "";

		// zona
		if (strval($this->zona->CurrentValue) <> "") {
			$this->zona->ViewValue = $this->zona->OptionCaption($this->zona->CurrentValue);
		} else {
			$this->zona->ViewValue = NULL;
		}
		$this->zona->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewCustomAttributes = "";

		// locacion
		$this->locacion->ViewValue = $this->locacion->CurrentValue;
		$this->locacion->ViewCustomAttributes = "";

		// numero
		$this->numero->ViewValue = $this->numero->CurrentValue;
		$this->numero->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// yacare_visible
		if (strval($this->yacare_visible->CurrentValue) <> "") {
			$this->yacare_visible->ViewValue = $this->yacare_visible->OptionCaption($this->yacare_visible->CurrentValue);
		} else {
			$this->yacare_visible->ViewValue = NULL;
		}
		$this->yacare_visible->ViewCustomAttributes = "";

		// banner_g
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->ViewValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->ViewValue = "";
		}
		$this->banner_g->ViewCustomAttributes = "";

		// banner_g_visible
		if (strval($this->banner_g_visible->CurrentValue) <> "") {
			$this->banner_g_visible->ViewValue = $this->banner_g_visible->OptionCaption($this->banner_g_visible->CurrentValue);
		} else {
			$this->banner_g_visible->ViewValue = NULL;
		}
		$this->banner_g_visible->ViewCustomAttributes = "";

		// banner_g_z_index
		$this->banner_g_z_index->ViewValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->ViewCustomAttributes = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->ViewValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->ViewCustomAttributes = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->ViewValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->ViewCustomAttributes = "";

		// banner_g_link
		$this->banner_g_link->ViewValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// archivo
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->ViewValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->ViewValue = "";
		}
		$this->archivo->ViewCustomAttributes = "";

		// precios
		if (strval($this->precios->CurrentValue) <> "") {
			$this->precios->ViewValue = $this->precios->OptionCaption($this->precios->CurrentValue);
		} else {
			$this->precios->ViewValue = NULL;
		}
		$this->precios->ViewCustomAttributes = "";

		// mapa_img
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->ViewValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->ViewValue = "";
		}
		$this->mapa_img->ViewCustomAttributes = "";

		// video_final_servidor
		if (strval($this->video_final_servidor->CurrentValue) <> "") {
			$this->video_final_servidor->ViewValue = $this->video_final_servidor->OptionCaption($this->video_final_servidor->CurrentValue);
		} else {
			$this->video_final_servidor->ViewValue = NULL;
		}
		$this->video_final_servidor->ViewCustomAttributes = "";

		// video_final_1
		$this->video_final_1->ViewValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->ViewCustomAttributes = "";

		// video_titu_1
		$this->video_titu_1->ViewValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->ViewCustomAttributes = "";

		// video_final_2
		$this->video_final_2->ViewValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->ViewCustomAttributes = "";

		// video_titu_2
		$this->video_titu_2->ViewValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->ViewCustomAttributes = "";

		// video_final_3
		$this->video_final_3->ViewValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->ViewCustomAttributes = "";

		// video_titu_3
		$this->video_titu_3->ViewValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->ViewCustomAttributes = "";

		// video_final_4
		$this->video_final_4->ViewValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->ViewCustomAttributes = "";

		// video_titu_4
		$this->video_titu_4->ViewValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->ViewCustomAttributes = "";

		// video_final_5
		$this->video_final_5->ViewValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->ViewCustomAttributes = "";

		// video_titu_5
		$this->video_titu_5->ViewValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";
			$this->plataforma->TooltipValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";
			$this->ext_id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";
			$this->hora_inicio->TooltipValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";
			$this->hora_fin->TooltipValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";
			$this->zona->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";
			$this->locacion->TooltipValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";
			$this->numero->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";
			$this->yacare_visible->TooltipValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;
			$this->banner_g->TooltipValue = "";

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";
			$this->banner_g_visible->TooltipValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";
			$this->banner_g_z_index->TooltipValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";
			$this->banner_g_pos_x->TooltipValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";
			$this->banner_g_pos_y->TooltipValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";
			$this->banner_g_link->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;
			$this->archivo->TooltipValue = "";

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";
			$this->precios->TooltipValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;
			$this->mapa_img->TooltipValue = "";

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";
			$this->video_final_servidor->TooltipValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";
			$this->video_final_1->TooltipValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";
			$this->video_titu_1->TooltipValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";
			$this->video_final_2->TooltipValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";
			$this->video_titu_2->TooltipValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";
			$this->video_final_3->TooltipValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";
			$this->video_titu_3->TooltipValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";
			$this->video_final_4->TooltipValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";
			$this->video_titu_4->TooltipValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";
			$this->video_final_5->TooltipValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
			$this->video_titu_5->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// id
			// tipo

			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = $this->tipo->Options(FALSE);

			// plataforma
			$this->plataforma->EditAttrs["class"] = "form-control";
			$this->plataforma->EditCustomAttributes = "";
			$this->plataforma->EditValue = $this->plataforma->Options(TRUE);

			// ext_id
			$this->ext_id->EditAttrs["class"] = "form-control";
			$this->ext_id->EditCustomAttributes = "";
			$this->ext_id->EditValue = ew_HtmlEncode($this->ext_id->CurrentValue);
			$this->ext_id->PlaceHolder = ew_RemoveHtml($this->ext_id->FldCaption());

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha->CurrentValue, 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// hora_inicio
			$this->hora_inicio->EditAttrs["class"] = "form-control";
			$this->hora_inicio->EditCustomAttributes = "";
			$this->hora_inicio->EditValue = ew_HtmlEncode($this->hora_inicio->CurrentValue);
			$this->hora_inicio->PlaceHolder = ew_RemoveHtml($this->hora_inicio->FldCaption());

			// hora_fin
			$this->hora_fin->EditAttrs["class"] = "form-control";
			$this->hora_fin->EditCustomAttributes = "";
			$this->hora_fin->EditValue = ew_HtmlEncode($this->hora_fin->CurrentValue);
			$this->hora_fin->PlaceHolder = ew_RemoveHtml($this->hora_fin->FldCaption());

			// zona
			$this->zona->EditAttrs["class"] = "form-control";
			$this->zona->EditCustomAttributes = "";
			$this->zona->EditValue = $this->zona->Options(TRUE);

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// locacion
			$this->locacion->EditAttrs["class"] = "form-control";
			$this->locacion->EditCustomAttributes = "";
			$this->locacion->EditValue = ew_HtmlEncode($this->locacion->CurrentValue);
			$this->locacion->PlaceHolder = ew_RemoveHtml($this->locacion->FldCaption());

			// numero
			$this->numero->EditAttrs["class"] = "form-control";
			$this->numero->EditCustomAttributes = "";
			$this->numero->EditValue = ew_HtmlEncode($this->numero->CurrentValue);
			$this->numero->PlaceHolder = ew_RemoveHtml($this->numero->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// yacare_visible
			$this->yacare_visible->EditAttrs["class"] = "form-control";
			$this->yacare_visible->EditCustomAttributes = "";
			$this->yacare_visible->EditValue = $this->yacare_visible->Options(TRUE);

			// banner_g
			$this->banner_g->EditAttrs["class"] = "form-control";
			$this->banner_g->EditCustomAttributes = "";
			if (!ew_Empty($this->banner_g->Upload->DbValue)) {
				$this->banner_g->EditValue = $this->banner_g->Upload->DbValue;
			} else {
				$this->banner_g->EditValue = "";
			}
			if (!ew_Empty($this->banner_g->CurrentValue))
				$this->banner_g->Upload->FileName = $this->banner_g->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->banner_g, $this->RowIndex);

			// banner_g_visible
			$this->banner_g_visible->EditAttrs["class"] = "form-control";
			$this->banner_g_visible->EditCustomAttributes = "";
			$this->banner_g_visible->EditValue = $this->banner_g_visible->Options(TRUE);

			// banner_g_z_index
			$this->banner_g_z_index->EditAttrs["class"] = "form-control";
			$this->banner_g_z_index->EditCustomAttributes = "";
			$this->banner_g_z_index->EditValue = ew_HtmlEncode($this->banner_g_z_index->CurrentValue);
			$this->banner_g_z_index->PlaceHolder = ew_RemoveHtml($this->banner_g_z_index->FldCaption());

			// banner_g_pos_x
			$this->banner_g_pos_x->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_x->EditCustomAttributes = "";
			$this->banner_g_pos_x->EditValue = ew_HtmlEncode($this->banner_g_pos_x->CurrentValue);
			$this->banner_g_pos_x->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_x->FldCaption());

			// banner_g_pos_y
			$this->banner_g_pos_y->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_y->EditCustomAttributes = "";
			$this->banner_g_pos_y->EditValue = ew_HtmlEncode($this->banner_g_pos_y->CurrentValue);
			$this->banner_g_pos_y->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_y->FldCaption());

			// banner_g_link
			$this->banner_g_link->EditAttrs["class"] = "form-control";
			$this->banner_g_link->EditCustomAttributes = "";
			$this->banner_g_link->EditValue = ew_HtmlEncode($this->banner_g_link->CurrentValue);
			$this->banner_g_link->PlaceHolder = ew_RemoveHtml($this->banner_g_link->FldCaption());

			// activo
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(FALSE);

			// archivo
			$this->archivo->EditAttrs["class"] = "form-control";
			$this->archivo->EditCustomAttributes = "";
			if (!ew_Empty($this->archivo->Upload->DbValue)) {
				$this->archivo->EditValue = $this->archivo->Upload->DbValue;
			} else {
				$this->archivo->EditValue = "";
			}
			if (!ew_Empty($this->archivo->CurrentValue))
				$this->archivo->Upload->FileName = $this->archivo->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->archivo, $this->RowIndex);

			// precios
			$this->precios->EditAttrs["class"] = "form-control";
			$this->precios->EditCustomAttributes = "";
			$this->precios->EditValue = $this->precios->Options(TRUE);

			// mapa_img
			$this->mapa_img->EditAttrs["class"] = "form-control";
			$this->mapa_img->EditCustomAttributes = "";
			if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
				$this->mapa_img->EditValue = $this->mapa_img->Upload->DbValue;
			} else {
				$this->mapa_img->EditValue = "";
			}
			if (!ew_Empty($this->mapa_img->CurrentValue))
				$this->mapa_img->Upload->FileName = $this->mapa_img->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->mapa_img, $this->RowIndex);

			// video_final_servidor
			$this->video_final_servidor->EditCustomAttributes = "";
			$this->video_final_servidor->EditValue = $this->video_final_servidor->Options(FALSE);

			// video_final_1
			$this->video_final_1->EditAttrs["class"] = "form-control";
			$this->video_final_1->EditCustomAttributes = "";
			$this->video_final_1->EditValue = ew_HtmlEncode($this->video_final_1->CurrentValue);
			$this->video_final_1->PlaceHolder = ew_RemoveHtml($this->video_final_1->FldCaption());

			// video_titu_1
			$this->video_titu_1->EditAttrs["class"] = "form-control";
			$this->video_titu_1->EditCustomAttributes = "";
			$this->video_titu_1->EditValue = ew_HtmlEncode($this->video_titu_1->CurrentValue);
			$this->video_titu_1->PlaceHolder = ew_RemoveHtml($this->video_titu_1->FldCaption());

			// video_final_2
			$this->video_final_2->EditAttrs["class"] = "form-control";
			$this->video_final_2->EditCustomAttributes = "";
			$this->video_final_2->EditValue = ew_HtmlEncode($this->video_final_2->CurrentValue);
			$this->video_final_2->PlaceHolder = ew_RemoveHtml($this->video_final_2->FldCaption());

			// video_titu_2
			$this->video_titu_2->EditAttrs["class"] = "form-control";
			$this->video_titu_2->EditCustomAttributes = "";
			$this->video_titu_2->EditValue = ew_HtmlEncode($this->video_titu_2->CurrentValue);
			$this->video_titu_2->PlaceHolder = ew_RemoveHtml($this->video_titu_2->FldCaption());

			// video_final_3
			$this->video_final_3->EditAttrs["class"] = "form-control";
			$this->video_final_3->EditCustomAttributes = "";
			$this->video_final_3->EditValue = ew_HtmlEncode($this->video_final_3->CurrentValue);
			$this->video_final_3->PlaceHolder = ew_RemoveHtml($this->video_final_3->FldCaption());

			// video_titu_3
			$this->video_titu_3->EditAttrs["class"] = "form-control";
			$this->video_titu_3->EditCustomAttributes = "";
			$this->video_titu_3->EditValue = ew_HtmlEncode($this->video_titu_3->CurrentValue);
			$this->video_titu_3->PlaceHolder = ew_RemoveHtml($this->video_titu_3->FldCaption());

			// video_final_4
			$this->video_final_4->EditAttrs["class"] = "form-control";
			$this->video_final_4->EditCustomAttributes = "";
			$this->video_final_4->EditValue = ew_HtmlEncode($this->video_final_4->CurrentValue);
			$this->video_final_4->PlaceHolder = ew_RemoveHtml($this->video_final_4->FldCaption());

			// video_titu_4
			$this->video_titu_4->EditAttrs["class"] = "form-control";
			$this->video_titu_4->EditCustomAttributes = "";
			$this->video_titu_4->EditValue = ew_HtmlEncode($this->video_titu_4->CurrentValue);
			$this->video_titu_4->PlaceHolder = ew_RemoveHtml($this->video_titu_4->FldCaption());

			// video_final_5
			$this->video_final_5->EditAttrs["class"] = "form-control";
			$this->video_final_5->EditCustomAttributes = "";
			$this->video_final_5->EditValue = ew_HtmlEncode($this->video_final_5->CurrentValue);
			$this->video_final_5->PlaceHolder = ew_RemoveHtml($this->video_final_5->FldCaption());

			// video_titu_5
			$this->video_titu_5->EditAttrs["class"] = "form-control";
			$this->video_titu_5->EditCustomAttributes = "";
			$this->video_titu_5->EditValue = ew_HtmlEncode($this->video_titu_5->CurrentValue);
			$this->video_titu_5->PlaceHolder = ew_RemoveHtml($this->video_titu_5->FldCaption());

			// Add refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// tipo
			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = $this->tipo->Options(FALSE);

			// plataforma
			$this->plataforma->EditAttrs["class"] = "form-control";
			$this->plataforma->EditCustomAttributes = "";
			$this->plataforma->EditValue = $this->plataforma->Options(TRUE);

			// ext_id
			$this->ext_id->EditAttrs["class"] = "form-control";
			$this->ext_id->EditCustomAttributes = "";
			$this->ext_id->EditValue = ew_HtmlEncode($this->ext_id->CurrentValue);
			$this->ext_id->PlaceHolder = ew_RemoveHtml($this->ext_id->FldCaption());

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha->CurrentValue, 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// hora_inicio
			$this->hora_inicio->EditAttrs["class"] = "form-control";
			$this->hora_inicio->EditCustomAttributes = "";
			$this->hora_inicio->EditValue = ew_HtmlEncode($this->hora_inicio->CurrentValue);
			$this->hora_inicio->PlaceHolder = ew_RemoveHtml($this->hora_inicio->FldCaption());

			// hora_fin
			$this->hora_fin->EditAttrs["class"] = "form-control";
			$this->hora_fin->EditCustomAttributes = "";
			$this->hora_fin->EditValue = ew_HtmlEncode($this->hora_fin->CurrentValue);
			$this->hora_fin->PlaceHolder = ew_RemoveHtml($this->hora_fin->FldCaption());

			// zona
			$this->zona->EditAttrs["class"] = "form-control";
			$this->zona->EditCustomAttributes = "";
			$this->zona->EditValue = $this->zona->Options(TRUE);

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// locacion
			$this->locacion->EditAttrs["class"] = "form-control";
			$this->locacion->EditCustomAttributes = "";
			$this->locacion->EditValue = ew_HtmlEncode($this->locacion->CurrentValue);
			$this->locacion->PlaceHolder = ew_RemoveHtml($this->locacion->FldCaption());

			// numero
			$this->numero->EditAttrs["class"] = "form-control";
			$this->numero->EditCustomAttributes = "";
			$this->numero->EditValue = ew_HtmlEncode($this->numero->CurrentValue);
			$this->numero->PlaceHolder = ew_RemoveHtml($this->numero->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// yacare_visible
			$this->yacare_visible->EditAttrs["class"] = "form-control";
			$this->yacare_visible->EditCustomAttributes = "";
			$this->yacare_visible->EditValue = $this->yacare_visible->Options(TRUE);

			// banner_g
			$this->banner_g->EditAttrs["class"] = "form-control";
			$this->banner_g->EditCustomAttributes = "";
			if (!ew_Empty($this->banner_g->Upload->DbValue)) {
				$this->banner_g->EditValue = $this->banner_g->Upload->DbValue;
			} else {
				$this->banner_g->EditValue = "";
			}
			if (!ew_Empty($this->banner_g->CurrentValue))
				$this->banner_g->Upload->FileName = $this->banner_g->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->banner_g, $this->RowIndex);

			// banner_g_visible
			$this->banner_g_visible->EditAttrs["class"] = "form-control";
			$this->banner_g_visible->EditCustomAttributes = "";
			$this->banner_g_visible->EditValue = $this->banner_g_visible->Options(TRUE);

			// banner_g_z_index
			$this->banner_g_z_index->EditAttrs["class"] = "form-control";
			$this->banner_g_z_index->EditCustomAttributes = "";
			$this->banner_g_z_index->EditValue = ew_HtmlEncode($this->banner_g_z_index->CurrentValue);
			$this->banner_g_z_index->PlaceHolder = ew_RemoveHtml($this->banner_g_z_index->FldCaption());

			// banner_g_pos_x
			$this->banner_g_pos_x->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_x->EditCustomAttributes = "";
			$this->banner_g_pos_x->EditValue = ew_HtmlEncode($this->banner_g_pos_x->CurrentValue);
			$this->banner_g_pos_x->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_x->FldCaption());

			// banner_g_pos_y
			$this->banner_g_pos_y->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_y->EditCustomAttributes = "";
			$this->banner_g_pos_y->EditValue = ew_HtmlEncode($this->banner_g_pos_y->CurrentValue);
			$this->banner_g_pos_y->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_y->FldCaption());

			// banner_g_link
			$this->banner_g_link->EditAttrs["class"] = "form-control";
			$this->banner_g_link->EditCustomAttributes = "";
			$this->banner_g_link->EditValue = ew_HtmlEncode($this->banner_g_link->CurrentValue);
			$this->banner_g_link->PlaceHolder = ew_RemoveHtml($this->banner_g_link->FldCaption());

			// activo
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(FALSE);

			// archivo
			$this->archivo->EditAttrs["class"] = "form-control";
			$this->archivo->EditCustomAttributes = "";
			if (!ew_Empty($this->archivo->Upload->DbValue)) {
				$this->archivo->EditValue = $this->archivo->Upload->DbValue;
			} else {
				$this->archivo->EditValue = "";
			}
			if (!ew_Empty($this->archivo->CurrentValue))
				$this->archivo->Upload->FileName = $this->archivo->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->archivo, $this->RowIndex);

			// precios
			$this->precios->EditAttrs["class"] = "form-control";
			$this->precios->EditCustomAttributes = "";
			$this->precios->EditValue = $this->precios->Options(TRUE);

			// mapa_img
			$this->mapa_img->EditAttrs["class"] = "form-control";
			$this->mapa_img->EditCustomAttributes = "";
			if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
				$this->mapa_img->EditValue = $this->mapa_img->Upload->DbValue;
			} else {
				$this->mapa_img->EditValue = "";
			}
			if (!ew_Empty($this->mapa_img->CurrentValue))
				$this->mapa_img->Upload->FileName = $this->mapa_img->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->mapa_img, $this->RowIndex);

			// video_final_servidor
			$this->video_final_servidor->EditCustomAttributes = "";
			$this->video_final_servidor->EditValue = $this->video_final_servidor->Options(FALSE);

			// video_final_1
			$this->video_final_1->EditAttrs["class"] = "form-control";
			$this->video_final_1->EditCustomAttributes = "";
			$this->video_final_1->EditValue = ew_HtmlEncode($this->video_final_1->CurrentValue);
			$this->video_final_1->PlaceHolder = ew_RemoveHtml($this->video_final_1->FldCaption());

			// video_titu_1
			$this->video_titu_1->EditAttrs["class"] = "form-control";
			$this->video_titu_1->EditCustomAttributes = "";
			$this->video_titu_1->EditValue = ew_HtmlEncode($this->video_titu_1->CurrentValue);
			$this->video_titu_1->PlaceHolder = ew_RemoveHtml($this->video_titu_1->FldCaption());

			// video_final_2
			$this->video_final_2->EditAttrs["class"] = "form-control";
			$this->video_final_2->EditCustomAttributes = "";
			$this->video_final_2->EditValue = ew_HtmlEncode($this->video_final_2->CurrentValue);
			$this->video_final_2->PlaceHolder = ew_RemoveHtml($this->video_final_2->FldCaption());

			// video_titu_2
			$this->video_titu_2->EditAttrs["class"] = "form-control";
			$this->video_titu_2->EditCustomAttributes = "";
			$this->video_titu_2->EditValue = ew_HtmlEncode($this->video_titu_2->CurrentValue);
			$this->video_titu_2->PlaceHolder = ew_RemoveHtml($this->video_titu_2->FldCaption());

			// video_final_3
			$this->video_final_3->EditAttrs["class"] = "form-control";
			$this->video_final_3->EditCustomAttributes = "";
			$this->video_final_3->EditValue = ew_HtmlEncode($this->video_final_3->CurrentValue);
			$this->video_final_3->PlaceHolder = ew_RemoveHtml($this->video_final_3->FldCaption());

			// video_titu_3
			$this->video_titu_3->EditAttrs["class"] = "form-control";
			$this->video_titu_3->EditCustomAttributes = "";
			$this->video_titu_3->EditValue = ew_HtmlEncode($this->video_titu_3->CurrentValue);
			$this->video_titu_3->PlaceHolder = ew_RemoveHtml($this->video_titu_3->FldCaption());

			// video_final_4
			$this->video_final_4->EditAttrs["class"] = "form-control";
			$this->video_final_4->EditCustomAttributes = "";
			$this->video_final_4->EditValue = ew_HtmlEncode($this->video_final_4->CurrentValue);
			$this->video_final_4->PlaceHolder = ew_RemoveHtml($this->video_final_4->FldCaption());

			// video_titu_4
			$this->video_titu_4->EditAttrs["class"] = "form-control";
			$this->video_titu_4->EditCustomAttributes = "";
			$this->video_titu_4->EditValue = ew_HtmlEncode($this->video_titu_4->CurrentValue);
			$this->video_titu_4->PlaceHolder = ew_RemoveHtml($this->video_titu_4->FldCaption());

			// video_final_5
			$this->video_final_5->EditAttrs["class"] = "form-control";
			$this->video_final_5->EditCustomAttributes = "";
			$this->video_final_5->EditValue = ew_HtmlEncode($this->video_final_5->CurrentValue);
			$this->video_final_5->PlaceHolder = ew_RemoveHtml($this->video_final_5->FldCaption());

			// video_titu_5
			$this->video_titu_5->EditAttrs["class"] = "form-control";
			$this->video_titu_5->EditCustomAttributes = "";
			$this->video_titu_5->EditValue = ew_HtmlEncode($this->video_titu_5->CurrentValue);
			$this->video_titu_5->PlaceHolder = ew_RemoveHtml($this->video_titu_5->FldCaption());

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// tipo
			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = $this->tipo->Options(FALSE);

			// plataforma
			$this->plataforma->EditAttrs["class"] = "form-control";
			$this->plataforma->EditCustomAttributes = "";
			$this->plataforma->EditValue = $this->plataforma->Options(TRUE);

			// ext_id
			$this->ext_id->EditAttrs["class"] = "form-control";
			$this->ext_id->EditCustomAttributes = "";
			$this->ext_id->EditValue = ew_HtmlEncode($this->ext_id->AdvancedSearch->SearchValue);
			$this->ext_id->PlaceHolder = ew_RemoveHtml($this->ext_id->FldCaption());

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// hora_inicio
			$this->hora_inicio->EditAttrs["class"] = "form-control";
			$this->hora_inicio->EditCustomAttributes = "";
			$this->hora_inicio->EditValue = ew_HtmlEncode(ew_UnFormatDateTime($this->hora_inicio->AdvancedSearch->SearchValue, 0));
			$this->hora_inicio->PlaceHolder = ew_RemoveHtml($this->hora_inicio->FldCaption());

			// hora_fin
			$this->hora_fin->EditAttrs["class"] = "form-control";
			$this->hora_fin->EditCustomAttributes = "";
			$this->hora_fin->EditValue = ew_HtmlEncode(ew_UnFormatDateTime($this->hora_fin->AdvancedSearch->SearchValue, 0));
			$this->hora_fin->PlaceHolder = ew_RemoveHtml($this->hora_fin->FldCaption());

			// zona
			$this->zona->EditAttrs["class"] = "form-control";
			$this->zona->EditCustomAttributes = "";
			$this->zona->EditValue = $this->zona->Options(TRUE);

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->AdvancedSearch->SearchValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// locacion
			$this->locacion->EditAttrs["class"] = "form-control";
			$this->locacion->EditCustomAttributes = "";
			$this->locacion->EditValue = ew_HtmlEncode($this->locacion->AdvancedSearch->SearchValue);
			$this->locacion->PlaceHolder = ew_RemoveHtml($this->locacion->FldCaption());

			// numero
			$this->numero->EditAttrs["class"] = "form-control";
			$this->numero->EditCustomAttributes = "";
			$this->numero->EditValue = ew_HtmlEncode($this->numero->AdvancedSearch->SearchValue);
			$this->numero->PlaceHolder = ew_RemoveHtml($this->numero->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->AdvancedSearch->SearchValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->AdvancedSearch->SearchValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// yacare_visible
			$this->yacare_visible->EditAttrs["class"] = "form-control";
			$this->yacare_visible->EditCustomAttributes = "";
			$this->yacare_visible->EditValue = $this->yacare_visible->Options(TRUE);

			// banner_g
			$this->banner_g->EditAttrs["class"] = "form-control";
			$this->banner_g->EditCustomAttributes = "";
			$this->banner_g->EditValue = ew_HtmlEncode($this->banner_g->AdvancedSearch->SearchValue);
			$this->banner_g->PlaceHolder = ew_RemoveHtml($this->banner_g->FldCaption());

			// banner_g_visible
			$this->banner_g_visible->EditAttrs["class"] = "form-control";
			$this->banner_g_visible->EditCustomAttributes = "";
			$this->banner_g_visible->EditValue = $this->banner_g_visible->Options(TRUE);

			// banner_g_z_index
			$this->banner_g_z_index->EditAttrs["class"] = "form-control";
			$this->banner_g_z_index->EditCustomAttributes = "";
			$this->banner_g_z_index->EditValue = ew_HtmlEncode($this->banner_g_z_index->AdvancedSearch->SearchValue);
			$this->banner_g_z_index->PlaceHolder = ew_RemoveHtml($this->banner_g_z_index->FldCaption());

			// banner_g_pos_x
			$this->banner_g_pos_x->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_x->EditCustomAttributes = "";
			$this->banner_g_pos_x->EditValue = ew_HtmlEncode($this->banner_g_pos_x->AdvancedSearch->SearchValue);
			$this->banner_g_pos_x->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_x->FldCaption());

			// banner_g_pos_y
			$this->banner_g_pos_y->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_y->EditCustomAttributes = "";
			$this->banner_g_pos_y->EditValue = ew_HtmlEncode($this->banner_g_pos_y->AdvancedSearch->SearchValue);
			$this->banner_g_pos_y->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_y->FldCaption());

			// banner_g_link
			$this->banner_g_link->EditAttrs["class"] = "form-control";
			$this->banner_g_link->EditCustomAttributes = "";
			$this->banner_g_link->EditValue = ew_HtmlEncode($this->banner_g_link->AdvancedSearch->SearchValue);
			$this->banner_g_link->PlaceHolder = ew_RemoveHtml($this->banner_g_link->FldCaption());

			// activo
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(FALSE);

			// archivo
			$this->archivo->EditAttrs["class"] = "form-control";
			$this->archivo->EditCustomAttributes = "";
			$this->archivo->EditValue = ew_HtmlEncode($this->archivo->AdvancedSearch->SearchValue);
			$this->archivo->PlaceHolder = ew_RemoveHtml($this->archivo->FldCaption());

			// precios
			$this->precios->EditAttrs["class"] = "form-control";
			$this->precios->EditCustomAttributes = "";
			$this->precios->EditValue = $this->precios->Options(TRUE);

			// mapa_img
			$this->mapa_img->EditAttrs["class"] = "form-control";
			$this->mapa_img->EditCustomAttributes = "";
			$this->mapa_img->EditValue = ew_HtmlEncode($this->mapa_img->AdvancedSearch->SearchValue);
			$this->mapa_img->PlaceHolder = ew_RemoveHtml($this->mapa_img->FldCaption());

			// video_final_servidor
			$this->video_final_servidor->EditCustomAttributes = "";
			$this->video_final_servidor->EditValue = $this->video_final_servidor->Options(FALSE);

			// video_final_1
			$this->video_final_1->EditAttrs["class"] = "form-control";
			$this->video_final_1->EditCustomAttributes = "";
			$this->video_final_1->EditValue = ew_HtmlEncode($this->video_final_1->AdvancedSearch->SearchValue);
			$this->video_final_1->PlaceHolder = ew_RemoveHtml($this->video_final_1->FldCaption());

			// video_titu_1
			$this->video_titu_1->EditAttrs["class"] = "form-control";
			$this->video_titu_1->EditCustomAttributes = "";
			$this->video_titu_1->EditValue = ew_HtmlEncode($this->video_titu_1->AdvancedSearch->SearchValue);
			$this->video_titu_1->PlaceHolder = ew_RemoveHtml($this->video_titu_1->FldCaption());

			// video_final_2
			$this->video_final_2->EditAttrs["class"] = "form-control";
			$this->video_final_2->EditCustomAttributes = "";
			$this->video_final_2->EditValue = ew_HtmlEncode($this->video_final_2->AdvancedSearch->SearchValue);
			$this->video_final_2->PlaceHolder = ew_RemoveHtml($this->video_final_2->FldCaption());

			// video_titu_2
			$this->video_titu_2->EditAttrs["class"] = "form-control";
			$this->video_titu_2->EditCustomAttributes = "";
			$this->video_titu_2->EditValue = ew_HtmlEncode($this->video_titu_2->AdvancedSearch->SearchValue);
			$this->video_titu_2->PlaceHolder = ew_RemoveHtml($this->video_titu_2->FldCaption());

			// video_final_3
			$this->video_final_3->EditAttrs["class"] = "form-control";
			$this->video_final_3->EditCustomAttributes = "";
			$this->video_final_3->EditValue = ew_HtmlEncode($this->video_final_3->AdvancedSearch->SearchValue);
			$this->video_final_3->PlaceHolder = ew_RemoveHtml($this->video_final_3->FldCaption());

			// video_titu_3
			$this->video_titu_3->EditAttrs["class"] = "form-control";
			$this->video_titu_3->EditCustomAttributes = "";
			$this->video_titu_3->EditValue = ew_HtmlEncode($this->video_titu_3->AdvancedSearch->SearchValue);
			$this->video_titu_3->PlaceHolder = ew_RemoveHtml($this->video_titu_3->FldCaption());

			// video_final_4
			$this->video_final_4->EditAttrs["class"] = "form-control";
			$this->video_final_4->EditCustomAttributes = "";
			$this->video_final_4->EditValue = ew_HtmlEncode($this->video_final_4->AdvancedSearch->SearchValue);
			$this->video_final_4->PlaceHolder = ew_RemoveHtml($this->video_final_4->FldCaption());

			// video_titu_4
			$this->video_titu_4->EditAttrs["class"] = "form-control";
			$this->video_titu_4->EditCustomAttributes = "";
			$this->video_titu_4->EditValue = ew_HtmlEncode($this->video_titu_4->AdvancedSearch->SearchValue);
			$this->video_titu_4->PlaceHolder = ew_RemoveHtml($this->video_titu_4->FldCaption());

			// video_final_5
			$this->video_final_5->EditAttrs["class"] = "form-control";
			$this->video_final_5->EditCustomAttributes = "";
			$this->video_final_5->EditValue = ew_HtmlEncode($this->video_final_5->AdvancedSearch->SearchValue);
			$this->video_final_5->PlaceHolder = ew_RemoveHtml($this->video_final_5->FldCaption());

			// video_titu_5
			$this->video_titu_5->EditAttrs["class"] = "form-control";
			$this->video_titu_5->EditCustomAttributes = "";
			$this->video_titu_5->EditValue = ew_HtmlEncode($this->video_titu_5->AdvancedSearch->SearchValue);
			$this->video_titu_5->PlaceHolder = ew_RemoveHtml($this->video_titu_5->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if ($this->tipo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->tipo->FldCaption(), $this->tipo->ReqErrMsg));
		}
		if (!$this->plataforma->FldIsDetailKey && !is_null($this->plataforma->FormValue) && $this->plataforma->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plataforma->FldCaption(), $this->plataforma->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ext_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->ext_id->FldErrMsg());
		}
		if (!$this->fecha->FldIsDetailKey && !is_null($this->fecha->FormValue) && $this->fecha->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha->FldCaption(), $this->fecha->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha->FldErrMsg());
		}
		if (!ew_CheckTime($this->hora_inicio->FormValue)) {
			ew_AddMessage($gsFormError, $this->hora_inicio->FldErrMsg());
		}
		if (!ew_CheckTime($this->hora_fin->FormValue)) {
			ew_AddMessage($gsFormError, $this->hora_fin->FldErrMsg());
		}
		if (!$this->lugar->FldIsDetailKey && !is_null($this->lugar->FormValue) && $this->lugar->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->lugar->FldCaption(), $this->lugar->ReqErrMsg));
		}
		if (!$this->numero->FldIsDetailKey && !is_null($this->numero->FormValue) && $this->numero->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->numero->FldCaption(), $this->numero->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->numero->FormValue)) {
			ew_AddMessage($gsFormError, $this->numero->FldErrMsg());
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->cantidad->FormValue)) {
			ew_AddMessage($gsFormError, $this->cantidad->FldErrMsg());
		}
		if (!$this->yacare_visible->FldIsDetailKey && !is_null($this->yacare_visible->FormValue) && $this->yacare_visible->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->yacare_visible->FldCaption(), $this->yacare_visible->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->banner_g_z_index->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_z_index->FldErrMsg());
		}
		if (!ew_CheckInteger($this->banner_g_pos_x->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_pos_x->FldErrMsg());
		}
		if (!ew_CheckInteger($this->banner_g_pos_y->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_pos_y->FldErrMsg());
		}
		if ($this->activo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->activo->FldCaption(), $this->activo->ReqErrMsg));
		}
		if ($this->video_final_servidor->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->video_final_servidor->FldCaption(), $this->video_final_servidor->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// tipo
			$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, "", $this->tipo->ReadOnly);

			// plataforma
			$this->plataforma->SetDbValueDef($rsnew, $this->plataforma->CurrentValue, "", $this->plataforma->ReadOnly);

			// ext_id
			$this->ext_id->SetDbValueDef($rsnew, $this->ext_id->CurrentValue, NULL, $this->ext_id->ReadOnly);

			// fecha
			$this->fecha->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha->CurrentValue, 0), ew_CurrentDate(), $this->fecha->ReadOnly);

			// hora_inicio
			$this->hora_inicio->SetDbValueDef($rsnew, $this->hora_inicio->CurrentValue, NULL, $this->hora_inicio->ReadOnly);

			// hora_fin
			$this->hora_fin->SetDbValueDef($rsnew, $this->hora_fin->CurrentValue, NULL, $this->hora_fin->ReadOnly);

			// zona
			$this->zona->SetDbValueDef($rsnew, $this->zona->CurrentValue, NULL, $this->zona->ReadOnly);

			// lugar
			$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, "", $this->lugar->ReadOnly);

			// locacion
			$this->locacion->SetDbValueDef($rsnew, $this->locacion->CurrentValue, NULL, $this->locacion->ReadOnly);

			// numero
			$this->numero->SetDbValueDef($rsnew, $this->numero->CurrentValue, 0, $this->numero->ReadOnly);

			// titulo
			$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", $this->titulo->ReadOnly);

			// cantidad
			$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, $this->cantidad->ReadOnly);

			// yacare_visible
			$this->yacare_visible->SetDbValueDef($rsnew, $this->yacare_visible->CurrentValue, "", $this->yacare_visible->ReadOnly);

			// banner_g
			if ($this->banner_g->Visible && !$this->banner_g->ReadOnly && !$this->banner_g->Upload->KeepFile) {
				$this->banner_g->Upload->DbValue = $rsold['banner_g']; // Get original value
				if ($this->banner_g->Upload->FileName == "") {
					$rsnew['banner_g'] = NULL;
				} else {
					$rsnew['banner_g'] = $this->banner_g->Upload->FileName;
				}
			}

			// banner_g_visible
			$this->banner_g_visible->SetDbValueDef($rsnew, $this->banner_g_visible->CurrentValue, NULL, $this->banner_g_visible->ReadOnly);

			// banner_g_z_index
			$this->banner_g_z_index->SetDbValueDef($rsnew, $this->banner_g_z_index->CurrentValue, NULL, $this->banner_g_z_index->ReadOnly);

			// banner_g_pos_x
			$this->banner_g_pos_x->SetDbValueDef($rsnew, $this->banner_g_pos_x->CurrentValue, NULL, $this->banner_g_pos_x->ReadOnly);

			// banner_g_pos_y
			$this->banner_g_pos_y->SetDbValueDef($rsnew, $this->banner_g_pos_y->CurrentValue, NULL, $this->banner_g_pos_y->ReadOnly);

			// banner_g_link
			$this->banner_g_link->SetDbValueDef($rsnew, $this->banner_g_link->CurrentValue, NULL, $this->banner_g_link->ReadOnly);

			// activo
			$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, "", $this->activo->ReadOnly);

			// archivo
			if ($this->archivo->Visible && !$this->archivo->ReadOnly && !$this->archivo->Upload->KeepFile) {
				$this->archivo->Upload->DbValue = $rsold['archivo']; // Get original value
				if ($this->archivo->Upload->FileName == "") {
					$rsnew['archivo'] = NULL;
				} else {
					$rsnew['archivo'] = $this->archivo->Upload->FileName;
				}
			}

			// precios
			$this->precios->SetDbValueDef($rsnew, $this->precios->CurrentValue, NULL, $this->precios->ReadOnly);

			// mapa_img
			if ($this->mapa_img->Visible && !$this->mapa_img->ReadOnly && !$this->mapa_img->Upload->KeepFile) {
				$this->mapa_img->Upload->DbValue = $rsold['mapa_img']; // Get original value
				if ($this->mapa_img->Upload->FileName == "") {
					$rsnew['mapa_img'] = NULL;
				} else {
					$rsnew['mapa_img'] = $this->mapa_img->Upload->FileName;
				}
			}

			// video_final_servidor
			$this->video_final_servidor->SetDbValueDef($rsnew, $this->video_final_servidor->CurrentValue, NULL, $this->video_final_servidor->ReadOnly);

			// video_final_1
			$this->video_final_1->SetDbValueDef($rsnew, $this->video_final_1->CurrentValue, NULL, $this->video_final_1->ReadOnly);

			// video_titu_1
			$this->video_titu_1->SetDbValueDef($rsnew, $this->video_titu_1->CurrentValue, NULL, $this->video_titu_1->ReadOnly);

			// video_final_2
			$this->video_final_2->SetDbValueDef($rsnew, $this->video_final_2->CurrentValue, NULL, $this->video_final_2->ReadOnly);

			// video_titu_2
			$this->video_titu_2->SetDbValueDef($rsnew, $this->video_titu_2->CurrentValue, NULL, $this->video_titu_2->ReadOnly);

			// video_final_3
			$this->video_final_3->SetDbValueDef($rsnew, $this->video_final_3->CurrentValue, NULL, $this->video_final_3->ReadOnly);

			// video_titu_3
			$this->video_titu_3->SetDbValueDef($rsnew, $this->video_titu_3->CurrentValue, NULL, $this->video_titu_3->ReadOnly);

			// video_final_4
			$this->video_final_4->SetDbValueDef($rsnew, $this->video_final_4->CurrentValue, NULL, $this->video_final_4->ReadOnly);

			// video_titu_4
			$this->video_titu_4->SetDbValueDef($rsnew, $this->video_titu_4->CurrentValue, NULL, $this->video_titu_4->ReadOnly);

			// video_final_5
			$this->video_final_5->SetDbValueDef($rsnew, $this->video_final_5->CurrentValue, NULL, $this->video_final_5->ReadOnly);

			// video_titu_5
			$this->video_titu_5->SetDbValueDef($rsnew, $this->video_titu_5->CurrentValue, NULL, $this->video_titu_5->ReadOnly);
			if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
				if (!ew_Empty($this->banner_g->Upload->Value)) {
					$rsnew['banner_g'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->banner_g->UploadPath), $rsnew['banner_g']); // Get new file name
				}
			}
			if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
				if (!ew_Empty($this->archivo->Upload->Value)) {
					$rsnew['archivo'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->archivo->UploadPath), $rsnew['archivo']); // Get new file name
				}
			}
			if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
				if (!ew_Empty($this->mapa_img->Upload->Value)) {
					$rsnew['mapa_img'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->mapa_img->UploadPath), $rsnew['mapa_img']); // Get new file name
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
						if (!ew_Empty($this->banner_g->Upload->Value)) {
							if (!$this->banner_g->Upload->SaveToFile($this->banner_g->UploadPath, $rsnew['banner_g'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
						if (!ew_Empty($this->archivo->Upload->Value)) {
							if (!$this->archivo->Upload->SaveToFile($this->archivo->UploadPath, $rsnew['archivo'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
						if (!ew_Empty($this->mapa_img->Upload->Value)) {
							if (!$this->mapa_img->Upload->SaveToFile($this->mapa_img->UploadPath, $rsnew['mapa_img'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// banner_g
		ew_CleanUploadTempPath($this->banner_g, $this->banner_g->Upload->Index);

		// archivo
		ew_CleanUploadTempPath($this->archivo, $this->archivo->Upload->Index);

		// mapa_img
		ew_CleanUploadTempPath($this->mapa_img, $this->mapa_img->Upload->Index);
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// tipo
		$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, "", strval($this->tipo->CurrentValue) == "");

		// plataforma
		$this->plataforma->SetDbValueDef($rsnew, $this->plataforma->CurrentValue, "", strval($this->plataforma->CurrentValue) == "");

		// ext_id
		$this->ext_id->SetDbValueDef($rsnew, $this->ext_id->CurrentValue, NULL, FALSE);

		// fecha
		$this->fecha->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha->CurrentValue, 0), ew_CurrentDate(), FALSE);

		// hora_inicio
		$this->hora_inicio->SetDbValueDef($rsnew, $this->hora_inicio->CurrentValue, NULL, FALSE);

		// hora_fin
		$this->hora_fin->SetDbValueDef($rsnew, $this->hora_fin->CurrentValue, NULL, FALSE);

		// zona
		$this->zona->SetDbValueDef($rsnew, $this->zona->CurrentValue, NULL, FALSE);

		// lugar
		$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, "", FALSE);

		// locacion
		$this->locacion->SetDbValueDef($rsnew, $this->locacion->CurrentValue, NULL, FALSE);

		// numero
		$this->numero->SetDbValueDef($rsnew, $this->numero->CurrentValue, 0, FALSE);

		// titulo
		$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", FALSE);

		// cantidad
		$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, FALSE);

		// yacare_visible
		$this->yacare_visible->SetDbValueDef($rsnew, $this->yacare_visible->CurrentValue, "", strval($this->yacare_visible->CurrentValue) == "");

		// banner_g
		if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
			$this->banner_g->Upload->DbValue = ""; // No need to delete old file
			if ($this->banner_g->Upload->FileName == "") {
				$rsnew['banner_g'] = NULL;
			} else {
				$rsnew['banner_g'] = $this->banner_g->Upload->FileName;
			}
		}

		// banner_g_visible
		$this->banner_g_visible->SetDbValueDef($rsnew, $this->banner_g_visible->CurrentValue, NULL, strval($this->banner_g_visible->CurrentValue) == "");

		// banner_g_z_index
		$this->banner_g_z_index->SetDbValueDef($rsnew, $this->banner_g_z_index->CurrentValue, NULL, FALSE);

		// banner_g_pos_x
		$this->banner_g_pos_x->SetDbValueDef($rsnew, $this->banner_g_pos_x->CurrentValue, NULL, FALSE);

		// banner_g_pos_y
		$this->banner_g_pos_y->SetDbValueDef($rsnew, $this->banner_g_pos_y->CurrentValue, NULL, FALSE);

		// banner_g_link
		$this->banner_g_link->SetDbValueDef($rsnew, $this->banner_g_link->CurrentValue, NULL, FALSE);

		// activo
		$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, "", FALSE);

		// archivo
		if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
			$this->archivo->Upload->DbValue = ""; // No need to delete old file
			if ($this->archivo->Upload->FileName == "") {
				$rsnew['archivo'] = NULL;
			} else {
				$rsnew['archivo'] = $this->archivo->Upload->FileName;
			}
		}

		// precios
		$this->precios->SetDbValueDef($rsnew, $this->precios->CurrentValue, NULL, FALSE);

		// mapa_img
		if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
			$this->mapa_img->Upload->DbValue = ""; // No need to delete old file
			if ($this->mapa_img->Upload->FileName == "") {
				$rsnew['mapa_img'] = NULL;
			} else {
				$rsnew['mapa_img'] = $this->mapa_img->Upload->FileName;
			}
		}

		// video_final_servidor
		$this->video_final_servidor->SetDbValueDef($rsnew, $this->video_final_servidor->CurrentValue, NULL, strval($this->video_final_servidor->CurrentValue) == "");

		// video_final_1
		$this->video_final_1->SetDbValueDef($rsnew, $this->video_final_1->CurrentValue, NULL, FALSE);

		// video_titu_1
		$this->video_titu_1->SetDbValueDef($rsnew, $this->video_titu_1->CurrentValue, NULL, FALSE);

		// video_final_2
		$this->video_final_2->SetDbValueDef($rsnew, $this->video_final_2->CurrentValue, NULL, FALSE);

		// video_titu_2
		$this->video_titu_2->SetDbValueDef($rsnew, $this->video_titu_2->CurrentValue, NULL, FALSE);

		// video_final_3
		$this->video_final_3->SetDbValueDef($rsnew, $this->video_final_3->CurrentValue, NULL, FALSE);

		// video_titu_3
		$this->video_titu_3->SetDbValueDef($rsnew, $this->video_titu_3->CurrentValue, NULL, FALSE);

		// video_final_4
		$this->video_final_4->SetDbValueDef($rsnew, $this->video_final_4->CurrentValue, NULL, FALSE);

		// video_titu_4
		$this->video_titu_4->SetDbValueDef($rsnew, $this->video_titu_4->CurrentValue, NULL, FALSE);

		// video_final_5
		$this->video_final_5->SetDbValueDef($rsnew, $this->video_final_5->CurrentValue, NULL, FALSE);

		// video_titu_5
		$this->video_titu_5->SetDbValueDef($rsnew, $this->video_titu_5->CurrentValue, NULL, FALSE);
		if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
			if (!ew_Empty($this->banner_g->Upload->Value)) {
				$rsnew['banner_g'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->banner_g->UploadPath), $rsnew['banner_g']); // Get new file name
			}
		}
		if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
			if (!ew_Empty($this->archivo->Upload->Value)) {
				$rsnew['archivo'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->archivo->UploadPath), $rsnew['archivo']); // Get new file name
			}
		}
		if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
			if (!ew_Empty($this->mapa_img->Upload->Value)) {
				$rsnew['mapa_img'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->mapa_img->UploadPath), $rsnew['mapa_img']); // Get new file name
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->id->setDbValue($conn->Insert_ID());
				$rsnew['id'] = $this->id->DbValue;
				if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
					if (!ew_Empty($this->banner_g->Upload->Value)) {
						if (!$this->banner_g->Upload->SaveToFile($this->banner_g->UploadPath, $rsnew['banner_g'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
					if (!ew_Empty($this->archivo->Upload->Value)) {
						if (!$this->archivo->Upload->SaveToFile($this->archivo->UploadPath, $rsnew['archivo'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
					if (!ew_Empty($this->mapa_img->Upload->Value)) {
						if (!$this->mapa_img->Upload->SaveToFile($this->mapa_img->UploadPath, $rsnew['mapa_img'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// banner_g
		ew_CleanUploadTempPath($this->banner_g, $this->banner_g->Upload->Index);

		// archivo
		ew_CleanUploadTempPath($this->archivo, $this->archivo->Upload->Index);

		// mapa_img
		ew_CleanUploadTempPath($this->mapa_img, $this->mapa_img->Upload->Index);
		return $AddRow;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->tipo->AdvancedSearch->Load();
		$this->plataforma->AdvancedSearch->Load();
		$this->ext_id->AdvancedSearch->Load();
		$this->fecha->AdvancedSearch->Load();
		$this->hora_inicio->AdvancedSearch->Load();
		$this->hora_fin->AdvancedSearch->Load();
		$this->zona->AdvancedSearch->Load();
		$this->lugar->AdvancedSearch->Load();
		$this->locacion->AdvancedSearch->Load();
		$this->numero->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->cantidad->AdvancedSearch->Load();
		$this->descripcion->AdvancedSearch->Load();
		$this->yacare_visible->AdvancedSearch->Load();
		$this->banner_g->AdvancedSearch->Load();
		$this->banner_g_visible->AdvancedSearch->Load();
		$this->banner_g_z_index->AdvancedSearch->Load();
		$this->banner_g_pos_x->AdvancedSearch->Load();
		$this->banner_g_pos_y->AdvancedSearch->Load();
		$this->banner_g_link->AdvancedSearch->Load();
		$this->activo->AdvancedSearch->Load();
		$this->archivo->AdvancedSearch->Load();
		$this->precios->AdvancedSearch->Load();
		$this->mapa_img->AdvancedSearch->Load();
		$this->video_final_servidor->AdvancedSearch->Load();
		$this->video_final_1->AdvancedSearch->Load();
		$this->video_titu_1->AdvancedSearch->Load();
		$this->video_final_2->AdvancedSearch->Load();
		$this->video_titu_2->AdvancedSearch->Load();
		$this->video_final_3->AdvancedSearch->Load();
		$this->video_titu_3->AdvancedSearch->Load();
		$this->video_final_4->AdvancedSearch->Load();
		$this->video_titu_4->AdvancedSearch->Load();
		$this->video_final_5->AdvancedSearch->Load();
		$this->video_titu_5->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_remates\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_remates',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fremateslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($remates_list)) $remates_list = new cremates_list();

// Page init
$remates_list->Page_Init();

// Page main
$remates_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$remates_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fremateslist = new ew_Form("fremateslist", "list");
fremateslist.FormKeyCountName = '<?php echo $remates_list->FormKeyCountName ?>';

// Validate form
fremateslist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_tipo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->tipo->FldCaption(), $remates->tipo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plataforma");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->plataforma->FldCaption(), $remates->plataforma->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ext_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->ext_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->fecha->FldCaption(), $remates->fecha->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->fecha->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_hora_inicio");
			if (elm && !ew_CheckTime(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->hora_inicio->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_hora_fin");
			if (elm && !ew_CheckTime(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->hora_fin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_lugar");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->lugar->FldCaption(), $remates->lugar->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_numero");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->numero->FldCaption(), $remates->numero->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_numero");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->numero->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->titulo->FldCaption(), $remates->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_cantidad");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->cantidad->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_yacare_visible");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->yacare_visible->FldCaption(), $remates->yacare_visible->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_z_index");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_z_index->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_pos_x");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_pos_x->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_pos_y");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_pos_y->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_activo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->activo->FldCaption(), $remates->activo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_video_final_servidor");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->video_final_servidor->FldCaption(), $remates->video_final_servidor->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fremateslist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "tipo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plataforma", false)) return false;
	if (ew_ValueChanged(fobj, infix, "ext_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "fecha", false)) return false;
	if (ew_ValueChanged(fobj, infix, "hora_inicio", false)) return false;
	if (ew_ValueChanged(fobj, infix, "hora_fin", false)) return false;
	if (ew_ValueChanged(fobj, infix, "zona", false)) return false;
	if (ew_ValueChanged(fobj, infix, "lugar", false)) return false;
	if (ew_ValueChanged(fobj, infix, "locacion", false)) return false;
	if (ew_ValueChanged(fobj, infix, "numero", false)) return false;
	if (ew_ValueChanged(fobj, infix, "titulo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "cantidad", false)) return false;
	if (ew_ValueChanged(fobj, infix, "yacare_visible", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g_visible", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g_z_index", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g_pos_x", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g_pos_y", false)) return false;
	if (ew_ValueChanged(fobj, infix, "banner_g_link", false)) return false;
	if (ew_ValueChanged(fobj, infix, "activo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "archivo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "precios", false)) return false;
	if (ew_ValueChanged(fobj, infix, "mapa_img", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_servidor", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_1", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_titu_1", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_2", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_titu_2", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_3", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_titu_3", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_4", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_titu_4", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_final_5", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video_titu_5", false)) return false;
	return true;
}

// Form_CustomValidate event
fremateslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fremateslist.ValidateRequired = true;
<?php } else { ?>
fremateslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fremateslist.Lists["x_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_tipo"].Options = <?php echo json_encode($remates->tipo->Options()) ?>;
fremateslist.Lists["x_plataforma"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_plataforma"].Options = <?php echo json_encode($remates->plataforma->Options()) ?>;
fremateslist.Lists["x_zona"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_zona"].Options = <?php echo json_encode($remates->zona->Options()) ?>;
fremateslist.Lists["x_yacare_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_yacare_visible"].Options = <?php echo json_encode($remates->yacare_visible->Options()) ?>;
fremateslist.Lists["x_banner_g_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_banner_g_visible"].Options = <?php echo json_encode($remates->banner_g_visible->Options()) ?>;
fremateslist.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_activo"].Options = <?php echo json_encode($remates->activo->Options()) ?>;
fremateslist.Lists["x_precios"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_precios"].Options = <?php echo json_encode($remates->precios->Options()) ?>;
fremateslist.Lists["x_video_final_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslist.Lists["x_video_final_servidor"].Options = <?php echo json_encode($remates->video_final_servidor->Options()) ?>;

// Form object for search
var CurrentSearchForm = fremateslistsrch = new ew_Form("fremateslistsrch");

// Validate function for search
fremateslistsrch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
fremateslistsrch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fremateslistsrch.ValidateRequired = true; // Use JavaScript validation
<?php } else { ?>
fremateslistsrch.ValidateRequired = false; // No JavaScript validation
<?php } ?>

// Dynamic selection lists
fremateslistsrch.Lists["x_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_tipo"].Options = <?php echo json_encode($remates->tipo->Options()) ?>;
fremateslistsrch.Lists["x_plataforma"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_plataforma"].Options = <?php echo json_encode($remates->plataforma->Options()) ?>;
fremateslistsrch.Lists["x_zona"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_zona"].Options = <?php echo json_encode($remates->zona->Options()) ?>;
fremateslistsrch.Lists["x_yacare_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_yacare_visible"].Options = <?php echo json_encode($remates->yacare_visible->Options()) ?>;
fremateslistsrch.Lists["x_banner_g_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_banner_g_visible"].Options = <?php echo json_encode($remates->banner_g_visible->Options()) ?>;
fremateslistsrch.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_activo"].Options = <?php echo json_encode($remates->activo->Options()) ?>;
fremateslistsrch.Lists["x_precios"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_precios"].Options = <?php echo json_encode($remates->precios->Options()) ?>;
fremateslistsrch.Lists["x_video_final_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fremateslistsrch.Lists["x_video_final_servidor"].Options = <?php echo json_encode($remates->video_final_servidor->Options()) ?>;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($remates->Export == "") { ?>
<div class="ewToolbar">
<?php if ($remates->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($remates_list->TotalRecs > 0 && $remates_list->ExportOptions->Visible()) { ?>
<?php $remates_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($remates_list->SearchOptions->Visible()) { ?>
<?php $remates_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($remates_list->FilterOptions->Visible()) { ?>
<?php $remates_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($remates->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
if ($remates->CurrentAction == "gridadd") {
	$remates->CurrentFilter = "0=1";
	$remates_list->StartRec = 1;
	$remates_list->DisplayRecs = $remates->GridAddRowCount;
	$remates_list->TotalRecs = $remates_list->DisplayRecs;
	$remates_list->StopRec = $remates_list->DisplayRecs;
} else {
	$bSelectLimit = $remates_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($remates_list->TotalRecs <= 0)
			$remates_list->TotalRecs = $remates->SelectRecordCount();
	} else {
		if (!$remates_list->Recordset && ($remates_list->Recordset = $remates_list->LoadRecordset()))
			$remates_list->TotalRecs = $remates_list->Recordset->RecordCount();
	}
	$remates_list->StartRec = 1;
	if ($remates_list->DisplayRecs <= 0 || ($remates->Export <> "" && $remates->ExportAll)) // Display all records
		$remates_list->DisplayRecs = $remates_list->TotalRecs;
	if (!($remates->Export <> "" && $remates->ExportAll))
		$remates_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$remates_list->Recordset = $remates_list->LoadRecordset($remates_list->StartRec-1, $remates_list->DisplayRecs);

	// Set no record found message
	if ($remates->CurrentAction == "" && $remates_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$remates_list->setWarningMessage(ew_DeniedMsg());
		if ($remates_list->SearchWhere == "0=101")
			$remates_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$remates_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$remates_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($remates->Export == "" && $remates->CurrentAction == "") { ?>
<form name="fremateslistsrch" id="fremateslistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($remates_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fremateslistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="remates">
	<div class="ewBasicSearch">
<?php
if ($gsSearchError == "")
	$remates_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$remates->RowType = EW_ROWTYPE_SEARCH;

// Render row
$remates->ResetAttrs();
$remates_list->RenderRow();
?>
<div id="xsr_1" class="ewRow">
<?php if ($remates->tipo->Visible) { // tipo ?>
	<div id="xsc_tipo" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $remates->tipo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_tipo" id="z_tipo" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x_tipo" id="x_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x_tipo") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ewRow">
<?php if ($remates->plataforma->Visible) { // plataforma ?>
	<div id="xsc_plataforma" class="ewCell form-group">
		<label for="x_plataforma" class="ewSearchCaption ewLabel"><?php echo $remates->plataforma->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_plataforma" id="z_plataforma" value="="></span>
		<span class="ewSearchField">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x_plataforma" name="x_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x_plataforma") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_3" class="ewRow">
<?php if ($remates->zona->Visible) { // zona ?>
	<div id="xsc_zona" class="ewCell form-group">
		<label for="x_zona" class="ewSearchCaption ewLabel"><?php echo $remates->zona->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_zona" id="z_zona" value="="></span>
		<span class="ewSearchField">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x_zona" name="x_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x_zona") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_4" class="ewRow">
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
	<div id="xsc_yacare_visible" class="ewCell form-group">
		<label for="x_yacare_visible" class="ewSearchCaption ewLabel"><?php echo $remates->yacare_visible->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_yacare_visible" id="z_yacare_visible" value="="></span>
		<span class="ewSearchField">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x_yacare_visible" name="x_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x_yacare_visible") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_5" class="ewRow">
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
	<div id="xsc_banner_g_visible" class="ewCell form-group">
		<label for="x_banner_g_visible" class="ewSearchCaption ewLabel"><?php echo $remates->banner_g_visible->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_banner_g_visible" id="z_banner_g_visible" value="="></span>
		<span class="ewSearchField">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x_banner_g_visible" name="x_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x_banner_g_visible") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_6" class="ewRow">
<?php if ($remates->activo->Visible) { // activo ?>
	<div id="xsc_activo" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $remates->activo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_activo" id="z_activo" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x_activo" id="x_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x_activo") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_7" class="ewRow">
<?php if ($remates->precios->Visible) { // precios ?>
	<div id="xsc_precios" class="ewCell form-group">
		<label for="x_precios" class="ewSearchCaption ewLabel"><?php echo $remates->precios->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_precios" id="z_precios" value="="></span>
		<span class="ewSearchField">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x_precios" name="x_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x_precios") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_8" class="ewRow">
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
	<div id="xsc_video_final_servidor" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $remates->video_final_servidor->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_video_final_servidor" id="z_video_final_servidor" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x_video_final_servidor" id="x_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x_video_final_servidor") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_9" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($remates_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($remates_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $remates_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($remates_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($remates_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($remates_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($remates_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $remates_list->ShowPageHeader(); ?>
<?php
$remates_list->ShowMessage();
?>
<?php if ($remates_list->TotalRecs > 0 || $remates->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid remates">
<?php if ($remates->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($remates->CurrentAction <> "gridadd" && $remates->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($remates_list->Pager)) $remates_list->Pager = new cPrevNextPager($remates_list->StartRec, $remates_list->DisplayRecs, $remates_list->TotalRecs) ?>
<?php if ($remates_list->Pager->RecordCount > 0 && $remates_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($remates_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($remates_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $remates_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($remates_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($remates_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $remates_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $remates_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $remates_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $remates_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($remates_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fremateslist" id="fremateslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($remates_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $remates_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="remates">
<div id="gmp_remates" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($remates_list->TotalRecs > 0 || $remates->CurrentAction == "add" || $remates->CurrentAction == "copy" || $remates->CurrentAction == "gridedit") { ?>
<table id="tbl_remateslist" class="table ewTable">
<?php echo $remates->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$remates_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$remates_list->RenderListOptions();

// Render list options (header, left)
$remates_list->ListOptions->Render("header", "left");
?>
<?php if ($remates->id->Visible) { // id ?>
	<?php if ($remates->SortUrl($remates->id) == "") { ?>
		<th data-name="id"><div id="elh_remates_id" class="remates_id"><div class="ewTableHeaderCaption"><?php echo $remates->id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->id) ?>',1);"><div id="elh_remates_id" class="remates_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->tipo->Visible) { // tipo ?>
	<?php if ($remates->SortUrl($remates->tipo) == "") { ?>
		<th data-name="tipo"><div id="elh_remates_tipo" class="remates_tipo"><div class="ewTableHeaderCaption"><?php echo $remates->tipo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="tipo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->tipo) ?>',1);"><div id="elh_remates_tipo" class="remates_tipo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->tipo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->tipo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->tipo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->plataforma->Visible) { // plataforma ?>
	<?php if ($remates->SortUrl($remates->plataforma) == "") { ?>
		<th data-name="plataforma"><div id="elh_remates_plataforma" class="remates_plataforma"><div class="ewTableHeaderCaption"><?php echo $remates->plataforma->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plataforma"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->plataforma) ?>',1);"><div id="elh_remates_plataforma" class="remates_plataforma">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->plataforma->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->plataforma->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->plataforma->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->ext_id->Visible) { // ext_id ?>
	<?php if ($remates->SortUrl($remates->ext_id) == "") { ?>
		<th data-name="ext_id"><div id="elh_remates_ext_id" class="remates_ext_id"><div class="ewTableHeaderCaption"><?php echo $remates->ext_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="ext_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->ext_id) ?>',1);"><div id="elh_remates_ext_id" class="remates_ext_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->ext_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->ext_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->ext_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->fecha->Visible) { // fecha ?>
	<?php if ($remates->SortUrl($remates->fecha) == "") { ?>
		<th data-name="fecha"><div id="elh_remates_fecha" class="remates_fecha"><div class="ewTableHeaderCaption"><?php echo $remates->fecha->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="fecha"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->fecha) ?>',1);"><div id="elh_remates_fecha" class="remates_fecha">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->fecha->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->fecha->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->fecha->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
	<?php if ($remates->SortUrl($remates->hora_inicio) == "") { ?>
		<th data-name="hora_inicio"><div id="elh_remates_hora_inicio" class="remates_hora_inicio"><div class="ewTableHeaderCaption"><?php echo $remates->hora_inicio->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="hora_inicio"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->hora_inicio) ?>',1);"><div id="elh_remates_hora_inicio" class="remates_hora_inicio">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->hora_inicio->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->hora_inicio->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->hora_inicio->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
	<?php if ($remates->SortUrl($remates->hora_fin) == "") { ?>
		<th data-name="hora_fin"><div id="elh_remates_hora_fin" class="remates_hora_fin"><div class="ewTableHeaderCaption"><?php echo $remates->hora_fin->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="hora_fin"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->hora_fin) ?>',1);"><div id="elh_remates_hora_fin" class="remates_hora_fin">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->hora_fin->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->hora_fin->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->hora_fin->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->zona->Visible) { // zona ?>
	<?php if ($remates->SortUrl($remates->zona) == "") { ?>
		<th data-name="zona"><div id="elh_remates_zona" class="remates_zona"><div class="ewTableHeaderCaption"><?php echo $remates->zona->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="zona"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->zona) ?>',1);"><div id="elh_remates_zona" class="remates_zona">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->zona->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->zona->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->zona->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->lugar->Visible) { // lugar ?>
	<?php if ($remates->SortUrl($remates->lugar) == "") { ?>
		<th data-name="lugar"><div id="elh_remates_lugar" class="remates_lugar"><div class="ewTableHeaderCaption"><?php echo $remates->lugar->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="lugar"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->lugar) ?>',1);"><div id="elh_remates_lugar" class="remates_lugar">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->lugar->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->lugar->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->lugar->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->locacion->Visible) { // locacion ?>
	<?php if ($remates->SortUrl($remates->locacion) == "") { ?>
		<th data-name="locacion"><div id="elh_remates_locacion" class="remates_locacion"><div class="ewTableHeaderCaption"><?php echo $remates->locacion->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="locacion"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->locacion) ?>',1);"><div id="elh_remates_locacion" class="remates_locacion">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->locacion->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->locacion->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->locacion->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->numero->Visible) { // numero ?>
	<?php if ($remates->SortUrl($remates->numero) == "") { ?>
		<th data-name="numero"><div id="elh_remates_numero" class="remates_numero"><div class="ewTableHeaderCaption"><?php echo $remates->numero->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="numero"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->numero) ?>',1);"><div id="elh_remates_numero" class="remates_numero">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->numero->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->numero->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->numero->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->titulo->Visible) { // titulo ?>
	<?php if ($remates->SortUrl($remates->titulo) == "") { ?>
		<th data-name="titulo"><div id="elh_remates_titulo" class="remates_titulo"><div class="ewTableHeaderCaption"><?php echo $remates->titulo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="titulo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->titulo) ?>',1);"><div id="elh_remates_titulo" class="remates_titulo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->titulo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->titulo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->titulo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->cantidad->Visible) { // cantidad ?>
	<?php if ($remates->SortUrl($remates->cantidad) == "") { ?>
		<th data-name="cantidad"><div id="elh_remates_cantidad" class="remates_cantidad"><div class="ewTableHeaderCaption"><?php echo $remates->cantidad->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="cantidad"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->cantidad) ?>',1);"><div id="elh_remates_cantidad" class="remates_cantidad">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->cantidad->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->cantidad->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->cantidad->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
	<?php if ($remates->SortUrl($remates->yacare_visible) == "") { ?>
		<th data-name="yacare_visible"><div id="elh_remates_yacare_visible" class="remates_yacare_visible"><div class="ewTableHeaderCaption"><?php echo $remates->yacare_visible->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="yacare_visible"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->yacare_visible) ?>',1);"><div id="elh_remates_yacare_visible" class="remates_yacare_visible">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->yacare_visible->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->yacare_visible->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->yacare_visible->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g->Visible) { // banner_g ?>
	<?php if ($remates->SortUrl($remates->banner_g) == "") { ?>
		<th data-name="banner_g"><div id="elh_remates_banner_g" class="remates_banner_g"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g) ?>',1);"><div id="elh_remates_banner_g" class="remates_banner_g">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
	<?php if ($remates->SortUrl($remates->banner_g_visible) == "") { ?>
		<th data-name="banner_g_visible"><div id="elh_remates_banner_g_visible" class="remates_banner_g_visible"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g_visible->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g_visible"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g_visible) ?>',1);"><div id="elh_remates_banner_g_visible" class="remates_banner_g_visible">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g_visible->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g_visible->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g_visible->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
	<?php if ($remates->SortUrl($remates->banner_g_z_index) == "") { ?>
		<th data-name="banner_g_z_index"><div id="elh_remates_banner_g_z_index" class="remates_banner_g_z_index"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g_z_index->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g_z_index"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g_z_index) ?>',1);"><div id="elh_remates_banner_g_z_index" class="remates_banner_g_z_index">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g_z_index->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g_z_index->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g_z_index->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
	<?php if ($remates->SortUrl($remates->banner_g_pos_x) == "") { ?>
		<th data-name="banner_g_pos_x"><div id="elh_remates_banner_g_pos_x" class="remates_banner_g_pos_x"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g_pos_x->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g_pos_x"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g_pos_x) ?>',1);"><div id="elh_remates_banner_g_pos_x" class="remates_banner_g_pos_x">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g_pos_x->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g_pos_x->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g_pos_x->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
	<?php if ($remates->SortUrl($remates->banner_g_pos_y) == "") { ?>
		<th data-name="banner_g_pos_y"><div id="elh_remates_banner_g_pos_y" class="remates_banner_g_pos_y"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g_pos_y->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g_pos_y"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g_pos_y) ?>',1);"><div id="elh_remates_banner_g_pos_y" class="remates_banner_g_pos_y">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g_pos_y->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g_pos_y->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g_pos_y->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
	<?php if ($remates->SortUrl($remates->banner_g_link) == "") { ?>
		<th data-name="banner_g_link"><div id="elh_remates_banner_g_link" class="remates_banner_g_link"><div class="ewTableHeaderCaption"><?php echo $remates->banner_g_link->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="banner_g_link"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->banner_g_link) ?>',1);"><div id="elh_remates_banner_g_link" class="remates_banner_g_link">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->banner_g_link->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->banner_g_link->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->banner_g_link->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->activo->Visible) { // activo ?>
	<?php if ($remates->SortUrl($remates->activo) == "") { ?>
		<th data-name="activo"><div id="elh_remates_activo" class="remates_activo"><div class="ewTableHeaderCaption"><?php echo $remates->activo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="activo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->activo) ?>',1);"><div id="elh_remates_activo" class="remates_activo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->activo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->activo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->activo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->archivo->Visible) { // archivo ?>
	<?php if ($remates->SortUrl($remates->archivo) == "") { ?>
		<th data-name="archivo"><div id="elh_remates_archivo" class="remates_archivo"><div class="ewTableHeaderCaption"><?php echo $remates->archivo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="archivo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->archivo) ?>',1);"><div id="elh_remates_archivo" class="remates_archivo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->archivo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->archivo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->archivo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->precios->Visible) { // precios ?>
	<?php if ($remates->SortUrl($remates->precios) == "") { ?>
		<th data-name="precios"><div id="elh_remates_precios" class="remates_precios"><div class="ewTableHeaderCaption"><?php echo $remates->precios->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="precios"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->precios) ?>',1);"><div id="elh_remates_precios" class="remates_precios">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->precios->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->precios->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->precios->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
	<?php if ($remates->SortUrl($remates->mapa_img) == "") { ?>
		<th data-name="mapa_img"><div id="elh_remates_mapa_img" class="remates_mapa_img"><div class="ewTableHeaderCaption"><?php echo $remates->mapa_img->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="mapa_img"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->mapa_img) ?>',1);"><div id="elh_remates_mapa_img" class="remates_mapa_img">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->mapa_img->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->mapa_img->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->mapa_img->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
	<?php if ($remates->SortUrl($remates->video_final_servidor) == "") { ?>
		<th data-name="video_final_servidor"><div id="elh_remates_video_final_servidor" class="remates_video_final_servidor"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_servidor->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_servidor"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_servidor) ?>',1);"><div id="elh_remates_video_final_servidor" class="remates_video_final_servidor">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_servidor->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_servidor->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_servidor->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
	<?php if ($remates->SortUrl($remates->video_final_1) == "") { ?>
		<th data-name="video_final_1"><div id="elh_remates_video_final_1" class="remates_video_final_1"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_1) ?>',1);"><div id="elh_remates_video_final_1" class="remates_video_final_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
	<?php if ($remates->SortUrl($remates->video_titu_1) == "") { ?>
		<th data-name="video_titu_1"><div id="elh_remates_video_titu_1" class="remates_video_titu_1"><div class="ewTableHeaderCaption"><?php echo $remates->video_titu_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_titu_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_titu_1) ?>',1);"><div id="elh_remates_video_titu_1" class="remates_video_titu_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_titu_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_titu_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_titu_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
	<?php if ($remates->SortUrl($remates->video_final_2) == "") { ?>
		<th data-name="video_final_2"><div id="elh_remates_video_final_2" class="remates_video_final_2"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_2) ?>',1);"><div id="elh_remates_video_final_2" class="remates_video_final_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
	<?php if ($remates->SortUrl($remates->video_titu_2) == "") { ?>
		<th data-name="video_titu_2"><div id="elh_remates_video_titu_2" class="remates_video_titu_2"><div class="ewTableHeaderCaption"><?php echo $remates->video_titu_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_titu_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_titu_2) ?>',1);"><div id="elh_remates_video_titu_2" class="remates_video_titu_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_titu_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_titu_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_titu_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
	<?php if ($remates->SortUrl($remates->video_final_3) == "") { ?>
		<th data-name="video_final_3"><div id="elh_remates_video_final_3" class="remates_video_final_3"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_3) ?>',1);"><div id="elh_remates_video_final_3" class="remates_video_final_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
	<?php if ($remates->SortUrl($remates->video_titu_3) == "") { ?>
		<th data-name="video_titu_3"><div id="elh_remates_video_titu_3" class="remates_video_titu_3"><div class="ewTableHeaderCaption"><?php echo $remates->video_titu_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_titu_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_titu_3) ?>',1);"><div id="elh_remates_video_titu_3" class="remates_video_titu_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_titu_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_titu_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_titu_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
	<?php if ($remates->SortUrl($remates->video_final_4) == "") { ?>
		<th data-name="video_final_4"><div id="elh_remates_video_final_4" class="remates_video_final_4"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_4) ?>',1);"><div id="elh_remates_video_final_4" class="remates_video_final_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
	<?php if ($remates->SortUrl($remates->video_titu_4) == "") { ?>
		<th data-name="video_titu_4"><div id="elh_remates_video_titu_4" class="remates_video_titu_4"><div class="ewTableHeaderCaption"><?php echo $remates->video_titu_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_titu_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_titu_4) ?>',1);"><div id="elh_remates_video_titu_4" class="remates_video_titu_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_titu_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_titu_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_titu_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
	<?php if ($remates->SortUrl($remates->video_final_5) == "") { ?>
		<th data-name="video_final_5"><div id="elh_remates_video_final_5" class="remates_video_final_5"><div class="ewTableHeaderCaption"><?php echo $remates->video_final_5->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_final_5"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_final_5) ?>',1);"><div id="elh_remates_video_final_5" class="remates_video_final_5">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_final_5->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_final_5->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_final_5->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
	<?php if ($remates->SortUrl($remates->video_titu_5) == "") { ?>
		<th data-name="video_titu_5"><div id="elh_remates_video_titu_5" class="remates_video_titu_5"><div class="ewTableHeaderCaption"><?php echo $remates->video_titu_5->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_titu_5"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $remates->SortUrl($remates->video_titu_5) ?>',1);"><div id="elh_remates_video_titu_5" class="remates_video_titu_5">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $remates->video_titu_5->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($remates->video_titu_5->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($remates->video_titu_5->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$remates_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
	if ($remates->CurrentAction == "add" || $remates->CurrentAction == "copy") {
		$remates_list->RowIndex = 0;
		$remates_list->KeyCount = $remates_list->RowIndex;
		if ($remates->CurrentAction == "copy" && !$remates_list->LoadRow())
				$remates->CurrentAction = "add";
		if ($remates->CurrentAction == "add")
			$remates_list->LoadDefaultValues();
		if ($remates->EventCancelled) // Insert failed
			$remates_list->RestoreFormValues(); // Restore form values

		// Set row properties
		$remates->ResetAttrs();
		$remates->RowAttrs = array_merge($remates->RowAttrs, array('data-rowindex'=>0, 'id'=>'r0_remates', 'data-rowtype'=>EW_ROWTYPE_ADD));
		$remates->RowType = EW_ROWTYPE_ADD;

		// Render row
		$remates_list->RenderRow();

		// Render list options
		$remates_list->RenderListOptions();
		$remates_list->StartRowCnt = 0;
?>
	<tr<?php echo $remates->RowAttributes() ?>>
<?php

// Render list options (body, left)
$remates_list->ListOptions->Render("body", "left", $remates_list->RowCnt);
?>
	<?php if ($remates->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="remates" data-field="x_id" name="o<?php echo $remates_list->RowIndex ?>_id" id="o<?php echo $remates_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($remates->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->tipo->Visible) { // tipo ?>
		<td data-name="tipo">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_tipo" class="form-group remates_tipo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_tipo" id="x<?php echo $remates_list->RowIndex ?>_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_tipo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_tipo" name="o<?php echo $remates_list->RowIndex ?>_tipo" id="o<?php echo $remates_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($remates->tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->plataforma->Visible) { // plataforma ?>
		<td data-name="plataforma">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_plataforma" class="form-group remates_plataforma">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_plataforma" name="x<?php echo $remates_list->RowIndex ?>_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_plataforma") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_plataforma" name="o<?php echo $remates_list->RowIndex ?>_plataforma" id="o<?php echo $remates_list->RowIndex ?>_plataforma" value="<?php echo ew_HtmlEncode($remates->plataforma->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->ext_id->Visible) { // ext_id ?>
		<td data-name="ext_id">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_ext_id" class="form-group remates_ext_id">
<input type="text" data-table="remates" data-field="x_ext_id" name="x<?php echo $remates_list->RowIndex ?>_ext_id" id="x<?php echo $remates_list->RowIndex ?>_ext_id" size="45" placeholder="<?php echo ew_HtmlEncode($remates->ext_id->getPlaceHolder()) ?>" value="<?php echo $remates->ext_id->EditValue ?>"<?php echo $remates->ext_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_ext_id" name="o<?php echo $remates_list->RowIndex ?>_ext_id" id="o<?php echo $remates_list->RowIndex ?>_ext_id" value="<?php echo ew_HtmlEncode($remates->ext_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->fecha->Visible) { // fecha ?>
		<td data-name="fecha">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_fecha" class="form-group remates_fecha">
<input type="text" data-table="remates" data-field="x_fecha" name="x<?php echo $remates_list->RowIndex ?>_fecha" id="x<?php echo $remates_list->RowIndex ?>_fecha" placeholder="<?php echo ew_HtmlEncode($remates->fecha->getPlaceHolder()) ?>" value="<?php echo $remates->fecha->EditValue ?>"<?php echo $remates->fecha->EditAttributes() ?>>
<?php if (!$remates->fecha->ReadOnly && !$remates->fecha->Disabled && !isset($remates->fecha->EditAttrs["readonly"]) && !isset($remates->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fremateslist", "x<?php echo $remates_list->RowIndex ?>_fecha", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="remates" data-field="x_fecha" name="o<?php echo $remates_list->RowIndex ?>_fecha" id="o<?php echo $remates_list->RowIndex ?>_fecha" value="<?php echo ew_HtmlEncode($remates->fecha->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
		<td data-name="hora_inicio">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_inicio" class="form-group remates_hora_inicio">
<input type="text" data-table="remates" data-field="x_hora_inicio" name="x<?php echo $remates_list->RowIndex ?>_hora_inicio" id="x<?php echo $remates_list->RowIndex ?>_hora_inicio" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_inicio->getPlaceHolder()) ?>" value="<?php echo $remates->hora_inicio->EditValue ?>"<?php echo $remates->hora_inicio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_inicio" name="o<?php echo $remates_list->RowIndex ?>_hora_inicio" id="o<?php echo $remates_list->RowIndex ?>_hora_inicio" value="<?php echo ew_HtmlEncode($remates->hora_inicio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
		<td data-name="hora_fin">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_fin" class="form-group remates_hora_fin">
<input type="text" data-table="remates" data-field="x_hora_fin" name="x<?php echo $remates_list->RowIndex ?>_hora_fin" id="x<?php echo $remates_list->RowIndex ?>_hora_fin" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_fin->getPlaceHolder()) ?>" value="<?php echo $remates->hora_fin->EditValue ?>"<?php echo $remates->hora_fin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_fin" name="o<?php echo $remates_list->RowIndex ?>_hora_fin" id="o<?php echo $remates_list->RowIndex ?>_hora_fin" value="<?php echo ew_HtmlEncode($remates->hora_fin->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->zona->Visible) { // zona ?>
		<td data-name="zona">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_zona" class="form-group remates_zona">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_zona" name="x<?php echo $remates_list->RowIndex ?>_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_zona") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_zona" name="o<?php echo $remates_list->RowIndex ?>_zona" id="o<?php echo $remates_list->RowIndex ?>_zona" value="<?php echo ew_HtmlEncode($remates->zona->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->lugar->Visible) { // lugar ?>
		<td data-name="lugar">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_lugar" class="form-group remates_lugar">
<input type="text" data-table="remates" data-field="x_lugar" name="x<?php echo $remates_list->RowIndex ?>_lugar" id="x<?php echo $remates_list->RowIndex ?>_lugar" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->lugar->getPlaceHolder()) ?>" value="<?php echo $remates->lugar->EditValue ?>"<?php echo $remates->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_lugar" name="o<?php echo $remates_list->RowIndex ?>_lugar" id="o<?php echo $remates_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($remates->lugar->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->locacion->Visible) { // locacion ?>
		<td data-name="locacion">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_locacion" class="form-group remates_locacion">
<input type="text" data-table="remates" data-field="x_locacion" name="x<?php echo $remates_list->RowIndex ?>_locacion" id="x<?php echo $remates_list->RowIndex ?>_locacion" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->locacion->getPlaceHolder()) ?>" value="<?php echo $remates->locacion->EditValue ?>"<?php echo $remates->locacion->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_locacion" name="o<?php echo $remates_list->RowIndex ?>_locacion" id="o<?php echo $remates_list->RowIndex ?>_locacion" value="<?php echo ew_HtmlEncode($remates->locacion->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->numero->Visible) { // numero ?>
		<td data-name="numero">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_numero" class="form-group remates_numero">
<input type="text" data-table="remates" data-field="x_numero" name="x<?php echo $remates_list->RowIndex ?>_numero" id="x<?php echo $remates_list->RowIndex ?>_numero" size="30" placeholder="<?php echo ew_HtmlEncode($remates->numero->getPlaceHolder()) ?>" value="<?php echo $remates->numero->EditValue ?>"<?php echo $remates->numero->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_numero" name="o<?php echo $remates_list->RowIndex ?>_numero" id="o<?php echo $remates_list->RowIndex ?>_numero" value="<?php echo ew_HtmlEncode($remates->numero->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->titulo->Visible) { // titulo ?>
		<td data-name="titulo">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_titulo" class="form-group remates_titulo">
<input type="text" data-table="remates" data-field="x_titulo" name="x<?php echo $remates_list->RowIndex ?>_titulo" id="x<?php echo $remates_list->RowIndex ?>_titulo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->titulo->getPlaceHolder()) ?>" value="<?php echo $remates->titulo->EditValue ?>"<?php echo $remates->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_titulo" name="o<?php echo $remates_list->RowIndex ?>_titulo" id="o<?php echo $remates_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($remates->titulo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_cantidad" class="form-group remates_cantidad">
<input type="text" data-table="remates" data-field="x_cantidad" name="x<?php echo $remates_list->RowIndex ?>_cantidad" id="x<?php echo $remates_list->RowIndex ?>_cantidad" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($remates->cantidad->getPlaceHolder()) ?>" value="<?php echo $remates->cantidad->EditValue ?>"<?php echo $remates->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_cantidad" name="o<?php echo $remates_list->RowIndex ?>_cantidad" id="o<?php echo $remates_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($remates->cantidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
		<td data-name="yacare_visible">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_yacare_visible" class="form-group remates_yacare_visible">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_yacare_visible" name="x<?php echo $remates_list->RowIndex ?>_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_yacare_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_yacare_visible" name="o<?php echo $remates_list->RowIndex ?>_yacare_visible" id="o<?php echo $remates_list->RowIndex ?>_yacare_visible" value="<?php echo ew_HtmlEncode($remates->yacare_visible->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g->Visible) { // banner_g ?>
		<td data-name="banner_g">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g" class="form-group remates_banner_g">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_banner_g">
<span title="<?php echo $remates->banner_g->FldTitle() ? $remates->banner_g->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_g->ReadOnly || $remates->banner_g->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_g" name="x<?php echo $remates_list->RowIndex ?>_banner_g" id="x<?php echo $remates_list->RowIndex ?>_banner_g"<?php echo $remates->banner_g->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fn_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fa_x<?php echo $remates_list->RowIndex ?>_banner_g" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fs_x<?php echo $remates_list->RowIndex ?>_banner_g" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fx_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fm_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_banner_g" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g" name="o<?php echo $remates_list->RowIndex ?>_banner_g" id="o<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo ew_HtmlEncode($remates->banner_g->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
		<td data-name="banner_g_visible">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_visible" class="form-group remates_banner_g_visible">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_banner_g_visible" name="x<?php echo $remates_list->RowIndex ?>_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_banner_g_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_visible" name="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" id="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" value="<?php echo ew_HtmlEncode($remates->banner_g_visible->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
		<td data-name="banner_g_z_index">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_z_index" class="form-group remates_banner_g_z_index">
<input type="text" data-table="remates" data-field="x_banner_g_z_index" name="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_z_index->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_z_index->EditValue ?>"<?php echo $remates->banner_g_z_index->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_z_index" name="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" value="<?php echo ew_HtmlEncode($remates->banner_g_z_index->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
		<td data-name="banner_g_pos_x">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_x" class="form-group remates_banner_g_pos_x">
<input type="text" data-table="remates" data-field="x_banner_g_pos_x" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_x->EditValue ?>"<?php echo $remates->banner_g_pos_x->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_x" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
		<td data-name="banner_g_pos_y">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_y" class="form-group remates_banner_g_pos_y">
<input type="text" data-table="remates" data-field="x_banner_g_pos_y" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_y->EditValue ?>"<?php echo $remates->banner_g_pos_y->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_y" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
		<td data-name="banner_g_link">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_link" class="form-group remates_banner_g_link">
<input type="text" data-table="remates" data-field="x_banner_g_link" name="x<?php echo $remates_list->RowIndex ?>_banner_g_link" id="x<?php echo $remates_list->RowIndex ?>_banner_g_link" size="50" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_link->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_link->EditValue ?>"<?php echo $remates->banner_g_link->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_link" name="o<?php echo $remates_list->RowIndex ?>_banner_g_link" id="o<?php echo $remates_list->RowIndex ?>_banner_g_link" value="<?php echo ew_HtmlEncode($remates->banner_g_link->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->activo->Visible) { // activo ?>
		<td data-name="activo">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_activo" class="form-group remates_activo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_activo" id="x<?php echo $remates_list->RowIndex ?>_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_activo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_activo" name="o<?php echo $remates_list->RowIndex ?>_activo" id="o<?php echo $remates_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($remates->activo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->archivo->Visible) { // archivo ?>
		<td data-name="archivo">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_archivo" class="form-group remates_archivo">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_archivo">
<span title="<?php echo $remates->archivo->FldTitle() ? $remates->archivo->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->archivo->ReadOnly || $remates->archivo->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_archivo" name="x<?php echo $remates_list->RowIndex ?>_archivo" id="x<?php echo $remates_list->RowIndex ?>_archivo"<?php echo $remates->archivo->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fn_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fa_x<?php echo $remates_list->RowIndex ?>_archivo" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fs_x<?php echo $remates_list->RowIndex ?>_archivo" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fx_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fm_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_archivo" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_archivo" name="o<?php echo $remates_list->RowIndex ?>_archivo" id="o<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo ew_HtmlEncode($remates->archivo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->precios->Visible) { // precios ?>
		<td data-name="precios">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_precios" class="form-group remates_precios">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_precios" name="x<?php echo $remates_list->RowIndex ?>_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_precios") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_precios" name="o<?php echo $remates_list->RowIndex ?>_precios" id="o<?php echo $remates_list->RowIndex ?>_precios" value="<?php echo ew_HtmlEncode($remates->precios->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
		<td data-name="mapa_img">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_mapa_img" class="form-group remates_mapa_img">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_mapa_img">
<span title="<?php echo $remates->mapa_img->FldTitle() ? $remates->mapa_img->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->mapa_img->ReadOnly || $remates->mapa_img->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_mapa_img" name="x<?php echo $remates_list->RowIndex ?>_mapa_img" id="x<?php echo $remates_list->RowIndex ?>_mapa_img"<?php echo $remates->mapa_img->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_mapa_img" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_mapa_img" name="o<?php echo $remates_list->RowIndex ?>_mapa_img" id="o<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo ew_HtmlEncode($remates->mapa_img->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
		<td data-name="video_final_servidor">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_servidor" class="form-group remates_video_final_servidor">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_video_final_servidor") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_servidor" name="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="<?php echo ew_HtmlEncode($remates->video_final_servidor->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
		<td data-name="video_final_1">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_1" class="form-group remates_video_final_1">
<input type="text" data-table="remates" data-field="x_video_final_1" name="x<?php echo $remates_list->RowIndex ?>_video_final_1" id="x<?php echo $remates_list->RowIndex ?>_video_final_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_1->EditValue ?>"<?php echo $remates->video_final_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_1" name="o<?php echo $remates_list->RowIndex ?>_video_final_1" id="o<?php echo $remates_list->RowIndex ?>_video_final_1" value="<?php echo ew_HtmlEncode($remates->video_final_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
		<td data-name="video_titu_1">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_1" class="form-group remates_video_titu_1">
<input type="text" data-table="remates" data-field="x_video_titu_1" name="x<?php echo $remates_list->RowIndex ?>_video_titu_1" id="x<?php echo $remates_list->RowIndex ?>_video_titu_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_1->EditValue ?>"<?php echo $remates->video_titu_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_1" name="o<?php echo $remates_list->RowIndex ?>_video_titu_1" id="o<?php echo $remates_list->RowIndex ?>_video_titu_1" value="<?php echo ew_HtmlEncode($remates->video_titu_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
		<td data-name="video_final_2">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_2" class="form-group remates_video_final_2">
<input type="text" data-table="remates" data-field="x_video_final_2" name="x<?php echo $remates_list->RowIndex ?>_video_final_2" id="x<?php echo $remates_list->RowIndex ?>_video_final_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_2->EditValue ?>"<?php echo $remates->video_final_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_2" name="o<?php echo $remates_list->RowIndex ?>_video_final_2" id="o<?php echo $remates_list->RowIndex ?>_video_final_2" value="<?php echo ew_HtmlEncode($remates->video_final_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
		<td data-name="video_titu_2">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_2" class="form-group remates_video_titu_2">
<input type="text" data-table="remates" data-field="x_video_titu_2" name="x<?php echo $remates_list->RowIndex ?>_video_titu_2" id="x<?php echo $remates_list->RowIndex ?>_video_titu_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_2->EditValue ?>"<?php echo $remates->video_titu_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_2" name="o<?php echo $remates_list->RowIndex ?>_video_titu_2" id="o<?php echo $remates_list->RowIndex ?>_video_titu_2" value="<?php echo ew_HtmlEncode($remates->video_titu_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
		<td data-name="video_final_3">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_3" class="form-group remates_video_final_3">
<input type="text" data-table="remates" data-field="x_video_final_3" name="x<?php echo $remates_list->RowIndex ?>_video_final_3" id="x<?php echo $remates_list->RowIndex ?>_video_final_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_3->EditValue ?>"<?php echo $remates->video_final_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_3" name="o<?php echo $remates_list->RowIndex ?>_video_final_3" id="o<?php echo $remates_list->RowIndex ?>_video_final_3" value="<?php echo ew_HtmlEncode($remates->video_final_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
		<td data-name="video_titu_3">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_3" class="form-group remates_video_titu_3">
<input type="text" data-table="remates" data-field="x_video_titu_3" name="x<?php echo $remates_list->RowIndex ?>_video_titu_3" id="x<?php echo $remates_list->RowIndex ?>_video_titu_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_3->EditValue ?>"<?php echo $remates->video_titu_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_3" name="o<?php echo $remates_list->RowIndex ?>_video_titu_3" id="o<?php echo $remates_list->RowIndex ?>_video_titu_3" value="<?php echo ew_HtmlEncode($remates->video_titu_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
		<td data-name="video_final_4">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_4" class="form-group remates_video_final_4">
<input type="text" data-table="remates" data-field="x_video_final_4" name="x<?php echo $remates_list->RowIndex ?>_video_final_4" id="x<?php echo $remates_list->RowIndex ?>_video_final_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_4->EditValue ?>"<?php echo $remates->video_final_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_4" name="o<?php echo $remates_list->RowIndex ?>_video_final_4" id="o<?php echo $remates_list->RowIndex ?>_video_final_4" value="<?php echo ew_HtmlEncode($remates->video_final_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
		<td data-name="video_titu_4">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_4" class="form-group remates_video_titu_4">
<input type="text" data-table="remates" data-field="x_video_titu_4" name="x<?php echo $remates_list->RowIndex ?>_video_titu_4" id="x<?php echo $remates_list->RowIndex ?>_video_titu_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_4->EditValue ?>"<?php echo $remates->video_titu_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_4" name="o<?php echo $remates_list->RowIndex ?>_video_titu_4" id="o<?php echo $remates_list->RowIndex ?>_video_titu_4" value="<?php echo ew_HtmlEncode($remates->video_titu_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
		<td data-name="video_final_5">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_5" class="form-group remates_video_final_5">
<input type="text" data-table="remates" data-field="x_video_final_5" name="x<?php echo $remates_list->RowIndex ?>_video_final_5" id="x<?php echo $remates_list->RowIndex ?>_video_final_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_5->EditValue ?>"<?php echo $remates->video_final_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_5" name="o<?php echo $remates_list->RowIndex ?>_video_final_5" id="o<?php echo $remates_list->RowIndex ?>_video_final_5" value="<?php echo ew_HtmlEncode($remates->video_final_5->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
		<td data-name="video_titu_5">
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_5" class="form-group remates_video_titu_5">
<input type="text" data-table="remates" data-field="x_video_titu_5" name="x<?php echo $remates_list->RowIndex ?>_video_titu_5" id="x<?php echo $remates_list->RowIndex ?>_video_titu_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_5->EditValue ?>"<?php echo $remates->video_titu_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_5" name="o<?php echo $remates_list->RowIndex ?>_video_titu_5" id="o<?php echo $remates_list->RowIndex ?>_video_titu_5" value="<?php echo ew_HtmlEncode($remates->video_titu_5->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$remates_list->ListOptions->Render("body", "right", $remates_list->RowCnt);
?>
<script type="text/javascript">
fremateslist.UpdateOpts(<?php echo $remates_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
<?php
if ($remates->ExportAll && $remates->Export <> "") {
	$remates_list->StopRec = $remates_list->TotalRecs;
} else {

	// Set the last record to display
	if ($remates_list->TotalRecs > $remates_list->StartRec + $remates_list->DisplayRecs - 1)
		$remates_list->StopRec = $remates_list->StartRec + $remates_list->DisplayRecs - 1;
	else
		$remates_list->StopRec = $remates_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($remates_list->FormKeyCountName) && ($remates->CurrentAction == "gridadd" || $remates->CurrentAction == "gridedit" || $remates->CurrentAction == "F")) {
		$remates_list->KeyCount = $objForm->GetValue($remates_list->FormKeyCountName);
		$remates_list->StopRec = $remates_list->StartRec + $remates_list->KeyCount - 1;
	}
}
$remates_list->RecCnt = $remates_list->StartRec - 1;
if ($remates_list->Recordset && !$remates_list->Recordset->EOF) {
	$remates_list->Recordset->MoveFirst();
	$bSelectLimit = $remates_list->UseSelectLimit;
	if (!$bSelectLimit && $remates_list->StartRec > 1)
		$remates_list->Recordset->Move($remates_list->StartRec - 1);
} elseif (!$remates->AllowAddDeleteRow && $remates_list->StopRec == 0) {
	$remates_list->StopRec = $remates->GridAddRowCount;
}

// Initialize aggregate
$remates->RowType = EW_ROWTYPE_AGGREGATEINIT;
$remates->ResetAttrs();
$remates_list->RenderRow();
$remates_list->EditRowCnt = 0;
if ($remates->CurrentAction == "edit")
	$remates_list->RowIndex = 1;
if ($remates->CurrentAction == "gridadd")
	$remates_list->RowIndex = 0;
if ($remates->CurrentAction == "gridedit")
	$remates_list->RowIndex = 0;
while ($remates_list->RecCnt < $remates_list->StopRec) {
	$remates_list->RecCnt++;
	if (intval($remates_list->RecCnt) >= intval($remates_list->StartRec)) {
		$remates_list->RowCnt++;
		if ($remates->CurrentAction == "gridadd" || $remates->CurrentAction == "gridedit" || $remates->CurrentAction == "F") {
			$remates_list->RowIndex++;
			$objForm->Index = $remates_list->RowIndex;
			if ($objForm->HasValue($remates_list->FormActionName))
				$remates_list->RowAction = strval($objForm->GetValue($remates_list->FormActionName));
			elseif ($remates->CurrentAction == "gridadd")
				$remates_list->RowAction = "insert";
			else
				$remates_list->RowAction = "";
		}

		// Set up key count
		$remates_list->KeyCount = $remates_list->RowIndex;

		// Init row class and style
		$remates->ResetAttrs();
		$remates->CssClass = "";
		if ($remates->CurrentAction == "gridadd") {
			$remates_list->LoadDefaultValues(); // Load default values
		} else {
			$remates_list->LoadRowValues($remates_list->Recordset); // Load row values
		}
		$remates->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($remates->CurrentAction == "gridadd") // Grid add
			$remates->RowType = EW_ROWTYPE_ADD; // Render add
		if ($remates->CurrentAction == "gridadd" && $remates->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$remates_list->RestoreCurrentRowFormValues($remates_list->RowIndex); // Restore form values
		if ($remates->CurrentAction == "edit") {
			if ($remates_list->CheckInlineEditKey() && $remates_list->EditRowCnt == 0) { // Inline edit
				$remates->RowType = EW_ROWTYPE_EDIT; // Render edit
			}
		}
		if ($remates->CurrentAction == "gridedit") { // Grid edit
			if ($remates->EventCancelled) {
				$remates_list->RestoreCurrentRowFormValues($remates_list->RowIndex); // Restore form values
			}
			if ($remates_list->RowAction == "insert")
				$remates->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$remates->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($remates->CurrentAction == "edit" && $remates->RowType == EW_ROWTYPE_EDIT && $remates->EventCancelled) { // Update failed
			$objForm->Index = 1;
			$remates_list->RestoreFormValues(); // Restore form values
		}
		if ($remates->CurrentAction == "gridedit" && ($remates->RowType == EW_ROWTYPE_EDIT || $remates->RowType == EW_ROWTYPE_ADD) && $remates->EventCancelled) // Update failed
			$remates_list->RestoreCurrentRowFormValues($remates_list->RowIndex); // Restore form values
		if ($remates->RowType == EW_ROWTYPE_EDIT) // Edit row
			$remates_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$remates->RowAttrs = array_merge($remates->RowAttrs, array('data-rowindex'=>$remates_list->RowCnt, 'id'=>'r' . $remates_list->RowCnt . '_remates', 'data-rowtype'=>$remates->RowType));

		// Render row
		$remates_list->RenderRow();

		// Render list options
		$remates_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($remates_list->RowAction <> "delete" && $remates_list->RowAction <> "insertdelete" && !($remates_list->RowAction == "insert" && $remates->CurrentAction == "F" && $remates_list->EmptyRow())) {
?>
	<tr<?php echo $remates->RowAttributes() ?>>
<?php

// Render list options (body, left)
$remates_list->ListOptions->Render("body", "left", $remates_list->RowCnt);
?>
	<?php if ($remates->id->Visible) { // id ?>
		<td data-name="id"<?php echo $remates->id->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="remates" data-field="x_id" name="o<?php echo $remates_list->RowIndex ?>_id" id="o<?php echo $remates_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($remates->id->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_id" class="form-group remates_id">
<span<?php echo $remates->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $remates->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="remates" data-field="x_id" name="x<?php echo $remates_list->RowIndex ?>_id" id="x<?php echo $remates_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($remates->id->CurrentValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_id" class="remates_id">
<span<?php echo $remates->id->ViewAttributes() ?>>
<?php echo $remates->id->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $remates_list->PageObjName . "_row_" . $remates_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($remates->tipo->Visible) { // tipo ?>
		<td data-name="tipo"<?php echo $remates->tipo->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_tipo" class="form-group remates_tipo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_tipo" id="x<?php echo $remates_list->RowIndex ?>_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_tipo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_tipo" name="o<?php echo $remates_list->RowIndex ?>_tipo" id="o<?php echo $remates_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($remates->tipo->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_tipo" class="form-group remates_tipo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_tipo" id="x<?php echo $remates_list->RowIndex ?>_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_tipo") ?>
</div></div>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_tipo" class="remates_tipo">
<span<?php echo $remates->tipo->ViewAttributes() ?>>
<?php echo $remates->tipo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->plataforma->Visible) { // plataforma ?>
		<td data-name="plataforma"<?php echo $remates->plataforma->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_plataforma" class="form-group remates_plataforma">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_plataforma" name="x<?php echo $remates_list->RowIndex ?>_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_plataforma") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_plataforma" name="o<?php echo $remates_list->RowIndex ?>_plataforma" id="o<?php echo $remates_list->RowIndex ?>_plataforma" value="<?php echo ew_HtmlEncode($remates->plataforma->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_plataforma" class="form-group remates_plataforma">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_plataforma" name="x<?php echo $remates_list->RowIndex ?>_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_plataforma") ?>
</select>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_plataforma" class="remates_plataforma">
<span<?php echo $remates->plataforma->ViewAttributes() ?>>
<?php echo $remates->plataforma->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->ext_id->Visible) { // ext_id ?>
		<td data-name="ext_id"<?php echo $remates->ext_id->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_ext_id" class="form-group remates_ext_id">
<input type="text" data-table="remates" data-field="x_ext_id" name="x<?php echo $remates_list->RowIndex ?>_ext_id" id="x<?php echo $remates_list->RowIndex ?>_ext_id" size="45" placeholder="<?php echo ew_HtmlEncode($remates->ext_id->getPlaceHolder()) ?>" value="<?php echo $remates->ext_id->EditValue ?>"<?php echo $remates->ext_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_ext_id" name="o<?php echo $remates_list->RowIndex ?>_ext_id" id="o<?php echo $remates_list->RowIndex ?>_ext_id" value="<?php echo ew_HtmlEncode($remates->ext_id->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_ext_id" class="form-group remates_ext_id">
<input type="text" data-table="remates" data-field="x_ext_id" name="x<?php echo $remates_list->RowIndex ?>_ext_id" id="x<?php echo $remates_list->RowIndex ?>_ext_id" size="45" placeholder="<?php echo ew_HtmlEncode($remates->ext_id->getPlaceHolder()) ?>" value="<?php echo $remates->ext_id->EditValue ?>"<?php echo $remates->ext_id->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_ext_id" class="remates_ext_id">
<span<?php echo $remates->ext_id->ViewAttributes() ?>>
<?php echo $remates->ext_id->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->fecha->Visible) { // fecha ?>
		<td data-name="fecha"<?php echo $remates->fecha->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_fecha" class="form-group remates_fecha">
<input type="text" data-table="remates" data-field="x_fecha" name="x<?php echo $remates_list->RowIndex ?>_fecha" id="x<?php echo $remates_list->RowIndex ?>_fecha" placeholder="<?php echo ew_HtmlEncode($remates->fecha->getPlaceHolder()) ?>" value="<?php echo $remates->fecha->EditValue ?>"<?php echo $remates->fecha->EditAttributes() ?>>
<?php if (!$remates->fecha->ReadOnly && !$remates->fecha->Disabled && !isset($remates->fecha->EditAttrs["readonly"]) && !isset($remates->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fremateslist", "x<?php echo $remates_list->RowIndex ?>_fecha", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="remates" data-field="x_fecha" name="o<?php echo $remates_list->RowIndex ?>_fecha" id="o<?php echo $remates_list->RowIndex ?>_fecha" value="<?php echo ew_HtmlEncode($remates->fecha->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_fecha" class="form-group remates_fecha">
<input type="text" data-table="remates" data-field="x_fecha" name="x<?php echo $remates_list->RowIndex ?>_fecha" id="x<?php echo $remates_list->RowIndex ?>_fecha" placeholder="<?php echo ew_HtmlEncode($remates->fecha->getPlaceHolder()) ?>" value="<?php echo $remates->fecha->EditValue ?>"<?php echo $remates->fecha->EditAttributes() ?>>
<?php if (!$remates->fecha->ReadOnly && !$remates->fecha->Disabled && !isset($remates->fecha->EditAttrs["readonly"]) && !isset($remates->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fremateslist", "x<?php echo $remates_list->RowIndex ?>_fecha", 0);
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_fecha" class="remates_fecha">
<span<?php echo $remates->fecha->ViewAttributes() ?>>
<?php echo $remates->fecha->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
		<td data-name="hora_inicio"<?php echo $remates->hora_inicio->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_inicio" class="form-group remates_hora_inicio">
<input type="text" data-table="remates" data-field="x_hora_inicio" name="x<?php echo $remates_list->RowIndex ?>_hora_inicio" id="x<?php echo $remates_list->RowIndex ?>_hora_inicio" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_inicio->getPlaceHolder()) ?>" value="<?php echo $remates->hora_inicio->EditValue ?>"<?php echo $remates->hora_inicio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_inicio" name="o<?php echo $remates_list->RowIndex ?>_hora_inicio" id="o<?php echo $remates_list->RowIndex ?>_hora_inicio" value="<?php echo ew_HtmlEncode($remates->hora_inicio->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_inicio" class="form-group remates_hora_inicio">
<input type="text" data-table="remates" data-field="x_hora_inicio" name="x<?php echo $remates_list->RowIndex ?>_hora_inicio" id="x<?php echo $remates_list->RowIndex ?>_hora_inicio" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_inicio->getPlaceHolder()) ?>" value="<?php echo $remates->hora_inicio->EditValue ?>"<?php echo $remates->hora_inicio->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_inicio" class="remates_hora_inicio">
<span<?php echo $remates->hora_inicio->ViewAttributes() ?>>
<?php echo $remates->hora_inicio->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
		<td data-name="hora_fin"<?php echo $remates->hora_fin->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_fin" class="form-group remates_hora_fin">
<input type="text" data-table="remates" data-field="x_hora_fin" name="x<?php echo $remates_list->RowIndex ?>_hora_fin" id="x<?php echo $remates_list->RowIndex ?>_hora_fin" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_fin->getPlaceHolder()) ?>" value="<?php echo $remates->hora_fin->EditValue ?>"<?php echo $remates->hora_fin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_fin" name="o<?php echo $remates_list->RowIndex ?>_hora_fin" id="o<?php echo $remates_list->RowIndex ?>_hora_fin" value="<?php echo ew_HtmlEncode($remates->hora_fin->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_fin" class="form-group remates_hora_fin">
<input type="text" data-table="remates" data-field="x_hora_fin" name="x<?php echo $remates_list->RowIndex ?>_hora_fin" id="x<?php echo $remates_list->RowIndex ?>_hora_fin" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_fin->getPlaceHolder()) ?>" value="<?php echo $remates->hora_fin->EditValue ?>"<?php echo $remates->hora_fin->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_hora_fin" class="remates_hora_fin">
<span<?php echo $remates->hora_fin->ViewAttributes() ?>>
<?php echo $remates->hora_fin->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->zona->Visible) { // zona ?>
		<td data-name="zona"<?php echo $remates->zona->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_zona" class="form-group remates_zona">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_zona" name="x<?php echo $remates_list->RowIndex ?>_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_zona") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_zona" name="o<?php echo $remates_list->RowIndex ?>_zona" id="o<?php echo $remates_list->RowIndex ?>_zona" value="<?php echo ew_HtmlEncode($remates->zona->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_zona" class="form-group remates_zona">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_zona" name="x<?php echo $remates_list->RowIndex ?>_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_zona") ?>
</select>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_zona" class="remates_zona">
<span<?php echo $remates->zona->ViewAttributes() ?>>
<?php echo $remates->zona->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->lugar->Visible) { // lugar ?>
		<td data-name="lugar"<?php echo $remates->lugar->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_lugar" class="form-group remates_lugar">
<input type="text" data-table="remates" data-field="x_lugar" name="x<?php echo $remates_list->RowIndex ?>_lugar" id="x<?php echo $remates_list->RowIndex ?>_lugar" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->lugar->getPlaceHolder()) ?>" value="<?php echo $remates->lugar->EditValue ?>"<?php echo $remates->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_lugar" name="o<?php echo $remates_list->RowIndex ?>_lugar" id="o<?php echo $remates_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($remates->lugar->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_lugar" class="form-group remates_lugar">
<input type="text" data-table="remates" data-field="x_lugar" name="x<?php echo $remates_list->RowIndex ?>_lugar" id="x<?php echo $remates_list->RowIndex ?>_lugar" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->lugar->getPlaceHolder()) ?>" value="<?php echo $remates->lugar->EditValue ?>"<?php echo $remates->lugar->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_lugar" class="remates_lugar">
<span<?php echo $remates->lugar->ViewAttributes() ?>>
<?php echo $remates->lugar->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->locacion->Visible) { // locacion ?>
		<td data-name="locacion"<?php echo $remates->locacion->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_locacion" class="form-group remates_locacion">
<input type="text" data-table="remates" data-field="x_locacion" name="x<?php echo $remates_list->RowIndex ?>_locacion" id="x<?php echo $remates_list->RowIndex ?>_locacion" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->locacion->getPlaceHolder()) ?>" value="<?php echo $remates->locacion->EditValue ?>"<?php echo $remates->locacion->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_locacion" name="o<?php echo $remates_list->RowIndex ?>_locacion" id="o<?php echo $remates_list->RowIndex ?>_locacion" value="<?php echo ew_HtmlEncode($remates->locacion->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_locacion" class="form-group remates_locacion">
<input type="text" data-table="remates" data-field="x_locacion" name="x<?php echo $remates_list->RowIndex ?>_locacion" id="x<?php echo $remates_list->RowIndex ?>_locacion" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->locacion->getPlaceHolder()) ?>" value="<?php echo $remates->locacion->EditValue ?>"<?php echo $remates->locacion->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_locacion" class="remates_locacion">
<span<?php echo $remates->locacion->ViewAttributes() ?>>
<?php echo $remates->locacion->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->numero->Visible) { // numero ?>
		<td data-name="numero"<?php echo $remates->numero->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_numero" class="form-group remates_numero">
<input type="text" data-table="remates" data-field="x_numero" name="x<?php echo $remates_list->RowIndex ?>_numero" id="x<?php echo $remates_list->RowIndex ?>_numero" size="30" placeholder="<?php echo ew_HtmlEncode($remates->numero->getPlaceHolder()) ?>" value="<?php echo $remates->numero->EditValue ?>"<?php echo $remates->numero->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_numero" name="o<?php echo $remates_list->RowIndex ?>_numero" id="o<?php echo $remates_list->RowIndex ?>_numero" value="<?php echo ew_HtmlEncode($remates->numero->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_numero" class="form-group remates_numero">
<input type="text" data-table="remates" data-field="x_numero" name="x<?php echo $remates_list->RowIndex ?>_numero" id="x<?php echo $remates_list->RowIndex ?>_numero" size="30" placeholder="<?php echo ew_HtmlEncode($remates->numero->getPlaceHolder()) ?>" value="<?php echo $remates->numero->EditValue ?>"<?php echo $remates->numero->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_numero" class="remates_numero">
<span<?php echo $remates->numero->ViewAttributes() ?>>
<?php echo $remates->numero->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->titulo->Visible) { // titulo ?>
		<td data-name="titulo"<?php echo $remates->titulo->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_titulo" class="form-group remates_titulo">
<input type="text" data-table="remates" data-field="x_titulo" name="x<?php echo $remates_list->RowIndex ?>_titulo" id="x<?php echo $remates_list->RowIndex ?>_titulo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->titulo->getPlaceHolder()) ?>" value="<?php echo $remates->titulo->EditValue ?>"<?php echo $remates->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_titulo" name="o<?php echo $remates_list->RowIndex ?>_titulo" id="o<?php echo $remates_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($remates->titulo->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_titulo" class="form-group remates_titulo">
<input type="text" data-table="remates" data-field="x_titulo" name="x<?php echo $remates_list->RowIndex ?>_titulo" id="x<?php echo $remates_list->RowIndex ?>_titulo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->titulo->getPlaceHolder()) ?>" value="<?php echo $remates->titulo->EditValue ?>"<?php echo $remates->titulo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_titulo" class="remates_titulo">
<span<?php echo $remates->titulo->ViewAttributes() ?>>
<?php echo $remates->titulo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad"<?php echo $remates->cantidad->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_cantidad" class="form-group remates_cantidad">
<input type="text" data-table="remates" data-field="x_cantidad" name="x<?php echo $remates_list->RowIndex ?>_cantidad" id="x<?php echo $remates_list->RowIndex ?>_cantidad" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($remates->cantidad->getPlaceHolder()) ?>" value="<?php echo $remates->cantidad->EditValue ?>"<?php echo $remates->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_cantidad" name="o<?php echo $remates_list->RowIndex ?>_cantidad" id="o<?php echo $remates_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($remates->cantidad->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_cantidad" class="form-group remates_cantidad">
<input type="text" data-table="remates" data-field="x_cantidad" name="x<?php echo $remates_list->RowIndex ?>_cantidad" id="x<?php echo $remates_list->RowIndex ?>_cantidad" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($remates->cantidad->getPlaceHolder()) ?>" value="<?php echo $remates->cantidad->EditValue ?>"<?php echo $remates->cantidad->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_cantidad" class="remates_cantidad">
<span<?php echo $remates->cantidad->ViewAttributes() ?>>
<?php echo $remates->cantidad->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
		<td data-name="yacare_visible"<?php echo $remates->yacare_visible->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_yacare_visible" class="form-group remates_yacare_visible">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_yacare_visible" name="x<?php echo $remates_list->RowIndex ?>_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_yacare_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_yacare_visible" name="o<?php echo $remates_list->RowIndex ?>_yacare_visible" id="o<?php echo $remates_list->RowIndex ?>_yacare_visible" value="<?php echo ew_HtmlEncode($remates->yacare_visible->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_yacare_visible" class="form-group remates_yacare_visible">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_yacare_visible" name="x<?php echo $remates_list->RowIndex ?>_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_yacare_visible") ?>
</select>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_yacare_visible" class="remates_yacare_visible">
<span<?php echo $remates->yacare_visible->ViewAttributes() ?>>
<?php echo $remates->yacare_visible->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g->Visible) { // banner_g ?>
		<td data-name="banner_g"<?php echo $remates->banner_g->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g" class="form-group remates_banner_g">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_banner_g">
<span title="<?php echo $remates->banner_g->FldTitle() ? $remates->banner_g->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_g->ReadOnly || $remates->banner_g->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_g" name="x<?php echo $remates_list->RowIndex ?>_banner_g" id="x<?php echo $remates_list->RowIndex ?>_banner_g"<?php echo $remates->banner_g->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fn_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fa_x<?php echo $remates_list->RowIndex ?>_banner_g" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fs_x<?php echo $remates_list->RowIndex ?>_banner_g" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fx_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fm_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_banner_g" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g" name="o<?php echo $remates_list->RowIndex ?>_banner_g" id="o<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo ew_HtmlEncode($remates->banner_g->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g" class="form-group remates_banner_g">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_banner_g">
<span title="<?php echo $remates->banner_g->FldTitle() ? $remates->banner_g->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_g->ReadOnly || $remates->banner_g->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_g" name="x<?php echo $remates_list->RowIndex ?>_banner_g" id="x<?php echo $remates_list->RowIndex ?>_banner_g"<?php echo $remates->banner_g->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fn_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $remates_list->RowIndex ?>_banner_g"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fa_x<?php echo $remates_list->RowIndex ?>_banner_g" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fa_x<?php echo $remates_list->RowIndex ?>_banner_g" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fs_x<?php echo $remates_list->RowIndex ?>_banner_g" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fx_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fm_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_banner_g" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g" class="remates_banner_g">
<span<?php echo $remates->banner_g->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->banner_g, $remates->banner_g->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
		<td data-name="banner_g_visible"<?php echo $remates->banner_g_visible->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_visible" class="form-group remates_banner_g_visible">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_banner_g_visible" name="x<?php echo $remates_list->RowIndex ?>_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_banner_g_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_visible" name="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" id="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" value="<?php echo ew_HtmlEncode($remates->banner_g_visible->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_visible" class="form-group remates_banner_g_visible">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_banner_g_visible" name="x<?php echo $remates_list->RowIndex ?>_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_banner_g_visible") ?>
</select>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_visible" class="remates_banner_g_visible">
<span<?php echo $remates->banner_g_visible->ViewAttributes() ?>>
<?php echo $remates->banner_g_visible->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
		<td data-name="banner_g_z_index"<?php echo $remates->banner_g_z_index->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_z_index" class="form-group remates_banner_g_z_index">
<input type="text" data-table="remates" data-field="x_banner_g_z_index" name="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_z_index->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_z_index->EditValue ?>"<?php echo $remates->banner_g_z_index->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_z_index" name="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" value="<?php echo ew_HtmlEncode($remates->banner_g_z_index->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_z_index" class="form-group remates_banner_g_z_index">
<input type="text" data-table="remates" data-field="x_banner_g_z_index" name="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_z_index->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_z_index->EditValue ?>"<?php echo $remates->banner_g_z_index->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_z_index" class="remates_banner_g_z_index">
<span<?php echo $remates->banner_g_z_index->ViewAttributes() ?>>
<?php echo $remates->banner_g_z_index->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
		<td data-name="banner_g_pos_x"<?php echo $remates->banner_g_pos_x->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_x" class="form-group remates_banner_g_pos_x">
<input type="text" data-table="remates" data-field="x_banner_g_pos_x" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_x->EditValue ?>"<?php echo $remates->banner_g_pos_x->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_x" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_x" class="form-group remates_banner_g_pos_x">
<input type="text" data-table="remates" data-field="x_banner_g_pos_x" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_x->EditValue ?>"<?php echo $remates->banner_g_pos_x->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_x" class="remates_banner_g_pos_x">
<span<?php echo $remates->banner_g_pos_x->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_x->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
		<td data-name="banner_g_pos_y"<?php echo $remates->banner_g_pos_y->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_y" class="form-group remates_banner_g_pos_y">
<input type="text" data-table="remates" data-field="x_banner_g_pos_y" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_y->EditValue ?>"<?php echo $remates->banner_g_pos_y->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_y" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_y" class="form-group remates_banner_g_pos_y">
<input type="text" data-table="remates" data-field="x_banner_g_pos_y" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_y->EditValue ?>"<?php echo $remates->banner_g_pos_y->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_pos_y" class="remates_banner_g_pos_y">
<span<?php echo $remates->banner_g_pos_y->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_y->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
		<td data-name="banner_g_link"<?php echo $remates->banner_g_link->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_link" class="form-group remates_banner_g_link">
<input type="text" data-table="remates" data-field="x_banner_g_link" name="x<?php echo $remates_list->RowIndex ?>_banner_g_link" id="x<?php echo $remates_list->RowIndex ?>_banner_g_link" size="50" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_link->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_link->EditValue ?>"<?php echo $remates->banner_g_link->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_link" name="o<?php echo $remates_list->RowIndex ?>_banner_g_link" id="o<?php echo $remates_list->RowIndex ?>_banner_g_link" value="<?php echo ew_HtmlEncode($remates->banner_g_link->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_link" class="form-group remates_banner_g_link">
<input type="text" data-table="remates" data-field="x_banner_g_link" name="x<?php echo $remates_list->RowIndex ?>_banner_g_link" id="x<?php echo $remates_list->RowIndex ?>_banner_g_link" size="50" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_link->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_link->EditValue ?>"<?php echo $remates->banner_g_link->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_banner_g_link" class="remates_banner_g_link">
<span<?php echo $remates->banner_g_link->ViewAttributes() ?>>
<?php echo $remates->banner_g_link->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->activo->Visible) { // activo ?>
		<td data-name="activo"<?php echo $remates->activo->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_activo" class="form-group remates_activo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_activo" id="x<?php echo $remates_list->RowIndex ?>_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_activo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_activo" name="o<?php echo $remates_list->RowIndex ?>_activo" id="o<?php echo $remates_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($remates->activo->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_activo" class="form-group remates_activo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_activo" id="x<?php echo $remates_list->RowIndex ?>_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_activo") ?>
</div></div>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_activo" class="remates_activo">
<span<?php echo $remates->activo->ViewAttributes() ?>>
<?php echo $remates->activo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->archivo->Visible) { // archivo ?>
		<td data-name="archivo"<?php echo $remates->archivo->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_archivo" class="form-group remates_archivo">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_archivo">
<span title="<?php echo $remates->archivo->FldTitle() ? $remates->archivo->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->archivo->ReadOnly || $remates->archivo->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_archivo" name="x<?php echo $remates_list->RowIndex ?>_archivo" id="x<?php echo $remates_list->RowIndex ?>_archivo"<?php echo $remates->archivo->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fn_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fa_x<?php echo $remates_list->RowIndex ?>_archivo" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fs_x<?php echo $remates_list->RowIndex ?>_archivo" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fx_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fm_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_archivo" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_archivo" name="o<?php echo $remates_list->RowIndex ?>_archivo" id="o<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo ew_HtmlEncode($remates->archivo->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_archivo" class="form-group remates_archivo">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_archivo">
<span title="<?php echo $remates->archivo->FldTitle() ? $remates->archivo->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->archivo->ReadOnly || $remates->archivo->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_archivo" name="x<?php echo $remates_list->RowIndex ?>_archivo" id="x<?php echo $remates_list->RowIndex ?>_archivo"<?php echo $remates->archivo->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fn_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $remates_list->RowIndex ?>_archivo"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fa_x<?php echo $remates_list->RowIndex ?>_archivo" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fa_x<?php echo $remates_list->RowIndex ?>_archivo" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fs_x<?php echo $remates_list->RowIndex ?>_archivo" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fx_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fm_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_archivo" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_archivo" class="remates_archivo">
<span<?php echo $remates->archivo->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->archivo, $remates->archivo->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->precios->Visible) { // precios ?>
		<td data-name="precios"<?php echo $remates->precios->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_precios" class="form-group remates_precios">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_precios" name="x<?php echo $remates_list->RowIndex ?>_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_precios") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_precios" name="o<?php echo $remates_list->RowIndex ?>_precios" id="o<?php echo $remates_list->RowIndex ?>_precios" value="<?php echo ew_HtmlEncode($remates->precios->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_precios" class="form-group remates_precios">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_precios" name="x<?php echo $remates_list->RowIndex ?>_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_precios") ?>
</select>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_precios" class="remates_precios">
<span<?php echo $remates->precios->ViewAttributes() ?>>
<?php echo $remates->precios->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
		<td data-name="mapa_img"<?php echo $remates->mapa_img->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_mapa_img" class="form-group remates_mapa_img">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_mapa_img">
<span title="<?php echo $remates->mapa_img->FldTitle() ? $remates->mapa_img->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->mapa_img->ReadOnly || $remates->mapa_img->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_mapa_img" name="x<?php echo $remates_list->RowIndex ?>_mapa_img" id="x<?php echo $remates_list->RowIndex ?>_mapa_img"<?php echo $remates->mapa_img->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_mapa_img" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_mapa_img" name="o<?php echo $remates_list->RowIndex ?>_mapa_img" id="o<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo ew_HtmlEncode($remates->mapa_img->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_mapa_img" class="form-group remates_mapa_img">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_mapa_img">
<span title="<?php echo $remates->mapa_img->FldTitle() ? $remates->mapa_img->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->mapa_img->ReadOnly || $remates->mapa_img->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_mapa_img" name="x<?php echo $remates_list->RowIndex ?>_mapa_img" id="x<?php echo $remates_list->RowIndex ?>_mapa_img"<?php echo $remates->mapa_img->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $remates_list->RowIndex ?>_mapa_img"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_mapa_img" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_mapa_img" class="remates_mapa_img">
<span<?php echo $remates->mapa_img->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->mapa_img, $remates->mapa_img->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
		<td data-name="video_final_servidor"<?php echo $remates->video_final_servidor->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_servidor" class="form-group remates_video_final_servidor">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_video_final_servidor") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_servidor" name="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="<?php echo ew_HtmlEncode($remates->video_final_servidor->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_servidor" class="form-group remates_video_final_servidor">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_video_final_servidor") ?>
</div></div>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_servidor" class="remates_video_final_servidor">
<span<?php echo $remates->video_final_servidor->ViewAttributes() ?>>
<?php echo $remates->video_final_servidor->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
		<td data-name="video_final_1"<?php echo $remates->video_final_1->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_1" class="form-group remates_video_final_1">
<input type="text" data-table="remates" data-field="x_video_final_1" name="x<?php echo $remates_list->RowIndex ?>_video_final_1" id="x<?php echo $remates_list->RowIndex ?>_video_final_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_1->EditValue ?>"<?php echo $remates->video_final_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_1" name="o<?php echo $remates_list->RowIndex ?>_video_final_1" id="o<?php echo $remates_list->RowIndex ?>_video_final_1" value="<?php echo ew_HtmlEncode($remates->video_final_1->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_1" class="form-group remates_video_final_1">
<input type="text" data-table="remates" data-field="x_video_final_1" name="x<?php echo $remates_list->RowIndex ?>_video_final_1" id="x<?php echo $remates_list->RowIndex ?>_video_final_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_1->EditValue ?>"<?php echo $remates->video_final_1->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_1" class="remates_video_final_1">
<span<?php echo $remates->video_final_1->ViewAttributes() ?>>
<?php echo $remates->video_final_1->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
		<td data-name="video_titu_1"<?php echo $remates->video_titu_1->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_1" class="form-group remates_video_titu_1">
<input type="text" data-table="remates" data-field="x_video_titu_1" name="x<?php echo $remates_list->RowIndex ?>_video_titu_1" id="x<?php echo $remates_list->RowIndex ?>_video_titu_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_1->EditValue ?>"<?php echo $remates->video_titu_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_1" name="o<?php echo $remates_list->RowIndex ?>_video_titu_1" id="o<?php echo $remates_list->RowIndex ?>_video_titu_1" value="<?php echo ew_HtmlEncode($remates->video_titu_1->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_1" class="form-group remates_video_titu_1">
<input type="text" data-table="remates" data-field="x_video_titu_1" name="x<?php echo $remates_list->RowIndex ?>_video_titu_1" id="x<?php echo $remates_list->RowIndex ?>_video_titu_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_1->EditValue ?>"<?php echo $remates->video_titu_1->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_1" class="remates_video_titu_1">
<span<?php echo $remates->video_titu_1->ViewAttributes() ?>>
<?php echo $remates->video_titu_1->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
		<td data-name="video_final_2"<?php echo $remates->video_final_2->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_2" class="form-group remates_video_final_2">
<input type="text" data-table="remates" data-field="x_video_final_2" name="x<?php echo $remates_list->RowIndex ?>_video_final_2" id="x<?php echo $remates_list->RowIndex ?>_video_final_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_2->EditValue ?>"<?php echo $remates->video_final_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_2" name="o<?php echo $remates_list->RowIndex ?>_video_final_2" id="o<?php echo $remates_list->RowIndex ?>_video_final_2" value="<?php echo ew_HtmlEncode($remates->video_final_2->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_2" class="form-group remates_video_final_2">
<input type="text" data-table="remates" data-field="x_video_final_2" name="x<?php echo $remates_list->RowIndex ?>_video_final_2" id="x<?php echo $remates_list->RowIndex ?>_video_final_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_2->EditValue ?>"<?php echo $remates->video_final_2->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_2" class="remates_video_final_2">
<span<?php echo $remates->video_final_2->ViewAttributes() ?>>
<?php echo $remates->video_final_2->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
		<td data-name="video_titu_2"<?php echo $remates->video_titu_2->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_2" class="form-group remates_video_titu_2">
<input type="text" data-table="remates" data-field="x_video_titu_2" name="x<?php echo $remates_list->RowIndex ?>_video_titu_2" id="x<?php echo $remates_list->RowIndex ?>_video_titu_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_2->EditValue ?>"<?php echo $remates->video_titu_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_2" name="o<?php echo $remates_list->RowIndex ?>_video_titu_2" id="o<?php echo $remates_list->RowIndex ?>_video_titu_2" value="<?php echo ew_HtmlEncode($remates->video_titu_2->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_2" class="form-group remates_video_titu_2">
<input type="text" data-table="remates" data-field="x_video_titu_2" name="x<?php echo $remates_list->RowIndex ?>_video_titu_2" id="x<?php echo $remates_list->RowIndex ?>_video_titu_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_2->EditValue ?>"<?php echo $remates->video_titu_2->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_2" class="remates_video_titu_2">
<span<?php echo $remates->video_titu_2->ViewAttributes() ?>>
<?php echo $remates->video_titu_2->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
		<td data-name="video_final_3"<?php echo $remates->video_final_3->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_3" class="form-group remates_video_final_3">
<input type="text" data-table="remates" data-field="x_video_final_3" name="x<?php echo $remates_list->RowIndex ?>_video_final_3" id="x<?php echo $remates_list->RowIndex ?>_video_final_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_3->EditValue ?>"<?php echo $remates->video_final_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_3" name="o<?php echo $remates_list->RowIndex ?>_video_final_3" id="o<?php echo $remates_list->RowIndex ?>_video_final_3" value="<?php echo ew_HtmlEncode($remates->video_final_3->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_3" class="form-group remates_video_final_3">
<input type="text" data-table="remates" data-field="x_video_final_3" name="x<?php echo $remates_list->RowIndex ?>_video_final_3" id="x<?php echo $remates_list->RowIndex ?>_video_final_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_3->EditValue ?>"<?php echo $remates->video_final_3->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_3" class="remates_video_final_3">
<span<?php echo $remates->video_final_3->ViewAttributes() ?>>
<?php echo $remates->video_final_3->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
		<td data-name="video_titu_3"<?php echo $remates->video_titu_3->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_3" class="form-group remates_video_titu_3">
<input type="text" data-table="remates" data-field="x_video_titu_3" name="x<?php echo $remates_list->RowIndex ?>_video_titu_3" id="x<?php echo $remates_list->RowIndex ?>_video_titu_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_3->EditValue ?>"<?php echo $remates->video_titu_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_3" name="o<?php echo $remates_list->RowIndex ?>_video_titu_3" id="o<?php echo $remates_list->RowIndex ?>_video_titu_3" value="<?php echo ew_HtmlEncode($remates->video_titu_3->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_3" class="form-group remates_video_titu_3">
<input type="text" data-table="remates" data-field="x_video_titu_3" name="x<?php echo $remates_list->RowIndex ?>_video_titu_3" id="x<?php echo $remates_list->RowIndex ?>_video_titu_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_3->EditValue ?>"<?php echo $remates->video_titu_3->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_3" class="remates_video_titu_3">
<span<?php echo $remates->video_titu_3->ViewAttributes() ?>>
<?php echo $remates->video_titu_3->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
		<td data-name="video_final_4"<?php echo $remates->video_final_4->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_4" class="form-group remates_video_final_4">
<input type="text" data-table="remates" data-field="x_video_final_4" name="x<?php echo $remates_list->RowIndex ?>_video_final_4" id="x<?php echo $remates_list->RowIndex ?>_video_final_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_4->EditValue ?>"<?php echo $remates->video_final_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_4" name="o<?php echo $remates_list->RowIndex ?>_video_final_4" id="o<?php echo $remates_list->RowIndex ?>_video_final_4" value="<?php echo ew_HtmlEncode($remates->video_final_4->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_4" class="form-group remates_video_final_4">
<input type="text" data-table="remates" data-field="x_video_final_4" name="x<?php echo $remates_list->RowIndex ?>_video_final_4" id="x<?php echo $remates_list->RowIndex ?>_video_final_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_4->EditValue ?>"<?php echo $remates->video_final_4->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_4" class="remates_video_final_4">
<span<?php echo $remates->video_final_4->ViewAttributes() ?>>
<?php echo $remates->video_final_4->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
		<td data-name="video_titu_4"<?php echo $remates->video_titu_4->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_4" class="form-group remates_video_titu_4">
<input type="text" data-table="remates" data-field="x_video_titu_4" name="x<?php echo $remates_list->RowIndex ?>_video_titu_4" id="x<?php echo $remates_list->RowIndex ?>_video_titu_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_4->EditValue ?>"<?php echo $remates->video_titu_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_4" name="o<?php echo $remates_list->RowIndex ?>_video_titu_4" id="o<?php echo $remates_list->RowIndex ?>_video_titu_4" value="<?php echo ew_HtmlEncode($remates->video_titu_4->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_4" class="form-group remates_video_titu_4">
<input type="text" data-table="remates" data-field="x_video_titu_4" name="x<?php echo $remates_list->RowIndex ?>_video_titu_4" id="x<?php echo $remates_list->RowIndex ?>_video_titu_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_4->EditValue ?>"<?php echo $remates->video_titu_4->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_4" class="remates_video_titu_4">
<span<?php echo $remates->video_titu_4->ViewAttributes() ?>>
<?php echo $remates->video_titu_4->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
		<td data-name="video_final_5"<?php echo $remates->video_final_5->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_5" class="form-group remates_video_final_5">
<input type="text" data-table="remates" data-field="x_video_final_5" name="x<?php echo $remates_list->RowIndex ?>_video_final_5" id="x<?php echo $remates_list->RowIndex ?>_video_final_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_5->EditValue ?>"<?php echo $remates->video_final_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_5" name="o<?php echo $remates_list->RowIndex ?>_video_final_5" id="o<?php echo $remates_list->RowIndex ?>_video_final_5" value="<?php echo ew_HtmlEncode($remates->video_final_5->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_5" class="form-group remates_video_final_5">
<input type="text" data-table="remates" data-field="x_video_final_5" name="x<?php echo $remates_list->RowIndex ?>_video_final_5" id="x<?php echo $remates_list->RowIndex ?>_video_final_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_5->EditValue ?>"<?php echo $remates->video_final_5->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_final_5" class="remates_video_final_5">
<span<?php echo $remates->video_final_5->ViewAttributes() ?>>
<?php echo $remates->video_final_5->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
		<td data-name="video_titu_5"<?php echo $remates->video_titu_5->CellAttributes() ?>>
<?php if ($remates->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_5" class="form-group remates_video_titu_5">
<input type="text" data-table="remates" data-field="x_video_titu_5" name="x<?php echo $remates_list->RowIndex ?>_video_titu_5" id="x<?php echo $remates_list->RowIndex ?>_video_titu_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_5->EditValue ?>"<?php echo $remates->video_titu_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_5" name="o<?php echo $remates_list->RowIndex ?>_video_titu_5" id="o<?php echo $remates_list->RowIndex ?>_video_titu_5" value="<?php echo ew_HtmlEncode($remates->video_titu_5->OldValue) ?>">
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_5" class="form-group remates_video_titu_5">
<input type="text" data-table="remates" data-field="x_video_titu_5" name="x<?php echo $remates_list->RowIndex ?>_video_titu_5" id="x<?php echo $remates_list->RowIndex ?>_video_titu_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_5->EditValue ?>"<?php echo $remates->video_titu_5->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($remates->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $remates_list->RowCnt ?>_remates_video_titu_5" class="remates_video_titu_5">
<span<?php echo $remates->video_titu_5->ViewAttributes() ?>>
<?php echo $remates->video_titu_5->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$remates_list->ListOptions->Render("body", "right", $remates_list->RowCnt);
?>
	</tr>
<?php if ($remates->RowType == EW_ROWTYPE_ADD || $remates->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fremateslist.UpdateOpts(<?php echo $remates_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($remates->CurrentAction <> "gridadd")
		if (!$remates_list->Recordset->EOF) $remates_list->Recordset->MoveNext();
}
?>
<?php
	if ($remates->CurrentAction == "gridadd" || $remates->CurrentAction == "gridedit") {
		$remates_list->RowIndex = '$rowindex$';
		$remates_list->LoadDefaultValues();

		// Set row properties
		$remates->ResetAttrs();
		$remates->RowAttrs = array_merge($remates->RowAttrs, array('data-rowindex'=>$remates_list->RowIndex, 'id'=>'r0_remates', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($remates->RowAttrs["class"], "ewTemplate");
		$remates->RowType = EW_ROWTYPE_ADD;

		// Render row
		$remates_list->RenderRow();

		// Render list options
		$remates_list->RenderListOptions();
		$remates_list->StartRowCnt = 0;
?>
	<tr<?php echo $remates->RowAttributes() ?>>
<?php

// Render list options (body, left)
$remates_list->ListOptions->Render("body", "left", $remates_list->RowIndex);
?>
	<?php if ($remates->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="remates" data-field="x_id" name="o<?php echo $remates_list->RowIndex ?>_id" id="o<?php echo $remates_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($remates->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->tipo->Visible) { // tipo ?>
		<td data-name="tipo">
<span id="el$rowindex$_remates_tipo" class="form-group remates_tipo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_tipo" id="x<?php echo $remates_list->RowIndex ?>_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_tipo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_tipo" name="o<?php echo $remates_list->RowIndex ?>_tipo" id="o<?php echo $remates_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($remates->tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->plataforma->Visible) { // plataforma ?>
		<td data-name="plataforma">
<span id="el$rowindex$_remates_plataforma" class="form-group remates_plataforma">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_plataforma" name="x<?php echo $remates_list->RowIndex ?>_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_plataforma") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_plataforma" name="o<?php echo $remates_list->RowIndex ?>_plataforma" id="o<?php echo $remates_list->RowIndex ?>_plataforma" value="<?php echo ew_HtmlEncode($remates->plataforma->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->ext_id->Visible) { // ext_id ?>
		<td data-name="ext_id">
<span id="el$rowindex$_remates_ext_id" class="form-group remates_ext_id">
<input type="text" data-table="remates" data-field="x_ext_id" name="x<?php echo $remates_list->RowIndex ?>_ext_id" id="x<?php echo $remates_list->RowIndex ?>_ext_id" size="45" placeholder="<?php echo ew_HtmlEncode($remates->ext_id->getPlaceHolder()) ?>" value="<?php echo $remates->ext_id->EditValue ?>"<?php echo $remates->ext_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_ext_id" name="o<?php echo $remates_list->RowIndex ?>_ext_id" id="o<?php echo $remates_list->RowIndex ?>_ext_id" value="<?php echo ew_HtmlEncode($remates->ext_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->fecha->Visible) { // fecha ?>
		<td data-name="fecha">
<span id="el$rowindex$_remates_fecha" class="form-group remates_fecha">
<input type="text" data-table="remates" data-field="x_fecha" name="x<?php echo $remates_list->RowIndex ?>_fecha" id="x<?php echo $remates_list->RowIndex ?>_fecha" placeholder="<?php echo ew_HtmlEncode($remates->fecha->getPlaceHolder()) ?>" value="<?php echo $remates->fecha->EditValue ?>"<?php echo $remates->fecha->EditAttributes() ?>>
<?php if (!$remates->fecha->ReadOnly && !$remates->fecha->Disabled && !isset($remates->fecha->EditAttrs["readonly"]) && !isset($remates->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fremateslist", "x<?php echo $remates_list->RowIndex ?>_fecha", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="remates" data-field="x_fecha" name="o<?php echo $remates_list->RowIndex ?>_fecha" id="o<?php echo $remates_list->RowIndex ?>_fecha" value="<?php echo ew_HtmlEncode($remates->fecha->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
		<td data-name="hora_inicio">
<span id="el$rowindex$_remates_hora_inicio" class="form-group remates_hora_inicio">
<input type="text" data-table="remates" data-field="x_hora_inicio" name="x<?php echo $remates_list->RowIndex ?>_hora_inicio" id="x<?php echo $remates_list->RowIndex ?>_hora_inicio" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_inicio->getPlaceHolder()) ?>" value="<?php echo $remates->hora_inicio->EditValue ?>"<?php echo $remates->hora_inicio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_inicio" name="o<?php echo $remates_list->RowIndex ?>_hora_inicio" id="o<?php echo $remates_list->RowIndex ?>_hora_inicio" value="<?php echo ew_HtmlEncode($remates->hora_inicio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
		<td data-name="hora_fin">
<span id="el$rowindex$_remates_hora_fin" class="form-group remates_hora_fin">
<input type="text" data-table="remates" data-field="x_hora_fin" name="x<?php echo $remates_list->RowIndex ?>_hora_fin" id="x<?php echo $remates_list->RowIndex ?>_hora_fin" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_fin->getPlaceHolder()) ?>" value="<?php echo $remates->hora_fin->EditValue ?>"<?php echo $remates->hora_fin->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_hora_fin" name="o<?php echo $remates_list->RowIndex ?>_hora_fin" id="o<?php echo $remates_list->RowIndex ?>_hora_fin" value="<?php echo ew_HtmlEncode($remates->hora_fin->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->zona->Visible) { // zona ?>
		<td data-name="zona">
<span id="el$rowindex$_remates_zona" class="form-group remates_zona">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_zona" name="x<?php echo $remates_list->RowIndex ?>_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_zona") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_zona" name="o<?php echo $remates_list->RowIndex ?>_zona" id="o<?php echo $remates_list->RowIndex ?>_zona" value="<?php echo ew_HtmlEncode($remates->zona->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->lugar->Visible) { // lugar ?>
		<td data-name="lugar">
<span id="el$rowindex$_remates_lugar" class="form-group remates_lugar">
<input type="text" data-table="remates" data-field="x_lugar" name="x<?php echo $remates_list->RowIndex ?>_lugar" id="x<?php echo $remates_list->RowIndex ?>_lugar" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->lugar->getPlaceHolder()) ?>" value="<?php echo $remates->lugar->EditValue ?>"<?php echo $remates->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_lugar" name="o<?php echo $remates_list->RowIndex ?>_lugar" id="o<?php echo $remates_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($remates->lugar->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->locacion->Visible) { // locacion ?>
		<td data-name="locacion">
<span id="el$rowindex$_remates_locacion" class="form-group remates_locacion">
<input type="text" data-table="remates" data-field="x_locacion" name="x<?php echo $remates_list->RowIndex ?>_locacion" id="x<?php echo $remates_list->RowIndex ?>_locacion" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->locacion->getPlaceHolder()) ?>" value="<?php echo $remates->locacion->EditValue ?>"<?php echo $remates->locacion->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_locacion" name="o<?php echo $remates_list->RowIndex ?>_locacion" id="o<?php echo $remates_list->RowIndex ?>_locacion" value="<?php echo ew_HtmlEncode($remates->locacion->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->numero->Visible) { // numero ?>
		<td data-name="numero">
<span id="el$rowindex$_remates_numero" class="form-group remates_numero">
<input type="text" data-table="remates" data-field="x_numero" name="x<?php echo $remates_list->RowIndex ?>_numero" id="x<?php echo $remates_list->RowIndex ?>_numero" size="30" placeholder="<?php echo ew_HtmlEncode($remates->numero->getPlaceHolder()) ?>" value="<?php echo $remates->numero->EditValue ?>"<?php echo $remates->numero->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_numero" name="o<?php echo $remates_list->RowIndex ?>_numero" id="o<?php echo $remates_list->RowIndex ?>_numero" value="<?php echo ew_HtmlEncode($remates->numero->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->titulo->Visible) { // titulo ?>
		<td data-name="titulo">
<span id="el$rowindex$_remates_titulo" class="form-group remates_titulo">
<input type="text" data-table="remates" data-field="x_titulo" name="x<?php echo $remates_list->RowIndex ?>_titulo" id="x<?php echo $remates_list->RowIndex ?>_titulo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->titulo->getPlaceHolder()) ?>" value="<?php echo $remates->titulo->EditValue ?>"<?php echo $remates->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_titulo" name="o<?php echo $remates_list->RowIndex ?>_titulo" id="o<?php echo $remates_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($remates->titulo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad">
<span id="el$rowindex$_remates_cantidad" class="form-group remates_cantidad">
<input type="text" data-table="remates" data-field="x_cantidad" name="x<?php echo $remates_list->RowIndex ?>_cantidad" id="x<?php echo $remates_list->RowIndex ?>_cantidad" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($remates->cantidad->getPlaceHolder()) ?>" value="<?php echo $remates->cantidad->EditValue ?>"<?php echo $remates->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_cantidad" name="o<?php echo $remates_list->RowIndex ?>_cantidad" id="o<?php echo $remates_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($remates->cantidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
		<td data-name="yacare_visible">
<span id="el$rowindex$_remates_yacare_visible" class="form-group remates_yacare_visible">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_yacare_visible" name="x<?php echo $remates_list->RowIndex ?>_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_yacare_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_yacare_visible" name="o<?php echo $remates_list->RowIndex ?>_yacare_visible" id="o<?php echo $remates_list->RowIndex ?>_yacare_visible" value="<?php echo ew_HtmlEncode($remates->yacare_visible->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g->Visible) { // banner_g ?>
		<td data-name="banner_g">
<span id="el$rowindex$_remates_banner_g" class="form-group remates_banner_g">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_banner_g">
<span title="<?php echo $remates->banner_g->FldTitle() ? $remates->banner_g->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_g->ReadOnly || $remates->banner_g->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_g" name="x<?php echo $remates_list->RowIndex ?>_banner_g" id="x<?php echo $remates_list->RowIndex ?>_banner_g"<?php echo $remates->banner_g->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fn_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fa_x<?php echo $remates_list->RowIndex ?>_banner_g" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fs_x<?php echo $remates_list->RowIndex ?>_banner_g" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fx_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_banner_g" id= "fm_x<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo $remates->banner_g->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_banner_g" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g" name="o<?php echo $remates_list->RowIndex ?>_banner_g" id="o<?php echo $remates_list->RowIndex ?>_banner_g" value="<?php echo ew_HtmlEncode($remates->banner_g->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
		<td data-name="banner_g_visible">
<span id="el$rowindex$_remates_banner_g_visible" class="form-group remates_banner_g_visible">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_banner_g_visible" name="x<?php echo $remates_list->RowIndex ?>_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_banner_g_visible") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_visible" name="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" id="o<?php echo $remates_list->RowIndex ?>_banner_g_visible" value="<?php echo ew_HtmlEncode($remates->banner_g_visible->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
		<td data-name="banner_g_z_index">
<span id="el$rowindex$_remates_banner_g_z_index" class="form-group remates_banner_g_z_index">
<input type="text" data-table="remates" data-field="x_banner_g_z_index" name="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="x<?php echo $remates_list->RowIndex ?>_banner_g_z_index" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_z_index->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_z_index->EditValue ?>"<?php echo $remates->banner_g_z_index->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_z_index" name="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" id="o<?php echo $remates_list->RowIndex ?>_banner_g_z_index" value="<?php echo ew_HtmlEncode($remates->banner_g_z_index->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
		<td data-name="banner_g_pos_x">
<span id="el$rowindex$_remates_banner_g_pos_x" class="form-group remates_banner_g_pos_x">
<input type="text" data-table="remates" data-field="x_banner_g_pos_x" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_x->EditValue ?>"<?php echo $remates->banner_g_pos_x->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_x" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_x" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
		<td data-name="banner_g_pos_y">
<span id="el$rowindex$_remates_banner_g_pos_y" class="form-group remates_banner_g_pos_y">
<input type="text" data-table="remates" data-field="x_banner_g_pos_y" name="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="x<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_y->EditValue ?>"<?php echo $remates->banner_g_pos_y->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_pos_y" name="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" id="o<?php echo $remates_list->RowIndex ?>_banner_g_pos_y" value="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
		<td data-name="banner_g_link">
<span id="el$rowindex$_remates_banner_g_link" class="form-group remates_banner_g_link">
<input type="text" data-table="remates" data-field="x_banner_g_link" name="x<?php echo $remates_list->RowIndex ?>_banner_g_link" id="x<?php echo $remates_list->RowIndex ?>_banner_g_link" size="50" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_link->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_link->EditValue ?>"<?php echo $remates->banner_g_link->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_banner_g_link" name="o<?php echo $remates_list->RowIndex ?>_banner_g_link" id="o<?php echo $remates_list->RowIndex ?>_banner_g_link" value="<?php echo ew_HtmlEncode($remates->banner_g_link->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->activo->Visible) { // activo ?>
		<td data-name="activo">
<span id="el$rowindex$_remates_activo" class="form-group remates_activo">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_activo" id="x<?php echo $remates_list->RowIndex ?>_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_activo") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_activo" name="o<?php echo $remates_list->RowIndex ?>_activo" id="o<?php echo $remates_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($remates->activo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->archivo->Visible) { // archivo ?>
		<td data-name="archivo">
<span id="el$rowindex$_remates_archivo" class="form-group remates_archivo">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_archivo">
<span title="<?php echo $remates->archivo->FldTitle() ? $remates->archivo->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->archivo->ReadOnly || $remates->archivo->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_archivo" name="x<?php echo $remates_list->RowIndex ?>_archivo" id="x<?php echo $remates_list->RowIndex ?>_archivo"<?php echo $remates->archivo->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fn_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fa_x<?php echo $remates_list->RowIndex ?>_archivo" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fs_x<?php echo $remates_list->RowIndex ?>_archivo" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fx_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_archivo" id= "fm_x<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo $remates->archivo->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_archivo" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_archivo" name="o<?php echo $remates_list->RowIndex ?>_archivo" id="o<?php echo $remates_list->RowIndex ?>_archivo" value="<?php echo ew_HtmlEncode($remates->archivo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->precios->Visible) { // precios ?>
		<td data-name="precios">
<span id="el$rowindex$_remates_precios" class="form-group remates_precios">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $remates_list->RowIndex ?>_precios" name="x<?php echo $remates_list->RowIndex ?>_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x<?php echo $remates_list->RowIndex ?>_precios") ?>
</select>
</span>
<input type="hidden" data-table="remates" data-field="x_precios" name="o<?php echo $remates_list->RowIndex ?>_precios" id="o<?php echo $remates_list->RowIndex ?>_precios" value="<?php echo ew_HtmlEncode($remates->precios->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
		<td data-name="mapa_img">
<span id="el$rowindex$_remates_mapa_img" class="form-group remates_mapa_img">
<div id="fd_x<?php echo $remates_list->RowIndex ?>_mapa_img">
<span title="<?php echo $remates->mapa_img->FldTitle() ? $remates->mapa_img->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->mapa_img->ReadOnly || $remates->mapa_img->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_mapa_img" name="x<?php echo $remates_list->RowIndex ?>_mapa_img" id="x<?php echo $remates_list->RowIndex ?>_mapa_img"<?php echo $remates->mapa_img->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fn_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fa_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="0">
<input type="hidden" name="fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fs_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="255">
<input type="hidden" name="fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fx_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" id= "fm_x<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo $remates->mapa_img->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $remates_list->RowIndex ?>_mapa_img" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="remates" data-field="x_mapa_img" name="o<?php echo $remates_list->RowIndex ?>_mapa_img" id="o<?php echo $remates_list->RowIndex ?>_mapa_img" value="<?php echo ew_HtmlEncode($remates->mapa_img->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
		<td data-name="video_final_servidor">
<span id="el$rowindex$_remates_video_final_servidor" class="form-group remates_video_final_servidor">
<div id="tp_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="x<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $remates_list->RowIndex ?>_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x{$remates_list->RowIndex}_video_final_servidor") ?>
</div></div>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_servidor" name="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" id="o<?php echo $remates_list->RowIndex ?>_video_final_servidor" value="<?php echo ew_HtmlEncode($remates->video_final_servidor->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
		<td data-name="video_final_1">
<span id="el$rowindex$_remates_video_final_1" class="form-group remates_video_final_1">
<input type="text" data-table="remates" data-field="x_video_final_1" name="x<?php echo $remates_list->RowIndex ?>_video_final_1" id="x<?php echo $remates_list->RowIndex ?>_video_final_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_1->EditValue ?>"<?php echo $remates->video_final_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_1" name="o<?php echo $remates_list->RowIndex ?>_video_final_1" id="o<?php echo $remates_list->RowIndex ?>_video_final_1" value="<?php echo ew_HtmlEncode($remates->video_final_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
		<td data-name="video_titu_1">
<span id="el$rowindex$_remates_video_titu_1" class="form-group remates_video_titu_1">
<input type="text" data-table="remates" data-field="x_video_titu_1" name="x<?php echo $remates_list->RowIndex ?>_video_titu_1" id="x<?php echo $remates_list->RowIndex ?>_video_titu_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_1->EditValue ?>"<?php echo $remates->video_titu_1->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_1" name="o<?php echo $remates_list->RowIndex ?>_video_titu_1" id="o<?php echo $remates_list->RowIndex ?>_video_titu_1" value="<?php echo ew_HtmlEncode($remates->video_titu_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
		<td data-name="video_final_2">
<span id="el$rowindex$_remates_video_final_2" class="form-group remates_video_final_2">
<input type="text" data-table="remates" data-field="x_video_final_2" name="x<?php echo $remates_list->RowIndex ?>_video_final_2" id="x<?php echo $remates_list->RowIndex ?>_video_final_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_2->EditValue ?>"<?php echo $remates->video_final_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_2" name="o<?php echo $remates_list->RowIndex ?>_video_final_2" id="o<?php echo $remates_list->RowIndex ?>_video_final_2" value="<?php echo ew_HtmlEncode($remates->video_final_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
		<td data-name="video_titu_2">
<span id="el$rowindex$_remates_video_titu_2" class="form-group remates_video_titu_2">
<input type="text" data-table="remates" data-field="x_video_titu_2" name="x<?php echo $remates_list->RowIndex ?>_video_titu_2" id="x<?php echo $remates_list->RowIndex ?>_video_titu_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_2->EditValue ?>"<?php echo $remates->video_titu_2->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_2" name="o<?php echo $remates_list->RowIndex ?>_video_titu_2" id="o<?php echo $remates_list->RowIndex ?>_video_titu_2" value="<?php echo ew_HtmlEncode($remates->video_titu_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
		<td data-name="video_final_3">
<span id="el$rowindex$_remates_video_final_3" class="form-group remates_video_final_3">
<input type="text" data-table="remates" data-field="x_video_final_3" name="x<?php echo $remates_list->RowIndex ?>_video_final_3" id="x<?php echo $remates_list->RowIndex ?>_video_final_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_3->EditValue ?>"<?php echo $remates->video_final_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_3" name="o<?php echo $remates_list->RowIndex ?>_video_final_3" id="o<?php echo $remates_list->RowIndex ?>_video_final_3" value="<?php echo ew_HtmlEncode($remates->video_final_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
		<td data-name="video_titu_3">
<span id="el$rowindex$_remates_video_titu_3" class="form-group remates_video_titu_3">
<input type="text" data-table="remates" data-field="x_video_titu_3" name="x<?php echo $remates_list->RowIndex ?>_video_titu_3" id="x<?php echo $remates_list->RowIndex ?>_video_titu_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_3->EditValue ?>"<?php echo $remates->video_titu_3->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_3" name="o<?php echo $remates_list->RowIndex ?>_video_titu_3" id="o<?php echo $remates_list->RowIndex ?>_video_titu_3" value="<?php echo ew_HtmlEncode($remates->video_titu_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
		<td data-name="video_final_4">
<span id="el$rowindex$_remates_video_final_4" class="form-group remates_video_final_4">
<input type="text" data-table="remates" data-field="x_video_final_4" name="x<?php echo $remates_list->RowIndex ?>_video_final_4" id="x<?php echo $remates_list->RowIndex ?>_video_final_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_4->EditValue ?>"<?php echo $remates->video_final_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_4" name="o<?php echo $remates_list->RowIndex ?>_video_final_4" id="o<?php echo $remates_list->RowIndex ?>_video_final_4" value="<?php echo ew_HtmlEncode($remates->video_final_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
		<td data-name="video_titu_4">
<span id="el$rowindex$_remates_video_titu_4" class="form-group remates_video_titu_4">
<input type="text" data-table="remates" data-field="x_video_titu_4" name="x<?php echo $remates_list->RowIndex ?>_video_titu_4" id="x<?php echo $remates_list->RowIndex ?>_video_titu_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_4->EditValue ?>"<?php echo $remates->video_titu_4->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_4" name="o<?php echo $remates_list->RowIndex ?>_video_titu_4" id="o<?php echo $remates_list->RowIndex ?>_video_titu_4" value="<?php echo ew_HtmlEncode($remates->video_titu_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
		<td data-name="video_final_5">
<span id="el$rowindex$_remates_video_final_5" class="form-group remates_video_final_5">
<input type="text" data-table="remates" data-field="x_video_final_5" name="x<?php echo $remates_list->RowIndex ?>_video_final_5" id="x<?php echo $remates_list->RowIndex ?>_video_final_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_5->EditValue ?>"<?php echo $remates->video_final_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_final_5" name="o<?php echo $remates_list->RowIndex ?>_video_final_5" id="o<?php echo $remates_list->RowIndex ?>_video_final_5" value="<?php echo ew_HtmlEncode($remates->video_final_5->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
		<td data-name="video_titu_5">
<span id="el$rowindex$_remates_video_titu_5" class="form-group remates_video_titu_5">
<input type="text" data-table="remates" data-field="x_video_titu_5" name="x<?php echo $remates_list->RowIndex ?>_video_titu_5" id="x<?php echo $remates_list->RowIndex ?>_video_titu_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_5->EditValue ?>"<?php echo $remates->video_titu_5->EditAttributes() ?>>
</span>
<input type="hidden" data-table="remates" data-field="x_video_titu_5" name="o<?php echo $remates_list->RowIndex ?>_video_titu_5" id="o<?php echo $remates_list->RowIndex ?>_video_titu_5" value="<?php echo ew_HtmlEncode($remates->video_titu_5->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$remates_list->ListOptions->Render("body", "right", $remates_list->RowCnt);
?>
<script type="text/javascript">
fremateslist.UpdateOpts(<?php echo $remates_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($remates->CurrentAction == "add" || $remates->CurrentAction == "copy") { ?>
<input type="hidden" name="<?php echo $remates_list->FormKeyCountName ?>" id="<?php echo $remates_list->FormKeyCountName ?>" value="<?php echo $remates_list->KeyCount ?>">
<?php } ?>
<?php if ($remates->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $remates_list->FormKeyCountName ?>" id="<?php echo $remates_list->FormKeyCountName ?>" value="<?php echo $remates_list->KeyCount ?>">
<?php echo $remates_list->MultiSelectKey ?>
<?php } ?>
<?php if ($remates->CurrentAction == "edit") { ?>
<input type="hidden" name="<?php echo $remates_list->FormKeyCountName ?>" id="<?php echo $remates_list->FormKeyCountName ?>" value="<?php echo $remates_list->KeyCount ?>">
<?php } ?>
<?php if ($remates->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $remates_list->FormKeyCountName ?>" id="<?php echo $remates_list->FormKeyCountName ?>" value="<?php echo $remates_list->KeyCount ?>">
<?php echo $remates_list->MultiSelectKey ?>
<?php } ?>
<?php if ($remates->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($remates_list->Recordset)
	$remates_list->Recordset->Close();
?>
<?php if ($remates->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($remates->CurrentAction <> "gridadd" && $remates->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($remates_list->Pager)) $remates_list->Pager = new cPrevNextPager($remates_list->StartRec, $remates_list->DisplayRecs, $remates_list->TotalRecs) ?>
<?php if ($remates_list->Pager->RecordCount > 0 && $remates_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($remates_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($remates_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $remates_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($remates_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($remates_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $remates_list->PageUrl() ?>start=<?php echo $remates_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $remates_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $remates_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $remates_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $remates_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($remates_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($remates_list->TotalRecs == 0 && $remates->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($remates_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">
fremateslistsrch.FilterList = <?php echo $remates_list->GetFilterList() ?>;
fremateslistsrch.Init();
fremateslist.Init();
</script>
<?php } ?>
<?php
$remates_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$remates_list->Page_Terminate();
?>
