<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "propiedadesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$propiedades_delete = NULL; // Initialize page object first

class cpropiedades_delete extends cpropiedades {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{1B8CF9BA-91E4-4950-9047-342B30417538}";

	// Table name
	var $TableName = 'propiedades';

	// Page object name
	var $PageObjName = 'propiedades_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (propiedades)
		if (!isset($GLOBALS["propiedades"]) || get_class($GLOBALS["propiedades"]) == "cpropiedades") {
			$GLOBALS["propiedades"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["propiedades"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'propiedades', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("propiedadeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->prov_id->SetVisibility();
		$this->loca_id->SetVisibility();
		$this->titulo->SetVisibility();
		$this->tipo_id->SetVisibility();
		$this->actividad->SetVisibility();
		$this->oper_tipo->SetVisibility();
		$this->img_1->SetVisibility();
		$this->img_2->SetVisibility();
		$this->img_3->SetVisibility();
		$this->img_4->SetVisibility();
		$this->img_5->SetVisibility();
		$this->img_6->SetVisibility();
		$this->img_7->SetVisibility();
		$this->img_8->SetVisibility();
		$this->img_9->SetVisibility();
		$this->img_10->SetVisibility();
		$this->video_servidor->SetVisibility();
		$this->video->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $propiedades;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($propiedades);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("propiedadeslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in propiedades class, propiedadesinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} elseif (@$_GET["a_delete"] == "1") {
			$this->CurrentAction = "D"; // Delete record directly
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->CurrentAction = "I"; // Display record
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("propiedadeslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->prov_id->setDbValue($rs->fields('prov_id'));
		if (array_key_exists('EV__prov_id', $rs->fields)) {
			$this->prov_id->VirtualValue = $rs->fields('EV__prov_id'); // Set up virtual field value
		} else {
			$this->prov_id->VirtualValue = ""; // Clear value
		}
		$this->loca_id->setDbValue($rs->fields('loca_id'));
		if (array_key_exists('EV__loca_id', $rs->fields)) {
			$this->loca_id->VirtualValue = $rs->fields('EV__loca_id'); // Set up virtual field value
		} else {
			$this->loca_id->VirtualValue = ""; // Clear value
		}
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->tipo_id->setDbValue($rs->fields('tipo_id'));
		$this->actividad->setDbValue($rs->fields('actividad'));
		$this->oper_tipo->setDbValue($rs->fields('oper_tipo'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->img_1->Upload->DbValue = $rs->fields('img_1');
		$this->img_1->CurrentValue = $this->img_1->Upload->DbValue;
		$this->img_2->Upload->DbValue = $rs->fields('img_2');
		$this->img_2->CurrentValue = $this->img_2->Upload->DbValue;
		$this->img_3->Upload->DbValue = $rs->fields('img_3');
		$this->img_3->CurrentValue = $this->img_3->Upload->DbValue;
		$this->img_4->Upload->DbValue = $rs->fields('img_4');
		$this->img_4->CurrentValue = $this->img_4->Upload->DbValue;
		$this->img_5->Upload->DbValue = $rs->fields('img_5');
		$this->img_5->CurrentValue = $this->img_5->Upload->DbValue;
		$this->img_6->Upload->DbValue = $rs->fields('img_6');
		$this->img_6->CurrentValue = $this->img_6->Upload->DbValue;
		$this->img_7->Upload->DbValue = $rs->fields('img_7');
		$this->img_7->CurrentValue = $this->img_7->Upload->DbValue;
		$this->img_8->Upload->DbValue = $rs->fields('img_8');
		$this->img_8->CurrentValue = $this->img_8->Upload->DbValue;
		$this->img_9->Upload->DbValue = $rs->fields('img_9');
		$this->img_9->CurrentValue = $this->img_9->Upload->DbValue;
		$this->img_10->Upload->DbValue = $rs->fields('img_10');
		$this->img_10->CurrentValue = $this->img_10->Upload->DbValue;
		$this->video_servidor->setDbValue($rs->fields('video_servidor'));
		$this->video->setDbValue($rs->fields('video'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->prov_id->DbValue = $row['prov_id'];
		$this->loca_id->DbValue = $row['loca_id'];
		$this->titulo->DbValue = $row['titulo'];
		$this->tipo_id->DbValue = $row['tipo_id'];
		$this->actividad->DbValue = $row['actividad'];
		$this->oper_tipo->DbValue = $row['oper_tipo'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->img_1->Upload->DbValue = $row['img_1'];
		$this->img_2->Upload->DbValue = $row['img_2'];
		$this->img_3->Upload->DbValue = $row['img_3'];
		$this->img_4->Upload->DbValue = $row['img_4'];
		$this->img_5->Upload->DbValue = $row['img_5'];
		$this->img_6->Upload->DbValue = $row['img_6'];
		$this->img_7->Upload->DbValue = $row['img_7'];
		$this->img_8->Upload->DbValue = $row['img_8'];
		$this->img_9->Upload->DbValue = $row['img_9'];
		$this->img_10->Upload->DbValue = $row['img_10'];
		$this->video_servidor->DbValue = $row['video_servidor'];
		$this->video->DbValue = $row['video'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// prov_id
		// loca_id
		// titulo
		// tipo_id
		// actividad
		// oper_tipo
		// descripcion
		// img_1
		// img_2
		// img_3
		// img_4
		// img_5
		// img_6
		// img_7
		// img_8
		// img_9
		// img_10
		// video_servidor
		// video

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		if ($this->prov_id->VirtualValue <> "") {
			$this->prov_id->ViewValue = $this->prov_id->VirtualValue;
		} else {
		if (strval($this->prov_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->prov_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prov_id->ViewValue = $this->prov_id->CurrentValue;
			}
		} else {
			$this->prov_id->ViewValue = NULL;
		}
		}
		$this->prov_id->ViewCustomAttributes = "";

		// loca_id
		if ($this->loca_id->VirtualValue <> "") {
			$this->loca_id->ViewValue = $this->loca_id->VirtualValue;
		} else {
		if (strval($this->loca_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->loca_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->loca_id->ViewValue = $this->loca_id->CurrentValue;
			}
		} else {
			$this->loca_id->ViewValue = NULL;
		}
		}
		$this->loca_id->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// tipo_id
		$this->tipo_id->ViewValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->ViewCustomAttributes = "";

		// actividad
		if (strval($this->actividad->CurrentValue) <> "") {
			$this->actividad->ViewValue = $this->actividad->OptionCaption($this->actividad->CurrentValue);
		} else {
			$this->actividad->ViewValue = NULL;
		}
		$this->actividad->ViewCustomAttributes = "";

		// oper_tipo
		if (strval($this->oper_tipo->CurrentValue) <> "") {
			$this->oper_tipo->ViewValue = $this->oper_tipo->OptionCaption($this->oper_tipo->CurrentValue);
		} else {
			$this->oper_tipo->ViewValue = NULL;
		}
		$this->oper_tipo->ViewCustomAttributes = "";

		// img_1
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->ViewValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->ViewValue = "";
		}
		$this->img_1->ViewCustomAttributes = "";

		// img_2
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->ViewValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->ViewValue = "";
		}
		$this->img_2->ViewCustomAttributes = "";

		// img_3
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->ViewValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->ViewValue = "";
		}
		$this->img_3->ViewCustomAttributes = "";

		// img_4
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->ViewValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->ViewValue = "";
		}
		$this->img_4->ViewCustomAttributes = "";

		// img_5
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->ViewValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->ViewValue = "";
		}
		$this->img_5->ViewCustomAttributes = "";

		// img_6
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->ViewValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->ViewValue = "";
		}
		$this->img_6->ViewCustomAttributes = "";

		// img_7
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->ViewValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->ViewValue = "";
		}
		$this->img_7->ViewCustomAttributes = "";

		// img_8
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->ViewValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->ViewValue = "";
		}
		$this->img_8->ViewCustomAttributes = "";

		// img_9
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->ViewValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->ViewValue = "";
		}
		$this->img_9->ViewCustomAttributes = "";

		// img_10
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->ViewValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->ViewValue = "";
		}
		$this->img_10->ViewCustomAttributes = "";

		// video_servidor
		if (strval($this->video_servidor->CurrentValue) <> "") {
			$this->video_servidor->ViewValue = $this->video_servidor->OptionCaption($this->video_servidor->CurrentValue);
		} else {
			$this->video_servidor->ViewValue = NULL;
		}
		$this->video_servidor->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// prov_id
			$this->prov_id->LinkCustomAttributes = "";
			$this->prov_id->HrefValue = "";
			$this->prov_id->TooltipValue = "";

			// loca_id
			$this->loca_id->LinkCustomAttributes = "";
			$this->loca_id->HrefValue = "";
			$this->loca_id->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// tipo_id
			$this->tipo_id->LinkCustomAttributes = "";
			$this->tipo_id->HrefValue = "";
			$this->tipo_id->TooltipValue = "";

			// actividad
			$this->actividad->LinkCustomAttributes = "";
			$this->actividad->HrefValue = "";
			$this->actividad->TooltipValue = "";

			// oper_tipo
			$this->oper_tipo->LinkCustomAttributes = "";
			$this->oper_tipo->HrefValue = "";
			$this->oper_tipo->TooltipValue = "";

			// img_1
			$this->img_1->LinkCustomAttributes = "";
			$this->img_1->HrefValue = "";
			$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;
			$this->img_1->TooltipValue = "";

			// img_2
			$this->img_2->LinkCustomAttributes = "";
			$this->img_2->HrefValue = "";
			$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;
			$this->img_2->TooltipValue = "";

			// img_3
			$this->img_3->LinkCustomAttributes = "";
			$this->img_3->HrefValue = "";
			$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;
			$this->img_3->TooltipValue = "";

			// img_4
			$this->img_4->LinkCustomAttributes = "";
			$this->img_4->HrefValue = "";
			$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;
			$this->img_4->TooltipValue = "";

			// img_5
			$this->img_5->LinkCustomAttributes = "";
			$this->img_5->HrefValue = "";
			$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;
			$this->img_5->TooltipValue = "";

			// img_6
			$this->img_6->LinkCustomAttributes = "";
			$this->img_6->HrefValue = "";
			$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;
			$this->img_6->TooltipValue = "";

			// img_7
			$this->img_7->LinkCustomAttributes = "";
			$this->img_7->HrefValue = "";
			$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;
			$this->img_7->TooltipValue = "";

			// img_8
			$this->img_8->LinkCustomAttributes = "";
			$this->img_8->HrefValue = "";
			$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;
			$this->img_8->TooltipValue = "";

			// img_9
			$this->img_9->LinkCustomAttributes = "";
			$this->img_9->HrefValue = "";
			$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;
			$this->img_9->TooltipValue = "";

			// img_10
			$this->img_10->LinkCustomAttributes = "";
			$this->img_10->HrefValue = "";
			$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;
			$this->img_10->TooltipValue = "";

			// video_servidor
			$this->video_servidor->LinkCustomAttributes = "";
			$this->video_servidor->HrefValue = "";
			$this->video_servidor->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("propiedadeslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($propiedades_delete)) $propiedades_delete = new cpropiedades_delete();

// Page init
$propiedades_delete->Page_Init();

// Page main
$propiedades_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$propiedades_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fpropiedadesdelete = new ew_Form("fpropiedadesdelete", "delete");

// Form_CustomValidate event
fpropiedadesdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fpropiedadesdelete.ValidateRequired = true;
<?php } else { ?>
fpropiedadesdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fpropiedadesdelete.Lists["x_prov_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_loca_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
fpropiedadesdelete.Lists["x_loca_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"localidades"};
fpropiedadesdelete.Lists["x_actividad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesdelete.Lists["x_actividad"].Options = <?php echo json_encode($propiedades->actividad->Options()) ?>;
fpropiedadesdelete.Lists["x_oper_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesdelete.Lists["x_oper_tipo"].Options = <?php echo json_encode($propiedades->oper_tipo->Options()) ?>;
fpropiedadesdelete.Lists["x_video_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesdelete.Lists["x_video_servidor"].Options = <?php echo json_encode($propiedades->video_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $propiedades_delete->ShowPageHeader(); ?>
<?php
$propiedades_delete->ShowMessage();
?>
<form name="fpropiedadesdelete" id="fpropiedadesdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($propiedades_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $propiedades_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="propiedades">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($propiedades_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $propiedades->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($propiedades->id->Visible) { // id ?>
		<th><span id="elh_propiedades_id" class="propiedades_id"><?php echo $propiedades->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
		<th><span id="elh_propiedades_prov_id" class="propiedades_prov_id"><?php echo $propiedades->prov_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
		<th><span id="elh_propiedades_loca_id" class="propiedades_loca_id"><?php echo $propiedades->loca_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->titulo->Visible) { // titulo ?>
		<th><span id="elh_propiedades_titulo" class="propiedades_titulo"><?php echo $propiedades->titulo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
		<th><span id="elh_propiedades_tipo_id" class="propiedades_tipo_id"><?php echo $propiedades->tipo_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->actividad->Visible) { // actividad ?>
		<th><span id="elh_propiedades_actividad" class="propiedades_actividad"><?php echo $propiedades->actividad->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
		<th><span id="elh_propiedades_oper_tipo" class="propiedades_oper_tipo"><?php echo $propiedades->oper_tipo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_1->Visible) { // img_1 ?>
		<th><span id="elh_propiedades_img_1" class="propiedades_img_1"><?php echo $propiedades->img_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_2->Visible) { // img_2 ?>
		<th><span id="elh_propiedades_img_2" class="propiedades_img_2"><?php echo $propiedades->img_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_3->Visible) { // img_3 ?>
		<th><span id="elh_propiedades_img_3" class="propiedades_img_3"><?php echo $propiedades->img_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_4->Visible) { // img_4 ?>
		<th><span id="elh_propiedades_img_4" class="propiedades_img_4"><?php echo $propiedades->img_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_5->Visible) { // img_5 ?>
		<th><span id="elh_propiedades_img_5" class="propiedades_img_5"><?php echo $propiedades->img_5->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_6->Visible) { // img_6 ?>
		<th><span id="elh_propiedades_img_6" class="propiedades_img_6"><?php echo $propiedades->img_6->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_7->Visible) { // img_7 ?>
		<th><span id="elh_propiedades_img_7" class="propiedades_img_7"><?php echo $propiedades->img_7->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_8->Visible) { // img_8 ?>
		<th><span id="elh_propiedades_img_8" class="propiedades_img_8"><?php echo $propiedades->img_8->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_9->Visible) { // img_9 ?>
		<th><span id="elh_propiedades_img_9" class="propiedades_img_9"><?php echo $propiedades->img_9->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->img_10->Visible) { // img_10 ?>
		<th><span id="elh_propiedades_img_10" class="propiedades_img_10"><?php echo $propiedades->img_10->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
		<th><span id="elh_propiedades_video_servidor" class="propiedades_video_servidor"><?php echo $propiedades->video_servidor->FldCaption() ?></span></th>
<?php } ?>
<?php if ($propiedades->video->Visible) { // video ?>
		<th><span id="elh_propiedades_video" class="propiedades_video"><?php echo $propiedades->video->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$propiedades_delete->RecCnt = 0;
$i = 0;
while (!$propiedades_delete->Recordset->EOF) {
	$propiedades_delete->RecCnt++;
	$propiedades_delete->RowCnt++;

	// Set row properties
	$propiedades->ResetAttrs();
	$propiedades->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$propiedades_delete->LoadRowValues($propiedades_delete->Recordset);

	// Render row
	$propiedades_delete->RenderRow();
?>
	<tr<?php echo $propiedades->RowAttributes() ?>>
<?php if ($propiedades->id->Visible) { // id ?>
		<td<?php echo $propiedades->id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_id" class="propiedades_id">
<span<?php echo $propiedades->id->ViewAttributes() ?>>
<?php echo $propiedades->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
		<td<?php echo $propiedades->prov_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_prov_id" class="propiedades_prov_id">
<span<?php echo $propiedades->prov_id->ViewAttributes() ?>>
<?php echo $propiedades->prov_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
		<td<?php echo $propiedades->loca_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_loca_id" class="propiedades_loca_id">
<span<?php echo $propiedades->loca_id->ViewAttributes() ?>>
<?php echo $propiedades->loca_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->titulo->Visible) { // titulo ?>
		<td<?php echo $propiedades->titulo->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_titulo" class="propiedades_titulo">
<span<?php echo $propiedades->titulo->ViewAttributes() ?>>
<?php echo $propiedades->titulo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
		<td<?php echo $propiedades->tipo_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_tipo_id" class="propiedades_tipo_id">
<span<?php echo $propiedades->tipo_id->ViewAttributes() ?>>
<?php echo $propiedades->tipo_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->actividad->Visible) { // actividad ?>
		<td<?php echo $propiedades->actividad->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_actividad" class="propiedades_actividad">
<span<?php echo $propiedades->actividad->ViewAttributes() ?>>
<?php echo $propiedades->actividad->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
		<td<?php echo $propiedades->oper_tipo->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_oper_tipo" class="propiedades_oper_tipo">
<span<?php echo $propiedades->oper_tipo->ViewAttributes() ?>>
<?php echo $propiedades->oper_tipo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_1->Visible) { // img_1 ?>
		<td<?php echo $propiedades->img_1->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_1" class="propiedades_img_1">
<span<?php echo $propiedades->img_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_1, $propiedades->img_1->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_2->Visible) { // img_2 ?>
		<td<?php echo $propiedades->img_2->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_2" class="propiedades_img_2">
<span<?php echo $propiedades->img_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_2, $propiedades->img_2->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_3->Visible) { // img_3 ?>
		<td<?php echo $propiedades->img_3->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_3" class="propiedades_img_3">
<span<?php echo $propiedades->img_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_3, $propiedades->img_3->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_4->Visible) { // img_4 ?>
		<td<?php echo $propiedades->img_4->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_4" class="propiedades_img_4">
<span<?php echo $propiedades->img_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_4, $propiedades->img_4->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_5->Visible) { // img_5 ?>
		<td<?php echo $propiedades->img_5->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_5" class="propiedades_img_5">
<span<?php echo $propiedades->img_5->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_5, $propiedades->img_5->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_6->Visible) { // img_6 ?>
		<td<?php echo $propiedades->img_6->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_6" class="propiedades_img_6">
<span<?php echo $propiedades->img_6->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_6, $propiedades->img_6->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_7->Visible) { // img_7 ?>
		<td<?php echo $propiedades->img_7->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_7" class="propiedades_img_7">
<span<?php echo $propiedades->img_7->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_7, $propiedades->img_7->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_8->Visible) { // img_8 ?>
		<td<?php echo $propiedades->img_8->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_8" class="propiedades_img_8">
<span<?php echo $propiedades->img_8->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_8, $propiedades->img_8->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_9->Visible) { // img_9 ?>
		<td<?php echo $propiedades->img_9->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_9" class="propiedades_img_9">
<span<?php echo $propiedades->img_9->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_9, $propiedades->img_9->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->img_10->Visible) { // img_10 ?>
		<td<?php echo $propiedades->img_10->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_img_10" class="propiedades_img_10">
<span<?php echo $propiedades->img_10->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_10, $propiedades->img_10->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
		<td<?php echo $propiedades->video_servidor->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_video_servidor" class="propiedades_video_servidor">
<span<?php echo $propiedades->video_servidor->ViewAttributes() ?>>
<?php echo $propiedades->video_servidor->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($propiedades->video->Visible) { // video ?>
		<td<?php echo $propiedades->video->CellAttributes() ?>>
<span id="el<?php echo $propiedades_delete->RowCnt ?>_propiedades_video" class="propiedades_video">
<span<?php echo $propiedades->video->ViewAttributes() ?>>
<?php echo $propiedades->video->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$propiedades_delete->Recordset->MoveNext();
}
$propiedades_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $propiedades_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fpropiedadesdelete.Init();
</script>
<?php
$propiedades_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$propiedades_delete->Page_Terminate();
?>
