<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "rematesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$remates_addopt = NULL; // Initialize page object first

class cremates_addopt extends cremates {

	// Page ID
	var $PageID = 'addopt';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'remates';

	// Page object name
	var $PageObjName = 'remates_addopt';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (remates)
		if (!isset($GLOBALS["remates"]) || get_class($GLOBALS["remates"]) == "cremates") {
			$GLOBALS["remates"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["remates"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'addopt', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'remates', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("remateslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->tipo->SetVisibility();
		$this->plataforma->SetVisibility();
		$this->ext_id->SetVisibility();
		$this->fecha->SetVisibility();
		$this->hora_inicio->SetVisibility();
		$this->hora_fin->SetVisibility();
		$this->zona->SetVisibility();
		$this->lugar->SetVisibility();
		$this->locacion->SetVisibility();
		$this->numero->SetVisibility();
		$this->titulo->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->descripcion->SetVisibility();
		$this->yacare_visible->SetVisibility();
		$this->banner_g->SetVisibility();
		$this->banner_g_visible->SetVisibility();
		$this->banner_g_z_index->SetVisibility();
		$this->banner_g_pos_x->SetVisibility();
		$this->banner_g_pos_y->SetVisibility();
		$this->banner_g_link->SetVisibility();
		$this->banner_ch->SetVisibility();
		$this->activo->SetVisibility();
		$this->archivo->SetVisibility();
		$this->precios->SetVisibility();
		$this->mapa_img->SetVisibility();
		$this->video_final_servidor->SetVisibility();
		$this->video_final_1->SetVisibility();
		$this->video_titu_1->SetVisibility();
		$this->video_final_2->SetVisibility();
		$this->video_titu_2->SetVisibility();
		$this->video_final_3->SetVisibility();
		$this->video_titu_3->SetVisibility();
		$this->video_final_4->SetVisibility();
		$this->video_titu_4->SetVisibility();
		$this->video_final_5->SetVisibility();
		$this->video_titu_5->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $remates;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($remates);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		set_error_handler("ew_ErrorHandler");

		// Set up Breadcrumb
		//$this->SetupBreadcrumb(); // Not used
		// Process form if post back

		if ($objForm->GetValue("a_addopt") <> "") {
			$this->CurrentAction = $objForm->GetValue("a_addopt"); // Get form action
			$this->LoadFormValues(); // Load form values

			// Validate form
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->setFailureMessage($gsFormError);
			}
		} else { // Not post back
			$this->CurrentAction = "I"; // Display blank record
			$this->LoadDefaultValues(); // Load default values
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow()) { // Add successful
					$row = array();
					$row["x_id"] = $this->id->DbValue;
					$row["x_tipo"] = $this->tipo->DbValue;
					$row["x_plataforma"] = $this->plataforma->DbValue;
					$row["x_ext_id"] = $this->ext_id->DbValue;
					$row["x_fecha"] = $this->fecha->DbValue;
					$row["x_hora_inicio"] = $this->hora_inicio->DbValue;
					$row["x_hora_fin"] = $this->hora_fin->DbValue;
					$row["x_zona"] = $this->zona->DbValue;
					$row["x_lugar"] = $this->lugar->DbValue;
					$row["x_locacion"] = $this->locacion->DbValue;
					$row["x_numero"] = $this->numero->DbValue;
					$row["x_titulo"] = $this->titulo->DbValue;
					$row["x_cantidad"] = $this->cantidad->DbValue;
					$row["x_descripcion"] = $this->descripcion->DbValue;
					$row["x_yacare_visible"] = $this->yacare_visible->DbValue;
					$row["x_banner_g"] = $this->banner_g->DbValue;
					$row["x_banner_g_visible"] = $this->banner_g_visible->DbValue;
					$row["x_banner_g_z_index"] = $this->banner_g_z_index->DbValue;
					$row["x_banner_g_pos_x"] = $this->banner_g_pos_x->DbValue;
					$row["x_banner_g_pos_y"] = $this->banner_g_pos_y->DbValue;
					$row["x_banner_g_link"] = $this->banner_g_link->DbValue;
					$row["x_banner_ch"] = $this->banner_ch->DbValue;
					$row["x_activo"] = $this->activo->DbValue;
					$row["x_archivo"] = $this->archivo->DbValue;
					$row["x_precios"] = $this->precios->DbValue;
					$row["x_mapa_img"] = $this->mapa_img->DbValue;
					$row["x_video_final_servidor"] = $this->video_final_servidor->DbValue;
					$row["x_video_final_1"] = $this->video_final_1->DbValue;
					$row["x_video_titu_1"] = $this->video_titu_1->DbValue;
					$row["x_video_final_2"] = $this->video_final_2->DbValue;
					$row["x_video_titu_2"] = $this->video_titu_2->DbValue;
					$row["x_video_final_3"] = $this->video_final_3->DbValue;
					$row["x_video_titu_3"] = $this->video_titu_3->DbValue;
					$row["x_video_final_4"] = $this->video_final_4->DbValue;
					$row["x_video_titu_4"] = $this->video_titu_4->DbValue;
					$row["x_video_final_5"] = $this->video_final_5->DbValue;
					$row["x_video_titu_5"] = $this->video_titu_5->DbValue;
					if (!EW_DEBUG_ENABLED && ob_get_length())
						ob_end_clean();
					echo ew_ArrayToJson(array($row));
				} else {
					$this->ShowMessage();
				}
				$this->Page_Terminate();
				exit();
		}

		// Render row
		$this->RowType = EW_ROWTYPE_ADD; // Render add type
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->banner_g->Upload->Index = $objForm->Index;
		$this->banner_g->Upload->UploadFile();
		$this->banner_g->CurrentValue = $this->banner_g->Upload->FileName;
		$this->banner_ch->Upload->Index = $objForm->Index;
		$this->banner_ch->Upload->UploadFile();
		$this->banner_ch->CurrentValue = $this->banner_ch->Upload->FileName;
		$this->archivo->Upload->Index = $objForm->Index;
		$this->archivo->Upload->UploadFile();
		$this->archivo->CurrentValue = $this->archivo->Upload->FileName;
		$this->mapa_img->Upload->Index = $objForm->Index;
		$this->mapa_img->Upload->UploadFile();
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->FileName;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->tipo->CurrentValue = "TV";
		$this->plataforma->CurrentValue = "WEB";
		$this->ext_id->CurrentValue = NULL;
		$this->ext_id->OldValue = $this->ext_id->CurrentValue;
		$this->fecha->CurrentValue = NULL;
		$this->fecha->OldValue = $this->fecha->CurrentValue;
		$this->hora_inicio->CurrentValue = NULL;
		$this->hora_inicio->OldValue = $this->hora_inicio->CurrentValue;
		$this->hora_fin->CurrentValue = NULL;
		$this->hora_fin->OldValue = $this->hora_fin->CurrentValue;
		$this->zona->CurrentValue = NULL;
		$this->zona->OldValue = $this->zona->CurrentValue;
		$this->lugar->CurrentValue = NULL;
		$this->lugar->OldValue = $this->lugar->CurrentValue;
		$this->locacion->CurrentValue = NULL;
		$this->locacion->OldValue = $this->locacion->CurrentValue;
		$this->numero->CurrentValue = NULL;
		$this->numero->OldValue = $this->numero->CurrentValue;
		$this->titulo->CurrentValue = NULL;
		$this->titulo->OldValue = $this->titulo->CurrentValue;
		$this->cantidad->CurrentValue = NULL;
		$this->cantidad->OldValue = $this->cantidad->CurrentValue;
		$this->descripcion->CurrentValue = NULL;
		$this->descripcion->OldValue = $this->descripcion->CurrentValue;
		$this->yacare_visible->CurrentValue = "SI";
		$this->banner_g->Upload->DbValue = NULL;
		$this->banner_g->OldValue = $this->banner_g->Upload->DbValue;
		$this->banner_g->CurrentValue = NULL; // Clear file related field
		$this->banner_g_visible->CurrentValue = "SI";
		$this->banner_g_z_index->CurrentValue = NULL;
		$this->banner_g_z_index->OldValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_pos_x->CurrentValue = NULL;
		$this->banner_g_pos_x->OldValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_y->CurrentValue = NULL;
		$this->banner_g_pos_y->OldValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_link->CurrentValue = NULL;
		$this->banner_g_link->OldValue = $this->banner_g_link->CurrentValue;
		$this->banner_ch->Upload->DbValue = NULL;
		$this->banner_ch->OldValue = $this->banner_ch->Upload->DbValue;
		$this->banner_ch->CurrentValue = NULL; // Clear file related field
		$this->activo->CurrentValue = NULL;
		$this->activo->OldValue = $this->activo->CurrentValue;
		$this->archivo->Upload->DbValue = NULL;
		$this->archivo->OldValue = $this->archivo->Upload->DbValue;
		$this->archivo->CurrentValue = NULL; // Clear file related field
		$this->precios->CurrentValue = NULL;
		$this->precios->OldValue = $this->precios->CurrentValue;
		$this->mapa_img->Upload->DbValue = NULL;
		$this->mapa_img->OldValue = $this->mapa_img->Upload->DbValue;
		$this->mapa_img->CurrentValue = NULL; // Clear file related field
		$this->video_final_servidor->CurrentValue = NULL;
		$this->video_final_servidor->OldValue = $this->video_final_servidor->CurrentValue;
		$this->video_final_1->CurrentValue = NULL;
		$this->video_final_1->OldValue = $this->video_final_1->CurrentValue;
		$this->video_titu_1->CurrentValue = NULL;
		$this->video_titu_1->OldValue = $this->video_titu_1->CurrentValue;
		$this->video_final_2->CurrentValue = NULL;
		$this->video_final_2->OldValue = $this->video_final_2->CurrentValue;
		$this->video_titu_2->CurrentValue = NULL;
		$this->video_titu_2->OldValue = $this->video_titu_2->CurrentValue;
		$this->video_final_3->CurrentValue = NULL;
		$this->video_final_3->OldValue = $this->video_final_3->CurrentValue;
		$this->video_titu_3->CurrentValue = NULL;
		$this->video_titu_3->OldValue = $this->video_titu_3->CurrentValue;
		$this->video_final_4->CurrentValue = NULL;
		$this->video_final_4->OldValue = $this->video_final_4->CurrentValue;
		$this->video_titu_4->CurrentValue = NULL;
		$this->video_titu_4->OldValue = $this->video_titu_4->CurrentValue;
		$this->video_final_5->CurrentValue = NULL;
		$this->video_final_5->OldValue = $this->video_final_5->CurrentValue;
		$this->video_titu_5->CurrentValue = NULL;
		$this->video_titu_5->OldValue = $this->video_titu_5->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->tipo->FldIsDetailKey) {
			$this->tipo->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_tipo")));
		}
		if (!$this->plataforma->FldIsDetailKey) {
			$this->plataforma->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_plataforma")));
		}
		if (!$this->ext_id->FldIsDetailKey) {
			$this->ext_id->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_ext_id")));
		}
		if (!$this->fecha->FldIsDetailKey) {
			$this->fecha->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_fecha")));
			$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		}
		if (!$this->hora_inicio->FldIsDetailKey) {
			$this->hora_inicio->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_hora_inicio")));
			$this->hora_inicio->CurrentValue = ew_UnFormatDateTime($this->hora_inicio->CurrentValue, 0);
		}
		if (!$this->hora_fin->FldIsDetailKey) {
			$this->hora_fin->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_hora_fin")));
			$this->hora_fin->CurrentValue = ew_UnFormatDateTime($this->hora_fin->CurrentValue, 0);
		}
		if (!$this->zona->FldIsDetailKey) {
			$this->zona->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_zona")));
		}
		if (!$this->lugar->FldIsDetailKey) {
			$this->lugar->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_lugar")));
		}
		if (!$this->locacion->FldIsDetailKey) {
			$this->locacion->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_locacion")));
		}
		if (!$this->numero->FldIsDetailKey) {
			$this->numero->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_numero")));
		}
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_titulo")));
		}
		if (!$this->cantidad->FldIsDetailKey) {
			$this->cantidad->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_cantidad")));
		}
		if (!$this->descripcion->FldIsDetailKey) {
			$this->descripcion->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_descripcion")));
		}
		if (!$this->yacare_visible->FldIsDetailKey) {
			$this->yacare_visible->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_yacare_visible")));
		}
		if (!$this->banner_g_visible->FldIsDetailKey) {
			$this->banner_g_visible->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_banner_g_visible")));
		}
		if (!$this->banner_g_z_index->FldIsDetailKey) {
			$this->banner_g_z_index->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_banner_g_z_index")));
		}
		if (!$this->banner_g_pos_x->FldIsDetailKey) {
			$this->banner_g_pos_x->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_banner_g_pos_x")));
		}
		if (!$this->banner_g_pos_y->FldIsDetailKey) {
			$this->banner_g_pos_y->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_banner_g_pos_y")));
		}
		if (!$this->banner_g_link->FldIsDetailKey) {
			$this->banner_g_link->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_banner_g_link")));
		}
		if (!$this->activo->FldIsDetailKey) {
			$this->activo->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_activo")));
		}
		if (!$this->precios->FldIsDetailKey) {
			$this->precios->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_precios")));
		}
		if (!$this->video_final_servidor->FldIsDetailKey) {
			$this->video_final_servidor->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_servidor")));
		}
		if (!$this->video_final_1->FldIsDetailKey) {
			$this->video_final_1->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_1")));
		}
		if (!$this->video_titu_1->FldIsDetailKey) {
			$this->video_titu_1->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_titu_1")));
		}
		if (!$this->video_final_2->FldIsDetailKey) {
			$this->video_final_2->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_2")));
		}
		if (!$this->video_titu_2->FldIsDetailKey) {
			$this->video_titu_2->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_titu_2")));
		}
		if (!$this->video_final_3->FldIsDetailKey) {
			$this->video_final_3->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_3")));
		}
		if (!$this->video_titu_3->FldIsDetailKey) {
			$this->video_titu_3->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_titu_3")));
		}
		if (!$this->video_final_4->FldIsDetailKey) {
			$this->video_final_4->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_4")));
		}
		if (!$this->video_titu_4->FldIsDetailKey) {
			$this->video_titu_4->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_titu_4")));
		}
		if (!$this->video_final_5->FldIsDetailKey) {
			$this->video_final_5->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_final_5")));
		}
		if (!$this->video_titu_5->FldIsDetailKey) {
			$this->video_titu_5->setFormValue(ew_ConvertFromUtf8($objForm->GetValue("x_video_titu_5")));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->tipo->CurrentValue = ew_ConvertToUtf8($this->tipo->FormValue);
		$this->plataforma->CurrentValue = ew_ConvertToUtf8($this->plataforma->FormValue);
		$this->ext_id->CurrentValue = ew_ConvertToUtf8($this->ext_id->FormValue);
		$this->fecha->CurrentValue = ew_ConvertToUtf8($this->fecha->FormValue);
		$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		$this->hora_inicio->CurrentValue = ew_ConvertToUtf8($this->hora_inicio->FormValue);
		$this->hora_inicio->CurrentValue = ew_UnFormatDateTime($this->hora_inicio->CurrentValue, 0);
		$this->hora_fin->CurrentValue = ew_ConvertToUtf8($this->hora_fin->FormValue);
		$this->hora_fin->CurrentValue = ew_UnFormatDateTime($this->hora_fin->CurrentValue, 0);
		$this->zona->CurrentValue = ew_ConvertToUtf8($this->zona->FormValue);
		$this->lugar->CurrentValue = ew_ConvertToUtf8($this->lugar->FormValue);
		$this->locacion->CurrentValue = ew_ConvertToUtf8($this->locacion->FormValue);
		$this->numero->CurrentValue = ew_ConvertToUtf8($this->numero->FormValue);
		$this->titulo->CurrentValue = ew_ConvertToUtf8($this->titulo->FormValue);
		$this->cantidad->CurrentValue = ew_ConvertToUtf8($this->cantidad->FormValue);
		$this->descripcion->CurrentValue = ew_ConvertToUtf8($this->descripcion->FormValue);
		$this->yacare_visible->CurrentValue = ew_ConvertToUtf8($this->yacare_visible->FormValue);
		$this->banner_g_visible->CurrentValue = ew_ConvertToUtf8($this->banner_g_visible->FormValue);
		$this->banner_g_z_index->CurrentValue = ew_ConvertToUtf8($this->banner_g_z_index->FormValue);
		$this->banner_g_pos_x->CurrentValue = ew_ConvertToUtf8($this->banner_g_pos_x->FormValue);
		$this->banner_g_pos_y->CurrentValue = ew_ConvertToUtf8($this->banner_g_pos_y->FormValue);
		$this->banner_g_link->CurrentValue = ew_ConvertToUtf8($this->banner_g_link->FormValue);
		$this->activo->CurrentValue = ew_ConvertToUtf8($this->activo->FormValue);
		$this->precios->CurrentValue = ew_ConvertToUtf8($this->precios->FormValue);
		$this->video_final_servidor->CurrentValue = ew_ConvertToUtf8($this->video_final_servidor->FormValue);
		$this->video_final_1->CurrentValue = ew_ConvertToUtf8($this->video_final_1->FormValue);
		$this->video_titu_1->CurrentValue = ew_ConvertToUtf8($this->video_titu_1->FormValue);
		$this->video_final_2->CurrentValue = ew_ConvertToUtf8($this->video_final_2->FormValue);
		$this->video_titu_2->CurrentValue = ew_ConvertToUtf8($this->video_titu_2->FormValue);
		$this->video_final_3->CurrentValue = ew_ConvertToUtf8($this->video_final_3->FormValue);
		$this->video_titu_3->CurrentValue = ew_ConvertToUtf8($this->video_titu_3->FormValue);
		$this->video_final_4->CurrentValue = ew_ConvertToUtf8($this->video_final_4->FormValue);
		$this->video_titu_4->CurrentValue = ew_ConvertToUtf8($this->video_titu_4->FormValue);
		$this->video_final_5->CurrentValue = ew_ConvertToUtf8($this->video_final_5->FormValue);
		$this->video_titu_5->CurrentValue = ew_ConvertToUtf8($this->video_titu_5->FormValue);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->plataforma->setDbValue($rs->fields('plataforma'));
		$this->ext_id->setDbValue($rs->fields('ext_id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->hora_inicio->setDbValue($rs->fields('hora_inicio'));
		$this->hora_fin->setDbValue($rs->fields('hora_fin'));
		$this->zona->setDbValue($rs->fields('zona'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->locacion->setDbValue($rs->fields('locacion'));
		$this->numero->setDbValue($rs->fields('numero'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->yacare_visible->setDbValue($rs->fields('yacare_visible'));
		$this->banner_g->Upload->DbValue = $rs->fields('banner_g');
		$this->banner_g->CurrentValue = $this->banner_g->Upload->DbValue;
		$this->banner_g_visible->setDbValue($rs->fields('banner_g_visible'));
		$this->banner_g_z_index->setDbValue($rs->fields('banner_g_z_index'));
		$this->banner_g_pos_x->setDbValue($rs->fields('banner_g_pos_x'));
		$this->banner_g_pos_y->setDbValue($rs->fields('banner_g_pos_y'));
		$this->banner_g_link->setDbValue($rs->fields('banner_g_link'));
		$this->banner_ch->Upload->DbValue = $rs->fields('banner_ch');
		$this->banner_ch->CurrentValue = $this->banner_ch->Upload->DbValue;
		$this->activo->setDbValue($rs->fields('activo'));
		$this->archivo->Upload->DbValue = $rs->fields('archivo');
		$this->archivo->CurrentValue = $this->archivo->Upload->DbValue;
		$this->precios->setDbValue($rs->fields('precios'));
		$this->mapa_img->Upload->DbValue = $rs->fields('mapa_img');
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->DbValue;
		$this->video_final_servidor->setDbValue($rs->fields('video_final_servidor'));
		$this->video_final_1->setDbValue($rs->fields('video_final_1'));
		$this->video_titu_1->setDbValue($rs->fields('video_titu_1'));
		$this->video_final_2->setDbValue($rs->fields('video_final_2'));
		$this->video_titu_2->setDbValue($rs->fields('video_titu_2'));
		$this->video_final_3->setDbValue($rs->fields('video_final_3'));
		$this->video_titu_3->setDbValue($rs->fields('video_titu_3'));
		$this->video_final_4->setDbValue($rs->fields('video_final_4'));
		$this->video_titu_4->setDbValue($rs->fields('video_titu_4'));
		$this->video_final_5->setDbValue($rs->fields('video_final_5'));
		$this->video_titu_5->setDbValue($rs->fields('video_titu_5'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->tipo->DbValue = $row['tipo'];
		$this->plataforma->DbValue = $row['plataforma'];
		$this->ext_id->DbValue = $row['ext_id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->hora_inicio->DbValue = $row['hora_inicio'];
		$this->hora_fin->DbValue = $row['hora_fin'];
		$this->zona->DbValue = $row['zona'];
		$this->lugar->DbValue = $row['lugar'];
		$this->locacion->DbValue = $row['locacion'];
		$this->numero->DbValue = $row['numero'];
		$this->titulo->DbValue = $row['titulo'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->yacare_visible->DbValue = $row['yacare_visible'];
		$this->banner_g->Upload->DbValue = $row['banner_g'];
		$this->banner_g_visible->DbValue = $row['banner_g_visible'];
		$this->banner_g_z_index->DbValue = $row['banner_g_z_index'];
		$this->banner_g_pos_x->DbValue = $row['banner_g_pos_x'];
		$this->banner_g_pos_y->DbValue = $row['banner_g_pos_y'];
		$this->banner_g_link->DbValue = $row['banner_g_link'];
		$this->banner_ch->Upload->DbValue = $row['banner_ch'];
		$this->activo->DbValue = $row['activo'];
		$this->archivo->Upload->DbValue = $row['archivo'];
		$this->precios->DbValue = $row['precios'];
		$this->mapa_img->Upload->DbValue = $row['mapa_img'];
		$this->video_final_servidor->DbValue = $row['video_final_servidor'];
		$this->video_final_1->DbValue = $row['video_final_1'];
		$this->video_titu_1->DbValue = $row['video_titu_1'];
		$this->video_final_2->DbValue = $row['video_final_2'];
		$this->video_titu_2->DbValue = $row['video_titu_2'];
		$this->video_final_3->DbValue = $row['video_final_3'];
		$this->video_titu_3->DbValue = $row['video_titu_3'];
		$this->video_final_4->DbValue = $row['video_final_4'];
		$this->video_titu_4->DbValue = $row['video_titu_4'];
		$this->video_final_5->DbValue = $row['video_final_5'];
		$this->video_titu_5->DbValue = $row['video_titu_5'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// tipo
		// plataforma
		// ext_id
		// fecha
		// hora_inicio
		// hora_fin
		// zona
		// lugar
		// locacion
		// numero
		// titulo
		// cantidad
		// descripcion
		// yacare_visible
		// banner_g
		// banner_g_visible
		// banner_g_z_index
		// banner_g_pos_x
		// banner_g_pos_y
		// banner_g_link
		// banner_ch
		// activo
		// archivo
		// precios
		// mapa_img
		// video_final_servidor
		// video_final_1
		// video_titu_1
		// video_final_2
		// video_titu_2
		// video_final_3
		// video_titu_3
		// video_final_4
		// video_titu_4
		// video_final_5
		// video_titu_5

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		if (strval($this->tipo->CurrentValue) <> "") {
			$this->tipo->ViewValue = $this->tipo->OptionCaption($this->tipo->CurrentValue);
		} else {
			$this->tipo->ViewValue = NULL;
		}
		$this->tipo->ViewCustomAttributes = "";

		// plataforma
		if (strval($this->plataforma->CurrentValue) <> "") {
			$this->plataforma->ViewValue = $this->plataforma->OptionCaption($this->plataforma->CurrentValue);
		} else {
			$this->plataforma->ViewValue = NULL;
		}
		$this->plataforma->ViewCustomAttributes = "";

		// ext_id
		$this->ext_id->ViewValue = $this->ext_id->CurrentValue;
		$this->ext_id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// hora_inicio
		$this->hora_inicio->ViewValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->ViewCustomAttributes = "";

		// hora_fin
		$this->hora_fin->ViewValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->ViewCustomAttributes = "";

		// zona
		if (strval($this->zona->CurrentValue) <> "") {
			$this->zona->ViewValue = $this->zona->OptionCaption($this->zona->CurrentValue);
		} else {
			$this->zona->ViewValue = NULL;
		}
		$this->zona->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewCustomAttributes = "";

		// locacion
		$this->locacion->ViewValue = $this->locacion->CurrentValue;
		$this->locacion->ViewCustomAttributes = "";

		// numero
		$this->numero->ViewValue = $this->numero->CurrentValue;
		$this->numero->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// yacare_visible
		if (strval($this->yacare_visible->CurrentValue) <> "") {
			$this->yacare_visible->ViewValue = $this->yacare_visible->OptionCaption($this->yacare_visible->CurrentValue);
		} else {
			$this->yacare_visible->ViewValue = NULL;
		}
		$this->yacare_visible->ViewCustomAttributes = "";

		// banner_g
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->ViewValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->ViewValue = "";
		}
		$this->banner_g->ViewCustomAttributes = "";

		// banner_g_visible
		if (strval($this->banner_g_visible->CurrentValue) <> "") {
			$this->banner_g_visible->ViewValue = $this->banner_g_visible->OptionCaption($this->banner_g_visible->CurrentValue);
		} else {
			$this->banner_g_visible->ViewValue = NULL;
		}
		$this->banner_g_visible->ViewCustomAttributes = "";

		// banner_g_z_index
		$this->banner_g_z_index->ViewValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->ViewCustomAttributes = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->ViewValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->ViewCustomAttributes = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->ViewValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->ViewCustomAttributes = "";

		// banner_g_link
		$this->banner_g_link->ViewValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->ViewCustomAttributes = "";

		// banner_ch
		if (!ew_Empty($this->banner_ch->Upload->DbValue)) {
			$this->banner_ch->ViewValue = $this->banner_ch->Upload->DbValue;
		} else {
			$this->banner_ch->ViewValue = "";
		}
		$this->banner_ch->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// archivo
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->ViewValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->ViewValue = "";
		}
		$this->archivo->ViewCustomAttributes = "";

		// precios
		if (strval($this->precios->CurrentValue) <> "") {
			$this->precios->ViewValue = $this->precios->OptionCaption($this->precios->CurrentValue);
		} else {
			$this->precios->ViewValue = NULL;
		}
		$this->precios->ViewCustomAttributes = "";

		// mapa_img
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->ViewValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->ViewValue = "";
		}
		$this->mapa_img->ViewCustomAttributes = "";

		// video_final_servidor
		if (strval($this->video_final_servidor->CurrentValue) <> "") {
			$this->video_final_servidor->ViewValue = $this->video_final_servidor->OptionCaption($this->video_final_servidor->CurrentValue);
		} else {
			$this->video_final_servidor->ViewValue = NULL;
		}
		$this->video_final_servidor->ViewCustomAttributes = "";

		// video_final_1
		$this->video_final_1->ViewValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->ViewCustomAttributes = "";

		// video_titu_1
		$this->video_titu_1->ViewValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->ViewCustomAttributes = "";

		// video_final_2
		$this->video_final_2->ViewValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->ViewCustomAttributes = "";

		// video_titu_2
		$this->video_titu_2->ViewValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->ViewCustomAttributes = "";

		// video_final_3
		$this->video_final_3->ViewValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->ViewCustomAttributes = "";

		// video_titu_3
		$this->video_titu_3->ViewValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->ViewCustomAttributes = "";

		// video_final_4
		$this->video_final_4->ViewValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->ViewCustomAttributes = "";

		// video_titu_4
		$this->video_titu_4->ViewValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->ViewCustomAttributes = "";

		// video_final_5
		$this->video_final_5->ViewValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->ViewCustomAttributes = "";

		// video_titu_5
		$this->video_titu_5->ViewValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->ViewCustomAttributes = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";
			$this->plataforma->TooltipValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";
			$this->ext_id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";
			$this->hora_inicio->TooltipValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";
			$this->hora_fin->TooltipValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";
			$this->zona->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";
			$this->locacion->TooltipValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";
			$this->numero->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";
			$this->descripcion->TooltipValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";
			$this->yacare_visible->TooltipValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;
			$this->banner_g->TooltipValue = "";

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";
			$this->banner_g_visible->TooltipValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";
			$this->banner_g_z_index->TooltipValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";
			$this->banner_g_pos_x->TooltipValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";
			$this->banner_g_pos_y->TooltipValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";
			$this->banner_g_link->TooltipValue = "";

			// banner_ch
			$this->banner_ch->LinkCustomAttributes = "";
			$this->banner_ch->HrefValue = "";
			$this->banner_ch->HrefValue2 = $this->banner_ch->UploadPath . $this->banner_ch->Upload->DbValue;
			$this->banner_ch->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;
			$this->archivo->TooltipValue = "";

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";
			$this->precios->TooltipValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;
			$this->mapa_img->TooltipValue = "";

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";
			$this->video_final_servidor->TooltipValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";
			$this->video_final_1->TooltipValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";
			$this->video_titu_1->TooltipValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";
			$this->video_final_2->TooltipValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";
			$this->video_titu_2->TooltipValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";
			$this->video_final_3->TooltipValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";
			$this->video_titu_3->TooltipValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";
			$this->video_final_4->TooltipValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";
			$this->video_titu_4->TooltipValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";
			$this->video_final_5->TooltipValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
			$this->video_titu_5->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// tipo
			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = $this->tipo->Options(FALSE);

			// plataforma
			$this->plataforma->EditAttrs["class"] = "form-control";
			$this->plataforma->EditCustomAttributes = "";
			$this->plataforma->EditValue = $this->plataforma->Options(TRUE);

			// ext_id
			$this->ext_id->EditAttrs["class"] = "form-control";
			$this->ext_id->EditCustomAttributes = "";
			$this->ext_id->EditValue = ew_HtmlEncode($this->ext_id->CurrentValue);
			$this->ext_id->PlaceHolder = ew_RemoveHtml($this->ext_id->FldCaption());

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha->CurrentValue, 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// hora_inicio
			$this->hora_inicio->EditAttrs["class"] = "form-control";
			$this->hora_inicio->EditCustomAttributes = "";
			$this->hora_inicio->EditValue = ew_HtmlEncode($this->hora_inicio->CurrentValue);
			$this->hora_inicio->PlaceHolder = ew_RemoveHtml($this->hora_inicio->FldCaption());

			// hora_fin
			$this->hora_fin->EditAttrs["class"] = "form-control";
			$this->hora_fin->EditCustomAttributes = "";
			$this->hora_fin->EditValue = ew_HtmlEncode($this->hora_fin->CurrentValue);
			$this->hora_fin->PlaceHolder = ew_RemoveHtml($this->hora_fin->FldCaption());

			// zona
			$this->zona->EditAttrs["class"] = "form-control";
			$this->zona->EditCustomAttributes = "";
			$this->zona->EditValue = $this->zona->Options(TRUE);

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// locacion
			$this->locacion->EditAttrs["class"] = "form-control";
			$this->locacion->EditCustomAttributes = "";
			$this->locacion->EditValue = ew_HtmlEncode($this->locacion->CurrentValue);
			$this->locacion->PlaceHolder = ew_RemoveHtml($this->locacion->FldCaption());

			// numero
			$this->numero->EditAttrs["class"] = "form-control";
			$this->numero->EditCustomAttributes = "";
			$this->numero->EditValue = ew_HtmlEncode($this->numero->CurrentValue);
			$this->numero->PlaceHolder = ew_RemoveHtml($this->numero->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// descripcion
			$this->descripcion->EditAttrs["class"] = "form-control";
			$this->descripcion->EditCustomAttributes = "";
			$this->descripcion->EditValue = ew_HtmlEncode($this->descripcion->CurrentValue);
			$this->descripcion->PlaceHolder = ew_RemoveHtml($this->descripcion->FldCaption());

			// yacare_visible
			$this->yacare_visible->EditAttrs["class"] = "form-control";
			$this->yacare_visible->EditCustomAttributes = "";
			$this->yacare_visible->EditValue = $this->yacare_visible->Options(TRUE);

			// banner_g
			$this->banner_g->EditAttrs["class"] = "form-control";
			$this->banner_g->EditCustomAttributes = "";
			if (!ew_Empty($this->banner_g->Upload->DbValue)) {
				$this->banner_g->EditValue = $this->banner_g->Upload->DbValue;
			} else {
				$this->banner_g->EditValue = "";
			}
			if (!ew_Empty($this->banner_g->CurrentValue))
				$this->banner_g->Upload->FileName = $this->banner_g->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->banner_g);

			// banner_g_visible
			$this->banner_g_visible->EditAttrs["class"] = "form-control";
			$this->banner_g_visible->EditCustomAttributes = "";
			$this->banner_g_visible->EditValue = $this->banner_g_visible->Options(TRUE);

			// banner_g_z_index
			$this->banner_g_z_index->EditAttrs["class"] = "form-control";
			$this->banner_g_z_index->EditCustomAttributes = "";
			$this->banner_g_z_index->EditValue = ew_HtmlEncode($this->banner_g_z_index->CurrentValue);
			$this->banner_g_z_index->PlaceHolder = ew_RemoveHtml($this->banner_g_z_index->FldCaption());

			// banner_g_pos_x
			$this->banner_g_pos_x->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_x->EditCustomAttributes = "";
			$this->banner_g_pos_x->EditValue = ew_HtmlEncode($this->banner_g_pos_x->CurrentValue);
			$this->banner_g_pos_x->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_x->FldCaption());

			// banner_g_pos_y
			$this->banner_g_pos_y->EditAttrs["class"] = "form-control";
			$this->banner_g_pos_y->EditCustomAttributes = "";
			$this->banner_g_pos_y->EditValue = ew_HtmlEncode($this->banner_g_pos_y->CurrentValue);
			$this->banner_g_pos_y->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_y->FldCaption());

			// banner_g_link
			$this->banner_g_link->EditAttrs["class"] = "form-control";
			$this->banner_g_link->EditCustomAttributes = "";
			$this->banner_g_link->EditValue = ew_HtmlEncode($this->banner_g_link->CurrentValue);
			$this->banner_g_link->PlaceHolder = ew_RemoveHtml($this->banner_g_link->FldCaption());

			// banner_ch
			$this->banner_ch->EditAttrs["class"] = "form-control";
			$this->banner_ch->EditCustomAttributes = "";
			if (!ew_Empty($this->banner_ch->Upload->DbValue)) {
				$this->banner_ch->EditValue = $this->banner_ch->Upload->DbValue;
			} else {
				$this->banner_ch->EditValue = "";
			}
			if (!ew_Empty($this->banner_ch->CurrentValue))
				$this->banner_ch->Upload->FileName = $this->banner_ch->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->banner_ch);

			// activo
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(FALSE);

			// archivo
			$this->archivo->EditAttrs["class"] = "form-control";
			$this->archivo->EditCustomAttributes = "";
			if (!ew_Empty($this->archivo->Upload->DbValue)) {
				$this->archivo->EditValue = $this->archivo->Upload->DbValue;
			} else {
				$this->archivo->EditValue = "";
			}
			if (!ew_Empty($this->archivo->CurrentValue))
				$this->archivo->Upload->FileName = $this->archivo->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->archivo);

			// precios
			$this->precios->EditAttrs["class"] = "form-control";
			$this->precios->EditCustomAttributes = "";
			$this->precios->EditValue = $this->precios->Options(TRUE);

			// mapa_img
			$this->mapa_img->EditAttrs["class"] = "form-control";
			$this->mapa_img->EditCustomAttributes = "";
			if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
				$this->mapa_img->EditValue = $this->mapa_img->Upload->DbValue;
			} else {
				$this->mapa_img->EditValue = "";
			}
			if (!ew_Empty($this->mapa_img->CurrentValue))
				$this->mapa_img->Upload->FileName = $this->mapa_img->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->mapa_img);

			// video_final_servidor
			$this->video_final_servidor->EditCustomAttributes = "";
			$this->video_final_servidor->EditValue = $this->video_final_servidor->Options(FALSE);

			// video_final_1
			$this->video_final_1->EditAttrs["class"] = "form-control";
			$this->video_final_1->EditCustomAttributes = "";
			$this->video_final_1->EditValue = ew_HtmlEncode($this->video_final_1->CurrentValue);
			$this->video_final_1->PlaceHolder = ew_RemoveHtml($this->video_final_1->FldCaption());

			// video_titu_1
			$this->video_titu_1->EditAttrs["class"] = "form-control";
			$this->video_titu_1->EditCustomAttributes = "";
			$this->video_titu_1->EditValue = ew_HtmlEncode($this->video_titu_1->CurrentValue);
			$this->video_titu_1->PlaceHolder = ew_RemoveHtml($this->video_titu_1->FldCaption());

			// video_final_2
			$this->video_final_2->EditAttrs["class"] = "form-control";
			$this->video_final_2->EditCustomAttributes = "";
			$this->video_final_2->EditValue = ew_HtmlEncode($this->video_final_2->CurrentValue);
			$this->video_final_2->PlaceHolder = ew_RemoveHtml($this->video_final_2->FldCaption());

			// video_titu_2
			$this->video_titu_2->EditAttrs["class"] = "form-control";
			$this->video_titu_2->EditCustomAttributes = "";
			$this->video_titu_2->EditValue = ew_HtmlEncode($this->video_titu_2->CurrentValue);
			$this->video_titu_2->PlaceHolder = ew_RemoveHtml($this->video_titu_2->FldCaption());

			// video_final_3
			$this->video_final_3->EditAttrs["class"] = "form-control";
			$this->video_final_3->EditCustomAttributes = "";
			$this->video_final_3->EditValue = ew_HtmlEncode($this->video_final_3->CurrentValue);
			$this->video_final_3->PlaceHolder = ew_RemoveHtml($this->video_final_3->FldCaption());

			// video_titu_3
			$this->video_titu_3->EditAttrs["class"] = "form-control";
			$this->video_titu_3->EditCustomAttributes = "";
			$this->video_titu_3->EditValue = ew_HtmlEncode($this->video_titu_3->CurrentValue);
			$this->video_titu_3->PlaceHolder = ew_RemoveHtml($this->video_titu_3->FldCaption());

			// video_final_4
			$this->video_final_4->EditAttrs["class"] = "form-control";
			$this->video_final_4->EditCustomAttributes = "";
			$this->video_final_4->EditValue = ew_HtmlEncode($this->video_final_4->CurrentValue);
			$this->video_final_4->PlaceHolder = ew_RemoveHtml($this->video_final_4->FldCaption());

			// video_titu_4
			$this->video_titu_4->EditAttrs["class"] = "form-control";
			$this->video_titu_4->EditCustomAttributes = "";
			$this->video_titu_4->EditValue = ew_HtmlEncode($this->video_titu_4->CurrentValue);
			$this->video_titu_4->PlaceHolder = ew_RemoveHtml($this->video_titu_4->FldCaption());

			// video_final_5
			$this->video_final_5->EditAttrs["class"] = "form-control";
			$this->video_final_5->EditCustomAttributes = "";
			$this->video_final_5->EditValue = ew_HtmlEncode($this->video_final_5->CurrentValue);
			$this->video_final_5->PlaceHolder = ew_RemoveHtml($this->video_final_5->FldCaption());

			// video_titu_5
			$this->video_titu_5->EditAttrs["class"] = "form-control";
			$this->video_titu_5->EditCustomAttributes = "";
			$this->video_titu_5->EditValue = ew_HtmlEncode($this->video_titu_5->CurrentValue);
			$this->video_titu_5->PlaceHolder = ew_RemoveHtml($this->video_titu_5->FldCaption());

			// Add refer script
			// tipo

			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";

			// banner_ch
			$this->banner_ch->LinkCustomAttributes = "";
			$this->banner_ch->HrefValue = "";
			$this->banner_ch->HrefValue2 = $this->banner_ch->UploadPath . $this->banner_ch->Upload->DbValue;

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if ($this->tipo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->tipo->FldCaption(), $this->tipo->ReqErrMsg));
		}
		if (!$this->plataforma->FldIsDetailKey && !is_null($this->plataforma->FormValue) && $this->plataforma->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->plataforma->FldCaption(), $this->plataforma->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->ext_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->ext_id->FldErrMsg());
		}
		if (!$this->fecha->FldIsDetailKey && !is_null($this->fecha->FormValue) && $this->fecha->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha->FldCaption(), $this->fecha->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha->FldErrMsg());
		}
		if (!ew_CheckTime($this->hora_inicio->FormValue)) {
			ew_AddMessage($gsFormError, $this->hora_inicio->FldErrMsg());
		}
		if (!ew_CheckTime($this->hora_fin->FormValue)) {
			ew_AddMessage($gsFormError, $this->hora_fin->FldErrMsg());
		}
		if (!$this->lugar->FldIsDetailKey && !is_null($this->lugar->FormValue) && $this->lugar->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->lugar->FldCaption(), $this->lugar->ReqErrMsg));
		}
		if (!$this->numero->FldIsDetailKey && !is_null($this->numero->FormValue) && $this->numero->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->numero->FldCaption(), $this->numero->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->numero->FormValue)) {
			ew_AddMessage($gsFormError, $this->numero->FldErrMsg());
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->cantidad->FormValue)) {
			ew_AddMessage($gsFormError, $this->cantidad->FldErrMsg());
		}
		if (!$this->yacare_visible->FldIsDetailKey && !is_null($this->yacare_visible->FormValue) && $this->yacare_visible->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->yacare_visible->FldCaption(), $this->yacare_visible->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->banner_g_z_index->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_z_index->FldErrMsg());
		}
		if (!ew_CheckInteger($this->banner_g_pos_x->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_pos_x->FldErrMsg());
		}
		if (!ew_CheckInteger($this->banner_g_pos_y->FormValue)) {
			ew_AddMessage($gsFormError, $this->banner_g_pos_y->FldErrMsg());
		}
		if ($this->activo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->activo->FldCaption(), $this->activo->ReqErrMsg));
		}
		if ($this->video_final_servidor->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->video_final_servidor->FldCaption(), $this->video_final_servidor->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// tipo
		$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, "", strval($this->tipo->CurrentValue) == "");

		// plataforma
		$this->plataforma->SetDbValueDef($rsnew, $this->plataforma->CurrentValue, "", strval($this->plataforma->CurrentValue) == "");

		// ext_id
		$this->ext_id->SetDbValueDef($rsnew, $this->ext_id->CurrentValue, NULL, FALSE);

		// fecha
		$this->fecha->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha->CurrentValue, 0), ew_CurrentDate(), FALSE);

		// hora_inicio
		$this->hora_inicio->SetDbValueDef($rsnew, $this->hora_inicio->CurrentValue, NULL, FALSE);

		// hora_fin
		$this->hora_fin->SetDbValueDef($rsnew, $this->hora_fin->CurrentValue, NULL, FALSE);

		// zona
		$this->zona->SetDbValueDef($rsnew, $this->zona->CurrentValue, NULL, FALSE);

		// lugar
		$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, "", FALSE);

		// locacion
		$this->locacion->SetDbValueDef($rsnew, $this->locacion->CurrentValue, NULL, FALSE);

		// numero
		$this->numero->SetDbValueDef($rsnew, $this->numero->CurrentValue, 0, FALSE);

		// titulo
		$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", FALSE);

		// cantidad
		$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, FALSE);

		// descripcion
		$this->descripcion->SetDbValueDef($rsnew, $this->descripcion->CurrentValue, NULL, FALSE);

		// yacare_visible
		$this->yacare_visible->SetDbValueDef($rsnew, $this->yacare_visible->CurrentValue, "", strval($this->yacare_visible->CurrentValue) == "");

		// banner_g
		if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
			$this->banner_g->Upload->DbValue = ""; // No need to delete old file
			if ($this->banner_g->Upload->FileName == "") {
				$rsnew['banner_g'] = NULL;
			} else {
				$rsnew['banner_g'] = $this->banner_g->Upload->FileName;
			}
		}

		// banner_g_visible
		$this->banner_g_visible->SetDbValueDef($rsnew, $this->banner_g_visible->CurrentValue, NULL, strval($this->banner_g_visible->CurrentValue) == "");

		// banner_g_z_index
		$this->banner_g_z_index->SetDbValueDef($rsnew, $this->banner_g_z_index->CurrentValue, NULL, FALSE);

		// banner_g_pos_x
		$this->banner_g_pos_x->SetDbValueDef($rsnew, $this->banner_g_pos_x->CurrentValue, NULL, FALSE);

		// banner_g_pos_y
		$this->banner_g_pos_y->SetDbValueDef($rsnew, $this->banner_g_pos_y->CurrentValue, NULL, FALSE);

		// banner_g_link
		$this->banner_g_link->SetDbValueDef($rsnew, $this->banner_g_link->CurrentValue, NULL, FALSE);

		// banner_ch
		if ($this->banner_ch->Visible && !$this->banner_ch->Upload->KeepFile) {
			$this->banner_ch->Upload->DbValue = ""; // No need to delete old file
			if ($this->banner_ch->Upload->FileName == "") {
				$rsnew['banner_ch'] = NULL;
			} else {
				$rsnew['banner_ch'] = $this->banner_ch->Upload->FileName;
			}
		}

		// activo
		$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, "", FALSE);

		// archivo
		if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
			$this->archivo->Upload->DbValue = ""; // No need to delete old file
			if ($this->archivo->Upload->FileName == "") {
				$rsnew['archivo'] = NULL;
			} else {
				$rsnew['archivo'] = $this->archivo->Upload->FileName;
			}
		}

		// precios
		$this->precios->SetDbValueDef($rsnew, $this->precios->CurrentValue, NULL, FALSE);

		// mapa_img
		if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
			$this->mapa_img->Upload->DbValue = ""; // No need to delete old file
			if ($this->mapa_img->Upload->FileName == "") {
				$rsnew['mapa_img'] = NULL;
			} else {
				$rsnew['mapa_img'] = $this->mapa_img->Upload->FileName;
			}
		}

		// video_final_servidor
		$this->video_final_servidor->SetDbValueDef($rsnew, $this->video_final_servidor->CurrentValue, NULL, strval($this->video_final_servidor->CurrentValue) == "");

		// video_final_1
		$this->video_final_1->SetDbValueDef($rsnew, $this->video_final_1->CurrentValue, NULL, FALSE);

		// video_titu_1
		$this->video_titu_1->SetDbValueDef($rsnew, $this->video_titu_1->CurrentValue, NULL, FALSE);

		// video_final_2
		$this->video_final_2->SetDbValueDef($rsnew, $this->video_final_2->CurrentValue, NULL, FALSE);

		// video_titu_2
		$this->video_titu_2->SetDbValueDef($rsnew, $this->video_titu_2->CurrentValue, NULL, FALSE);

		// video_final_3
		$this->video_final_3->SetDbValueDef($rsnew, $this->video_final_3->CurrentValue, NULL, FALSE);

		// video_titu_3
		$this->video_titu_3->SetDbValueDef($rsnew, $this->video_titu_3->CurrentValue, NULL, FALSE);

		// video_final_4
		$this->video_final_4->SetDbValueDef($rsnew, $this->video_final_4->CurrentValue, NULL, FALSE);

		// video_titu_4
		$this->video_titu_4->SetDbValueDef($rsnew, $this->video_titu_4->CurrentValue, NULL, FALSE);

		// video_final_5
		$this->video_final_5->SetDbValueDef($rsnew, $this->video_final_5->CurrentValue, NULL, FALSE);

		// video_titu_5
		$this->video_titu_5->SetDbValueDef($rsnew, $this->video_titu_5->CurrentValue, NULL, FALSE);
		if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
			if (!ew_Empty($this->banner_g->Upload->Value)) {
				$rsnew['banner_g'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->banner_g->UploadPath), $rsnew['banner_g']); // Get new file name
			}
		}
		if ($this->banner_ch->Visible && !$this->banner_ch->Upload->KeepFile) {
			if (!ew_Empty($this->banner_ch->Upload->Value)) {
				$rsnew['banner_ch'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->banner_ch->UploadPath), $rsnew['banner_ch']); // Get new file name
			}
		}
		if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
			if (!ew_Empty($this->archivo->Upload->Value)) {
				$rsnew['archivo'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->archivo->UploadPath), $rsnew['archivo']); // Get new file name
			}
		}
		if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
			if (!ew_Empty($this->mapa_img->Upload->Value)) {
				$rsnew['mapa_img'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->mapa_img->UploadPath), $rsnew['mapa_img']); // Get new file name
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->id->setDbValue($conn->Insert_ID());
				$rsnew['id'] = $this->id->DbValue;
				if ($this->banner_g->Visible && !$this->banner_g->Upload->KeepFile) {
					if (!ew_Empty($this->banner_g->Upload->Value)) {
						if (!$this->banner_g->Upload->SaveToFile($this->banner_g->UploadPath, $rsnew['banner_g'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->banner_ch->Visible && !$this->banner_ch->Upload->KeepFile) {
					if (!ew_Empty($this->banner_ch->Upload->Value)) {
						if (!$this->banner_ch->Upload->SaveToFile($this->banner_ch->UploadPath, $rsnew['banner_ch'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->archivo->Visible && !$this->archivo->Upload->KeepFile) {
					if (!ew_Empty($this->archivo->Upload->Value)) {
						if (!$this->archivo->Upload->SaveToFile($this->archivo->UploadPath, $rsnew['archivo'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->mapa_img->Visible && !$this->mapa_img->Upload->KeepFile) {
					if (!ew_Empty($this->mapa_img->Upload->Value)) {
						if (!$this->mapa_img->Upload->SaveToFile($this->mapa_img->UploadPath, $rsnew['mapa_img'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// banner_g
		ew_CleanUploadTempPath($this->banner_g, $this->banner_g->Upload->Index);

		// banner_ch
		ew_CleanUploadTempPath($this->banner_ch, $this->banner_ch->Upload->Index);

		// archivo
		ew_CleanUploadTempPath($this->archivo, $this->archivo->Upload->Index);

		// mapa_img
		ew_CleanUploadTempPath($this->mapa_img, $this->mapa_img->Upload->Index);
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("remateslist.php"), "", $this->TableVar, TRUE);
		$PageId = "addopt";
		$Breadcrumb->Add("addopt", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Custom validate event
	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($remates_addopt)) $remates_addopt = new cremates_addopt();

// Page init
$remates_addopt->Page_Init();

// Page main
$remates_addopt->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$remates_addopt->Page_Render();
?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "addopt";
var CurrentForm = frematesaddopt = new ew_Form("frematesaddopt", "addopt");

// Validate form
frematesaddopt.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_tipo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->tipo->FldCaption(), $remates->tipo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_plataforma");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->plataforma->FldCaption(), $remates->plataforma->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_ext_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->ext_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->fecha->FldCaption(), $remates->fecha->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->fecha->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_hora_inicio");
			if (elm && !ew_CheckTime(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->hora_inicio->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_hora_fin");
			if (elm && !ew_CheckTime(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->hora_fin->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_lugar");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->lugar->FldCaption(), $remates->lugar->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_numero");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->numero->FldCaption(), $remates->numero->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_numero");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->numero->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->titulo->FldCaption(), $remates->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_cantidad");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->cantidad->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_yacare_visible");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->yacare_visible->FldCaption(), $remates->yacare_visible->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_z_index");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_z_index->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_pos_x");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_pos_x->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_banner_g_pos_y");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($remates->banner_g_pos_y->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_activo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->activo->FldCaption(), $remates->activo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_video_final_servidor");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $remates->video_final_servidor->FldCaption(), $remates->video_final_servidor->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}
	return true;
}

// Form_CustomValidate event
frematesaddopt.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
frematesaddopt.ValidateRequired = true;
<?php } else { ?>
frematesaddopt.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
frematesaddopt.Lists["x_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_tipo"].Options = <?php echo json_encode($remates->tipo->Options()) ?>;
frematesaddopt.Lists["x_plataforma"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_plataforma"].Options = <?php echo json_encode($remates->plataforma->Options()) ?>;
frematesaddopt.Lists["x_zona"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_zona"].Options = <?php echo json_encode($remates->zona->Options()) ?>;
frematesaddopt.Lists["x_yacare_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_yacare_visible"].Options = <?php echo json_encode($remates->yacare_visible->Options()) ?>;
frematesaddopt.Lists["x_banner_g_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_banner_g_visible"].Options = <?php echo json_encode($remates->banner_g_visible->Options()) ?>;
frematesaddopt.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_activo"].Options = <?php echo json_encode($remates->activo->Options()) ?>;
frematesaddopt.Lists["x_precios"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_precios"].Options = <?php echo json_encode($remates->precios->Options()) ?>;
frematesaddopt.Lists["x_video_final_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesaddopt.Lists["x_video_final_servidor"].Options = <?php echo json_encode($remates->video_final_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php
$remates_addopt->ShowMessage();
?>
<form name="frematesaddopt" id="frematesaddopt" class="ewForm form-horizontal" action="rematesaddopt.php" method="post">
<?php if ($remates_addopt->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $remates_addopt->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="remates">
<input type="hidden" name="a_addopt" id="a_addopt" value="A">
<?php if ($remates->tipo->Visible) { // tipo ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->tipo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<div id="tp_x_tipo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_tipo" data-value-separator="<?php echo $remates->tipo->DisplayValueSeparatorAttribute() ?>" name="x_tipo" id="x_tipo" value="{value}"<?php echo $remates->tipo->EditAttributes() ?>></div>
<div id="dsl_x_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->tipo->RadioButtonListHtml(FALSE, "x_tipo") ?>
</div></div>
</div>
	</div>
<?php } ?>	
<?php if ($remates->plataforma->Visible) { // plataforma ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_plataforma"><?php echo $remates->plataforma->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<select data-table="remates" data-field="x_plataforma" data-value-separator="<?php echo $remates->plataforma->DisplayValueSeparatorAttribute() ?>" id="x_plataforma" name="x_plataforma"<?php echo $remates->plataforma->EditAttributes() ?>>
<?php echo $remates->plataforma->SelectOptionListHtml("x_plataforma") ?>
</select>
</div>
	</div>
<?php } ?>	
<?php if ($remates->ext_id->Visible) { // ext_id ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_ext_id"><?php echo $remates->ext_id->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_ext_id" name="x_ext_id" id="x_ext_id" size="45" placeholder="<?php echo ew_HtmlEncode($remates->ext_id->getPlaceHolder()) ?>" value="<?php echo $remates->ext_id->EditValue ?>"<?php echo $remates->ext_id->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->fecha->Visible) { // fecha ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_fecha"><?php echo $remates->fecha->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_fecha" name="x_fecha" id="x_fecha" placeholder="<?php echo ew_HtmlEncode($remates->fecha->getPlaceHolder()) ?>" value="<?php echo $remates->fecha->EditValue ?>"<?php echo $remates->fecha->EditAttributes() ?>>
<?php if (!$remates->fecha->ReadOnly && !$remates->fecha->Disabled && !isset($remates->fecha->EditAttrs["readonly"]) && !isset($remates->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("frematesaddopt", "x_fecha", 0);
</script>
<?php } ?>
</div>
	</div>
<?php } ?>	
<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_hora_inicio"><?php echo $remates->hora_inicio->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_hora_inicio" name="x_hora_inicio" id="x_hora_inicio" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_inicio->getPlaceHolder()) ?>" value="<?php echo $remates->hora_inicio->EditValue ?>"<?php echo $remates->hora_inicio->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_hora_fin"><?php echo $remates->hora_fin->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_hora_fin" name="x_hora_fin" id="x_hora_fin" size="30" placeholder="<?php echo ew_HtmlEncode($remates->hora_fin->getPlaceHolder()) ?>" value="<?php echo $remates->hora_fin->EditValue ?>"<?php echo $remates->hora_fin->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->zona->Visible) { // zona ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_zona"><?php echo $remates->zona->FldCaption() ?></label>
		<div class="col-sm-9">
<select data-table="remates" data-field="x_zona" data-value-separator="<?php echo $remates->zona->DisplayValueSeparatorAttribute() ?>" id="x_zona" name="x_zona"<?php echo $remates->zona->EditAttributes() ?>>
<?php echo $remates->zona->SelectOptionListHtml("x_zona") ?>
</select>
</div>
	</div>
<?php } ?>	
<?php if ($remates->lugar->Visible) { // lugar ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_lugar"><?php echo $remates->lugar->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_lugar" name="x_lugar" id="x_lugar" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->lugar->getPlaceHolder()) ?>" value="<?php echo $remates->lugar->EditValue ?>"<?php echo $remates->lugar->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->locacion->Visible) { // locacion ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_locacion"><?php echo $remates->locacion->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_locacion" name="x_locacion" id="x_locacion" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->locacion->getPlaceHolder()) ?>" value="<?php echo $remates->locacion->EditValue ?>"<?php echo $remates->locacion->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->numero->Visible) { // numero ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_numero"><?php echo $remates->numero->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_numero" name="x_numero" id="x_numero" size="30" placeholder="<?php echo ew_HtmlEncode($remates->numero->getPlaceHolder()) ?>" value="<?php echo $remates->numero->EditValue ?>"<?php echo $remates->numero->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->titulo->Visible) { // titulo ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_titulo"><?php echo $remates->titulo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_titulo" name="x_titulo" id="x_titulo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->titulo->getPlaceHolder()) ?>" value="<?php echo $remates->titulo->EditValue ?>"<?php echo $remates->titulo->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->cantidad->Visible) { // cantidad ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_cantidad"><?php echo $remates->cantidad->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_cantidad" name="x_cantidad" id="x_cantidad" size="30" maxlength="10" placeholder="<?php echo ew_HtmlEncode($remates->cantidad->getPlaceHolder()) ?>" value="<?php echo $remates->cantidad->EditValue ?>"<?php echo $remates->cantidad->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->descripcion->Visible) { // descripcion ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_descripcion"><?php echo $remates->descripcion->FldCaption() ?></label>
		<div class="col-sm-9">
<textarea data-table="remates" data-field="x_descripcion" name="x_descripcion" id="x_descripcion" cols="60" rows="4" placeholder="<?php echo ew_HtmlEncode($remates->descripcion->getPlaceHolder()) ?>"<?php echo $remates->descripcion->EditAttributes() ?>><?php echo $remates->descripcion->EditValue ?></textarea>
</div>
	</div>
<?php } ?>	
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_yacare_visible"><?php echo $remates->yacare_visible->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<select data-table="remates" data-field="x_yacare_visible" data-value-separator="<?php echo $remates->yacare_visible->DisplayValueSeparatorAttribute() ?>" id="x_yacare_visible" name="x_yacare_visible"<?php echo $remates->yacare_visible->EditAttributes() ?>>
<?php echo $remates->yacare_visible->SelectOptionListHtml("x_yacare_visible") ?>
</select>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g->Visible) { // banner_g ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->banner_g->FldCaption() ?></label>
		<div class="col-sm-9">
<div id="fd_x_banner_g">
<span title="<?php echo $remates->banner_g->FldTitle() ? $remates->banner_g->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_g->ReadOnly || $remates->banner_g->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_g" name="x_banner_g" id="x_banner_g"<?php echo $remates->banner_g->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_banner_g" id= "fn_x_banner_g" value="<?php echo $remates->banner_g->Upload->FileName ?>">
<input type="hidden" name="fa_x_banner_g" id= "fa_x_banner_g" value="0">
<input type="hidden" name="fs_x_banner_g" id= "fs_x_banner_g" value="255">
<input type="hidden" name="fx_x_banner_g" id= "fx_x_banner_g" value="<?php echo $remates->banner_g->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_banner_g" id= "fm_x_banner_g" value="<?php echo $remates->banner_g->UploadMaxFileSize ?>">
</div>
<table id="ft_x_banner_g" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_banner_g_visible"><?php echo $remates->banner_g_visible->FldCaption() ?></label>
		<div class="col-sm-9">
<select data-table="remates" data-field="x_banner_g_visible" data-value-separator="<?php echo $remates->banner_g_visible->DisplayValueSeparatorAttribute() ?>" id="x_banner_g_visible" name="x_banner_g_visible"<?php echo $remates->banner_g_visible->EditAttributes() ?>>
<?php echo $remates->banner_g_visible->SelectOptionListHtml("x_banner_g_visible") ?>
</select>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_banner_g_z_index"><?php echo $remates->banner_g_z_index->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_banner_g_z_index" name="x_banner_g_z_index" id="x_banner_g_z_index" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_z_index->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_z_index->EditValue ?>"<?php echo $remates->banner_g_z_index->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_banner_g_pos_x"><?php echo $remates->banner_g_pos_x->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_banner_g_pos_x" name="x_banner_g_pos_x" id="x_banner_g_pos_x" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_x->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_x->EditValue ?>"<?php echo $remates->banner_g_pos_x->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_banner_g_pos_y"><?php echo $remates->banner_g_pos_y->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_banner_g_pos_y" name="x_banner_g_pos_y" id="x_banner_g_pos_y" size="10" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_pos_y->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_pos_y->EditValue ?>"<?php echo $remates->banner_g_pos_y->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_banner_g_link"><?php echo $remates->banner_g_link->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_banner_g_link" name="x_banner_g_link" id="x_banner_g_link" size="50" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->banner_g_link->getPlaceHolder()) ?>" value="<?php echo $remates->banner_g_link->EditValue ?>"<?php echo $remates->banner_g_link->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->banner_ch->Visible) { // banner_ch ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->banner_ch->FldCaption() ?></label>
		<div class="col-sm-9">
<div id="fd_x_banner_ch">
<span title="<?php echo $remates->banner_ch->FldTitle() ? $remates->banner_ch->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->banner_ch->ReadOnly || $remates->banner_ch->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_banner_ch" name="x_banner_ch" id="x_banner_ch"<?php echo $remates->banner_ch->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_banner_ch" id= "fn_x_banner_ch" value="<?php echo $remates->banner_ch->Upload->FileName ?>">
<input type="hidden" name="fa_x_banner_ch" id= "fa_x_banner_ch" value="0">
<input type="hidden" name="fs_x_banner_ch" id= "fs_x_banner_ch" value="255">
<input type="hidden" name="fx_x_banner_ch" id= "fx_x_banner_ch" value="<?php echo $remates->banner_ch->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_banner_ch" id= "fm_x_banner_ch" value="<?php echo $remates->banner_ch->UploadMaxFileSize ?>">
</div>
<table id="ft_x_banner_ch" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</div>
	</div>
<?php } ?>	
<?php if ($remates->activo->Visible) { // activo ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->activo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<div id="tp_x_activo" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_activo" data-value-separator="<?php echo $remates->activo->DisplayValueSeparatorAttribute() ?>" name="x_activo" id="x_activo" value="{value}"<?php echo $remates->activo->EditAttributes() ?>></div>
<div id="dsl_x_activo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->activo->RadioButtonListHtml(FALSE, "x_activo") ?>
</div></div>
</div>
	</div>
<?php } ?>	
<?php if ($remates->archivo->Visible) { // archivo ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->archivo->FldCaption() ?></label>
		<div class="col-sm-9">
<div id="fd_x_archivo">
<span title="<?php echo $remates->archivo->FldTitle() ? $remates->archivo->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->archivo->ReadOnly || $remates->archivo->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_archivo" name="x_archivo" id="x_archivo"<?php echo $remates->archivo->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_archivo" id= "fn_x_archivo" value="<?php echo $remates->archivo->Upload->FileName ?>">
<input type="hidden" name="fa_x_archivo" id= "fa_x_archivo" value="0">
<input type="hidden" name="fs_x_archivo" id= "fs_x_archivo" value="255">
<input type="hidden" name="fx_x_archivo" id= "fx_x_archivo" value="<?php echo $remates->archivo->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_archivo" id= "fm_x_archivo" value="<?php echo $remates->archivo->UploadMaxFileSize ?>">
</div>
<table id="ft_x_archivo" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</div>
	</div>
<?php } ?>	
<?php if ($remates->precios->Visible) { // precios ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_precios"><?php echo $remates->precios->FldCaption() ?></label>
		<div class="col-sm-9">
<select data-table="remates" data-field="x_precios" data-value-separator="<?php echo $remates->precios->DisplayValueSeparatorAttribute() ?>" id="x_precios" name="x_precios"<?php echo $remates->precios->EditAttributes() ?>>
<?php echo $remates->precios->SelectOptionListHtml("x_precios") ?>
</select>
</div>
	</div>
<?php } ?>	
<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->mapa_img->FldCaption() ?></label>
		<div class="col-sm-9">
<div id="fd_x_mapa_img">
<span title="<?php echo $remates->mapa_img->FldTitle() ? $remates->mapa_img->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($remates->mapa_img->ReadOnly || $remates->mapa_img->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="remates" data-field="x_mapa_img" name="x_mapa_img" id="x_mapa_img"<?php echo $remates->mapa_img->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_mapa_img" id= "fn_x_mapa_img" value="<?php echo $remates->mapa_img->Upload->FileName ?>">
<input type="hidden" name="fa_x_mapa_img" id= "fa_x_mapa_img" value="0">
<input type="hidden" name="fs_x_mapa_img" id= "fs_x_mapa_img" value="255">
<input type="hidden" name="fx_x_mapa_img" id= "fx_x_mapa_img" value="<?php echo $remates->mapa_img->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_mapa_img" id= "fm_x_mapa_img" value="<?php echo $remates->mapa_img->UploadMaxFileSize ?>">
</div>
<table id="ft_x_mapa_img" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel"><?php echo $remates->video_final_servidor->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-9">
<div id="tp_x_video_final_servidor" class="ewTemplate"><input type="radio" data-table="remates" data-field="x_video_final_servidor" data-value-separator="<?php echo $remates->video_final_servidor->DisplayValueSeparatorAttribute() ?>" name="x_video_final_servidor" id="x_video_final_servidor" value="{value}"<?php echo $remates->video_final_servidor->EditAttributes() ?>></div>
<div id="dsl_x_video_final_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $remates->video_final_servidor->RadioButtonListHtml(FALSE, "x_video_final_servidor") ?>
</div></div>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_final_1"><?php echo $remates->video_final_1->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_final_1" name="x_video_final_1" id="x_video_final_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_1->EditValue ?>"<?php echo $remates->video_final_1->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_titu_1"><?php echo $remates->video_titu_1->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_titu_1" name="x_video_titu_1" id="x_video_titu_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_1->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_1->EditValue ?>"<?php echo $remates->video_titu_1->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_final_2"><?php echo $remates->video_final_2->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_final_2" name="x_video_final_2" id="x_video_final_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_2->EditValue ?>"<?php echo $remates->video_final_2->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_titu_2"><?php echo $remates->video_titu_2->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_titu_2" name="x_video_titu_2" id="x_video_titu_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_2->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_2->EditValue ?>"<?php echo $remates->video_titu_2->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_final_3"><?php echo $remates->video_final_3->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_final_3" name="x_video_final_3" id="x_video_final_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_3->EditValue ?>"<?php echo $remates->video_final_3->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_titu_3"><?php echo $remates->video_titu_3->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_titu_3" name="x_video_titu_3" id="x_video_titu_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_3->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_3->EditValue ?>"<?php echo $remates->video_titu_3->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_final_4"><?php echo $remates->video_final_4->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_final_4" name="x_video_final_4" id="x_video_final_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_4->EditValue ?>"<?php echo $remates->video_final_4->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_titu_4"><?php echo $remates->video_titu_4->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_titu_4" name="x_video_titu_4" id="x_video_titu_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_4->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_4->EditValue ?>"<?php echo $remates->video_titu_4->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_final_5"><?php echo $remates->video_final_5->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_final_5" name="x_video_final_5" id="x_video_final_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_final_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_final_5->EditValue ?>"<?php echo $remates->video_final_5->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
	<div class="form-group">
		<label class="col-sm-3 control-label ewLabel" for="x_video_titu_5"><?php echo $remates->video_titu_5->FldCaption() ?></label>
		<div class="col-sm-9">
<input type="text" data-table="remates" data-field="x_video_titu_5" name="x_video_titu_5" id="x_video_titu_5" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($remates->video_titu_5->getPlaceHolder()) ?>" value="<?php echo $remates->video_titu_5->EditValue ?>"<?php echo $remates->video_titu_5->EditAttributes() ?>>
</div>
	</div>
<?php } ?>	
</form>
<script type="text/javascript">
frematesaddopt.Init();
</script>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php
$remates_addopt->Page_Terminate();
?>
