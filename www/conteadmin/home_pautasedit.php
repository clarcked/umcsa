<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "home_pautasinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$home_pautas_edit = NULL; // Initialize page object first

class chome_pautas_edit extends chome_pautas {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'home_pautas';

	// Page object name
	var $PageObjName = 'home_pautas_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (home_pautas)
		if (!isset($GLOBALS["home_pautas"]) || get_class($GLOBALS["home_pautas"]) == "chome_pautas") {
			$GLOBALS["home_pautas"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["home_pautas"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'home_pautas', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("home_pautaslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->activa->SetVisibility();
		$this->fecha_desde->SetVisibility();
		$this->fecha_hasta->SetVisibility();
		$this->remate_1->SetVisibility();
		$this->remate_2->SetVisibility();
		$this->remate_3->SetVisibility();
		$this->remate_4->SetVisibility();
		$this->vivolink->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $home_pautas;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($home_pautas);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $IsModal = FALSE;
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "") {
			$this->Page_Terminate("home_pautaslist.php"); // Invalid key, return to list
		}

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("home_pautaslist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "home_pautaslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->activa->FldIsDetailKey) {
			$this->activa->setFormValue($objForm->GetValue("x_activa"));
		}
		if (!$this->fecha_desde->FldIsDetailKey) {
			$this->fecha_desde->setFormValue($objForm->GetValue("x_fecha_desde"));
			$this->fecha_desde->CurrentValue = ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0);
		}
		if (!$this->fecha_hasta->FldIsDetailKey) {
			$this->fecha_hasta->setFormValue($objForm->GetValue("x_fecha_hasta"));
			$this->fecha_hasta->CurrentValue = ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0);
		}
		if (!$this->remate_1->FldIsDetailKey) {
			$this->remate_1->setFormValue($objForm->GetValue("x_remate_1"));
		}
		if (!$this->remate_2->FldIsDetailKey) {
			$this->remate_2->setFormValue($objForm->GetValue("x_remate_2"));
		}
		if (!$this->remate_3->FldIsDetailKey) {
			$this->remate_3->setFormValue($objForm->GetValue("x_remate_3"));
		}
		if (!$this->remate_4->FldIsDetailKey) {
			$this->remate_4->setFormValue($objForm->GetValue("x_remate_4"));
		}
		if (!$this->vivolink->FldIsDetailKey) {
			$this->vivolink->setFormValue($objForm->GetValue("x_vivolink"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->activa->CurrentValue = $this->activa->FormValue;
		$this->fecha_desde->CurrentValue = $this->fecha_desde->FormValue;
		$this->fecha_desde->CurrentValue = ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0);
		$this->fecha_hasta->CurrentValue = $this->fecha_hasta->FormValue;
		$this->fecha_hasta->CurrentValue = ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0);
		$this->remate_1->CurrentValue = $this->remate_1->FormValue;
		$this->remate_2->CurrentValue = $this->remate_2->FormValue;
		$this->remate_3->CurrentValue = $this->remate_3->FormValue;
		$this->remate_4->CurrentValue = $this->remate_4->FormValue;
		$this->vivolink->CurrentValue = $this->vivolink->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->fecha_desde->setDbValue($rs->fields('fecha_desde'));
		$this->fecha_hasta->setDbValue($rs->fields('fecha_hasta'));
		$this->remate_1->setDbValue($rs->fields('remate_1'));
		$this->remate_2->setDbValue($rs->fields('remate_2'));
		$this->remate_3->setDbValue($rs->fields('remate_3'));
		$this->remate_4->setDbValue($rs->fields('remate_4'));
		$this->vivolink->setDbValue($rs->fields('vivolink'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->activa->DbValue = $row['activa'];
		$this->fecha_desde->DbValue = $row['fecha_desde'];
		$this->fecha_hasta->DbValue = $row['fecha_hasta'];
		$this->remate_1->DbValue = $row['remate_1'];
		$this->remate_2->DbValue = $row['remate_2'];
		$this->remate_3->DbValue = $row['remate_3'];
		$this->remate_4->DbValue = $row['remate_4'];
		$this->vivolink->DbValue = $row['vivolink'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// activa
		// fecha_desde
		// fecha_hasta
		// remate_1
		// remate_2
		// remate_3
		// remate_4
		// vivolink

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// fecha_desde
		$this->fecha_desde->ViewValue = $this->fecha_desde->CurrentValue;
		$this->fecha_desde->ViewValue = ew_FormatDateTime($this->fecha_desde->ViewValue, 0);
		$this->fecha_desde->ViewCustomAttributes = "";

		// fecha_hasta
		$this->fecha_hasta->ViewValue = $this->fecha_hasta->CurrentValue;
		$this->fecha_hasta->ViewValue = ew_FormatDateTime($this->fecha_hasta->ViewValue, 0);
		$this->fecha_hasta->ViewCustomAttributes = "";

		// remate_1
		if (strval($this->remate_1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_1->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_1->ViewValue = $this->remate_1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_1->ViewValue = $this->remate_1->CurrentValue;
			}
		} else {
			$this->remate_1->ViewValue = NULL;
		}
		$this->remate_1->ViewCustomAttributes = "";

		// remate_2
		if (strval($this->remate_2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_2->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_2->ViewValue = $this->remate_2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_2->ViewValue = $this->remate_2->CurrentValue;
			}
		} else {
			$this->remate_2->ViewValue = NULL;
		}
		$this->remate_2->ViewCustomAttributes = "";

		// remate_3
		if (strval($this->remate_3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_3->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_3->ViewValue = $this->remate_3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_3->ViewValue = $this->remate_3->CurrentValue;
			}
		} else {
			$this->remate_3->ViewValue = NULL;
		}
		$this->remate_3->ViewCustomAttributes = "";

		// remate_4
		if (strval($this->remate_4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_4->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_4->ViewValue = $this->remate_4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_4->ViewValue = $this->remate_4->CurrentValue;
			}
		} else {
			$this->remate_4->ViewValue = NULL;
		}
		$this->remate_4->ViewCustomAttributes = "";

		// vivolink
		$this->vivolink->ViewValue = $this->vivolink->CurrentValue;
		$this->vivolink->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";
			$this->fecha_desde->TooltipValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";
			$this->fecha_hasta->TooltipValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";
			$this->remate_1->TooltipValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";
			$this->remate_2->TooltipValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";
			$this->remate_3->TooltipValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";
			$this->remate_4->TooltipValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
			$this->vivolink->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->ViewCustomAttributes = "";

			// activa
			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// fecha_desde
			$this->fecha_desde->EditAttrs["class"] = "form-control";
			$this->fecha_desde->EditCustomAttributes = "";
			$this->fecha_desde->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_desde->CurrentValue, 8));
			$this->fecha_desde->PlaceHolder = ew_RemoveHtml($this->fecha_desde->FldCaption());

			// fecha_hasta
			$this->fecha_hasta->EditAttrs["class"] = "form-control";
			$this->fecha_hasta->EditCustomAttributes = "";
			$this->fecha_hasta->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_hasta->CurrentValue, 8));
			$this->fecha_hasta->PlaceHolder = ew_RemoveHtml($this->fecha_hasta->FldCaption());

			// remate_1
			$this->remate_1->EditAttrs["class"] = "form-control";
			$this->remate_1->EditCustomAttributes = "";
			if (trim(strval($this->remate_1->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_1->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_1->EditValue = $arwrk;

			// remate_2
			$this->remate_2->EditAttrs["class"] = "form-control";
			$this->remate_2->EditCustomAttributes = "";
			if (trim(strval($this->remate_2->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_2->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_2->EditValue = $arwrk;

			// remate_3
			$this->remate_3->EditAttrs["class"] = "form-control";
			$this->remate_3->EditCustomAttributes = "";
			if (trim(strval($this->remate_3->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_3->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_3->EditValue = $arwrk;

			// remate_4
			$this->remate_4->EditAttrs["class"] = "form-control";
			$this->remate_4->EditCustomAttributes = "";
			if (trim(strval($this->remate_4->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_4->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_4->EditValue = $arwrk;

			// vivolink
			$this->vivolink->EditAttrs["class"] = "form-control";
			$this->vivolink->EditCustomAttributes = "";
			$this->vivolink->EditValue = ew_HtmlEncode($this->vivolink->CurrentValue);
			$this->vivolink->PlaceHolder = ew_RemoveHtml($this->vivolink->FldCaption());

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->fecha_desde->FldIsDetailKey && !is_null($this->fecha_desde->FormValue) && $this->fecha_desde->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha_desde->FldCaption(), $this->fecha_desde->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha_desde->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_desde->FldErrMsg());
		}
		if (!$this->fecha_hasta->FldIsDetailKey && !is_null($this->fecha_hasta->FormValue) && $this->fecha_hasta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha_hasta->FldCaption(), $this->fecha_hasta->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha_hasta->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_hasta->FldErrMsg());
		}
		if (!$this->remate_1->FldIsDetailKey && !is_null($this->remate_1->FormValue) && $this->remate_1->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->remate_1->FldCaption(), $this->remate_1->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// activa
			$this->activa->SetDbValueDef($rsnew, $this->activa->CurrentValue, NULL, $this->activa->ReadOnly);

			// fecha_desde
			$this->fecha_desde->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0), ew_CurrentDate(), $this->fecha_desde->ReadOnly);

			// fecha_hasta
			$this->fecha_hasta->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0), ew_CurrentDate(), $this->fecha_hasta->ReadOnly);

			// remate_1
			$this->remate_1->SetDbValueDef($rsnew, $this->remate_1->CurrentValue, 0, $this->remate_1->ReadOnly);

			// remate_2
			$this->remate_2->SetDbValueDef($rsnew, $this->remate_2->CurrentValue, NULL, $this->remate_2->ReadOnly);

			// remate_3
			$this->remate_3->SetDbValueDef($rsnew, $this->remate_3->CurrentValue, NULL, $this->remate_3->ReadOnly);

			// remate_4
			$this->remate_4->SetDbValueDef($rsnew, $this->remate_4->CurrentValue, NULL, $this->remate_4->ReadOnly);

			// vivolink
			$this->vivolink->SetDbValueDef($rsnew, $this->vivolink->CurrentValue, NULL, $this->vivolink->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("home_pautaslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_remate_1":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_1->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_2":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_2->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_3":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_3->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_4":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_4->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($home_pautas_edit)) $home_pautas_edit = new chome_pautas_edit();

// Page init
$home_pautas_edit->Page_Init();

// Page main
$home_pautas_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$home_pautas_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fhome_pautasedit = new ew_Form("fhome_pautasedit", "edit");

// Validate form
fhome_pautasedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_fecha_desde");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->fecha_desde->FldCaption(), $home_pautas->fecha_desde->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha_desde");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($home_pautas->fecha_desde->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha_hasta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->fecha_hasta->FldCaption(), $home_pautas->fecha_hasta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha_hasta");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($home_pautas->fecha_hasta->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_remate_1");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->remate_1->FldCaption(), $home_pautas->remate_1->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fhome_pautasedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fhome_pautasedit.ValidateRequired = true;
<?php } else { ?>
fhome_pautasedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fhome_pautasedit.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fhome_pautasedit.Lists["x_activa"].Options = <?php echo json_encode($home_pautas->activa->Options()) ?>;
fhome_pautasedit.Lists["x_remate_1"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasedit.Lists["x_remate_2"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasedit.Lists["x_remate_3"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasedit.Lists["x_remate_4"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$home_pautas_edit->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $home_pautas_edit->ShowPageHeader(); ?>
<?php
$home_pautas_edit->ShowMessage();
?>
<form name="fhome_pautasedit" id="fhome_pautasedit" class="<?php echo $home_pautas_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($home_pautas_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $home_pautas_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="home_pautas">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($home_pautas_edit->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($home_pautas->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label id="elh_home_pautas_id" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->id->CellAttributes() ?>>
<span id="el_home_pautas_id">
<span<?php echo $home_pautas->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $home_pautas->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($home_pautas->id->CurrentValue) ?>">
<?php echo $home_pautas->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->activa->Visible) { // activa ?>
	<div id="r_activa" class="form-group">
		<label id="elh_home_pautas_activa" for="x_activa" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->activa->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->activa->CellAttributes() ?>>
<span id="el_home_pautas_activa">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x_activa" name="x_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x_activa") ?>
</select>
</span>
<?php echo $home_pautas->activa->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
	<div id="r_fecha_desde" class="form-group">
		<label id="elh_home_pautas_fecha_desde" for="x_fecha_desde" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->fecha_desde->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->fecha_desde->CellAttributes() ?>>
<span id="el_home_pautas_fecha_desde">
<input type="text" data-table="home_pautas" data-field="x_fecha_desde" name="x_fecha_desde" id="x_fecha_desde" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_desde->EditValue ?>"<?php echo $home_pautas->fecha_desde->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_desde->ReadOnly && !$home_pautas->fecha_desde->Disabled && !isset($home_pautas->fecha_desde->EditAttrs["readonly"]) && !isset($home_pautas->fecha_desde->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautasedit", "x_fecha_desde", 0);
</script>
<?php } ?>
</span>
<?php echo $home_pautas->fecha_desde->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
	<div id="r_fecha_hasta" class="form-group">
		<label id="elh_home_pautas_fecha_hasta" for="x_fecha_hasta" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->fecha_hasta->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->fecha_hasta->CellAttributes() ?>>
<span id="el_home_pautas_fecha_hasta">
<input type="text" data-table="home_pautas" data-field="x_fecha_hasta" name="x_fecha_hasta" id="x_fecha_hasta" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_hasta->EditValue ?>"<?php echo $home_pautas->fecha_hasta->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_hasta->ReadOnly && !$home_pautas->fecha_hasta->Disabled && !isset($home_pautas->fecha_hasta->EditAttrs["readonly"]) && !isset($home_pautas->fecha_hasta->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautasedit", "x_fecha_hasta", 0);
</script>
<?php } ?>
</span>
<?php echo $home_pautas->fecha_hasta->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
	<div id="r_remate_1" class="form-group">
		<label id="elh_home_pautas_remate_1" for="x_remate_1" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->remate_1->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->remate_1->CellAttributes() ?>>
<span id="el_home_pautas_remate_1">
<select data-table="home_pautas" data-field="x_remate_1" data-value-separator="<?php echo $home_pautas->remate_1->DisplayValueSeparatorAttribute() ?>" id="x_remate_1" name="x_remate_1"<?php echo $home_pautas->remate_1->EditAttributes() ?>>
<?php echo $home_pautas->remate_1->SelectOptionListHtml("x_remate_1") ?>
</select>
<input type="hidden" name="s_x_remate_1" id="s_x_remate_1" value="<?php echo $home_pautas->remate_1->LookupFilterQuery() ?>">
</span>
<?php echo $home_pautas->remate_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
	<div id="r_remate_2" class="form-group">
		<label id="elh_home_pautas_remate_2" for="x_remate_2" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->remate_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->remate_2->CellAttributes() ?>>
<span id="el_home_pautas_remate_2">
<select data-table="home_pautas" data-field="x_remate_2" data-value-separator="<?php echo $home_pautas->remate_2->DisplayValueSeparatorAttribute() ?>" id="x_remate_2" name="x_remate_2"<?php echo $home_pautas->remate_2->EditAttributes() ?>>
<?php echo $home_pautas->remate_2->SelectOptionListHtml("x_remate_2") ?>
</select>
<input type="hidden" name="s_x_remate_2" id="s_x_remate_2" value="<?php echo $home_pautas->remate_2->LookupFilterQuery() ?>">
</span>
<?php echo $home_pautas->remate_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
	<div id="r_remate_3" class="form-group">
		<label id="elh_home_pautas_remate_3" for="x_remate_3" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->remate_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->remate_3->CellAttributes() ?>>
<span id="el_home_pautas_remate_3">
<select data-table="home_pautas" data-field="x_remate_3" data-value-separator="<?php echo $home_pautas->remate_3->DisplayValueSeparatorAttribute() ?>" id="x_remate_3" name="x_remate_3"<?php echo $home_pautas->remate_3->EditAttributes() ?>>
<?php echo $home_pautas->remate_3->SelectOptionListHtml("x_remate_3") ?>
</select>
<input type="hidden" name="s_x_remate_3" id="s_x_remate_3" value="<?php echo $home_pautas->remate_3->LookupFilterQuery() ?>">
</span>
<?php echo $home_pautas->remate_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
	<div id="r_remate_4" class="form-group">
		<label id="elh_home_pautas_remate_4" for="x_remate_4" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->remate_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->remate_4->CellAttributes() ?>>
<span id="el_home_pautas_remate_4">
<select data-table="home_pautas" data-field="x_remate_4" data-value-separator="<?php echo $home_pautas->remate_4->DisplayValueSeparatorAttribute() ?>" id="x_remate_4" name="x_remate_4"<?php echo $home_pautas->remate_4->EditAttributes() ?>>
<?php echo $home_pautas->remate_4->SelectOptionListHtml("x_remate_4") ?>
</select>
<input type="hidden" name="s_x_remate_4" id="s_x_remate_4" value="<?php echo $home_pautas->remate_4->LookupFilterQuery() ?>">
</span>
<?php echo $home_pautas->remate_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
	<div id="r_vivolink" class="form-group">
		<label id="elh_home_pautas_vivolink" for="x_vivolink" class="col-sm-2 control-label ewLabel"><?php echo $home_pautas->vivolink->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $home_pautas->vivolink->CellAttributes() ?>>
<span id="el_home_pautas_vivolink">
<input type="text" data-table="home_pautas" data-field="x_vivolink" name="x_vivolink" id="x_vivolink" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($home_pautas->vivolink->getPlaceHolder()) ?>" value="<?php echo $home_pautas->vivolink->EditValue ?>"<?php echo $home_pautas->vivolink->EditAttributes() ?>>
</span>
<?php echo $home_pautas->vivolink->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php if (!$home_pautas_edit->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $home_pautas_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fhome_pautasedit.Init();
</script>
<?php
$home_pautas_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$home_pautas_edit->Page_Terminate();
?>
