<?php

// Global variable for table object
$staff = NULL;

//
// Table class for staff
//
class cstaff extends cTable {
	var $id;
	var $nombre;
	var $grupo1;
	var $grupo2;
	var $grupo3;
	var $grupo4;
	var $subgrupo;
	var $foto;
	var $cv;
	var $_email;
	var $tel;
	var $orden;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'staff';
		$this->TableName = 'staff';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`staff`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = TRUE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 20;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('staff', 'staff', 'x_id', 'id', '`id`', '`id`', 19, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// nombre
		$this->nombre = new cField('staff', 'staff', 'x_nombre', 'nombre', '`nombre`', '`nombre`', 200, -1, FALSE, '`nombre`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->nombre->Sortable = TRUE; // Allow sort
		$this->fields['nombre'] = &$this->nombre;

		// grupo1
		$this->grupo1 = new cField('staff', 'staff', 'x_grupo1', 'grupo1', '`grupo1`', '`grupo1`', 19, -1, FALSE, '`grupo1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->grupo1->Sortable = TRUE; // Allow sort
		$this->grupo1->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->grupo1->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->grupo1->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['grupo1'] = &$this->grupo1;

		// grupo2
		$this->grupo2 = new cField('staff', 'staff', 'x_grupo2', 'grupo2', '`grupo2`', '`grupo2`', 3, -1, FALSE, '`grupo2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->grupo2->Sortable = TRUE; // Allow sort
		$this->grupo2->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->grupo2->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->grupo2->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['grupo2'] = &$this->grupo2;

		// grupo3
		$this->grupo3 = new cField('staff', 'staff', 'x_grupo3', 'grupo3', '`grupo3`', '`grupo3`', 3, -1, FALSE, '`grupo3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->grupo3->Sortable = TRUE; // Allow sort
		$this->grupo3->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->grupo3->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->grupo3->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['grupo3'] = &$this->grupo3;

		// grupo4
		$this->grupo4 = new cField('staff', 'staff', 'x_grupo4', 'grupo4', '`grupo4`', '`grupo4`', 3, -1, FALSE, '`grupo4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->grupo4->Sortable = TRUE; // Allow sort
		$this->grupo4->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->grupo4->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->grupo4->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['grupo4'] = &$this->grupo4;

		// subgrupo
		$this->subgrupo = new cField('staff', 'staff', 'x_subgrupo', 'subgrupo', '`subgrupo`', '`subgrupo`', 19, -1, FALSE, '`subgrupo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->subgrupo->Sortable = TRUE; // Allow sort
		$this->subgrupo->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->subgrupo->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->subgrupo->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['subgrupo'] = &$this->subgrupo;

		// foto
		$this->foto = new cField('staff', 'staff', 'x_foto', 'foto', '`foto`', '`foto`', 200, -1, TRUE, '`foto`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->foto->Sortable = TRUE; // Allow sort
		$this->fields['foto'] = &$this->foto;

		// cv
		$this->cv = new cField('staff', 'staff', 'x_cv', 'cv', '`cv`', '`cv`', 200, -1, TRUE, '`cv`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->cv->Sortable = TRUE; // Allow sort
		$this->fields['cv'] = &$this->cv;

		// email
		$this->_email = new cField('staff', 'staff', 'x__email', 'email', '`email`', '`email`', 200, -1, FALSE, '`email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->_email->Sortable = TRUE; // Allow sort
		$this->fields['email'] = &$this->_email;

		// tel
		$this->tel = new cField('staff', 'staff', 'x_tel', 'tel', '`tel`', '`tel`', 200, -1, FALSE, '`tel`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->tel->Sortable = TRUE; // Allow sort
		$this->fields['tel'] = &$this->tel;

		// orden
		$this->orden = new cField('staff', 'staff', 'x_orden', 'orden', '`orden`', '`orden`', 3, -1, FALSE, '`orden`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->orden->Sortable = TRUE; // Allow sort
		$this->orden->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['orden'] = &$this->orden;
	}

	// Set Field Visibility
	function SetFieldVisibility($fldparm) {
		global $Security;
		return $this->$fldparm->Visible; // Returns original value
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`staff`";
	}

	function SqlFrom() { // For backward compatibility
		return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
		$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
		return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
		$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
		return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
		$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
		return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
		$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
		return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
		$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
		return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
		$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 8) == 8);
			case "search":
				return (($allow & 8) == 8);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id', $this->DBID) . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "stafflist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "stafflist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("staffview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("staffview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "staffadd.php?" . $this->UrlParm($parm);
		else
			$url = "staffadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("staffedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("staffadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("staffdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "id:" . ew_VarToJson($this->id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return $this->AddMasterUrl(ew_CurrentPage() . "?" . $sUrlParm);
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["id"]))
				$arKeys[] = ew_StripSlashes($_POST["id"]);
			elseif (isset($_GET["id"]))
				$arKeys[] = ew_StripSlashes($_GET["id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->nombre->setDbValue($rs->fields('nombre'));
		$this->grupo1->setDbValue($rs->fields('grupo1'));
		$this->grupo2->setDbValue($rs->fields('grupo2'));
		$this->grupo3->setDbValue($rs->fields('grupo3'));
		$this->grupo4->setDbValue($rs->fields('grupo4'));
		$this->subgrupo->setDbValue($rs->fields('subgrupo'));
		$this->foto->Upload->DbValue = $rs->fields('foto');
		$this->cv->Upload->DbValue = $rs->fields('cv');
		$this->_email->setDbValue($rs->fields('email'));
		$this->tel->setDbValue($rs->fields('tel'));
		$this->orden->setDbValue($rs->fields('orden'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// nombre
		// grupo1
		// grupo2
		// grupo3
		// grupo4
		// subgrupo
		// foto
		// cv
		// email
		// tel
		// orden
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// nombre
		$this->nombre->ViewValue = $this->nombre->CurrentValue;
		$this->nombre->ViewCustomAttributes = "";

		// grupo1
		if (strval($this->grupo1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo1->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo1->ViewValue = $this->grupo1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo1->ViewValue = $this->grupo1->CurrentValue;
			}
		} else {
			$this->grupo1->ViewValue = NULL;
		}
		$this->grupo1->ViewCustomAttributes = "";

		// grupo2
		if (strval($this->grupo2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo2->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo2->ViewValue = $this->grupo2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo2->ViewValue = $this->grupo2->CurrentValue;
			}
		} else {
			$this->grupo2->ViewValue = NULL;
		}
		$this->grupo2->ViewCustomAttributes = "";

		// grupo3
		if (strval($this->grupo3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo3->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo3->ViewValue = $this->grupo3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo3->ViewValue = $this->grupo3->CurrentValue;
			}
		} else {
			$this->grupo3->ViewValue = NULL;
		}
		$this->grupo3->ViewCustomAttributes = "";

		// grupo4
		if (strval($this->grupo4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->grupo4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_grupos`";
		$sWhereWrk = "";
		$this->grupo4->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->grupo4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->grupo4->ViewValue = $this->grupo4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->grupo4->ViewValue = $this->grupo4->CurrentValue;
			}
		} else {
			$this->grupo4->ViewValue = NULL;
		}
		$this->grupo4->ViewCustomAttributes = "";

		// subgrupo
		if (strval($this->subgrupo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->subgrupo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff_subgrupos`";
		$sWhereWrk = "";
		$this->subgrupo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->subgrupo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->subgrupo->ViewValue = $this->subgrupo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->subgrupo->ViewValue = $this->subgrupo->CurrentValue;
			}
		} else {
			$this->subgrupo->ViewValue = NULL;
		}
		$this->subgrupo->ViewCustomAttributes = "";

		// foto
		if (!ew_Empty($this->foto->Upload->DbValue)) {
			$this->foto->ViewValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->ViewValue = "";
		}
		$this->foto->ViewCustomAttributes = "";

		// cv
		if (!ew_Empty($this->cv->Upload->DbValue)) {
			$this->cv->ViewValue = $this->cv->Upload->DbValue;
		} else {
			$this->cv->ViewValue = "";
		}
		$this->cv->ViewCustomAttributes = "";

		// email
		$this->_email->ViewValue = $this->_email->CurrentValue;
		$this->_email->ViewCustomAttributes = "";

		// tel
		$this->tel->ViewValue = $this->tel->CurrentValue;
		$this->tel->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// nombre
		$this->nombre->LinkCustomAttributes = "";
		$this->nombre->HrefValue = "";
		$this->nombre->TooltipValue = "";

		// grupo1
		$this->grupo1->LinkCustomAttributes = "";
		$this->grupo1->HrefValue = "";
		$this->grupo1->TooltipValue = "";

		// grupo2
		$this->grupo2->LinkCustomAttributes = "";
		$this->grupo2->HrefValue = "";
		$this->grupo2->TooltipValue = "";

		// grupo3
		$this->grupo3->LinkCustomAttributes = "";
		$this->grupo3->HrefValue = "";
		$this->grupo3->TooltipValue = "";

		// grupo4
		$this->grupo4->LinkCustomAttributes = "";
		$this->grupo4->HrefValue = "";
		$this->grupo4->TooltipValue = "";

		// subgrupo
		$this->subgrupo->LinkCustomAttributes = "";
		$this->subgrupo->HrefValue = "";
		$this->subgrupo->TooltipValue = "";

		// foto
		$this->foto->LinkCustomAttributes = "";
		$this->foto->HrefValue = "";
		$this->foto->HrefValue2 = $this->foto->UploadPath . $this->foto->Upload->DbValue;
		$this->foto->TooltipValue = "";

		// cv
		$this->cv->LinkCustomAttributes = "";
		$this->cv->HrefValue = "";
		$this->cv->HrefValue2 = $this->cv->UploadPath . $this->cv->Upload->DbValue;
		$this->cv->TooltipValue = "";

		// email
		$this->_email->LinkCustomAttributes = "";
		$this->_email->HrefValue = "";
		$this->_email->TooltipValue = "";

		// tel
		$this->tel->LinkCustomAttributes = "";
		$this->tel->HrefValue = "";
		$this->tel->TooltipValue = "";

		// orden
		$this->orden->LinkCustomAttributes = "";
		$this->orden->HrefValue = "";
		$this->orden->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// nombre
		$this->nombre->EditAttrs["class"] = "form-control";
		$this->nombre->EditCustomAttributes = "";
		$this->nombre->EditValue = $this->nombre->CurrentValue;
		$this->nombre->PlaceHolder = ew_RemoveHtml($this->nombre->FldCaption());

		// grupo1
		$this->grupo1->EditAttrs["class"] = "form-control";
		$this->grupo1->EditCustomAttributes = "";

		// grupo2
		$this->grupo2->EditAttrs["class"] = "form-control";
		$this->grupo2->EditCustomAttributes = "";

		// grupo3
		$this->grupo3->EditAttrs["class"] = "form-control";
		$this->grupo3->EditCustomAttributes = "";

		// grupo4
		$this->grupo4->EditAttrs["class"] = "form-control";
		$this->grupo4->EditCustomAttributes = "";

		// subgrupo
		$this->subgrupo->EditAttrs["class"] = "form-control";
		$this->subgrupo->EditCustomAttributes = "";

		// foto
		$this->foto->EditAttrs["class"] = "form-control";
		$this->foto->EditCustomAttributes = "";
		if (!ew_Empty($this->foto->Upload->DbValue)) {
			$this->foto->EditValue = $this->foto->Upload->DbValue;
		} else {
			$this->foto->EditValue = "";
		}
		if (!ew_Empty($this->foto->CurrentValue))
			$this->foto->Upload->FileName = $this->foto->CurrentValue;

		// cv
		$this->cv->EditAttrs["class"] = "form-control";
		$this->cv->EditCustomAttributes = "";
		if (!ew_Empty($this->cv->Upload->DbValue)) {
			$this->cv->EditValue = $this->cv->Upload->DbValue;
		} else {
			$this->cv->EditValue = "";
		}
		if (!ew_Empty($this->cv->CurrentValue))
			$this->cv->Upload->FileName = $this->cv->CurrentValue;

		// email
		$this->_email->EditAttrs["class"] = "form-control";
		$this->_email->EditCustomAttributes = "";
		$this->_email->EditValue = $this->_email->CurrentValue;
		$this->_email->PlaceHolder = ew_RemoveHtml($this->_email->FldCaption());

		// tel
		$this->tel->EditAttrs["class"] = "form-control";
		$this->tel->EditCustomAttributes = "";
		$this->tel->EditValue = $this->tel->CurrentValue;
		$this->tel->PlaceHolder = ew_RemoveHtml($this->tel->FldCaption());

		// orden
		$this->orden->EditAttrs["class"] = "form-control";
		$this->orden->EditCustomAttributes = "";
		$this->orden->EditValue = $this->orden->CurrentValue;
		$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->nombre->Exportable) $Doc->ExportCaption($this->nombre);
					if ($this->grupo1->Exportable) $Doc->ExportCaption($this->grupo1);
					if ($this->grupo2->Exportable) $Doc->ExportCaption($this->grupo2);
					if ($this->grupo3->Exportable) $Doc->ExportCaption($this->grupo3);
					if ($this->grupo4->Exportable) $Doc->ExportCaption($this->grupo4);
					if ($this->subgrupo->Exportable) $Doc->ExportCaption($this->subgrupo);
					if ($this->foto->Exportable) $Doc->ExportCaption($this->foto);
					if ($this->cv->Exportable) $Doc->ExportCaption($this->cv);
					if ($this->_email->Exportable) $Doc->ExportCaption($this->_email);
					if ($this->tel->Exportable) $Doc->ExportCaption($this->tel);
					if ($this->orden->Exportable) $Doc->ExportCaption($this->orden);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->nombre->Exportable) $Doc->ExportCaption($this->nombre);
					if ($this->grupo1->Exportable) $Doc->ExportCaption($this->grupo1);
					if ($this->grupo2->Exportable) $Doc->ExportCaption($this->grupo2);
					if ($this->grupo3->Exportable) $Doc->ExportCaption($this->grupo3);
					if ($this->grupo4->Exportable) $Doc->ExportCaption($this->grupo4);
					if ($this->subgrupo->Exportable) $Doc->ExportCaption($this->subgrupo);
					if ($this->foto->Exportable) $Doc->ExportCaption($this->foto);
					if ($this->cv->Exportable) $Doc->ExportCaption($this->cv);
					if ($this->_email->Exportable) $Doc->ExportCaption($this->_email);
					if ($this->tel->Exportable) $Doc->ExportCaption($this->tel);
					if ($this->orden->Exportable) $Doc->ExportCaption($this->orden);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->nombre->Exportable) $Doc->ExportField($this->nombre);
						if ($this->grupo1->Exportable) $Doc->ExportField($this->grupo1);
						if ($this->grupo2->Exportable) $Doc->ExportField($this->grupo2);
						if ($this->grupo3->Exportable) $Doc->ExportField($this->grupo3);
						if ($this->grupo4->Exportable) $Doc->ExportField($this->grupo4);
						if ($this->subgrupo->Exportable) $Doc->ExportField($this->subgrupo);
						if ($this->foto->Exportable) $Doc->ExportField($this->foto);
						if ($this->cv->Exportable) $Doc->ExportField($this->cv);
						if ($this->_email->Exportable) $Doc->ExportField($this->_email);
						if ($this->tel->Exportable) $Doc->ExportField($this->tel);
						if ($this->orden->Exportable) $Doc->ExportField($this->orden);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->nombre->Exportable) $Doc->ExportField($this->nombre);
						if ($this->grupo1->Exportable) $Doc->ExportField($this->grupo1);
						if ($this->grupo2->Exportable) $Doc->ExportField($this->grupo2);
						if ($this->grupo3->Exportable) $Doc->ExportField($this->grupo3);
						if ($this->grupo4->Exportable) $Doc->ExportField($this->grupo4);
						if ($this->subgrupo->Exportable) $Doc->ExportField($this->subgrupo);
						if ($this->foto->Exportable) $Doc->ExportField($this->foto);
						if ($this->cv->Exportable) $Doc->ExportField($this->cv);
						if ($this->_email->Exportable) $Doc->ExportField($this->_email);
						if ($this->tel->Exportable) $Doc->ExportField($this->tel);
						if ($this->orden->Exportable) $Doc->ExportField($this->orden);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
