<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_list = NULL; // Initialize page object first

class clotes_list extends clotes {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_list';

	// Grid form hidden field names
	var $FormName = 'floteslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "lotesadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "lotesdelete.php";
		$this->MultiUpdateUrl = "lotesupdate.php";

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption floteslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to inline edit mode
				if ($this->CurrentAction == "edit")
					$this->InlineEditMode();

				// Switch to inline add mode
				if ($this->CurrentAction == "add" || $this->CurrentAction == "copy")
					$this->InlineAddMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Inline Update
					if (($this->CurrentAction == "update" || $this->CurrentAction == "overwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "edit")
						$this->InlineUpdate();

					// Insert Inline
					if ($this->CurrentAction == "insert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "add")
						$this->InlineInsert();

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Process filter list
			$this->ProcessFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->setKey("id", ""); // Clear inline edit key
		$this->precio->FormValue = ""; // Clear form value
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Switch to Inline Edit mode
	function InlineEditMode() {
		global $Security, $Language;
		if (!$Security->CanEdit())
			$this->Page_Terminate("login.php"); // Go to login page
		$bInlineEdit = TRUE;
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		} else {
			$bInlineEdit = FALSE;
		}
		if ($bInlineEdit) {
			if ($this->LoadRow()) {
				$this->setKey("id", $this->id->CurrentValue); // Set up inline edit key
				$_SESSION[EW_SESSION_INLINE_MODE] = "edit"; // Enable inline edit
			}
		}
	}

	// Perform update to Inline Edit record
	function InlineUpdate() {
		global $Language, $objForm, $gsFormError;
		$objForm->Index = 1; 
		$this->LoadFormValues(); // Get form values

		// Validate form
		$bInlineUpdate = TRUE;
		if (!$this->ValidateForm()) {	
			$bInlineUpdate = FALSE; // Form error, reset action
			$this->setFailureMessage($gsFormError);
		} else {
			$bInlineUpdate = FALSE;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			if ($this->SetupKeyValues($rowkey)) { // Set up key values
				if ($this->CheckInlineEditKey()) { // Check key
					$this->SendEmail = TRUE; // Send email on update success
					$bInlineUpdate = $this->EditRow(); // Update record
				} else {
					$bInlineUpdate = FALSE;
				}
			}
		}
		if ($bInlineUpdate) { // Update success
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
			$this->EventCancelled = TRUE; // Cancel event
			$this->CurrentAction = "edit"; // Stay in edit mode
		}
	}

	// Check Inline Edit key
	function CheckInlineEditKey() {

		//CheckInlineEditKey = True
		if (strval($this->getKey("id")) <> strval($this->id->CurrentValue))
			return FALSE;
		return TRUE;
	}

	// Switch to Inline Add mode
	function InlineAddMode() {
		global $Security, $Language;
		if (!$Security->CanAdd())
			$this->Page_Terminate("login.php"); // Return to login page
		if ($this->CurrentAction == "copy") {
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CurrentAction = "add";
			}
		}
		$_SESSION[EW_SESSION_INLINE_MODE] = "add"; // Enable inline add
	}

	// Perform update to Inline Add/Copy record
	function InlineInsert() {
		global $Language, $objForm, $gsFormError;
		$this->LoadOldRecord(); // Load old recordset
		$objForm->Index = 0;
		$this->LoadFormValues(); // Get form values

		// Validate form
		if (!$this->ValidateForm()) {
			$this->setFailureMessage($gsFormError); // Set validation error message
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
			return;
		}
		$this->SendEmail = TRUE; // Send email on add success
		if ($this->AddRow($this->OldRecordset)) { // Add record
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up add success message
			$this->ClearInlineMode(); // Clear inline add mode
		} else { // Add failed
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
		}
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_venta_tipo") && $objForm->HasValue("o_venta_tipo") && $this->venta_tipo->CurrentValue <> $this->venta_tipo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_activo") && $objForm->HasValue("o_activo") && $this->activo->CurrentValue <> $this->activo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_vendido") && $objForm->HasValue("o_vendido") && $this->vendido->CurrentValue <> $this->vendido->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_codigo") && $objForm->HasValue("o_codigo") && $this->codigo->CurrentValue <> $this->codigo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remate_id") && $objForm->HasValue("o_remate_id") && $this->remate_id->CurrentValue <> $this->remate_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_conjunto_lote_id") && $objForm->HasValue("o_conjunto_lote_id") && $this->conjunto_lote_id->CurrentValue <> $this->conjunto_lote_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_orden") && $objForm->HasValue("o_orden") && $this->orden->CurrentValue <> $this->orden->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_video") && $objForm->HasValue("o_video") && $this->video->CurrentValue <> $this->video->OldValue)
			return FALSE;
		if (!ew_Empty($this->foto_1->Upload->Value))
			return FALSE;
		if (!ew_Empty($this->foto_2->Upload->Value))
			return FALSE;
		if (!ew_Empty($this->foto_3->Upload->Value))
			return FALSE;
		if (!ew_Empty($this->foto_4->Upload->Value))
			return FALSE;
		if ($objForm->HasValue("x_precio") && $objForm->HasValue("o_precio") && $this->precio->CurrentValue <> $this->precio->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_fecha_film") && $objForm->HasValue("o_fecha_film") && $this->fecha_film->CurrentValue <> $this->fecha_film->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_establecimiento") && $objForm->HasValue("o_establecimiento") && $this->establecimiento->CurrentValue <> $this->establecimiento->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remitente") && $objForm->HasValue("o_remitente") && $this->remitente->CurrentValue <> $this->remitente->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_tipo") && $objForm->HasValue("o_tipo") && $this->tipo->CurrentValue <> $this->tipo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_categoria") && $objForm->HasValue("o_categoria") && $this->categoria->CurrentValue <> $this->categoria->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_sub_categoria") && $objForm->HasValue("o_sub_categoria") && $this->sub_categoria->CurrentValue <> $this->sub_categoria->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_cantidad") && $objForm->HasValue("o_cantidad") && $this->cantidad->CurrentValue <> $this->cantidad->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_peso") && $objForm->HasValue("o_peso") && $this->peso->CurrentValue <> $this->peso->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_caracteristicas") && $objForm->HasValue("o_caracteristicas") && $this->caracteristicas->CurrentValue <> $this->caracteristicas->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_provincia") && $objForm->HasValue("o_provincia") && $this->provincia->CurrentValue <> $this->provincia->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_localidad") && $objForm->HasValue("o_localidad") && $this->localidad->CurrentValue <> $this->localidad->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_lugar") && $objForm->HasValue("o_lugar") && $this->lugar->CurrentValue <> $this->lugar->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_titulo") && $objForm->HasValue("o_titulo") && $this->titulo->CurrentValue <> $this->titulo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_edad_aprox") && $objForm->HasValue("o_edad_aprox") && $this->edad_aprox->CurrentValue <> $this->edad_aprox->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_trazabilidad") && $objForm->HasValue("o_trazabilidad") && $this->trazabilidad->CurrentValue <> $this->trazabilidad->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_mio_mio") && $objForm->HasValue("o_mio_mio") && $this->mio_mio->CurrentValue <> $this->mio_mio->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_garrapata") && $objForm->HasValue("o_garrapata") && $this->garrapata->CurrentValue <> $this->garrapata->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_plazo") && $objForm->HasValue("o_plazo") && $this->plazo->CurrentValue <> $this->plazo->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_visitas") && $objForm->HasValue("o_visitas") && $this->visitas->CurrentValue <> $this->visitas->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {
		global $UserProfile;

		// Load server side filters
		if (EW_SEARCH_FILTER_OPTION == "Server") {
			$sSavedFilterList = $UserProfile->GetSearchFilters(CurrentUserName(), "floteslistsrch");
		} else {
			$sSavedFilterList = "";
		}

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->id->AdvancedSearch->ToJSON(), ","); // Field id
		$sFilterList = ew_Concat($sFilterList, $this->venta_tipo->AdvancedSearch->ToJSON(), ","); // Field venta_tipo
		$sFilterList = ew_Concat($sFilterList, $this->activo->AdvancedSearch->ToJSON(), ","); // Field activo
		$sFilterList = ew_Concat($sFilterList, $this->vendido->AdvancedSearch->ToJSON(), ","); // Field vendido
		$sFilterList = ew_Concat($sFilterList, $this->codigo->AdvancedSearch->ToJSON(), ","); // Field codigo
		$sFilterList = ew_Concat($sFilterList, $this->remate_id->AdvancedSearch->ToJSON(), ","); // Field remate_id
		$sFilterList = ew_Concat($sFilterList, $this->conjunto_lote_id->AdvancedSearch->ToJSON(), ","); // Field conjunto_lote_id
		$sFilterList = ew_Concat($sFilterList, $this->orden->AdvancedSearch->ToJSON(), ","); // Field orden
		$sFilterList = ew_Concat($sFilterList, $this->video->AdvancedSearch->ToJSON(), ","); // Field video
		$sFilterList = ew_Concat($sFilterList, $this->foto_1->AdvancedSearch->ToJSON(), ","); // Field foto_1
		$sFilterList = ew_Concat($sFilterList, $this->foto_2->AdvancedSearch->ToJSON(), ","); // Field foto_2
		$sFilterList = ew_Concat($sFilterList, $this->foto_3->AdvancedSearch->ToJSON(), ","); // Field foto_3
		$sFilterList = ew_Concat($sFilterList, $this->foto_4->AdvancedSearch->ToJSON(), ","); // Field foto_4
		$sFilterList = ew_Concat($sFilterList, $this->precio->AdvancedSearch->ToJSON(), ","); // Field precio
		$sFilterList = ew_Concat($sFilterList, $this->fecha_film->AdvancedSearch->ToJSON(), ","); // Field fecha_film
		$sFilterList = ew_Concat($sFilterList, $this->establecimiento->AdvancedSearch->ToJSON(), ","); // Field establecimiento
		$sFilterList = ew_Concat($sFilterList, $this->remitente->AdvancedSearch->ToJSON(), ","); // Field remitente
		$sFilterList = ew_Concat($sFilterList, $this->tipo->AdvancedSearch->ToJSON(), ","); // Field tipo
		$sFilterList = ew_Concat($sFilterList, $this->categoria->AdvancedSearch->ToJSON(), ","); // Field categoria
		$sFilterList = ew_Concat($sFilterList, $this->sub_categoria->AdvancedSearch->ToJSON(), ","); // Field sub_categoria
		$sFilterList = ew_Concat($sFilterList, $this->cantidad->AdvancedSearch->ToJSON(), ","); // Field cantidad
		$sFilterList = ew_Concat($sFilterList, $this->peso->AdvancedSearch->ToJSON(), ","); // Field peso
		$sFilterList = ew_Concat($sFilterList, $this->caracteristicas->AdvancedSearch->ToJSON(), ","); // Field caracteristicas
		$sFilterList = ew_Concat($sFilterList, $this->provincia->AdvancedSearch->ToJSON(), ","); // Field provincia
		$sFilterList = ew_Concat($sFilterList, $this->localidad->AdvancedSearch->ToJSON(), ","); // Field localidad
		$sFilterList = ew_Concat($sFilterList, $this->lugar->AdvancedSearch->ToJSON(), ","); // Field lugar
		$sFilterList = ew_Concat($sFilterList, $this->titulo->AdvancedSearch->ToJSON(), ","); // Field titulo
		$sFilterList = ew_Concat($sFilterList, $this->edad_aprox->AdvancedSearch->ToJSON(), ","); // Field edad_aprox
		$sFilterList = ew_Concat($sFilterList, $this->trazabilidad->AdvancedSearch->ToJSON(), ","); // Field trazabilidad
		$sFilterList = ew_Concat($sFilterList, $this->mio_mio->AdvancedSearch->ToJSON(), ","); // Field mio_mio
		$sFilterList = ew_Concat($sFilterList, $this->garrapata->AdvancedSearch->ToJSON(), ","); // Field garrapata
		$sFilterList = ew_Concat($sFilterList, $this->observaciones->AdvancedSearch->ToJSON(), ","); // Field observaciones
		$sFilterList = ew_Concat($sFilterList, $this->plazo->AdvancedSearch->ToJSON(), ","); // Field plazo
		$sFilterList = ew_Concat($sFilterList, $this->visitas->AdvancedSearch->ToJSON(), ","); // Field visitas
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}
		$sFilterList = preg_replace('/,$/', "", $sFilterList);

		// Return filter list in json
		if ($sFilterList <> "")
			$sFilterList = "\"data\":{" . $sFilterList . "}";
		if ($sSavedFilterList <> "") {
			if ($sFilterList <> "")
				$sFilterList .= ",";
			$sFilterList .= "\"filters\":" . $sSavedFilterList;
		}
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Process filter list
	function ProcessFilterList() {
		global $UserProfile;
		if (@$_POST["ajax"] == "savefilters") { // Save filter request (Ajax)
			$filters = ew_StripSlashes(@$_POST["filters"]);
			$UserProfile->SetSearchFilters(CurrentUserName(), "floteslistsrch", $filters);

			// Clean output buffer
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			echo ew_ArrayToJson(array(array("success" => TRUE))); // Success
			$this->Page_Terminate();
			exit();
		} elseif (@$_POST["cmd"] == "resetfilter") {
			$this->RestoreFilterList();
		}
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field id
		$this->id->AdvancedSearch->SearchValue = @$filter["x_id"];
		$this->id->AdvancedSearch->SearchOperator = @$filter["z_id"];
		$this->id->AdvancedSearch->SearchCondition = @$filter["v_id"];
		$this->id->AdvancedSearch->SearchValue2 = @$filter["y_id"];
		$this->id->AdvancedSearch->SearchOperator2 = @$filter["w_id"];
		$this->id->AdvancedSearch->Save();

		// Field venta_tipo
		$this->venta_tipo->AdvancedSearch->SearchValue = @$filter["x_venta_tipo"];
		$this->venta_tipo->AdvancedSearch->SearchOperator = @$filter["z_venta_tipo"];
		$this->venta_tipo->AdvancedSearch->SearchCondition = @$filter["v_venta_tipo"];
		$this->venta_tipo->AdvancedSearch->SearchValue2 = @$filter["y_venta_tipo"];
		$this->venta_tipo->AdvancedSearch->SearchOperator2 = @$filter["w_venta_tipo"];
		$this->venta_tipo->AdvancedSearch->Save();

		// Field activo
		$this->activo->AdvancedSearch->SearchValue = @$filter["x_activo"];
		$this->activo->AdvancedSearch->SearchOperator = @$filter["z_activo"];
		$this->activo->AdvancedSearch->SearchCondition = @$filter["v_activo"];
		$this->activo->AdvancedSearch->SearchValue2 = @$filter["y_activo"];
		$this->activo->AdvancedSearch->SearchOperator2 = @$filter["w_activo"];
		$this->activo->AdvancedSearch->Save();

		// Field vendido
		$this->vendido->AdvancedSearch->SearchValue = @$filter["x_vendido"];
		$this->vendido->AdvancedSearch->SearchOperator = @$filter["z_vendido"];
		$this->vendido->AdvancedSearch->SearchCondition = @$filter["v_vendido"];
		$this->vendido->AdvancedSearch->SearchValue2 = @$filter["y_vendido"];
		$this->vendido->AdvancedSearch->SearchOperator2 = @$filter["w_vendido"];
		$this->vendido->AdvancedSearch->Save();

		// Field codigo
		$this->codigo->AdvancedSearch->SearchValue = @$filter["x_codigo"];
		$this->codigo->AdvancedSearch->SearchOperator = @$filter["z_codigo"];
		$this->codigo->AdvancedSearch->SearchCondition = @$filter["v_codigo"];
		$this->codigo->AdvancedSearch->SearchValue2 = @$filter["y_codigo"];
		$this->codigo->AdvancedSearch->SearchOperator2 = @$filter["w_codigo"];
		$this->codigo->AdvancedSearch->Save();

		// Field remate_id
		$this->remate_id->AdvancedSearch->SearchValue = @$filter["x_remate_id"];
		$this->remate_id->AdvancedSearch->SearchOperator = @$filter["z_remate_id"];
		$this->remate_id->AdvancedSearch->SearchCondition = @$filter["v_remate_id"];
		$this->remate_id->AdvancedSearch->SearchValue2 = @$filter["y_remate_id"];
		$this->remate_id->AdvancedSearch->SearchOperator2 = @$filter["w_remate_id"];
		$this->remate_id->AdvancedSearch->Save();

		// Field conjunto_lote_id
		$this->conjunto_lote_id->AdvancedSearch->SearchValue = @$filter["x_conjunto_lote_id"];
		$this->conjunto_lote_id->AdvancedSearch->SearchOperator = @$filter["z_conjunto_lote_id"];
		$this->conjunto_lote_id->AdvancedSearch->SearchCondition = @$filter["v_conjunto_lote_id"];
		$this->conjunto_lote_id->AdvancedSearch->SearchValue2 = @$filter["y_conjunto_lote_id"];
		$this->conjunto_lote_id->AdvancedSearch->SearchOperator2 = @$filter["w_conjunto_lote_id"];
		$this->conjunto_lote_id->AdvancedSearch->Save();

		// Field orden
		$this->orden->AdvancedSearch->SearchValue = @$filter["x_orden"];
		$this->orden->AdvancedSearch->SearchOperator = @$filter["z_orden"];
		$this->orden->AdvancedSearch->SearchCondition = @$filter["v_orden"];
		$this->orden->AdvancedSearch->SearchValue2 = @$filter["y_orden"];
		$this->orden->AdvancedSearch->SearchOperator2 = @$filter["w_orden"];
		$this->orden->AdvancedSearch->Save();

		// Field video
		$this->video->AdvancedSearch->SearchValue = @$filter["x_video"];
		$this->video->AdvancedSearch->SearchOperator = @$filter["z_video"];
		$this->video->AdvancedSearch->SearchCondition = @$filter["v_video"];
		$this->video->AdvancedSearch->SearchValue2 = @$filter["y_video"];
		$this->video->AdvancedSearch->SearchOperator2 = @$filter["w_video"];
		$this->video->AdvancedSearch->Save();

		// Field foto_1
		$this->foto_1->AdvancedSearch->SearchValue = @$filter["x_foto_1"];
		$this->foto_1->AdvancedSearch->SearchOperator = @$filter["z_foto_1"];
		$this->foto_1->AdvancedSearch->SearchCondition = @$filter["v_foto_1"];
		$this->foto_1->AdvancedSearch->SearchValue2 = @$filter["y_foto_1"];
		$this->foto_1->AdvancedSearch->SearchOperator2 = @$filter["w_foto_1"];
		$this->foto_1->AdvancedSearch->Save();

		// Field foto_2
		$this->foto_2->AdvancedSearch->SearchValue = @$filter["x_foto_2"];
		$this->foto_2->AdvancedSearch->SearchOperator = @$filter["z_foto_2"];
		$this->foto_2->AdvancedSearch->SearchCondition = @$filter["v_foto_2"];
		$this->foto_2->AdvancedSearch->SearchValue2 = @$filter["y_foto_2"];
		$this->foto_2->AdvancedSearch->SearchOperator2 = @$filter["w_foto_2"];
		$this->foto_2->AdvancedSearch->Save();

		// Field foto_3
		$this->foto_3->AdvancedSearch->SearchValue = @$filter["x_foto_3"];
		$this->foto_3->AdvancedSearch->SearchOperator = @$filter["z_foto_3"];
		$this->foto_3->AdvancedSearch->SearchCondition = @$filter["v_foto_3"];
		$this->foto_3->AdvancedSearch->SearchValue2 = @$filter["y_foto_3"];
		$this->foto_3->AdvancedSearch->SearchOperator2 = @$filter["w_foto_3"];
		$this->foto_3->AdvancedSearch->Save();

		// Field foto_4
		$this->foto_4->AdvancedSearch->SearchValue = @$filter["x_foto_4"];
		$this->foto_4->AdvancedSearch->SearchOperator = @$filter["z_foto_4"];
		$this->foto_4->AdvancedSearch->SearchCondition = @$filter["v_foto_4"];
		$this->foto_4->AdvancedSearch->SearchValue2 = @$filter["y_foto_4"];
		$this->foto_4->AdvancedSearch->SearchOperator2 = @$filter["w_foto_4"];
		$this->foto_4->AdvancedSearch->Save();

		// Field precio
		$this->precio->AdvancedSearch->SearchValue = @$filter["x_precio"];
		$this->precio->AdvancedSearch->SearchOperator = @$filter["z_precio"];
		$this->precio->AdvancedSearch->SearchCondition = @$filter["v_precio"];
		$this->precio->AdvancedSearch->SearchValue2 = @$filter["y_precio"];
		$this->precio->AdvancedSearch->SearchOperator2 = @$filter["w_precio"];
		$this->precio->AdvancedSearch->Save();

		// Field fecha_film
		$this->fecha_film->AdvancedSearch->SearchValue = @$filter["x_fecha_film"];
		$this->fecha_film->AdvancedSearch->SearchOperator = @$filter["z_fecha_film"];
		$this->fecha_film->AdvancedSearch->SearchCondition = @$filter["v_fecha_film"];
		$this->fecha_film->AdvancedSearch->SearchValue2 = @$filter["y_fecha_film"];
		$this->fecha_film->AdvancedSearch->SearchOperator2 = @$filter["w_fecha_film"];
		$this->fecha_film->AdvancedSearch->Save();

		// Field establecimiento
		$this->establecimiento->AdvancedSearch->SearchValue = @$filter["x_establecimiento"];
		$this->establecimiento->AdvancedSearch->SearchOperator = @$filter["z_establecimiento"];
		$this->establecimiento->AdvancedSearch->SearchCondition = @$filter["v_establecimiento"];
		$this->establecimiento->AdvancedSearch->SearchValue2 = @$filter["y_establecimiento"];
		$this->establecimiento->AdvancedSearch->SearchOperator2 = @$filter["w_establecimiento"];
		$this->establecimiento->AdvancedSearch->Save();

		// Field remitente
		$this->remitente->AdvancedSearch->SearchValue = @$filter["x_remitente"];
		$this->remitente->AdvancedSearch->SearchOperator = @$filter["z_remitente"];
		$this->remitente->AdvancedSearch->SearchCondition = @$filter["v_remitente"];
		$this->remitente->AdvancedSearch->SearchValue2 = @$filter["y_remitente"];
		$this->remitente->AdvancedSearch->SearchOperator2 = @$filter["w_remitente"];
		$this->remitente->AdvancedSearch->Save();

		// Field tipo
		$this->tipo->AdvancedSearch->SearchValue = @$filter["x_tipo"];
		$this->tipo->AdvancedSearch->SearchOperator = @$filter["z_tipo"];
		$this->tipo->AdvancedSearch->SearchCondition = @$filter["v_tipo"];
		$this->tipo->AdvancedSearch->SearchValue2 = @$filter["y_tipo"];
		$this->tipo->AdvancedSearch->SearchOperator2 = @$filter["w_tipo"];
		$this->tipo->AdvancedSearch->Save();

		// Field categoria
		$this->categoria->AdvancedSearch->SearchValue = @$filter["x_categoria"];
		$this->categoria->AdvancedSearch->SearchOperator = @$filter["z_categoria"];
		$this->categoria->AdvancedSearch->SearchCondition = @$filter["v_categoria"];
		$this->categoria->AdvancedSearch->SearchValue2 = @$filter["y_categoria"];
		$this->categoria->AdvancedSearch->SearchOperator2 = @$filter["w_categoria"];
		$this->categoria->AdvancedSearch->Save();

		// Field sub_categoria
		$this->sub_categoria->AdvancedSearch->SearchValue = @$filter["x_sub_categoria"];
		$this->sub_categoria->AdvancedSearch->SearchOperator = @$filter["z_sub_categoria"];
		$this->sub_categoria->AdvancedSearch->SearchCondition = @$filter["v_sub_categoria"];
		$this->sub_categoria->AdvancedSearch->SearchValue2 = @$filter["y_sub_categoria"];
		$this->sub_categoria->AdvancedSearch->SearchOperator2 = @$filter["w_sub_categoria"];
		$this->sub_categoria->AdvancedSearch->Save();

		// Field cantidad
		$this->cantidad->AdvancedSearch->SearchValue = @$filter["x_cantidad"];
		$this->cantidad->AdvancedSearch->SearchOperator = @$filter["z_cantidad"];
		$this->cantidad->AdvancedSearch->SearchCondition = @$filter["v_cantidad"];
		$this->cantidad->AdvancedSearch->SearchValue2 = @$filter["y_cantidad"];
		$this->cantidad->AdvancedSearch->SearchOperator2 = @$filter["w_cantidad"];
		$this->cantidad->AdvancedSearch->Save();

		// Field peso
		$this->peso->AdvancedSearch->SearchValue = @$filter["x_peso"];
		$this->peso->AdvancedSearch->SearchOperator = @$filter["z_peso"];
		$this->peso->AdvancedSearch->SearchCondition = @$filter["v_peso"];
		$this->peso->AdvancedSearch->SearchValue2 = @$filter["y_peso"];
		$this->peso->AdvancedSearch->SearchOperator2 = @$filter["w_peso"];
		$this->peso->AdvancedSearch->Save();

		// Field caracteristicas
		$this->caracteristicas->AdvancedSearch->SearchValue = @$filter["x_caracteristicas"];
		$this->caracteristicas->AdvancedSearch->SearchOperator = @$filter["z_caracteristicas"];
		$this->caracteristicas->AdvancedSearch->SearchCondition = @$filter["v_caracteristicas"];
		$this->caracteristicas->AdvancedSearch->SearchValue2 = @$filter["y_caracteristicas"];
		$this->caracteristicas->AdvancedSearch->SearchOperator2 = @$filter["w_caracteristicas"];
		$this->caracteristicas->AdvancedSearch->Save();

		// Field provincia
		$this->provincia->AdvancedSearch->SearchValue = @$filter["x_provincia"];
		$this->provincia->AdvancedSearch->SearchOperator = @$filter["z_provincia"];
		$this->provincia->AdvancedSearch->SearchCondition = @$filter["v_provincia"];
		$this->provincia->AdvancedSearch->SearchValue2 = @$filter["y_provincia"];
		$this->provincia->AdvancedSearch->SearchOperator2 = @$filter["w_provincia"];
		$this->provincia->AdvancedSearch->Save();

		// Field localidad
		$this->localidad->AdvancedSearch->SearchValue = @$filter["x_localidad"];
		$this->localidad->AdvancedSearch->SearchOperator = @$filter["z_localidad"];
		$this->localidad->AdvancedSearch->SearchCondition = @$filter["v_localidad"];
		$this->localidad->AdvancedSearch->SearchValue2 = @$filter["y_localidad"];
		$this->localidad->AdvancedSearch->SearchOperator2 = @$filter["w_localidad"];
		$this->localidad->AdvancedSearch->Save();

		// Field lugar
		$this->lugar->AdvancedSearch->SearchValue = @$filter["x_lugar"];
		$this->lugar->AdvancedSearch->SearchOperator = @$filter["z_lugar"];
		$this->lugar->AdvancedSearch->SearchCondition = @$filter["v_lugar"];
		$this->lugar->AdvancedSearch->SearchValue2 = @$filter["y_lugar"];
		$this->lugar->AdvancedSearch->SearchOperator2 = @$filter["w_lugar"];
		$this->lugar->AdvancedSearch->Save();

		// Field titulo
		$this->titulo->AdvancedSearch->SearchValue = @$filter["x_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator = @$filter["z_titulo"];
		$this->titulo->AdvancedSearch->SearchCondition = @$filter["v_titulo"];
		$this->titulo->AdvancedSearch->SearchValue2 = @$filter["y_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator2 = @$filter["w_titulo"];
		$this->titulo->AdvancedSearch->Save();

		// Field edad_aprox
		$this->edad_aprox->AdvancedSearch->SearchValue = @$filter["x_edad_aprox"];
		$this->edad_aprox->AdvancedSearch->SearchOperator = @$filter["z_edad_aprox"];
		$this->edad_aprox->AdvancedSearch->SearchCondition = @$filter["v_edad_aprox"];
		$this->edad_aprox->AdvancedSearch->SearchValue2 = @$filter["y_edad_aprox"];
		$this->edad_aprox->AdvancedSearch->SearchOperator2 = @$filter["w_edad_aprox"];
		$this->edad_aprox->AdvancedSearch->Save();

		// Field trazabilidad
		$this->trazabilidad->AdvancedSearch->SearchValue = @$filter["x_trazabilidad"];
		$this->trazabilidad->AdvancedSearch->SearchOperator = @$filter["z_trazabilidad"];
		$this->trazabilidad->AdvancedSearch->SearchCondition = @$filter["v_trazabilidad"];
		$this->trazabilidad->AdvancedSearch->SearchValue2 = @$filter["y_trazabilidad"];
		$this->trazabilidad->AdvancedSearch->SearchOperator2 = @$filter["w_trazabilidad"];
		$this->trazabilidad->AdvancedSearch->Save();

		// Field mio_mio
		$this->mio_mio->AdvancedSearch->SearchValue = @$filter["x_mio_mio"];
		$this->mio_mio->AdvancedSearch->SearchOperator = @$filter["z_mio_mio"];
		$this->mio_mio->AdvancedSearch->SearchCondition = @$filter["v_mio_mio"];
		$this->mio_mio->AdvancedSearch->SearchValue2 = @$filter["y_mio_mio"];
		$this->mio_mio->AdvancedSearch->SearchOperator2 = @$filter["w_mio_mio"];
		$this->mio_mio->AdvancedSearch->Save();

		// Field garrapata
		$this->garrapata->AdvancedSearch->SearchValue = @$filter["x_garrapata"];
		$this->garrapata->AdvancedSearch->SearchOperator = @$filter["z_garrapata"];
		$this->garrapata->AdvancedSearch->SearchCondition = @$filter["v_garrapata"];
		$this->garrapata->AdvancedSearch->SearchValue2 = @$filter["y_garrapata"];
		$this->garrapata->AdvancedSearch->SearchOperator2 = @$filter["w_garrapata"];
		$this->garrapata->AdvancedSearch->Save();

		// Field observaciones
		$this->observaciones->AdvancedSearch->SearchValue = @$filter["x_observaciones"];
		$this->observaciones->AdvancedSearch->SearchOperator = @$filter["z_observaciones"];
		$this->observaciones->AdvancedSearch->SearchCondition = @$filter["v_observaciones"];
		$this->observaciones->AdvancedSearch->SearchValue2 = @$filter["y_observaciones"];
		$this->observaciones->AdvancedSearch->SearchOperator2 = @$filter["w_observaciones"];
		$this->observaciones->AdvancedSearch->Save();

		// Field plazo
		$this->plazo->AdvancedSearch->SearchValue = @$filter["x_plazo"];
		$this->plazo->AdvancedSearch->SearchOperator = @$filter["z_plazo"];
		$this->plazo->AdvancedSearch->SearchCondition = @$filter["v_plazo"];
		$this->plazo->AdvancedSearch->SearchValue2 = @$filter["y_plazo"];
		$this->plazo->AdvancedSearch->SearchOperator2 = @$filter["w_plazo"];
		$this->plazo->AdvancedSearch->Save();

		// Field visitas
		$this->visitas->AdvancedSearch->SearchValue = @$filter["x_visitas"];
		$this->visitas->AdvancedSearch->SearchOperator = @$filter["z_visitas"];
		$this->visitas->AdvancedSearch->SearchCondition = @$filter["v_visitas"];
		$this->visitas->AdvancedSearch->SearchValue2 = @$filter["y_visitas"];
		$this->visitas->AdvancedSearch->SearchOperator2 = @$filter["w_visitas"];
		$this->visitas->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->id, $Default, FALSE); // id
		$this->BuildSearchSql($sWhere, $this->venta_tipo, $Default, FALSE); // venta_tipo
		$this->BuildSearchSql($sWhere, $this->activo, $Default, FALSE); // activo
		$this->BuildSearchSql($sWhere, $this->vendido, $Default, FALSE); // vendido
		$this->BuildSearchSql($sWhere, $this->codigo, $Default, FALSE); // codigo
		$this->BuildSearchSql($sWhere, $this->remate_id, $Default, FALSE); // remate_id
		$this->BuildSearchSql($sWhere, $this->conjunto_lote_id, $Default, FALSE); // conjunto_lote_id
		$this->BuildSearchSql($sWhere, $this->orden, $Default, FALSE); // orden
		$this->BuildSearchSql($sWhere, $this->video, $Default, FALSE); // video
		$this->BuildSearchSql($sWhere, $this->foto_1, $Default, FALSE); // foto_1
		$this->BuildSearchSql($sWhere, $this->foto_2, $Default, FALSE); // foto_2
		$this->BuildSearchSql($sWhere, $this->foto_3, $Default, FALSE); // foto_3
		$this->BuildSearchSql($sWhere, $this->foto_4, $Default, FALSE); // foto_4
		$this->BuildSearchSql($sWhere, $this->precio, $Default, FALSE); // precio
		$this->BuildSearchSql($sWhere, $this->fecha_film, $Default, FALSE); // fecha_film
		$this->BuildSearchSql($sWhere, $this->establecimiento, $Default, FALSE); // establecimiento
		$this->BuildSearchSql($sWhere, $this->remitente, $Default, FALSE); // remitente
		$this->BuildSearchSql($sWhere, $this->tipo, $Default, FALSE); // tipo
		$this->BuildSearchSql($sWhere, $this->categoria, $Default, FALSE); // categoria
		$this->BuildSearchSql($sWhere, $this->sub_categoria, $Default, FALSE); // sub_categoria
		$this->BuildSearchSql($sWhere, $this->cantidad, $Default, FALSE); // cantidad
		$this->BuildSearchSql($sWhere, $this->peso, $Default, FALSE); // peso
		$this->BuildSearchSql($sWhere, $this->caracteristicas, $Default, FALSE); // caracteristicas
		$this->BuildSearchSql($sWhere, $this->provincia, $Default, FALSE); // provincia
		$this->BuildSearchSql($sWhere, $this->localidad, $Default, FALSE); // localidad
		$this->BuildSearchSql($sWhere, $this->lugar, $Default, FALSE); // lugar
		$this->BuildSearchSql($sWhere, $this->titulo, $Default, FALSE); // titulo
		$this->BuildSearchSql($sWhere, $this->edad_aprox, $Default, FALSE); // edad_aprox
		$this->BuildSearchSql($sWhere, $this->trazabilidad, $Default, FALSE); // trazabilidad
		$this->BuildSearchSql($sWhere, $this->mio_mio, $Default, FALSE); // mio_mio
		$this->BuildSearchSql($sWhere, $this->garrapata, $Default, FALSE); // garrapata
		$this->BuildSearchSql($sWhere, $this->observaciones, $Default, FALSE); // observaciones
		$this->BuildSearchSql($sWhere, $this->plazo, $Default, FALSE); // plazo
		$this->BuildSearchSql($sWhere, $this->visitas, $Default, FALSE); // visitas

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->id->AdvancedSearch->Save(); // id
			$this->venta_tipo->AdvancedSearch->Save(); // venta_tipo
			$this->activo->AdvancedSearch->Save(); // activo
			$this->vendido->AdvancedSearch->Save(); // vendido
			$this->codigo->AdvancedSearch->Save(); // codigo
			$this->remate_id->AdvancedSearch->Save(); // remate_id
			$this->conjunto_lote_id->AdvancedSearch->Save(); // conjunto_lote_id
			$this->orden->AdvancedSearch->Save(); // orden
			$this->video->AdvancedSearch->Save(); // video
			$this->foto_1->AdvancedSearch->Save(); // foto_1
			$this->foto_2->AdvancedSearch->Save(); // foto_2
			$this->foto_3->AdvancedSearch->Save(); // foto_3
			$this->foto_4->AdvancedSearch->Save(); // foto_4
			$this->precio->AdvancedSearch->Save(); // precio
			$this->fecha_film->AdvancedSearch->Save(); // fecha_film
			$this->establecimiento->AdvancedSearch->Save(); // establecimiento
			$this->remitente->AdvancedSearch->Save(); // remitente
			$this->tipo->AdvancedSearch->Save(); // tipo
			$this->categoria->AdvancedSearch->Save(); // categoria
			$this->sub_categoria->AdvancedSearch->Save(); // sub_categoria
			$this->cantidad->AdvancedSearch->Save(); // cantidad
			$this->peso->AdvancedSearch->Save(); // peso
			$this->caracteristicas->AdvancedSearch->Save(); // caracteristicas
			$this->provincia->AdvancedSearch->Save(); // provincia
			$this->localidad->AdvancedSearch->Save(); // localidad
			$this->lugar->AdvancedSearch->Save(); // lugar
			$this->titulo->AdvancedSearch->Save(); // titulo
			$this->edad_aprox->AdvancedSearch->Save(); // edad_aprox
			$this->trazabilidad->AdvancedSearch->Save(); // trazabilidad
			$this->mio_mio->AdvancedSearch->Save(); // mio_mio
			$this->garrapata->AdvancedSearch->Save(); // garrapata
			$this->observaciones->AdvancedSearch->Save(); // observaciones
			$this->plazo->AdvancedSearch->Save(); // plazo
			$this->visitas->AdvancedSearch->Save(); // visitas
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1)
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE || $Fld->FldDataType == EW_DATATYPE_TIME) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->codigo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->remate_id, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->conjunto_lote_id, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->orden, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->foto_4, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->establecimiento, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->remitente, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->tipo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->categoria, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->trazabilidad, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->mio_mio, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->garrapata, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSQL(&$Where, &$Fld, $arKeywords, $type) {
		global $EW_BASIC_SEARCH_IGNORE_PATTERN;
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if ($EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace($EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->venta_tipo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->activo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->vendido->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->codigo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remate_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->conjunto_lote_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->orden->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->foto_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->precio->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->fecha_film->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->establecimiento->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remitente->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->tipo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->categoria->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->sub_categoria->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->cantidad->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->peso->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->caracteristicas->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->provincia->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->localidad->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->lugar->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->titulo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->edad_aprox->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->trazabilidad->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->mio_mio->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->garrapata->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->observaciones->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->plazo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->visitas->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->id->AdvancedSearch->UnsetSession();
		$this->venta_tipo->AdvancedSearch->UnsetSession();
		$this->activo->AdvancedSearch->UnsetSession();
		$this->vendido->AdvancedSearch->UnsetSession();
		$this->codigo->AdvancedSearch->UnsetSession();
		$this->remate_id->AdvancedSearch->UnsetSession();
		$this->conjunto_lote_id->AdvancedSearch->UnsetSession();
		$this->orden->AdvancedSearch->UnsetSession();
		$this->video->AdvancedSearch->UnsetSession();
		$this->foto_1->AdvancedSearch->UnsetSession();
		$this->foto_2->AdvancedSearch->UnsetSession();
		$this->foto_3->AdvancedSearch->UnsetSession();
		$this->foto_4->AdvancedSearch->UnsetSession();
		$this->precio->AdvancedSearch->UnsetSession();
		$this->fecha_film->AdvancedSearch->UnsetSession();
		$this->establecimiento->AdvancedSearch->UnsetSession();
		$this->remitente->AdvancedSearch->UnsetSession();
		$this->tipo->AdvancedSearch->UnsetSession();
		$this->categoria->AdvancedSearch->UnsetSession();
		$this->sub_categoria->AdvancedSearch->UnsetSession();
		$this->cantidad->AdvancedSearch->UnsetSession();
		$this->peso->AdvancedSearch->UnsetSession();
		$this->caracteristicas->AdvancedSearch->UnsetSession();
		$this->provincia->AdvancedSearch->UnsetSession();
		$this->localidad->AdvancedSearch->UnsetSession();
		$this->lugar->AdvancedSearch->UnsetSession();
		$this->titulo->AdvancedSearch->UnsetSession();
		$this->edad_aprox->AdvancedSearch->UnsetSession();
		$this->trazabilidad->AdvancedSearch->UnsetSession();
		$this->mio_mio->AdvancedSearch->UnsetSession();
		$this->garrapata->AdvancedSearch->UnsetSession();
		$this->observaciones->AdvancedSearch->UnsetSession();
		$this->plazo->AdvancedSearch->UnsetSession();
		$this->visitas->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->id->AdvancedSearch->Load();
		$this->venta_tipo->AdvancedSearch->Load();
		$this->activo->AdvancedSearch->Load();
		$this->vendido->AdvancedSearch->Load();
		$this->codigo->AdvancedSearch->Load();
		$this->remate_id->AdvancedSearch->Load();
		$this->conjunto_lote_id->AdvancedSearch->Load();
		$this->orden->AdvancedSearch->Load();
		$this->video->AdvancedSearch->Load();
		$this->foto_1->AdvancedSearch->Load();
		$this->foto_2->AdvancedSearch->Load();
		$this->foto_3->AdvancedSearch->Load();
		$this->foto_4->AdvancedSearch->Load();
		$this->precio->AdvancedSearch->Load();
		$this->fecha_film->AdvancedSearch->Load();
		$this->establecimiento->AdvancedSearch->Load();
		$this->remitente->AdvancedSearch->Load();
		$this->tipo->AdvancedSearch->Load();
		$this->categoria->AdvancedSearch->Load();
		$this->sub_categoria->AdvancedSearch->Load();
		$this->cantidad->AdvancedSearch->Load();
		$this->peso->AdvancedSearch->Load();
		$this->caracteristicas->AdvancedSearch->Load();
		$this->provincia->AdvancedSearch->Load();
		$this->localidad->AdvancedSearch->Load();
		$this->lugar->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->edad_aprox->AdvancedSearch->Load();
		$this->trazabilidad->AdvancedSearch->Load();
		$this->mio_mio->AdvancedSearch->Load();
		$this->garrapata->AdvancedSearch->Load();
		$this->observaciones->AdvancedSearch->Load();
		$this->plazo->AdvancedSearch->Load();
		$this->visitas->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->venta_tipo); // venta_tipo
			$this->UpdateSort($this->activo); // activo
			$this->UpdateSort($this->vendido); // vendido
			$this->UpdateSort($this->codigo); // codigo
			$this->UpdateSort($this->remate_id); // remate_id
			$this->UpdateSort($this->conjunto_lote_id); // conjunto_lote_id
			$this->UpdateSort($this->orden); // orden
			$this->UpdateSort($this->video); // video
			$this->UpdateSort($this->foto_1); // foto_1
			$this->UpdateSort($this->foto_2); // foto_2
			$this->UpdateSort($this->foto_3); // foto_3
			$this->UpdateSort($this->foto_4); // foto_4
			$this->UpdateSort($this->precio); // precio
			$this->UpdateSort($this->fecha_film); // fecha_film
			$this->UpdateSort($this->establecimiento); // establecimiento
			$this->UpdateSort($this->remitente); // remitente
			$this->UpdateSort($this->tipo); // tipo
			$this->UpdateSort($this->categoria); // categoria
			$this->UpdateSort($this->sub_categoria); // sub_categoria
			$this->UpdateSort($this->cantidad); // cantidad
			$this->UpdateSort($this->peso); // peso
			$this->UpdateSort($this->caracteristicas); // caracteristicas
			$this->UpdateSort($this->provincia); // provincia
			$this->UpdateSort($this->localidad); // localidad
			$this->UpdateSort($this->lugar); // lugar
			$this->UpdateSort($this->titulo); // titulo
			$this->UpdateSort($this->edad_aprox); // edad_aprox
			$this->UpdateSort($this->trazabilidad); // trazabilidad
			$this->UpdateSort($this->mio_mio); // mio_mio
			$this->UpdateSort($this->garrapata); // garrapata
			$this->UpdateSort($this->plazo); // plazo
			$this->UpdateSort($this->visitas); // visitas
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
				$this->remate_id->setSort("DESC");
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->setSessionOrderByList($sOrderBy);
				$this->id->setSort("");
				$this->venta_tipo->setSort("");
				$this->activo->setSort("");
				$this->vendido->setSort("");
				$this->codigo->setSort("");
				$this->remate_id->setSort("");
				$this->conjunto_lote_id->setSort("");
				$this->orden->setSort("");
				$this->video->setSort("");
				$this->foto_1->setSort("");
				$this->foto_2->setSort("");
				$this->foto_3->setSort("");
				$this->foto_4->setSort("");
				$this->precio->setSort("");
				$this->fecha_film->setSort("");
				$this->establecimiento->setSort("");
				$this->remitente->setSort("");
				$this->tipo->setSort("");
				$this->categoria->setSort("");
				$this->sub_categoria->setSort("");
				$this->cantidad->setSort("");
				$this->peso->setSort("");
				$this->caracteristicas->setSort("");
				$this->provincia->setSort("");
				$this->localidad->setSort("");
				$this->lugar->setSort("");
				$this->titulo->setSort("");
				$this->edad_aprox->setSort("");
				$this->trazabilidad->setSort("");
				$this->mio_mio->setSort("");
				$this->garrapata->setSort("");
				$this->plazo->setSort("");
				$this->visitas->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = TRUE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = ($Security->CanDelete() || $Security->CanEdit());
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if (($this->CurrentAction == "add" || $this->CurrentAction == "copy") && $this->RowType == EW_ROWTYPE_ADD) { // Inline Add/Copy
			$this->ListOptions->CustomItem = "copy"; // Show copy column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
			$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
				"<a class=\"ewGridLink ewInlineInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("InsertLink") . "</a>&nbsp;" .
				"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
				"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"insert\"></div>";
			return;
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($this->CurrentAction == "edit" && $this->RowType == EW_ROWTYPE_EDIT) { // Inline-Edit
			$this->ListOptions->CustomItem = "edit"; // Show edit column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
					"<a class=\"ewGridLink ewInlineUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . ew_GetHashUrl($this->PageName(), $this->PageObjName . "_row_" . $this->RowCnt) . "');\">" . $Language->Phrase("UpdateLink") . "</a>&nbsp;" .
					"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
					"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"update\"></div>";
			$oListOpt->Body .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_key\" id=\"k" . $this->RowIndex . "_key\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\">";
			return;
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		$viewcaption = ew_HtmlTitle($Language->Phrase("ViewLink"));
		if ($Security->CanView()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . $viewcaption . "\" data-caption=\"" . $viewcaption . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		$editcaption = ew_HtmlTitle($Language->Phrase("EditLink"));
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" href=\"" . ew_HtmlEncode(ew_GetHashUrl($this->InlineEditUrl, $this->PageObjName . "_row_" . $this->RowCnt)) . "\">" . $Language->Phrase("InlineEditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		$copycaption = ew_HtmlTitle($Language->Phrase("CopyLink"));
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineCopyUrl) . "\">" . $Language->Phrase("InlineCopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->id->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("AddLink"));
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Inline Add
		$item = &$option->Add("inlineadd");
		$item->Body = "<a class=\"ewAddEdit ewInlineAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineAddUrl) . "\">" .$Language->Phrase("InlineAddLink") . "</a>";
		$item->Visible = ($this->InlineAddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Add multi delete
		$item = &$option->Add("multidelete");
		$item->Body = "<a class=\"ewAction ewMultiDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.floteslist,url:'" . $this->MultiDeleteUrl . "',msg:ewLanguage.Phrase('DeleteConfirmMsg')});return false;\">" . $Language->Phrase("DeleteSelectedLink") . "</a>";
		$item->Visible = ($Security->CanDelete());

		// Add multi update
		$item = &$option->Add("multiupdate");
		$item->Body = "<a class=\"ewAction ewMultiUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" data-table=\"lotes\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.floteslist,url:'" . $this->MultiUpdateUrl . "'});return false;\">" . $Language->Phrase("UpdateSelectedLink") . "</a>";
		$item->Visible = ($Security->CanEdit());

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"floteslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"floteslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.floteslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"floteslistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Advanced search button
		$item = &$this->SearchOptions->Add("advancedsearch");
		$item->Body = "<a class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" href=\"lotessrch.php\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		$item->Visible = TRUE;

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->foto_1->Upload->Index = $objForm->Index;
		$this->foto_1->Upload->UploadFile();
		$this->foto_1->CurrentValue = $this->foto_1->Upload->FileName;
		$this->foto_2->Upload->Index = $objForm->Index;
		$this->foto_2->Upload->UploadFile();
		$this->foto_2->CurrentValue = $this->foto_2->Upload->FileName;
		$this->foto_3->Upload->Index = $objForm->Index;
		$this->foto_3->Upload->UploadFile();
		$this->foto_3->CurrentValue = $this->foto_3->Upload->FileName;
		$this->foto_4->Upload->Index = $objForm->Index;
		$this->foto_4->Upload->UploadFile();
		$this->foto_4->CurrentValue = $this->foto_4->Upload->FileName;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->id->CurrentValue = NULL;
		$this->id->OldValue = $this->id->CurrentValue;
		$this->venta_tipo->CurrentValue = "REMATE";
		$this->venta_tipo->OldValue = $this->venta_tipo->CurrentValue;
		$this->activo->CurrentValue = "SI";
		$this->activo->OldValue = $this->activo->CurrentValue;
		$this->vendido->CurrentValue = "NO";
		$this->vendido->OldValue = $this->vendido->CurrentValue;
		$this->codigo->CurrentValue = NULL;
		$this->codigo->OldValue = $this->codigo->CurrentValue;
		$this->remate_id->CurrentValue = NULL;
		$this->remate_id->OldValue = $this->remate_id->CurrentValue;
		$this->conjunto_lote_id->CurrentValue = NULL;
		$this->conjunto_lote_id->OldValue = $this->conjunto_lote_id->CurrentValue;
		$this->orden->CurrentValue = NULL;
		$this->orden->OldValue = $this->orden->CurrentValue;
		$this->video->CurrentValue = NULL;
		$this->video->OldValue = $this->video->CurrentValue;
		$this->foto_1->Upload->DbValue = NULL;
		$this->foto_1->OldValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = NULL;
		$this->foto_2->OldValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = NULL;
		$this->foto_3->OldValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = NULL;
		$this->foto_4->OldValue = $this->foto_4->Upload->DbValue;
		$this->precio->CurrentValue = NULL;
		$this->precio->OldValue = $this->precio->CurrentValue;
		$this->fecha_film->CurrentValue = NULL;
		$this->fecha_film->OldValue = $this->fecha_film->CurrentValue;
		$this->establecimiento->CurrentValue = NULL;
		$this->establecimiento->OldValue = $this->establecimiento->CurrentValue;
		$this->remitente->CurrentValue = NULL;
		$this->remitente->OldValue = $this->remitente->CurrentValue;
		$this->tipo->CurrentValue = NULL;
		$this->tipo->OldValue = $this->tipo->CurrentValue;
		$this->categoria->CurrentValue = NULL;
		$this->categoria->OldValue = $this->categoria->CurrentValue;
		$this->sub_categoria->CurrentValue = NULL;
		$this->sub_categoria->OldValue = $this->sub_categoria->CurrentValue;
		$this->cantidad->CurrentValue = NULL;
		$this->cantidad->OldValue = $this->cantidad->CurrentValue;
		$this->peso->CurrentValue = NULL;
		$this->peso->OldValue = $this->peso->CurrentValue;
		$this->caracteristicas->CurrentValue = NULL;
		$this->caracteristicas->OldValue = $this->caracteristicas->CurrentValue;
		$this->provincia->CurrentValue = NULL;
		$this->provincia->OldValue = $this->provincia->CurrentValue;
		$this->localidad->CurrentValue = NULL;
		$this->localidad->OldValue = $this->localidad->CurrentValue;
		$this->lugar->CurrentValue = NULL;
		$this->lugar->OldValue = $this->lugar->CurrentValue;
		$this->titulo->CurrentValue = NULL;
		$this->titulo->OldValue = $this->titulo->CurrentValue;
		$this->edad_aprox->CurrentValue = NULL;
		$this->edad_aprox->OldValue = $this->edad_aprox->CurrentValue;
		$this->trazabilidad->CurrentValue = NULL;
		$this->trazabilidad->OldValue = $this->trazabilidad->CurrentValue;
		$this->mio_mio->CurrentValue = NULL;
		$this->mio_mio->OldValue = $this->mio_mio->CurrentValue;
		$this->garrapata->CurrentValue = NULL;
		$this->garrapata->OldValue = $this->garrapata->CurrentValue;
		$this->plazo->CurrentValue = NULL;
		$this->plazo->OldValue = $this->plazo->CurrentValue;
		$this->visitas->CurrentValue = NULL;
		$this->visitas->OldValue = $this->visitas->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_id"]);
		if ($this->id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->id->AdvancedSearch->SearchOperator = @$_GET["z_id"];

		// venta_tipo
		$this->venta_tipo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_venta_tipo"]);
		if ($this->venta_tipo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->venta_tipo->AdvancedSearch->SearchOperator = @$_GET["z_venta_tipo"];

		// activo
		$this->activo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_activo"]);
		if ($this->activo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->activo->AdvancedSearch->SearchOperator = @$_GET["z_activo"];

		// vendido
		$this->vendido->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_vendido"]);
		if ($this->vendido->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->vendido->AdvancedSearch->SearchOperator = @$_GET["z_vendido"];

		// codigo
		$this->codigo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_codigo"]);
		if ($this->codigo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->codigo->AdvancedSearch->SearchOperator = @$_GET["z_codigo"];

		// remate_id
		$this->remate_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remate_id"]);
		if ($this->remate_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remate_id->AdvancedSearch->SearchOperator = @$_GET["z_remate_id"];

		// conjunto_lote_id
		$this->conjunto_lote_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_conjunto_lote_id"]);
		if ($this->conjunto_lote_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->conjunto_lote_id->AdvancedSearch->SearchOperator = @$_GET["z_conjunto_lote_id"];

		// orden
		$this->orden->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_orden"]);
		if ($this->orden->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->orden->AdvancedSearch->SearchOperator = @$_GET["z_orden"];

		// video
		$this->video->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video"]);
		if ($this->video->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video->AdvancedSearch->SearchOperator = @$_GET["z_video"];

		// foto_1
		$this->foto_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_1"]);
		if ($this->foto_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_1->AdvancedSearch->SearchOperator = @$_GET["z_foto_1"];

		// foto_2
		$this->foto_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_2"]);
		if ($this->foto_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_2->AdvancedSearch->SearchOperator = @$_GET["z_foto_2"];

		// foto_3
		$this->foto_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_3"]);
		if ($this->foto_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_3->AdvancedSearch->SearchOperator = @$_GET["z_foto_3"];

		// foto_4
		$this->foto_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_foto_4"]);
		if ($this->foto_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->foto_4->AdvancedSearch->SearchOperator = @$_GET["z_foto_4"];

		// precio
		$this->precio->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_precio"]);
		if ($this->precio->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->precio->AdvancedSearch->SearchOperator = @$_GET["z_precio"];

		// fecha_film
		$this->fecha_film->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_fecha_film"]);
		if ($this->fecha_film->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->fecha_film->AdvancedSearch->SearchOperator = @$_GET["z_fecha_film"];

		// establecimiento
		$this->establecimiento->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_establecimiento"]);
		if ($this->establecimiento->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->establecimiento->AdvancedSearch->SearchOperator = @$_GET["z_establecimiento"];

		// remitente
		$this->remitente->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remitente"]);
		if ($this->remitente->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remitente->AdvancedSearch->SearchOperator = @$_GET["z_remitente"];

		// tipo
		$this->tipo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_tipo"]);
		if ($this->tipo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->tipo->AdvancedSearch->SearchOperator = @$_GET["z_tipo"];

		// categoria
		$this->categoria->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_categoria"]);
		if ($this->categoria->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->categoria->AdvancedSearch->SearchOperator = @$_GET["z_categoria"];

		// sub_categoria
		$this->sub_categoria->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_sub_categoria"]);
		if ($this->sub_categoria->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->sub_categoria->AdvancedSearch->SearchOperator = @$_GET["z_sub_categoria"];

		// cantidad
		$this->cantidad->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_cantidad"]);
		if ($this->cantidad->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->cantidad->AdvancedSearch->SearchOperator = @$_GET["z_cantidad"];

		// peso
		$this->peso->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_peso"]);
		if ($this->peso->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->peso->AdvancedSearch->SearchOperator = @$_GET["z_peso"];

		// caracteristicas
		$this->caracteristicas->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_caracteristicas"]);
		if ($this->caracteristicas->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->caracteristicas->AdvancedSearch->SearchOperator = @$_GET["z_caracteristicas"];

		// provincia
		$this->provincia->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_provincia"]);
		if ($this->provincia->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->provincia->AdvancedSearch->SearchOperator = @$_GET["z_provincia"];

		// localidad
		$this->localidad->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_localidad"]);
		if ($this->localidad->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->localidad->AdvancedSearch->SearchOperator = @$_GET["z_localidad"];

		// lugar
		$this->lugar->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_lugar"]);
		if ($this->lugar->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->lugar->AdvancedSearch->SearchOperator = @$_GET["z_lugar"];

		// titulo
		$this->titulo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_titulo"]);
		if ($this->titulo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->titulo->AdvancedSearch->SearchOperator = @$_GET["z_titulo"];

		// edad_aprox
		$this->edad_aprox->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_edad_aprox"]);
		if ($this->edad_aprox->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->edad_aprox->AdvancedSearch->SearchOperator = @$_GET["z_edad_aprox"];

		// trazabilidad
		$this->trazabilidad->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_trazabilidad"]);
		if ($this->trazabilidad->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->trazabilidad->AdvancedSearch->SearchOperator = @$_GET["z_trazabilidad"];

		// mio_mio
		$this->mio_mio->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_mio_mio"]);
		if ($this->mio_mio->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->mio_mio->AdvancedSearch->SearchOperator = @$_GET["z_mio_mio"];

		// garrapata
		$this->garrapata->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_garrapata"]);
		if ($this->garrapata->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->garrapata->AdvancedSearch->SearchOperator = @$_GET["z_garrapata"];

		// observaciones
		$this->observaciones->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_observaciones"]);
		if ($this->observaciones->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->observaciones->AdvancedSearch->SearchOperator = @$_GET["z_observaciones"];

		// plazo
		$this->plazo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_plazo"]);
		if ($this->plazo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->plazo->AdvancedSearch->SearchOperator = @$_GET["z_plazo"];

		// visitas
		$this->visitas->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_visitas"]);
		if ($this->visitas->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->visitas->AdvancedSearch->SearchOperator = @$_GET["z_visitas"];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->venta_tipo->FldIsDetailKey) {
			$this->venta_tipo->setFormValue($objForm->GetValue("x_venta_tipo"));
		}
		$this->venta_tipo->setOldValue($objForm->GetValue("o_venta_tipo"));
		if (!$this->activo->FldIsDetailKey) {
			$this->activo->setFormValue($objForm->GetValue("x_activo"));
		}
		$this->activo->setOldValue($objForm->GetValue("o_activo"));
		if (!$this->vendido->FldIsDetailKey) {
			$this->vendido->setFormValue($objForm->GetValue("x_vendido"));
		}
		$this->vendido->setOldValue($objForm->GetValue("o_vendido"));
		if (!$this->codigo->FldIsDetailKey) {
			$this->codigo->setFormValue($objForm->GetValue("x_codigo"));
		}
		$this->codigo->setOldValue($objForm->GetValue("o_codigo"));
		if (!$this->remate_id->FldIsDetailKey) {
			$this->remate_id->setFormValue($objForm->GetValue("x_remate_id"));
		}
		$this->remate_id->setOldValue($objForm->GetValue("o_remate_id"));
		if (!$this->conjunto_lote_id->FldIsDetailKey) {
			$this->conjunto_lote_id->setFormValue($objForm->GetValue("x_conjunto_lote_id"));
		}
		$this->conjunto_lote_id->setOldValue($objForm->GetValue("o_conjunto_lote_id"));
		if (!$this->orden->FldIsDetailKey) {
			$this->orden->setFormValue($objForm->GetValue("x_orden"));
		}
		$this->orden->setOldValue($objForm->GetValue("o_orden"));
		if (!$this->video->FldIsDetailKey) {
			$this->video->setFormValue($objForm->GetValue("x_video"));
		}
		$this->video->setOldValue($objForm->GetValue("o_video"));
		if (!$this->precio->FldIsDetailKey) {
			$this->precio->setFormValue($objForm->GetValue("x_precio"));
		}
		$this->precio->setOldValue($objForm->GetValue("o_precio"));
		if (!$this->fecha_film->FldIsDetailKey) {
			$this->fecha_film->setFormValue($objForm->GetValue("x_fecha_film"));
			$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		}
		$this->fecha_film->setOldValue($objForm->GetValue("o_fecha_film"));
		if (!$this->establecimiento->FldIsDetailKey) {
			$this->establecimiento->setFormValue($objForm->GetValue("x_establecimiento"));
		}
		$this->establecimiento->setOldValue($objForm->GetValue("o_establecimiento"));
		if (!$this->remitente->FldIsDetailKey) {
			$this->remitente->setFormValue($objForm->GetValue("x_remitente"));
		}
		$this->remitente->setOldValue($objForm->GetValue("o_remitente"));
		if (!$this->tipo->FldIsDetailKey) {
			$this->tipo->setFormValue($objForm->GetValue("x_tipo"));
		}
		$this->tipo->setOldValue($objForm->GetValue("o_tipo"));
		if (!$this->categoria->FldIsDetailKey) {
			$this->categoria->setFormValue($objForm->GetValue("x_categoria"));
		}
		$this->categoria->setOldValue($objForm->GetValue("o_categoria"));
		if (!$this->sub_categoria->FldIsDetailKey) {
			$this->sub_categoria->setFormValue($objForm->GetValue("x_sub_categoria"));
		}
		$this->sub_categoria->setOldValue($objForm->GetValue("o_sub_categoria"));
		if (!$this->cantidad->FldIsDetailKey) {
			$this->cantidad->setFormValue($objForm->GetValue("x_cantidad"));
		}
		$this->cantidad->setOldValue($objForm->GetValue("o_cantidad"));
		if (!$this->peso->FldIsDetailKey) {
			$this->peso->setFormValue($objForm->GetValue("x_peso"));
		}
		$this->peso->setOldValue($objForm->GetValue("o_peso"));
		if (!$this->caracteristicas->FldIsDetailKey) {
			$this->caracteristicas->setFormValue($objForm->GetValue("x_caracteristicas"));
		}
		$this->caracteristicas->setOldValue($objForm->GetValue("o_caracteristicas"));
		if (!$this->provincia->FldIsDetailKey) {
			$this->provincia->setFormValue($objForm->GetValue("x_provincia"));
		}
		$this->provincia->setOldValue($objForm->GetValue("o_provincia"));
		if (!$this->localidad->FldIsDetailKey) {
			$this->localidad->setFormValue($objForm->GetValue("x_localidad"));
		}
		$this->localidad->setOldValue($objForm->GetValue("o_localidad"));
		if (!$this->lugar->FldIsDetailKey) {
			$this->lugar->setFormValue($objForm->GetValue("x_lugar"));
		}
		$this->lugar->setOldValue($objForm->GetValue("o_lugar"));
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		$this->titulo->setOldValue($objForm->GetValue("o_titulo"));
		if (!$this->edad_aprox->FldIsDetailKey) {
			$this->edad_aprox->setFormValue($objForm->GetValue("x_edad_aprox"));
		}
		$this->edad_aprox->setOldValue($objForm->GetValue("o_edad_aprox"));
		if (!$this->trazabilidad->FldIsDetailKey) {
			$this->trazabilidad->setFormValue($objForm->GetValue("x_trazabilidad"));
		}
		$this->trazabilidad->setOldValue($objForm->GetValue("o_trazabilidad"));
		if (!$this->mio_mio->FldIsDetailKey) {
			$this->mio_mio->setFormValue($objForm->GetValue("x_mio_mio"));
		}
		$this->mio_mio->setOldValue($objForm->GetValue("o_mio_mio"));
		if (!$this->garrapata->FldIsDetailKey) {
			$this->garrapata->setFormValue($objForm->GetValue("x_garrapata"));
		}
		$this->garrapata->setOldValue($objForm->GetValue("o_garrapata"));
		if (!$this->plazo->FldIsDetailKey) {
			$this->plazo->setFormValue($objForm->GetValue("x_plazo"));
		}
		$this->plazo->setOldValue($objForm->GetValue("o_plazo"));
		if (!$this->visitas->FldIsDetailKey) {
			$this->visitas->setFormValue($objForm->GetValue("x_visitas"));
		}
		$this->visitas->setOldValue($objForm->GetValue("o_visitas"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->CurrentValue = $this->id->FormValue;
		$this->venta_tipo->CurrentValue = $this->venta_tipo->FormValue;
		$this->activo->CurrentValue = $this->activo->FormValue;
		$this->vendido->CurrentValue = $this->vendido->FormValue;
		$this->codigo->CurrentValue = $this->codigo->FormValue;
		$this->remate_id->CurrentValue = $this->remate_id->FormValue;
		$this->conjunto_lote_id->CurrentValue = $this->conjunto_lote_id->FormValue;
		$this->orden->CurrentValue = $this->orden->FormValue;
		$this->video->CurrentValue = $this->video->FormValue;
		$this->precio->CurrentValue = $this->precio->FormValue;
		$this->fecha_film->CurrentValue = $this->fecha_film->FormValue;
		$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		$this->establecimiento->CurrentValue = $this->establecimiento->FormValue;
		$this->remitente->CurrentValue = $this->remitente->FormValue;
		$this->tipo->CurrentValue = $this->tipo->FormValue;
		$this->categoria->CurrentValue = $this->categoria->FormValue;
		$this->sub_categoria->CurrentValue = $this->sub_categoria->FormValue;
		$this->cantidad->CurrentValue = $this->cantidad->FormValue;
		$this->peso->CurrentValue = $this->peso->FormValue;
		$this->caracteristicas->CurrentValue = $this->caracteristicas->FormValue;
		$this->provincia->CurrentValue = $this->provincia->FormValue;
		$this->localidad->CurrentValue = $this->localidad->FormValue;
		$this->lugar->CurrentValue = $this->lugar->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->edad_aprox->CurrentValue = $this->edad_aprox->FormValue;
		$this->trazabilidad->CurrentValue = $this->trazabilidad->FormValue;
		$this->mio_mio->CurrentValue = $this->mio_mio->FormValue;
		$this->garrapata->CurrentValue = $this->garrapata->FormValue;
		$this->plazo->CurrentValue = $this->plazo->FormValue;
		$this->visitas->CurrentValue = $this->visitas->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		if (array_key_exists('EV__remate_id', $rs->fields)) {
			$this->remate_id->VirtualValue = $rs->fields('EV__remate_id'); // Set up virtual field value
		} else {
			$this->remate_id->VirtualValue = ""; // Clear value
		}
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		if (array_key_exists('EV__conjunto_lote_id', $rs->fields)) {
			$this->conjunto_lote_id->VirtualValue = $rs->fields('EV__conjunto_lote_id'); // Set up virtual field value
		} else {
			$this->conjunto_lote_id->VirtualValue = ""; // Clear value
		}
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		if (array_key_exists('EV__tipo', $rs->fields)) {
			$this->tipo->VirtualValue = $rs->fields('EV__tipo'); // Set up virtual field value
		} else {
			$this->tipo->VirtualValue = ""; // Clear value
		}
		$this->categoria->setDbValue($rs->fields('categoria'));
		if (array_key_exists('EV__categoria', $rs->fields)) {
			$this->categoria->VirtualValue = $rs->fields('EV__categoria'); // Set up virtual field value
		} else {
			$this->categoria->VirtualValue = ""; // Clear value
		}
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		if (array_key_exists('EV__sub_categoria', $rs->fields)) {
			$this->sub_categoria->VirtualValue = $rs->fields('EV__sub_categoria'); // Set up virtual field value
		} else {
			$this->sub_categoria->VirtualValue = ""; // Clear value
		}
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		if (array_key_exists('EV__localidad', $rs->fields)) {
			$this->localidad->VirtualValue = $rs->fields('EV__localidad'); // Set up virtual field value
		} else {
			$this->localidad->VirtualValue = ""; // Clear value
		}
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->venta_tipo->DbValue = $row['venta_tipo'];
		$this->activo->DbValue = $row['activo'];
		$this->vendido->DbValue = $row['vendido'];
		$this->codigo->DbValue = $row['codigo'];
		$this->remate_id->DbValue = $row['remate_id'];
		$this->conjunto_lote_id->DbValue = $row['conjunto_lote_id'];
		$this->orden->DbValue = $row['orden'];
		$this->video->DbValue = $row['video'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->precio->DbValue = $row['precio'];
		$this->fecha_film->DbValue = $row['fecha_film'];
		$this->establecimiento->DbValue = $row['establecimiento'];
		$this->remitente->DbValue = $row['remitente'];
		$this->tipo->DbValue = $row['tipo'];
		$this->categoria->DbValue = $row['categoria'];
		$this->sub_categoria->DbValue = $row['sub_categoria'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->peso->DbValue = $row['peso'];
		$this->caracteristicas->DbValue = $row['caracteristicas'];
		$this->provincia->DbValue = $row['provincia'];
		$this->localidad->DbValue = $row['localidad'];
		$this->lugar->DbValue = $row['lugar'];
		$this->titulo->DbValue = $row['titulo'];
		$this->edad_aprox->DbValue = $row['edad_aprox'];
		$this->trazabilidad->DbValue = $row['trazabilidad'];
		$this->mio_mio->DbValue = $row['mio_mio'];
		$this->garrapata->DbValue = $row['garrapata'];
		$this->observaciones->DbValue = $row['observaciones'];
		$this->plazo->DbValue = $row['plazo'];
		$this->visitas->DbValue = $row['visitas'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Convert decimal values if posted back
		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// id
			// venta_tipo

			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->CurrentValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			if (trim(strval($this->remate_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->remate_id->EditValue = $arwrk;

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			if (trim(strval($this->conjunto_lote_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `remate_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `conjuntos_lotes`";
			$sWhereWrk = "";
			$this->conjunto_lote_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->conjunto_lote_id->EditValue = $arwrk;

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->CurrentValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->CurrentValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_1->Upload->DbValue)) {
				$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
			} else {
				$this->foto_1->EditValue = "";
			}
			if (!ew_Empty($this->foto_1->CurrentValue))
				$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_1, $this->RowIndex);

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_2->Upload->DbValue)) {
				$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
			} else {
				$this->foto_2->EditValue = "";
			}
			if (!ew_Empty($this->foto_2->CurrentValue))
				$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_2, $this->RowIndex);

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_3->Upload->DbValue)) {
				$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
			} else {
				$this->foto_3->EditValue = "";
			}
			if (!ew_Empty($this->foto_3->CurrentValue))
				$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_3, $this->RowIndex);

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_4->Upload->DbValue)) {
				$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
			} else {
				$this->foto_4->EditValue = "";
			}
			if (!ew_Empty($this->foto_4->CurrentValue))
				$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_4, $this->RowIndex);

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->CurrentValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());
			if (strval($this->precio->EditValue) <> "" && is_numeric($this->precio->EditValue)) {
			$this->precio->EditValue = ew_FormatNumber($this->precio->EditValue, -2, -1, -2, 0);
			$this->precio->OldValue = $this->precio->EditValue;
			}

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_film->CurrentValue, 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->CurrentValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->CurrentValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			if (trim(strval($this->tipo->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->tipo->EditValue = $arwrk;

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			if (trim(strval($this->categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->categoria->EditValue = $arwrk;

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			if (trim(strval($this->sub_categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->sub_categoria->EditValue = $arwrk;

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->CurrentValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->CurrentValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";
			if (trim(strval($this->provincia->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->provincia->EditValue = $arwrk;

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->CurrentValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->CurrentValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->CurrentValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->CurrentValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());

			// Add refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// venta_tipo
			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->CurrentValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			if (trim(strval($this->remate_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->remate_id->EditValue = $arwrk;

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			if (trim(strval($this->conjunto_lote_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `remate_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `conjuntos_lotes`";
			$sWhereWrk = "";
			$this->conjunto_lote_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->conjunto_lote_id->EditValue = $arwrk;

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->CurrentValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->CurrentValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_1->Upload->DbValue)) {
				$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
			} else {
				$this->foto_1->EditValue = "";
			}
			if (!ew_Empty($this->foto_1->CurrentValue))
				$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_1, $this->RowIndex);

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_2->Upload->DbValue)) {
				$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
			} else {
				$this->foto_2->EditValue = "";
			}
			if (!ew_Empty($this->foto_2->CurrentValue))
				$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_2, $this->RowIndex);

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_3->Upload->DbValue)) {
				$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
			} else {
				$this->foto_3->EditValue = "";
			}
			if (!ew_Empty($this->foto_3->CurrentValue))
				$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_3, $this->RowIndex);

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_4->Upload->DbValue)) {
				$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
			} else {
				$this->foto_4->EditValue = "";
			}
			if (!ew_Empty($this->foto_4->CurrentValue))
				$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;
			if (is_numeric($this->RowIndex) && !$this->EventCancelled) ew_RenderUploadField($this->foto_4, $this->RowIndex);

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->CurrentValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());
			if (strval($this->precio->EditValue) <> "" && is_numeric($this->precio->EditValue)) {
			$this->precio->EditValue = ew_FormatNumber($this->precio->EditValue, -2, -1, -2, 0);
			$this->precio->OldValue = $this->precio->EditValue;
			}

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_film->CurrentValue, 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->CurrentValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->CurrentValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			if (trim(strval($this->tipo->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->tipo->EditValue = $arwrk;

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			if (trim(strval($this->categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->categoria->EditValue = $arwrk;

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			if (trim(strval($this->sub_categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->sub_categoria->EditValue = $arwrk;

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->CurrentValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->CurrentValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";
			if (trim(strval($this->provincia->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->provincia->EditValue = $arwrk;

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->CurrentValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->CurrentValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->CurrentValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->CurrentValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// venta_tipo
			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->AdvancedSearch->SearchValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			$this->remate_id->EditValue = ew_HtmlEncode($this->remate_id->AdvancedSearch->SearchValue);
			$this->remate_id->PlaceHolder = ew_RemoveHtml($this->remate_id->FldCaption());

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			$this->conjunto_lote_id->EditValue = ew_HtmlEncode($this->conjunto_lote_id->AdvancedSearch->SearchValue);
			$this->conjunto_lote_id->PlaceHolder = ew_RemoveHtml($this->conjunto_lote_id->FldCaption());

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->AdvancedSearch->SearchValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->AdvancedSearch->SearchValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			$this->foto_1->EditValue = ew_HtmlEncode($this->foto_1->AdvancedSearch->SearchValue);
			$this->foto_1->PlaceHolder = ew_RemoveHtml($this->foto_1->FldCaption());

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			$this->foto_2->EditValue = ew_HtmlEncode($this->foto_2->AdvancedSearch->SearchValue);
			$this->foto_2->PlaceHolder = ew_RemoveHtml($this->foto_2->FldCaption());

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			$this->foto_3->EditValue = ew_HtmlEncode($this->foto_3->AdvancedSearch->SearchValue);
			$this->foto_3->PlaceHolder = ew_RemoveHtml($this->foto_3->FldCaption());

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			$this->foto_4->EditValue = ew_HtmlEncode($this->foto_4->AdvancedSearch->SearchValue);
			$this->foto_4->PlaceHolder = ew_RemoveHtml($this->foto_4->FldCaption());

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->AdvancedSearch->SearchValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha_film->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->AdvancedSearch->SearchValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->AdvancedSearch->SearchValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = ew_HtmlEncode($this->tipo->AdvancedSearch->SearchValue);
			$this->tipo->PlaceHolder = ew_RemoveHtml($this->tipo->FldCaption());

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			$this->categoria->EditValue = ew_HtmlEncode($this->categoria->AdvancedSearch->SearchValue);
			$this->categoria->PlaceHolder = ew_RemoveHtml($this->categoria->FldCaption());

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			$this->sub_categoria->EditValue = ew_HtmlEncode($this->sub_categoria->AdvancedSearch->SearchValue);
			$this->sub_categoria->PlaceHolder = ew_RemoveHtml($this->sub_categoria->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->AdvancedSearch->SearchValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->AdvancedSearch->SearchValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->AdvancedSearch->SearchValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->AdvancedSearch->SearchValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->AdvancedSearch->SearchValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->AdvancedSearch->SearchValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->AdvancedSearch->SearchValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->AdvancedSearch->SearchValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->AdvancedSearch->SearchValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckNumber($this->precio->FormValue)) {
			ew_AddMessage($gsFormError, $this->precio->FldErrMsg());
		}
		if (!ew_CheckDateDef($this->fecha_film->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_film->FldErrMsg());
		}
		if (!$this->categoria->FldIsDetailKey && !is_null($this->categoria->FormValue) && $this->categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->categoria->FldCaption(), $this->categoria->ReqErrMsg));
		}
		if (!$this->sub_categoria->FldIsDetailKey && !is_null($this->sub_categoria->FormValue) && $this->sub_categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->sub_categoria->FldCaption(), $this->sub_categoria->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->cantidad->FormValue)) {
			ew_AddMessage($gsFormError, $this->cantidad->FldErrMsg());
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->visitas->FormValue)) {
			ew_AddMessage($gsFormError, $this->visitas->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// venta_tipo
			$this->venta_tipo->SetDbValueDef($rsnew, $this->venta_tipo->CurrentValue, NULL, $this->venta_tipo->ReadOnly);

			// activo
			$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, NULL, $this->activo->ReadOnly);

			// vendido
			$this->vendido->SetDbValueDef($rsnew, $this->vendido->CurrentValue, NULL, $this->vendido->ReadOnly);

			// codigo
			$this->codigo->SetDbValueDef($rsnew, $this->codigo->CurrentValue, NULL, $this->codigo->ReadOnly);

			// remate_id
			$this->remate_id->SetDbValueDef($rsnew, $this->remate_id->CurrentValue, NULL, $this->remate_id->ReadOnly);

			// conjunto_lote_id
			$this->conjunto_lote_id->SetDbValueDef($rsnew, $this->conjunto_lote_id->CurrentValue, NULL, $this->conjunto_lote_id->ReadOnly);

			// orden
			$this->orden->SetDbValueDef($rsnew, $this->orden->CurrentValue, NULL, $this->orden->ReadOnly);

			// video
			$this->video->SetDbValueDef($rsnew, $this->video->CurrentValue, NULL, $this->video->ReadOnly);

			// foto_1
			if ($this->foto_1->Visible && !$this->foto_1->ReadOnly && !$this->foto_1->Upload->KeepFile) {
				$this->foto_1->Upload->DbValue = $rsold['foto_1']; // Get original value
				if ($this->foto_1->Upload->FileName == "") {
					$rsnew['foto_1'] = NULL;
				} else {
					$rsnew['foto_1'] = $this->foto_1->Upload->FileName;
				}
				$this->foto_1->ImageWidth = 750; // Resize width
				$this->foto_1->ImageHeight = 0; // Resize height
			}

			// foto_2
			if ($this->foto_2->Visible && !$this->foto_2->ReadOnly && !$this->foto_2->Upload->KeepFile) {
				$this->foto_2->Upload->DbValue = $rsold['foto_2']; // Get original value
				if ($this->foto_2->Upload->FileName == "") {
					$rsnew['foto_2'] = NULL;
				} else {
					$rsnew['foto_2'] = $this->foto_2->Upload->FileName;
				}
				$this->foto_2->ImageWidth = 750; // Resize width
				$this->foto_2->ImageHeight = 0; // Resize height
			}

			// foto_3
			if ($this->foto_3->Visible && !$this->foto_3->ReadOnly && !$this->foto_3->Upload->KeepFile) {
				$this->foto_3->Upload->DbValue = $rsold['foto_3']; // Get original value
				if ($this->foto_3->Upload->FileName == "") {
					$rsnew['foto_3'] = NULL;
				} else {
					$rsnew['foto_3'] = $this->foto_3->Upload->FileName;
				}
				$this->foto_3->ImageWidth = 750; // Resize width
				$this->foto_3->ImageHeight = 0; // Resize height
			}

			// foto_4
			if ($this->foto_4->Visible && !$this->foto_4->ReadOnly && !$this->foto_4->Upload->KeepFile) {
				$this->foto_4->Upload->DbValue = $rsold['foto_4']; // Get original value
				if ($this->foto_4->Upload->FileName == "") {
					$rsnew['foto_4'] = NULL;
				} else {
					$rsnew['foto_4'] = $this->foto_4->Upload->FileName;
				}
				$this->foto_4->ImageWidth = 750; // Resize width
				$this->foto_4->ImageHeight = 0; // Resize height
			}

			// precio
			$this->precio->SetDbValueDef($rsnew, $this->precio->CurrentValue, NULL, $this->precio->ReadOnly);

			// fecha_film
			$this->fecha_film->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0), NULL, $this->fecha_film->ReadOnly);

			// establecimiento
			$this->establecimiento->SetDbValueDef($rsnew, $this->establecimiento->CurrentValue, NULL, $this->establecimiento->ReadOnly);

			// remitente
			$this->remitente->SetDbValueDef($rsnew, $this->remitente->CurrentValue, NULL, $this->remitente->ReadOnly);

			// tipo
			$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, NULL, $this->tipo->ReadOnly);

			// categoria
			$this->categoria->SetDbValueDef($rsnew, $this->categoria->CurrentValue, 0, $this->categoria->ReadOnly);

			// sub_categoria
			$this->sub_categoria->SetDbValueDef($rsnew, $this->sub_categoria->CurrentValue, 0, $this->sub_categoria->ReadOnly);

			// cantidad
			$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, $this->cantidad->ReadOnly);

			// peso
			$this->peso->SetDbValueDef($rsnew, $this->peso->CurrentValue, NULL, $this->peso->ReadOnly);

			// caracteristicas
			$this->caracteristicas->SetDbValueDef($rsnew, $this->caracteristicas->CurrentValue, NULL, $this->caracteristicas->ReadOnly);

			// provincia
			$this->provincia->SetDbValueDef($rsnew, $this->provincia->CurrentValue, NULL, $this->provincia->ReadOnly);

			// localidad
			$this->localidad->SetDbValueDef($rsnew, $this->localidad->CurrentValue, NULL, $this->localidad->ReadOnly);

			// lugar
			$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, NULL, $this->lugar->ReadOnly);

			// titulo
			$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", $this->titulo->ReadOnly);

			// edad_aprox
			$this->edad_aprox->SetDbValueDef($rsnew, $this->edad_aprox->CurrentValue, NULL, $this->edad_aprox->ReadOnly);

			// trazabilidad
			$this->trazabilidad->SetDbValueDef($rsnew, $this->trazabilidad->CurrentValue, NULL, $this->trazabilidad->ReadOnly);

			// mio_mio
			$this->mio_mio->SetDbValueDef($rsnew, $this->mio_mio->CurrentValue, NULL, $this->mio_mio->ReadOnly);

			// garrapata
			$this->garrapata->SetDbValueDef($rsnew, $this->garrapata->CurrentValue, NULL, $this->garrapata->ReadOnly);

			// plazo
			$this->plazo->SetDbValueDef($rsnew, $this->plazo->CurrentValue, NULL, $this->plazo->ReadOnly);

			// visitas
			$this->visitas->SetDbValueDef($rsnew, $this->visitas->CurrentValue, NULL, $this->visitas->ReadOnly);
			if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
				if (!ew_Empty($this->foto_1->Upload->Value)) {
					$rsnew['foto_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_1->UploadPath), $rsnew['foto_1']); // Get new file name
				}
			}
			if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
				if (!ew_Empty($this->foto_2->Upload->Value)) {
					$rsnew['foto_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_2->UploadPath), $rsnew['foto_2']); // Get new file name
				}
			}
			if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
				if (!ew_Empty($this->foto_3->Upload->Value)) {
					$rsnew['foto_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_3->UploadPath), $rsnew['foto_3']); // Get new file name
				}
			}
			if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
				if (!ew_Empty($this->foto_4->Upload->Value)) {
					$rsnew['foto_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_4->UploadPath), $rsnew['foto_4']); // Get new file name
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
						if (!ew_Empty($this->foto_1->Upload->Value)) {
							$this->foto_1->Upload->Resize($this->foto_1->ImageWidth, $this->foto_1->ImageHeight);
							if (!$this->foto_1->Upload->SaveToFile($this->foto_1->UploadPath, $rsnew['foto_1'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
						if (!ew_Empty($this->foto_2->Upload->Value)) {
							$this->foto_2->Upload->Resize($this->foto_2->ImageWidth, $this->foto_2->ImageHeight);
							if (!$this->foto_2->Upload->SaveToFile($this->foto_2->UploadPath, $rsnew['foto_2'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
						if (!ew_Empty($this->foto_3->Upload->Value)) {
							$this->foto_3->Upload->Resize($this->foto_3->ImageWidth, $this->foto_3->ImageHeight);
							if (!$this->foto_3->Upload->SaveToFile($this->foto_3->UploadPath, $rsnew['foto_3'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
						if (!ew_Empty($this->foto_4->Upload->Value)) {
							$this->foto_4->Upload->Resize($this->foto_4->ImageWidth, $this->foto_4->ImageHeight);
							if (!$this->foto_4->Upload->SaveToFile($this->foto_4->UploadPath, $rsnew['foto_4'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// foto_1
		ew_CleanUploadTempPath($this->foto_1, $this->foto_1->Upload->Index);

		// foto_2
		ew_CleanUploadTempPath($this->foto_2, $this->foto_2->Upload->Index);

		// foto_3
		ew_CleanUploadTempPath($this->foto_3, $this->foto_3->Upload->Index);

		// foto_4
		ew_CleanUploadTempPath($this->foto_4, $this->foto_4->Upload->Index);
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// venta_tipo
		$this->venta_tipo->SetDbValueDef($rsnew, $this->venta_tipo->CurrentValue, NULL, strval($this->venta_tipo->CurrentValue) == "");

		// activo
		$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, NULL, strval($this->activo->CurrentValue) == "");

		// vendido
		$this->vendido->SetDbValueDef($rsnew, $this->vendido->CurrentValue, NULL, strval($this->vendido->CurrentValue) == "");

		// codigo
		$this->codigo->SetDbValueDef($rsnew, $this->codigo->CurrentValue, NULL, FALSE);

		// remate_id
		$this->remate_id->SetDbValueDef($rsnew, $this->remate_id->CurrentValue, NULL, FALSE);

		// conjunto_lote_id
		$this->conjunto_lote_id->SetDbValueDef($rsnew, $this->conjunto_lote_id->CurrentValue, NULL, FALSE);

		// orden
		$this->orden->SetDbValueDef($rsnew, $this->orden->CurrentValue, NULL, FALSE);

		// video
		$this->video->SetDbValueDef($rsnew, $this->video->CurrentValue, NULL, FALSE);

		// foto_1
		if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
			$this->foto_1->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_1->Upload->FileName == "") {
				$rsnew['foto_1'] = NULL;
			} else {
				$rsnew['foto_1'] = $this->foto_1->Upload->FileName;
			}
			$this->foto_1->ImageWidth = 750; // Resize width
			$this->foto_1->ImageHeight = 0; // Resize height
		}

		// foto_2
		if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
			$this->foto_2->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_2->Upload->FileName == "") {
				$rsnew['foto_2'] = NULL;
			} else {
				$rsnew['foto_2'] = $this->foto_2->Upload->FileName;
			}
			$this->foto_2->ImageWidth = 750; // Resize width
			$this->foto_2->ImageHeight = 0; // Resize height
		}

		// foto_3
		if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
			$this->foto_3->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_3->Upload->FileName == "") {
				$rsnew['foto_3'] = NULL;
			} else {
				$rsnew['foto_3'] = $this->foto_3->Upload->FileName;
			}
			$this->foto_3->ImageWidth = 750; // Resize width
			$this->foto_3->ImageHeight = 0; // Resize height
		}

		// foto_4
		if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
			$this->foto_4->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_4->Upload->FileName == "") {
				$rsnew['foto_4'] = NULL;
			} else {
				$rsnew['foto_4'] = $this->foto_4->Upload->FileName;
			}
			$this->foto_4->ImageWidth = 750; // Resize width
			$this->foto_4->ImageHeight = 0; // Resize height
		}

		// precio
		$this->precio->SetDbValueDef($rsnew, $this->precio->CurrentValue, NULL, FALSE);

		// fecha_film
		$this->fecha_film->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0), NULL, FALSE);

		// establecimiento
		$this->establecimiento->SetDbValueDef($rsnew, $this->establecimiento->CurrentValue, NULL, FALSE);

		// remitente
		$this->remitente->SetDbValueDef($rsnew, $this->remitente->CurrentValue, NULL, FALSE);

		// tipo
		$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, NULL, FALSE);

		// categoria
		$this->categoria->SetDbValueDef($rsnew, $this->categoria->CurrentValue, 0, FALSE);

		// sub_categoria
		$this->sub_categoria->SetDbValueDef($rsnew, $this->sub_categoria->CurrentValue, 0, FALSE);

		// cantidad
		$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, FALSE);

		// peso
		$this->peso->SetDbValueDef($rsnew, $this->peso->CurrentValue, NULL, FALSE);

		// caracteristicas
		$this->caracteristicas->SetDbValueDef($rsnew, $this->caracteristicas->CurrentValue, NULL, FALSE);

		// provincia
		$this->provincia->SetDbValueDef($rsnew, $this->provincia->CurrentValue, NULL, FALSE);

		// localidad
		$this->localidad->SetDbValueDef($rsnew, $this->localidad->CurrentValue, NULL, FALSE);

		// lugar
		$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, NULL, FALSE);

		// titulo
		$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", FALSE);

		// edad_aprox
		$this->edad_aprox->SetDbValueDef($rsnew, $this->edad_aprox->CurrentValue, NULL, FALSE);

		// trazabilidad
		$this->trazabilidad->SetDbValueDef($rsnew, $this->trazabilidad->CurrentValue, NULL, FALSE);

		// mio_mio
		$this->mio_mio->SetDbValueDef($rsnew, $this->mio_mio->CurrentValue, NULL, FALSE);

		// garrapata
		$this->garrapata->SetDbValueDef($rsnew, $this->garrapata->CurrentValue, NULL, FALSE);

		// plazo
		$this->plazo->SetDbValueDef($rsnew, $this->plazo->CurrentValue, NULL, FALSE);

		// visitas
		$this->visitas->SetDbValueDef($rsnew, $this->visitas->CurrentValue, NULL, FALSE);
		if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
			if (!ew_Empty($this->foto_1->Upload->Value)) {
				$rsnew['foto_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_1->UploadPath), $rsnew['foto_1']); // Get new file name
			}
		}
		if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
			if (!ew_Empty($this->foto_2->Upload->Value)) {
				$rsnew['foto_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_2->UploadPath), $rsnew['foto_2']); // Get new file name
			}
		}
		if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
			if (!ew_Empty($this->foto_3->Upload->Value)) {
				$rsnew['foto_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_3->UploadPath), $rsnew['foto_3']); // Get new file name
			}
		}
		if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
			if (!ew_Empty($this->foto_4->Upload->Value)) {
				$rsnew['foto_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_4->UploadPath), $rsnew['foto_4']); // Get new file name
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->id->setDbValue($conn->Insert_ID());
				$rsnew['id'] = $this->id->DbValue;
				if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
					if (!ew_Empty($this->foto_1->Upload->Value)) {
						$this->foto_1->Upload->Resize($this->foto_1->ImageWidth, $this->foto_1->ImageHeight);
						if (!$this->foto_1->Upload->SaveToFile($this->foto_1->UploadPath, $rsnew['foto_1'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
					if (!ew_Empty($this->foto_2->Upload->Value)) {
						$this->foto_2->Upload->Resize($this->foto_2->ImageWidth, $this->foto_2->ImageHeight);
						if (!$this->foto_2->Upload->SaveToFile($this->foto_2->UploadPath, $rsnew['foto_2'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
					if (!ew_Empty($this->foto_3->Upload->Value)) {
						$this->foto_3->Upload->Resize($this->foto_3->ImageWidth, $this->foto_3->ImageHeight);
						if (!$this->foto_3->Upload->SaveToFile($this->foto_3->UploadPath, $rsnew['foto_3'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
					if (!ew_Empty($this->foto_4->Upload->Value)) {
						$this->foto_4->Upload->Resize($this->foto_4->ImageWidth, $this->foto_4->ImageHeight);
						if (!$this->foto_4->Upload->SaveToFile($this->foto_4->UploadPath, $rsnew['foto_4'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// foto_1
		ew_CleanUploadTempPath($this->foto_1, $this->foto_1->Upload->Index);

		// foto_2
		ew_CleanUploadTempPath($this->foto_2, $this->foto_2->Upload->Index);

		// foto_3
		ew_CleanUploadTempPath($this->foto_3, $this->foto_3->Upload->Index);

		// foto_4
		ew_CleanUploadTempPath($this->foto_4, $this->foto_4->Upload->Index);
		return $AddRow;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->venta_tipo->AdvancedSearch->Load();
		$this->activo->AdvancedSearch->Load();
		$this->vendido->AdvancedSearch->Load();
		$this->codigo->AdvancedSearch->Load();
		$this->remate_id->AdvancedSearch->Load();
		$this->conjunto_lote_id->AdvancedSearch->Load();
		$this->orden->AdvancedSearch->Load();
		$this->video->AdvancedSearch->Load();
		$this->foto_1->AdvancedSearch->Load();
		$this->foto_2->AdvancedSearch->Load();
		$this->foto_3->AdvancedSearch->Load();
		$this->foto_4->AdvancedSearch->Load();
		$this->precio->AdvancedSearch->Load();
		$this->fecha_film->AdvancedSearch->Load();
		$this->establecimiento->AdvancedSearch->Load();
		$this->remitente->AdvancedSearch->Load();
		$this->tipo->AdvancedSearch->Load();
		$this->categoria->AdvancedSearch->Load();
		$this->sub_categoria->AdvancedSearch->Load();
		$this->cantidad->AdvancedSearch->Load();
		$this->peso->AdvancedSearch->Load();
		$this->caracteristicas->AdvancedSearch->Load();
		$this->provincia->AdvancedSearch->Load();
		$this->localidad->AdvancedSearch->Load();
		$this->lugar->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->edad_aprox->AdvancedSearch->Load();
		$this->trazabilidad->AdvancedSearch->Load();
		$this->mio_mio->AdvancedSearch->Load();
		$this->garrapata->AdvancedSearch->Load();
		$this->observaciones->AdvancedSearch->Load();
		$this->plazo->AdvancedSearch->Load();
		$this->visitas->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_lotes\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_lotes',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.floteslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
		case "x_remate_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_conjunto_lote_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
			$sWhereWrk = "{filter}";
			$this->conjunto_lote_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`remate_id` IN ({filter_value})', "t1" => "19", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_tipo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_sub_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_provincia":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
			$sWhereWrk = "{filter}";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`provincia_id` IN ({filter_value})', "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
		case "x_remate_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_conjunto_lote_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
			$sWhereWrk = "{filter}";
			$this->conjunto_lote_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`remate_id` IN ({filter_value})', "t1" => "19", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_tipo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
			}
		} 
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld` FROM `localidades`";
			$sWhereWrk = "(`nombre` LIKE '{query_value}%') AND ({filter})";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f1" => "`provincia_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_list)) $lotes_list = new clotes_list();

// Page init
$lotes_list->Page_Init();

// Page main
$lotes_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = floteslist = new ew_Form("floteslist", "list");
floteslist.FormKeyCountName = '<?php echo $lotes_list->FormKeyCountName ?>';

// Validate form
floteslist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_precio");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->precio->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha_film");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->fecha_film->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_categoria");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->categoria->FldCaption(), $lotes->categoria->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_sub_categoria");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->sub_categoria->FldCaption(), $lotes->sub_categoria->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_cantidad");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->cantidad->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->titulo->FldCaption(), $lotes->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_visitas");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->visitas->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
floteslist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "venta_tipo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "activo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vendido", false)) return false;
	if (ew_ValueChanged(fobj, infix, "codigo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remate_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "conjunto_lote_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "orden", false)) return false;
	if (ew_ValueChanged(fobj, infix, "video", false)) return false;
	if (ew_ValueChanged(fobj, infix, "foto_1", false)) return false;
	if (ew_ValueChanged(fobj, infix, "foto_2", false)) return false;
	if (ew_ValueChanged(fobj, infix, "foto_3", false)) return false;
	if (ew_ValueChanged(fobj, infix, "foto_4", false)) return false;
	if (ew_ValueChanged(fobj, infix, "precio", false)) return false;
	if (ew_ValueChanged(fobj, infix, "fecha_film", false)) return false;
	if (ew_ValueChanged(fobj, infix, "establecimiento", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remitente", false)) return false;
	if (ew_ValueChanged(fobj, infix, "tipo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "categoria", false)) return false;
	if (ew_ValueChanged(fobj, infix, "sub_categoria", false)) return false;
	if (ew_ValueChanged(fobj, infix, "cantidad", false)) return false;
	if (ew_ValueChanged(fobj, infix, "peso", false)) return false;
	if (ew_ValueChanged(fobj, infix, "caracteristicas", false)) return false;
	if (ew_ValueChanged(fobj, infix, "provincia", false)) return false;
	if (ew_ValueChanged(fobj, infix, "localidad", false)) return false;
	if (ew_ValueChanged(fobj, infix, "lugar", false)) return false;
	if (ew_ValueChanged(fobj, infix, "titulo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "edad_aprox", false)) return false;
	if (ew_ValueChanged(fobj, infix, "trazabilidad", false)) return false;
	if (ew_ValueChanged(fobj, infix, "mio_mio", false)) return false;
	if (ew_ValueChanged(fobj, infix, "garrapata", false)) return false;
	if (ew_ValueChanged(fobj, infix, "plazo", false)) return false;
	if (ew_ValueChanged(fobj, infix, "visitas", false)) return false;
	return true;
}

// Form_CustomValidate event
floteslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
floteslist.ValidateRequired = true;
<?php } else { ?>
floteslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
floteslist.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
floteslist.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
floteslist.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
floteslist.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
floteslist.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":["x_remate_id"],"ChildFields":[],"FilterFields":["x_remate_id"],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
floteslist.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
floteslist.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
floteslist.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
floteslist.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
floteslist.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":["x_provincia"],"ChildFields":[],"FilterFields":["x_provincia_id"],"Options":[],"Template":"","LinkTable":"localidades"};
floteslist.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
floteslist.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
floteslist.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslist.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
var CurrentSearchForm = floteslistsrch = new ew_Form("floteslistsrch");

// Validate function for search
floteslistsrch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
floteslistsrch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
floteslistsrch.ValidateRequired = true; // Use JavaScript validation
<?php } else { ?>
floteslistsrch.ValidateRequired = false; // No JavaScript validation
<?php } ?>

// Dynamic selection lists
floteslistsrch.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslistsrch.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
floteslistsrch.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslistsrch.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
floteslistsrch.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
floteslistsrch.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":["x_remate_id"],"ChildFields":[],"FilterFields":["x_remate_id"],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
floteslistsrch.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
floteslistsrch.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
floteslistsrch.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslistsrch.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
floteslistsrch.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslistsrch.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
floteslistsrch.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
floteslistsrch.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($lotes->Export == "") { ?>
<div class="ewToolbar">
<?php if ($lotes->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($lotes_list->TotalRecs > 0 && $lotes_list->ExportOptions->Visible()) { ?>
<?php $lotes_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($lotes_list->SearchOptions->Visible()) { ?>
<?php $lotes_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($lotes_list->FilterOptions->Visible()) { ?>
<?php $lotes_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($lotes->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
if ($lotes->CurrentAction == "gridadd") {
	$lotes->CurrentFilter = "0=1";
	$lotes_list->StartRec = 1;
	$lotes_list->DisplayRecs = $lotes->GridAddRowCount;
	$lotes_list->TotalRecs = $lotes_list->DisplayRecs;
	$lotes_list->StopRec = $lotes_list->DisplayRecs;
} else {
	$bSelectLimit = $lotes_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($lotes_list->TotalRecs <= 0)
			$lotes_list->TotalRecs = $lotes->SelectRecordCount();
	} else {
		if (!$lotes_list->Recordset && ($lotes_list->Recordset = $lotes_list->LoadRecordset()))
			$lotes_list->TotalRecs = $lotes_list->Recordset->RecordCount();
	}
	$lotes_list->StartRec = 1;
	if ($lotes_list->DisplayRecs <= 0 || ($lotes->Export <> "" && $lotes->ExportAll)) // Display all records
		$lotes_list->DisplayRecs = $lotes_list->TotalRecs;
	if (!($lotes->Export <> "" && $lotes->ExportAll))
		$lotes_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$lotes_list->Recordset = $lotes_list->LoadRecordset($lotes_list->StartRec-1, $lotes_list->DisplayRecs);

	// Set no record found message
	if ($lotes->CurrentAction == "" && $lotes_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$lotes_list->setWarningMessage(ew_DeniedMsg());
		if ($lotes_list->SearchWhere == "0=101")
			$lotes_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$lotes_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$lotes_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($lotes->Export == "" && $lotes->CurrentAction == "") { ?>
<form name="floteslistsrch" id="floteslistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($lotes_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="floteslistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="lotes">
	<div class="ewBasicSearch">
<?php
if ($gsSearchError == "")
	$lotes_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$lotes->RowType = EW_ROWTYPE_SEARCH;

// Render row
$lotes->ResetAttrs();
$lotes_list->RenderRow();
?>
<div id="xsr_1" class="ewRow">
<?php if ($lotes->activo->Visible) { // activo ?>
	<div id="xsc_activo" class="ewCell form-group">
		<label for="x_activo" class="ewSearchCaption ewLabel"><?php echo $lotes->activo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_activo" id="z_activo" value="="></span>
		<span class="ewSearchField">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x_activo" name="x_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x_activo") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ewRow">
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<div id="xsc_vendido" class="ewCell form-group">
		<label for="x_vendido" class="ewSearchCaption ewLabel"><?php echo $lotes->vendido->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_vendido" id="z_vendido" value="="></span>
		<span class="ewSearchField">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x_vendido" name="x_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x_vendido") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_3" class="ewRow">
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<div id="xsc_codigo" class="ewCell form-group">
		<label for="x_codigo" class="ewSearchCaption ewLabel"><?php echo $lotes->codigo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_codigo" id="z_codigo" value="LIKE"></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_codigo" name="x_codigo" id="x_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_4" class="ewRow">
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<div id="xsc_remate_id" class="ewCell form-group">
		<label for="x_remate_id" class="ewSearchCaption ewLabel"><?php echo $lotes->remate_id->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_remate_id" id="z_remate_id" value="="></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_remate_id" name="x_remate_id" id="x_remate_id" size="10" placeholder="<?php echo ew_HtmlEncode($lotes->remate_id->getPlaceHolder()) ?>" value="<?php echo $lotes->remate_id->EditValue ?>"<?php echo $lotes->remate_id->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_5" class="ewRow">
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<div id="xsc_conjunto_lote_id" class="ewCell form-group">
		<label for="x_conjunto_lote_id" class="ewSearchCaption ewLabel"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_conjunto_lote_id" id="z_conjunto_lote_id" value="="></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_conjunto_lote_id" name="x_conjunto_lote_id" id="x_conjunto_lote_id" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->conjunto_lote_id->getPlaceHolder()) ?>" value="<?php echo $lotes->conjunto_lote_id->EditValue ?>"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_6" class="ewRow">
<?php if ($lotes->orden->Visible) { // orden ?>
	<div id="xsc_orden" class="ewCell form-group">
		<label for="x_orden" class="ewSearchCaption ewLabel"><?php echo $lotes->orden->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_orden" id="z_orden" value="LIKE"></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_orden" name="x_orden" id="x_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_7" class="ewRow">
<?php if ($lotes->video->Visible) { // video ?>
	<div id="xsc_video" class="ewCell form-group">
		<label for="x_video" class="ewSearchCaption ewLabel"><?php echo $lotes->video->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_video" id="z_video" value="LIKE"></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_video" name="x_video" id="x_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_8" class="ewRow">
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<div id="xsc_establecimiento" class="ewCell form-group">
		<label for="x_establecimiento" class="ewSearchCaption ewLabel"><?php echo $lotes->establecimiento->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_establecimiento" id="z_establecimiento" value="LIKE"></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x_establecimiento" id="x_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_9" class="ewRow">
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<div id="xsc_remitente" class="ewCell form-group">
		<label for="x_remitente" class="ewSearchCaption ewLabel"><?php echo $lotes->remitente->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_remitente" id="z_remitente" value="LIKE"></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_remitente" name="x_remitente" id="x_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_10" class="ewRow">
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<div id="xsc_tipo" class="ewCell form-group">
		<label for="x_tipo" class="ewSearchCaption ewLabel"><?php echo $lotes->tipo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_tipo" id="z_tipo" value="="></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_tipo" name="x_tipo" id="x_tipo" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->tipo->getPlaceHolder()) ?>" value="<?php echo $lotes->tipo->EditValue ?>"<?php echo $lotes->tipo->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_11" class="ewRow">
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<div id="xsc_categoria" class="ewCell form-group">
		<label for="x_categoria" class="ewSearchCaption ewLabel"><?php echo $lotes->categoria->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_categoria" id="z_categoria" value="="></span>
		<span class="ewSearchField">
<input type="text" data-table="lotes" data-field="x_categoria" name="x_categoria" id="x_categoria" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->categoria->getPlaceHolder()) ?>" value="<?php echo $lotes->categoria->EditValue ?>"<?php echo $lotes->categoria->EditAttributes() ?>>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_12" class="ewRow">
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<div id="xsc_trazabilidad" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $lotes->trazabilidad->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_trazabilidad" id="z_trazabilidad" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x_trazabilidad" id="x_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x_trazabilidad") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_13" class="ewRow">
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<div id="xsc_mio_mio" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $lotes->mio_mio->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_mio_mio" id="z_mio_mio" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x_mio_mio" id="x_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x_mio_mio") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_14" class="ewRow">
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<div id="xsc_garrapata" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $lotes->garrapata->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_garrapata" id="z_garrapata" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x_garrapata" id="x_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x_garrapata") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_15" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($lotes_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($lotes_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $lotes_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($lotes_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($lotes_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($lotes_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($lotes_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $lotes_list->ShowPageHeader(); ?>
<?php
$lotes_list->ShowMessage();
?>
<?php if ($lotes_list->TotalRecs > 0 || $lotes->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid lotes">
<?php if ($lotes->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($lotes->CurrentAction <> "gridadd" && $lotes->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($lotes_list->Pager)) $lotes_list->Pager = new cPrevNextPager($lotes_list->StartRec, $lotes_list->DisplayRecs, $lotes_list->TotalRecs) ?>
<?php if ($lotes_list->Pager->RecordCount > 0 && $lotes_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($lotes_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($lotes_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $lotes_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($lotes_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($lotes_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $lotes_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $lotes_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $lotes_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $lotes_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($lotes_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="floteslist" id="floteslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<div id="gmp_lotes" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($lotes_list->TotalRecs > 0 || $lotes->CurrentAction == "add" || $lotes->CurrentAction == "copy" || $lotes->CurrentAction == "gridedit") { ?>
<table id="tbl_loteslist" class="table ewTable">
<?php echo $lotes->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$lotes_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$lotes_list->RenderListOptions();

// Render list options (header, left)
$lotes_list->ListOptions->Render("header", "left");
?>
<?php if ($lotes->id->Visible) { // id ?>
	<?php if ($lotes->SortUrl($lotes->id) == "") { ?>
		<th data-name="id"><div id="elh_lotes_id" class="lotes_id"><div class="ewTableHeaderCaption"><?php echo $lotes->id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->id) ?>',1);"><div id="elh_lotes_id" class="lotes_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
	<?php if ($lotes->SortUrl($lotes->venta_tipo) == "") { ?>
		<th data-name="venta_tipo"><div id="elh_lotes_venta_tipo" class="lotes_venta_tipo"><div class="ewTableHeaderCaption"><?php echo $lotes->venta_tipo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="venta_tipo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->venta_tipo) ?>',1);"><div id="elh_lotes_venta_tipo" class="lotes_venta_tipo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->venta_tipo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->venta_tipo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->venta_tipo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->activo->Visible) { // activo ?>
	<?php if ($lotes->SortUrl($lotes->activo) == "") { ?>
		<th data-name="activo"><div id="elh_lotes_activo" class="lotes_activo"><div class="ewTableHeaderCaption"><?php echo $lotes->activo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="activo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->activo) ?>',1);"><div id="elh_lotes_activo" class="lotes_activo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->activo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->activo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->activo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<?php if ($lotes->SortUrl($lotes->vendido) == "") { ?>
		<th data-name="vendido"><div id="elh_lotes_vendido" class="lotes_vendido"><div class="ewTableHeaderCaption"><?php echo $lotes->vendido->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="vendido"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->vendido) ?>',1);"><div id="elh_lotes_vendido" class="lotes_vendido">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->vendido->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->vendido->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->vendido->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<?php if ($lotes->SortUrl($lotes->codigo) == "") { ?>
		<th data-name="codigo"><div id="elh_lotes_codigo" class="lotes_codigo"><div class="ewTableHeaderCaption"><?php echo $lotes->codigo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="codigo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->codigo) ?>',1);"><div id="elh_lotes_codigo" class="lotes_codigo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->codigo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->codigo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->codigo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<?php if ($lotes->SortUrl($lotes->remate_id) == "") { ?>
		<th data-name="remate_id"><div id="elh_lotes_remate_id" class="lotes_remate_id"><div class="ewTableHeaderCaption"><?php echo $lotes->remate_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remate_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->remate_id) ?>',1);"><div id="elh_lotes_remate_id" class="lotes_remate_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->remate_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->remate_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->remate_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<?php if ($lotes->SortUrl($lotes->conjunto_lote_id) == "") { ?>
		<th data-name="conjunto_lote_id"><div id="elh_lotes_conjunto_lote_id" class="lotes_conjunto_lote_id"><div class="ewTableHeaderCaption"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="conjunto_lote_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->conjunto_lote_id) ?>',1);"><div id="elh_lotes_conjunto_lote_id" class="lotes_conjunto_lote_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->conjunto_lote_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->conjunto_lote_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->orden->Visible) { // orden ?>
	<?php if ($lotes->SortUrl($lotes->orden) == "") { ?>
		<th data-name="orden"><div id="elh_lotes_orden" class="lotes_orden"><div class="ewTableHeaderCaption"><?php echo $lotes->orden->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="orden"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->orden) ?>',1);"><div id="elh_lotes_orden" class="lotes_orden">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->orden->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->orden->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->orden->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->video->Visible) { // video ?>
	<?php if ($lotes->SortUrl($lotes->video) == "") { ?>
		<th data-name="video"><div id="elh_lotes_video" class="lotes_video"><div class="ewTableHeaderCaption"><?php echo $lotes->video->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->video) ?>',1);"><div id="elh_lotes_video" class="lotes_video">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->video->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->video->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->video->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
	<?php if ($lotes->SortUrl($lotes->foto_1) == "") { ?>
		<th data-name="foto_1"><div id="elh_lotes_foto_1" class="lotes_foto_1"><div class="ewTableHeaderCaption"><?php echo $lotes->foto_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->foto_1) ?>',1);"><div id="elh_lotes_foto_1" class="lotes_foto_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->foto_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->foto_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->foto_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
	<?php if ($lotes->SortUrl($lotes->foto_2) == "") { ?>
		<th data-name="foto_2"><div id="elh_lotes_foto_2" class="lotes_foto_2"><div class="ewTableHeaderCaption"><?php echo $lotes->foto_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->foto_2) ?>',1);"><div id="elh_lotes_foto_2" class="lotes_foto_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->foto_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->foto_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->foto_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
	<?php if ($lotes->SortUrl($lotes->foto_3) == "") { ?>
		<th data-name="foto_3"><div id="elh_lotes_foto_3" class="lotes_foto_3"><div class="ewTableHeaderCaption"><?php echo $lotes->foto_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->foto_3) ?>',1);"><div id="elh_lotes_foto_3" class="lotes_foto_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->foto_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->foto_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->foto_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
	<?php if ($lotes->SortUrl($lotes->foto_4) == "") { ?>
		<th data-name="foto_4"><div id="elh_lotes_foto_4" class="lotes_foto_4"><div class="ewTableHeaderCaption"><?php echo $lotes->foto_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="foto_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->foto_4) ?>',1);"><div id="elh_lotes_foto_4" class="lotes_foto_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->foto_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->foto_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->foto_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->precio->Visible) { // precio ?>
	<?php if ($lotes->SortUrl($lotes->precio) == "") { ?>
		<th data-name="precio"><div id="elh_lotes_precio" class="lotes_precio"><div class="ewTableHeaderCaption"><?php echo $lotes->precio->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="precio"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->precio) ?>',1);"><div id="elh_lotes_precio" class="lotes_precio">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->precio->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->precio->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->precio->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
	<?php if ($lotes->SortUrl($lotes->fecha_film) == "") { ?>
		<th data-name="fecha_film"><div id="elh_lotes_fecha_film" class="lotes_fecha_film"><div class="ewTableHeaderCaption"><?php echo $lotes->fecha_film->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="fecha_film"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->fecha_film) ?>',1);"><div id="elh_lotes_fecha_film" class="lotes_fecha_film">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->fecha_film->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->fecha_film->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->fecha_film->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<?php if ($lotes->SortUrl($lotes->establecimiento) == "") { ?>
		<th data-name="establecimiento"><div id="elh_lotes_establecimiento" class="lotes_establecimiento"><div class="ewTableHeaderCaption"><?php echo $lotes->establecimiento->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="establecimiento"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->establecimiento) ?>',1);"><div id="elh_lotes_establecimiento" class="lotes_establecimiento">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->establecimiento->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->establecimiento->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->establecimiento->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<?php if ($lotes->SortUrl($lotes->remitente) == "") { ?>
		<th data-name="remitente"><div id="elh_lotes_remitente" class="lotes_remitente"><div class="ewTableHeaderCaption"><?php echo $lotes->remitente->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remitente"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->remitente) ?>',1);"><div id="elh_lotes_remitente" class="lotes_remitente">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->remitente->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->remitente->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->remitente->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<?php if ($lotes->SortUrl($lotes->tipo) == "") { ?>
		<th data-name="tipo"><div id="elh_lotes_tipo" class="lotes_tipo"><div class="ewTableHeaderCaption"><?php echo $lotes->tipo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="tipo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->tipo) ?>',1);"><div id="elh_lotes_tipo" class="lotes_tipo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->tipo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->tipo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->tipo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<?php if ($lotes->SortUrl($lotes->categoria) == "") { ?>
		<th data-name="categoria"><div id="elh_lotes_categoria" class="lotes_categoria"><div class="ewTableHeaderCaption"><?php echo $lotes->categoria->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="categoria"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->categoria) ?>',1);"><div id="elh_lotes_categoria" class="lotes_categoria">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->categoria->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->categoria->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->categoria->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
	<?php if ($lotes->SortUrl($lotes->sub_categoria) == "") { ?>
		<th data-name="sub_categoria"><div id="elh_lotes_sub_categoria" class="lotes_sub_categoria"><div class="ewTableHeaderCaption"><?php echo $lotes->sub_categoria->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="sub_categoria"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->sub_categoria) ?>',1);"><div id="elh_lotes_sub_categoria" class="lotes_sub_categoria">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->sub_categoria->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->sub_categoria->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->sub_categoria->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
	<?php if ($lotes->SortUrl($lotes->cantidad) == "") { ?>
		<th data-name="cantidad"><div id="elh_lotes_cantidad" class="lotes_cantidad"><div class="ewTableHeaderCaption"><?php echo $lotes->cantidad->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="cantidad"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->cantidad) ?>',1);"><div id="elh_lotes_cantidad" class="lotes_cantidad">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->cantidad->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->cantidad->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->cantidad->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->peso->Visible) { // peso ?>
	<?php if ($lotes->SortUrl($lotes->peso) == "") { ?>
		<th data-name="peso"><div id="elh_lotes_peso" class="lotes_peso"><div class="ewTableHeaderCaption"><?php echo $lotes->peso->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="peso"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->peso) ?>',1);"><div id="elh_lotes_peso" class="lotes_peso">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->peso->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->peso->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->peso->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
	<?php if ($lotes->SortUrl($lotes->caracteristicas) == "") { ?>
		<th data-name="caracteristicas"><div id="elh_lotes_caracteristicas" class="lotes_caracteristicas"><div class="ewTableHeaderCaption"><?php echo $lotes->caracteristicas->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="caracteristicas"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->caracteristicas) ?>',1);"><div id="elh_lotes_caracteristicas" class="lotes_caracteristicas">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->caracteristicas->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->caracteristicas->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->caracteristicas->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->provincia->Visible) { // provincia ?>
	<?php if ($lotes->SortUrl($lotes->provincia) == "") { ?>
		<th data-name="provincia"><div id="elh_lotes_provincia" class="lotes_provincia"><div class="ewTableHeaderCaption"><?php echo $lotes->provincia->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="provincia"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->provincia) ?>',1);"><div id="elh_lotes_provincia" class="lotes_provincia">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->provincia->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->provincia->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->provincia->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->localidad->Visible) { // localidad ?>
	<?php if ($lotes->SortUrl($lotes->localidad) == "") { ?>
		<th data-name="localidad"><div id="elh_lotes_localidad" class="lotes_localidad"><div class="ewTableHeaderCaption"><?php echo $lotes->localidad->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="localidad"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->localidad) ?>',1);"><div id="elh_lotes_localidad" class="lotes_localidad">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->localidad->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->localidad->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->localidad->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->lugar->Visible) { // lugar ?>
	<?php if ($lotes->SortUrl($lotes->lugar) == "") { ?>
		<th data-name="lugar"><div id="elh_lotes_lugar" class="lotes_lugar"><div class="ewTableHeaderCaption"><?php echo $lotes->lugar->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="lugar"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->lugar) ?>',1);"><div id="elh_lotes_lugar" class="lotes_lugar">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->lugar->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->lugar->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->lugar->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->titulo->Visible) { // titulo ?>
	<?php if ($lotes->SortUrl($lotes->titulo) == "") { ?>
		<th data-name="titulo"><div id="elh_lotes_titulo" class="lotes_titulo"><div class="ewTableHeaderCaption"><?php echo $lotes->titulo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="titulo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->titulo) ?>',1);"><div id="elh_lotes_titulo" class="lotes_titulo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->titulo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->titulo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->titulo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
	<?php if ($lotes->SortUrl($lotes->edad_aprox) == "") { ?>
		<th data-name="edad_aprox"><div id="elh_lotes_edad_aprox" class="lotes_edad_aprox"><div class="ewTableHeaderCaption"><?php echo $lotes->edad_aprox->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="edad_aprox"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->edad_aprox) ?>',1);"><div id="elh_lotes_edad_aprox" class="lotes_edad_aprox">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->edad_aprox->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->edad_aprox->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->edad_aprox->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<?php if ($lotes->SortUrl($lotes->trazabilidad) == "") { ?>
		<th data-name="trazabilidad"><div id="elh_lotes_trazabilidad" class="lotes_trazabilidad"><div class="ewTableHeaderCaption"><?php echo $lotes->trazabilidad->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="trazabilidad"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->trazabilidad) ?>',1);"><div id="elh_lotes_trazabilidad" class="lotes_trazabilidad">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->trazabilidad->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->trazabilidad->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->trazabilidad->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<?php if ($lotes->SortUrl($lotes->mio_mio) == "") { ?>
		<th data-name="mio_mio"><div id="elh_lotes_mio_mio" class="lotes_mio_mio"><div class="ewTableHeaderCaption"><?php echo $lotes->mio_mio->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="mio_mio"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->mio_mio) ?>',1);"><div id="elh_lotes_mio_mio" class="lotes_mio_mio">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->mio_mio->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->mio_mio->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->mio_mio->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<?php if ($lotes->SortUrl($lotes->garrapata) == "") { ?>
		<th data-name="garrapata"><div id="elh_lotes_garrapata" class="lotes_garrapata"><div class="ewTableHeaderCaption"><?php echo $lotes->garrapata->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="garrapata"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->garrapata) ?>',1);"><div id="elh_lotes_garrapata" class="lotes_garrapata">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->garrapata->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($lotes->garrapata->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->garrapata->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->plazo->Visible) { // plazo ?>
	<?php if ($lotes->SortUrl($lotes->plazo) == "") { ?>
		<th data-name="plazo"><div id="elh_lotes_plazo" class="lotes_plazo"><div class="ewTableHeaderCaption"><?php echo $lotes->plazo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="plazo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->plazo) ?>',1);"><div id="elh_lotes_plazo" class="lotes_plazo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->plazo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->plazo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->plazo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($lotes->visitas->Visible) { // visitas ?>
	<?php if ($lotes->SortUrl($lotes->visitas) == "") { ?>
		<th data-name="visitas"><div id="elh_lotes_visitas" class="lotes_visitas"><div class="ewTableHeaderCaption"><?php echo $lotes->visitas->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="visitas"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $lotes->SortUrl($lotes->visitas) ?>',1);"><div id="elh_lotes_visitas" class="lotes_visitas">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $lotes->visitas->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($lotes->visitas->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($lotes->visitas->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$lotes_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
	if ($lotes->CurrentAction == "add" || $lotes->CurrentAction == "copy") {
		$lotes_list->RowIndex = 0;
		$lotes_list->KeyCount = $lotes_list->RowIndex;
		if ($lotes->CurrentAction == "copy" && !$lotes_list->LoadRow())
				$lotes->CurrentAction = "add";
		if ($lotes->CurrentAction == "add")
			$lotes_list->LoadDefaultValues();
		if ($lotes->EventCancelled) // Insert failed
			$lotes_list->RestoreFormValues(); // Restore form values

		// Set row properties
		$lotes->ResetAttrs();
		$lotes->RowAttrs = array_merge($lotes->RowAttrs, array('data-rowindex'=>0, 'id'=>'r0_lotes', 'data-rowtype'=>EW_ROWTYPE_ADD));
		$lotes->RowType = EW_ROWTYPE_ADD;

		// Render row
		$lotes_list->RenderRow();

		// Render list options
		$lotes_list->RenderListOptions();
		$lotes_list->StartRowCnt = 0;
?>
	<tr<?php echo $lotes->RowAttributes() ?>>
<?php

// Render list options (body, left)
$lotes_list->ListOptions->Render("body", "left", $lotes_list->RowCnt);
?>
	<?php if ($lotes->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="lotes" data-field="x_id" name="o<?php echo $lotes_list->RowIndex ?>_id" id="o<?php echo $lotes_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($lotes->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
		<td data-name="venta_tipo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_venta_tipo" class="form-group lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_venta_tipo" name="x<?php echo $lotes_list->RowIndex ?>_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_venta_tipo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_venta_tipo" name="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" id="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" value="<?php echo ew_HtmlEncode($lotes->venta_tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->activo->Visible) { // activo ?>
		<td data-name="activo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_activo" class="form-group lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_activo" name="x<?php echo $lotes_list->RowIndex ?>_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_activo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_activo" name="o<?php echo $lotes_list->RowIndex ?>_activo" id="o<?php echo $lotes_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($lotes->activo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->vendido->Visible) { // vendido ?>
		<td data-name="vendido">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_vendido" class="form-group lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_vendido" name="x<?php echo $lotes_list->RowIndex ?>_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_vendido") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_vendido" name="o<?php echo $lotes_list->RowIndex ?>_vendido" id="o<?php echo $lotes_list->RowIndex ?>_vendido" value="<?php echo ew_HtmlEncode($lotes->vendido->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->codigo->Visible) { // codigo ?>
		<td data-name="codigo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_codigo" class="form-group lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x<?php echo $lotes_list->RowIndex ?>_codigo" id="x<?php echo $lotes_list->RowIndex ?>_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_codigo" name="o<?php echo $lotes_list->RowIndex ?>_codigo" id="o<?php echo $lotes_list->RowIndex ?>_codigo" value="<?php echo ew_HtmlEncode($lotes->codigo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->remate_id->Visible) { // remate_id ?>
		<td data-name="remate_id">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remate_id" class="form-group lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_remate_id" name="x<?php echo $lotes_list->RowIndex ?>_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_remate_id") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" id="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_remate_id" name="o<?php echo $lotes_list->RowIndex ?>_remate_id" id="o<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo ew_HtmlEncode($lotes->remate_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
		<td data-name="conjunto_lote_id">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_conjunto_lote_id" class="form-group lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" name="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "conjuntos_lotes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->conjunto_lote_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id',url:'conjuntos_lotesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_conjunto_lote_id" name="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo ew_HtmlEncode($lotes->conjunto_lote_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->orden->Visible) { // orden ?>
		<td data-name="orden">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_orden" class="form-group lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x<?php echo $lotes_list->RowIndex ?>_orden" id="x<?php echo $lotes_list->RowIndex ?>_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_orden" name="o<?php echo $lotes_list->RowIndex ?>_orden" id="o<?php echo $lotes_list->RowIndex ?>_orden" value="<?php echo ew_HtmlEncode($lotes->orden->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->video->Visible) { // video ?>
		<td data-name="video">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_video" class="form-group lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x<?php echo $lotes_list->RowIndex ?>_video" id="x<?php echo $lotes_list->RowIndex ?>_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_video" name="o<?php echo $lotes_list->RowIndex ?>_video" id="o<?php echo $lotes_list->RowIndex ?>_video" value="<?php echo ew_HtmlEncode($lotes->video->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
		<td data-name="foto_1">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_1" class="form-group lotes_foto_1">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x<?php echo $lotes_list->RowIndex ?>_foto_1" id="x<?php echo $lotes_list->RowIndex ?>_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_1" name="o<?php echo $lotes_list->RowIndex ?>_foto_1" id="o<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo ew_HtmlEncode($lotes->foto_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
		<td data-name="foto_2">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_2" class="form-group lotes_foto_2">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x<?php echo $lotes_list->RowIndex ?>_foto_2" id="x<?php echo $lotes_list->RowIndex ?>_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_2" name="o<?php echo $lotes_list->RowIndex ?>_foto_2" id="o<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo ew_HtmlEncode($lotes->foto_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
		<td data-name="foto_3">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_3" class="form-group lotes_foto_3">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x<?php echo $lotes_list->RowIndex ?>_foto_3" id="x<?php echo $lotes_list->RowIndex ?>_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_3" name="o<?php echo $lotes_list->RowIndex ?>_foto_3" id="o<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo ew_HtmlEncode($lotes->foto_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
		<td data-name="foto_4">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_4" class="form-group lotes_foto_4">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x<?php echo $lotes_list->RowIndex ?>_foto_4" id="x<?php echo $lotes_list->RowIndex ?>_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_4" name="o<?php echo $lotes_list->RowIndex ?>_foto_4" id="o<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo ew_HtmlEncode($lotes->foto_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->precio->Visible) { // precio ?>
		<td data-name="precio">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_precio" class="form-group lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x<?php echo $lotes_list->RowIndex ?>_precio" id="x<?php echo $lotes_list->RowIndex ?>_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_precio" name="o<?php echo $lotes_list->RowIndex ?>_precio" id="o<?php echo $lotes_list->RowIndex ?>_precio" value="<?php echo ew_HtmlEncode($lotes->precio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
		<td data-name="fecha_film">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_fecha_film" class="form-group lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x<?php echo $lotes_list->RowIndex ?>_fecha_film" id="x<?php echo $lotes_list->RowIndex ?>_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("floteslist", "x<?php echo $lotes_list->RowIndex ?>_fecha_film", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="lotes" data-field="x_fecha_film" name="o<?php echo $lotes_list->RowIndex ?>_fecha_film" id="o<?php echo $lotes_list->RowIndex ?>_fecha_film" value="<?php echo ew_HtmlEncode($lotes->fecha_film->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
		<td data-name="establecimiento">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_establecimiento" class="form-group lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x<?php echo $lotes_list->RowIndex ?>_establecimiento" id="x<?php echo $lotes_list->RowIndex ?>_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_establecimiento" name="o<?php echo $lotes_list->RowIndex ?>_establecimiento" id="o<?php echo $lotes_list->RowIndex ?>_establecimiento" value="<?php echo ew_HtmlEncode($lotes->establecimiento->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->remitente->Visible) { // remitente ?>
		<td data-name="remitente">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remitente" class="form-group lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x<?php echo $lotes_list->RowIndex ?>_remitente" id="x<?php echo $lotes_list->RowIndex ?>_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_remitente" name="o<?php echo $lotes_list->RowIndex ?>_remitente" id="o<?php echo $lotes_list->RowIndex ?>_remitente" value="<?php echo ew_HtmlEncode($lotes->remitente->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->tipo->Visible) { // tipo ?>
		<td data-name="tipo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_tipo" class="form-group lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_tipo" name="x<?php echo $lotes_list->RowIndex ?>_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_tipo") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "tipos")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->tipo->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_tipo',url:'tiposaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_tipo"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->tipo->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_tipo" id="s_x<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_tipo" name="o<?php echo $lotes_list->RowIndex ?>_tipo" id="o<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($lotes->tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->categoria->Visible) { // categoria ?>
		<td data-name="categoria">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_categoria" class="form-group lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_categoria" name="x<?php echo $lotes_list->RowIndex ?>_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "categorias_ganado")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_categoria',url:'categorias_ganadoaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_categoria" name="o<?php echo $lotes_list->RowIndex ?>_categoria" id="o<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo ew_HtmlEncode($lotes->categoria->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
		<td data-name="sub_categoria">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_sub_categoria" class="form-group lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_sub_categoria" name="x<?php echo $lotes_list->RowIndex ?>_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_sub_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "sub_categorias")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->sub_categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_sub_categoria',url:'sub_categoriasaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_sub_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->sub_categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_sub_categoria" name="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo ew_HtmlEncode($lotes->sub_categoria->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_cantidad" class="form-group lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x<?php echo $lotes_list->RowIndex ?>_cantidad" id="x<?php echo $lotes_list->RowIndex ?>_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_cantidad" name="o<?php echo $lotes_list->RowIndex ?>_cantidad" id="o<?php echo $lotes_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($lotes->cantidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->peso->Visible) { // peso ?>
		<td data-name="peso">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_peso" class="form-group lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x<?php echo $lotes_list->RowIndex ?>_peso" id="x<?php echo $lotes_list->RowIndex ?>_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_peso" name="o<?php echo $lotes_list->RowIndex ?>_peso" id="o<?php echo $lotes_list->RowIndex ?>_peso" value="<?php echo ew_HtmlEncode($lotes->peso->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
		<td data-name="caracteristicas">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_caracteristicas" class="form-group lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_caracteristicas" name="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" value="<?php echo ew_HtmlEncode($lotes->caracteristicas->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->provincia->Visible) { // provincia ?>
		<td data-name="provincia">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_provincia" class="form-group lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_provincia" name="x<?php echo $lotes_list->RowIndex ?>_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_provincia") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_provincia" id="s_x<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_provincia" name="o<?php echo $lotes_list->RowIndex ?>_provincia" id="o<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo ew_HtmlEncode($lotes->provincia->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->localidad->Visible) { // localidad ?>
		<td data-name="localidad">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_localidad" class="form-group lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $lotes_list->RowIndex ?>_localidad" style="white-space: nowrap; z-index: <?php echo (9000 - $lotes_list->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" id="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_localidad" id="x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x<?php echo $lotes_list->RowIndex ?>_localidad" id="q_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
floteslist.CreateAutoSuggest({"id":"x<?php echo $lotes_list->RowIndex ?>_localidad","forceSelect":false});
</script>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->localidad->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_localidad',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_localidad"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->localidad->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_localidad" id="s_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" name="o<?php echo $lotes_list->RowIndex ?>_localidad" id="o<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->lugar->Visible) { // lugar ?>
		<td data-name="lugar">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_lugar" class="form-group lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x<?php echo $lotes_list->RowIndex ?>_lugar" id="x<?php echo $lotes_list->RowIndex ?>_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_lugar" name="o<?php echo $lotes_list->RowIndex ?>_lugar" id="o<?php echo $lotes_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($lotes->lugar->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->titulo->Visible) { // titulo ?>
		<td data-name="titulo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_titulo" class="form-group lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x<?php echo $lotes_list->RowIndex ?>_titulo" id="x<?php echo $lotes_list->RowIndex ?>_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_titulo" name="o<?php echo $lotes_list->RowIndex ?>_titulo" id="o<?php echo $lotes_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($lotes->titulo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
		<td data-name="edad_aprox">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_edad_aprox" class="form-group lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_edad_aprox" name="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" value="<?php echo ew_HtmlEncode($lotes->edad_aprox->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
		<td data-name="trazabilidad">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_trazabilidad" class="form-group lotes_trazabilidad">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_trazabilidad") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_trazabilidad" name="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="<?php echo ew_HtmlEncode($lotes->trazabilidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
		<td data-name="mio_mio">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_mio_mio" class="form-group lotes_mio_mio">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_mio_mio" id="x<?php echo $lotes_list->RowIndex ?>_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_mio_mio") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_mio_mio" name="o<?php echo $lotes_list->RowIndex ?>_mio_mio" id="o<?php echo $lotes_list->RowIndex ?>_mio_mio" value="<?php echo ew_HtmlEncode($lotes->mio_mio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->garrapata->Visible) { // garrapata ?>
		<td data-name="garrapata">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_garrapata" class="form-group lotes_garrapata">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_garrapata" id="x<?php echo $lotes_list->RowIndex ?>_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_garrapata") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_garrapata" name="o<?php echo $lotes_list->RowIndex ?>_garrapata" id="o<?php echo $lotes_list->RowIndex ?>_garrapata" value="<?php echo ew_HtmlEncode($lotes->garrapata->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->plazo->Visible) { // plazo ?>
		<td data-name="plazo">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_plazo" class="form-group lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x<?php echo $lotes_list->RowIndex ?>_plazo" id="x<?php echo $lotes_list->RowIndex ?>_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_plazo" name="o<?php echo $lotes_list->RowIndex ?>_plazo" id="o<?php echo $lotes_list->RowIndex ?>_plazo" value="<?php echo ew_HtmlEncode($lotes->plazo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->visitas->Visible) { // visitas ?>
		<td data-name="visitas">
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_visitas" class="form-group lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x<?php echo $lotes_list->RowIndex ?>_visitas" id="x<?php echo $lotes_list->RowIndex ?>_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_visitas" name="o<?php echo $lotes_list->RowIndex ?>_visitas" id="o<?php echo $lotes_list->RowIndex ?>_visitas" value="<?php echo ew_HtmlEncode($lotes->visitas->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$lotes_list->ListOptions->Render("body", "right", $lotes_list->RowCnt);
?>
<script type="text/javascript">
floteslist.UpdateOpts(<?php echo $lotes_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
<?php
if ($lotes->ExportAll && $lotes->Export <> "") {
	$lotes_list->StopRec = $lotes_list->TotalRecs;
} else {

	// Set the last record to display
	if ($lotes_list->TotalRecs > $lotes_list->StartRec + $lotes_list->DisplayRecs - 1)
		$lotes_list->StopRec = $lotes_list->StartRec + $lotes_list->DisplayRecs - 1;
	else
		$lotes_list->StopRec = $lotes_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($lotes_list->FormKeyCountName) && ($lotes->CurrentAction == "gridadd" || $lotes->CurrentAction == "gridedit" || $lotes->CurrentAction == "F")) {
		$lotes_list->KeyCount = $objForm->GetValue($lotes_list->FormKeyCountName);
		$lotes_list->StopRec = $lotes_list->StartRec + $lotes_list->KeyCount - 1;
	}
}
$lotes_list->RecCnt = $lotes_list->StartRec - 1;
if ($lotes_list->Recordset && !$lotes_list->Recordset->EOF) {
	$lotes_list->Recordset->MoveFirst();
	$bSelectLimit = $lotes_list->UseSelectLimit;
	if (!$bSelectLimit && $lotes_list->StartRec > 1)
		$lotes_list->Recordset->Move($lotes_list->StartRec - 1);
} elseif (!$lotes->AllowAddDeleteRow && $lotes_list->StopRec == 0) {
	$lotes_list->StopRec = $lotes->GridAddRowCount;
}

// Initialize aggregate
$lotes->RowType = EW_ROWTYPE_AGGREGATEINIT;
$lotes->ResetAttrs();
$lotes_list->RenderRow();
$lotes_list->EditRowCnt = 0;
if ($lotes->CurrentAction == "edit")
	$lotes_list->RowIndex = 1;
if ($lotes->CurrentAction == "gridadd")
	$lotes_list->RowIndex = 0;
if ($lotes->CurrentAction == "gridedit")
	$lotes_list->RowIndex = 0;
while ($lotes_list->RecCnt < $lotes_list->StopRec) {
	$lotes_list->RecCnt++;
	if (intval($lotes_list->RecCnt) >= intval($lotes_list->StartRec)) {
		$lotes_list->RowCnt++;
		if ($lotes->CurrentAction == "gridadd" || $lotes->CurrentAction == "gridedit" || $lotes->CurrentAction == "F") {
			$lotes_list->RowIndex++;
			$objForm->Index = $lotes_list->RowIndex;
			if ($objForm->HasValue($lotes_list->FormActionName))
				$lotes_list->RowAction = strval($objForm->GetValue($lotes_list->FormActionName));
			elseif ($lotes->CurrentAction == "gridadd")
				$lotes_list->RowAction = "insert";
			else
				$lotes_list->RowAction = "";
		}

		// Set up key count
		$lotes_list->KeyCount = $lotes_list->RowIndex;

		// Init row class and style
		$lotes->ResetAttrs();
		$lotes->CssClass = "";
		if ($lotes->CurrentAction == "gridadd") {
			$lotes_list->LoadDefaultValues(); // Load default values
		} else {
			$lotes_list->LoadRowValues($lotes_list->Recordset); // Load row values
		}
		$lotes->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($lotes->CurrentAction == "gridadd") // Grid add
			$lotes->RowType = EW_ROWTYPE_ADD; // Render add
		if ($lotes->CurrentAction == "gridadd" && $lotes->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$lotes_list->RestoreCurrentRowFormValues($lotes_list->RowIndex); // Restore form values
		if ($lotes->CurrentAction == "edit") {
			if ($lotes_list->CheckInlineEditKey() && $lotes_list->EditRowCnt == 0) { // Inline edit
				$lotes->RowType = EW_ROWTYPE_EDIT; // Render edit
			}
		}
		if ($lotes->CurrentAction == "gridedit") { // Grid edit
			if ($lotes->EventCancelled) {
				$lotes_list->RestoreCurrentRowFormValues($lotes_list->RowIndex); // Restore form values
			}
			if ($lotes_list->RowAction == "insert")
				$lotes->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$lotes->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($lotes->CurrentAction == "edit" && $lotes->RowType == EW_ROWTYPE_EDIT && $lotes->EventCancelled) { // Update failed
			$objForm->Index = 1;
			$lotes_list->RestoreFormValues(); // Restore form values
		}
		if ($lotes->CurrentAction == "gridedit" && ($lotes->RowType == EW_ROWTYPE_EDIT || $lotes->RowType == EW_ROWTYPE_ADD) && $lotes->EventCancelled) // Update failed
			$lotes_list->RestoreCurrentRowFormValues($lotes_list->RowIndex); // Restore form values
		if ($lotes->RowType == EW_ROWTYPE_EDIT) // Edit row
			$lotes_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$lotes->RowAttrs = array_merge($lotes->RowAttrs, array('data-rowindex'=>$lotes_list->RowCnt, 'id'=>'r' . $lotes_list->RowCnt . '_lotes', 'data-rowtype'=>$lotes->RowType));

		// Render row
		$lotes_list->RenderRow();

		// Render list options
		$lotes_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($lotes_list->RowAction <> "delete" && $lotes_list->RowAction <> "insertdelete" && !($lotes_list->RowAction == "insert" && $lotes->CurrentAction == "F" && $lotes_list->EmptyRow())) {
?>
	<tr<?php echo $lotes->RowAttributes() ?>>
<?php

// Render list options (body, left)
$lotes_list->ListOptions->Render("body", "left", $lotes_list->RowCnt);
?>
	<?php if ($lotes->id->Visible) { // id ?>
		<td data-name="id"<?php echo $lotes->id->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="lotes" data-field="x_id" name="o<?php echo $lotes_list->RowIndex ?>_id" id="o<?php echo $lotes_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($lotes->id->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_id" class="form-group lotes_id">
<span<?php echo $lotes->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $lotes->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="lotes" data-field="x_id" name="x<?php echo $lotes_list->RowIndex ?>_id" id="x<?php echo $lotes_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($lotes->id->CurrentValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_id" class="lotes_id">
<span<?php echo $lotes->id->ViewAttributes() ?>>
<?php echo $lotes->id->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $lotes_list->PageObjName . "_row_" . $lotes_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
		<td data-name="venta_tipo"<?php echo $lotes->venta_tipo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_venta_tipo" class="form-group lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_venta_tipo" name="x<?php echo $lotes_list->RowIndex ?>_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_venta_tipo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_venta_tipo" name="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" id="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" value="<?php echo ew_HtmlEncode($lotes->venta_tipo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_venta_tipo" class="form-group lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_venta_tipo" name="x<?php echo $lotes_list->RowIndex ?>_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_venta_tipo") ?>
</select>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_venta_tipo" class="lotes_venta_tipo">
<span<?php echo $lotes->venta_tipo->ViewAttributes() ?>>
<?php echo $lotes->venta_tipo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->activo->Visible) { // activo ?>
		<td data-name="activo"<?php echo $lotes->activo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_activo" class="form-group lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_activo" name="x<?php echo $lotes_list->RowIndex ?>_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_activo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_activo" name="o<?php echo $lotes_list->RowIndex ?>_activo" id="o<?php echo $lotes_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($lotes->activo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_activo" class="form-group lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_activo" name="x<?php echo $lotes_list->RowIndex ?>_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_activo") ?>
</select>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_activo" class="lotes_activo">
<span<?php echo $lotes->activo->ViewAttributes() ?>>
<?php echo $lotes->activo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->vendido->Visible) { // vendido ?>
		<td data-name="vendido"<?php echo $lotes->vendido->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_vendido" class="form-group lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_vendido" name="x<?php echo $lotes_list->RowIndex ?>_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_vendido") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_vendido" name="o<?php echo $lotes_list->RowIndex ?>_vendido" id="o<?php echo $lotes_list->RowIndex ?>_vendido" value="<?php echo ew_HtmlEncode($lotes->vendido->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_vendido" class="form-group lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_vendido" name="x<?php echo $lotes_list->RowIndex ?>_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_vendido") ?>
</select>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_vendido" class="lotes_vendido">
<span<?php echo $lotes->vendido->ViewAttributes() ?>>
<?php echo $lotes->vendido->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->codigo->Visible) { // codigo ?>
		<td data-name="codigo"<?php echo $lotes->codigo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_codigo" class="form-group lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x<?php echo $lotes_list->RowIndex ?>_codigo" id="x<?php echo $lotes_list->RowIndex ?>_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_codigo" name="o<?php echo $lotes_list->RowIndex ?>_codigo" id="o<?php echo $lotes_list->RowIndex ?>_codigo" value="<?php echo ew_HtmlEncode($lotes->codigo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_codigo" class="form-group lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x<?php echo $lotes_list->RowIndex ?>_codigo" id="x<?php echo $lotes_list->RowIndex ?>_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_codigo" class="lotes_codigo">
<span<?php echo $lotes->codigo->ViewAttributes() ?>>
<?php echo $lotes->codigo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->remate_id->Visible) { // remate_id ?>
		<td data-name="remate_id"<?php echo $lotes->remate_id->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remate_id" class="form-group lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_remate_id" name="x<?php echo $lotes_list->RowIndex ?>_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_remate_id") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" id="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_remate_id" name="o<?php echo $lotes_list->RowIndex ?>_remate_id" id="o<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo ew_HtmlEncode($lotes->remate_id->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remate_id" class="form-group lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_remate_id" name="x<?php echo $lotes_list->RowIndex ?>_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_remate_id") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" id="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remate_id" class="lotes_remate_id">
<span<?php echo $lotes->remate_id->ViewAttributes() ?>>
<?php echo $lotes->remate_id->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
		<td data-name="conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_conjunto_lote_id" class="form-group lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" name="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "conjuntos_lotes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->conjunto_lote_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id',url:'conjuntos_lotesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_conjunto_lote_id" name="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo ew_HtmlEncode($lotes->conjunto_lote_id->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_conjunto_lote_id" class="form-group lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" name="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "conjuntos_lotes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->conjunto_lote_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id',url:'conjuntos_lotesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_conjunto_lote_id" class="lotes_conjunto_lote_id">
<span<?php echo $lotes->conjunto_lote_id->ViewAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->orden->Visible) { // orden ?>
		<td data-name="orden"<?php echo $lotes->orden->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_orden" class="form-group lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x<?php echo $lotes_list->RowIndex ?>_orden" id="x<?php echo $lotes_list->RowIndex ?>_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_orden" name="o<?php echo $lotes_list->RowIndex ?>_orden" id="o<?php echo $lotes_list->RowIndex ?>_orden" value="<?php echo ew_HtmlEncode($lotes->orden->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_orden" class="form-group lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x<?php echo $lotes_list->RowIndex ?>_orden" id="x<?php echo $lotes_list->RowIndex ?>_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_orden" class="lotes_orden">
<span<?php echo $lotes->orden->ViewAttributes() ?>>
<?php echo $lotes->orden->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->video->Visible) { // video ?>
		<td data-name="video"<?php echo $lotes->video->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_video" class="form-group lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x<?php echo $lotes_list->RowIndex ?>_video" id="x<?php echo $lotes_list->RowIndex ?>_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_video" name="o<?php echo $lotes_list->RowIndex ?>_video" id="o<?php echo $lotes_list->RowIndex ?>_video" value="<?php echo ew_HtmlEncode($lotes->video->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_video" class="form-group lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x<?php echo $lotes_list->RowIndex ?>_video" id="x<?php echo $lotes_list->RowIndex ?>_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_video" class="lotes_video">
<span<?php echo $lotes->video->ViewAttributes() ?>>
<?php echo $lotes->video->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
		<td data-name="foto_1"<?php echo $lotes->foto_1->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_1" class="form-group lotes_foto_1">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x<?php echo $lotes_list->RowIndex ?>_foto_1" id="x<?php echo $lotes_list->RowIndex ?>_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_1" name="o<?php echo $lotes_list->RowIndex ?>_foto_1" id="o<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo ew_HtmlEncode($lotes->foto_1->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_1" class="form-group lotes_foto_1">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x<?php echo $lotes_list->RowIndex ?>_foto_1" id="x<?php echo $lotes_list->RowIndex ?>_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $lotes_list->RowIndex ?>_foto_1"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_1" class="lotes_foto_1">
<span<?php echo $lotes->foto_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_1, $lotes->foto_1->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
		<td data-name="foto_2"<?php echo $lotes->foto_2->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_2" class="form-group lotes_foto_2">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x<?php echo $lotes_list->RowIndex ?>_foto_2" id="x<?php echo $lotes_list->RowIndex ?>_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_2" name="o<?php echo $lotes_list->RowIndex ?>_foto_2" id="o<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo ew_HtmlEncode($lotes->foto_2->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_2" class="form-group lotes_foto_2">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x<?php echo $lotes_list->RowIndex ?>_foto_2" id="x<?php echo $lotes_list->RowIndex ?>_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $lotes_list->RowIndex ?>_foto_2"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_2" class="lotes_foto_2">
<span<?php echo $lotes->foto_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_2, $lotes->foto_2->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
		<td data-name="foto_3"<?php echo $lotes->foto_3->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_3" class="form-group lotes_foto_3">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x<?php echo $lotes_list->RowIndex ?>_foto_3" id="x<?php echo $lotes_list->RowIndex ?>_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_3" name="o<?php echo $lotes_list->RowIndex ?>_foto_3" id="o<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo ew_HtmlEncode($lotes->foto_3->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_3" class="form-group lotes_foto_3">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x<?php echo $lotes_list->RowIndex ?>_foto_3" id="x<?php echo $lotes_list->RowIndex ?>_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $lotes_list->RowIndex ?>_foto_3"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_3" class="lotes_foto_3">
<span<?php echo $lotes->foto_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_3, $lotes->foto_3->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
		<td data-name="foto_4"<?php echo $lotes->foto_4->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_4" class="form-group lotes_foto_4">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x<?php echo $lotes_list->RowIndex ?>_foto_4" id="x<?php echo $lotes_list->RowIndex ?>_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_4" name="o<?php echo $lotes_list->RowIndex ?>_foto_4" id="o<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo ew_HtmlEncode($lotes->foto_4->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_4" class="form-group lotes_foto_4">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x<?php echo $lotes_list->RowIndex ?>_foto_4" id="x<?php echo $lotes_list->RowIndex ?>_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<?php if (@$_POST["fa_x<?php echo $lotes_list->RowIndex ?>_foto_4"] == "0") { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="1">
<?php } ?>
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_foto_4" class="lotes_foto_4">
<span<?php echo $lotes->foto_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_4, $lotes->foto_4->ListViewValue()) ?>
</span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->precio->Visible) { // precio ?>
		<td data-name="precio"<?php echo $lotes->precio->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_precio" class="form-group lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x<?php echo $lotes_list->RowIndex ?>_precio" id="x<?php echo $lotes_list->RowIndex ?>_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_precio" name="o<?php echo $lotes_list->RowIndex ?>_precio" id="o<?php echo $lotes_list->RowIndex ?>_precio" value="<?php echo ew_HtmlEncode($lotes->precio->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_precio" class="form-group lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x<?php echo $lotes_list->RowIndex ?>_precio" id="x<?php echo $lotes_list->RowIndex ?>_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_precio" class="lotes_precio">
<span<?php echo $lotes->precio->ViewAttributes() ?>>
<?php echo $lotes->precio->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
		<td data-name="fecha_film"<?php echo $lotes->fecha_film->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_fecha_film" class="form-group lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x<?php echo $lotes_list->RowIndex ?>_fecha_film" id="x<?php echo $lotes_list->RowIndex ?>_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("floteslist", "x<?php echo $lotes_list->RowIndex ?>_fecha_film", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="lotes" data-field="x_fecha_film" name="o<?php echo $lotes_list->RowIndex ?>_fecha_film" id="o<?php echo $lotes_list->RowIndex ?>_fecha_film" value="<?php echo ew_HtmlEncode($lotes->fecha_film->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_fecha_film" class="form-group lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x<?php echo $lotes_list->RowIndex ?>_fecha_film" id="x<?php echo $lotes_list->RowIndex ?>_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("floteslist", "x<?php echo $lotes_list->RowIndex ?>_fecha_film", 0);
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_fecha_film" class="lotes_fecha_film">
<span<?php echo $lotes->fecha_film->ViewAttributes() ?>>
<?php echo $lotes->fecha_film->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
		<td data-name="establecimiento"<?php echo $lotes->establecimiento->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_establecimiento" class="form-group lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x<?php echo $lotes_list->RowIndex ?>_establecimiento" id="x<?php echo $lotes_list->RowIndex ?>_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_establecimiento" name="o<?php echo $lotes_list->RowIndex ?>_establecimiento" id="o<?php echo $lotes_list->RowIndex ?>_establecimiento" value="<?php echo ew_HtmlEncode($lotes->establecimiento->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_establecimiento" class="form-group lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x<?php echo $lotes_list->RowIndex ?>_establecimiento" id="x<?php echo $lotes_list->RowIndex ?>_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_establecimiento" class="lotes_establecimiento">
<span<?php echo $lotes->establecimiento->ViewAttributes() ?>>
<?php echo $lotes->establecimiento->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->remitente->Visible) { // remitente ?>
		<td data-name="remitente"<?php echo $lotes->remitente->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remitente" class="form-group lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x<?php echo $lotes_list->RowIndex ?>_remitente" id="x<?php echo $lotes_list->RowIndex ?>_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_remitente" name="o<?php echo $lotes_list->RowIndex ?>_remitente" id="o<?php echo $lotes_list->RowIndex ?>_remitente" value="<?php echo ew_HtmlEncode($lotes->remitente->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remitente" class="form-group lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x<?php echo $lotes_list->RowIndex ?>_remitente" id="x<?php echo $lotes_list->RowIndex ?>_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_remitente" class="lotes_remitente">
<span<?php echo $lotes->remitente->ViewAttributes() ?>>
<?php echo $lotes->remitente->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->tipo->Visible) { // tipo ?>
		<td data-name="tipo"<?php echo $lotes->tipo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_tipo" class="form-group lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_tipo" name="x<?php echo $lotes_list->RowIndex ?>_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_tipo") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "tipos")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->tipo->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_tipo',url:'tiposaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_tipo"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->tipo->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_tipo" id="s_x<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_tipo" name="o<?php echo $lotes_list->RowIndex ?>_tipo" id="o<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($lotes->tipo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_tipo" class="form-group lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_tipo" name="x<?php echo $lotes_list->RowIndex ?>_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_tipo") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "tipos")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->tipo->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_tipo',url:'tiposaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_tipo"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->tipo->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_tipo" id="s_x<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_tipo" class="lotes_tipo">
<span<?php echo $lotes->tipo->ViewAttributes() ?>>
<?php echo $lotes->tipo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->categoria->Visible) { // categoria ?>
		<td data-name="categoria"<?php echo $lotes->categoria->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_categoria" class="form-group lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_categoria" name="x<?php echo $lotes_list->RowIndex ?>_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "categorias_ganado")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_categoria',url:'categorias_ganadoaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_categoria" name="o<?php echo $lotes_list->RowIndex ?>_categoria" id="o<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo ew_HtmlEncode($lotes->categoria->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_categoria" class="form-group lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_categoria" name="x<?php echo $lotes_list->RowIndex ?>_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "categorias_ganado")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_categoria',url:'categorias_ganadoaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_categoria" class="lotes_categoria">
<span<?php echo $lotes->categoria->ViewAttributes() ?>>
<?php echo $lotes->categoria->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
		<td data-name="sub_categoria"<?php echo $lotes->sub_categoria->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_sub_categoria" class="form-group lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_sub_categoria" name="x<?php echo $lotes_list->RowIndex ?>_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_sub_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "sub_categorias")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->sub_categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_sub_categoria',url:'sub_categoriasaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_sub_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->sub_categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_sub_categoria" name="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo ew_HtmlEncode($lotes->sub_categoria->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_sub_categoria" class="form-group lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_sub_categoria" name="x<?php echo $lotes_list->RowIndex ?>_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_sub_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "sub_categorias")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->sub_categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_sub_categoria',url:'sub_categoriasaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_sub_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->sub_categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_sub_categoria" class="lotes_sub_categoria">
<span<?php echo $lotes->sub_categoria->ViewAttributes() ?>>
<?php echo $lotes->sub_categoria->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad"<?php echo $lotes->cantidad->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_cantidad" class="form-group lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x<?php echo $lotes_list->RowIndex ?>_cantidad" id="x<?php echo $lotes_list->RowIndex ?>_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_cantidad" name="o<?php echo $lotes_list->RowIndex ?>_cantidad" id="o<?php echo $lotes_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($lotes->cantidad->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_cantidad" class="form-group lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x<?php echo $lotes_list->RowIndex ?>_cantidad" id="x<?php echo $lotes_list->RowIndex ?>_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_cantidad" class="lotes_cantidad">
<span<?php echo $lotes->cantidad->ViewAttributes() ?>>
<?php echo $lotes->cantidad->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->peso->Visible) { // peso ?>
		<td data-name="peso"<?php echo $lotes->peso->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_peso" class="form-group lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x<?php echo $lotes_list->RowIndex ?>_peso" id="x<?php echo $lotes_list->RowIndex ?>_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_peso" name="o<?php echo $lotes_list->RowIndex ?>_peso" id="o<?php echo $lotes_list->RowIndex ?>_peso" value="<?php echo ew_HtmlEncode($lotes->peso->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_peso" class="form-group lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x<?php echo $lotes_list->RowIndex ?>_peso" id="x<?php echo $lotes_list->RowIndex ?>_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_peso" class="lotes_peso">
<span<?php echo $lotes->peso->ViewAttributes() ?>>
<?php echo $lotes->peso->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
		<td data-name="caracteristicas"<?php echo $lotes->caracteristicas->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_caracteristicas" class="form-group lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_caracteristicas" name="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" value="<?php echo ew_HtmlEncode($lotes->caracteristicas->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_caracteristicas" class="form-group lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_caracteristicas" class="lotes_caracteristicas">
<span<?php echo $lotes->caracteristicas->ViewAttributes() ?>>
<?php echo $lotes->caracteristicas->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->provincia->Visible) { // provincia ?>
		<td data-name="provincia"<?php echo $lotes->provincia->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_provincia" class="form-group lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_provincia" name="x<?php echo $lotes_list->RowIndex ?>_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_provincia") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_provincia" id="s_x<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_provincia" name="o<?php echo $lotes_list->RowIndex ?>_provincia" id="o<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo ew_HtmlEncode($lotes->provincia->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_provincia" class="form-group lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_provincia" name="x<?php echo $lotes_list->RowIndex ?>_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_provincia") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_provincia" id="s_x<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_provincia" class="lotes_provincia">
<span<?php echo $lotes->provincia->ViewAttributes() ?>>
<?php echo $lotes->provincia->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->localidad->Visible) { // localidad ?>
		<td data-name="localidad"<?php echo $lotes->localidad->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_localidad" class="form-group lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $lotes_list->RowIndex ?>_localidad" style="white-space: nowrap; z-index: <?php echo (9000 - $lotes_list->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" id="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_localidad" id="x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x<?php echo $lotes_list->RowIndex ?>_localidad" id="q_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
floteslist.CreateAutoSuggest({"id":"x<?php echo $lotes_list->RowIndex ?>_localidad","forceSelect":false});
</script>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->localidad->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_localidad',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_localidad"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->localidad->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_localidad" id="s_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" name="o<?php echo $lotes_list->RowIndex ?>_localidad" id="o<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_localidad" class="form-group lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $lotes_list->RowIndex ?>_localidad" style="white-space: nowrap; z-index: <?php echo (9000 - $lotes_list->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" id="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_localidad" id="x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x<?php echo $lotes_list->RowIndex ?>_localidad" id="q_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
floteslist.CreateAutoSuggest({"id":"x<?php echo $lotes_list->RowIndex ?>_localidad","forceSelect":false});
</script>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->localidad->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_localidad',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_localidad"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->localidad->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_localidad" id="s_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_localidad" class="lotes_localidad">
<span<?php echo $lotes->localidad->ViewAttributes() ?>>
<?php echo $lotes->localidad->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->lugar->Visible) { // lugar ?>
		<td data-name="lugar"<?php echo $lotes->lugar->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_lugar" class="form-group lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x<?php echo $lotes_list->RowIndex ?>_lugar" id="x<?php echo $lotes_list->RowIndex ?>_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_lugar" name="o<?php echo $lotes_list->RowIndex ?>_lugar" id="o<?php echo $lotes_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($lotes->lugar->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_lugar" class="form-group lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x<?php echo $lotes_list->RowIndex ?>_lugar" id="x<?php echo $lotes_list->RowIndex ?>_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_lugar" class="lotes_lugar">
<span<?php echo $lotes->lugar->ViewAttributes() ?>>
<?php echo $lotes->lugar->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->titulo->Visible) { // titulo ?>
		<td data-name="titulo"<?php echo $lotes->titulo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_titulo" class="form-group lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x<?php echo $lotes_list->RowIndex ?>_titulo" id="x<?php echo $lotes_list->RowIndex ?>_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_titulo" name="o<?php echo $lotes_list->RowIndex ?>_titulo" id="o<?php echo $lotes_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($lotes->titulo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_titulo" class="form-group lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x<?php echo $lotes_list->RowIndex ?>_titulo" id="x<?php echo $lotes_list->RowIndex ?>_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_titulo" class="lotes_titulo">
<span<?php echo $lotes->titulo->ViewAttributes() ?>>
<?php echo $lotes->titulo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
		<td data-name="edad_aprox"<?php echo $lotes->edad_aprox->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_edad_aprox" class="form-group lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_edad_aprox" name="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" value="<?php echo ew_HtmlEncode($lotes->edad_aprox->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_edad_aprox" class="form-group lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_edad_aprox" class="lotes_edad_aprox">
<span<?php echo $lotes->edad_aprox->ViewAttributes() ?>>
<?php echo $lotes->edad_aprox->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
		<td data-name="trazabilidad"<?php echo $lotes->trazabilidad->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_trazabilidad" class="form-group lotes_trazabilidad">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_trazabilidad") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_trazabilidad" name="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="<?php echo ew_HtmlEncode($lotes->trazabilidad->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_trazabilidad" class="form-group lotes_trazabilidad">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_trazabilidad") ?>
</div></div>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_trazabilidad" class="lotes_trazabilidad">
<span<?php echo $lotes->trazabilidad->ViewAttributes() ?>>
<?php echo $lotes->trazabilidad->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
		<td data-name="mio_mio"<?php echo $lotes->mio_mio->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_mio_mio" class="form-group lotes_mio_mio">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_mio_mio" id="x<?php echo $lotes_list->RowIndex ?>_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_mio_mio") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_mio_mio" name="o<?php echo $lotes_list->RowIndex ?>_mio_mio" id="o<?php echo $lotes_list->RowIndex ?>_mio_mio" value="<?php echo ew_HtmlEncode($lotes->mio_mio->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_mio_mio" class="form-group lotes_mio_mio">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_mio_mio" id="x<?php echo $lotes_list->RowIndex ?>_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_mio_mio") ?>
</div></div>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_mio_mio" class="lotes_mio_mio">
<span<?php echo $lotes->mio_mio->ViewAttributes() ?>>
<?php echo $lotes->mio_mio->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->garrapata->Visible) { // garrapata ?>
		<td data-name="garrapata"<?php echo $lotes->garrapata->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_garrapata" class="form-group lotes_garrapata">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_garrapata" id="x<?php echo $lotes_list->RowIndex ?>_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_garrapata") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_garrapata" name="o<?php echo $lotes_list->RowIndex ?>_garrapata" id="o<?php echo $lotes_list->RowIndex ?>_garrapata" value="<?php echo ew_HtmlEncode($lotes->garrapata->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_garrapata" class="form-group lotes_garrapata">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_garrapata" id="x<?php echo $lotes_list->RowIndex ?>_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_garrapata") ?>
</div></div>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_garrapata" class="lotes_garrapata">
<span<?php echo $lotes->garrapata->ViewAttributes() ?>>
<?php echo $lotes->garrapata->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->plazo->Visible) { // plazo ?>
		<td data-name="plazo"<?php echo $lotes->plazo->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_plazo" class="form-group lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x<?php echo $lotes_list->RowIndex ?>_plazo" id="x<?php echo $lotes_list->RowIndex ?>_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_plazo" name="o<?php echo $lotes_list->RowIndex ?>_plazo" id="o<?php echo $lotes_list->RowIndex ?>_plazo" value="<?php echo ew_HtmlEncode($lotes->plazo->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_plazo" class="form-group lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x<?php echo $lotes_list->RowIndex ?>_plazo" id="x<?php echo $lotes_list->RowIndex ?>_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_plazo" class="lotes_plazo">
<span<?php echo $lotes->plazo->ViewAttributes() ?>>
<?php echo $lotes->plazo->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($lotes->visitas->Visible) { // visitas ?>
		<td data-name="visitas"<?php echo $lotes->visitas->CellAttributes() ?>>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_visitas" class="form-group lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x<?php echo $lotes_list->RowIndex ?>_visitas" id="x<?php echo $lotes_list->RowIndex ?>_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_visitas" name="o<?php echo $lotes_list->RowIndex ?>_visitas" id="o<?php echo $lotes_list->RowIndex ?>_visitas" value="<?php echo ew_HtmlEncode($lotes->visitas->OldValue) ?>">
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_visitas" class="form-group lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x<?php echo $lotes_list->RowIndex ?>_visitas" id="x<?php echo $lotes_list->RowIndex ?>_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($lotes->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $lotes_list->RowCnt ?>_lotes_visitas" class="lotes_visitas">
<span<?php echo $lotes->visitas->ViewAttributes() ?>>
<?php echo $lotes->visitas->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$lotes_list->ListOptions->Render("body", "right", $lotes_list->RowCnt);
?>
	</tr>
<?php if ($lotes->RowType == EW_ROWTYPE_ADD || $lotes->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
floteslist.UpdateOpts(<?php echo $lotes_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($lotes->CurrentAction <> "gridadd")
		if (!$lotes_list->Recordset->EOF) $lotes_list->Recordset->MoveNext();
}
?>
<?php
	if ($lotes->CurrentAction == "gridadd" || $lotes->CurrentAction == "gridedit") {
		$lotes_list->RowIndex = '$rowindex$';
		$lotes_list->LoadDefaultValues();

		// Set row properties
		$lotes->ResetAttrs();
		$lotes->RowAttrs = array_merge($lotes->RowAttrs, array('data-rowindex'=>$lotes_list->RowIndex, 'id'=>'r0_lotes', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($lotes->RowAttrs["class"], "ewTemplate");
		$lotes->RowType = EW_ROWTYPE_ADD;

		// Render row
		$lotes_list->RenderRow();

		// Render list options
		$lotes_list->RenderListOptions();
		$lotes_list->StartRowCnt = 0;
?>
	<tr<?php echo $lotes->RowAttributes() ?>>
<?php

// Render list options (body, left)
$lotes_list->ListOptions->Render("body", "left", $lotes_list->RowIndex);
?>
	<?php if ($lotes->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="lotes" data-field="x_id" name="o<?php echo $lotes_list->RowIndex ?>_id" id="o<?php echo $lotes_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($lotes->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
		<td data-name="venta_tipo">
<span id="el$rowindex$_lotes_venta_tipo" class="form-group lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_venta_tipo" name="x<?php echo $lotes_list->RowIndex ?>_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_venta_tipo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_venta_tipo" name="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" id="o<?php echo $lotes_list->RowIndex ?>_venta_tipo" value="<?php echo ew_HtmlEncode($lotes->venta_tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->activo->Visible) { // activo ?>
		<td data-name="activo">
<span id="el$rowindex$_lotes_activo" class="form-group lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_activo" name="x<?php echo $lotes_list->RowIndex ?>_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_activo") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_activo" name="o<?php echo $lotes_list->RowIndex ?>_activo" id="o<?php echo $lotes_list->RowIndex ?>_activo" value="<?php echo ew_HtmlEncode($lotes->activo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->vendido->Visible) { // vendido ?>
		<td data-name="vendido">
<span id="el$rowindex$_lotes_vendido" class="form-group lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_vendido" name="x<?php echo $lotes_list->RowIndex ?>_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_vendido") ?>
</select>
</span>
<input type="hidden" data-table="lotes" data-field="x_vendido" name="o<?php echo $lotes_list->RowIndex ?>_vendido" id="o<?php echo $lotes_list->RowIndex ?>_vendido" value="<?php echo ew_HtmlEncode($lotes->vendido->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->codigo->Visible) { // codigo ?>
		<td data-name="codigo">
<span id="el$rowindex$_lotes_codigo" class="form-group lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x<?php echo $lotes_list->RowIndex ?>_codigo" id="x<?php echo $lotes_list->RowIndex ?>_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_codigo" name="o<?php echo $lotes_list->RowIndex ?>_codigo" id="o<?php echo $lotes_list->RowIndex ?>_codigo" value="<?php echo ew_HtmlEncode($lotes->codigo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->remate_id->Visible) { // remate_id ?>
		<td data-name="remate_id">
<span id="el$rowindex$_lotes_remate_id" class="form-group lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_remate_id" name="x<?php echo $lotes_list->RowIndex ?>_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_remate_id") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" id="s_x<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_remate_id" name="o<?php echo $lotes_list->RowIndex ?>_remate_id" id="o<?php echo $lotes_list->RowIndex ?>_remate_id" value="<?php echo ew_HtmlEncode($lotes->remate_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
		<td data-name="conjunto_lote_id">
<span id="el$rowindex$_lotes_conjunto_lote_id" class="form-group lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" name="x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "conjuntos_lotes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->conjunto_lote_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id',url:'conjuntos_lotesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="s_x<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_conjunto_lote_id" name="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" id="o<?php echo $lotes_list->RowIndex ?>_conjunto_lote_id" value="<?php echo ew_HtmlEncode($lotes->conjunto_lote_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->orden->Visible) { // orden ?>
		<td data-name="orden">
<span id="el$rowindex$_lotes_orden" class="form-group lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x<?php echo $lotes_list->RowIndex ?>_orden" id="x<?php echo $lotes_list->RowIndex ?>_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_orden" name="o<?php echo $lotes_list->RowIndex ?>_orden" id="o<?php echo $lotes_list->RowIndex ?>_orden" value="<?php echo ew_HtmlEncode($lotes->orden->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->video->Visible) { // video ?>
		<td data-name="video">
<span id="el$rowindex$_lotes_video" class="form-group lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x<?php echo $lotes_list->RowIndex ?>_video" id="x<?php echo $lotes_list->RowIndex ?>_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_video" name="o<?php echo $lotes_list->RowIndex ?>_video" id="o<?php echo $lotes_list->RowIndex ?>_video" value="<?php echo ew_HtmlEncode($lotes->video->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
		<td data-name="foto_1">
<span id="el$rowindex$_lotes_foto_1" class="form-group lotes_foto_1">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x<?php echo $lotes_list->RowIndex ?>_foto_1" id="x<?php echo $lotes_list->RowIndex ?>_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_1" name="o<?php echo $lotes_list->RowIndex ?>_foto_1" id="o<?php echo $lotes_list->RowIndex ?>_foto_1" value="<?php echo ew_HtmlEncode($lotes->foto_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
		<td data-name="foto_2">
<span id="el$rowindex$_lotes_foto_2" class="form-group lotes_foto_2">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x<?php echo $lotes_list->RowIndex ?>_foto_2" id="x<?php echo $lotes_list->RowIndex ?>_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_2" name="o<?php echo $lotes_list->RowIndex ?>_foto_2" id="o<?php echo $lotes_list->RowIndex ?>_foto_2" value="<?php echo ew_HtmlEncode($lotes->foto_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
		<td data-name="foto_3">
<span id="el$rowindex$_lotes_foto_3" class="form-group lotes_foto_3">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x<?php echo $lotes_list->RowIndex ?>_foto_3" id="x<?php echo $lotes_list->RowIndex ?>_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_3" name="o<?php echo $lotes_list->RowIndex ?>_foto_3" id="o<?php echo $lotes_list->RowIndex ?>_foto_3" value="<?php echo ew_HtmlEncode($lotes->foto_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
		<td data-name="foto_4">
<span id="el$rowindex$_lotes_foto_4" class="form-group lotes_foto_4">
<div id="fd_x<?php echo $lotes_list->RowIndex ?>_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x<?php echo $lotes_list->RowIndex ?>_foto_4" id="x<?php echo $lotes_list->RowIndex ?>_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fn_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<input type="hidden" name="fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fa_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="0">
<input type="hidden" name="fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fs_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="255">
<input type="hidden" name="fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fx_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" id= "fm_x<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x<?php echo $lotes_list->RowIndex ?>_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<input type="hidden" data-table="lotes" data-field="x_foto_4" name="o<?php echo $lotes_list->RowIndex ?>_foto_4" id="o<?php echo $lotes_list->RowIndex ?>_foto_4" value="<?php echo ew_HtmlEncode($lotes->foto_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->precio->Visible) { // precio ?>
		<td data-name="precio">
<span id="el$rowindex$_lotes_precio" class="form-group lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x<?php echo $lotes_list->RowIndex ?>_precio" id="x<?php echo $lotes_list->RowIndex ?>_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_precio" name="o<?php echo $lotes_list->RowIndex ?>_precio" id="o<?php echo $lotes_list->RowIndex ?>_precio" value="<?php echo ew_HtmlEncode($lotes->precio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
		<td data-name="fecha_film">
<span id="el$rowindex$_lotes_fecha_film" class="form-group lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x<?php echo $lotes_list->RowIndex ?>_fecha_film" id="x<?php echo $lotes_list->RowIndex ?>_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("floteslist", "x<?php echo $lotes_list->RowIndex ?>_fecha_film", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="lotes" data-field="x_fecha_film" name="o<?php echo $lotes_list->RowIndex ?>_fecha_film" id="o<?php echo $lotes_list->RowIndex ?>_fecha_film" value="<?php echo ew_HtmlEncode($lotes->fecha_film->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
		<td data-name="establecimiento">
<span id="el$rowindex$_lotes_establecimiento" class="form-group lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x<?php echo $lotes_list->RowIndex ?>_establecimiento" id="x<?php echo $lotes_list->RowIndex ?>_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_establecimiento" name="o<?php echo $lotes_list->RowIndex ?>_establecimiento" id="o<?php echo $lotes_list->RowIndex ?>_establecimiento" value="<?php echo ew_HtmlEncode($lotes->establecimiento->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->remitente->Visible) { // remitente ?>
		<td data-name="remitente">
<span id="el$rowindex$_lotes_remitente" class="form-group lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x<?php echo $lotes_list->RowIndex ?>_remitente" id="x<?php echo $lotes_list->RowIndex ?>_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_remitente" name="o<?php echo $lotes_list->RowIndex ?>_remitente" id="o<?php echo $lotes_list->RowIndex ?>_remitente" value="<?php echo ew_HtmlEncode($lotes->remitente->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->tipo->Visible) { // tipo ?>
		<td data-name="tipo">
<span id="el$rowindex$_lotes_tipo" class="form-group lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_tipo" name="x<?php echo $lotes_list->RowIndex ?>_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_tipo") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "tipos")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->tipo->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_tipo',url:'tiposaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_tipo"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->tipo->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_tipo" id="s_x<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_tipo" name="o<?php echo $lotes_list->RowIndex ?>_tipo" id="o<?php echo $lotes_list->RowIndex ?>_tipo" value="<?php echo ew_HtmlEncode($lotes->tipo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->categoria->Visible) { // categoria ?>
		<td data-name="categoria">
<span id="el$rowindex$_lotes_categoria" class="form-group lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_categoria" name="x<?php echo $lotes_list->RowIndex ?>_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "categorias_ganado")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_categoria',url:'categorias_ganadoaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_categoria" name="o<?php echo $lotes_list->RowIndex ?>_categoria" id="o<?php echo $lotes_list->RowIndex ?>_categoria" value="<?php echo ew_HtmlEncode($lotes->categoria->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
		<td data-name="sub_categoria">
<span id="el$rowindex$_lotes_sub_categoria" class="form-group lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_sub_categoria" name="x<?php echo $lotes_list->RowIndex ?>_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_sub_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "sub_categorias")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->sub_categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_sub_categoria',url:'sub_categoriasaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_sub_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->sub_categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="s_x<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_sub_categoria" name="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" id="o<?php echo $lotes_list->RowIndex ?>_sub_categoria" value="<?php echo ew_HtmlEncode($lotes->sub_categoria->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->cantidad->Visible) { // cantidad ?>
		<td data-name="cantidad">
<span id="el$rowindex$_lotes_cantidad" class="form-group lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x<?php echo $lotes_list->RowIndex ?>_cantidad" id="x<?php echo $lotes_list->RowIndex ?>_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_cantidad" name="o<?php echo $lotes_list->RowIndex ?>_cantidad" id="o<?php echo $lotes_list->RowIndex ?>_cantidad" value="<?php echo ew_HtmlEncode($lotes->cantidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->peso->Visible) { // peso ?>
		<td data-name="peso">
<span id="el$rowindex$_lotes_peso" class="form-group lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x<?php echo $lotes_list->RowIndex ?>_peso" id="x<?php echo $lotes_list->RowIndex ?>_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_peso" name="o<?php echo $lotes_list->RowIndex ?>_peso" id="o<?php echo $lotes_list->RowIndex ?>_peso" value="<?php echo ew_HtmlEncode($lotes->peso->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
		<td data-name="caracteristicas">
<span id="el$rowindex$_lotes_caracteristicas" class="form-group lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="x<?php echo $lotes_list->RowIndex ?>_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_caracteristicas" name="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" id="o<?php echo $lotes_list->RowIndex ?>_caracteristicas" value="<?php echo ew_HtmlEncode($lotes->caracteristicas->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->provincia->Visible) { // provincia ?>
		<td data-name="provincia">
<span id="el$rowindex$_lotes_provincia" class="form-group lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $lotes_list->RowIndex ?>_provincia" name="x<?php echo $lotes_list->RowIndex ?>_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x<?php echo $lotes_list->RowIndex ?>_provincia") ?>
</select>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_provincia" id="s_x<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_provincia" name="o<?php echo $lotes_list->RowIndex ?>_provincia" id="o<?php echo $lotes_list->RowIndex ?>_provincia" value="<?php echo ew_HtmlEncode($lotes->provincia->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->localidad->Visible) { // localidad ?>
		<td data-name="localidad">
<span id="el$rowindex$_lotes_localidad" class="form-group lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $lotes_list->RowIndex ?>_localidad" style="white-space: nowrap; z-index: <?php echo (9000 - $lotes_list->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" id="sv_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_localidad" id="x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x<?php echo $lotes_list->RowIndex ?>_localidad" id="q_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
floteslist.CreateAutoSuggest({"id":"x<?php echo $lotes_list->RowIndex ?>_localidad","forceSelect":false});
</script>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->localidad->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x<?php echo $lotes_list->RowIndex ?>_localidad',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x<?php echo $lotes_list->RowIndex ?>_localidad"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->localidad->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x<?php echo $lotes_list->RowIndex ?>_localidad" id="s_x<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" name="o<?php echo $lotes_list->RowIndex ?>_localidad" id="o<?php echo $lotes_list->RowIndex ?>_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->lugar->Visible) { // lugar ?>
		<td data-name="lugar">
<span id="el$rowindex$_lotes_lugar" class="form-group lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x<?php echo $lotes_list->RowIndex ?>_lugar" id="x<?php echo $lotes_list->RowIndex ?>_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_lugar" name="o<?php echo $lotes_list->RowIndex ?>_lugar" id="o<?php echo $lotes_list->RowIndex ?>_lugar" value="<?php echo ew_HtmlEncode($lotes->lugar->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->titulo->Visible) { // titulo ?>
		<td data-name="titulo">
<span id="el$rowindex$_lotes_titulo" class="form-group lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x<?php echo $lotes_list->RowIndex ?>_titulo" id="x<?php echo $lotes_list->RowIndex ?>_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_titulo" name="o<?php echo $lotes_list->RowIndex ?>_titulo" id="o<?php echo $lotes_list->RowIndex ?>_titulo" value="<?php echo ew_HtmlEncode($lotes->titulo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
		<td data-name="edad_aprox">
<span id="el$rowindex$_lotes_edad_aprox" class="form-group lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="x<?php echo $lotes_list->RowIndex ?>_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_edad_aprox" name="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" id="o<?php echo $lotes_list->RowIndex ?>_edad_aprox" value="<?php echo ew_HtmlEncode($lotes->edad_aprox->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
		<td data-name="trazabilidad">
<span id="el$rowindex$_lotes_trazabilidad" class="form-group lotes_trazabilidad">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="x<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_trazabilidad") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_trazabilidad" name="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" id="o<?php echo $lotes_list->RowIndex ?>_trazabilidad" value="<?php echo ew_HtmlEncode($lotes->trazabilidad->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
		<td data-name="mio_mio">
<span id="el$rowindex$_lotes_mio_mio" class="form-group lotes_mio_mio">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_mio_mio" id="x<?php echo $lotes_list->RowIndex ?>_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_mio_mio") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_mio_mio" name="o<?php echo $lotes_list->RowIndex ?>_mio_mio" id="o<?php echo $lotes_list->RowIndex ?>_mio_mio" value="<?php echo ew_HtmlEncode($lotes->mio_mio->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->garrapata->Visible) { // garrapata ?>
		<td data-name="garrapata">
<span id="el$rowindex$_lotes_garrapata" class="form-group lotes_garrapata">
<div id="tp_x<?php echo $lotes_list->RowIndex ?>_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x<?php echo $lotes_list->RowIndex ?>_garrapata" id="x<?php echo $lotes_list->RowIndex ?>_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $lotes_list->RowIndex ?>_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x{$lotes_list->RowIndex}_garrapata") ?>
</div></div>
</span>
<input type="hidden" data-table="lotes" data-field="x_garrapata" name="o<?php echo $lotes_list->RowIndex ?>_garrapata" id="o<?php echo $lotes_list->RowIndex ?>_garrapata" value="<?php echo ew_HtmlEncode($lotes->garrapata->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->plazo->Visible) { // plazo ?>
		<td data-name="plazo">
<span id="el$rowindex$_lotes_plazo" class="form-group lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x<?php echo $lotes_list->RowIndex ?>_plazo" id="x<?php echo $lotes_list->RowIndex ?>_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_plazo" name="o<?php echo $lotes_list->RowIndex ?>_plazo" id="o<?php echo $lotes_list->RowIndex ?>_plazo" value="<?php echo ew_HtmlEncode($lotes->plazo->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($lotes->visitas->Visible) { // visitas ?>
		<td data-name="visitas">
<span id="el$rowindex$_lotes_visitas" class="form-group lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x<?php echo $lotes_list->RowIndex ?>_visitas" id="x<?php echo $lotes_list->RowIndex ?>_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_visitas" name="o<?php echo $lotes_list->RowIndex ?>_visitas" id="o<?php echo $lotes_list->RowIndex ?>_visitas" value="<?php echo ew_HtmlEncode($lotes->visitas->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$lotes_list->ListOptions->Render("body", "right", $lotes_list->RowCnt);
?>
<script type="text/javascript">
floteslist.UpdateOpts(<?php echo $lotes_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($lotes->CurrentAction == "add" || $lotes->CurrentAction == "copy") { ?>
<input type="hidden" name="<?php echo $lotes_list->FormKeyCountName ?>" id="<?php echo $lotes_list->FormKeyCountName ?>" value="<?php echo $lotes_list->KeyCount ?>">
<?php } ?>
<?php if ($lotes->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $lotes_list->FormKeyCountName ?>" id="<?php echo $lotes_list->FormKeyCountName ?>" value="<?php echo $lotes_list->KeyCount ?>">
<?php echo $lotes_list->MultiSelectKey ?>
<?php } ?>
<?php if ($lotes->CurrentAction == "edit") { ?>
<input type="hidden" name="<?php echo $lotes_list->FormKeyCountName ?>" id="<?php echo $lotes_list->FormKeyCountName ?>" value="<?php echo $lotes_list->KeyCount ?>">
<?php } ?>
<?php if ($lotes->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $lotes_list->FormKeyCountName ?>" id="<?php echo $lotes_list->FormKeyCountName ?>" value="<?php echo $lotes_list->KeyCount ?>">
<?php echo $lotes_list->MultiSelectKey ?>
<?php } ?>
<?php if ($lotes->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($lotes_list->Recordset)
	$lotes_list->Recordset->Close();
?>
<?php if ($lotes->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($lotes->CurrentAction <> "gridadd" && $lotes->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($lotes_list->Pager)) $lotes_list->Pager = new cPrevNextPager($lotes_list->StartRec, $lotes_list->DisplayRecs, $lotes_list->TotalRecs) ?>
<?php if ($lotes_list->Pager->RecordCount > 0 && $lotes_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($lotes_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($lotes_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $lotes_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($lotes_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($lotes_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $lotes_list->PageUrl() ?>start=<?php echo $lotes_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $lotes_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $lotes_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $lotes_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $lotes_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($lotes_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($lotes_list->TotalRecs == 0 && $lotes->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($lotes_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">
floteslistsrch.FilterList = <?php echo $lotes_list->GetFilterList() ?>;
floteslistsrch.Init();
floteslist.Init();
</script>
<?php } ?>
<?php
$lotes_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$lotes_list->Page_Terminate();
?>
