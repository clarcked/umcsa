<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "propiedadesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$propiedades_view = NULL; // Initialize page object first

class cpropiedades_view extends cpropiedades {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{1B8CF9BA-91E4-4950-9047-342B30417538}";

	// Table name
	var $TableName = 'propiedades';

	// Page object name
	var $PageObjName = 'propiedades_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (propiedades)
		if (!isset($GLOBALS["propiedades"]) || get_class($GLOBALS["propiedades"]) == "cpropiedades") {
			$GLOBALS["propiedades"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["propiedades"];
		}
		$KeyUrl = "";
		if (@$_GET["id"] <> "") {
			$this->RecKey["id"] = $_GET["id"];
			$KeyUrl .= "&amp;id=" . urlencode($this->RecKey["id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'propiedades', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("propiedadeslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->prov_id->SetVisibility();
		$this->loca_id->SetVisibility();
		$this->titulo->SetVisibility();
		$this->tipo_id->SetVisibility();
		$this->actividad->SetVisibility();
		$this->oper_tipo->SetVisibility();
		$this->descripcion->SetVisibility();
		$this->img_1->SetVisibility();
		$this->img_2->SetVisibility();
		$this->img_3->SetVisibility();
		$this->img_4->SetVisibility();
		$this->img_5->SetVisibility();
		$this->img_6->SetVisibility();
		$this->img_7->SetVisibility();
		$this->img_8->SetVisibility();
		$this->img_9->SetVisibility();
		$this->img_10->SetVisibility();
		$this->video_servidor->SetVisibility();
		$this->video->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $propiedades;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($propiedades);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $IsModal = FALSE;
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->RecKey["id"] = $this->id->QueryStringValue;
			} elseif (@$_POST["id"] <> "") {
				$this->id->setFormValue($_POST["id"]);
				$this->RecKey["id"] = $this->id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("propiedadeslist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->id->CurrentValue) == strval($this->Recordset->fields('id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "propiedadeslist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "propiedadeslist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("ViewPageAddLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->AddUrl) . "',caption:'" . $addcaption . "'});\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$editcaption = ew_HtmlTitle($Language->Phrase("ViewPageEditLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->EditUrl) . "',caption:'" . $editcaption . "'});\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$copycaption = ew_HtmlTitle($Language->Phrase("ViewPageCopyLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->CopyUrl) . "',caption:'" . $copycaption . "'});\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		if ($this->IsModal) // Handle as inline delete
			$item->Body = "<a onclick=\"return ew_ConfirmDelete(this);\" class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode(ew_AddQueryStringToUrl($this->DeleteUrl, "a_delete=1")) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->prov_id->setDbValue($rs->fields('prov_id'));
		if (array_key_exists('EV__prov_id', $rs->fields)) {
			$this->prov_id->VirtualValue = $rs->fields('EV__prov_id'); // Set up virtual field value
		} else {
			$this->prov_id->VirtualValue = ""; // Clear value
		}
		$this->loca_id->setDbValue($rs->fields('loca_id'));
		if (array_key_exists('EV__loca_id', $rs->fields)) {
			$this->loca_id->VirtualValue = $rs->fields('EV__loca_id'); // Set up virtual field value
		} else {
			$this->loca_id->VirtualValue = ""; // Clear value
		}
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->tipo_id->setDbValue($rs->fields('tipo_id'));
		$this->actividad->setDbValue($rs->fields('actividad'));
		$this->oper_tipo->setDbValue($rs->fields('oper_tipo'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->img_1->Upload->DbValue = $rs->fields('img_1');
		$this->img_1->CurrentValue = $this->img_1->Upload->DbValue;
		$this->img_2->Upload->DbValue = $rs->fields('img_2');
		$this->img_2->CurrentValue = $this->img_2->Upload->DbValue;
		$this->img_3->Upload->DbValue = $rs->fields('img_3');
		$this->img_3->CurrentValue = $this->img_3->Upload->DbValue;
		$this->img_4->Upload->DbValue = $rs->fields('img_4');
		$this->img_4->CurrentValue = $this->img_4->Upload->DbValue;
		$this->img_5->Upload->DbValue = $rs->fields('img_5');
		$this->img_5->CurrentValue = $this->img_5->Upload->DbValue;
		$this->img_6->Upload->DbValue = $rs->fields('img_6');
		$this->img_6->CurrentValue = $this->img_6->Upload->DbValue;
		$this->img_7->Upload->DbValue = $rs->fields('img_7');
		$this->img_7->CurrentValue = $this->img_7->Upload->DbValue;
		$this->img_8->Upload->DbValue = $rs->fields('img_8');
		$this->img_8->CurrentValue = $this->img_8->Upload->DbValue;
		$this->img_9->Upload->DbValue = $rs->fields('img_9');
		$this->img_9->CurrentValue = $this->img_9->Upload->DbValue;
		$this->img_10->Upload->DbValue = $rs->fields('img_10');
		$this->img_10->CurrentValue = $this->img_10->Upload->DbValue;
		$this->video_servidor->setDbValue($rs->fields('video_servidor'));
		$this->video->setDbValue($rs->fields('video'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->prov_id->DbValue = $row['prov_id'];
		$this->loca_id->DbValue = $row['loca_id'];
		$this->titulo->DbValue = $row['titulo'];
		$this->tipo_id->DbValue = $row['tipo_id'];
		$this->actividad->DbValue = $row['actividad'];
		$this->oper_tipo->DbValue = $row['oper_tipo'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->img_1->Upload->DbValue = $row['img_1'];
		$this->img_2->Upload->DbValue = $row['img_2'];
		$this->img_3->Upload->DbValue = $row['img_3'];
		$this->img_4->Upload->DbValue = $row['img_4'];
		$this->img_5->Upload->DbValue = $row['img_5'];
		$this->img_6->Upload->DbValue = $row['img_6'];
		$this->img_7->Upload->DbValue = $row['img_7'];
		$this->img_8->Upload->DbValue = $row['img_8'];
		$this->img_9->Upload->DbValue = $row['img_9'];
		$this->img_10->Upload->DbValue = $row['img_10'];
		$this->video_servidor->DbValue = $row['video_servidor'];
		$this->video->DbValue = $row['video'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// prov_id
		// loca_id
		// titulo
		// tipo_id
		// actividad
		// oper_tipo
		// descripcion
		// img_1
		// img_2
		// img_3
		// img_4
		// img_5
		// img_6
		// img_7
		// img_8
		// img_9
		// img_10
		// video_servidor
		// video

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		if ($this->prov_id->VirtualValue <> "") {
			$this->prov_id->ViewValue = $this->prov_id->VirtualValue;
		} else {
		if (strval($this->prov_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->prov_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prov_id->ViewValue = $this->prov_id->CurrentValue;
			}
		} else {
			$this->prov_id->ViewValue = NULL;
		}
		}
		$this->prov_id->ViewCustomAttributes = "";

		// loca_id
		if ($this->loca_id->VirtualValue <> "") {
			$this->loca_id->ViewValue = $this->loca_id->VirtualValue;
		} else {
		if (strval($this->loca_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->loca_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->loca_id->ViewValue = $this->loca_id->CurrentValue;
			}
		} else {
			$this->loca_id->ViewValue = NULL;
		}
		}
		$this->loca_id->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// tipo_id
		$this->tipo_id->ViewValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->ViewCustomAttributes = "";

		// actividad
		if (strval($this->actividad->CurrentValue) <> "") {
			$this->actividad->ViewValue = $this->actividad->OptionCaption($this->actividad->CurrentValue);
		} else {
			$this->actividad->ViewValue = NULL;
		}
		$this->actividad->ViewCustomAttributes = "";

		// oper_tipo
		if (strval($this->oper_tipo->CurrentValue) <> "") {
			$this->oper_tipo->ViewValue = $this->oper_tipo->OptionCaption($this->oper_tipo->CurrentValue);
		} else {
			$this->oper_tipo->ViewValue = NULL;
		}
		$this->oper_tipo->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// img_1
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->ViewValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->ViewValue = "";
		}
		$this->img_1->ViewCustomAttributes = "";

		// img_2
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->ViewValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->ViewValue = "";
		}
		$this->img_2->ViewCustomAttributes = "";

		// img_3
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->ViewValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->ViewValue = "";
		}
		$this->img_3->ViewCustomAttributes = "";

		// img_4
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->ViewValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->ViewValue = "";
		}
		$this->img_4->ViewCustomAttributes = "";

		// img_5
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->ViewValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->ViewValue = "";
		}
		$this->img_5->ViewCustomAttributes = "";

		// img_6
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->ViewValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->ViewValue = "";
		}
		$this->img_6->ViewCustomAttributes = "";

		// img_7
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->ViewValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->ViewValue = "";
		}
		$this->img_7->ViewCustomAttributes = "";

		// img_8
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->ViewValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->ViewValue = "";
		}
		$this->img_8->ViewCustomAttributes = "";

		// img_9
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->ViewValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->ViewValue = "";
		}
		$this->img_9->ViewCustomAttributes = "";

		// img_10
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->ViewValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->ViewValue = "";
		}
		$this->img_10->ViewCustomAttributes = "";

		// video_servidor
		if (strval($this->video_servidor->CurrentValue) <> "") {
			$this->video_servidor->ViewValue = $this->video_servidor->OptionCaption($this->video_servidor->CurrentValue);
		} else {
			$this->video_servidor->ViewValue = NULL;
		}
		$this->video_servidor->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// prov_id
			$this->prov_id->LinkCustomAttributes = "";
			$this->prov_id->HrefValue = "";
			$this->prov_id->TooltipValue = "";

			// loca_id
			$this->loca_id->LinkCustomAttributes = "";
			$this->loca_id->HrefValue = "";
			$this->loca_id->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// tipo_id
			$this->tipo_id->LinkCustomAttributes = "";
			$this->tipo_id->HrefValue = "";
			$this->tipo_id->TooltipValue = "";

			// actividad
			$this->actividad->LinkCustomAttributes = "";
			$this->actividad->HrefValue = "";
			$this->actividad->TooltipValue = "";

			// oper_tipo
			$this->oper_tipo->LinkCustomAttributes = "";
			$this->oper_tipo->HrefValue = "";
			$this->oper_tipo->TooltipValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";
			$this->descripcion->TooltipValue = "";

			// img_1
			$this->img_1->LinkCustomAttributes = "";
			$this->img_1->HrefValue = "";
			$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;
			$this->img_1->TooltipValue = "";

			// img_2
			$this->img_2->LinkCustomAttributes = "";
			$this->img_2->HrefValue = "";
			$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;
			$this->img_2->TooltipValue = "";

			// img_3
			$this->img_3->LinkCustomAttributes = "";
			$this->img_3->HrefValue = "";
			$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;
			$this->img_3->TooltipValue = "";

			// img_4
			$this->img_4->LinkCustomAttributes = "";
			$this->img_4->HrefValue = "";
			$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;
			$this->img_4->TooltipValue = "";

			// img_5
			$this->img_5->LinkCustomAttributes = "";
			$this->img_5->HrefValue = "";
			$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;
			$this->img_5->TooltipValue = "";

			// img_6
			$this->img_6->LinkCustomAttributes = "";
			$this->img_6->HrefValue = "";
			$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;
			$this->img_6->TooltipValue = "";

			// img_7
			$this->img_7->LinkCustomAttributes = "";
			$this->img_7->HrefValue = "";
			$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;
			$this->img_7->TooltipValue = "";

			// img_8
			$this->img_8->LinkCustomAttributes = "";
			$this->img_8->HrefValue = "";
			$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;
			$this->img_8->TooltipValue = "";

			// img_9
			$this->img_9->LinkCustomAttributes = "";
			$this->img_9->HrefValue = "";
			$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;
			$this->img_9->TooltipValue = "";

			// img_10
			$this->img_10->LinkCustomAttributes = "";
			$this->img_10->HrefValue = "";
			$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;
			$this->img_10->TooltipValue = "";

			// video_servidor
			$this->video_servidor->LinkCustomAttributes = "";
			$this->video_servidor->HrefValue = "";
			$this->video_servidor->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_propiedades\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_propiedades',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fpropiedadesview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("propiedadeslist.php"), "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($propiedades_view)) $propiedades_view = new cpropiedades_view();

// Page init
$propiedades_view->Page_Init();

// Page main
$propiedades_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$propiedades_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = fpropiedadesview = new ew_Form("fpropiedadesview", "view");

// Form_CustomValidate event
fpropiedadesview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fpropiedadesview.ValidateRequired = true;
<?php } else { ?>
fpropiedadesview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fpropiedadesview.Lists["x_prov_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_loca_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
fpropiedadesview.Lists["x_loca_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"localidades"};
fpropiedadesview.Lists["x_actividad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesview.Lists["x_actividad"].Options = <?php echo json_encode($propiedades->actividad->Options()) ?>;
fpropiedadesview.Lists["x_oper_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesview.Lists["x_oper_tipo"].Options = <?php echo json_encode($propiedades->oper_tipo->Options()) ?>;
fpropiedadesview.Lists["x_video_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadesview.Lists["x_video_servidor"].Options = <?php echo json_encode($propiedades->video_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($propiedades->Export == "") { ?>
<div class="ewToolbar">
<?php if (!$propiedades_view->IsModal) { ?>
<?php if ($propiedades->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php } ?>
<?php $propiedades_view->ExportOptions->Render("body") ?>
<?php
	foreach ($propiedades_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if (!$propiedades_view->IsModal) { ?>
<?php if ($propiedades->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $propiedades_view->ShowPageHeader(); ?>
<?php
$propiedades_view->ShowMessage();
?>
<?php if (!$propiedades_view->IsModal) { ?>
<?php if ($propiedades->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($propiedades_view->Pager)) $propiedades_view->Pager = new cPrevNextPager($propiedades_view->StartRec, $propiedades_view->DisplayRecs, $propiedades_view->TotalRecs) ?>
<?php if ($propiedades_view->Pager->RecordCount > 0 && $propiedades_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($propiedades_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($propiedades_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $propiedades_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($propiedades_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($propiedades_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $propiedades_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<?php } ?>
<form name="fpropiedadesview" id="fpropiedadesview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($propiedades_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $propiedades_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="propiedades">
<?php if ($propiedades_view->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($propiedades->id->Visible) { // id ?>
	<tr id="r_id">
		<td><span id="elh_propiedades_id"><?php echo $propiedades->id->FldCaption() ?></span></td>
		<td data-name="id"<?php echo $propiedades->id->CellAttributes() ?>>
<span id="el_propiedades_id">
<span<?php echo $propiedades->id->ViewAttributes() ?>>
<?php echo $propiedades->id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
	<tr id="r_prov_id">
		<td><span id="elh_propiedades_prov_id"><?php echo $propiedades->prov_id->FldCaption() ?></span></td>
		<td data-name="prov_id"<?php echo $propiedades->prov_id->CellAttributes() ?>>
<span id="el_propiedades_prov_id">
<span<?php echo $propiedades->prov_id->ViewAttributes() ?>>
<?php echo $propiedades->prov_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
	<tr id="r_loca_id">
		<td><span id="elh_propiedades_loca_id"><?php echo $propiedades->loca_id->FldCaption() ?></span></td>
		<td data-name="loca_id"<?php echo $propiedades->loca_id->CellAttributes() ?>>
<span id="el_propiedades_loca_id">
<span<?php echo $propiedades->loca_id->ViewAttributes() ?>>
<?php echo $propiedades->loca_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->titulo->Visible) { // titulo ?>
	<tr id="r_titulo">
		<td><span id="elh_propiedades_titulo"><?php echo $propiedades->titulo->FldCaption() ?></span></td>
		<td data-name="titulo"<?php echo $propiedades->titulo->CellAttributes() ?>>
<span id="el_propiedades_titulo">
<span<?php echo $propiedades->titulo->ViewAttributes() ?>>
<?php echo $propiedades->titulo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
	<tr id="r_tipo_id">
		<td><span id="elh_propiedades_tipo_id"><?php echo $propiedades->tipo_id->FldCaption() ?></span></td>
		<td data-name="tipo_id"<?php echo $propiedades->tipo_id->CellAttributes() ?>>
<span id="el_propiedades_tipo_id">
<span<?php echo $propiedades->tipo_id->ViewAttributes() ?>>
<?php echo $propiedades->tipo_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->actividad->Visible) { // actividad ?>
	<tr id="r_actividad">
		<td><span id="elh_propiedades_actividad"><?php echo $propiedades->actividad->FldCaption() ?></span></td>
		<td data-name="actividad"<?php echo $propiedades->actividad->CellAttributes() ?>>
<span id="el_propiedades_actividad">
<span<?php echo $propiedades->actividad->ViewAttributes() ?>>
<?php echo $propiedades->actividad->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
	<tr id="r_oper_tipo">
		<td><span id="elh_propiedades_oper_tipo"><?php echo $propiedades->oper_tipo->FldCaption() ?></span></td>
		<td data-name="oper_tipo"<?php echo $propiedades->oper_tipo->CellAttributes() ?>>
<span id="el_propiedades_oper_tipo">
<span<?php echo $propiedades->oper_tipo->ViewAttributes() ?>>
<?php echo $propiedades->oper_tipo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->descripcion->Visible) { // descripcion ?>
	<tr id="r_descripcion">
		<td><span id="elh_propiedades_descripcion"><?php echo $propiedades->descripcion->FldCaption() ?></span></td>
		<td data-name="descripcion"<?php echo $propiedades->descripcion->CellAttributes() ?>>
<span id="el_propiedades_descripcion">
<span<?php echo $propiedades->descripcion->ViewAttributes() ?>>
<?php echo $propiedades->descripcion->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_1->Visible) { // img_1 ?>
	<tr id="r_img_1">
		<td><span id="elh_propiedades_img_1"><?php echo $propiedades->img_1->FldCaption() ?></span></td>
		<td data-name="img_1"<?php echo $propiedades->img_1->CellAttributes() ?>>
<span id="el_propiedades_img_1">
<span<?php echo $propiedades->img_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_1, $propiedades->img_1->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_2->Visible) { // img_2 ?>
	<tr id="r_img_2">
		<td><span id="elh_propiedades_img_2"><?php echo $propiedades->img_2->FldCaption() ?></span></td>
		<td data-name="img_2"<?php echo $propiedades->img_2->CellAttributes() ?>>
<span id="el_propiedades_img_2">
<span<?php echo $propiedades->img_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_2, $propiedades->img_2->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_3->Visible) { // img_3 ?>
	<tr id="r_img_3">
		<td><span id="elh_propiedades_img_3"><?php echo $propiedades->img_3->FldCaption() ?></span></td>
		<td data-name="img_3"<?php echo $propiedades->img_3->CellAttributes() ?>>
<span id="el_propiedades_img_3">
<span<?php echo $propiedades->img_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_3, $propiedades->img_3->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_4->Visible) { // img_4 ?>
	<tr id="r_img_4">
		<td><span id="elh_propiedades_img_4"><?php echo $propiedades->img_4->FldCaption() ?></span></td>
		<td data-name="img_4"<?php echo $propiedades->img_4->CellAttributes() ?>>
<span id="el_propiedades_img_4">
<span<?php echo $propiedades->img_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_4, $propiedades->img_4->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_5->Visible) { // img_5 ?>
	<tr id="r_img_5">
		<td><span id="elh_propiedades_img_5"><?php echo $propiedades->img_5->FldCaption() ?></span></td>
		<td data-name="img_5"<?php echo $propiedades->img_5->CellAttributes() ?>>
<span id="el_propiedades_img_5">
<span<?php echo $propiedades->img_5->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_5, $propiedades->img_5->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_6->Visible) { // img_6 ?>
	<tr id="r_img_6">
		<td><span id="elh_propiedades_img_6"><?php echo $propiedades->img_6->FldCaption() ?></span></td>
		<td data-name="img_6"<?php echo $propiedades->img_6->CellAttributes() ?>>
<span id="el_propiedades_img_6">
<span<?php echo $propiedades->img_6->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_6, $propiedades->img_6->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_7->Visible) { // img_7 ?>
	<tr id="r_img_7">
		<td><span id="elh_propiedades_img_7"><?php echo $propiedades->img_7->FldCaption() ?></span></td>
		<td data-name="img_7"<?php echo $propiedades->img_7->CellAttributes() ?>>
<span id="el_propiedades_img_7">
<span<?php echo $propiedades->img_7->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_7, $propiedades->img_7->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_8->Visible) { // img_8 ?>
	<tr id="r_img_8">
		<td><span id="elh_propiedades_img_8"><?php echo $propiedades->img_8->FldCaption() ?></span></td>
		<td data-name="img_8"<?php echo $propiedades->img_8->CellAttributes() ?>>
<span id="el_propiedades_img_8">
<span<?php echo $propiedades->img_8->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_8, $propiedades->img_8->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_9->Visible) { // img_9 ?>
	<tr id="r_img_9">
		<td><span id="elh_propiedades_img_9"><?php echo $propiedades->img_9->FldCaption() ?></span></td>
		<td data-name="img_9"<?php echo $propiedades->img_9->CellAttributes() ?>>
<span id="el_propiedades_img_9">
<span<?php echo $propiedades->img_9->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_9, $propiedades->img_9->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->img_10->Visible) { // img_10 ?>
	<tr id="r_img_10">
		<td><span id="elh_propiedades_img_10"><?php echo $propiedades->img_10->FldCaption() ?></span></td>
		<td data-name="img_10"<?php echo $propiedades->img_10->CellAttributes() ?>>
<span id="el_propiedades_img_10">
<span<?php echo $propiedades->img_10->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_10, $propiedades->img_10->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
	<tr id="r_video_servidor">
		<td><span id="elh_propiedades_video_servidor"><?php echo $propiedades->video_servidor->FldCaption() ?></span></td>
		<td data-name="video_servidor"<?php echo $propiedades->video_servidor->CellAttributes() ?>>
<span id="el_propiedades_video_servidor">
<span<?php echo $propiedades->video_servidor->ViewAttributes() ?>>
<?php echo $propiedades->video_servidor->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($propiedades->video->Visible) { // video ?>
	<tr id="r_video">
		<td><span id="elh_propiedades_video"><?php echo $propiedades->video->FldCaption() ?></span></td>
		<td data-name="video"<?php echo $propiedades->video->CellAttributes() ?>>
<span id="el_propiedades_video">
<span<?php echo $propiedades->video->ViewAttributes() ?>>
<?php echo $propiedades->video->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if (!$propiedades_view->IsModal) { ?>
<?php if ($propiedades->Export == "") { ?>
<?php if (!isset($propiedades_view->Pager)) $propiedades_view->Pager = new cPrevNextPager($propiedades_view->StartRec, $propiedades_view->DisplayRecs, $propiedades_view->TotalRecs) ?>
<?php if ($propiedades_view->Pager->RecordCount > 0 && $propiedades_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($propiedades_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($propiedades_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $propiedades_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($propiedades_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($propiedades_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $propiedades_view->PageUrl() ?>start=<?php echo $propiedades_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $propiedades_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
<?php } ?>
</form>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">
fpropiedadesview.Init();
</script>
<?php } ?>
<?php
$propiedades_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$propiedades_view->Page_Terminate();
?>
