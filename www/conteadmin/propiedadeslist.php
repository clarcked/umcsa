<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "propiedadesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$propiedades_list = NULL; // Initialize page object first

class cpropiedades_list extends cpropiedades {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{1B8CF9BA-91E4-4950-9047-342B30417538}";

	// Table name
	var $TableName = 'propiedades';

	// Page object name
	var $PageObjName = 'propiedades_list';

	// Grid form hidden field names
	var $FormName = 'fpropiedadeslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (propiedades)
		if (!isset($GLOBALS["propiedades"]) || get_class($GLOBALS["propiedades"]) == "cpropiedades") {
			$GLOBALS["propiedades"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["propiedades"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "propiedadesadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "propiedadesdelete.php";
		$this->MultiUpdateUrl = "propiedadesupdate.php";

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'propiedades', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fpropiedadeslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->prov_id->SetVisibility();
		$this->loca_id->SetVisibility();
		$this->titulo->SetVisibility();
		$this->tipo_id->SetVisibility();
		$this->actividad->SetVisibility();
		$this->oper_tipo->SetVisibility();
		$this->img_1->SetVisibility();
		$this->img_2->SetVisibility();
		$this->img_3->SetVisibility();
		$this->img_4->SetVisibility();
		$this->img_5->SetVisibility();
		$this->img_6->SetVisibility();
		$this->img_7->SetVisibility();
		$this->img_8->SetVisibility();
		$this->img_9->SetVisibility();
		$this->img_10->SetVisibility();
		$this->video_servidor->SetVisibility();
		$this->video->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $propiedades;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($propiedades);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Process filter list
			$this->ProcessFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Get list of filters
	function GetFilterList() {
		global $UserProfile;

		// Load server side filters
		if (EW_SEARCH_FILTER_OPTION == "Server") {
			$sSavedFilterList = $UserProfile->GetSearchFilters(CurrentUserName(), "fpropiedadeslistsrch");
		} else {
			$sSavedFilterList = "";
		}

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->id->AdvancedSearch->ToJSON(), ","); // Field id
		$sFilterList = ew_Concat($sFilterList, $this->prov_id->AdvancedSearch->ToJSON(), ","); // Field prov_id
		$sFilterList = ew_Concat($sFilterList, $this->loca_id->AdvancedSearch->ToJSON(), ","); // Field loca_id
		$sFilterList = ew_Concat($sFilterList, $this->titulo->AdvancedSearch->ToJSON(), ","); // Field titulo
		$sFilterList = ew_Concat($sFilterList, $this->tipo_id->AdvancedSearch->ToJSON(), ","); // Field tipo_id
		$sFilterList = ew_Concat($sFilterList, $this->actividad->AdvancedSearch->ToJSON(), ","); // Field actividad
		$sFilterList = ew_Concat($sFilterList, $this->oper_tipo->AdvancedSearch->ToJSON(), ","); // Field oper_tipo
		$sFilterList = ew_Concat($sFilterList, $this->descripcion->AdvancedSearch->ToJSON(), ","); // Field descripcion
		$sFilterList = ew_Concat($sFilterList, $this->img_1->AdvancedSearch->ToJSON(), ","); // Field img_1
		$sFilterList = ew_Concat($sFilterList, $this->img_2->AdvancedSearch->ToJSON(), ","); // Field img_2
		$sFilterList = ew_Concat($sFilterList, $this->img_3->AdvancedSearch->ToJSON(), ","); // Field img_3
		$sFilterList = ew_Concat($sFilterList, $this->img_4->AdvancedSearch->ToJSON(), ","); // Field img_4
		$sFilterList = ew_Concat($sFilterList, $this->img_5->AdvancedSearch->ToJSON(), ","); // Field img_5
		$sFilterList = ew_Concat($sFilterList, $this->img_6->AdvancedSearch->ToJSON(), ","); // Field img_6
		$sFilterList = ew_Concat($sFilterList, $this->img_7->AdvancedSearch->ToJSON(), ","); // Field img_7
		$sFilterList = ew_Concat($sFilterList, $this->img_8->AdvancedSearch->ToJSON(), ","); // Field img_8
		$sFilterList = ew_Concat($sFilterList, $this->img_9->AdvancedSearch->ToJSON(), ","); // Field img_9
		$sFilterList = ew_Concat($sFilterList, $this->img_10->AdvancedSearch->ToJSON(), ","); // Field img_10
		$sFilterList = ew_Concat($sFilterList, $this->video_servidor->AdvancedSearch->ToJSON(), ","); // Field video_servidor
		$sFilterList = ew_Concat($sFilterList, $this->video->AdvancedSearch->ToJSON(), ","); // Field video
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}
		$sFilterList = preg_replace('/,$/', "", $sFilterList);

		// Return filter list in json
		if ($sFilterList <> "")
			$sFilterList = "\"data\":{" . $sFilterList . "}";
		if ($sSavedFilterList <> "") {
			if ($sFilterList <> "")
				$sFilterList .= ",";
			$sFilterList .= "\"filters\":" . $sSavedFilterList;
		}
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Process filter list
	function ProcessFilterList() {
		global $UserProfile;
		if (@$_POST["ajax"] == "savefilters") { // Save filter request (Ajax)
			$filters = ew_StripSlashes(@$_POST["filters"]);
			$UserProfile->SetSearchFilters(CurrentUserName(), "fpropiedadeslistsrch", $filters);

			// Clean output buffer
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			echo ew_ArrayToJson(array(array("success" => TRUE))); // Success
			$this->Page_Terminate();
			exit();
		} elseif (@$_POST["cmd"] == "resetfilter") {
			$this->RestoreFilterList();
		}
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field id
		$this->id->AdvancedSearch->SearchValue = @$filter["x_id"];
		$this->id->AdvancedSearch->SearchOperator = @$filter["z_id"];
		$this->id->AdvancedSearch->SearchCondition = @$filter["v_id"];
		$this->id->AdvancedSearch->SearchValue2 = @$filter["y_id"];
		$this->id->AdvancedSearch->SearchOperator2 = @$filter["w_id"];
		$this->id->AdvancedSearch->Save();

		// Field prov_id
		$this->prov_id->AdvancedSearch->SearchValue = @$filter["x_prov_id"];
		$this->prov_id->AdvancedSearch->SearchOperator = @$filter["z_prov_id"];
		$this->prov_id->AdvancedSearch->SearchCondition = @$filter["v_prov_id"];
		$this->prov_id->AdvancedSearch->SearchValue2 = @$filter["y_prov_id"];
		$this->prov_id->AdvancedSearch->SearchOperator2 = @$filter["w_prov_id"];
		$this->prov_id->AdvancedSearch->Save();

		// Field loca_id
		$this->loca_id->AdvancedSearch->SearchValue = @$filter["x_loca_id"];
		$this->loca_id->AdvancedSearch->SearchOperator = @$filter["z_loca_id"];
		$this->loca_id->AdvancedSearch->SearchCondition = @$filter["v_loca_id"];
		$this->loca_id->AdvancedSearch->SearchValue2 = @$filter["y_loca_id"];
		$this->loca_id->AdvancedSearch->SearchOperator2 = @$filter["w_loca_id"];
		$this->loca_id->AdvancedSearch->Save();

		// Field titulo
		$this->titulo->AdvancedSearch->SearchValue = @$filter["x_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator = @$filter["z_titulo"];
		$this->titulo->AdvancedSearch->SearchCondition = @$filter["v_titulo"];
		$this->titulo->AdvancedSearch->SearchValue2 = @$filter["y_titulo"];
		$this->titulo->AdvancedSearch->SearchOperator2 = @$filter["w_titulo"];
		$this->titulo->AdvancedSearch->Save();

		// Field tipo_id
		$this->tipo_id->AdvancedSearch->SearchValue = @$filter["x_tipo_id"];
		$this->tipo_id->AdvancedSearch->SearchOperator = @$filter["z_tipo_id"];
		$this->tipo_id->AdvancedSearch->SearchCondition = @$filter["v_tipo_id"];
		$this->tipo_id->AdvancedSearch->SearchValue2 = @$filter["y_tipo_id"];
		$this->tipo_id->AdvancedSearch->SearchOperator2 = @$filter["w_tipo_id"];
		$this->tipo_id->AdvancedSearch->Save();

		// Field actividad
		$this->actividad->AdvancedSearch->SearchValue = @$filter["x_actividad"];
		$this->actividad->AdvancedSearch->SearchOperator = @$filter["z_actividad"];
		$this->actividad->AdvancedSearch->SearchCondition = @$filter["v_actividad"];
		$this->actividad->AdvancedSearch->SearchValue2 = @$filter["y_actividad"];
		$this->actividad->AdvancedSearch->SearchOperator2 = @$filter["w_actividad"];
		$this->actividad->AdvancedSearch->Save();

		// Field oper_tipo
		$this->oper_tipo->AdvancedSearch->SearchValue = @$filter["x_oper_tipo"];
		$this->oper_tipo->AdvancedSearch->SearchOperator = @$filter["z_oper_tipo"];
		$this->oper_tipo->AdvancedSearch->SearchCondition = @$filter["v_oper_tipo"];
		$this->oper_tipo->AdvancedSearch->SearchValue2 = @$filter["y_oper_tipo"];
		$this->oper_tipo->AdvancedSearch->SearchOperator2 = @$filter["w_oper_tipo"];
		$this->oper_tipo->AdvancedSearch->Save();

		// Field descripcion
		$this->descripcion->AdvancedSearch->SearchValue = @$filter["x_descripcion"];
		$this->descripcion->AdvancedSearch->SearchOperator = @$filter["z_descripcion"];
		$this->descripcion->AdvancedSearch->SearchCondition = @$filter["v_descripcion"];
		$this->descripcion->AdvancedSearch->SearchValue2 = @$filter["y_descripcion"];
		$this->descripcion->AdvancedSearch->SearchOperator2 = @$filter["w_descripcion"];
		$this->descripcion->AdvancedSearch->Save();

		// Field img_1
		$this->img_1->AdvancedSearch->SearchValue = @$filter["x_img_1"];
		$this->img_1->AdvancedSearch->SearchOperator = @$filter["z_img_1"];
		$this->img_1->AdvancedSearch->SearchCondition = @$filter["v_img_1"];
		$this->img_1->AdvancedSearch->SearchValue2 = @$filter["y_img_1"];
		$this->img_1->AdvancedSearch->SearchOperator2 = @$filter["w_img_1"];
		$this->img_1->AdvancedSearch->Save();

		// Field img_2
		$this->img_2->AdvancedSearch->SearchValue = @$filter["x_img_2"];
		$this->img_2->AdvancedSearch->SearchOperator = @$filter["z_img_2"];
		$this->img_2->AdvancedSearch->SearchCondition = @$filter["v_img_2"];
		$this->img_2->AdvancedSearch->SearchValue2 = @$filter["y_img_2"];
		$this->img_2->AdvancedSearch->SearchOperator2 = @$filter["w_img_2"];
		$this->img_2->AdvancedSearch->Save();

		// Field img_3
		$this->img_3->AdvancedSearch->SearchValue = @$filter["x_img_3"];
		$this->img_3->AdvancedSearch->SearchOperator = @$filter["z_img_3"];
		$this->img_3->AdvancedSearch->SearchCondition = @$filter["v_img_3"];
		$this->img_3->AdvancedSearch->SearchValue2 = @$filter["y_img_3"];
		$this->img_3->AdvancedSearch->SearchOperator2 = @$filter["w_img_3"];
		$this->img_3->AdvancedSearch->Save();

		// Field img_4
		$this->img_4->AdvancedSearch->SearchValue = @$filter["x_img_4"];
		$this->img_4->AdvancedSearch->SearchOperator = @$filter["z_img_4"];
		$this->img_4->AdvancedSearch->SearchCondition = @$filter["v_img_4"];
		$this->img_4->AdvancedSearch->SearchValue2 = @$filter["y_img_4"];
		$this->img_4->AdvancedSearch->SearchOperator2 = @$filter["w_img_4"];
		$this->img_4->AdvancedSearch->Save();

		// Field img_5
		$this->img_5->AdvancedSearch->SearchValue = @$filter["x_img_5"];
		$this->img_5->AdvancedSearch->SearchOperator = @$filter["z_img_5"];
		$this->img_5->AdvancedSearch->SearchCondition = @$filter["v_img_5"];
		$this->img_5->AdvancedSearch->SearchValue2 = @$filter["y_img_5"];
		$this->img_5->AdvancedSearch->SearchOperator2 = @$filter["w_img_5"];
		$this->img_5->AdvancedSearch->Save();

		// Field img_6
		$this->img_6->AdvancedSearch->SearchValue = @$filter["x_img_6"];
		$this->img_6->AdvancedSearch->SearchOperator = @$filter["z_img_6"];
		$this->img_6->AdvancedSearch->SearchCondition = @$filter["v_img_6"];
		$this->img_6->AdvancedSearch->SearchValue2 = @$filter["y_img_6"];
		$this->img_6->AdvancedSearch->SearchOperator2 = @$filter["w_img_6"];
		$this->img_6->AdvancedSearch->Save();

		// Field img_7
		$this->img_7->AdvancedSearch->SearchValue = @$filter["x_img_7"];
		$this->img_7->AdvancedSearch->SearchOperator = @$filter["z_img_7"];
		$this->img_7->AdvancedSearch->SearchCondition = @$filter["v_img_7"];
		$this->img_7->AdvancedSearch->SearchValue2 = @$filter["y_img_7"];
		$this->img_7->AdvancedSearch->SearchOperator2 = @$filter["w_img_7"];
		$this->img_7->AdvancedSearch->Save();

		// Field img_8
		$this->img_8->AdvancedSearch->SearchValue = @$filter["x_img_8"];
		$this->img_8->AdvancedSearch->SearchOperator = @$filter["z_img_8"];
		$this->img_8->AdvancedSearch->SearchCondition = @$filter["v_img_8"];
		$this->img_8->AdvancedSearch->SearchValue2 = @$filter["y_img_8"];
		$this->img_8->AdvancedSearch->SearchOperator2 = @$filter["w_img_8"];
		$this->img_8->AdvancedSearch->Save();

		// Field img_9
		$this->img_9->AdvancedSearch->SearchValue = @$filter["x_img_9"];
		$this->img_9->AdvancedSearch->SearchOperator = @$filter["z_img_9"];
		$this->img_9->AdvancedSearch->SearchCondition = @$filter["v_img_9"];
		$this->img_9->AdvancedSearch->SearchValue2 = @$filter["y_img_9"];
		$this->img_9->AdvancedSearch->SearchOperator2 = @$filter["w_img_9"];
		$this->img_9->AdvancedSearch->Save();

		// Field img_10
		$this->img_10->AdvancedSearch->SearchValue = @$filter["x_img_10"];
		$this->img_10->AdvancedSearch->SearchOperator = @$filter["z_img_10"];
		$this->img_10->AdvancedSearch->SearchCondition = @$filter["v_img_10"];
		$this->img_10->AdvancedSearch->SearchValue2 = @$filter["y_img_10"];
		$this->img_10->AdvancedSearch->SearchOperator2 = @$filter["w_img_10"];
		$this->img_10->AdvancedSearch->Save();

		// Field video_servidor
		$this->video_servidor->AdvancedSearch->SearchValue = @$filter["x_video_servidor"];
		$this->video_servidor->AdvancedSearch->SearchOperator = @$filter["z_video_servidor"];
		$this->video_servidor->AdvancedSearch->SearchCondition = @$filter["v_video_servidor"];
		$this->video_servidor->AdvancedSearch->SearchValue2 = @$filter["y_video_servidor"];
		$this->video_servidor->AdvancedSearch->SearchOperator2 = @$filter["w_video_servidor"];
		$this->video_servidor->AdvancedSearch->Save();

		// Field video
		$this->video->AdvancedSearch->SearchValue = @$filter["x_video"];
		$this->video->AdvancedSearch->SearchOperator = @$filter["z_video"];
		$this->video->AdvancedSearch->SearchCondition = @$filter["v_video"];
		$this->video->AdvancedSearch->SearchValue2 = @$filter["y_video"];
		$this->video->AdvancedSearch->SearchOperator2 = @$filter["w_video"];
		$this->video->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->id, $Default, FALSE); // id
		$this->BuildSearchSql($sWhere, $this->prov_id, $Default, FALSE); // prov_id
		$this->BuildSearchSql($sWhere, $this->loca_id, $Default, FALSE); // loca_id
		$this->BuildSearchSql($sWhere, $this->titulo, $Default, FALSE); // titulo
		$this->BuildSearchSql($sWhere, $this->tipo_id, $Default, FALSE); // tipo_id
		$this->BuildSearchSql($sWhere, $this->actividad, $Default, FALSE); // actividad
		$this->BuildSearchSql($sWhere, $this->oper_tipo, $Default, FALSE); // oper_tipo
		$this->BuildSearchSql($sWhere, $this->descripcion, $Default, FALSE); // descripcion
		$this->BuildSearchSql($sWhere, $this->img_1, $Default, FALSE); // img_1
		$this->BuildSearchSql($sWhere, $this->img_2, $Default, FALSE); // img_2
		$this->BuildSearchSql($sWhere, $this->img_3, $Default, FALSE); // img_3
		$this->BuildSearchSql($sWhere, $this->img_4, $Default, FALSE); // img_4
		$this->BuildSearchSql($sWhere, $this->img_5, $Default, FALSE); // img_5
		$this->BuildSearchSql($sWhere, $this->img_6, $Default, FALSE); // img_6
		$this->BuildSearchSql($sWhere, $this->img_7, $Default, FALSE); // img_7
		$this->BuildSearchSql($sWhere, $this->img_8, $Default, FALSE); // img_8
		$this->BuildSearchSql($sWhere, $this->img_9, $Default, FALSE); // img_9
		$this->BuildSearchSql($sWhere, $this->img_10, $Default, FALSE); // img_10
		$this->BuildSearchSql($sWhere, $this->video_servidor, $Default, FALSE); // video_servidor
		$this->BuildSearchSql($sWhere, $this->video, $Default, FALSE); // video

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->id->AdvancedSearch->Save(); // id
			$this->prov_id->AdvancedSearch->Save(); // prov_id
			$this->loca_id->AdvancedSearch->Save(); // loca_id
			$this->titulo->AdvancedSearch->Save(); // titulo
			$this->tipo_id->AdvancedSearch->Save(); // tipo_id
			$this->actividad->AdvancedSearch->Save(); // actividad
			$this->oper_tipo->AdvancedSearch->Save(); // oper_tipo
			$this->descripcion->AdvancedSearch->Save(); // descripcion
			$this->img_1->AdvancedSearch->Save(); // img_1
			$this->img_2->AdvancedSearch->Save(); // img_2
			$this->img_3->AdvancedSearch->Save(); // img_3
			$this->img_4->AdvancedSearch->Save(); // img_4
			$this->img_5->AdvancedSearch->Save(); // img_5
			$this->img_6->AdvancedSearch->Save(); // img_6
			$this->img_7->AdvancedSearch->Save(); // img_7
			$this->img_8->AdvancedSearch->Save(); // img_8
			$this->img_9->AdvancedSearch->Save(); // img_9
			$this->img_10->AdvancedSearch->Save(); // img_10
			$this->video_servidor->AdvancedSearch->Save(); // video_servidor
			$this->video->AdvancedSearch->Save(); // video
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1)
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE || $Fld->FldDataType == EW_DATATYPE_TIME) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->titulo, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->descripcion, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_1, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_2, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_3, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_4, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_5, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_6, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_7, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_8, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_9, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->img_10, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->video, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSQL(&$Where, &$Fld, $arKeywords, $type) {
		global $EW_BASIC_SEARCH_IGNORE_PATTERN;
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if ($EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace($EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->prov_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->loca_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->titulo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->tipo_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->actividad->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->oper_tipo->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->descripcion->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_5->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_6->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_7->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_8->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_9->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->img_10->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video_servidor->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->video->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->id->AdvancedSearch->UnsetSession();
		$this->prov_id->AdvancedSearch->UnsetSession();
		$this->loca_id->AdvancedSearch->UnsetSession();
		$this->titulo->AdvancedSearch->UnsetSession();
		$this->tipo_id->AdvancedSearch->UnsetSession();
		$this->actividad->AdvancedSearch->UnsetSession();
		$this->oper_tipo->AdvancedSearch->UnsetSession();
		$this->descripcion->AdvancedSearch->UnsetSession();
		$this->img_1->AdvancedSearch->UnsetSession();
		$this->img_2->AdvancedSearch->UnsetSession();
		$this->img_3->AdvancedSearch->UnsetSession();
		$this->img_4->AdvancedSearch->UnsetSession();
		$this->img_5->AdvancedSearch->UnsetSession();
		$this->img_6->AdvancedSearch->UnsetSession();
		$this->img_7->AdvancedSearch->UnsetSession();
		$this->img_8->AdvancedSearch->UnsetSession();
		$this->img_9->AdvancedSearch->UnsetSession();
		$this->img_10->AdvancedSearch->UnsetSession();
		$this->video_servidor->AdvancedSearch->UnsetSession();
		$this->video->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->id->AdvancedSearch->Load();
		$this->prov_id->AdvancedSearch->Load();
		$this->loca_id->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->tipo_id->AdvancedSearch->Load();
		$this->actividad->AdvancedSearch->Load();
		$this->oper_tipo->AdvancedSearch->Load();
		$this->descripcion->AdvancedSearch->Load();
		$this->img_1->AdvancedSearch->Load();
		$this->img_2->AdvancedSearch->Load();
		$this->img_3->AdvancedSearch->Load();
		$this->img_4->AdvancedSearch->Load();
		$this->img_5->AdvancedSearch->Load();
		$this->img_6->AdvancedSearch->Load();
		$this->img_7->AdvancedSearch->Load();
		$this->img_8->AdvancedSearch->Load();
		$this->img_9->AdvancedSearch->Load();
		$this->img_10->AdvancedSearch->Load();
		$this->video_servidor->AdvancedSearch->Load();
		$this->video->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->prov_id); // prov_id
			$this->UpdateSort($this->loca_id); // loca_id
			$this->UpdateSort($this->titulo); // titulo
			$this->UpdateSort($this->tipo_id); // tipo_id
			$this->UpdateSort($this->actividad); // actividad
			$this->UpdateSort($this->oper_tipo); // oper_tipo
			$this->UpdateSort($this->img_1); // img_1
			$this->UpdateSort($this->img_2); // img_2
			$this->UpdateSort($this->img_3); // img_3
			$this->UpdateSort($this->img_4); // img_4
			$this->UpdateSort($this->img_5); // img_5
			$this->UpdateSort($this->img_6); // img_6
			$this->UpdateSort($this->img_7); // img_7
			$this->UpdateSort($this->img_8); // img_8
			$this->UpdateSort($this->img_9); // img_9
			$this->UpdateSort($this->img_10); // img_10
			$this->UpdateSort($this->video_servidor); // video_servidor
			$this->UpdateSort($this->video); // video
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->setSessionOrderByList($sOrderBy);
				$this->id->setSort("");
				$this->prov_id->setSort("");
				$this->loca_id->setSort("");
				$this->titulo->setSort("");
				$this->tipo_id->setSort("");
				$this->actividad->setSort("");
				$this->oper_tipo->setSort("");
				$this->img_1->setSort("");
				$this->img_2->setSort("");
				$this->img_3->setSort("");
				$this->img_4->setSort("");
				$this->img_5->setSort("");
				$this->img_6->setSort("");
				$this->img_7->setSort("");
				$this->img_8->setSort("");
				$this->img_9->setSort("");
				$this->img_10->setSort("");
				$this->video_servidor->setSort("");
				$this->video->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		$viewcaption = ew_HtmlTitle($Language->Phrase("ViewLink"));
		if ($Security->CanView()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . $viewcaption . "\" data-caption=\"" . $viewcaption . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		$editcaption = ew_HtmlTitle($Language->Phrase("EditLink"));
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		$copycaption = ew_HtmlTitle($Language->Phrase("CopyLink"));
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("AddLink"));
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$option = $options["action"];

		// Add multi delete
		$item = &$option->Add("multidelete");
		$item->Body = "<a class=\"ewAction ewMultiDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fpropiedadeslist,url:'" . $this->MultiDeleteUrl . "'});return false;\">" . $Language->Phrase("DeleteSelectedLink") . "</a>";
		$item->Visible = ($Security->CanDelete());

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fpropiedadeslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fpropiedadeslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fpropiedadeslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fpropiedadeslistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;

		// Hide detail items for dropdown if necessary
		$this->ListOptions->HideDetailItemsForDropDown();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_id"]);
		if ($this->id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->id->AdvancedSearch->SearchOperator = @$_GET["z_id"];

		// prov_id
		$this->prov_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_prov_id"]);
		if ($this->prov_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->prov_id->AdvancedSearch->SearchOperator = @$_GET["z_prov_id"];

		// loca_id
		$this->loca_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_loca_id"]);
		if ($this->loca_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->loca_id->AdvancedSearch->SearchOperator = @$_GET["z_loca_id"];

		// titulo
		$this->titulo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_titulo"]);
		if ($this->titulo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->titulo->AdvancedSearch->SearchOperator = @$_GET["z_titulo"];

		// tipo_id
		$this->tipo_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_tipo_id"]);
		if ($this->tipo_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->tipo_id->AdvancedSearch->SearchOperator = @$_GET["z_tipo_id"];

		// actividad
		$this->actividad->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_actividad"]);
		if ($this->actividad->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->actividad->AdvancedSearch->SearchOperator = @$_GET["z_actividad"];

		// oper_tipo
		$this->oper_tipo->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_oper_tipo"]);
		if ($this->oper_tipo->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->oper_tipo->AdvancedSearch->SearchOperator = @$_GET["z_oper_tipo"];

		// descripcion
		$this->descripcion->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_descripcion"]);
		if ($this->descripcion->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->descripcion->AdvancedSearch->SearchOperator = @$_GET["z_descripcion"];

		// img_1
		$this->img_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_1"]);
		if ($this->img_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_1->AdvancedSearch->SearchOperator = @$_GET["z_img_1"];

		// img_2
		$this->img_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_2"]);
		if ($this->img_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_2->AdvancedSearch->SearchOperator = @$_GET["z_img_2"];

		// img_3
		$this->img_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_3"]);
		if ($this->img_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_3->AdvancedSearch->SearchOperator = @$_GET["z_img_3"];

		// img_4
		$this->img_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_4"]);
		if ($this->img_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_4->AdvancedSearch->SearchOperator = @$_GET["z_img_4"];

		// img_5
		$this->img_5->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_5"]);
		if ($this->img_5->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_5->AdvancedSearch->SearchOperator = @$_GET["z_img_5"];

		// img_6
		$this->img_6->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_6"]);
		if ($this->img_6->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_6->AdvancedSearch->SearchOperator = @$_GET["z_img_6"];

		// img_7
		$this->img_7->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_7"]);
		if ($this->img_7->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_7->AdvancedSearch->SearchOperator = @$_GET["z_img_7"];

		// img_8
		$this->img_8->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_8"]);
		if ($this->img_8->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_8->AdvancedSearch->SearchOperator = @$_GET["z_img_8"];

		// img_9
		$this->img_9->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_9"]);
		if ($this->img_9->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_9->AdvancedSearch->SearchOperator = @$_GET["z_img_9"];

		// img_10
		$this->img_10->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_img_10"]);
		if ($this->img_10->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->img_10->AdvancedSearch->SearchOperator = @$_GET["z_img_10"];

		// video_servidor
		$this->video_servidor->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video_servidor"]);
		if ($this->video_servidor->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video_servidor->AdvancedSearch->SearchOperator = @$_GET["z_video_servidor"];

		// video
		$this->video->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_video"]);
		if ($this->video->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->video->AdvancedSearch->SearchOperator = @$_GET["z_video"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->prov_id->setDbValue($rs->fields('prov_id'));
		if (array_key_exists('EV__prov_id', $rs->fields)) {
			$this->prov_id->VirtualValue = $rs->fields('EV__prov_id'); // Set up virtual field value
		} else {
			$this->prov_id->VirtualValue = ""; // Clear value
		}
		$this->loca_id->setDbValue($rs->fields('loca_id'));
		if (array_key_exists('EV__loca_id', $rs->fields)) {
			$this->loca_id->VirtualValue = $rs->fields('EV__loca_id'); // Set up virtual field value
		} else {
			$this->loca_id->VirtualValue = ""; // Clear value
		}
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->tipo_id->setDbValue($rs->fields('tipo_id'));
		$this->actividad->setDbValue($rs->fields('actividad'));
		$this->oper_tipo->setDbValue($rs->fields('oper_tipo'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->img_1->Upload->DbValue = $rs->fields('img_1');
		$this->img_1->CurrentValue = $this->img_1->Upload->DbValue;
		$this->img_2->Upload->DbValue = $rs->fields('img_2');
		$this->img_2->CurrentValue = $this->img_2->Upload->DbValue;
		$this->img_3->Upload->DbValue = $rs->fields('img_3');
		$this->img_3->CurrentValue = $this->img_3->Upload->DbValue;
		$this->img_4->Upload->DbValue = $rs->fields('img_4');
		$this->img_4->CurrentValue = $this->img_4->Upload->DbValue;
		$this->img_5->Upload->DbValue = $rs->fields('img_5');
		$this->img_5->CurrentValue = $this->img_5->Upload->DbValue;
		$this->img_6->Upload->DbValue = $rs->fields('img_6');
		$this->img_6->CurrentValue = $this->img_6->Upload->DbValue;
		$this->img_7->Upload->DbValue = $rs->fields('img_7');
		$this->img_7->CurrentValue = $this->img_7->Upload->DbValue;
		$this->img_8->Upload->DbValue = $rs->fields('img_8');
		$this->img_8->CurrentValue = $this->img_8->Upload->DbValue;
		$this->img_9->Upload->DbValue = $rs->fields('img_9');
		$this->img_9->CurrentValue = $this->img_9->Upload->DbValue;
		$this->img_10->Upload->DbValue = $rs->fields('img_10');
		$this->img_10->CurrentValue = $this->img_10->Upload->DbValue;
		$this->video_servidor->setDbValue($rs->fields('video_servidor'));
		$this->video->setDbValue($rs->fields('video'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->prov_id->DbValue = $row['prov_id'];
		$this->loca_id->DbValue = $row['loca_id'];
		$this->titulo->DbValue = $row['titulo'];
		$this->tipo_id->DbValue = $row['tipo_id'];
		$this->actividad->DbValue = $row['actividad'];
		$this->oper_tipo->DbValue = $row['oper_tipo'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->img_1->Upload->DbValue = $row['img_1'];
		$this->img_2->Upload->DbValue = $row['img_2'];
		$this->img_3->Upload->DbValue = $row['img_3'];
		$this->img_4->Upload->DbValue = $row['img_4'];
		$this->img_5->Upload->DbValue = $row['img_5'];
		$this->img_6->Upload->DbValue = $row['img_6'];
		$this->img_7->Upload->DbValue = $row['img_7'];
		$this->img_8->Upload->DbValue = $row['img_8'];
		$this->img_9->Upload->DbValue = $row['img_9'];
		$this->img_10->Upload->DbValue = $row['img_10'];
		$this->video_servidor->DbValue = $row['video_servidor'];
		$this->video->DbValue = $row['video'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// prov_id
		// loca_id
		// titulo
		// tipo_id
		// actividad
		// oper_tipo
		// descripcion
		// img_1
		// img_2
		// img_3
		// img_4
		// img_5
		// img_6
		// img_7
		// img_8
		// img_9
		// img_10
		// video_servidor
		// video

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// prov_id
		if ($this->prov_id->VirtualValue <> "") {
			$this->prov_id->ViewValue = $this->prov_id->VirtualValue;
		} else {
		if (strval($this->prov_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->prov_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->prov_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prov_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prov_id->ViewValue = $this->prov_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prov_id->ViewValue = $this->prov_id->CurrentValue;
			}
		} else {
			$this->prov_id->ViewValue = NULL;
		}
		}
		$this->prov_id->ViewCustomAttributes = "";

		// loca_id
		if ($this->loca_id->VirtualValue <> "") {
			$this->loca_id->ViewValue = $this->loca_id->VirtualValue;
		} else {
		if (strval($this->loca_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->loca_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->loca_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->loca_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->loca_id->ViewValue = $this->loca_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->loca_id->ViewValue = $this->loca_id->CurrentValue;
			}
		} else {
			$this->loca_id->ViewValue = NULL;
		}
		}
		$this->loca_id->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// tipo_id
		$this->tipo_id->ViewValue = $this->tipo_id->CurrentValue;
		$this->tipo_id->ViewCustomAttributes = "";

		// actividad
		if (strval($this->actividad->CurrentValue) <> "") {
			$this->actividad->ViewValue = $this->actividad->OptionCaption($this->actividad->CurrentValue);
		} else {
			$this->actividad->ViewValue = NULL;
		}
		$this->actividad->ViewCustomAttributes = "";

		// oper_tipo
		if (strval($this->oper_tipo->CurrentValue) <> "") {
			$this->oper_tipo->ViewValue = $this->oper_tipo->OptionCaption($this->oper_tipo->CurrentValue);
		} else {
			$this->oper_tipo->ViewValue = NULL;
		}
		$this->oper_tipo->ViewCustomAttributes = "";

		// img_1
		if (!ew_Empty($this->img_1->Upload->DbValue)) {
			$this->img_1->ViewValue = $this->img_1->Upload->DbValue;
		} else {
			$this->img_1->ViewValue = "";
		}
		$this->img_1->ViewCustomAttributes = "";

		// img_2
		if (!ew_Empty($this->img_2->Upload->DbValue)) {
			$this->img_2->ViewValue = $this->img_2->Upload->DbValue;
		} else {
			$this->img_2->ViewValue = "";
		}
		$this->img_2->ViewCustomAttributes = "";

		// img_3
		if (!ew_Empty($this->img_3->Upload->DbValue)) {
			$this->img_3->ViewValue = $this->img_3->Upload->DbValue;
		} else {
			$this->img_3->ViewValue = "";
		}
		$this->img_3->ViewCustomAttributes = "";

		// img_4
		if (!ew_Empty($this->img_4->Upload->DbValue)) {
			$this->img_4->ViewValue = $this->img_4->Upload->DbValue;
		} else {
			$this->img_4->ViewValue = "";
		}
		$this->img_4->ViewCustomAttributes = "";

		// img_5
		if (!ew_Empty($this->img_5->Upload->DbValue)) {
			$this->img_5->ViewValue = $this->img_5->Upload->DbValue;
		} else {
			$this->img_5->ViewValue = "";
		}
		$this->img_5->ViewCustomAttributes = "";

		// img_6
		if (!ew_Empty($this->img_6->Upload->DbValue)) {
			$this->img_6->ViewValue = $this->img_6->Upload->DbValue;
		} else {
			$this->img_6->ViewValue = "";
		}
		$this->img_6->ViewCustomAttributes = "";

		// img_7
		if (!ew_Empty($this->img_7->Upload->DbValue)) {
			$this->img_7->ViewValue = $this->img_7->Upload->DbValue;
		} else {
			$this->img_7->ViewValue = "";
		}
		$this->img_7->ViewCustomAttributes = "";

		// img_8
		if (!ew_Empty($this->img_8->Upload->DbValue)) {
			$this->img_8->ViewValue = $this->img_8->Upload->DbValue;
		} else {
			$this->img_8->ViewValue = "";
		}
		$this->img_8->ViewCustomAttributes = "";

		// img_9
		if (!ew_Empty($this->img_9->Upload->DbValue)) {
			$this->img_9->ViewValue = $this->img_9->Upload->DbValue;
		} else {
			$this->img_9->ViewValue = "";
		}
		$this->img_9->ViewCustomAttributes = "";

		// img_10
		if (!ew_Empty($this->img_10->Upload->DbValue)) {
			$this->img_10->ViewValue = $this->img_10->Upload->DbValue;
		} else {
			$this->img_10->ViewValue = "";
		}
		$this->img_10->ViewCustomAttributes = "";

		// video_servidor
		if (strval($this->video_servidor->CurrentValue) <> "") {
			$this->video_servidor->ViewValue = $this->video_servidor->OptionCaption($this->video_servidor->CurrentValue);
		} else {
			$this->video_servidor->ViewValue = NULL;
		}
		$this->video_servidor->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// prov_id
			$this->prov_id->LinkCustomAttributes = "";
			$this->prov_id->HrefValue = "";
			$this->prov_id->TooltipValue = "";

			// loca_id
			$this->loca_id->LinkCustomAttributes = "";
			$this->loca_id->HrefValue = "";
			$this->loca_id->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// tipo_id
			$this->tipo_id->LinkCustomAttributes = "";
			$this->tipo_id->HrefValue = "";
			$this->tipo_id->TooltipValue = "";

			// actividad
			$this->actividad->LinkCustomAttributes = "";
			$this->actividad->HrefValue = "";
			$this->actividad->TooltipValue = "";

			// oper_tipo
			$this->oper_tipo->LinkCustomAttributes = "";
			$this->oper_tipo->HrefValue = "";
			$this->oper_tipo->TooltipValue = "";

			// img_1
			$this->img_1->LinkCustomAttributes = "";
			$this->img_1->HrefValue = "";
			$this->img_1->HrefValue2 = $this->img_1->UploadPath . $this->img_1->Upload->DbValue;
			$this->img_1->TooltipValue = "";

			// img_2
			$this->img_2->LinkCustomAttributes = "";
			$this->img_2->HrefValue = "";
			$this->img_2->HrefValue2 = $this->img_2->UploadPath . $this->img_2->Upload->DbValue;
			$this->img_2->TooltipValue = "";

			// img_3
			$this->img_3->LinkCustomAttributes = "";
			$this->img_3->HrefValue = "";
			$this->img_3->HrefValue2 = $this->img_3->UploadPath . $this->img_3->Upload->DbValue;
			$this->img_3->TooltipValue = "";

			// img_4
			$this->img_4->LinkCustomAttributes = "";
			$this->img_4->HrefValue = "";
			$this->img_4->HrefValue2 = $this->img_4->UploadPath . $this->img_4->Upload->DbValue;
			$this->img_4->TooltipValue = "";

			// img_5
			$this->img_5->LinkCustomAttributes = "";
			$this->img_5->HrefValue = "";
			$this->img_5->HrefValue2 = $this->img_5->UploadPath . $this->img_5->Upload->DbValue;
			$this->img_5->TooltipValue = "";

			// img_6
			$this->img_6->LinkCustomAttributes = "";
			$this->img_6->HrefValue = "";
			$this->img_6->HrefValue2 = $this->img_6->UploadPath . $this->img_6->Upload->DbValue;
			$this->img_6->TooltipValue = "";

			// img_7
			$this->img_7->LinkCustomAttributes = "";
			$this->img_7->HrefValue = "";
			$this->img_7->HrefValue2 = $this->img_7->UploadPath . $this->img_7->Upload->DbValue;
			$this->img_7->TooltipValue = "";

			// img_8
			$this->img_8->LinkCustomAttributes = "";
			$this->img_8->HrefValue = "";
			$this->img_8->HrefValue2 = $this->img_8->UploadPath . $this->img_8->Upload->DbValue;
			$this->img_8->TooltipValue = "";

			// img_9
			$this->img_9->LinkCustomAttributes = "";
			$this->img_9->HrefValue = "";
			$this->img_9->HrefValue2 = $this->img_9->UploadPath . $this->img_9->Upload->DbValue;
			$this->img_9->TooltipValue = "";

			// img_10
			$this->img_10->LinkCustomAttributes = "";
			$this->img_10->HrefValue = "";
			$this->img_10->HrefValue2 = $this->img_10->UploadPath . $this->img_10->Upload->DbValue;
			$this->img_10->TooltipValue = "";

			// video_servidor
			$this->video_servidor->LinkCustomAttributes = "";
			$this->video_servidor->HrefValue = "";
			$this->video_servidor->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// prov_id
			$this->prov_id->EditCustomAttributes = "";

			// loca_id
			$this->loca_id->EditAttrs["class"] = "form-control";
			$this->loca_id->EditCustomAttributes = "";
			$this->loca_id->EditValue = ew_HtmlEncode($this->loca_id->AdvancedSearch->SearchValue);
			$this->loca_id->PlaceHolder = ew_RemoveHtml($this->loca_id->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->AdvancedSearch->SearchValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// tipo_id
			$this->tipo_id->EditAttrs["class"] = "form-control";
			$this->tipo_id->EditCustomAttributes = "";
			$this->tipo_id->EditValue = ew_HtmlEncode($this->tipo_id->AdvancedSearch->SearchValue);
			$this->tipo_id->PlaceHolder = ew_RemoveHtml($this->tipo_id->FldCaption());

			// actividad
			$this->actividad->EditCustomAttributes = "";
			$this->actividad->EditValue = $this->actividad->Options(FALSE);

			// oper_tipo
			$this->oper_tipo->EditCustomAttributes = "";
			$this->oper_tipo->EditValue = $this->oper_tipo->Options(FALSE);

			// img_1
			$this->img_1->EditAttrs["class"] = "form-control";
			$this->img_1->EditCustomAttributes = "";
			$this->img_1->EditValue = ew_HtmlEncode($this->img_1->AdvancedSearch->SearchValue);
			$this->img_1->PlaceHolder = ew_RemoveHtml($this->img_1->FldCaption());

			// img_2
			$this->img_2->EditAttrs["class"] = "form-control";
			$this->img_2->EditCustomAttributes = "";
			$this->img_2->EditValue = ew_HtmlEncode($this->img_2->AdvancedSearch->SearchValue);
			$this->img_2->PlaceHolder = ew_RemoveHtml($this->img_2->FldCaption());

			// img_3
			$this->img_3->EditAttrs["class"] = "form-control";
			$this->img_3->EditCustomAttributes = "";
			$this->img_3->EditValue = ew_HtmlEncode($this->img_3->AdvancedSearch->SearchValue);
			$this->img_3->PlaceHolder = ew_RemoveHtml($this->img_3->FldCaption());

			// img_4
			$this->img_4->EditAttrs["class"] = "form-control";
			$this->img_4->EditCustomAttributes = "";
			$this->img_4->EditValue = ew_HtmlEncode($this->img_4->AdvancedSearch->SearchValue);
			$this->img_4->PlaceHolder = ew_RemoveHtml($this->img_4->FldCaption());

			// img_5
			$this->img_5->EditAttrs["class"] = "form-control";
			$this->img_5->EditCustomAttributes = "";
			$this->img_5->EditValue = ew_HtmlEncode($this->img_5->AdvancedSearch->SearchValue);
			$this->img_5->PlaceHolder = ew_RemoveHtml($this->img_5->FldCaption());

			// img_6
			$this->img_6->EditAttrs["class"] = "form-control";
			$this->img_6->EditCustomAttributes = "";
			$this->img_6->EditValue = ew_HtmlEncode($this->img_6->AdvancedSearch->SearchValue);
			$this->img_6->PlaceHolder = ew_RemoveHtml($this->img_6->FldCaption());

			// img_7
			$this->img_7->EditAttrs["class"] = "form-control";
			$this->img_7->EditCustomAttributes = "";
			$this->img_7->EditValue = ew_HtmlEncode($this->img_7->AdvancedSearch->SearchValue);
			$this->img_7->PlaceHolder = ew_RemoveHtml($this->img_7->FldCaption());

			// img_8
			$this->img_8->EditAttrs["class"] = "form-control";
			$this->img_8->EditCustomAttributes = "";
			$this->img_8->EditValue = ew_HtmlEncode($this->img_8->AdvancedSearch->SearchValue);
			$this->img_8->PlaceHolder = ew_RemoveHtml($this->img_8->FldCaption());

			// img_9
			$this->img_9->EditAttrs["class"] = "form-control";
			$this->img_9->EditCustomAttributes = "";
			$this->img_9->EditValue = ew_HtmlEncode($this->img_9->AdvancedSearch->SearchValue);
			$this->img_9->PlaceHolder = ew_RemoveHtml($this->img_9->FldCaption());

			// img_10
			$this->img_10->EditAttrs["class"] = "form-control";
			$this->img_10->EditCustomAttributes = "";
			$this->img_10->EditValue = ew_HtmlEncode($this->img_10->AdvancedSearch->SearchValue);
			$this->img_10->PlaceHolder = ew_RemoveHtml($this->img_10->FldCaption());

			// video_servidor
			$this->video_servidor->EditCustomAttributes = "";
			$this->video_servidor->EditValue = $this->video_servidor->Options(FALSE);

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->AdvancedSearch->SearchValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->prov_id->AdvancedSearch->Load();
		$this->loca_id->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->tipo_id->AdvancedSearch->Load();
		$this->actividad->AdvancedSearch->Load();
		$this->oper_tipo->AdvancedSearch->Load();
		$this->descripcion->AdvancedSearch->Load();
		$this->img_1->AdvancedSearch->Load();
		$this->img_2->AdvancedSearch->Load();
		$this->img_3->AdvancedSearch->Load();
		$this->img_4->AdvancedSearch->Load();
		$this->img_5->AdvancedSearch->Load();
		$this->img_6->AdvancedSearch->Load();
		$this->img_7->AdvancedSearch->Load();
		$this->img_8->AdvancedSearch->Load();
		$this->img_9->AdvancedSearch->Load();
		$this->img_10->AdvancedSearch->Load();
		$this->video_servidor->AdvancedSearch->Load();
		$this->video->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_propiedades\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_propiedades',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fpropiedadeslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($propiedades_list)) $propiedades_list = new cpropiedades_list();

// Page init
$propiedades_list->Page_Init();

// Page main
$propiedades_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$propiedades_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fpropiedadeslist = new ew_Form("fpropiedadeslist", "list");
fpropiedadeslist.FormKeyCountName = '<?php echo $propiedades_list->FormKeyCountName ?>';

// Form_CustomValidate event
fpropiedadeslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fpropiedadeslist.ValidateRequired = true;
<?php } else { ?>
fpropiedadeslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fpropiedadeslist.Lists["x_prov_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_loca_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
fpropiedadeslist.Lists["x_loca_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"localidades"};
fpropiedadeslist.Lists["x_actividad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslist.Lists["x_actividad"].Options = <?php echo json_encode($propiedades->actividad->Options()) ?>;
fpropiedadeslist.Lists["x_oper_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslist.Lists["x_oper_tipo"].Options = <?php echo json_encode($propiedades->oper_tipo->Options()) ?>;
fpropiedadeslist.Lists["x_video_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslist.Lists["x_video_servidor"].Options = <?php echo json_encode($propiedades->video_servidor->Options()) ?>;

// Form object for search
var CurrentSearchForm = fpropiedadeslistsrch = new ew_Form("fpropiedadeslistsrch");

// Validate function for search
fpropiedadeslistsrch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
fpropiedadeslistsrch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fpropiedadeslistsrch.ValidateRequired = true; // Use JavaScript validation
<?php } else { ?>
fpropiedadeslistsrch.ValidateRequired = false; // No JavaScript validation
<?php } ?>

// Dynamic selection lists
fpropiedadeslistsrch.Lists["x_actividad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslistsrch.Lists["x_actividad"].Options = <?php echo json_encode($propiedades->actividad->Options()) ?>;
fpropiedadeslistsrch.Lists["x_oper_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslistsrch.Lists["x_oper_tipo"].Options = <?php echo json_encode($propiedades->oper_tipo->Options()) ?>;
fpropiedadeslistsrch.Lists["x_video_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fpropiedadeslistsrch.Lists["x_video_servidor"].Options = <?php echo json_encode($propiedades->video_servidor->Options()) ?>;
</script>
<style type="text/css">
.ewTablePreviewRow { /* main table preview row color */
	background-color: #FFFFFF; /* preview row color */
}
.ewTablePreviewRow .ewGrid {
	display: table;
}
.ewTablePreviewRow .ewGrid .ewTable {
	width: auto;
}
</style>
<div id="ewPreview" class="hide"><ul class="nav nav-tabs"></ul><div class="tab-content"><div class="tab-pane fade"></div></div></div>
<script type="text/javascript" src="phpjs/ewpreview.min.js"></script>
<script type="text/javascript">
var EW_PREVIEW_PLACEMENT = EW_CSS_FLIP ? "left" : "right";
var EW_PREVIEW_SINGLE_ROW = false;
var EW_PREVIEW_OVERLAY = false;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($propiedades->Export == "") { ?>
<div class="ewToolbar">
<?php if ($propiedades->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($propiedades_list->TotalRecs > 0 && $propiedades_list->ExportOptions->Visible()) { ?>
<?php $propiedades_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($propiedades_list->SearchOptions->Visible()) { ?>
<?php $propiedades_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($propiedades_list->FilterOptions->Visible()) { ?>
<?php $propiedades_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($propiedades->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
	$bSelectLimit = $propiedades_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($propiedades_list->TotalRecs <= 0)
			$propiedades_list->TotalRecs = $propiedades->SelectRecordCount();
	} else {
		if (!$propiedades_list->Recordset && ($propiedades_list->Recordset = $propiedades_list->LoadRecordset()))
			$propiedades_list->TotalRecs = $propiedades_list->Recordset->RecordCount();
	}
	$propiedades_list->StartRec = 1;
	if ($propiedades_list->DisplayRecs <= 0 || ($propiedades->Export <> "" && $propiedades->ExportAll)) // Display all records
		$propiedades_list->DisplayRecs = $propiedades_list->TotalRecs;
	if (!($propiedades->Export <> "" && $propiedades->ExportAll))
		$propiedades_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$propiedades_list->Recordset = $propiedades_list->LoadRecordset($propiedades_list->StartRec-1, $propiedades_list->DisplayRecs);

	// Set no record found message
	if ($propiedades->CurrentAction == "" && $propiedades_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$propiedades_list->setWarningMessage(ew_DeniedMsg());
		if ($propiedades_list->SearchWhere == "0=101")
			$propiedades_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$propiedades_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
$propiedades_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($propiedades->Export == "" && $propiedades->CurrentAction == "") { ?>
<form name="fpropiedadeslistsrch" id="fpropiedadeslistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($propiedades_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fpropiedadeslistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="propiedades">
	<div class="ewBasicSearch">
<?php
if ($gsSearchError == "")
	$propiedades_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$propiedades->RowType = EW_ROWTYPE_SEARCH;

// Render row
$propiedades->ResetAttrs();
$propiedades_list->RenderRow();
?>
<div id="xsr_1" class="ewRow">
<?php if ($propiedades->actividad->Visible) { // actividad ?>
	<div id="xsc_actividad" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $propiedades->actividad->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_actividad" id="z_actividad" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_actividad" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_actividad" data-value-separator="<?php echo $propiedades->actividad->DisplayValueSeparatorAttribute() ?>" name="x_actividad" id="x_actividad" value="{value}"<?php echo $propiedades->actividad->EditAttributes() ?>></div>
<div id="dsl_x_actividad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->actividad->RadioButtonListHtml(FALSE, "x_actividad") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ewRow">
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
	<div id="xsc_oper_tipo" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $propiedades->oper_tipo->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_oper_tipo" id="z_oper_tipo" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_oper_tipo" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_oper_tipo" data-value-separator="<?php echo $propiedades->oper_tipo->DisplayValueSeparatorAttribute() ?>" name="x_oper_tipo" id="x_oper_tipo" value="{value}"<?php echo $propiedades->oper_tipo->EditAttributes() ?>></div>
<div id="dsl_x_oper_tipo" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->oper_tipo->RadioButtonListHtml(FALSE, "x_oper_tipo") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_3" class="ewRow">
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
	<div id="xsc_video_servidor" class="ewCell form-group">
		<label class="ewSearchCaption ewLabel"><?php echo $propiedades->video_servidor->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_video_servidor" id="z_video_servidor" value="="></span>
		<span class="ewSearchField">
<div id="tp_x_video_servidor" class="ewTemplate"><input type="radio" data-table="propiedades" data-field="x_video_servidor" data-value-separator="<?php echo $propiedades->video_servidor->DisplayValueSeparatorAttribute() ?>" name="x_video_servidor" id="x_video_servidor" value="{value}"<?php echo $propiedades->video_servidor->EditAttributes() ?>></div>
<div id="dsl_x_video_servidor" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $propiedades->video_servidor->RadioButtonListHtml(FALSE, "x_video_servidor") ?>
</div></div>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_4" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($propiedades_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($propiedades_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $propiedades_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($propiedades_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($propiedades_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($propiedades_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($propiedades_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $propiedades_list->ShowPageHeader(); ?>
<?php
$propiedades_list->ShowMessage();
?>
<?php if ($propiedades_list->TotalRecs > 0 || $propiedades->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid propiedades">
<?php if ($propiedades->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($propiedades->CurrentAction <> "gridadd" && $propiedades->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($propiedades_list->Pager)) $propiedades_list->Pager = new cPrevNextPager($propiedades_list->StartRec, $propiedades_list->DisplayRecs, $propiedades_list->TotalRecs) ?>
<?php if ($propiedades_list->Pager->RecordCount > 0 && $propiedades_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($propiedades_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($propiedades_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $propiedades_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($propiedades_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($propiedades_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $propiedades_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $propiedades_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $propiedades_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $propiedades_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($propiedades_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fpropiedadeslist" id="fpropiedadeslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($propiedades_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $propiedades_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="propiedades">
<div id="gmp_propiedades" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($propiedades_list->TotalRecs > 0 || $propiedades->CurrentAction == "gridedit") { ?>
<table id="tbl_propiedadeslist" class="table ewTable">
<?php echo $propiedades->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$propiedades_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$propiedades_list->RenderListOptions();

// Render list options (header, left)
$propiedades_list->ListOptions->Render("header", "left");
?>
<?php if ($propiedades->id->Visible) { // id ?>
	<?php if ($propiedades->SortUrl($propiedades->id) == "") { ?>
		<th data-name="id"><div id="elh_propiedades_id" class="propiedades_id"><div class="ewTableHeaderCaption"><?php echo $propiedades->id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->id) ?>',1);"><div id="elh_propiedades_id" class="propiedades_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
	<?php if ($propiedades->SortUrl($propiedades->prov_id) == "") { ?>
		<th data-name="prov_id"><div id="elh_propiedades_prov_id" class="propiedades_prov_id"><div class="ewTableHeaderCaption"><?php echo $propiedades->prov_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prov_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->prov_id) ?>',1);"><div id="elh_propiedades_prov_id" class="propiedades_prov_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->prov_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->prov_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->prov_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
	<?php if ($propiedades->SortUrl($propiedades->loca_id) == "") { ?>
		<th data-name="loca_id"><div id="elh_propiedades_loca_id" class="propiedades_loca_id"><div class="ewTableHeaderCaption"><?php echo $propiedades->loca_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="loca_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->loca_id) ?>',1);"><div id="elh_propiedades_loca_id" class="propiedades_loca_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->loca_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->loca_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->loca_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->titulo->Visible) { // titulo ?>
	<?php if ($propiedades->SortUrl($propiedades->titulo) == "") { ?>
		<th data-name="titulo"><div id="elh_propiedades_titulo" class="propiedades_titulo"><div class="ewTableHeaderCaption"><?php echo $propiedades->titulo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="titulo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->titulo) ?>',1);"><div id="elh_propiedades_titulo" class="propiedades_titulo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->titulo->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->titulo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->titulo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
	<?php if ($propiedades->SortUrl($propiedades->tipo_id) == "") { ?>
		<th data-name="tipo_id"><div id="elh_propiedades_tipo_id" class="propiedades_tipo_id"><div class="ewTableHeaderCaption"><?php echo $propiedades->tipo_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="tipo_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->tipo_id) ?>',1);"><div id="elh_propiedades_tipo_id" class="propiedades_tipo_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->tipo_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->tipo_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->tipo_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->actividad->Visible) { // actividad ?>
	<?php if ($propiedades->SortUrl($propiedades->actividad) == "") { ?>
		<th data-name="actividad"><div id="elh_propiedades_actividad" class="propiedades_actividad"><div class="ewTableHeaderCaption"><?php echo $propiedades->actividad->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="actividad"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->actividad) ?>',1);"><div id="elh_propiedades_actividad" class="propiedades_actividad">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->actividad->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->actividad->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->actividad->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
	<?php if ($propiedades->SortUrl($propiedades->oper_tipo) == "") { ?>
		<th data-name="oper_tipo"><div id="elh_propiedades_oper_tipo" class="propiedades_oper_tipo"><div class="ewTableHeaderCaption"><?php echo $propiedades->oper_tipo->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="oper_tipo"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->oper_tipo) ?>',1);"><div id="elh_propiedades_oper_tipo" class="propiedades_oper_tipo">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->oper_tipo->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->oper_tipo->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->oper_tipo->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_1->Visible) { // img_1 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_1) == "") { ?>
		<th data-name="img_1"><div id="elh_propiedades_img_1" class="propiedades_img_1"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_1) ?>',1);"><div id="elh_propiedades_img_1" class="propiedades_img_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_1->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_2->Visible) { // img_2 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_2) == "") { ?>
		<th data-name="img_2"><div id="elh_propiedades_img_2" class="propiedades_img_2"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_2) ?>',1);"><div id="elh_propiedades_img_2" class="propiedades_img_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_2->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_3->Visible) { // img_3 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_3) == "") { ?>
		<th data-name="img_3"><div id="elh_propiedades_img_3" class="propiedades_img_3"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_3) ?>',1);"><div id="elh_propiedades_img_3" class="propiedades_img_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_3->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_4->Visible) { // img_4 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_4) == "") { ?>
		<th data-name="img_4"><div id="elh_propiedades_img_4" class="propiedades_img_4"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_4) ?>',1);"><div id="elh_propiedades_img_4" class="propiedades_img_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_4->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_5->Visible) { // img_5 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_5) == "") { ?>
		<th data-name="img_5"><div id="elh_propiedades_img_5" class="propiedades_img_5"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_5->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_5"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_5) ?>',1);"><div id="elh_propiedades_img_5" class="propiedades_img_5">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_5->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_5->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_5->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_6->Visible) { // img_6 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_6) == "") { ?>
		<th data-name="img_6"><div id="elh_propiedades_img_6" class="propiedades_img_6"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_6->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_6"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_6) ?>',1);"><div id="elh_propiedades_img_6" class="propiedades_img_6">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_6->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_6->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_6->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_7->Visible) { // img_7 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_7) == "") { ?>
		<th data-name="img_7"><div id="elh_propiedades_img_7" class="propiedades_img_7"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_7->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_7"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_7) ?>',1);"><div id="elh_propiedades_img_7" class="propiedades_img_7">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_7->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_7->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_7->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_8->Visible) { // img_8 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_8) == "") { ?>
		<th data-name="img_8"><div id="elh_propiedades_img_8" class="propiedades_img_8"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_8->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_8"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_8) ?>',1);"><div id="elh_propiedades_img_8" class="propiedades_img_8">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_8->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_8->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_8->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_9->Visible) { // img_9 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_9) == "") { ?>
		<th data-name="img_9"><div id="elh_propiedades_img_9" class="propiedades_img_9"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_9->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_9"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_9) ?>',1);"><div id="elh_propiedades_img_9" class="propiedades_img_9">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_9->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_9->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_9->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->img_10->Visible) { // img_10 ?>
	<?php if ($propiedades->SortUrl($propiedades->img_10) == "") { ?>
		<th data-name="img_10"><div id="elh_propiedades_img_10" class="propiedades_img_10"><div class="ewTableHeaderCaption"><?php echo $propiedades->img_10->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="img_10"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->img_10) ?>',1);"><div id="elh_propiedades_img_10" class="propiedades_img_10">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->img_10->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->img_10->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->img_10->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
	<?php if ($propiedades->SortUrl($propiedades->video_servidor) == "") { ?>
		<th data-name="video_servidor"><div id="elh_propiedades_video_servidor" class="propiedades_video_servidor"><div class="ewTableHeaderCaption"><?php echo $propiedades->video_servidor->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video_servidor"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->video_servidor) ?>',1);"><div id="elh_propiedades_video_servidor" class="propiedades_video_servidor">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->video_servidor->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->video_servidor->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->video_servidor->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($propiedades->video->Visible) { // video ?>
	<?php if ($propiedades->SortUrl($propiedades->video) == "") { ?>
		<th data-name="video"><div id="elh_propiedades_video" class="propiedades_video"><div class="ewTableHeaderCaption"><?php echo $propiedades->video->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="video"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $propiedades->SortUrl($propiedades->video) ?>',1);"><div id="elh_propiedades_video" class="propiedades_video">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $propiedades->video->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($propiedades->video->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($propiedades->video->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$propiedades_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($propiedades->ExportAll && $propiedades->Export <> "") {
	$propiedades_list->StopRec = $propiedades_list->TotalRecs;
} else {

	// Set the last record to display
	if ($propiedades_list->TotalRecs > $propiedades_list->StartRec + $propiedades_list->DisplayRecs - 1)
		$propiedades_list->StopRec = $propiedades_list->StartRec + $propiedades_list->DisplayRecs - 1;
	else
		$propiedades_list->StopRec = $propiedades_list->TotalRecs;
}
$propiedades_list->RecCnt = $propiedades_list->StartRec - 1;
if ($propiedades_list->Recordset && !$propiedades_list->Recordset->EOF) {
	$propiedades_list->Recordset->MoveFirst();
	$bSelectLimit = $propiedades_list->UseSelectLimit;
	if (!$bSelectLimit && $propiedades_list->StartRec > 1)
		$propiedades_list->Recordset->Move($propiedades_list->StartRec - 1);
} elseif (!$propiedades->AllowAddDeleteRow && $propiedades_list->StopRec == 0) {
	$propiedades_list->StopRec = $propiedades->GridAddRowCount;
}

// Initialize aggregate
$propiedades->RowType = EW_ROWTYPE_AGGREGATEINIT;
$propiedades->ResetAttrs();
$propiedades_list->RenderRow();
while ($propiedades_list->RecCnt < $propiedades_list->StopRec) {
	$propiedades_list->RecCnt++;
	if (intval($propiedades_list->RecCnt) >= intval($propiedades_list->StartRec)) {
		$propiedades_list->RowCnt++;

		// Set up key count
		$propiedades_list->KeyCount = $propiedades_list->RowIndex;

		// Init row class and style
		$propiedades->ResetAttrs();
		$propiedades->CssClass = "";
		if ($propiedades->CurrentAction == "gridadd") {
		} else {
			$propiedades_list->LoadRowValues($propiedades_list->Recordset); // Load row values
		}
		$propiedades->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$propiedades->RowAttrs = array_merge($propiedades->RowAttrs, array('data-rowindex'=>$propiedades_list->RowCnt, 'id'=>'r' . $propiedades_list->RowCnt . '_propiedades', 'data-rowtype'=>$propiedades->RowType));

		// Render row
		$propiedades_list->RenderRow();

		// Render list options
		$propiedades_list->RenderListOptions();
?>
	<tr<?php echo $propiedades->RowAttributes() ?>>
<?php

// Render list options (body, left)
$propiedades_list->ListOptions->Render("body", "left", $propiedades_list->RowCnt);
?>
	<?php if ($propiedades->id->Visible) { // id ?>
		<td data-name="id"<?php echo $propiedades->id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_id" class="propiedades_id">
<span<?php echo $propiedades->id->ViewAttributes() ?>>
<?php echo $propiedades->id->ListViewValue() ?></span>
</span>
<a id="<?php echo $propiedades_list->PageObjName . "_row_" . $propiedades_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($propiedades->prov_id->Visible) { // prov_id ?>
		<td data-name="prov_id"<?php echo $propiedades->prov_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_prov_id" class="propiedades_prov_id">
<span<?php echo $propiedades->prov_id->ViewAttributes() ?>>
<?php echo $propiedades->prov_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->loca_id->Visible) { // loca_id ?>
		<td data-name="loca_id"<?php echo $propiedades->loca_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_loca_id" class="propiedades_loca_id">
<span<?php echo $propiedades->loca_id->ViewAttributes() ?>>
<?php echo $propiedades->loca_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->titulo->Visible) { // titulo ?>
		<td data-name="titulo"<?php echo $propiedades->titulo->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_titulo" class="propiedades_titulo">
<span<?php echo $propiedades->titulo->ViewAttributes() ?>>
<?php echo $propiedades->titulo->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->tipo_id->Visible) { // tipo_id ?>
		<td data-name="tipo_id"<?php echo $propiedades->tipo_id->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_tipo_id" class="propiedades_tipo_id">
<span<?php echo $propiedades->tipo_id->ViewAttributes() ?>>
<?php echo $propiedades->tipo_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->actividad->Visible) { // actividad ?>
		<td data-name="actividad"<?php echo $propiedades->actividad->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_actividad" class="propiedades_actividad">
<span<?php echo $propiedades->actividad->ViewAttributes() ?>>
<?php echo $propiedades->actividad->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->oper_tipo->Visible) { // oper_tipo ?>
		<td data-name="oper_tipo"<?php echo $propiedades->oper_tipo->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_oper_tipo" class="propiedades_oper_tipo">
<span<?php echo $propiedades->oper_tipo->ViewAttributes() ?>>
<?php echo $propiedades->oper_tipo->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_1->Visible) { // img_1 ?>
		<td data-name="img_1"<?php echo $propiedades->img_1->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_1" class="propiedades_img_1">
<span<?php echo $propiedades->img_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_1, $propiedades->img_1->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_2->Visible) { // img_2 ?>
		<td data-name="img_2"<?php echo $propiedades->img_2->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_2" class="propiedades_img_2">
<span<?php echo $propiedades->img_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_2, $propiedades->img_2->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_3->Visible) { // img_3 ?>
		<td data-name="img_3"<?php echo $propiedades->img_3->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_3" class="propiedades_img_3">
<span<?php echo $propiedades->img_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_3, $propiedades->img_3->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_4->Visible) { // img_4 ?>
		<td data-name="img_4"<?php echo $propiedades->img_4->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_4" class="propiedades_img_4">
<span<?php echo $propiedades->img_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_4, $propiedades->img_4->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_5->Visible) { // img_5 ?>
		<td data-name="img_5"<?php echo $propiedades->img_5->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_5" class="propiedades_img_5">
<span<?php echo $propiedades->img_5->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_5, $propiedades->img_5->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_6->Visible) { // img_6 ?>
		<td data-name="img_6"<?php echo $propiedades->img_6->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_6" class="propiedades_img_6">
<span<?php echo $propiedades->img_6->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_6, $propiedades->img_6->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_7->Visible) { // img_7 ?>
		<td data-name="img_7"<?php echo $propiedades->img_7->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_7" class="propiedades_img_7">
<span<?php echo $propiedades->img_7->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_7, $propiedades->img_7->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_8->Visible) { // img_8 ?>
		<td data-name="img_8"<?php echo $propiedades->img_8->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_8" class="propiedades_img_8">
<span<?php echo $propiedades->img_8->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_8, $propiedades->img_8->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_9->Visible) { // img_9 ?>
		<td data-name="img_9"<?php echo $propiedades->img_9->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_9" class="propiedades_img_9">
<span<?php echo $propiedades->img_9->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_9, $propiedades->img_9->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->img_10->Visible) { // img_10 ?>
		<td data-name="img_10"<?php echo $propiedades->img_10->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_img_10" class="propiedades_img_10">
<span<?php echo $propiedades->img_10->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($propiedades->img_10, $propiedades->img_10->ListViewValue()) ?>
</span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->video_servidor->Visible) { // video_servidor ?>
		<td data-name="video_servidor"<?php echo $propiedades->video_servidor->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_video_servidor" class="propiedades_video_servidor">
<span<?php echo $propiedades->video_servidor->ViewAttributes() ?>>
<?php echo $propiedades->video_servidor->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($propiedades->video->Visible) { // video ?>
		<td data-name="video"<?php echo $propiedades->video->CellAttributes() ?>>
<span id="el<?php echo $propiedades_list->RowCnt ?>_propiedades_video" class="propiedades_video">
<span<?php echo $propiedades->video->ViewAttributes() ?>>
<?php echo $propiedades->video->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$propiedades_list->ListOptions->Render("body", "right", $propiedades_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($propiedades->CurrentAction <> "gridadd")
		$propiedades_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($propiedades->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($propiedades_list->Recordset)
	$propiedades_list->Recordset->Close();
?>
<?php if ($propiedades->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($propiedades->CurrentAction <> "gridadd" && $propiedades->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($propiedades_list->Pager)) $propiedades_list->Pager = new cPrevNextPager($propiedades_list->StartRec, $propiedades_list->DisplayRecs, $propiedades_list->TotalRecs) ?>
<?php if ($propiedades_list->Pager->RecordCount > 0 && $propiedades_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($propiedades_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($propiedades_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $propiedades_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($propiedades_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($propiedades_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $propiedades_list->PageUrl() ?>start=<?php echo $propiedades_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $propiedades_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $propiedades_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $propiedades_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $propiedades_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($propiedades_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($propiedades_list->TotalRecs == 0 && $propiedades->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($propiedades_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">
fpropiedadeslistsrch.FilterList = <?php echo $propiedades_list->GetFilterList() ?>;
fpropiedadeslistsrch.Init();
fpropiedadeslist.Init();
</script>
<?php } ?>
<?php
$propiedades_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($propiedades->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$propiedades_list->Page_Terminate();
?>
