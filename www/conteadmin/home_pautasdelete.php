<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "home_pautasinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$home_pautas_delete = NULL; // Initialize page object first

class chome_pautas_delete extends chome_pautas {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'home_pautas';

	// Page object name
	var $PageObjName = 'home_pautas_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (home_pautas)
		if (!isset($GLOBALS["home_pautas"]) || get_class($GLOBALS["home_pautas"]) == "chome_pautas") {
			$GLOBALS["home_pautas"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["home_pautas"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'home_pautas', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("home_pautaslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->activa->SetVisibility();
		$this->fecha_desde->SetVisibility();
		$this->fecha_hasta->SetVisibility();
		$this->remate_1->SetVisibility();
		$this->remate_2->SetVisibility();
		$this->remate_3->SetVisibility();
		$this->remate_4->SetVisibility();
		$this->vivolink->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $home_pautas;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($home_pautas);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("home_pautaslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in home_pautas class, home_pautasinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} elseif (@$_GET["a_delete"] == "1") {
			$this->CurrentAction = "D"; // Delete record directly
		} else {
			$this->CurrentAction = "D"; // Delete record directly
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("home_pautaslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->fecha_desde->setDbValue($rs->fields('fecha_desde'));
		$this->fecha_hasta->setDbValue($rs->fields('fecha_hasta'));
		$this->remate_1->setDbValue($rs->fields('remate_1'));
		$this->remate_2->setDbValue($rs->fields('remate_2'));
		$this->remate_3->setDbValue($rs->fields('remate_3'));
		$this->remate_4->setDbValue($rs->fields('remate_4'));
		$this->vivolink->setDbValue($rs->fields('vivolink'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->activa->DbValue = $row['activa'];
		$this->fecha_desde->DbValue = $row['fecha_desde'];
		$this->fecha_hasta->DbValue = $row['fecha_hasta'];
		$this->remate_1->DbValue = $row['remate_1'];
		$this->remate_2->DbValue = $row['remate_2'];
		$this->remate_3->DbValue = $row['remate_3'];
		$this->remate_4->DbValue = $row['remate_4'];
		$this->vivolink->DbValue = $row['vivolink'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// activa
		// fecha_desde
		// fecha_hasta
		// remate_1
		// remate_2
		// remate_3
		// remate_4
		// vivolink

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// fecha_desde
		$this->fecha_desde->ViewValue = $this->fecha_desde->CurrentValue;
		$this->fecha_desde->ViewValue = ew_FormatDateTime($this->fecha_desde->ViewValue, 0);
		$this->fecha_desde->ViewCustomAttributes = "";

		// fecha_hasta
		$this->fecha_hasta->ViewValue = $this->fecha_hasta->CurrentValue;
		$this->fecha_hasta->ViewValue = ew_FormatDateTime($this->fecha_hasta->ViewValue, 0);
		$this->fecha_hasta->ViewCustomAttributes = "";

		// remate_1
		if (strval($this->remate_1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_1->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_1->ViewValue = $this->remate_1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_1->ViewValue = $this->remate_1->CurrentValue;
			}
		} else {
			$this->remate_1->ViewValue = NULL;
		}
		$this->remate_1->ViewCustomAttributes = "";

		// remate_2
		if (strval($this->remate_2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_2->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_2->ViewValue = $this->remate_2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_2->ViewValue = $this->remate_2->CurrentValue;
			}
		} else {
			$this->remate_2->ViewValue = NULL;
		}
		$this->remate_2->ViewCustomAttributes = "";

		// remate_3
		if (strval($this->remate_3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_3->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_3->ViewValue = $this->remate_3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_3->ViewValue = $this->remate_3->CurrentValue;
			}
		} else {
			$this->remate_3->ViewValue = NULL;
		}
		$this->remate_3->ViewCustomAttributes = "";

		// remate_4
		if (strval($this->remate_4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_4->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_4->ViewValue = $this->remate_4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_4->ViewValue = $this->remate_4->CurrentValue;
			}
		} else {
			$this->remate_4->ViewValue = NULL;
		}
		$this->remate_4->ViewCustomAttributes = "";

		// vivolink
		$this->vivolink->ViewValue = $this->vivolink->CurrentValue;
		$this->vivolink->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";
			$this->fecha_desde->TooltipValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";
			$this->fecha_hasta->TooltipValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";
			$this->remate_1->TooltipValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";
			$this->remate_2->TooltipValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";
			$this->remate_3->TooltipValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";
			$this->remate_4->TooltipValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
			$this->vivolink->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("home_pautaslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($home_pautas_delete)) $home_pautas_delete = new chome_pautas_delete();

// Page init
$home_pautas_delete->Page_Init();

// Page main
$home_pautas_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$home_pautas_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fhome_pautasdelete = new ew_Form("fhome_pautasdelete", "delete");

// Form_CustomValidate event
fhome_pautasdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fhome_pautasdelete.ValidateRequired = true;
<?php } else { ?>
fhome_pautasdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fhome_pautasdelete.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fhome_pautasdelete.Lists["x_activa"].Options = <?php echo json_encode($home_pautas->activa->Options()) ?>;
fhome_pautasdelete.Lists["x_remate_1"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasdelete.Lists["x_remate_2"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasdelete.Lists["x_remate_3"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautasdelete.Lists["x_remate_4"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $home_pautas_delete->ShowPageHeader(); ?>
<?php
$home_pautas_delete->ShowMessage();
?>
<form name="fhome_pautasdelete" id="fhome_pautasdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($home_pautas_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $home_pautas_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="home_pautas">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($home_pautas_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $home_pautas->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($home_pautas->id->Visible) { // id ?>
		<th><span id="elh_home_pautas_id" class="home_pautas_id"><?php echo $home_pautas->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->activa->Visible) { // activa ?>
		<th><span id="elh_home_pautas_activa" class="home_pautas_activa"><?php echo $home_pautas->activa->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
		<th><span id="elh_home_pautas_fecha_desde" class="home_pautas_fecha_desde"><?php echo $home_pautas->fecha_desde->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
		<th><span id="elh_home_pautas_fecha_hasta" class="home_pautas_fecha_hasta"><?php echo $home_pautas->fecha_hasta->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
		<th><span id="elh_home_pautas_remate_1" class="home_pautas_remate_1"><?php echo $home_pautas->remate_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
		<th><span id="elh_home_pautas_remate_2" class="home_pautas_remate_2"><?php echo $home_pautas->remate_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
		<th><span id="elh_home_pautas_remate_3" class="home_pautas_remate_3"><?php echo $home_pautas->remate_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
		<th><span id="elh_home_pautas_remate_4" class="home_pautas_remate_4"><?php echo $home_pautas->remate_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
		<th><span id="elh_home_pautas_vivolink" class="home_pautas_vivolink"><?php echo $home_pautas->vivolink->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$home_pautas_delete->RecCnt = 0;
$i = 0;
while (!$home_pautas_delete->Recordset->EOF) {
	$home_pautas_delete->RecCnt++;
	$home_pautas_delete->RowCnt++;

	// Set row properties
	$home_pautas->ResetAttrs();
	$home_pautas->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$home_pautas_delete->LoadRowValues($home_pautas_delete->Recordset);

	// Render row
	$home_pautas_delete->RenderRow();
?>
	<tr<?php echo $home_pautas->RowAttributes() ?>>
<?php if ($home_pautas->id->Visible) { // id ?>
		<td<?php echo $home_pautas->id->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_id" class="home_pautas_id">
<span<?php echo $home_pautas->id->ViewAttributes() ?>>
<?php echo $home_pautas->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->activa->Visible) { // activa ?>
		<td<?php echo $home_pautas->activa->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_activa" class="home_pautas_activa">
<span<?php echo $home_pautas->activa->ViewAttributes() ?>>
<?php echo $home_pautas->activa->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
		<td<?php echo $home_pautas->fecha_desde->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_fecha_desde" class="home_pautas_fecha_desde">
<span<?php echo $home_pautas->fecha_desde->ViewAttributes() ?>>
<?php echo $home_pautas->fecha_desde->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
		<td<?php echo $home_pautas->fecha_hasta->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_fecha_hasta" class="home_pautas_fecha_hasta">
<span<?php echo $home_pautas->fecha_hasta->ViewAttributes() ?>>
<?php echo $home_pautas->fecha_hasta->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
		<td<?php echo $home_pautas->remate_1->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_remate_1" class="home_pautas_remate_1">
<span<?php echo $home_pautas->remate_1->ViewAttributes() ?>>
<?php echo $home_pautas->remate_1->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
		<td<?php echo $home_pautas->remate_2->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_remate_2" class="home_pautas_remate_2">
<span<?php echo $home_pautas->remate_2->ViewAttributes() ?>>
<?php echo $home_pautas->remate_2->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
		<td<?php echo $home_pautas->remate_3->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_remate_3" class="home_pautas_remate_3">
<span<?php echo $home_pautas->remate_3->ViewAttributes() ?>>
<?php echo $home_pautas->remate_3->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
		<td<?php echo $home_pautas->remate_4->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_remate_4" class="home_pautas_remate_4">
<span<?php echo $home_pautas->remate_4->ViewAttributes() ?>>
<?php echo $home_pautas->remate_4->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
		<td<?php echo $home_pautas->vivolink->CellAttributes() ?>>
<span id="el<?php echo $home_pautas_delete->RowCnt ?>_home_pautas_vivolink" class="home_pautas_vivolink">
<span<?php echo $home_pautas->vivolink->ViewAttributes() ?>>
<?php echo $home_pautas->vivolink->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$home_pautas_delete->Recordset->MoveNext();
}
$home_pautas_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $home_pautas_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fhome_pautasdelete.Init();
</script>
<?php
$home_pautas_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$home_pautas_delete->Page_Terminate();
?>
