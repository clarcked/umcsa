<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_view = NULL; // Initialize page object first

class clotes_view extends clotes {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}
		$KeyUrl = "";
		if (@$_GET["id"] <> "") {
			$this->RecKey["id"] = $_GET["id"];
			$KeyUrl .= "&amp;id=" . urlencode($this->RecKey["id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("loteslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->observaciones->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $IsModal = FALSE;
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->RecKey["id"] = $this->id->QueryStringValue;
			} elseif (@$_POST["id"] <> "") {
				$this->id->setFormValue($_POST["id"]);
				$this->RecKey["id"] = $this->id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("loteslist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->id->CurrentValue) == strval($this->Recordset->fields('id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "loteslist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "loteslist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("ViewPageAddLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->AddUrl) . "',caption:'" . $addcaption . "'});\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$editcaption = ew_HtmlTitle($Language->Phrase("ViewPageEditLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->EditUrl) . "',caption:'" . $editcaption . "'});\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$copycaption = ew_HtmlTitle($Language->Phrase("ViewPageCopyLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->CopyUrl) . "',caption:'" . $copycaption . "'});\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a onclick=\"return ew_ConfirmDelete(this);\" class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		if (array_key_exists('EV__remate_id', $rs->fields)) {
			$this->remate_id->VirtualValue = $rs->fields('EV__remate_id'); // Set up virtual field value
		} else {
			$this->remate_id->VirtualValue = ""; // Clear value
		}
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		if (array_key_exists('EV__conjunto_lote_id', $rs->fields)) {
			$this->conjunto_lote_id->VirtualValue = $rs->fields('EV__conjunto_lote_id'); // Set up virtual field value
		} else {
			$this->conjunto_lote_id->VirtualValue = ""; // Clear value
		}
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		if (array_key_exists('EV__tipo', $rs->fields)) {
			$this->tipo->VirtualValue = $rs->fields('EV__tipo'); // Set up virtual field value
		} else {
			$this->tipo->VirtualValue = ""; // Clear value
		}
		$this->categoria->setDbValue($rs->fields('categoria'));
		if (array_key_exists('EV__categoria', $rs->fields)) {
			$this->categoria->VirtualValue = $rs->fields('EV__categoria'); // Set up virtual field value
		} else {
			$this->categoria->VirtualValue = ""; // Clear value
		}
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		if (array_key_exists('EV__sub_categoria', $rs->fields)) {
			$this->sub_categoria->VirtualValue = $rs->fields('EV__sub_categoria'); // Set up virtual field value
		} else {
			$this->sub_categoria->VirtualValue = ""; // Clear value
		}
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		if (array_key_exists('EV__localidad', $rs->fields)) {
			$this->localidad->VirtualValue = $rs->fields('EV__localidad'); // Set up virtual field value
		} else {
			$this->localidad->VirtualValue = ""; // Clear value
		}
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->venta_tipo->DbValue = $row['venta_tipo'];
		$this->activo->DbValue = $row['activo'];
		$this->vendido->DbValue = $row['vendido'];
		$this->codigo->DbValue = $row['codigo'];
		$this->remate_id->DbValue = $row['remate_id'];
		$this->conjunto_lote_id->DbValue = $row['conjunto_lote_id'];
		$this->orden->DbValue = $row['orden'];
		$this->video->DbValue = $row['video'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->precio->DbValue = $row['precio'];
		$this->fecha_film->DbValue = $row['fecha_film'];
		$this->establecimiento->DbValue = $row['establecimiento'];
		$this->remitente->DbValue = $row['remitente'];
		$this->tipo->DbValue = $row['tipo'];
		$this->categoria->DbValue = $row['categoria'];
		$this->sub_categoria->DbValue = $row['sub_categoria'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->peso->DbValue = $row['peso'];
		$this->caracteristicas->DbValue = $row['caracteristicas'];
		$this->provincia->DbValue = $row['provincia'];
		$this->localidad->DbValue = $row['localidad'];
		$this->lugar->DbValue = $row['lugar'];
		$this->titulo->DbValue = $row['titulo'];
		$this->edad_aprox->DbValue = $row['edad_aprox'];
		$this->trazabilidad->DbValue = $row['trazabilidad'];
		$this->mio_mio->DbValue = $row['mio_mio'];
		$this->garrapata->DbValue = $row['garrapata'];
		$this->observaciones->DbValue = $row['observaciones'];
		$this->plazo->DbValue = $row['plazo'];
		$this->visitas->DbValue = $row['visitas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Convert decimal values if posted back
		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// observaciones
		$this->observaciones->ViewValue = $this->observaciones->CurrentValue;
		$this->observaciones->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";
			$this->observaciones->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_lotes\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_lotes',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.flotesview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("loteslist.php"), "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_view)) $lotes_view = new clotes_view();

// Page init
$lotes_view->Page_Init();

// Page main
$lotes_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = flotesview = new ew_Form("flotesview", "view");

// Form_CustomValidate event
flotesview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
flotesview.ValidateRequired = true;
<?php } else { ?>
flotesview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
flotesview.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
flotesview.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
flotesview.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
flotesview.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
flotesview.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
flotesview.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
flotesview.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
flotesview.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
flotesview.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
flotesview.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"localidades"};
flotesview.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
flotesview.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
flotesview.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesview.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($lotes->Export == "") { ?>
<div class="ewToolbar">
<?php if (!$lotes_view->IsModal) { ?>
<?php if ($lotes->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php } ?>
<?php $lotes_view->ExportOptions->Render("body") ?>
<?php
	foreach ($lotes_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if (!$lotes_view->IsModal) { ?>
<?php if ($lotes->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $lotes_view->ShowPageHeader(); ?>
<?php
$lotes_view->ShowMessage();
?>
<?php if (!$lotes_view->IsModal) { ?>
<?php if ($lotes->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($lotes_view->Pager)) $lotes_view->Pager = new cPrevNextPager($lotes_view->StartRec, $lotes_view->DisplayRecs, $lotes_view->TotalRecs) ?>
<?php if ($lotes_view->Pager->RecordCount > 0 && $lotes_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($lotes_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($lotes_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $lotes_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($lotes_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($lotes_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $lotes_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<?php } ?>
<form name="flotesview" id="flotesview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<?php if ($lotes_view->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($lotes->id->Visible) { // id ?>
	<tr id="r_id">
		<td><span id="elh_lotes_id"><?php echo $lotes->id->FldCaption() ?></span></td>
		<td data-name="id"<?php echo $lotes->id->CellAttributes() ?>>
<span id="el_lotes_id">
<span<?php echo $lotes->id->ViewAttributes() ?>>
<?php echo $lotes->id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
	<tr id="r_venta_tipo">
		<td><span id="elh_lotes_venta_tipo"><?php echo $lotes->venta_tipo->FldCaption() ?></span></td>
		<td data-name="venta_tipo"<?php echo $lotes->venta_tipo->CellAttributes() ?>>
<span id="el_lotes_venta_tipo">
<span<?php echo $lotes->venta_tipo->ViewAttributes() ?>>
<?php echo $lotes->venta_tipo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
	<tr id="r_activo">
		<td><span id="elh_lotes_activo"><?php echo $lotes->activo->FldCaption() ?></span></td>
		<td data-name="activo"<?php echo $lotes->activo->CellAttributes() ?>>
<span id="el_lotes_activo">
<span<?php echo $lotes->activo->ViewAttributes() ?>>
<?php echo $lotes->activo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<tr id="r_vendido">
		<td><span id="elh_lotes_vendido"><?php echo $lotes->vendido->FldCaption() ?></span></td>
		<td data-name="vendido"<?php echo $lotes->vendido->CellAttributes() ?>>
<span id="el_lotes_vendido">
<span<?php echo $lotes->vendido->ViewAttributes() ?>>
<?php echo $lotes->vendido->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<tr id="r_codigo">
		<td><span id="elh_lotes_codigo"><?php echo $lotes->codigo->FldCaption() ?></span></td>
		<td data-name="codigo"<?php echo $lotes->codigo->CellAttributes() ?>>
<span id="el_lotes_codigo">
<span<?php echo $lotes->codigo->ViewAttributes() ?>>
<?php echo $lotes->codigo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<tr id="r_remate_id">
		<td><span id="elh_lotes_remate_id"><?php echo $lotes->remate_id->FldCaption() ?></span></td>
		<td data-name="remate_id"<?php echo $lotes->remate_id->CellAttributes() ?>>
<span id="el_lotes_remate_id">
<span<?php echo $lotes->remate_id->ViewAttributes() ?>>
<?php echo $lotes->remate_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<tr id="r_conjunto_lote_id">
		<td><span id="elh_lotes_conjunto_lote_id"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></td>
		<td data-name="conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
<span id="el_lotes_conjunto_lote_id">
<span<?php echo $lotes->conjunto_lote_id->ViewAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
	<tr id="r_orden">
		<td><span id="elh_lotes_orden"><?php echo $lotes->orden->FldCaption() ?></span></td>
		<td data-name="orden"<?php echo $lotes->orden->CellAttributes() ?>>
<span id="el_lotes_orden">
<span<?php echo $lotes->orden->ViewAttributes() ?>>
<?php echo $lotes->orden->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
	<tr id="r_video">
		<td><span id="elh_lotes_video"><?php echo $lotes->video->FldCaption() ?></span></td>
		<td data-name="video"<?php echo $lotes->video->CellAttributes() ?>>
<span id="el_lotes_video">
<span<?php echo $lotes->video->ViewAttributes() ?>>
<?php echo $lotes->video->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
	<tr id="r_foto_1">
		<td><span id="elh_lotes_foto_1"><?php echo $lotes->foto_1->FldCaption() ?></span></td>
		<td data-name="foto_1"<?php echo $lotes->foto_1->CellAttributes() ?>>
<span id="el_lotes_foto_1">
<span<?php echo $lotes->foto_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_1, $lotes->foto_1->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
	<tr id="r_foto_2">
		<td><span id="elh_lotes_foto_2"><?php echo $lotes->foto_2->FldCaption() ?></span></td>
		<td data-name="foto_2"<?php echo $lotes->foto_2->CellAttributes() ?>>
<span id="el_lotes_foto_2">
<span<?php echo $lotes->foto_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_2, $lotes->foto_2->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
	<tr id="r_foto_3">
		<td><span id="elh_lotes_foto_3"><?php echo $lotes->foto_3->FldCaption() ?></span></td>
		<td data-name="foto_3"<?php echo $lotes->foto_3->CellAttributes() ?>>
<span id="el_lotes_foto_3">
<span<?php echo $lotes->foto_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_3, $lotes->foto_3->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
	<tr id="r_foto_4">
		<td><span id="elh_lotes_foto_4"><?php echo $lotes->foto_4->FldCaption() ?></span></td>
		<td data-name="foto_4"<?php echo $lotes->foto_4->CellAttributes() ?>>
<span id="el_lotes_foto_4">
<span<?php echo $lotes->foto_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($lotes->foto_4, $lotes->foto_4->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
	<tr id="r_precio">
		<td><span id="elh_lotes_precio"><?php echo $lotes->precio->FldCaption() ?></span></td>
		<td data-name="precio"<?php echo $lotes->precio->CellAttributes() ?>>
<span id="el_lotes_precio">
<span<?php echo $lotes->precio->ViewAttributes() ?>>
<?php echo $lotes->precio->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
	<tr id="r_fecha_film">
		<td><span id="elh_lotes_fecha_film"><?php echo $lotes->fecha_film->FldCaption() ?></span></td>
		<td data-name="fecha_film"<?php echo $lotes->fecha_film->CellAttributes() ?>>
<span id="el_lotes_fecha_film">
<span<?php echo $lotes->fecha_film->ViewAttributes() ?>>
<?php echo $lotes->fecha_film->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<tr id="r_establecimiento">
		<td><span id="elh_lotes_establecimiento"><?php echo $lotes->establecimiento->FldCaption() ?></span></td>
		<td data-name="establecimiento"<?php echo $lotes->establecimiento->CellAttributes() ?>>
<span id="el_lotes_establecimiento">
<span<?php echo $lotes->establecimiento->ViewAttributes() ?>>
<?php echo $lotes->establecimiento->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<tr id="r_remitente">
		<td><span id="elh_lotes_remitente"><?php echo $lotes->remitente->FldCaption() ?></span></td>
		<td data-name="remitente"<?php echo $lotes->remitente->CellAttributes() ?>>
<span id="el_lotes_remitente">
<span<?php echo $lotes->remitente->ViewAttributes() ?>>
<?php echo $lotes->remitente->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<tr id="r_tipo">
		<td><span id="elh_lotes_tipo"><?php echo $lotes->tipo->FldCaption() ?></span></td>
		<td data-name="tipo"<?php echo $lotes->tipo->CellAttributes() ?>>
<span id="el_lotes_tipo">
<span<?php echo $lotes->tipo->ViewAttributes() ?>>
<?php echo $lotes->tipo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<tr id="r_categoria">
		<td><span id="elh_lotes_categoria"><?php echo $lotes->categoria->FldCaption() ?></span></td>
		<td data-name="categoria"<?php echo $lotes->categoria->CellAttributes() ?>>
<span id="el_lotes_categoria">
<span<?php echo $lotes->categoria->ViewAttributes() ?>>
<?php echo $lotes->categoria->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
	<tr id="r_sub_categoria">
		<td><span id="elh_lotes_sub_categoria"><?php echo $lotes->sub_categoria->FldCaption() ?></span></td>
		<td data-name="sub_categoria"<?php echo $lotes->sub_categoria->CellAttributes() ?>>
<span id="el_lotes_sub_categoria">
<span<?php echo $lotes->sub_categoria->ViewAttributes() ?>>
<?php echo $lotes->sub_categoria->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
	<tr id="r_cantidad">
		<td><span id="elh_lotes_cantidad"><?php echo $lotes->cantidad->FldCaption() ?></span></td>
		<td data-name="cantidad"<?php echo $lotes->cantidad->CellAttributes() ?>>
<span id="el_lotes_cantidad">
<span<?php echo $lotes->cantidad->ViewAttributes() ?>>
<?php echo $lotes->cantidad->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
	<tr id="r_peso">
		<td><span id="elh_lotes_peso"><?php echo $lotes->peso->FldCaption() ?></span></td>
		<td data-name="peso"<?php echo $lotes->peso->CellAttributes() ?>>
<span id="el_lotes_peso">
<span<?php echo $lotes->peso->ViewAttributes() ?>>
<?php echo $lotes->peso->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
	<tr id="r_caracteristicas">
		<td><span id="elh_lotes_caracteristicas"><?php echo $lotes->caracteristicas->FldCaption() ?></span></td>
		<td data-name="caracteristicas"<?php echo $lotes->caracteristicas->CellAttributes() ?>>
<span id="el_lotes_caracteristicas">
<span<?php echo $lotes->caracteristicas->ViewAttributes() ?>>
<?php echo $lotes->caracteristicas->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
	<tr id="r_provincia">
		<td><span id="elh_lotes_provincia"><?php echo $lotes->provincia->FldCaption() ?></span></td>
		<td data-name="provincia"<?php echo $lotes->provincia->CellAttributes() ?>>
<span id="el_lotes_provincia">
<span<?php echo $lotes->provincia->ViewAttributes() ?>>
<?php echo $lotes->provincia->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
	<tr id="r_localidad">
		<td><span id="elh_lotes_localidad"><?php echo $lotes->localidad->FldCaption() ?></span></td>
		<td data-name="localidad"<?php echo $lotes->localidad->CellAttributes() ?>>
<span id="el_lotes_localidad">
<span<?php echo $lotes->localidad->ViewAttributes() ?>>
<?php echo $lotes->localidad->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
	<tr id="r_lugar">
		<td><span id="elh_lotes_lugar"><?php echo $lotes->lugar->FldCaption() ?></span></td>
		<td data-name="lugar"<?php echo $lotes->lugar->CellAttributes() ?>>
<span id="el_lotes_lugar">
<span<?php echo $lotes->lugar->ViewAttributes() ?>>
<?php echo $lotes->lugar->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
	<tr id="r_titulo">
		<td><span id="elh_lotes_titulo"><?php echo $lotes->titulo->FldCaption() ?></span></td>
		<td data-name="titulo"<?php echo $lotes->titulo->CellAttributes() ?>>
<span id="el_lotes_titulo">
<span<?php echo $lotes->titulo->ViewAttributes() ?>>
<?php echo $lotes->titulo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
	<tr id="r_edad_aprox">
		<td><span id="elh_lotes_edad_aprox"><?php echo $lotes->edad_aprox->FldCaption() ?></span></td>
		<td data-name="edad_aprox"<?php echo $lotes->edad_aprox->CellAttributes() ?>>
<span id="el_lotes_edad_aprox">
<span<?php echo $lotes->edad_aprox->ViewAttributes() ?>>
<?php echo $lotes->edad_aprox->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<tr id="r_trazabilidad">
		<td><span id="elh_lotes_trazabilidad"><?php echo $lotes->trazabilidad->FldCaption() ?></span></td>
		<td data-name="trazabilidad"<?php echo $lotes->trazabilidad->CellAttributes() ?>>
<span id="el_lotes_trazabilidad">
<span<?php echo $lotes->trazabilidad->ViewAttributes() ?>>
<?php echo $lotes->trazabilidad->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<tr id="r_mio_mio">
		<td><span id="elh_lotes_mio_mio"><?php echo $lotes->mio_mio->FldCaption() ?></span></td>
		<td data-name="mio_mio"<?php echo $lotes->mio_mio->CellAttributes() ?>>
<span id="el_lotes_mio_mio">
<span<?php echo $lotes->mio_mio->ViewAttributes() ?>>
<?php echo $lotes->mio_mio->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<tr id="r_garrapata">
		<td><span id="elh_lotes_garrapata"><?php echo $lotes->garrapata->FldCaption() ?></span></td>
		<td data-name="garrapata"<?php echo $lotes->garrapata->CellAttributes() ?>>
<span id="el_lotes_garrapata">
<span<?php echo $lotes->garrapata->ViewAttributes() ?>>
<?php echo $lotes->garrapata->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->observaciones->Visible) { // observaciones ?>
	<tr id="r_observaciones">
		<td><span id="elh_lotes_observaciones"><?php echo $lotes->observaciones->FldCaption() ?></span></td>
		<td data-name="observaciones"<?php echo $lotes->observaciones->CellAttributes() ?>>
<span id="el_lotes_observaciones">
<span<?php echo $lotes->observaciones->ViewAttributes() ?>>
<?php echo $lotes->observaciones->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
	<tr id="r_plazo">
		<td><span id="elh_lotes_plazo"><?php echo $lotes->plazo->FldCaption() ?></span></td>
		<td data-name="plazo"<?php echo $lotes->plazo->CellAttributes() ?>>
<span id="el_lotes_plazo">
<span<?php echo $lotes->plazo->ViewAttributes() ?>>
<?php echo $lotes->plazo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
	<tr id="r_visitas">
		<td><span id="elh_lotes_visitas"><?php echo $lotes->visitas->FldCaption() ?></span></td>
		<td data-name="visitas"<?php echo $lotes->visitas->CellAttributes() ?>>
<span id="el_lotes_visitas">
<span<?php echo $lotes->visitas->ViewAttributes() ?>>
<?php echo $lotes->visitas->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if (!$lotes_view->IsModal) { ?>
<?php if ($lotes->Export == "") { ?>
<?php if (!isset($lotes_view->Pager)) $lotes_view->Pager = new cPrevNextPager($lotes_view->StartRec, $lotes_view->DisplayRecs, $lotes_view->TotalRecs) ?>
<?php if ($lotes_view->Pager->RecordCount > 0 && $lotes_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($lotes_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($lotes_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $lotes_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($lotes_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($lotes_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $lotes_view->PageUrl() ?>start=<?php echo $lotes_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $lotes_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
<?php } ?>
</form>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">
flotesview.Init();
</script>
<?php } ?>
<?php
$lotes_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($lotes->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$lotes_view->Page_Terminate();
?>
