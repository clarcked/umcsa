<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "home_pautasinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$home_pautas_list = NULL; // Initialize page object first

class chome_pautas_list extends chome_pautas {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'home_pautas';

	// Page object name
	var $PageObjName = 'home_pautas_list';

	// Grid form hidden field names
	var $FormName = 'fhome_pautaslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (home_pautas)
		if (!isset($GLOBALS["home_pautas"]) || get_class($GLOBALS["home_pautas"]) == "chome_pautas") {
			$GLOBALS["home_pautas"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["home_pautas"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "home_pautasadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "home_pautasdelete.php";
		$this->MultiUpdateUrl = "home_pautasupdate.php";

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'home_pautas', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption fhome_pautaslistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}

		// Create form object
		$objForm = new cFormObj();

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->activa->SetVisibility();
		$this->fecha_desde->SetVisibility();
		$this->fecha_hasta->SetVisibility();
		$this->remate_1->SetVisibility();
		$this->remate_2->SetVisibility();
		$this->remate_3->SetVisibility();
		$this->remate_4->SetVisibility();
		$this->vivolink->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $home_pautas;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($home_pautas);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid edit mode
				if ($this->CurrentAction == "gridedit")
					$this->GridEditMode();

				// Switch to inline edit mode
				if ($this->CurrentAction == "edit")
					$this->InlineEditMode();

				// Switch to inline add mode
				if ($this->CurrentAction == "add" || $this->CurrentAction == "copy")
					$this->InlineAddMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Update
					if (($this->CurrentAction == "gridupdate" || $this->CurrentAction == "gridoverwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridedit") {
						if ($this->ValidateGridForm()) {
							$bGridUpdate = $this->GridUpdate();
						} else {
							$bGridUpdate = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridUpdate) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridedit"; // Stay in Grid Edit mode
						}
					}

					// Inline Update
					if (($this->CurrentAction == "update" || $this->CurrentAction == "overwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "edit")
						$this->InlineUpdate();

					// Insert Inline
					if ($this->CurrentAction == "insert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "add")
						$this->InlineInsert();

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Process filter list
			$this->ProcessFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->setKey("id", ""); // Clear inline edit key
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Switch to Inline Edit mode
	function InlineEditMode() {
		global $Security, $Language;
		if (!$Security->CanEdit())
			$this->Page_Terminate("login.php"); // Go to login page
		$bInlineEdit = TRUE;
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		} else {
			$bInlineEdit = FALSE;
		}
		if ($bInlineEdit) {
			if ($this->LoadRow()) {
				$this->setKey("id", $this->id->CurrentValue); // Set up inline edit key
				$_SESSION[EW_SESSION_INLINE_MODE] = "edit"; // Enable inline edit
			}
		}
	}

	// Perform update to Inline Edit record
	function InlineUpdate() {
		global $Language, $objForm, $gsFormError;
		$objForm->Index = 1; 
		$this->LoadFormValues(); // Get form values

		// Validate form
		$bInlineUpdate = TRUE;
		if (!$this->ValidateForm()) {	
			$bInlineUpdate = FALSE; // Form error, reset action
			$this->setFailureMessage($gsFormError);
		} else {
			$bInlineUpdate = FALSE;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			if ($this->SetupKeyValues($rowkey)) { // Set up key values
				if ($this->CheckInlineEditKey()) { // Check key
					$this->SendEmail = TRUE; // Send email on update success
					$bInlineUpdate = $this->EditRow(); // Update record
				} else {
					$bInlineUpdate = FALSE;
				}
			}
		}
		if ($bInlineUpdate) { // Update success
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
			$this->EventCancelled = TRUE; // Cancel event
			$this->CurrentAction = "edit"; // Stay in edit mode
		}
	}

	// Check Inline Edit key
	function CheckInlineEditKey() {

		//CheckInlineEditKey = True
		if (strval($this->getKey("id")) <> strval($this->id->CurrentValue))
			return FALSE;
		return TRUE;
	}

	// Switch to Inline Add mode
	function InlineAddMode() {
		global $Security, $Language;
		if (!$Security->CanAdd())
			$this->Page_Terminate("login.php"); // Return to login page
		if ($this->CurrentAction == "copy") {
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CurrentAction = "add";
			}
		}
		$_SESSION[EW_SESSION_INLINE_MODE] = "add"; // Enable inline add
	}

	// Perform update to Inline Add/Copy record
	function InlineInsert() {
		global $Language, $objForm, $gsFormError;
		$this->LoadOldRecord(); // Load old recordset
		$objForm->Index = 0;
		$this->LoadFormValues(); // Get form values

		// Validate form
		if (!$this->ValidateForm()) {
			$this->setFailureMessage($gsFormError); // Set validation error message
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
			return;
		}
		$this->SendEmail = TRUE; // Send email on add success
		if ($this->AddRow($this->OldRecordset)) { // Add record
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up add success message
			$this->ClearInlineMode(); // Clear inline add mode
		} else { // Add failed
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
		}
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_activa") && $objForm->HasValue("o_activa") && $this->activa->CurrentValue <> $this->activa->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_fecha_desde") && $objForm->HasValue("o_fecha_desde") && $this->fecha_desde->CurrentValue <> $this->fecha_desde->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_fecha_hasta") && $objForm->HasValue("o_fecha_hasta") && $this->fecha_hasta->CurrentValue <> $this->fecha_hasta->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remate_1") && $objForm->HasValue("o_remate_1") && $this->remate_1->CurrentValue <> $this->remate_1->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remate_2") && $objForm->HasValue("o_remate_2") && $this->remate_2->CurrentValue <> $this->remate_2->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remate_3") && $objForm->HasValue("o_remate_3") && $this->remate_3->CurrentValue <> $this->remate_3->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_remate_4") && $objForm->HasValue("o_remate_4") && $this->remate_4->CurrentValue <> $this->remate_4->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_vivolink") && $objForm->HasValue("o_vivolink") && $this->vivolink->CurrentValue <> $this->vivolink->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {
		global $UserProfile;

		// Load server side filters
		if (EW_SEARCH_FILTER_OPTION == "Server") {
			$sSavedFilterList = $UserProfile->GetSearchFilters(CurrentUserName(), "fhome_pautaslistsrch");
		} else {
			$sSavedFilterList = "";
		}

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->id->AdvancedSearch->ToJSON(), ","); // Field id
		$sFilterList = ew_Concat($sFilterList, $this->activa->AdvancedSearch->ToJSON(), ","); // Field activa
		$sFilterList = ew_Concat($sFilterList, $this->fecha_desde->AdvancedSearch->ToJSON(), ","); // Field fecha_desde
		$sFilterList = ew_Concat($sFilterList, $this->fecha_hasta->AdvancedSearch->ToJSON(), ","); // Field fecha_hasta
		$sFilterList = ew_Concat($sFilterList, $this->remate_1->AdvancedSearch->ToJSON(), ","); // Field remate_1
		$sFilterList = ew_Concat($sFilterList, $this->remate_2->AdvancedSearch->ToJSON(), ","); // Field remate_2
		$sFilterList = ew_Concat($sFilterList, $this->remate_3->AdvancedSearch->ToJSON(), ","); // Field remate_3
		$sFilterList = ew_Concat($sFilterList, $this->remate_4->AdvancedSearch->ToJSON(), ","); // Field remate_4
		$sFilterList = ew_Concat($sFilterList, $this->vivolink->AdvancedSearch->ToJSON(), ","); // Field vivolink
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}
		$sFilterList = preg_replace('/,$/', "", $sFilterList);

		// Return filter list in json
		if ($sFilterList <> "")
			$sFilterList = "\"data\":{" . $sFilterList . "}";
		if ($sSavedFilterList <> "") {
			if ($sFilterList <> "")
				$sFilterList .= ",";
			$sFilterList .= "\"filters\":" . $sSavedFilterList;
		}
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Process filter list
	function ProcessFilterList() {
		global $UserProfile;
		if (@$_POST["ajax"] == "savefilters") { // Save filter request (Ajax)
			$filters = ew_StripSlashes(@$_POST["filters"]);
			$UserProfile->SetSearchFilters(CurrentUserName(), "fhome_pautaslistsrch", $filters);

			// Clean output buffer
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			echo ew_ArrayToJson(array(array("success" => TRUE))); // Success
			$this->Page_Terminate();
			exit();
		} elseif (@$_POST["cmd"] == "resetfilter") {
			$this->RestoreFilterList();
		}
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field id
		$this->id->AdvancedSearch->SearchValue = @$filter["x_id"];
		$this->id->AdvancedSearch->SearchOperator = @$filter["z_id"];
		$this->id->AdvancedSearch->SearchCondition = @$filter["v_id"];
		$this->id->AdvancedSearch->SearchValue2 = @$filter["y_id"];
		$this->id->AdvancedSearch->SearchOperator2 = @$filter["w_id"];
		$this->id->AdvancedSearch->Save();

		// Field activa
		$this->activa->AdvancedSearch->SearchValue = @$filter["x_activa"];
		$this->activa->AdvancedSearch->SearchOperator = @$filter["z_activa"];
		$this->activa->AdvancedSearch->SearchCondition = @$filter["v_activa"];
		$this->activa->AdvancedSearch->SearchValue2 = @$filter["y_activa"];
		$this->activa->AdvancedSearch->SearchOperator2 = @$filter["w_activa"];
		$this->activa->AdvancedSearch->Save();

		// Field fecha_desde
		$this->fecha_desde->AdvancedSearch->SearchValue = @$filter["x_fecha_desde"];
		$this->fecha_desde->AdvancedSearch->SearchOperator = @$filter["z_fecha_desde"];
		$this->fecha_desde->AdvancedSearch->SearchCondition = @$filter["v_fecha_desde"];
		$this->fecha_desde->AdvancedSearch->SearchValue2 = @$filter["y_fecha_desde"];
		$this->fecha_desde->AdvancedSearch->SearchOperator2 = @$filter["w_fecha_desde"];
		$this->fecha_desde->AdvancedSearch->Save();

		// Field fecha_hasta
		$this->fecha_hasta->AdvancedSearch->SearchValue = @$filter["x_fecha_hasta"];
		$this->fecha_hasta->AdvancedSearch->SearchOperator = @$filter["z_fecha_hasta"];
		$this->fecha_hasta->AdvancedSearch->SearchCondition = @$filter["v_fecha_hasta"];
		$this->fecha_hasta->AdvancedSearch->SearchValue2 = @$filter["y_fecha_hasta"];
		$this->fecha_hasta->AdvancedSearch->SearchOperator2 = @$filter["w_fecha_hasta"];
		$this->fecha_hasta->AdvancedSearch->Save();

		// Field remate_1
		$this->remate_1->AdvancedSearch->SearchValue = @$filter["x_remate_1"];
		$this->remate_1->AdvancedSearch->SearchOperator = @$filter["z_remate_1"];
		$this->remate_1->AdvancedSearch->SearchCondition = @$filter["v_remate_1"];
		$this->remate_1->AdvancedSearch->SearchValue2 = @$filter["y_remate_1"];
		$this->remate_1->AdvancedSearch->SearchOperator2 = @$filter["w_remate_1"];
		$this->remate_1->AdvancedSearch->Save();

		// Field remate_2
		$this->remate_2->AdvancedSearch->SearchValue = @$filter["x_remate_2"];
		$this->remate_2->AdvancedSearch->SearchOperator = @$filter["z_remate_2"];
		$this->remate_2->AdvancedSearch->SearchCondition = @$filter["v_remate_2"];
		$this->remate_2->AdvancedSearch->SearchValue2 = @$filter["y_remate_2"];
		$this->remate_2->AdvancedSearch->SearchOperator2 = @$filter["w_remate_2"];
		$this->remate_2->AdvancedSearch->Save();

		// Field remate_3
		$this->remate_3->AdvancedSearch->SearchValue = @$filter["x_remate_3"];
		$this->remate_3->AdvancedSearch->SearchOperator = @$filter["z_remate_3"];
		$this->remate_3->AdvancedSearch->SearchCondition = @$filter["v_remate_3"];
		$this->remate_3->AdvancedSearch->SearchValue2 = @$filter["y_remate_3"];
		$this->remate_3->AdvancedSearch->SearchOperator2 = @$filter["w_remate_3"];
		$this->remate_3->AdvancedSearch->Save();

		// Field remate_4
		$this->remate_4->AdvancedSearch->SearchValue = @$filter["x_remate_4"];
		$this->remate_4->AdvancedSearch->SearchOperator = @$filter["z_remate_4"];
		$this->remate_4->AdvancedSearch->SearchCondition = @$filter["v_remate_4"];
		$this->remate_4->AdvancedSearch->SearchValue2 = @$filter["y_remate_4"];
		$this->remate_4->AdvancedSearch->SearchOperator2 = @$filter["w_remate_4"];
		$this->remate_4->AdvancedSearch->Save();

		// Field vivolink
		$this->vivolink->AdvancedSearch->SearchValue = @$filter["x_vivolink"];
		$this->vivolink->AdvancedSearch->SearchOperator = @$filter["z_vivolink"];
		$this->vivolink->AdvancedSearch->SearchCondition = @$filter["v_vivolink"];
		$this->vivolink->AdvancedSearch->SearchValue2 = @$filter["y_vivolink"];
		$this->vivolink->AdvancedSearch->SearchOperator2 = @$filter["w_vivolink"];
		$this->vivolink->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->id, $Default, FALSE); // id
		$this->BuildSearchSql($sWhere, $this->activa, $Default, FALSE); // activa
		$this->BuildSearchSql($sWhere, $this->fecha_desde, $Default, FALSE); // fecha_desde
		$this->BuildSearchSql($sWhere, $this->fecha_hasta, $Default, FALSE); // fecha_hasta
		$this->BuildSearchSql($sWhere, $this->remate_1, $Default, FALSE); // remate_1
		$this->BuildSearchSql($sWhere, $this->remate_2, $Default, FALSE); // remate_2
		$this->BuildSearchSql($sWhere, $this->remate_3, $Default, FALSE); // remate_3
		$this->BuildSearchSql($sWhere, $this->remate_4, $Default, FALSE); // remate_4
		$this->BuildSearchSql($sWhere, $this->vivolink, $Default, FALSE); // vivolink

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->id->AdvancedSearch->Save(); // id
			$this->activa->AdvancedSearch->Save(); // activa
			$this->fecha_desde->AdvancedSearch->Save(); // fecha_desde
			$this->fecha_hasta->AdvancedSearch->Save(); // fecha_hasta
			$this->remate_1->AdvancedSearch->Save(); // remate_1
			$this->remate_2->AdvancedSearch->Save(); // remate_2
			$this->remate_3->AdvancedSearch->Save(); // remate_3
			$this->remate_4->AdvancedSearch->Save(); // remate_4
			$this->vivolink->AdvancedSearch->Save(); // vivolink
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1)
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE || $Fld->FldDataType == EW_DATATYPE_TIME) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->vivolink, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSQL(&$Where, &$Fld, $arKeywords, $type) {
		global $EW_BASIC_SEARCH_IGNORE_PATTERN;
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if ($EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace($EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->activa->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->fecha_desde->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->fecha_hasta->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remate_1->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remate_2->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remate_3->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->remate_4->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->vivolink->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->id->AdvancedSearch->UnsetSession();
		$this->activa->AdvancedSearch->UnsetSession();
		$this->fecha_desde->AdvancedSearch->UnsetSession();
		$this->fecha_hasta->AdvancedSearch->UnsetSession();
		$this->remate_1->AdvancedSearch->UnsetSession();
		$this->remate_2->AdvancedSearch->UnsetSession();
		$this->remate_3->AdvancedSearch->UnsetSession();
		$this->remate_4->AdvancedSearch->UnsetSession();
		$this->vivolink->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->id->AdvancedSearch->Load();
		$this->activa->AdvancedSearch->Load();
		$this->fecha_desde->AdvancedSearch->Load();
		$this->fecha_hasta->AdvancedSearch->Load();
		$this->remate_1->AdvancedSearch->Load();
		$this->remate_2->AdvancedSearch->Load();
		$this->remate_3->AdvancedSearch->Load();
		$this->remate_4->AdvancedSearch->Load();
		$this->vivolink->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->activa); // activa
			$this->UpdateSort($this->fecha_desde); // fecha_desde
			$this->UpdateSort($this->fecha_hasta); // fecha_hasta
			$this->UpdateSort($this->remate_1); // remate_1
			$this->UpdateSort($this->remate_2); // remate_2
			$this->UpdateSort($this->remate_3); // remate_3
			$this->UpdateSort($this->remate_4); // remate_4
			$this->UpdateSort($this->vivolink); // vivolink
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
				$this->fecha_desde->setSort("DESC");
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->id->setSort("");
				$this->activa->setSort("");
				$this->fecha_desde->setSort("");
				$this->fecha_hasta->setSort("");
				$this->remate_1->setSort("");
				$this->remate_2->setSort("");
				$this->remate_3->setSort("");
				$this->remate_4->setSort("");
				$this->vivolink->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = TRUE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = ($Security->CanDelete() || $Security->CanEdit());
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if (($this->CurrentAction == "add" || $this->CurrentAction == "copy") && $this->RowType == EW_ROWTYPE_ADD) { // Inline Add/Copy
			$this->ListOptions->CustomItem = "copy"; // Show copy column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
			$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
				"<a class=\"ewGridLink ewInlineInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("InsertLink") . "</a>&nbsp;" .
				"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
				"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"insert\"></div>";
			return;
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($this->CurrentAction == "edit" && $this->RowType == EW_ROWTYPE_EDIT) { // Inline-Edit
			$this->ListOptions->CustomItem = "edit"; // Show edit column only
			$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
					"<a class=\"ewGridLink ewInlineUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . ew_GetHashUrl($this->PageName(), $this->PageObjName . "_row_" . $this->RowCnt) . "');\">" . $Language->Phrase("UpdateLink") . "</a>&nbsp;" .
					"<a class=\"ewGridLink ewInlineCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("CancelLink") . "</a>" .
					"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"update\"></div>";
			$oListOpt->Body .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_key\" id=\"k" . $this->RowIndex . "_key\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\">";
			return;
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		$viewcaption = ew_HtmlTitle($Language->Phrase("ViewLink"));
		if ($Security->CanView()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . $viewcaption . "\" data-caption=\"" . $viewcaption . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		$editcaption = ew_HtmlTitle($Language->Phrase("EditLink"));
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineEditLink")) . "\" href=\"" . ew_HtmlEncode(ew_GetHashUrl($this->InlineEditUrl, $this->PageObjName . "_row_" . $this->RowCnt)) . "\">" . $Language->Phrase("InlineEditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		$copycaption = ew_HtmlTitle($Language->Phrase("CopyLink"));
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
			$oListOpt->Body .= "<a class=\"ewRowLink ewInlineCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineCopyUrl) . "\">" . $Language->Phrase("InlineCopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt && $this->Export == "" && $this->CurrentAction == "") {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		if ($this->CurrentAction == "gridedit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->id->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("AddLink"));
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Inline Add
		$item = &$option->Add("inlineadd");
		$item->Body = "<a class=\"ewAddEdit ewInlineAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("InlineAddLink")) . "\" href=\"" . ew_HtmlEncode($this->InlineAddUrl) . "\">" .$Language->Phrase("InlineAddLink") . "</a>";
		$item->Visible = ($this->InlineAddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());

		// Add grid edit
		$option = $options["addedit"];
		$item = &$option->Add("gridedit");
		$item->Body = "<a class=\"ewAddEdit ewGridEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GridEditUrl) . "\">" . $Language->Phrase("GridEditLink") . "</a>";
		$item->Visible = ($this->GridEditUrl <> "" && $Security->CanEdit());
		$option = $options["action"];

		// Add multi delete
		$item = &$option->Add("multidelete");
		$item->Body = "<a class=\"ewAction ewMultiDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fhome_pautaslist,url:'" . $this->MultiDeleteUrl . "',msg:ewLanguage.Phrase('DeleteConfirmMsg')});return false;\">" . $Language->Phrase("DeleteSelectedLink") . "</a>";
		$item->Visible = ($Security->CanDelete());

		// Add multi update
		$item = &$option->Add("multiupdate");
		$item->Body = "<a class=\"ewAction ewMultiUpdate\" title=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" data-table=\"home_pautas\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("UpdateSelectedLink")) . "\" href=\"\" onclick=\"ew_SubmitAction(event,{f:document.fhome_pautaslist,url:'" . $this->MultiUpdateUrl . "'});return false;\">" . $Language->Phrase("UpdateSelectedLink") . "</a>";
		$item->Visible = ($Security->CanEdit());

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"fhome_pautaslistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"fhome_pautaslistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.fhome_pautaslist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
			if ($this->CurrentAction == "gridedit") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
					$item = &$option->Add("gridsave");
					$item->Body = "<a class=\"ewAction ewGridSave\" title=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridSaveLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->PageName() . "');\">" . $Language->Phrase("GridSaveLink") . "</a>";
					$item = &$option->Add("gridcancel");
					$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
					$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			$this->CurrentAction = ""; // Clear action
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"fhome_pautaslistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->id->CurrentValue = NULL;
		$this->id->OldValue = $this->id->CurrentValue;
		$this->activa->CurrentValue = NULL;
		$this->activa->OldValue = $this->activa->CurrentValue;
		$this->fecha_desde->CurrentValue = NULL;
		$this->fecha_desde->OldValue = $this->fecha_desde->CurrentValue;
		$this->fecha_hasta->CurrentValue = NULL;
		$this->fecha_hasta->OldValue = $this->fecha_hasta->CurrentValue;
		$this->remate_1->CurrentValue = NULL;
		$this->remate_1->OldValue = $this->remate_1->CurrentValue;
		$this->remate_2->CurrentValue = NULL;
		$this->remate_2->OldValue = $this->remate_2->CurrentValue;
		$this->remate_3->CurrentValue = NULL;
		$this->remate_3->OldValue = $this->remate_3->CurrentValue;
		$this->remate_4->CurrentValue = NULL;
		$this->remate_4->OldValue = $this->remate_4->CurrentValue;
		$this->vivolink->CurrentValue = NULL;
		$this->vivolink->OldValue = $this->vivolink->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_id"]);
		if ($this->id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->id->AdvancedSearch->SearchOperator = @$_GET["z_id"];
		if (is_array($this->id->AdvancedSearch->SearchValue)) $this->id->AdvancedSearch->SearchValue = implode(",", $this->id->AdvancedSearch->SearchValue);
		if (is_array($this->id->AdvancedSearch->SearchValue2)) $this->id->AdvancedSearch->SearchValue2 = implode(",", $this->id->AdvancedSearch->SearchValue2);

		// activa
		$this->activa->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_activa"]);
		if ($this->activa->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->activa->AdvancedSearch->SearchOperator = @$_GET["z_activa"];

		// fecha_desde
		$this->fecha_desde->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_fecha_desde"]);
		if ($this->fecha_desde->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->fecha_desde->AdvancedSearch->SearchOperator = @$_GET["z_fecha_desde"];

		// fecha_hasta
		$this->fecha_hasta->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_fecha_hasta"]);
		if ($this->fecha_hasta->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->fecha_hasta->AdvancedSearch->SearchOperator = @$_GET["z_fecha_hasta"];

		// remate_1
		$this->remate_1->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remate_1"]);
		if ($this->remate_1->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remate_1->AdvancedSearch->SearchOperator = @$_GET["z_remate_1"];

		// remate_2
		$this->remate_2->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remate_2"]);
		if ($this->remate_2->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remate_2->AdvancedSearch->SearchOperator = @$_GET["z_remate_2"];

		// remate_3
		$this->remate_3->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remate_3"]);
		if ($this->remate_3->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remate_3->AdvancedSearch->SearchOperator = @$_GET["z_remate_3"];

		// remate_4
		$this->remate_4->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_remate_4"]);
		if ($this->remate_4->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->remate_4->AdvancedSearch->SearchOperator = @$_GET["z_remate_4"];

		// vivolink
		$this->vivolink->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_vivolink"]);
		if ($this->vivolink->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->vivolink->AdvancedSearch->SearchOperator = @$_GET["z_vivolink"];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->activa->FldIsDetailKey) {
			$this->activa->setFormValue($objForm->GetValue("x_activa"));
		}
		$this->activa->setOldValue($objForm->GetValue("o_activa"));
		if (!$this->fecha_desde->FldIsDetailKey) {
			$this->fecha_desde->setFormValue($objForm->GetValue("x_fecha_desde"));
			$this->fecha_desde->CurrentValue = ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0);
		}
		$this->fecha_desde->setOldValue($objForm->GetValue("o_fecha_desde"));
		if (!$this->fecha_hasta->FldIsDetailKey) {
			$this->fecha_hasta->setFormValue($objForm->GetValue("x_fecha_hasta"));
			$this->fecha_hasta->CurrentValue = ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0);
		}
		$this->fecha_hasta->setOldValue($objForm->GetValue("o_fecha_hasta"));
		if (!$this->remate_1->FldIsDetailKey) {
			$this->remate_1->setFormValue($objForm->GetValue("x_remate_1"));
		}
		$this->remate_1->setOldValue($objForm->GetValue("o_remate_1"));
		if (!$this->remate_2->FldIsDetailKey) {
			$this->remate_2->setFormValue($objForm->GetValue("x_remate_2"));
		}
		$this->remate_2->setOldValue($objForm->GetValue("o_remate_2"));
		if (!$this->remate_3->FldIsDetailKey) {
			$this->remate_3->setFormValue($objForm->GetValue("x_remate_3"));
		}
		$this->remate_3->setOldValue($objForm->GetValue("o_remate_3"));
		if (!$this->remate_4->FldIsDetailKey) {
			$this->remate_4->setFormValue($objForm->GetValue("x_remate_4"));
		}
		$this->remate_4->setOldValue($objForm->GetValue("o_remate_4"));
		if (!$this->vivolink->FldIsDetailKey) {
			$this->vivolink->setFormValue($objForm->GetValue("x_vivolink"));
		}
		$this->vivolink->setOldValue($objForm->GetValue("o_vivolink"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->CurrentValue = $this->id->FormValue;
		$this->activa->CurrentValue = $this->activa->FormValue;
		$this->fecha_desde->CurrentValue = $this->fecha_desde->FormValue;
		$this->fecha_desde->CurrentValue = ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0);
		$this->fecha_hasta->CurrentValue = $this->fecha_hasta->FormValue;
		$this->fecha_hasta->CurrentValue = ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0);
		$this->remate_1->CurrentValue = $this->remate_1->FormValue;
		$this->remate_2->CurrentValue = $this->remate_2->FormValue;
		$this->remate_3->CurrentValue = $this->remate_3->FormValue;
		$this->remate_4->CurrentValue = $this->remate_4->FormValue;
		$this->vivolink->CurrentValue = $this->vivolink->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->fecha_desde->setDbValue($rs->fields('fecha_desde'));
		$this->fecha_hasta->setDbValue($rs->fields('fecha_hasta'));
		$this->remate_1->setDbValue($rs->fields('remate_1'));
		$this->remate_2->setDbValue($rs->fields('remate_2'));
		$this->remate_3->setDbValue($rs->fields('remate_3'));
		$this->remate_4->setDbValue($rs->fields('remate_4'));
		$this->vivolink->setDbValue($rs->fields('vivolink'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->activa->DbValue = $row['activa'];
		$this->fecha_desde->DbValue = $row['fecha_desde'];
		$this->fecha_hasta->DbValue = $row['fecha_hasta'];
		$this->remate_1->DbValue = $row['remate_1'];
		$this->remate_2->DbValue = $row['remate_2'];
		$this->remate_3->DbValue = $row['remate_3'];
		$this->remate_4->DbValue = $row['remate_4'];
		$this->vivolink->DbValue = $row['vivolink'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// activa
		// fecha_desde
		// fecha_hasta
		// remate_1
		// remate_2
		// remate_3
		// remate_4
		// vivolink

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// fecha_desde
		$this->fecha_desde->ViewValue = $this->fecha_desde->CurrentValue;
		$this->fecha_desde->ViewValue = ew_FormatDateTime($this->fecha_desde->ViewValue, 0);
		$this->fecha_desde->ViewCustomAttributes = "";

		// fecha_hasta
		$this->fecha_hasta->ViewValue = $this->fecha_hasta->CurrentValue;
		$this->fecha_hasta->ViewValue = ew_FormatDateTime($this->fecha_hasta->ViewValue, 0);
		$this->fecha_hasta->ViewCustomAttributes = "";

		// remate_1
		if (strval($this->remate_1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_1->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_1->ViewValue = $this->remate_1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_1->ViewValue = $this->remate_1->CurrentValue;
			}
		} else {
			$this->remate_1->ViewValue = NULL;
		}
		$this->remate_1->ViewCustomAttributes = "";

		// remate_2
		if (strval($this->remate_2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_2->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_2->ViewValue = $this->remate_2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_2->ViewValue = $this->remate_2->CurrentValue;
			}
		} else {
			$this->remate_2->ViewValue = NULL;
		}
		$this->remate_2->ViewCustomAttributes = "";

		// remate_3
		if (strval($this->remate_3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_3->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_3->ViewValue = $this->remate_3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_3->ViewValue = $this->remate_3->CurrentValue;
			}
		} else {
			$this->remate_3->ViewValue = NULL;
		}
		$this->remate_3->ViewCustomAttributes = "";

		// remate_4
		if (strval($this->remate_4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_4->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_4->ViewValue = $this->remate_4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_4->ViewValue = $this->remate_4->CurrentValue;
			}
		} else {
			$this->remate_4->ViewValue = NULL;
		}
		$this->remate_4->ViewCustomAttributes = "";

		// vivolink
		$this->vivolink->ViewValue = $this->vivolink->CurrentValue;
		$this->vivolink->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";
			$this->fecha_desde->TooltipValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";
			$this->fecha_hasta->TooltipValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";
			$this->remate_1->TooltipValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";
			$this->remate_2->TooltipValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";
			$this->remate_3->TooltipValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";
			$this->remate_4->TooltipValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
			$this->vivolink->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// id
			// activa

			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// fecha_desde
			$this->fecha_desde->EditAttrs["class"] = "form-control";
			$this->fecha_desde->EditCustomAttributes = "";
			$this->fecha_desde->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_desde->CurrentValue, 8));
			$this->fecha_desde->PlaceHolder = ew_RemoveHtml($this->fecha_desde->FldCaption());

			// fecha_hasta
			$this->fecha_hasta->EditAttrs["class"] = "form-control";
			$this->fecha_hasta->EditCustomAttributes = "";
			$this->fecha_hasta->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_hasta->CurrentValue, 8));
			$this->fecha_hasta->PlaceHolder = ew_RemoveHtml($this->fecha_hasta->FldCaption());

			// remate_1
			$this->remate_1->EditAttrs["class"] = "form-control";
			$this->remate_1->EditCustomAttributes = "";
			if (trim(strval($this->remate_1->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_1->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_1->EditValue = $arwrk;

			// remate_2
			$this->remate_2->EditAttrs["class"] = "form-control";
			$this->remate_2->EditCustomAttributes = "";
			if (trim(strval($this->remate_2->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_2->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_2->EditValue = $arwrk;

			// remate_3
			$this->remate_3->EditAttrs["class"] = "form-control";
			$this->remate_3->EditCustomAttributes = "";
			if (trim(strval($this->remate_3->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_3->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_3->EditValue = $arwrk;

			// remate_4
			$this->remate_4->EditAttrs["class"] = "form-control";
			$this->remate_4->EditCustomAttributes = "";
			if (trim(strval($this->remate_4->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_4->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_4->EditValue = $arwrk;

			// vivolink
			$this->vivolink->EditAttrs["class"] = "form-control";
			$this->vivolink->EditCustomAttributes = "";
			$this->vivolink->EditValue = ew_HtmlEncode($this->vivolink->CurrentValue);
			$this->vivolink->PlaceHolder = ew_RemoveHtml($this->vivolink->FldCaption());

			// Add refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->ViewCustomAttributes = "";

			// activa
			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// fecha_desde
			$this->fecha_desde->EditAttrs["class"] = "form-control";
			$this->fecha_desde->EditCustomAttributes = "";
			$this->fecha_desde->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_desde->CurrentValue, 8));
			$this->fecha_desde->PlaceHolder = ew_RemoveHtml($this->fecha_desde->FldCaption());

			// fecha_hasta
			$this->fecha_hasta->EditAttrs["class"] = "form-control";
			$this->fecha_hasta->EditCustomAttributes = "";
			$this->fecha_hasta->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_hasta->CurrentValue, 8));
			$this->fecha_hasta->PlaceHolder = ew_RemoveHtml($this->fecha_hasta->FldCaption());

			// remate_1
			$this->remate_1->EditAttrs["class"] = "form-control";
			$this->remate_1->EditCustomAttributes = "";
			if (trim(strval($this->remate_1->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_1->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_1->EditValue = $arwrk;

			// remate_2
			$this->remate_2->EditAttrs["class"] = "form-control";
			$this->remate_2->EditCustomAttributes = "";
			if (trim(strval($this->remate_2->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_2->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_2->EditValue = $arwrk;

			// remate_3
			$this->remate_3->EditAttrs["class"] = "form-control";
			$this->remate_3->EditCustomAttributes = "";
			if (trim(strval($this->remate_3->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_3->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_3->EditValue = $arwrk;

			// remate_4
			$this->remate_4->EditAttrs["class"] = "form-control";
			$this->remate_4->EditCustomAttributes = "";
			if (trim(strval($this->remate_4->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_4->LookupFilters = array("df2" => "0");
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$rowswrk = count($arwrk);
			for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
				$arwrk[$rowcntwrk][2] = ew_FormatDateTime($arwrk[$rowcntwrk][2], 0);
			}
			$this->remate_4->EditValue = $arwrk;

			// vivolink
			$this->vivolink->EditAttrs["class"] = "form-control";
			$this->vivolink->EditCustomAttributes = "";
			$this->vivolink->EditValue = ew_HtmlEncode($this->vivolink->CurrentValue);
			$this->vivolink->PlaceHolder = ew_RemoveHtml($this->vivolink->FldCaption());

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";

			// fecha_desde
			$this->fecha_desde->LinkCustomAttributes = "";
			$this->fecha_desde->HrefValue = "";

			// fecha_hasta
			$this->fecha_hasta->LinkCustomAttributes = "";
			$this->fecha_hasta->HrefValue = "";

			// remate_1
			$this->remate_1->LinkCustomAttributes = "";
			$this->remate_1->HrefValue = "";

			// remate_2
			$this->remate_2->LinkCustomAttributes = "";
			$this->remate_2->HrefValue = "";

			// remate_3
			$this->remate_3->LinkCustomAttributes = "";
			$this->remate_3->HrefValue = "";

			// remate_4
			$this->remate_4->LinkCustomAttributes = "";
			$this->remate_4->HrefValue = "";

			// vivolink
			$this->vivolink->LinkCustomAttributes = "";
			$this->vivolink->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditCustomAttributes = "";

			// activa
			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// fecha_desde
			$this->fecha_desde->EditAttrs["class"] = "form-control";
			$this->fecha_desde->EditCustomAttributes = "";
			$this->fecha_desde->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha_desde->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha_desde->PlaceHolder = ew_RemoveHtml($this->fecha_desde->FldCaption());

			// fecha_hasta
			$this->fecha_hasta->EditAttrs["class"] = "form-control";
			$this->fecha_hasta->EditCustomAttributes = "";
			$this->fecha_hasta->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha_hasta->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha_hasta->PlaceHolder = ew_RemoveHtml($this->fecha_hasta->FldCaption());

			// remate_1
			$this->remate_1->EditAttrs["class"] = "form-control";
			$this->remate_1->EditCustomAttributes = "";

			// remate_2
			$this->remate_2->EditAttrs["class"] = "form-control";
			$this->remate_2->EditCustomAttributes = "";

			// remate_3
			$this->remate_3->EditAttrs["class"] = "form-control";
			$this->remate_3->EditCustomAttributes = "";

			// remate_4
			$this->remate_4->EditAttrs["class"] = "form-control";
			$this->remate_4->EditCustomAttributes = "";

			// vivolink
			$this->vivolink->EditAttrs["class"] = "form-control";
			$this->vivolink->EditCustomAttributes = "";
			$this->vivolink->EditValue = ew_HtmlEncode($this->vivolink->AdvancedSearch->SearchValue);
			$this->vivolink->PlaceHolder = ew_RemoveHtml($this->vivolink->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->fecha_desde->FldIsDetailKey && !is_null($this->fecha_desde->FormValue) && $this->fecha_desde->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha_desde->FldCaption(), $this->fecha_desde->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha_desde->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_desde->FldErrMsg());
		}
		if (!$this->fecha_hasta->FldIsDetailKey && !is_null($this->fecha_hasta->FormValue) && $this->fecha_hasta->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha_hasta->FldCaption(), $this->fecha_hasta->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha_hasta->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_hasta->FldErrMsg());
		}
		if (!$this->remate_1->FldIsDetailKey && !is_null($this->remate_1->FormValue) && $this->remate_1->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->remate_1->FldCaption(), $this->remate_1->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// activa
			$this->activa->SetDbValueDef($rsnew, $this->activa->CurrentValue, NULL, $this->activa->ReadOnly);

			// fecha_desde
			$this->fecha_desde->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0), ew_CurrentDate(), $this->fecha_desde->ReadOnly);

			// fecha_hasta
			$this->fecha_hasta->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0), ew_CurrentDate(), $this->fecha_hasta->ReadOnly);

			// remate_1
			$this->remate_1->SetDbValueDef($rsnew, $this->remate_1->CurrentValue, 0, $this->remate_1->ReadOnly);

			// remate_2
			$this->remate_2->SetDbValueDef($rsnew, $this->remate_2->CurrentValue, NULL, $this->remate_2->ReadOnly);

			// remate_3
			$this->remate_3->SetDbValueDef($rsnew, $this->remate_3->CurrentValue, NULL, $this->remate_3->ReadOnly);

			// remate_4
			$this->remate_4->SetDbValueDef($rsnew, $this->remate_4->CurrentValue, NULL, $this->remate_4->ReadOnly);

			// vivolink
			$this->vivolink->SetDbValueDef($rsnew, $this->vivolink->CurrentValue, NULL, $this->vivolink->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// activa
		$this->activa->SetDbValueDef($rsnew, $this->activa->CurrentValue, NULL, FALSE);

		// fecha_desde
		$this->fecha_desde->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_desde->CurrentValue, 0), ew_CurrentDate(), FALSE);

		// fecha_hasta
		$this->fecha_hasta->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_hasta->CurrentValue, 0), ew_CurrentDate(), FALSE);

		// remate_1
		$this->remate_1->SetDbValueDef($rsnew, $this->remate_1->CurrentValue, 0, FALSE);

		// remate_2
		$this->remate_2->SetDbValueDef($rsnew, $this->remate_2->CurrentValue, NULL, FALSE);

		// remate_3
		$this->remate_3->SetDbValueDef($rsnew, $this->remate_3->CurrentValue, NULL, FALSE);

		// remate_4
		$this->remate_4->SetDbValueDef($rsnew, $this->remate_4->CurrentValue, NULL, FALSE);

		// vivolink
		$this->vivolink->SetDbValueDef($rsnew, $this->vivolink->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->id->setDbValue($conn->Insert_ID());
				$rsnew['id'] = $this->id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->activa->AdvancedSearch->Load();
		$this->fecha_desde->AdvancedSearch->Load();
		$this->fecha_hasta->AdvancedSearch->Load();
		$this->remate_1->AdvancedSearch->Load();
		$this->remate_2->AdvancedSearch->Load();
		$this->remate_3->AdvancedSearch->Load();
		$this->remate_4->AdvancedSearch->Load();
		$this->vivolink->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_home_pautas\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_home_pautas',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.fhome_pautaslist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
		case "x_remate_1":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_1->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_2":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_2->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_3":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_3->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_remate_4":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_4->LookupFilters = array("df2" => "0");
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `numero` DESC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		if ($pageId == "list") {
			switch ($fld->FldVar) {
			}
		} elseif ($pageId == "extbs") {
			switch ($fld->FldVar) {
			}
		} 
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($home_pautas_list)) $home_pautas_list = new chome_pautas_list();

// Page init
$home_pautas_list->Page_Init();

// Page main
$home_pautas_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$home_pautas_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($home_pautas->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = fhome_pautaslist = new ew_Form("fhome_pautaslist", "list");
fhome_pautaslist.FormKeyCountName = '<?php echo $home_pautas_list->FormKeyCountName ?>';

// Validate form
fhome_pautaslist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_fecha_desde");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->fecha_desde->FldCaption(), $home_pautas->fecha_desde->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha_desde");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($home_pautas->fecha_desde->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha_hasta");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->fecha_hasta->FldCaption(), $home_pautas->fecha_hasta->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha_hasta");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($home_pautas->fecha_hasta->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_remate_1");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $home_pautas->remate_1->FldCaption(), $home_pautas->remate_1->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
fhome_pautaslist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "activa", false)) return false;
	if (ew_ValueChanged(fobj, infix, "fecha_desde", false)) return false;
	if (ew_ValueChanged(fobj, infix, "fecha_hasta", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remate_1", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remate_2", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remate_3", false)) return false;
	if (ew_ValueChanged(fobj, infix, "remate_4", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vivolink", false)) return false;
	return true;
}

// Form_CustomValidate event
fhome_pautaslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fhome_pautaslist.ValidateRequired = true;
<?php } else { ?>
fhome_pautaslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fhome_pautaslist.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fhome_pautaslist.Lists["x_activa"].Options = <?php echo json_encode($home_pautas->activa->Options()) ?>;
fhome_pautaslist.Lists["x_remate_1"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautaslist.Lists["x_remate_2"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautaslist.Lists["x_remate_3"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
fhome_pautaslist.Lists["x_remate_4"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","x_fecha","x_lugar",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};

// Form object for search
var CurrentSearchForm = fhome_pautaslistsrch = new ew_Form("fhome_pautaslistsrch");

// Validate function for search
fhome_pautaslistsrch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}

// Form_CustomValidate event
fhome_pautaslistsrch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fhome_pautaslistsrch.ValidateRequired = true; // Use JavaScript validation
<?php } else { ?>
fhome_pautaslistsrch.ValidateRequired = false; // No JavaScript validation
<?php } ?>

// Dynamic selection lists
fhome_pautaslistsrch.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fhome_pautaslistsrch.Lists["x_activa"].Options = <?php echo json_encode($home_pautas->activa->Options()) ?>;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($home_pautas->Export == "") { ?>
<div class="ewToolbar">
<?php if ($home_pautas->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($home_pautas_list->TotalRecs > 0 && $home_pautas_list->ExportOptions->Visible()) { ?>
<?php $home_pautas_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($home_pautas_list->SearchOptions->Visible()) { ?>
<?php $home_pautas_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($home_pautas_list->FilterOptions->Visible()) { ?>
<?php $home_pautas_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($home_pautas->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
if ($home_pautas->CurrentAction == "gridadd") {
	$home_pautas->CurrentFilter = "0=1";
	$home_pautas_list->StartRec = 1;
	$home_pautas_list->DisplayRecs = $home_pautas->GridAddRowCount;
	$home_pautas_list->TotalRecs = $home_pautas_list->DisplayRecs;
	$home_pautas_list->StopRec = $home_pautas_list->DisplayRecs;
} else {
	$bSelectLimit = $home_pautas_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($home_pautas_list->TotalRecs <= 0)
			$home_pautas_list->TotalRecs = $home_pautas->SelectRecordCount();
	} else {
		if (!$home_pautas_list->Recordset && ($home_pautas_list->Recordset = $home_pautas_list->LoadRecordset()))
			$home_pautas_list->TotalRecs = $home_pautas_list->Recordset->RecordCount();
	}
	$home_pautas_list->StartRec = 1;
	if ($home_pautas_list->DisplayRecs <= 0 || ($home_pautas->Export <> "" && $home_pautas->ExportAll)) // Display all records
		$home_pautas_list->DisplayRecs = $home_pautas_list->TotalRecs;
	if (!($home_pautas->Export <> "" && $home_pautas->ExportAll))
		$home_pautas_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$home_pautas_list->Recordset = $home_pautas_list->LoadRecordset($home_pautas_list->StartRec-1, $home_pautas_list->DisplayRecs);

	// Set no record found message
	if ($home_pautas->CurrentAction == "" && $home_pautas_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$home_pautas_list->setWarningMessage(ew_DeniedMsg());
		if ($home_pautas_list->SearchWhere == "0=101")
			$home_pautas_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$home_pautas_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$home_pautas_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($home_pautas->Export == "" && $home_pautas->CurrentAction == "") { ?>
<form name="fhome_pautaslistsrch" id="fhome_pautaslistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($home_pautas_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="fhome_pautaslistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="home_pautas">
	<div class="ewBasicSearch">
<?php
if ($gsSearchError == "")
	$home_pautas_list->LoadAdvancedSearch(); // Load advanced search

// Render for search
$home_pautas->RowType = EW_ROWTYPE_SEARCH;

// Render row
$home_pautas->ResetAttrs();
$home_pautas_list->RenderRow();
?>
<div id="xsr_1" class="ewRow">
<?php if ($home_pautas->activa->Visible) { // activa ?>
	<div id="xsc_activa" class="ewCell form-group">
		<label for="x_activa" class="ewSearchCaption ewLabel"><?php echo $home_pautas->activa->FldCaption() ?></label>
		<span class="ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_activa" id="z_activa" value="="></span>
		<span class="ewSearchField">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x_activa" name="x_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x_activa") ?>
</select>
</span>
	</div>
<?php } ?>
</div>
<div id="xsr_2" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($home_pautas_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($home_pautas_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $home_pautas_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($home_pautas_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($home_pautas_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($home_pautas_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($home_pautas_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $home_pautas_list->ShowPageHeader(); ?>
<?php
$home_pautas_list->ShowMessage();
?>
<?php if ($home_pautas_list->TotalRecs > 0 || $home_pautas->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid home_pautas">
<?php if ($home_pautas->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($home_pautas->CurrentAction <> "gridadd" && $home_pautas->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($home_pautas_list->Pager)) $home_pautas_list->Pager = new cPrevNextPager($home_pautas_list->StartRec, $home_pautas_list->DisplayRecs, $home_pautas_list->TotalRecs) ?>
<?php if ($home_pautas_list->Pager->RecordCount > 0 && $home_pautas_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($home_pautas_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($home_pautas_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $home_pautas_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($home_pautas_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($home_pautas_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $home_pautas_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $home_pautas_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $home_pautas_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $home_pautas_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($home_pautas_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="fhome_pautaslist" id="fhome_pautaslist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($home_pautas_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $home_pautas_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="home_pautas">
<div id="gmp_home_pautas" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($home_pautas_list->TotalRecs > 0 || $home_pautas->CurrentAction == "add" || $home_pautas->CurrentAction == "copy" || $home_pautas->CurrentAction == "gridedit") { ?>
<table id="tbl_home_pautaslist" class="table ewTable">
<?php echo $home_pautas->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$home_pautas_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$home_pautas_list->RenderListOptions();

// Render list options (header, left)
$home_pautas_list->ListOptions->Render("header", "left");
?>
<?php if ($home_pautas->id->Visible) { // id ?>
	<?php if ($home_pautas->SortUrl($home_pautas->id) == "") { ?>
		<th data-name="id"><div id="elh_home_pautas_id" class="home_pautas_id"><div class="ewTableHeaderCaption"><?php echo $home_pautas->id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->id) ?>',1);"><div id="elh_home_pautas_id" class="home_pautas_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->activa->Visible) { // activa ?>
	<?php if ($home_pautas->SortUrl($home_pautas->activa) == "") { ?>
		<th data-name="activa"><div id="elh_home_pautas_activa" class="home_pautas_activa"><div class="ewTableHeaderCaption"><?php echo $home_pautas->activa->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="activa"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->activa) ?>',1);"><div id="elh_home_pautas_activa" class="home_pautas_activa">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->activa->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->activa->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->activa->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
	<?php if ($home_pautas->SortUrl($home_pautas->fecha_desde) == "") { ?>
		<th data-name="fecha_desde"><div id="elh_home_pautas_fecha_desde" class="home_pautas_fecha_desde"><div class="ewTableHeaderCaption"><?php echo $home_pautas->fecha_desde->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="fecha_desde"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->fecha_desde) ?>',1);"><div id="elh_home_pautas_fecha_desde" class="home_pautas_fecha_desde">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->fecha_desde->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->fecha_desde->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->fecha_desde->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
	<?php if ($home_pautas->SortUrl($home_pautas->fecha_hasta) == "") { ?>
		<th data-name="fecha_hasta"><div id="elh_home_pautas_fecha_hasta" class="home_pautas_fecha_hasta"><div class="ewTableHeaderCaption"><?php echo $home_pautas->fecha_hasta->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="fecha_hasta"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->fecha_hasta) ?>',1);"><div id="elh_home_pautas_fecha_hasta" class="home_pautas_fecha_hasta">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->fecha_hasta->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->fecha_hasta->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->fecha_hasta->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
	<?php if ($home_pautas->SortUrl($home_pautas->remate_1) == "") { ?>
		<th data-name="remate_1"><div id="elh_home_pautas_remate_1" class="home_pautas_remate_1"><div class="ewTableHeaderCaption"><?php echo $home_pautas->remate_1->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remate_1"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->remate_1) ?>',1);"><div id="elh_home_pautas_remate_1" class="home_pautas_remate_1">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->remate_1->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->remate_1->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->remate_1->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
	<?php if ($home_pautas->SortUrl($home_pautas->remate_2) == "") { ?>
		<th data-name="remate_2"><div id="elh_home_pautas_remate_2" class="home_pautas_remate_2"><div class="ewTableHeaderCaption"><?php echo $home_pautas->remate_2->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remate_2"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->remate_2) ?>',1);"><div id="elh_home_pautas_remate_2" class="home_pautas_remate_2">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->remate_2->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->remate_2->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->remate_2->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
	<?php if ($home_pautas->SortUrl($home_pautas->remate_3) == "") { ?>
		<th data-name="remate_3"><div id="elh_home_pautas_remate_3" class="home_pautas_remate_3"><div class="ewTableHeaderCaption"><?php echo $home_pautas->remate_3->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remate_3"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->remate_3) ?>',1);"><div id="elh_home_pautas_remate_3" class="home_pautas_remate_3">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->remate_3->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->remate_3->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->remate_3->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
	<?php if ($home_pautas->SortUrl($home_pautas->remate_4) == "") { ?>
		<th data-name="remate_4"><div id="elh_home_pautas_remate_4" class="home_pautas_remate_4"><div class="ewTableHeaderCaption"><?php echo $home_pautas->remate_4->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="remate_4"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->remate_4) ?>',1);"><div id="elh_home_pautas_remate_4" class="home_pautas_remate_4">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->remate_4->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->remate_4->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->remate_4->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
	<?php if ($home_pautas->SortUrl($home_pautas->vivolink) == "") { ?>
		<th data-name="vivolink"><div id="elh_home_pautas_vivolink" class="home_pautas_vivolink"><div class="ewTableHeaderCaption"><?php echo $home_pautas->vivolink->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="vivolink"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $home_pautas->SortUrl($home_pautas->vivolink) ?>',1);"><div id="elh_home_pautas_vivolink" class="home_pautas_vivolink">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $home_pautas->vivolink->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($home_pautas->vivolink->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($home_pautas->vivolink->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$home_pautas_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
	if ($home_pautas->CurrentAction == "add" || $home_pautas->CurrentAction == "copy") {
		$home_pautas_list->RowIndex = 0;
		$home_pautas_list->KeyCount = $home_pautas_list->RowIndex;
		if ($home_pautas->CurrentAction == "copy" && !$home_pautas_list->LoadRow())
				$home_pautas->CurrentAction = "add";
		if ($home_pautas->CurrentAction == "add")
			$home_pautas_list->LoadDefaultValues();
		if ($home_pautas->EventCancelled) // Insert failed
			$home_pautas_list->RestoreFormValues(); // Restore form values

		// Set row properties
		$home_pautas->ResetAttrs();
		$home_pautas->RowAttrs = array_merge($home_pautas->RowAttrs, array('data-rowindex'=>0, 'id'=>'r0_home_pautas', 'data-rowtype'=>EW_ROWTYPE_ADD));
		$home_pautas->RowType = EW_ROWTYPE_ADD;

		// Render row
		$home_pautas_list->RenderRow();

		// Render list options
		$home_pautas_list->RenderListOptions();
		$home_pautas_list->StartRowCnt = 0;
?>
	<tr<?php echo $home_pautas->RowAttributes() ?>>
<?php

// Render list options (body, left)
$home_pautas_list->ListOptions->Render("body", "left", $home_pautas_list->RowCnt);
?>
	<?php if ($home_pautas->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="home_pautas" data-field="x_id" name="o<?php echo $home_pautas_list->RowIndex ?>_id[]" id="o<?php echo $home_pautas_list->RowIndex ?>_id[]" value="<?php echo ew_HtmlEncode($home_pautas->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->activa->Visible) { // activa ?>
		<td data-name="activa">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_activa" class="form-group home_pautas_activa">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_activa" name="x<?php echo $home_pautas_list->RowIndex ?>_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_activa") ?>
</select>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_activa" name="o<?php echo $home_pautas_list->RowIndex ?>_activa" id="o<?php echo $home_pautas_list->RowIndex ?>_activa" value="<?php echo ew_HtmlEncode($home_pautas->activa->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
		<td data-name="fecha_desde">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_desde" class="form-group home_pautas_fecha_desde">
<input type="text" data-table="home_pautas" data-field="x_fecha_desde" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_desde->EditValue ?>"<?php echo $home_pautas->fecha_desde->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_desde->ReadOnly && !$home_pautas->fecha_desde->Disabled && !isset($home_pautas->fecha_desde->EditAttrs["readonly"]) && !isset($home_pautas->fecha_desde->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_desde" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" value="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
		<td data-name="fecha_hasta">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_hasta" class="form-group home_pautas_fecha_hasta">
<input type="text" data-table="home_pautas" data-field="x_fecha_hasta" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_hasta->EditValue ?>"<?php echo $home_pautas->fecha_hasta->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_hasta->ReadOnly && !$home_pautas->fecha_hasta->Disabled && !isset($home_pautas->fecha_hasta->EditAttrs["readonly"]) && !isset($home_pautas->fecha_hasta->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_hasta" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" value="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
		<td data-name="remate_1">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_1" class="form-group home_pautas_remate_1">
<select data-table="home_pautas" data-field="x_remate_1" data-value-separator="<?php echo $home_pautas->remate_1->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_1" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_1"<?php echo $home_pautas->remate_1->EditAttributes() ?>>
<?php echo $home_pautas->remate_1->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_1") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo $home_pautas->remate_1->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_1" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo ew_HtmlEncode($home_pautas->remate_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
		<td data-name="remate_2">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_2" class="form-group home_pautas_remate_2">
<select data-table="home_pautas" data-field="x_remate_2" data-value-separator="<?php echo $home_pautas->remate_2->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_2" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_2"<?php echo $home_pautas->remate_2->EditAttributes() ?>>
<?php echo $home_pautas->remate_2->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_2") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo $home_pautas->remate_2->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_2" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo ew_HtmlEncode($home_pautas->remate_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
		<td data-name="remate_3">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_3" class="form-group home_pautas_remate_3">
<select data-table="home_pautas" data-field="x_remate_3" data-value-separator="<?php echo $home_pautas->remate_3->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_3" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_3"<?php echo $home_pautas->remate_3->EditAttributes() ?>>
<?php echo $home_pautas->remate_3->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_3") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo $home_pautas->remate_3->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_3" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo ew_HtmlEncode($home_pautas->remate_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
		<td data-name="remate_4">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_4" class="form-group home_pautas_remate_4">
<select data-table="home_pautas" data-field="x_remate_4" data-value-separator="<?php echo $home_pautas->remate_4->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_4" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_4"<?php echo $home_pautas->remate_4->EditAttributes() ?>>
<?php echo $home_pautas->remate_4->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_4") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo $home_pautas->remate_4->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_4" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo ew_HtmlEncode($home_pautas->remate_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
		<td data-name="vivolink">
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_vivolink" class="form-group home_pautas_vivolink">
<input type="text" data-table="home_pautas" data-field="x_vivolink" name="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($home_pautas->vivolink->getPlaceHolder()) ?>" value="<?php echo $home_pautas->vivolink->EditValue ?>"<?php echo $home_pautas->vivolink->EditAttributes() ?>>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_vivolink" name="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" value="<?php echo ew_HtmlEncode($home_pautas->vivolink->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$home_pautas_list->ListOptions->Render("body", "right", $home_pautas_list->RowCnt);
?>
<script type="text/javascript">
fhome_pautaslist.UpdateOpts(<?php echo $home_pautas_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
<?php
if ($home_pautas->ExportAll && $home_pautas->Export <> "") {
	$home_pautas_list->StopRec = $home_pautas_list->TotalRecs;
} else {

	// Set the last record to display
	if ($home_pautas_list->TotalRecs > $home_pautas_list->StartRec + $home_pautas_list->DisplayRecs - 1)
		$home_pautas_list->StopRec = $home_pautas_list->StartRec + $home_pautas_list->DisplayRecs - 1;
	else
		$home_pautas_list->StopRec = $home_pautas_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($home_pautas_list->FormKeyCountName) && ($home_pautas->CurrentAction == "gridadd" || $home_pautas->CurrentAction == "gridedit" || $home_pautas->CurrentAction == "F")) {
		$home_pautas_list->KeyCount = $objForm->GetValue($home_pautas_list->FormKeyCountName);
		$home_pautas_list->StopRec = $home_pautas_list->StartRec + $home_pautas_list->KeyCount - 1;
	}
}
$home_pautas_list->RecCnt = $home_pautas_list->StartRec - 1;
if ($home_pautas_list->Recordset && !$home_pautas_list->Recordset->EOF) {
	$home_pautas_list->Recordset->MoveFirst();
	$bSelectLimit = $home_pautas_list->UseSelectLimit;
	if (!$bSelectLimit && $home_pautas_list->StartRec > 1)
		$home_pautas_list->Recordset->Move($home_pautas_list->StartRec - 1);
} elseif (!$home_pautas->AllowAddDeleteRow && $home_pautas_list->StopRec == 0) {
	$home_pautas_list->StopRec = $home_pautas->GridAddRowCount;
}

// Initialize aggregate
$home_pautas->RowType = EW_ROWTYPE_AGGREGATEINIT;
$home_pautas->ResetAttrs();
$home_pautas_list->RenderRow();
$home_pautas_list->EditRowCnt = 0;
if ($home_pautas->CurrentAction == "edit")
	$home_pautas_list->RowIndex = 1;
if ($home_pautas->CurrentAction == "gridadd")
	$home_pautas_list->RowIndex = 0;
if ($home_pautas->CurrentAction == "gridedit")
	$home_pautas_list->RowIndex = 0;
while ($home_pautas_list->RecCnt < $home_pautas_list->StopRec) {
	$home_pautas_list->RecCnt++;
	if (intval($home_pautas_list->RecCnt) >= intval($home_pautas_list->StartRec)) {
		$home_pautas_list->RowCnt++;
		if ($home_pautas->CurrentAction == "gridadd" || $home_pautas->CurrentAction == "gridedit" || $home_pautas->CurrentAction == "F") {
			$home_pautas_list->RowIndex++;
			$objForm->Index = $home_pautas_list->RowIndex;
			if ($objForm->HasValue($home_pautas_list->FormActionName))
				$home_pautas_list->RowAction = strval($objForm->GetValue($home_pautas_list->FormActionName));
			elseif ($home_pautas->CurrentAction == "gridadd")
				$home_pautas_list->RowAction = "insert";
			else
				$home_pautas_list->RowAction = "";
		}

		// Set up key count
		$home_pautas_list->KeyCount = $home_pautas_list->RowIndex;

		// Init row class and style
		$home_pautas->ResetAttrs();
		$home_pautas->CssClass = "";
		if ($home_pautas->CurrentAction == "gridadd") {
			$home_pautas_list->LoadDefaultValues(); // Load default values
		} else {
			$home_pautas_list->LoadRowValues($home_pautas_list->Recordset); // Load row values
		}
		$home_pautas->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($home_pautas->CurrentAction == "gridadd") // Grid add
			$home_pautas->RowType = EW_ROWTYPE_ADD; // Render add
		if ($home_pautas->CurrentAction == "gridadd" && $home_pautas->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$home_pautas_list->RestoreCurrentRowFormValues($home_pautas_list->RowIndex); // Restore form values
		if ($home_pautas->CurrentAction == "edit") {
			if ($home_pautas_list->CheckInlineEditKey() && $home_pautas_list->EditRowCnt == 0) { // Inline edit
				$home_pautas->RowType = EW_ROWTYPE_EDIT; // Render edit
			}
		}
		if ($home_pautas->CurrentAction == "gridedit") { // Grid edit
			if ($home_pautas->EventCancelled) {
				$home_pautas_list->RestoreCurrentRowFormValues($home_pautas_list->RowIndex); // Restore form values
			}
			if ($home_pautas_list->RowAction == "insert")
				$home_pautas->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$home_pautas->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($home_pautas->CurrentAction == "edit" && $home_pautas->RowType == EW_ROWTYPE_EDIT && $home_pautas->EventCancelled) { // Update failed
			$objForm->Index = 1;
			$home_pautas_list->RestoreFormValues(); // Restore form values
		}
		if ($home_pautas->CurrentAction == "gridedit" && ($home_pautas->RowType == EW_ROWTYPE_EDIT || $home_pautas->RowType == EW_ROWTYPE_ADD) && $home_pautas->EventCancelled) // Update failed
			$home_pautas_list->RestoreCurrentRowFormValues($home_pautas_list->RowIndex); // Restore form values
		if ($home_pautas->RowType == EW_ROWTYPE_EDIT) // Edit row
			$home_pautas_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$home_pautas->RowAttrs = array_merge($home_pautas->RowAttrs, array('data-rowindex'=>$home_pautas_list->RowCnt, 'id'=>'r' . $home_pautas_list->RowCnt . '_home_pautas', 'data-rowtype'=>$home_pautas->RowType));

		// Render row
		$home_pautas_list->RenderRow();

		// Render list options
		$home_pautas_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($home_pautas_list->RowAction <> "delete" && $home_pautas_list->RowAction <> "insertdelete" && !($home_pautas_list->RowAction == "insert" && $home_pautas->CurrentAction == "F" && $home_pautas_list->EmptyRow())) {
?>
	<tr<?php echo $home_pautas->RowAttributes() ?>>
<?php

// Render list options (body, left)
$home_pautas_list->ListOptions->Render("body", "left", $home_pautas_list->RowCnt);
?>
	<?php if ($home_pautas->id->Visible) { // id ?>
		<td data-name="id"<?php echo $home_pautas->id->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="home_pautas" data-field="x_id" name="o<?php echo $home_pautas_list->RowIndex ?>_id[]" id="o<?php echo $home_pautas_list->RowIndex ?>_id[]" value="<?php echo ew_HtmlEncode($home_pautas->id->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_id" class="form-group home_pautas_id">
<span<?php echo $home_pautas->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $home_pautas->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_id" name="x<?php echo $home_pautas_list->RowIndex ?>_id" id="x<?php echo $home_pautas_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($home_pautas->id->CurrentValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_id" class="home_pautas_id">
<span<?php echo $home_pautas->id->ViewAttributes() ?>>
<?php echo $home_pautas->id->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $home_pautas_list->PageObjName . "_row_" . $home_pautas_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($home_pautas->activa->Visible) { // activa ?>
		<td data-name="activa"<?php echo $home_pautas->activa->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_activa" class="form-group home_pautas_activa">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_activa" name="x<?php echo $home_pautas_list->RowIndex ?>_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_activa") ?>
</select>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_activa" name="o<?php echo $home_pautas_list->RowIndex ?>_activa" id="o<?php echo $home_pautas_list->RowIndex ?>_activa" value="<?php echo ew_HtmlEncode($home_pautas->activa->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_activa" class="form-group home_pautas_activa">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_activa" name="x<?php echo $home_pautas_list->RowIndex ?>_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_activa") ?>
</select>
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_activa" class="home_pautas_activa">
<span<?php echo $home_pautas->activa->ViewAttributes() ?>>
<?php echo $home_pautas->activa->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
		<td data-name="fecha_desde"<?php echo $home_pautas->fecha_desde->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_desde" class="form-group home_pautas_fecha_desde">
<input type="text" data-table="home_pautas" data-field="x_fecha_desde" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_desde->EditValue ?>"<?php echo $home_pautas->fecha_desde->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_desde->ReadOnly && !$home_pautas->fecha_desde->Disabled && !isset($home_pautas->fecha_desde->EditAttrs["readonly"]) && !isset($home_pautas->fecha_desde->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_desde" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" value="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_desde" class="form-group home_pautas_fecha_desde">
<input type="text" data-table="home_pautas" data-field="x_fecha_desde" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_desde->EditValue ?>"<?php echo $home_pautas->fecha_desde->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_desde->ReadOnly && !$home_pautas->fecha_desde->Disabled && !isset($home_pautas->fecha_desde->EditAttrs["readonly"]) && !isset($home_pautas->fecha_desde->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde", 0);
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_desde" class="home_pautas_fecha_desde">
<span<?php echo $home_pautas->fecha_desde->ViewAttributes() ?>>
<?php echo $home_pautas->fecha_desde->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
		<td data-name="fecha_hasta"<?php echo $home_pautas->fecha_hasta->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_hasta" class="form-group home_pautas_fecha_hasta">
<input type="text" data-table="home_pautas" data-field="x_fecha_hasta" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_hasta->EditValue ?>"<?php echo $home_pautas->fecha_hasta->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_hasta->ReadOnly && !$home_pautas->fecha_hasta->Disabled && !isset($home_pautas->fecha_hasta->EditAttrs["readonly"]) && !isset($home_pautas->fecha_hasta->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_hasta" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" value="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_hasta" class="form-group home_pautas_fecha_hasta">
<input type="text" data-table="home_pautas" data-field="x_fecha_hasta" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_hasta->EditValue ?>"<?php echo $home_pautas->fecha_hasta->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_hasta->ReadOnly && !$home_pautas->fecha_hasta->Disabled && !isset($home_pautas->fecha_hasta->EditAttrs["readonly"]) && !isset($home_pautas->fecha_hasta->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta", 0);
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_fecha_hasta" class="home_pautas_fecha_hasta">
<span<?php echo $home_pautas->fecha_hasta->ViewAttributes() ?>>
<?php echo $home_pautas->fecha_hasta->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
		<td data-name="remate_1"<?php echo $home_pautas->remate_1->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_1" class="form-group home_pautas_remate_1">
<select data-table="home_pautas" data-field="x_remate_1" data-value-separator="<?php echo $home_pautas->remate_1->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_1" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_1"<?php echo $home_pautas->remate_1->EditAttributes() ?>>
<?php echo $home_pautas->remate_1->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_1") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo $home_pautas->remate_1->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_1" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo ew_HtmlEncode($home_pautas->remate_1->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_1" class="form-group home_pautas_remate_1">
<select data-table="home_pautas" data-field="x_remate_1" data-value-separator="<?php echo $home_pautas->remate_1->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_1" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_1"<?php echo $home_pautas->remate_1->EditAttributes() ?>>
<?php echo $home_pautas->remate_1->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_1") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo $home_pautas->remate_1->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_1" class="home_pautas_remate_1">
<span<?php echo $home_pautas->remate_1->ViewAttributes() ?>>
<?php echo $home_pautas->remate_1->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
		<td data-name="remate_2"<?php echo $home_pautas->remate_2->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_2" class="form-group home_pautas_remate_2">
<select data-table="home_pautas" data-field="x_remate_2" data-value-separator="<?php echo $home_pautas->remate_2->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_2" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_2"<?php echo $home_pautas->remate_2->EditAttributes() ?>>
<?php echo $home_pautas->remate_2->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_2") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo $home_pautas->remate_2->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_2" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo ew_HtmlEncode($home_pautas->remate_2->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_2" class="form-group home_pautas_remate_2">
<select data-table="home_pautas" data-field="x_remate_2" data-value-separator="<?php echo $home_pautas->remate_2->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_2" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_2"<?php echo $home_pautas->remate_2->EditAttributes() ?>>
<?php echo $home_pautas->remate_2->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_2") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo $home_pautas->remate_2->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_2" class="home_pautas_remate_2">
<span<?php echo $home_pautas->remate_2->ViewAttributes() ?>>
<?php echo $home_pautas->remate_2->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
		<td data-name="remate_3"<?php echo $home_pautas->remate_3->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_3" class="form-group home_pautas_remate_3">
<select data-table="home_pautas" data-field="x_remate_3" data-value-separator="<?php echo $home_pautas->remate_3->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_3" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_3"<?php echo $home_pautas->remate_3->EditAttributes() ?>>
<?php echo $home_pautas->remate_3->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_3") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo $home_pautas->remate_3->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_3" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo ew_HtmlEncode($home_pautas->remate_3->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_3" class="form-group home_pautas_remate_3">
<select data-table="home_pautas" data-field="x_remate_3" data-value-separator="<?php echo $home_pautas->remate_3->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_3" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_3"<?php echo $home_pautas->remate_3->EditAttributes() ?>>
<?php echo $home_pautas->remate_3->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_3") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo $home_pautas->remate_3->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_3" class="home_pautas_remate_3">
<span<?php echo $home_pautas->remate_3->ViewAttributes() ?>>
<?php echo $home_pautas->remate_3->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
		<td data-name="remate_4"<?php echo $home_pautas->remate_4->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_4" class="form-group home_pautas_remate_4">
<select data-table="home_pautas" data-field="x_remate_4" data-value-separator="<?php echo $home_pautas->remate_4->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_4" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_4"<?php echo $home_pautas->remate_4->EditAttributes() ?>>
<?php echo $home_pautas->remate_4->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_4") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo $home_pautas->remate_4->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_4" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo ew_HtmlEncode($home_pautas->remate_4->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_4" class="form-group home_pautas_remate_4">
<select data-table="home_pautas" data-field="x_remate_4" data-value-separator="<?php echo $home_pautas->remate_4->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_4" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_4"<?php echo $home_pautas->remate_4->EditAttributes() ?>>
<?php echo $home_pautas->remate_4->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_4") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo $home_pautas->remate_4->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_remate_4" class="home_pautas_remate_4">
<span<?php echo $home_pautas->remate_4->ViewAttributes() ?>>
<?php echo $home_pautas->remate_4->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
		<td data-name="vivolink"<?php echo $home_pautas->vivolink->CellAttributes() ?>>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_vivolink" class="form-group home_pautas_vivolink">
<input type="text" data-table="home_pautas" data-field="x_vivolink" name="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($home_pautas->vivolink->getPlaceHolder()) ?>" value="<?php echo $home_pautas->vivolink->EditValue ?>"<?php echo $home_pautas->vivolink->EditAttributes() ?>>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_vivolink" name="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" value="<?php echo ew_HtmlEncode($home_pautas->vivolink->OldValue) ?>">
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_vivolink" class="form-group home_pautas_vivolink">
<input type="text" data-table="home_pautas" data-field="x_vivolink" name="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($home_pautas->vivolink->getPlaceHolder()) ?>" value="<?php echo $home_pautas->vivolink->EditValue ?>"<?php echo $home_pautas->vivolink->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($home_pautas->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $home_pautas_list->RowCnt ?>_home_pautas_vivolink" class="home_pautas_vivolink">
<span<?php echo $home_pautas->vivolink->ViewAttributes() ?>>
<?php echo $home_pautas->vivolink->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$home_pautas_list->ListOptions->Render("body", "right", $home_pautas_list->RowCnt);
?>
	</tr>
<?php if ($home_pautas->RowType == EW_ROWTYPE_ADD || $home_pautas->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fhome_pautaslist.UpdateOpts(<?php echo $home_pautas_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($home_pautas->CurrentAction <> "gridadd")
		if (!$home_pautas_list->Recordset->EOF) $home_pautas_list->Recordset->MoveNext();
}
?>
<?php
	if ($home_pautas->CurrentAction == "gridadd" || $home_pautas->CurrentAction == "gridedit") {
		$home_pautas_list->RowIndex = '$rowindex$';
		$home_pautas_list->LoadDefaultValues();

		// Set row properties
		$home_pautas->ResetAttrs();
		$home_pautas->RowAttrs = array_merge($home_pautas->RowAttrs, array('data-rowindex'=>$home_pautas_list->RowIndex, 'id'=>'r0_home_pautas', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($home_pautas->RowAttrs["class"], "ewTemplate");
		$home_pautas->RowType = EW_ROWTYPE_ADD;

		// Render row
		$home_pautas_list->RenderRow();

		// Render list options
		$home_pautas_list->RenderListOptions();
		$home_pautas_list->StartRowCnt = 0;
?>
	<tr<?php echo $home_pautas->RowAttributes() ?>>
<?php

// Render list options (body, left)
$home_pautas_list->ListOptions->Render("body", "left", $home_pautas_list->RowIndex);
?>
	<?php if ($home_pautas->id->Visible) { // id ?>
		<td data-name="id">
<input type="hidden" data-table="home_pautas" data-field="x_id" name="o<?php echo $home_pautas_list->RowIndex ?>_id[]" id="o<?php echo $home_pautas_list->RowIndex ?>_id[]" value="<?php echo ew_HtmlEncode($home_pautas->id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->activa->Visible) { // activa ?>
		<td data-name="activa">
<span id="el$rowindex$_home_pautas_activa" class="form-group home_pautas_activa">
<select data-table="home_pautas" data-field="x_activa" data-value-separator="<?php echo $home_pautas->activa->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_activa" name="x<?php echo $home_pautas_list->RowIndex ?>_activa"<?php echo $home_pautas->activa->EditAttributes() ?>>
<?php echo $home_pautas->activa->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_activa") ?>
</select>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_activa" name="o<?php echo $home_pautas_list->RowIndex ?>_activa" id="o<?php echo $home_pautas_list->RowIndex ?>_activa" value="<?php echo ew_HtmlEncode($home_pautas->activa->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_desde->Visible) { // fecha_desde ?>
		<td data-name="fecha_desde">
<span id="el$rowindex$_home_pautas_fecha_desde" class="form-group home_pautas_fecha_desde">
<input type="text" data-table="home_pautas" data-field="x_fecha_desde" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_desde->EditValue ?>"<?php echo $home_pautas->fecha_desde->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_desde->ReadOnly && !$home_pautas->fecha_desde->Disabled && !isset($home_pautas->fecha_desde->EditAttrs["readonly"]) && !isset($home_pautas->fecha_desde->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_desde", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_desde" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_desde" value="<?php echo ew_HtmlEncode($home_pautas->fecha_desde->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->fecha_hasta->Visible) { // fecha_hasta ?>
		<td data-name="fecha_hasta">
<span id="el$rowindex$_home_pautas_fecha_hasta" class="form-group home_pautas_fecha_hasta">
<input type="text" data-table="home_pautas" data-field="x_fecha_hasta" name="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->getPlaceHolder()) ?>" value="<?php echo $home_pautas->fecha_hasta->EditValue ?>"<?php echo $home_pautas->fecha_hasta->EditAttributes() ?>>
<?php if (!$home_pautas->fecha_hasta->ReadOnly && !$home_pautas->fecha_hasta->Disabled && !isset($home_pautas->fecha_hasta->EditAttrs["readonly"]) && !isset($home_pautas->fecha_hasta->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fhome_pautaslist", "x<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta", 0);
</script>
<?php } ?>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_fecha_hasta" name="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" id="o<?php echo $home_pautas_list->RowIndex ?>_fecha_hasta" value="<?php echo ew_HtmlEncode($home_pautas->fecha_hasta->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_1->Visible) { // remate_1 ?>
		<td data-name="remate_1">
<span id="el$rowindex$_home_pautas_remate_1" class="form-group home_pautas_remate_1">
<select data-table="home_pautas" data-field="x_remate_1" data-value-separator="<?php echo $home_pautas->remate_1->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_1" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_1"<?php echo $home_pautas->remate_1->EditAttributes() ?>>
<?php echo $home_pautas->remate_1->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_1") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo $home_pautas->remate_1->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_1" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_1" value="<?php echo ew_HtmlEncode($home_pautas->remate_1->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_2->Visible) { // remate_2 ?>
		<td data-name="remate_2">
<span id="el$rowindex$_home_pautas_remate_2" class="form-group home_pautas_remate_2">
<select data-table="home_pautas" data-field="x_remate_2" data-value-separator="<?php echo $home_pautas->remate_2->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_2" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_2"<?php echo $home_pautas->remate_2->EditAttributes() ?>>
<?php echo $home_pautas->remate_2->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_2") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo $home_pautas->remate_2->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_2" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_2" value="<?php echo ew_HtmlEncode($home_pautas->remate_2->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_3->Visible) { // remate_3 ?>
		<td data-name="remate_3">
<span id="el$rowindex$_home_pautas_remate_3" class="form-group home_pautas_remate_3">
<select data-table="home_pautas" data-field="x_remate_3" data-value-separator="<?php echo $home_pautas->remate_3->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_3" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_3"<?php echo $home_pautas->remate_3->EditAttributes() ?>>
<?php echo $home_pautas->remate_3->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_3") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo $home_pautas->remate_3->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_3" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_3" value="<?php echo ew_HtmlEncode($home_pautas->remate_3->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->remate_4->Visible) { // remate_4 ?>
		<td data-name="remate_4">
<span id="el$rowindex$_home_pautas_remate_4" class="form-group home_pautas_remate_4">
<select data-table="home_pautas" data-field="x_remate_4" data-value-separator="<?php echo $home_pautas->remate_4->DisplayValueSeparatorAttribute() ?>" id="x<?php echo $home_pautas_list->RowIndex ?>_remate_4" name="x<?php echo $home_pautas_list->RowIndex ?>_remate_4"<?php echo $home_pautas->remate_4->EditAttributes() ?>>
<?php echo $home_pautas->remate_4->SelectOptionListHtml("x<?php echo $home_pautas_list->RowIndex ?>_remate_4") ?>
</select>
<input type="hidden" name="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="s_x<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo $home_pautas->remate_4->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="home_pautas" data-field="x_remate_4" name="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" id="o<?php echo $home_pautas_list->RowIndex ?>_remate_4" value="<?php echo ew_HtmlEncode($home_pautas->remate_4->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($home_pautas->vivolink->Visible) { // vivolink ?>
		<td data-name="vivolink">
<span id="el$rowindex$_home_pautas_vivolink" class="form-group home_pautas_vivolink">
<input type="text" data-table="home_pautas" data-field="x_vivolink" name="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="x<?php echo $home_pautas_list->RowIndex ?>_vivolink" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($home_pautas->vivolink->getPlaceHolder()) ?>" value="<?php echo $home_pautas->vivolink->EditValue ?>"<?php echo $home_pautas->vivolink->EditAttributes() ?>>
</span>
<input type="hidden" data-table="home_pautas" data-field="x_vivolink" name="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" id="o<?php echo $home_pautas_list->RowIndex ?>_vivolink" value="<?php echo ew_HtmlEncode($home_pautas->vivolink->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$home_pautas_list->ListOptions->Render("body", "right", $home_pautas_list->RowCnt);
?>
<script type="text/javascript">
fhome_pautaslist.UpdateOpts(<?php echo $home_pautas_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($home_pautas->CurrentAction == "add" || $home_pautas->CurrentAction == "copy") { ?>
<input type="hidden" name="<?php echo $home_pautas_list->FormKeyCountName ?>" id="<?php echo $home_pautas_list->FormKeyCountName ?>" value="<?php echo $home_pautas_list->KeyCount ?>">
<?php } ?>
<?php if ($home_pautas->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $home_pautas_list->FormKeyCountName ?>" id="<?php echo $home_pautas_list->FormKeyCountName ?>" value="<?php echo $home_pautas_list->KeyCount ?>">
<?php echo $home_pautas_list->MultiSelectKey ?>
<?php } ?>
<?php if ($home_pautas->CurrentAction == "edit") { ?>
<input type="hidden" name="<?php echo $home_pautas_list->FormKeyCountName ?>" id="<?php echo $home_pautas_list->FormKeyCountName ?>" value="<?php echo $home_pautas_list->KeyCount ?>">
<?php } ?>
<?php if ($home_pautas->CurrentAction == "gridedit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $home_pautas_list->FormKeyCountName ?>" id="<?php echo $home_pautas_list->FormKeyCountName ?>" value="<?php echo $home_pautas_list->KeyCount ?>">
<?php echo $home_pautas_list->MultiSelectKey ?>
<?php } ?>
<?php if ($home_pautas->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($home_pautas_list->Recordset)
	$home_pautas_list->Recordset->Close();
?>
<?php if ($home_pautas->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($home_pautas->CurrentAction <> "gridadd" && $home_pautas->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($home_pautas_list->Pager)) $home_pautas_list->Pager = new cPrevNextPager($home_pautas_list->StartRec, $home_pautas_list->DisplayRecs, $home_pautas_list->TotalRecs) ?>
<?php if ($home_pautas_list->Pager->RecordCount > 0 && $home_pautas_list->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($home_pautas_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($home_pautas_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $home_pautas_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($home_pautas_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($home_pautas_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $home_pautas_list->PageUrl() ?>start=<?php echo $home_pautas_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $home_pautas_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $home_pautas_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $home_pautas_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $home_pautas_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($home_pautas_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($home_pautas_list->TotalRecs == 0 && $home_pautas->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($home_pautas_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($home_pautas->Export == "") { ?>
<script type="text/javascript">
fhome_pautaslistsrch.FilterList = <?php echo $home_pautas_list->GetFilterList() ?>;
fhome_pautaslistsrch.Init();
fhome_pautaslist.Init();
</script>
<?php } ?>
<?php
$home_pautas_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($home_pautas->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$home_pautas_list->Page_Terminate();
?>
