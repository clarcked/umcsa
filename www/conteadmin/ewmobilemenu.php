<!-- Begin Main Menu -->
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(33, "mmi_notiliniers", $Language->MenuPhrase("33", "MenuText"), "notilinierslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}notiliniers'), FALSE, FALSE);
$RootMenu->AddMenuItem(31, "mmi_home_pautas", $Language->MenuPhrase("31", "MenuText"), "home_pautaslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}home_pautas'), FALSE, FALSE);
$RootMenu->AddMenuItem(27, "mmi_videos_lotes", $Language->MenuPhrase("27", "MenuText"), "videos_loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}videos_lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(32, "mmi_conjuntos_lotes", $Language->MenuPhrase("32", "MenuText"), "conjuntos_loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}conjuntos_lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(4, "mmi_lotes", $Language->MenuPhrase("4", "MenuText"), "loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(6, "mmi_remates", $Language->MenuPhrase("6", "MenuText"), "remateslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}remates'), FALSE, FALSE);
$RootMenu->AddMenuItem(12, "mmi_tipos", $Language->MenuPhrase("12", "MenuText"), "tiposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}tipos'), FALSE, FALSE);
$RootMenu->AddMenuItem(10, "mmi_categorias_ganado", $Language->MenuPhrase("10", "MenuText"), "categorias_ganadolist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}categorias_ganado'), FALSE, FALSE);
$RootMenu->AddMenuItem(13, "mmi_usuarios", $Language->MenuPhrase("13", "MenuText"), "usuarioslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}usuarios'), FALSE, FALSE);
$RootMenu->AddMenuItem(16, "mmi_userlevelpermissions", $Language->MenuPhrase("16", "MenuText"), "userlevelpermissionslist.php", -1, "", (@$_SESSION[EW_SESSION_USER_LEVEL] & EW_ALLOW_ADMIN) == EW_ALLOW_ADMIN, FALSE, FALSE);
$RootMenu->AddMenuItem(17, "mmi_userlevels", $Language->MenuPhrase("17", "MenuText"), "userlevelslist.php", -1, "", (@$_SESSION[EW_SESSION_USER_LEVEL] & EW_ALLOW_ADMIN) == EW_ALLOW_ADMIN, FALSE, FALSE);
$RootMenu->AddMenuItem(34, "mmi_galerias_imagenes", $Language->MenuPhrase("34", "MenuText"), "galerias_imageneslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}galerias_imagenes'), FALSE, FALSE);
$RootMenu->AddMenuItem(9, "mmi_fotos", $Language->MenuPhrase("9", "MenuText"), "fotoslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}fotos'), FALSE, FALSE);
$RootMenu->AddMenuItem(5, "mmi_provincias", $Language->MenuPhrase("5", "MenuText"), "provinciaslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}provincias'), FALSE, FALSE);
$RootMenu->AddMenuItem(3, "mmi_localidades", $Language->MenuPhrase("3", "MenuText"), "localidadeslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}localidades'), FALSE, FALSE);
$RootMenu->AddMenuItem(26, "mmci_Exportar_Planilla", $Language->MenuPhrase("26", "MenuText"), "remaplanilla-prev.php", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(28, "mmi_staff", $Language->MenuPhrase("28", "MenuText"), "stafflist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff'), FALSE, FALSE);
$RootMenu->AddMenuItem(29, "mmi_staff_grupos", $Language->MenuPhrase("29", "MenuText"), "staff_gruposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff_grupos'), FALSE, FALSE);
$RootMenu->AddMenuItem(30, "mmi_staff_subgrupos", $Language->MenuPhrase("30", "MenuText"), "staff_subgruposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff_subgrupos'), FALSE, FALSE);
$RootMenu->AddMenuItem(-1, "mmi_logout", $Language->Phrase("Logout"), "logout.php", -1, "", IsLoggedIn());
$RootMenu->AddMenuItem(-1, "mmi_login", $Language->Phrase("Login"), "login.php", -1, "", !IsLoggedIn() && substr(@$_SERVER["URL"], -1 * strlen("login.php")) <> "login.php");
$RootMenu->Render();
?>
<!-- End Main Menu -->
