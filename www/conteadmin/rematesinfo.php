<?php

// Global variable for table object
$remates = NULL;

//
// Table class for remates
//
class cremates extends cTable {
	var $id;
	var $tipo;
	var $plataforma;
	var $ext_id;
	var $fecha;
	var $hora_inicio;
	var $hora_fin;
	var $zona;
	var $lugar;
	var $locacion;
	var $numero;
	var $titulo;
	var $cantidad;
	var $descripcion;
	var $yacare_visible;
	var $banner_g;
	var $banner_g_visible;
	var $banner_g_z_index;
	var $banner_g_pos_x;
	var $banner_g_pos_y;
	var $banner_g_link;
	var $banner_ch;
	var $activo;
	var $archivo;
	var $precios;
	var $mapa_img;
	var $video_final_servidor;
	var $video_final_1;
	var $video_titu_1;
	var $video_final_2;
	var $video_titu_2;
	var $video_final_3;
	var $video_titu_3;
	var $video_final_4;
	var $video_titu_4;
	var $video_final_5;
	var $video_titu_5;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'remates';
		$this->TableName = 'remates';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`remates`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = TRUE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 20;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('remates', 'remates', 'x_id', 'id', '`id`', '`id`', 19, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// tipo
		$this->tipo = new cField('remates', 'remates', 'x_tipo', 'tipo', '`tipo`', '`tipo`', 202, -1, FALSE, '`tipo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->tipo->Sortable = TRUE; // Allow sort
		$this->tipo->OptionCount = 2;
		$this->fields['tipo'] = &$this->tipo;

		// plataforma
		$this->plataforma = new cField('remates', 'remates', 'x_plataforma', 'plataforma', '`plataforma`', '`plataforma`', 202, -1, FALSE, '`plataforma`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->plataforma->Sortable = TRUE; // Allow sort
		$this->plataforma->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->plataforma->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->plataforma->OptionCount = 2;
		$this->fields['plataforma'] = &$this->plataforma;

		// ext_id
		$this->ext_id = new cField('remates', 'remates', 'x_ext_id', 'ext_id', '`ext_id`', '`ext_id`', 19, -1, FALSE, '`ext_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->ext_id->Sortable = TRUE; // Allow sort
		$this->ext_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['ext_id'] = &$this->ext_id;

		// fecha
		$this->fecha = new cField('remates', 'remates', 'x_fecha', 'fecha', '`fecha`', ew_CastDateFieldForLike('`fecha`', 0, "DB"), 133, 0, FALSE, '`fecha`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fecha->Sortable = TRUE; // Allow sort
		$this->fecha->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_DATE_FORMAT"], $Language->Phrase("IncorrectDate"));
		$this->fields['fecha'] = &$this->fecha;

		// hora_inicio
		$this->hora_inicio = new cField('remates', 'remates', 'x_hora_inicio', 'hora_inicio', '`hora_inicio`', ew_CastDateFieldForLike('`hora_inicio`', 0, "DB"), 134, -1, FALSE, '`hora_inicio`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->hora_inicio->Sortable = TRUE; // Allow sort
		$this->hora_inicio->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_TIME_SEPARATOR"], $Language->Phrase("IncorrectTime"));
		$this->fields['hora_inicio'] = &$this->hora_inicio;

		// hora_fin
		$this->hora_fin = new cField('remates', 'remates', 'x_hora_fin', 'hora_fin', '`hora_fin`', ew_CastDateFieldForLike('`hora_fin`', 0, "DB"), 134, -1, FALSE, '`hora_fin`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->hora_fin->Sortable = TRUE; // Allow sort
		$this->hora_fin->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_TIME_SEPARATOR"], $Language->Phrase("IncorrectTime"));
		$this->fields['hora_fin'] = &$this->hora_fin;

		// zona
		$this->zona = new cField('remates', 'remates', 'x_zona', 'zona', '`zona`', '`zona`', 202, -1, FALSE, '`zona`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->zona->Sortable = TRUE; // Allow sort
		$this->zona->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->zona->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->zona->OptionCount = 3;
		$this->fields['zona'] = &$this->zona;

		// lugar
		$this->lugar = new cField('remates', 'remates', 'x_lugar', 'lugar', '`lugar`', '`lugar`', 200, -1, FALSE, '`lugar`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->lugar->Sortable = TRUE; // Allow sort
		$this->fields['lugar'] = &$this->lugar;

		// locacion
		$this->locacion = new cField('remates', 'remates', 'x_locacion', 'locacion', '`locacion`', '`locacion`', 200, -1, FALSE, '`locacion`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->locacion->Sortable = TRUE; // Allow sort
		$this->fields['locacion'] = &$this->locacion;

		// numero
		$this->numero = new cField('remates', 'remates', 'x_numero', 'numero', '`numero`', '`numero`', 3, -1, FALSE, '`numero`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->numero->Sortable = TRUE; // Allow sort
		$this->numero->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['numero'] = &$this->numero;

		// titulo
		$this->titulo = new cField('remates', 'remates', 'x_titulo', 'titulo', '`titulo`', '`titulo`', 200, -1, FALSE, '`titulo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->titulo->Sortable = TRUE; // Allow sort
		$this->fields['titulo'] = &$this->titulo;

		// cantidad
		$this->cantidad = new cField('remates', 'remates', 'x_cantidad', 'cantidad', '`cantidad`', '`cantidad`', 3, -1, FALSE, '`cantidad`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->cantidad->Sortable = TRUE; // Allow sort
		$this->cantidad->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['cantidad'] = &$this->cantidad;

		// descripcion
		$this->descripcion = new cField('remates', 'remates', 'x_descripcion', 'descripcion', '`descripcion`', '`descripcion`', 201, -1, FALSE, '`descripcion`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->descripcion->Sortable = TRUE; // Allow sort
		$this->fields['descripcion'] = &$this->descripcion;

		// yacare_visible
		$this->yacare_visible = new cField('remates', 'remates', 'x_yacare_visible', 'yacare_visible', '`yacare_visible`', '`yacare_visible`', 202, -1, FALSE, '`yacare_visible`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->yacare_visible->Sortable = TRUE; // Allow sort
		$this->yacare_visible->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->yacare_visible->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->yacare_visible->OptionCount = 2;
		$this->fields['yacare_visible'] = &$this->yacare_visible;

		// banner_g
		$this->banner_g = new cField('remates', 'remates', 'x_banner_g', 'banner_g', '`banner_g`', '`banner_g`', 200, -1, TRUE, '`banner_g`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->banner_g->Sortable = TRUE; // Allow sort
		$this->fields['banner_g'] = &$this->banner_g;

		// banner_g_visible
		$this->banner_g_visible = new cField('remates', 'remates', 'x_banner_g_visible', 'banner_g_visible', '`banner_g_visible`', '`banner_g_visible`', 202, -1, FALSE, '`banner_g_visible`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->banner_g_visible->Sortable = TRUE; // Allow sort
		$this->banner_g_visible->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->banner_g_visible->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->banner_g_visible->OptionCount = 2;
		$this->fields['banner_g_visible'] = &$this->banner_g_visible;

		// banner_g_z_index
		$this->banner_g_z_index = new cField('remates', 'remates', 'x_banner_g_z_index', 'banner_g_z_index', '`banner_g_z_index`', '`banner_g_z_index`', 16, -1, FALSE, '`banner_g_z_index`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->banner_g_z_index->Sortable = TRUE; // Allow sort
		$this->banner_g_z_index->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['banner_g_z_index'] = &$this->banner_g_z_index;

		// banner_g_pos_x
		$this->banner_g_pos_x = new cField('remates', 'remates', 'x_banner_g_pos_x', 'banner_g_pos_x', '`banner_g_pos_x`', '`banner_g_pos_x`', 16, -1, FALSE, '`banner_g_pos_x`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->banner_g_pos_x->Sortable = TRUE; // Allow sort
		$this->banner_g_pos_x->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['banner_g_pos_x'] = &$this->banner_g_pos_x;

		// banner_g_pos_y
		$this->banner_g_pos_y = new cField('remates', 'remates', 'x_banner_g_pos_y', 'banner_g_pos_y', '`banner_g_pos_y`', '`banner_g_pos_y`', 16, -1, FALSE, '`banner_g_pos_y`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->banner_g_pos_y->Sortable = TRUE; // Allow sort
		$this->banner_g_pos_y->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['banner_g_pos_y'] = &$this->banner_g_pos_y;

		// banner_g_link
		$this->banner_g_link = new cField('remates', 'remates', 'x_banner_g_link', 'banner_g_link', '`banner_g_link`', '`banner_g_link`', 200, -1, FALSE, '`banner_g_link`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->banner_g_link->Sortable = TRUE; // Allow sort
		$this->fields['banner_g_link'] = &$this->banner_g_link;

		// banner_ch
		$this->banner_ch = new cField('remates', 'remates', 'x_banner_ch', 'banner_ch', '`banner_ch`', '`banner_ch`', 200, -1, TRUE, '`banner_ch`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->banner_ch->Sortable = TRUE; // Allow sort
		$this->fields['banner_ch'] = &$this->banner_ch;

		// activo
		$this->activo = new cField('remates', 'remates', 'x_activo', 'activo', '`activo`', '`activo`', 202, -1, FALSE, '`activo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->activo->Sortable = TRUE; // Allow sort
		$this->activo->OptionCount = 2;
		$this->fields['activo'] = &$this->activo;

		// archivo
		$this->archivo = new cField('remates', 'remates', 'x_archivo', 'archivo', '`archivo`', '`archivo`', 200, -1, TRUE, '`archivo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->archivo->Sortable = TRUE; // Allow sort
		$this->fields['archivo'] = &$this->archivo;

		// precios
		$this->precios = new cField('remates', 'remates', 'x_precios', 'precios', '`precios`', '`precios`', 202, -1, FALSE, '`precios`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->precios->Sortable = TRUE; // Allow sort
		$this->precios->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->precios->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->precios->OptionCount = 2;
		$this->fields['precios'] = &$this->precios;

		// mapa_img
		$this->mapa_img = new cField('remates', 'remates', 'x_mapa_img', 'mapa_img', '`mapa_img`', '`mapa_img`', 200, -1, TRUE, '`mapa_img`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->mapa_img->Sortable = TRUE; // Allow sort
		$this->fields['mapa_img'] = &$this->mapa_img;

		// video_final_servidor
		$this->video_final_servidor = new cField('remates', 'remates', 'x_video_final_servidor', 'video_final_servidor', '`video_final_servidor`', '`video_final_servidor`', 202, -1, FALSE, '`video_final_servidor`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->video_final_servidor->Sortable = TRUE; // Allow sort
		$this->video_final_servidor->OptionCount = 2;
		$this->fields['video_final_servidor'] = &$this->video_final_servidor;

		// video_final_1
		$this->video_final_1 = new cField('remates', 'remates', 'x_video_final_1', 'video_final_1', '`video_final_1`', '`video_final_1`', 200, -1, FALSE, '`video_final_1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_final_1->Sortable = TRUE; // Allow sort
		$this->fields['video_final_1'] = &$this->video_final_1;

		// video_titu_1
		$this->video_titu_1 = new cField('remates', 'remates', 'x_video_titu_1', 'video_titu_1', '`video_titu_1`', '`video_titu_1`', 200, -1, FALSE, '`video_titu_1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_titu_1->Sortable = TRUE; // Allow sort
		$this->fields['video_titu_1'] = &$this->video_titu_1;

		// video_final_2
		$this->video_final_2 = new cField('remates', 'remates', 'x_video_final_2', 'video_final_2', '`video_final_2`', '`video_final_2`', 200, -1, FALSE, '`video_final_2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_final_2->Sortable = TRUE; // Allow sort
		$this->fields['video_final_2'] = &$this->video_final_2;

		// video_titu_2
		$this->video_titu_2 = new cField('remates', 'remates', 'x_video_titu_2', 'video_titu_2', '`video_titu_2`', '`video_titu_2`', 200, -1, FALSE, '`video_titu_2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_titu_2->Sortable = TRUE; // Allow sort
		$this->fields['video_titu_2'] = &$this->video_titu_2;

		// video_final_3
		$this->video_final_3 = new cField('remates', 'remates', 'x_video_final_3', 'video_final_3', '`video_final_3`', '`video_final_3`', 200, -1, FALSE, '`video_final_3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_final_3->Sortable = TRUE; // Allow sort
		$this->fields['video_final_3'] = &$this->video_final_3;

		// video_titu_3
		$this->video_titu_3 = new cField('remates', 'remates', 'x_video_titu_3', 'video_titu_3', '`video_titu_3`', '`video_titu_3`', 200, -1, FALSE, '`video_titu_3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_titu_3->Sortable = TRUE; // Allow sort
		$this->fields['video_titu_3'] = &$this->video_titu_3;

		// video_final_4
		$this->video_final_4 = new cField('remates', 'remates', 'x_video_final_4', 'video_final_4', '`video_final_4`', '`video_final_4`', 200, -1, FALSE, '`video_final_4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_final_4->Sortable = TRUE; // Allow sort
		$this->fields['video_final_4'] = &$this->video_final_4;

		// video_titu_4
		$this->video_titu_4 = new cField('remates', 'remates', 'x_video_titu_4', 'video_titu_4', '`video_titu_4`', '`video_titu_4`', 200, -1, FALSE, '`video_titu_4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_titu_4->Sortable = TRUE; // Allow sort
		$this->fields['video_titu_4'] = &$this->video_titu_4;

		// video_final_5
		$this->video_final_5 = new cField('remates', 'remates', 'x_video_final_5', 'video_final_5', '`video_final_5`', '`video_final_5`', 200, -1, FALSE, '`video_final_5`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_final_5->Sortable = TRUE; // Allow sort
		$this->fields['video_final_5'] = &$this->video_final_5;

		// video_titu_5
		$this->video_titu_5 = new cField('remates', 'remates', 'x_video_titu_5', 'video_titu_5', '`video_titu_5`', '`video_titu_5`', 200, -1, FALSE, '`video_titu_5`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video_titu_5->Sortable = TRUE; // Allow sort
		$this->fields['video_titu_5'] = &$this->video_titu_5;
	}

	// Set Field Visibility
	function SetFieldVisibility($fldparm) {
		global $Security;
		return $this->$fldparm->Visible; // Returns original value
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`remates`";
	}

	function SqlFrom() { // For backward compatibility
		return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
		$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
		return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
		$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
		return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
		$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
		return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
		$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
		return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
		$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`fecha` DESC";
	}

	function SqlOrderBy() { // For backward compatibility
		return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
		$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 8) == 8);
			case "search":
				return (($allow & 8) == 8);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id', $this->DBID) . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "remateslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "remateslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("rematesview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("rematesview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "rematesadd.php?" . $this->UrlParm($parm);
		else
			$url = "rematesadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("rematesedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("rematesadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("rematesdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "id:" . ew_VarToJson($this->id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return $this->AddMasterUrl(ew_CurrentPage() . "?" . $sUrlParm);
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["id"]))
				$arKeys[] = ew_StripSlashes($_POST["id"]);
			elseif (isset($_GET["id"]))
				$arKeys[] = ew_StripSlashes($_GET["id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->plataforma->setDbValue($rs->fields('plataforma'));
		$this->ext_id->setDbValue($rs->fields('ext_id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->hora_inicio->setDbValue($rs->fields('hora_inicio'));
		$this->hora_fin->setDbValue($rs->fields('hora_fin'));
		$this->zona->setDbValue($rs->fields('zona'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->locacion->setDbValue($rs->fields('locacion'));
		$this->numero->setDbValue($rs->fields('numero'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->yacare_visible->setDbValue($rs->fields('yacare_visible'));
		$this->banner_g->Upload->DbValue = $rs->fields('banner_g');
		$this->banner_g_visible->setDbValue($rs->fields('banner_g_visible'));
		$this->banner_g_z_index->setDbValue($rs->fields('banner_g_z_index'));
		$this->banner_g_pos_x->setDbValue($rs->fields('banner_g_pos_x'));
		$this->banner_g_pos_y->setDbValue($rs->fields('banner_g_pos_y'));
		$this->banner_g_link->setDbValue($rs->fields('banner_g_link'));
		$this->banner_ch->Upload->DbValue = $rs->fields('banner_ch');
		$this->activo->setDbValue($rs->fields('activo'));
		$this->archivo->Upload->DbValue = $rs->fields('archivo');
		$this->precios->setDbValue($rs->fields('precios'));
		$this->mapa_img->Upload->DbValue = $rs->fields('mapa_img');
		$this->video_final_servidor->setDbValue($rs->fields('video_final_servidor'));
		$this->video_final_1->setDbValue($rs->fields('video_final_1'));
		$this->video_titu_1->setDbValue($rs->fields('video_titu_1'));
		$this->video_final_2->setDbValue($rs->fields('video_final_2'));
		$this->video_titu_2->setDbValue($rs->fields('video_titu_2'));
		$this->video_final_3->setDbValue($rs->fields('video_final_3'));
		$this->video_titu_3->setDbValue($rs->fields('video_titu_3'));
		$this->video_final_4->setDbValue($rs->fields('video_final_4'));
		$this->video_titu_4->setDbValue($rs->fields('video_titu_4'));
		$this->video_final_5->setDbValue($rs->fields('video_final_5'));
		$this->video_titu_5->setDbValue($rs->fields('video_titu_5'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// tipo
		// plataforma
		// ext_id
		// fecha
		// hora_inicio
		// hora_fin
		// zona
		// lugar
		// locacion
		// numero
		// titulo
		// cantidad
		// descripcion
		// yacare_visible
		// banner_g
		// banner_g_visible
		// banner_g_z_index
		// banner_g_pos_x
		// banner_g_pos_y
		// banner_g_link
		// banner_ch

		$this->banner_ch->CellCssStyle = "white-space: nowrap;";

		// activo
		// archivo
		// precios
		// mapa_img
		// video_final_servidor
		// video_final_1
		// video_titu_1
		// video_final_2
		// video_titu_2
		// video_final_3
		// video_titu_3
		// video_final_4
		// video_titu_4
		// video_final_5
		// video_titu_5
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		if (strval($this->tipo->CurrentValue) <> "") {
			$this->tipo->ViewValue = $this->tipo->OptionCaption($this->tipo->CurrentValue);
		} else {
			$this->tipo->ViewValue = NULL;
		}
		$this->tipo->ViewCustomAttributes = "";

		// plataforma
		if (strval($this->plataforma->CurrentValue) <> "") {
			$this->plataforma->ViewValue = $this->plataforma->OptionCaption($this->plataforma->CurrentValue);
		} else {
			$this->plataforma->ViewValue = NULL;
		}
		$this->plataforma->ViewCustomAttributes = "";

		// ext_id
		$this->ext_id->ViewValue = $this->ext_id->CurrentValue;
		$this->ext_id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// hora_inicio
		$this->hora_inicio->ViewValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->ViewCustomAttributes = "";

		// hora_fin
		$this->hora_fin->ViewValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->ViewCustomAttributes = "";

		// zona
		if (strval($this->zona->CurrentValue) <> "") {
			$this->zona->ViewValue = $this->zona->OptionCaption($this->zona->CurrentValue);
		} else {
			$this->zona->ViewValue = NULL;
		}
		$this->zona->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewCustomAttributes = "";

		// locacion
		$this->locacion->ViewValue = $this->locacion->CurrentValue;
		$this->locacion->ViewCustomAttributes = "";

		// numero
		$this->numero->ViewValue = $this->numero->CurrentValue;
		$this->numero->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// yacare_visible
		if (strval($this->yacare_visible->CurrentValue) <> "") {
			$this->yacare_visible->ViewValue = $this->yacare_visible->OptionCaption($this->yacare_visible->CurrentValue);
		} else {
			$this->yacare_visible->ViewValue = NULL;
		}
		$this->yacare_visible->ViewCustomAttributes = "";

		// banner_g
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->ViewValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->ViewValue = "";
		}
		$this->banner_g->ViewCustomAttributes = "";

		// banner_g_visible
		if (strval($this->banner_g_visible->CurrentValue) <> "") {
			$this->banner_g_visible->ViewValue = $this->banner_g_visible->OptionCaption($this->banner_g_visible->CurrentValue);
		} else {
			$this->banner_g_visible->ViewValue = NULL;
		}
		$this->banner_g_visible->ViewCustomAttributes = "";

		// banner_g_z_index
		$this->banner_g_z_index->ViewValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->ViewCustomAttributes = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->ViewValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->ViewCustomAttributes = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->ViewValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->ViewCustomAttributes = "";

		// banner_g_link
		$this->banner_g_link->ViewValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->ViewCustomAttributes = "";

		// banner_ch
		if (!ew_Empty($this->banner_ch->Upload->DbValue)) {
			$this->banner_ch->ViewValue = $this->banner_ch->Upload->DbValue;
		} else {
			$this->banner_ch->ViewValue = "";
		}
		$this->banner_ch->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// archivo
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->ViewValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->ViewValue = "";
		}
		$this->archivo->ViewCustomAttributes = "";

		// precios
		if (strval($this->precios->CurrentValue) <> "") {
			$this->precios->ViewValue = $this->precios->OptionCaption($this->precios->CurrentValue);
		} else {
			$this->precios->ViewValue = NULL;
		}
		$this->precios->ViewCustomAttributes = "";

		// mapa_img
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->ViewValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->ViewValue = "";
		}
		$this->mapa_img->ViewCustomAttributes = "";

		// video_final_servidor
		if (strval($this->video_final_servidor->CurrentValue) <> "") {
			$this->video_final_servidor->ViewValue = $this->video_final_servidor->OptionCaption($this->video_final_servidor->CurrentValue);
		} else {
			$this->video_final_servidor->ViewValue = NULL;
		}
		$this->video_final_servidor->ViewCustomAttributes = "";

		// video_final_1
		$this->video_final_1->ViewValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->ViewCustomAttributes = "";

		// video_titu_1
		$this->video_titu_1->ViewValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->ViewCustomAttributes = "";

		// video_final_2
		$this->video_final_2->ViewValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->ViewCustomAttributes = "";

		// video_titu_2
		$this->video_titu_2->ViewValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->ViewCustomAttributes = "";

		// video_final_3
		$this->video_final_3->ViewValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->ViewCustomAttributes = "";

		// video_titu_3
		$this->video_titu_3->ViewValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->ViewCustomAttributes = "";

		// video_final_4
		$this->video_final_4->ViewValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->ViewCustomAttributes = "";

		// video_titu_4
		$this->video_titu_4->ViewValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->ViewCustomAttributes = "";

		// video_final_5
		$this->video_final_5->ViewValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->ViewCustomAttributes = "";

		// video_titu_5
		$this->video_titu_5->ViewValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// tipo
		$this->tipo->LinkCustomAttributes = "";
		$this->tipo->HrefValue = "";
		$this->tipo->TooltipValue = "";

		// plataforma
		$this->plataforma->LinkCustomAttributes = "";
		$this->plataforma->HrefValue = "";
		$this->plataforma->TooltipValue = "";

		// ext_id
		$this->ext_id->LinkCustomAttributes = "";
		$this->ext_id->HrefValue = "";
		$this->ext_id->TooltipValue = "";

		// fecha
		$this->fecha->LinkCustomAttributes = "";
		$this->fecha->HrefValue = "";
		$this->fecha->TooltipValue = "";

		// hora_inicio
		$this->hora_inicio->LinkCustomAttributes = "";
		$this->hora_inicio->HrefValue = "";
		$this->hora_inicio->TooltipValue = "";

		// hora_fin
		$this->hora_fin->LinkCustomAttributes = "";
		$this->hora_fin->HrefValue = "";
		$this->hora_fin->TooltipValue = "";

		// zona
		$this->zona->LinkCustomAttributes = "";
		$this->zona->HrefValue = "";
		$this->zona->TooltipValue = "";

		// lugar
		$this->lugar->LinkCustomAttributes = "";
		$this->lugar->HrefValue = "";
		$this->lugar->TooltipValue = "";

		// locacion
		$this->locacion->LinkCustomAttributes = "";
		$this->locacion->HrefValue = "";
		$this->locacion->TooltipValue = "";

		// numero
		$this->numero->LinkCustomAttributes = "";
		$this->numero->HrefValue = "";
		$this->numero->TooltipValue = "";

		// titulo
		$this->titulo->LinkCustomAttributes = "";
		$this->titulo->HrefValue = "";
		$this->titulo->TooltipValue = "";

		// cantidad
		$this->cantidad->LinkCustomAttributes = "";
		$this->cantidad->HrefValue = "";
		$this->cantidad->TooltipValue = "";

		// descripcion
		$this->descripcion->LinkCustomAttributes = "";
		$this->descripcion->HrefValue = "";
		$this->descripcion->TooltipValue = "";

		// yacare_visible
		$this->yacare_visible->LinkCustomAttributes = "";
		$this->yacare_visible->HrefValue = "";
		$this->yacare_visible->TooltipValue = "";

		// banner_g
		$this->banner_g->LinkCustomAttributes = "";
		$this->banner_g->HrefValue = "";
		$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;
		$this->banner_g->TooltipValue = "";

		// banner_g_visible
		$this->banner_g_visible->LinkCustomAttributes = "";
		$this->banner_g_visible->HrefValue = "";
		$this->banner_g_visible->TooltipValue = "";

		// banner_g_z_index
		$this->banner_g_z_index->LinkCustomAttributes = "";
		$this->banner_g_z_index->HrefValue = "";
		$this->banner_g_z_index->TooltipValue = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->LinkCustomAttributes = "";
		$this->banner_g_pos_x->HrefValue = "";
		$this->banner_g_pos_x->TooltipValue = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->LinkCustomAttributes = "";
		$this->banner_g_pos_y->HrefValue = "";
		$this->banner_g_pos_y->TooltipValue = "";

		// banner_g_link
		$this->banner_g_link->LinkCustomAttributes = "";
		$this->banner_g_link->HrefValue = "";
		$this->banner_g_link->TooltipValue = "";

		// banner_ch
		$this->banner_ch->LinkCustomAttributes = "";
		$this->banner_ch->HrefValue = "";
		$this->banner_ch->HrefValue2 = $this->banner_ch->UploadPath . $this->banner_ch->Upload->DbValue;
		$this->banner_ch->TooltipValue = "";

		// activo
		$this->activo->LinkCustomAttributes = "";
		$this->activo->HrefValue = "";
		$this->activo->TooltipValue = "";

		// archivo
		$this->archivo->LinkCustomAttributes = "";
		$this->archivo->HrefValue = "";
		$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;
		$this->archivo->TooltipValue = "";

		// precios
		$this->precios->LinkCustomAttributes = "";
		$this->precios->HrefValue = "";
		$this->precios->TooltipValue = "";

		// mapa_img
		$this->mapa_img->LinkCustomAttributes = "";
		$this->mapa_img->HrefValue = "";
		$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;
		$this->mapa_img->TooltipValue = "";

		// video_final_servidor
		$this->video_final_servidor->LinkCustomAttributes = "";
		$this->video_final_servidor->HrefValue = "";
		$this->video_final_servidor->TooltipValue = "";

		// video_final_1
		$this->video_final_1->LinkCustomAttributes = "";
		$this->video_final_1->HrefValue = "";
		$this->video_final_1->TooltipValue = "";

		// video_titu_1
		$this->video_titu_1->LinkCustomAttributes = "";
		$this->video_titu_1->HrefValue = "";
		$this->video_titu_1->TooltipValue = "";

		// video_final_2
		$this->video_final_2->LinkCustomAttributes = "";
		$this->video_final_2->HrefValue = "";
		$this->video_final_2->TooltipValue = "";

		// video_titu_2
		$this->video_titu_2->LinkCustomAttributes = "";
		$this->video_titu_2->HrefValue = "";
		$this->video_titu_2->TooltipValue = "";

		// video_final_3
		$this->video_final_3->LinkCustomAttributes = "";
		$this->video_final_3->HrefValue = "";
		$this->video_final_3->TooltipValue = "";

		// video_titu_3
		$this->video_titu_3->LinkCustomAttributes = "";
		$this->video_titu_3->HrefValue = "";
		$this->video_titu_3->TooltipValue = "";

		// video_final_4
		$this->video_final_4->LinkCustomAttributes = "";
		$this->video_final_4->HrefValue = "";
		$this->video_final_4->TooltipValue = "";

		// video_titu_4
		$this->video_titu_4->LinkCustomAttributes = "";
		$this->video_titu_4->HrefValue = "";
		$this->video_titu_4->TooltipValue = "";

		// video_final_5
		$this->video_final_5->LinkCustomAttributes = "";
		$this->video_final_5->HrefValue = "";
		$this->video_final_5->TooltipValue = "";

		// video_titu_5
		$this->video_titu_5->LinkCustomAttributes = "";
		$this->video_titu_5->HrefValue = "";
		$this->video_titu_5->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		$this->tipo->EditCustomAttributes = "";
		$this->tipo->EditValue = $this->tipo->Options(FALSE);

		// plataforma
		$this->plataforma->EditAttrs["class"] = "form-control";
		$this->plataforma->EditCustomAttributes = "";
		$this->plataforma->EditValue = $this->plataforma->Options(TRUE);

		// ext_id
		$this->ext_id->EditAttrs["class"] = "form-control";
		$this->ext_id->EditCustomAttributes = "";
		$this->ext_id->EditValue = $this->ext_id->CurrentValue;
		$this->ext_id->PlaceHolder = ew_RemoveHtml($this->ext_id->FldCaption());

		// fecha
		$this->fecha->EditAttrs["class"] = "form-control";
		$this->fecha->EditCustomAttributes = "";
		$this->fecha->EditValue = ew_FormatDateTime($this->fecha->CurrentValue, 8);
		$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

		// hora_inicio
		$this->hora_inicio->EditAttrs["class"] = "form-control";
		$this->hora_inicio->EditCustomAttributes = "";
		$this->hora_inicio->EditValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->PlaceHolder = ew_RemoveHtml($this->hora_inicio->FldCaption());

		// hora_fin
		$this->hora_fin->EditAttrs["class"] = "form-control";
		$this->hora_fin->EditCustomAttributes = "";
		$this->hora_fin->EditValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->PlaceHolder = ew_RemoveHtml($this->hora_fin->FldCaption());

		// zona
		$this->zona->EditAttrs["class"] = "form-control";
		$this->zona->EditCustomAttributes = "";
		$this->zona->EditValue = $this->zona->Options(TRUE);

		// lugar
		$this->lugar->EditAttrs["class"] = "form-control";
		$this->lugar->EditCustomAttributes = "";
		$this->lugar->EditValue = $this->lugar->CurrentValue;
		$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

		// locacion
		$this->locacion->EditAttrs["class"] = "form-control";
		$this->locacion->EditCustomAttributes = "";
		$this->locacion->EditValue = $this->locacion->CurrentValue;
		$this->locacion->PlaceHolder = ew_RemoveHtml($this->locacion->FldCaption());

		// numero
		$this->numero->EditAttrs["class"] = "form-control";
		$this->numero->EditCustomAttributes = "";
		$this->numero->EditValue = $this->numero->CurrentValue;
		$this->numero->PlaceHolder = ew_RemoveHtml($this->numero->FldCaption());

		// titulo
		$this->titulo->EditAttrs["class"] = "form-control";
		$this->titulo->EditCustomAttributes = "";
		$this->titulo->EditValue = $this->titulo->CurrentValue;
		$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

		// cantidad
		$this->cantidad->EditAttrs["class"] = "form-control";
		$this->cantidad->EditCustomAttributes = "";
		$this->cantidad->EditValue = $this->cantidad->CurrentValue;
		$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

		// descripcion
		$this->descripcion->EditAttrs["class"] = "form-control";
		$this->descripcion->EditCustomAttributes = "";
		$this->descripcion->EditValue = $this->descripcion->CurrentValue;
		$this->descripcion->PlaceHolder = ew_RemoveHtml($this->descripcion->FldCaption());

		// yacare_visible
		$this->yacare_visible->EditAttrs["class"] = "form-control";
		$this->yacare_visible->EditCustomAttributes = "";
		$this->yacare_visible->EditValue = $this->yacare_visible->Options(TRUE);

		// banner_g
		$this->banner_g->EditAttrs["class"] = "form-control";
		$this->banner_g->EditCustomAttributes = "";
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->EditValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->EditValue = "";
		}
		if (!ew_Empty($this->banner_g->CurrentValue))
			$this->banner_g->Upload->FileName = $this->banner_g->CurrentValue;

		// banner_g_visible
		$this->banner_g_visible->EditAttrs["class"] = "form-control";
		$this->banner_g_visible->EditCustomAttributes = "";
		$this->banner_g_visible->EditValue = $this->banner_g_visible->Options(TRUE);

		// banner_g_z_index
		$this->banner_g_z_index->EditAttrs["class"] = "form-control";
		$this->banner_g_z_index->EditCustomAttributes = "";
		$this->banner_g_z_index->EditValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->PlaceHolder = ew_RemoveHtml($this->banner_g_z_index->FldCaption());

		// banner_g_pos_x
		$this->banner_g_pos_x->EditAttrs["class"] = "form-control";
		$this->banner_g_pos_x->EditCustomAttributes = "";
		$this->banner_g_pos_x->EditValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_x->FldCaption());

		// banner_g_pos_y
		$this->banner_g_pos_y->EditAttrs["class"] = "form-control";
		$this->banner_g_pos_y->EditCustomAttributes = "";
		$this->banner_g_pos_y->EditValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->PlaceHolder = ew_RemoveHtml($this->banner_g_pos_y->FldCaption());

		// banner_g_link
		$this->banner_g_link->EditAttrs["class"] = "form-control";
		$this->banner_g_link->EditCustomAttributes = "";
		$this->banner_g_link->EditValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->PlaceHolder = ew_RemoveHtml($this->banner_g_link->FldCaption());

		// banner_ch
		$this->banner_ch->EditAttrs["class"] = "form-control";
		$this->banner_ch->EditCustomAttributes = "";
		if (!ew_Empty($this->banner_ch->Upload->DbValue)) {
			$this->banner_ch->EditValue = $this->banner_ch->Upload->DbValue;
		} else {
			$this->banner_ch->EditValue = "";
		}
		if (!ew_Empty($this->banner_ch->CurrentValue))
			$this->banner_ch->Upload->FileName = $this->banner_ch->CurrentValue;

		// activo
		$this->activo->EditCustomAttributes = "";
		$this->activo->EditValue = $this->activo->Options(FALSE);

		// archivo
		$this->archivo->EditAttrs["class"] = "form-control";
		$this->archivo->EditCustomAttributes = "";
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->EditValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->EditValue = "";
		}
		if (!ew_Empty($this->archivo->CurrentValue))
			$this->archivo->Upload->FileName = $this->archivo->CurrentValue;

		// precios
		$this->precios->EditAttrs["class"] = "form-control";
		$this->precios->EditCustomAttributes = "";
		$this->precios->EditValue = $this->precios->Options(TRUE);

		// mapa_img
		$this->mapa_img->EditAttrs["class"] = "form-control";
		$this->mapa_img->EditCustomAttributes = "";
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->EditValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->EditValue = "";
		}
		if (!ew_Empty($this->mapa_img->CurrentValue))
			$this->mapa_img->Upload->FileName = $this->mapa_img->CurrentValue;

		// video_final_servidor
		$this->video_final_servidor->EditCustomAttributes = "";
		$this->video_final_servidor->EditValue = $this->video_final_servidor->Options(FALSE);

		// video_final_1
		$this->video_final_1->EditAttrs["class"] = "form-control";
		$this->video_final_1->EditCustomAttributes = "";
		$this->video_final_1->EditValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->PlaceHolder = ew_RemoveHtml($this->video_final_1->FldCaption());

		// video_titu_1
		$this->video_titu_1->EditAttrs["class"] = "form-control";
		$this->video_titu_1->EditCustomAttributes = "";
		$this->video_titu_1->EditValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->PlaceHolder = ew_RemoveHtml($this->video_titu_1->FldCaption());

		// video_final_2
		$this->video_final_2->EditAttrs["class"] = "form-control";
		$this->video_final_2->EditCustomAttributes = "";
		$this->video_final_2->EditValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->PlaceHolder = ew_RemoveHtml($this->video_final_2->FldCaption());

		// video_titu_2
		$this->video_titu_2->EditAttrs["class"] = "form-control";
		$this->video_titu_2->EditCustomAttributes = "";
		$this->video_titu_2->EditValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->PlaceHolder = ew_RemoveHtml($this->video_titu_2->FldCaption());

		// video_final_3
		$this->video_final_3->EditAttrs["class"] = "form-control";
		$this->video_final_3->EditCustomAttributes = "";
		$this->video_final_3->EditValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->PlaceHolder = ew_RemoveHtml($this->video_final_3->FldCaption());

		// video_titu_3
		$this->video_titu_3->EditAttrs["class"] = "form-control";
		$this->video_titu_3->EditCustomAttributes = "";
		$this->video_titu_3->EditValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->PlaceHolder = ew_RemoveHtml($this->video_titu_3->FldCaption());

		// video_final_4
		$this->video_final_4->EditAttrs["class"] = "form-control";
		$this->video_final_4->EditCustomAttributes = "";
		$this->video_final_4->EditValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->PlaceHolder = ew_RemoveHtml($this->video_final_4->FldCaption());

		// video_titu_4
		$this->video_titu_4->EditAttrs["class"] = "form-control";
		$this->video_titu_4->EditCustomAttributes = "";
		$this->video_titu_4->EditValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->PlaceHolder = ew_RemoveHtml($this->video_titu_4->FldCaption());

		// video_final_5
		$this->video_final_5->EditAttrs["class"] = "form-control";
		$this->video_final_5->EditCustomAttributes = "";
		$this->video_final_5->EditValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->PlaceHolder = ew_RemoveHtml($this->video_final_5->FldCaption());

		// video_titu_5
		$this->video_titu_5->EditAttrs["class"] = "form-control";
		$this->video_titu_5->EditCustomAttributes = "";
		$this->video_titu_5->EditValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->PlaceHolder = ew_RemoveHtml($this->video_titu_5->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->tipo->Exportable) $Doc->ExportCaption($this->tipo);
					if ($this->plataforma->Exportable) $Doc->ExportCaption($this->plataforma);
					if ($this->ext_id->Exportable) $Doc->ExportCaption($this->ext_id);
					if ($this->fecha->Exportable) $Doc->ExportCaption($this->fecha);
					if ($this->hora_inicio->Exportable) $Doc->ExportCaption($this->hora_inicio);
					if ($this->hora_fin->Exportable) $Doc->ExportCaption($this->hora_fin);
					if ($this->zona->Exportable) $Doc->ExportCaption($this->zona);
					if ($this->lugar->Exportable) $Doc->ExportCaption($this->lugar);
					if ($this->locacion->Exportable) $Doc->ExportCaption($this->locacion);
					if ($this->numero->Exportable) $Doc->ExportCaption($this->numero);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->cantidad->Exportable) $Doc->ExportCaption($this->cantidad);
					if ($this->descripcion->Exportable) $Doc->ExportCaption($this->descripcion);
					if ($this->yacare_visible->Exportable) $Doc->ExportCaption($this->yacare_visible);
					if ($this->banner_g->Exportable) $Doc->ExportCaption($this->banner_g);
					if ($this->banner_g_visible->Exportable) $Doc->ExportCaption($this->banner_g_visible);
					if ($this->banner_g_z_index->Exportable) $Doc->ExportCaption($this->banner_g_z_index);
					if ($this->banner_g_pos_x->Exportable) $Doc->ExportCaption($this->banner_g_pos_x);
					if ($this->banner_g_pos_y->Exportable) $Doc->ExportCaption($this->banner_g_pos_y);
					if ($this->banner_g_link->Exportable) $Doc->ExportCaption($this->banner_g_link);
					if ($this->activo->Exportable) $Doc->ExportCaption($this->activo);
					if ($this->archivo->Exportable) $Doc->ExportCaption($this->archivo);
					if ($this->precios->Exportable) $Doc->ExportCaption($this->precios);
					if ($this->mapa_img->Exportable) $Doc->ExportCaption($this->mapa_img);
					if ($this->video_final_servidor->Exportable) $Doc->ExportCaption($this->video_final_servidor);
					if ($this->video_final_1->Exportable) $Doc->ExportCaption($this->video_final_1);
					if ($this->video_titu_1->Exportable) $Doc->ExportCaption($this->video_titu_1);
					if ($this->video_final_2->Exportable) $Doc->ExportCaption($this->video_final_2);
					if ($this->video_titu_2->Exportable) $Doc->ExportCaption($this->video_titu_2);
					if ($this->video_final_3->Exportable) $Doc->ExportCaption($this->video_final_3);
					if ($this->video_titu_3->Exportable) $Doc->ExportCaption($this->video_titu_3);
					if ($this->video_final_4->Exportable) $Doc->ExportCaption($this->video_final_4);
					if ($this->video_titu_4->Exportable) $Doc->ExportCaption($this->video_titu_4);
					if ($this->video_final_5->Exportable) $Doc->ExportCaption($this->video_final_5);
					if ($this->video_titu_5->Exportable) $Doc->ExportCaption($this->video_titu_5);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->tipo->Exportable) $Doc->ExportCaption($this->tipo);
					if ($this->plataforma->Exportable) $Doc->ExportCaption($this->plataforma);
					if ($this->ext_id->Exportable) $Doc->ExportCaption($this->ext_id);
					if ($this->fecha->Exportable) $Doc->ExportCaption($this->fecha);
					if ($this->hora_inicio->Exportable) $Doc->ExportCaption($this->hora_inicio);
					if ($this->hora_fin->Exportable) $Doc->ExportCaption($this->hora_fin);
					if ($this->zona->Exportable) $Doc->ExportCaption($this->zona);
					if ($this->lugar->Exportable) $Doc->ExportCaption($this->lugar);
					if ($this->locacion->Exportable) $Doc->ExportCaption($this->locacion);
					if ($this->numero->Exportable) $Doc->ExportCaption($this->numero);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->cantidad->Exportable) $Doc->ExportCaption($this->cantidad);
					if ($this->yacare_visible->Exportable) $Doc->ExportCaption($this->yacare_visible);
					if ($this->banner_g->Exportable) $Doc->ExportCaption($this->banner_g);
					if ($this->banner_g_visible->Exportable) $Doc->ExportCaption($this->banner_g_visible);
					if ($this->banner_g_z_index->Exportable) $Doc->ExportCaption($this->banner_g_z_index);
					if ($this->banner_g_pos_x->Exportable) $Doc->ExportCaption($this->banner_g_pos_x);
					if ($this->banner_g_pos_y->Exportable) $Doc->ExportCaption($this->banner_g_pos_y);
					if ($this->banner_g_link->Exportable) $Doc->ExportCaption($this->banner_g_link);
					if ($this->activo->Exportable) $Doc->ExportCaption($this->activo);
					if ($this->archivo->Exportable) $Doc->ExportCaption($this->archivo);
					if ($this->precios->Exportable) $Doc->ExportCaption($this->precios);
					if ($this->mapa_img->Exportable) $Doc->ExportCaption($this->mapa_img);
					if ($this->video_final_servidor->Exportable) $Doc->ExportCaption($this->video_final_servidor);
					if ($this->video_final_1->Exportable) $Doc->ExportCaption($this->video_final_1);
					if ($this->video_titu_1->Exportable) $Doc->ExportCaption($this->video_titu_1);
					if ($this->video_final_2->Exportable) $Doc->ExportCaption($this->video_final_2);
					if ($this->video_titu_2->Exportable) $Doc->ExportCaption($this->video_titu_2);
					if ($this->video_final_3->Exportable) $Doc->ExportCaption($this->video_final_3);
					if ($this->video_titu_3->Exportable) $Doc->ExportCaption($this->video_titu_3);
					if ($this->video_final_4->Exportable) $Doc->ExportCaption($this->video_final_4);
					if ($this->video_titu_4->Exportable) $Doc->ExportCaption($this->video_titu_4);
					if ($this->video_final_5->Exportable) $Doc->ExportCaption($this->video_final_5);
					if ($this->video_titu_5->Exportable) $Doc->ExportCaption($this->video_titu_5);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->tipo->Exportable) $Doc->ExportField($this->tipo);
						if ($this->plataforma->Exportable) $Doc->ExportField($this->plataforma);
						if ($this->ext_id->Exportable) $Doc->ExportField($this->ext_id);
						if ($this->fecha->Exportable) $Doc->ExportField($this->fecha);
						if ($this->hora_inicio->Exportable) $Doc->ExportField($this->hora_inicio);
						if ($this->hora_fin->Exportable) $Doc->ExportField($this->hora_fin);
						if ($this->zona->Exportable) $Doc->ExportField($this->zona);
						if ($this->lugar->Exportable) $Doc->ExportField($this->lugar);
						if ($this->locacion->Exportable) $Doc->ExportField($this->locacion);
						if ($this->numero->Exportable) $Doc->ExportField($this->numero);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->cantidad->Exportable) $Doc->ExportField($this->cantidad);
						if ($this->descripcion->Exportable) $Doc->ExportField($this->descripcion);
						if ($this->yacare_visible->Exportable) $Doc->ExportField($this->yacare_visible);
						if ($this->banner_g->Exportable) $Doc->ExportField($this->banner_g);
						if ($this->banner_g_visible->Exportable) $Doc->ExportField($this->banner_g_visible);
						if ($this->banner_g_z_index->Exportable) $Doc->ExportField($this->banner_g_z_index);
						if ($this->banner_g_pos_x->Exportable) $Doc->ExportField($this->banner_g_pos_x);
						if ($this->banner_g_pos_y->Exportable) $Doc->ExportField($this->banner_g_pos_y);
						if ($this->banner_g_link->Exportable) $Doc->ExportField($this->banner_g_link);
						if ($this->activo->Exportable) $Doc->ExportField($this->activo);
						if ($this->archivo->Exportable) $Doc->ExportField($this->archivo);
						if ($this->precios->Exportable) $Doc->ExportField($this->precios);
						if ($this->mapa_img->Exportable) $Doc->ExportField($this->mapa_img);
						if ($this->video_final_servidor->Exportable) $Doc->ExportField($this->video_final_servidor);
						if ($this->video_final_1->Exportable) $Doc->ExportField($this->video_final_1);
						if ($this->video_titu_1->Exportable) $Doc->ExportField($this->video_titu_1);
						if ($this->video_final_2->Exportable) $Doc->ExportField($this->video_final_2);
						if ($this->video_titu_2->Exportable) $Doc->ExportField($this->video_titu_2);
						if ($this->video_final_3->Exportable) $Doc->ExportField($this->video_final_3);
						if ($this->video_titu_3->Exportable) $Doc->ExportField($this->video_titu_3);
						if ($this->video_final_4->Exportable) $Doc->ExportField($this->video_final_4);
						if ($this->video_titu_4->Exportable) $Doc->ExportField($this->video_titu_4);
						if ($this->video_final_5->Exportable) $Doc->ExportField($this->video_final_5);
						if ($this->video_titu_5->Exportable) $Doc->ExportField($this->video_titu_5);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->tipo->Exportable) $Doc->ExportField($this->tipo);
						if ($this->plataforma->Exportable) $Doc->ExportField($this->plataforma);
						if ($this->ext_id->Exportable) $Doc->ExportField($this->ext_id);
						if ($this->fecha->Exportable) $Doc->ExportField($this->fecha);
						if ($this->hora_inicio->Exportable) $Doc->ExportField($this->hora_inicio);
						if ($this->hora_fin->Exportable) $Doc->ExportField($this->hora_fin);
						if ($this->zona->Exportable) $Doc->ExportField($this->zona);
						if ($this->lugar->Exportable) $Doc->ExportField($this->lugar);
						if ($this->locacion->Exportable) $Doc->ExportField($this->locacion);
						if ($this->numero->Exportable) $Doc->ExportField($this->numero);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->cantidad->Exportable) $Doc->ExportField($this->cantidad);
						if ($this->yacare_visible->Exportable) $Doc->ExportField($this->yacare_visible);
						if ($this->banner_g->Exportable) $Doc->ExportField($this->banner_g);
						if ($this->banner_g_visible->Exportable) $Doc->ExportField($this->banner_g_visible);
						if ($this->banner_g_z_index->Exportable) $Doc->ExportField($this->banner_g_z_index);
						if ($this->banner_g_pos_x->Exportable) $Doc->ExportField($this->banner_g_pos_x);
						if ($this->banner_g_pos_y->Exportable) $Doc->ExportField($this->banner_g_pos_y);
						if ($this->banner_g_link->Exportable) $Doc->ExportField($this->banner_g_link);
						if ($this->activo->Exportable) $Doc->ExportField($this->activo);
						if ($this->archivo->Exportable) $Doc->ExportField($this->archivo);
						if ($this->precios->Exportable) $Doc->ExportField($this->precios);
						if ($this->mapa_img->Exportable) $Doc->ExportField($this->mapa_img);
						if ($this->video_final_servidor->Exportable) $Doc->ExportField($this->video_final_servidor);
						if ($this->video_final_1->Exportable) $Doc->ExportField($this->video_final_1);
						if ($this->video_titu_1->Exportable) $Doc->ExportField($this->video_titu_1);
						if ($this->video_final_2->Exportable) $Doc->ExportField($this->video_final_2);
						if ($this->video_titu_2->Exportable) $Doc->ExportField($this->video_titu_2);
						if ($this->video_final_3->Exportable) $Doc->ExportField($this->video_final_3);
						if ($this->video_titu_3->Exportable) $Doc->ExportField($this->video_titu_3);
						if ($this->video_final_4->Exportable) $Doc->ExportField($this->video_final_4);
						if ($this->video_titu_4->Exportable) $Doc->ExportField($this->video_titu_4);
						if ($this->video_final_5->Exportable) $Doc->ExportField($this->video_final_5);
						if ($this->video_titu_5->Exportable) $Doc->ExportField($this->video_titu_5);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
