<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "notiliniersinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$notiliniers_delete = NULL; // Initialize page object first

class cnotiliniers_delete extends cnotiliniers {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'notiliniers';

	// Page object name
	var $PageObjName = 'notiliniers_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (notiliniers)
		if (!isset($GLOBALS["notiliniers"]) || get_class($GLOBALS["notiliniers"]) == "cnotiliniers") {
			$GLOBALS["notiliniers"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["notiliniers"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'notiliniers', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("notilinierslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->fecha->SetVisibility();
		$this->activa->SetVisibility();
		$this->titulo->SetVisibility();
		$this->bajada->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->epigrafe_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->epigrafe_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->epigrafe_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->epigrafe_4->SetVisibility();
		$this->galeria_id->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $notiliniers;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($notiliniers);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("notilinierslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in notiliniers class, notiliniersinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} elseif (@$_GET["a_delete"] == "1") {
			$this->CurrentAction = "D"; // Delete record directly
		} else {
			$this->CurrentAction = "D"; // Delete record directly
		}
		if ($this->CurrentAction == "D") {
			$this->SendEmail = TRUE; // Send email on delete success
			if ($this->DeleteRows()) { // Delete rows
				if ($this->getSuccessMessage() == "")
					$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			} else { // Delete failed
				$this->Page_Terminate($this->getReturnUrl()); // Return to caller
			}
		}
		if ($this->CurrentAction == "I") { // Load records for display
			if ($this->Recordset = $this->LoadRecordset())
				$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
			if ($this->TotalRecs <= 0) { // No record found, exit
				if ($this->Recordset)
					$this->Recordset->Close();
				$this->Page_Terminate("notilinierslist.php"); // Return to list
			}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->bajada->setDbValue($rs->fields('bajada'));
		$this->cuerpo->setDbValue($rs->fields('cuerpo'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->epigrafe_1->setDbValue($rs->fields('epigrafe_1'));
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->epigrafe_2->setDbValue($rs->fields('epigrafe_2'));
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->epigrafe_3->setDbValue($rs->fields('epigrafe_3'));
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->epigrafe_4->setDbValue($rs->fields('epigrafe_4'));
		$this->galeria_id->setDbValue($rs->fields('galeria_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->activa->DbValue = $row['activa'];
		$this->titulo->DbValue = $row['titulo'];
		$this->bajada->DbValue = $row['bajada'];
		$this->cuerpo->DbValue = $row['cuerpo'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->epigrafe_1->DbValue = $row['epigrafe_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->epigrafe_2->DbValue = $row['epigrafe_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->epigrafe_3->DbValue = $row['epigrafe_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->epigrafe_4->DbValue = $row['epigrafe_4'];
		$this->galeria_id->DbValue = $row['galeria_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// fecha
		// activa
		// titulo
		// bajada
		// cuerpo
		// foto_1
		// epigrafe_1
		// foto_2
		// epigrafe_2
		// foto_3
		// epigrafe_3
		// foto_4
		// epigrafe_4
		// galeria_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// bajada
		$this->bajada->ViewValue = $this->bajada->CurrentValue;
		$this->bajada->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// epigrafe_1
		$this->epigrafe_1->ViewValue = $this->epigrafe_1->CurrentValue;
		$this->epigrafe_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// epigrafe_2
		$this->epigrafe_2->ViewValue = $this->epigrafe_2->CurrentValue;
		$this->epigrafe_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// epigrafe_3
		$this->epigrafe_3->ViewValue = $this->epigrafe_3->CurrentValue;
		$this->epigrafe_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// epigrafe_4
		$this->epigrafe_4->ViewValue = $this->epigrafe_4->CurrentValue;
		$this->epigrafe_4->ViewCustomAttributes = "";

		// galeria_id
		if (strval($this->galeria_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->galeria_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `galerias_imagenes`";
		$sWhereWrk = "";
		$this->galeria_id->LookupFilters = array();
		$lookuptblfilter = "`contenido_tipo`='LINIERS'";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->galeria_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->galeria_id->ViewValue = $this->galeria_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->galeria_id->ViewValue = $this->galeria_id->CurrentValue;
			}
		} else {
			$this->galeria_id->ViewValue = NULL;
		}
		$this->galeria_id->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// bajada
			$this->bajada->LinkCustomAttributes = "";
			$this->bajada->HrefValue = "";
			$this->bajada->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// epigrafe_1
			$this->epigrafe_1->LinkCustomAttributes = "";
			$this->epigrafe_1->HrefValue = "";
			$this->epigrafe_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// epigrafe_2
			$this->epigrafe_2->LinkCustomAttributes = "";
			$this->epigrafe_2->HrefValue = "";
			$this->epigrafe_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// epigrafe_3
			$this->epigrafe_3->LinkCustomAttributes = "";
			$this->epigrafe_3->HrefValue = "";
			$this->epigrafe_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// epigrafe_4
			$this->epigrafe_4->LinkCustomAttributes = "";
			$this->epigrafe_4->HrefValue = "";
			$this->epigrafe_4->TooltipValue = "";

			// galeria_id
			$this->galeria_id->LinkCustomAttributes = "";
			$this->galeria_id->HrefValue = "";
			$this->galeria_id->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("notilinierslist.php"), "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($notiliniers_delete)) $notiliniers_delete = new cnotiliniers_delete();

// Page init
$notiliniers_delete->Page_Init();

// Page main
$notiliniers_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$notiliniers_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = fnotiliniersdelete = new ew_Form("fnotiliniersdelete", "delete");

// Form_CustomValidate event
fnotiliniersdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fnotiliniersdelete.ValidateRequired = true;
<?php } else { ?>
fnotiliniersdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fnotiliniersdelete.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fnotiliniersdelete.Lists["x_activa"].Options = <?php echo json_encode($notiliniers->activa->Options()) ?>;
fnotiliniersdelete.Lists["x_galeria_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"galerias_imagenes"};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $notiliniers_delete->ShowPageHeader(); ?>
<?php
$notiliniers_delete->ShowMessage();
?>
<form name="fnotiliniersdelete" id="fnotiliniersdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($notiliniers_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $notiliniers_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="notiliniers">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($notiliniers_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $notiliniers->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($notiliniers->id->Visible) { // id ?>
		<th><span id="elh_notiliniers_id" class="notiliniers_id"><?php echo $notiliniers->id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->fecha->Visible) { // fecha ?>
		<th><span id="elh_notiliniers_fecha" class="notiliniers_fecha"><?php echo $notiliniers->fecha->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->activa->Visible) { // activa ?>
		<th><span id="elh_notiliniers_activa" class="notiliniers_activa"><?php echo $notiliniers->activa->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->titulo->Visible) { // titulo ?>
		<th><span id="elh_notiliniers_titulo" class="notiliniers_titulo"><?php echo $notiliniers->titulo->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->bajada->Visible) { // bajada ?>
		<th><span id="elh_notiliniers_bajada" class="notiliniers_bajada"><?php echo $notiliniers->bajada->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->foto_1->Visible) { // foto_1 ?>
		<th><span id="elh_notiliniers_foto_1" class="notiliniers_foto_1"><?php echo $notiliniers->foto_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->epigrafe_1->Visible) { // epigrafe_1 ?>
		<th><span id="elh_notiliniers_epigrafe_1" class="notiliniers_epigrafe_1"><?php echo $notiliniers->epigrafe_1->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->foto_2->Visible) { // foto_2 ?>
		<th><span id="elh_notiliniers_foto_2" class="notiliniers_foto_2"><?php echo $notiliniers->foto_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->epigrafe_2->Visible) { // epigrafe_2 ?>
		<th><span id="elh_notiliniers_epigrafe_2" class="notiliniers_epigrafe_2"><?php echo $notiliniers->epigrafe_2->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->foto_3->Visible) { // foto_3 ?>
		<th><span id="elh_notiliniers_foto_3" class="notiliniers_foto_3"><?php echo $notiliniers->foto_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->epigrafe_3->Visible) { // epigrafe_3 ?>
		<th><span id="elh_notiliniers_epigrafe_3" class="notiliniers_epigrafe_3"><?php echo $notiliniers->epigrafe_3->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->foto_4->Visible) { // foto_4 ?>
		<th><span id="elh_notiliniers_foto_4" class="notiliniers_foto_4"><?php echo $notiliniers->foto_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->epigrafe_4->Visible) { // epigrafe_4 ?>
		<th><span id="elh_notiliniers_epigrafe_4" class="notiliniers_epigrafe_4"><?php echo $notiliniers->epigrafe_4->FldCaption() ?></span></th>
<?php } ?>
<?php if ($notiliniers->galeria_id->Visible) { // galeria_id ?>
		<th><span id="elh_notiliniers_galeria_id" class="notiliniers_galeria_id"><?php echo $notiliniers->galeria_id->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$notiliniers_delete->RecCnt = 0;
$i = 0;
while (!$notiliniers_delete->Recordset->EOF) {
	$notiliniers_delete->RecCnt++;
	$notiliniers_delete->RowCnt++;

	// Set row properties
	$notiliniers->ResetAttrs();
	$notiliniers->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$notiliniers_delete->LoadRowValues($notiliniers_delete->Recordset);

	// Render row
	$notiliniers_delete->RenderRow();
?>
	<tr<?php echo $notiliniers->RowAttributes() ?>>
<?php if ($notiliniers->id->Visible) { // id ?>
		<td<?php echo $notiliniers->id->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_id" class="notiliniers_id">
<span<?php echo $notiliniers->id->ViewAttributes() ?>>
<?php echo $notiliniers->id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->fecha->Visible) { // fecha ?>
		<td<?php echo $notiliniers->fecha->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_fecha" class="notiliniers_fecha">
<span<?php echo $notiliniers->fecha->ViewAttributes() ?>>
<?php echo $notiliniers->fecha->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->activa->Visible) { // activa ?>
		<td<?php echo $notiliniers->activa->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_activa" class="notiliniers_activa">
<span<?php echo $notiliniers->activa->ViewAttributes() ?>>
<?php echo $notiliniers->activa->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->titulo->Visible) { // titulo ?>
		<td<?php echo $notiliniers->titulo->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_titulo" class="notiliniers_titulo">
<span<?php echo $notiliniers->titulo->ViewAttributes() ?>>
<?php echo $notiliniers->titulo->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->bajada->Visible) { // bajada ?>
		<td<?php echo $notiliniers->bajada->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_bajada" class="notiliniers_bajada">
<span<?php echo $notiliniers->bajada->ViewAttributes() ?>>
<?php echo $notiliniers->bajada->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->foto_1->Visible) { // foto_1 ?>
		<td<?php echo $notiliniers->foto_1->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_foto_1" class="notiliniers_foto_1">
<span<?php echo $notiliniers->foto_1->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_1, $notiliniers->foto_1->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->epigrafe_1->Visible) { // epigrafe_1 ?>
		<td<?php echo $notiliniers->epigrafe_1->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_epigrafe_1" class="notiliniers_epigrafe_1">
<span<?php echo $notiliniers->epigrafe_1->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_1->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->foto_2->Visible) { // foto_2 ?>
		<td<?php echo $notiliniers->foto_2->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_foto_2" class="notiliniers_foto_2">
<span<?php echo $notiliniers->foto_2->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_2, $notiliniers->foto_2->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->epigrafe_2->Visible) { // epigrafe_2 ?>
		<td<?php echo $notiliniers->epigrafe_2->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_epigrafe_2" class="notiliniers_epigrafe_2">
<span<?php echo $notiliniers->epigrafe_2->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_2->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->foto_3->Visible) { // foto_3 ?>
		<td<?php echo $notiliniers->foto_3->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_foto_3" class="notiliniers_foto_3">
<span<?php echo $notiliniers->foto_3->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_3, $notiliniers->foto_3->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->epigrafe_3->Visible) { // epigrafe_3 ?>
		<td<?php echo $notiliniers->epigrafe_3->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_epigrafe_3" class="notiliniers_epigrafe_3">
<span<?php echo $notiliniers->epigrafe_3->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_3->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->foto_4->Visible) { // foto_4 ?>
		<td<?php echo $notiliniers->foto_4->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_foto_4" class="notiliniers_foto_4">
<span<?php echo $notiliniers->foto_4->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($notiliniers->foto_4, $notiliniers->foto_4->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->epigrafe_4->Visible) { // epigrafe_4 ?>
		<td<?php echo $notiliniers->epigrafe_4->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_epigrafe_4" class="notiliniers_epigrafe_4">
<span<?php echo $notiliniers->epigrafe_4->ViewAttributes() ?>>
<?php echo $notiliniers->epigrafe_4->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($notiliniers->galeria_id->Visible) { // galeria_id ?>
		<td<?php echo $notiliniers->galeria_id->CellAttributes() ?>>
<span id="el<?php echo $notiliniers_delete->RowCnt ?>_notiliniers_galeria_id" class="notiliniers_galeria_id">
<span<?php echo $notiliniers->galeria_id->ViewAttributes() ?>>
<?php echo $notiliniers->galeria_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$notiliniers_delete->Recordset->MoveNext();
}
$notiliniers_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $notiliniers_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fnotiliniersdelete.Init();
</script>
<?php
$notiliniers_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$notiliniers_delete->Page_Terminate();
?>
