<?php

// Global variable for table object
$home_pautas = NULL;

//
// Table class for home_pautas
//
class chome_pautas extends cTable {
	var $id;
	var $activa;
	var $fecha_desde;
	var $fecha_hasta;
	var $remate_1;
	var $remate_2;
	var $remate_3;
	var $remate_4;
	var $vivolink;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'home_pautas';
		$this->TableName = 'home_pautas';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`home_pautas`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = TRUE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 20;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('home_pautas', 'home_pautas', 'x_id', 'id', '`id`', '`id`', 19, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'CHECKBOX');
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// activa
		$this->activa = new cField('home_pautas', 'home_pautas', 'x_activa', 'activa', '`activa`', '`activa`', 202, -1, FALSE, '`activa`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->activa->Sortable = TRUE; // Allow sort
		$this->activa->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->activa->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->activa->OptionCount = 2;
		$this->fields['activa'] = &$this->activa;

		// fecha_desde
		$this->fecha_desde = new cField('home_pautas', 'home_pautas', 'x_fecha_desde', 'fecha_desde', '`fecha_desde`', ew_CastDateFieldForLike('`fecha_desde`', 0, "DB"), 133, 0, FALSE, '`fecha_desde`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fecha_desde->Sortable = TRUE; // Allow sort
		$this->fecha_desde->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_DATE_FORMAT"], $Language->Phrase("IncorrectDate"));
		$this->fields['fecha_desde'] = &$this->fecha_desde;

		// fecha_hasta
		$this->fecha_hasta = new cField('home_pautas', 'home_pautas', 'x_fecha_hasta', 'fecha_hasta', '`fecha_hasta`', ew_CastDateFieldForLike('`fecha_hasta`', 0, "DB"), 133, 0, FALSE, '`fecha_hasta`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fecha_hasta->Sortable = TRUE; // Allow sort
		$this->fecha_hasta->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_DATE_FORMAT"], $Language->Phrase("IncorrectDate"));
		$this->fields['fecha_hasta'] = &$this->fecha_hasta;

		// remate_1
		$this->remate_1 = new cField('home_pautas', 'home_pautas', 'x_remate_1', 'remate_1', '`remate_1`', '`remate_1`', 19, -1, FALSE, '`remate_1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->remate_1->Sortable = TRUE; // Allow sort
		$this->remate_1->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->remate_1->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->remate_1->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['remate_1'] = &$this->remate_1;

		// remate_2
		$this->remate_2 = new cField('home_pautas', 'home_pautas', 'x_remate_2', 'remate_2', '`remate_2`', '`remate_2`', 19, -1, FALSE, '`remate_2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->remate_2->Sortable = TRUE; // Allow sort
		$this->remate_2->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->remate_2->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->remate_2->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['remate_2'] = &$this->remate_2;

		// remate_3
		$this->remate_3 = new cField('home_pautas', 'home_pautas', 'x_remate_3', 'remate_3', '`remate_3`', '`remate_3`', 19, -1, FALSE, '`remate_3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->remate_3->Sortable = TRUE; // Allow sort
		$this->remate_3->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->remate_3->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->remate_3->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['remate_3'] = &$this->remate_3;

		// remate_4
		$this->remate_4 = new cField('home_pautas', 'home_pautas', 'x_remate_4', 'remate_4', '`remate_4`', '`remate_4`', 19, -1, FALSE, '`remate_4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->remate_4->Sortable = TRUE; // Allow sort
		$this->remate_4->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->remate_4->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->remate_4->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['remate_4'] = &$this->remate_4;

		// vivolink
		$this->vivolink = new cField('home_pautas', 'home_pautas', 'x_vivolink', 'vivolink', '`vivolink`', '`vivolink`', 200, -1, FALSE, '`vivolink`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->vivolink->Sortable = TRUE; // Allow sort
		$this->fields['vivolink'] = &$this->vivolink;
	}

	// Set Field Visibility
	function SetFieldVisibility($fldparm) {
		global $Security;
		return $this->$fldparm->Visible; // Returns original value
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`home_pautas`";
	}

	function SqlFrom() { // For backward compatibility
		return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
		$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
		return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
		$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
		return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
		$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
		return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
		$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
		return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
		$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`fecha_desde` DESC";
	}

	function SqlOrderBy() { // For backward compatibility
		return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
		$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 8) == 8);
			case "search":
				return (($allow & 8) == 8);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id', $this->DBID) . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "home_pautaslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "home_pautaslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("home_pautasview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("home_pautasview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "home_pautasadd.php?" . $this->UrlParm($parm);
		else
			$url = "home_pautasadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("home_pautasedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("home_pautasadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("home_pautasdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "id:" . ew_VarToJson($this->id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return $this->AddMasterUrl(ew_CurrentPage() . "?" . $sUrlParm);
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["id"]))
				$arKeys[] = ew_StripSlashes($_POST["id"]);
			elseif (isset($_GET["id"]))
				$arKeys[] = ew_StripSlashes($_GET["id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->fecha_desde->setDbValue($rs->fields('fecha_desde'));
		$this->fecha_hasta->setDbValue($rs->fields('fecha_hasta'));
		$this->remate_1->setDbValue($rs->fields('remate_1'));
		$this->remate_2->setDbValue($rs->fields('remate_2'));
		$this->remate_3->setDbValue($rs->fields('remate_3'));
		$this->remate_4->setDbValue($rs->fields('remate_4'));
		$this->vivolink->setDbValue($rs->fields('vivolink'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// activa
		// fecha_desde
		// fecha_hasta
		// remate_1
		// remate_2
		// remate_3
		// remate_4
		// vivolink
		// id

		$this->id->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// fecha_desde
		$this->fecha_desde->ViewValue = $this->fecha_desde->CurrentValue;
		$this->fecha_desde->ViewValue = ew_FormatDateTime($this->fecha_desde->ViewValue, 0);
		$this->fecha_desde->ViewCustomAttributes = "";

		// fecha_hasta
		$this->fecha_hasta->ViewValue = $this->fecha_hasta->CurrentValue;
		$this->fecha_hasta->ViewValue = ew_FormatDateTime($this->fecha_hasta->ViewValue, 0);
		$this->fecha_hasta->ViewCustomAttributes = "";

		// remate_1
		if (strval($this->remate_1->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_1->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_1->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_1, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_1->ViewValue = $this->remate_1->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_1->ViewValue = $this->remate_1->CurrentValue;
			}
		} else {
			$this->remate_1->ViewValue = NULL;
		}
		$this->remate_1->ViewCustomAttributes = "";

		// remate_2
		if (strval($this->remate_2->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_2->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_2->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_2, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_2->ViewValue = $this->remate_2->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_2->ViewValue = $this->remate_2->CurrentValue;
			}
		} else {
			$this->remate_2->ViewValue = NULL;
		}
		$this->remate_2->ViewCustomAttributes = "";

		// remate_3
		if (strval($this->remate_3->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_3->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_3->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_3, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_3->ViewValue = $this->remate_3->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_3->ViewValue = $this->remate_3->CurrentValue;
			}
		} else {
			$this->remate_3->ViewValue = NULL;
		}
		$this->remate_3->ViewCustomAttributes = "";

		// remate_4
		if (strval($this->remate_4->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_4->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, `fecha` AS `Disp2Fld`, `lugar` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_4->LookupFilters = array("df2" => "0");
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_4, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `numero` DESC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = ew_FormatDateTime($rswrk->fields('Disp2Fld'), 0);
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->remate_4->ViewValue = $this->remate_4->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_4->ViewValue = $this->remate_4->CurrentValue;
			}
		} else {
			$this->remate_4->ViewValue = NULL;
		}
		$this->remate_4->ViewCustomAttributes = "";

		// vivolink
		$this->vivolink->ViewValue = $this->vivolink->CurrentValue;
		$this->vivolink->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// activa
		$this->activa->LinkCustomAttributes = "";
		$this->activa->HrefValue = "";
		$this->activa->TooltipValue = "";

		// fecha_desde
		$this->fecha_desde->LinkCustomAttributes = "";
		$this->fecha_desde->HrefValue = "";
		$this->fecha_desde->TooltipValue = "";

		// fecha_hasta
		$this->fecha_hasta->LinkCustomAttributes = "";
		$this->fecha_hasta->HrefValue = "";
		$this->fecha_hasta->TooltipValue = "";

		// remate_1
		$this->remate_1->LinkCustomAttributes = "";
		$this->remate_1->HrefValue = "";
		$this->remate_1->TooltipValue = "";

		// remate_2
		$this->remate_2->LinkCustomAttributes = "";
		$this->remate_2->HrefValue = "";
		$this->remate_2->TooltipValue = "";

		// remate_3
		$this->remate_3->LinkCustomAttributes = "";
		$this->remate_3->HrefValue = "";
		$this->remate_3->TooltipValue = "";

		// remate_4
		$this->remate_4->LinkCustomAttributes = "";
		$this->remate_4->HrefValue = "";
		$this->remate_4->TooltipValue = "";

		// vivolink
		$this->vivolink->LinkCustomAttributes = "";
		$this->vivolink->HrefValue = "";
		$this->vivolink->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->ViewCustomAttributes = "";

		// activa
		$this->activa->EditAttrs["class"] = "form-control";
		$this->activa->EditCustomAttributes = "";
		$this->activa->EditValue = $this->activa->Options(TRUE);

		// fecha_desde
		$this->fecha_desde->EditAttrs["class"] = "form-control";
		$this->fecha_desde->EditCustomAttributes = "";
		$this->fecha_desde->EditValue = ew_FormatDateTime($this->fecha_desde->CurrentValue, 8);
		$this->fecha_desde->PlaceHolder = ew_RemoveHtml($this->fecha_desde->FldCaption());

		// fecha_hasta
		$this->fecha_hasta->EditAttrs["class"] = "form-control";
		$this->fecha_hasta->EditCustomAttributes = "";
		$this->fecha_hasta->EditValue = ew_FormatDateTime($this->fecha_hasta->CurrentValue, 8);
		$this->fecha_hasta->PlaceHolder = ew_RemoveHtml($this->fecha_hasta->FldCaption());

		// remate_1
		$this->remate_1->EditAttrs["class"] = "form-control";
		$this->remate_1->EditCustomAttributes = "";

		// remate_2
		$this->remate_2->EditAttrs["class"] = "form-control";
		$this->remate_2->EditCustomAttributes = "";

		// remate_3
		$this->remate_3->EditAttrs["class"] = "form-control";
		$this->remate_3->EditCustomAttributes = "";

		// remate_4
		$this->remate_4->EditAttrs["class"] = "form-control";
		$this->remate_4->EditCustomAttributes = "";

		// vivolink
		$this->vivolink->EditAttrs["class"] = "form-control";
		$this->vivolink->EditCustomAttributes = "";
		$this->vivolink->EditValue = $this->vivolink->CurrentValue;
		$this->vivolink->PlaceHolder = ew_RemoveHtml($this->vivolink->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->activa->Exportable) $Doc->ExportCaption($this->activa);
					if ($this->fecha_desde->Exportable) $Doc->ExportCaption($this->fecha_desde);
					if ($this->fecha_hasta->Exportable) $Doc->ExportCaption($this->fecha_hasta);
					if ($this->remate_1->Exportable) $Doc->ExportCaption($this->remate_1);
					if ($this->remate_2->Exportable) $Doc->ExportCaption($this->remate_2);
					if ($this->remate_3->Exportable) $Doc->ExportCaption($this->remate_3);
					if ($this->remate_4->Exportable) $Doc->ExportCaption($this->remate_4);
					if ($this->vivolink->Exportable) $Doc->ExportCaption($this->vivolink);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->activa->Exportable) $Doc->ExportCaption($this->activa);
					if ($this->fecha_desde->Exportable) $Doc->ExportCaption($this->fecha_desde);
					if ($this->fecha_hasta->Exportable) $Doc->ExportCaption($this->fecha_hasta);
					if ($this->remate_1->Exportable) $Doc->ExportCaption($this->remate_1);
					if ($this->remate_2->Exportable) $Doc->ExportCaption($this->remate_2);
					if ($this->remate_3->Exportable) $Doc->ExportCaption($this->remate_3);
					if ($this->remate_4->Exportable) $Doc->ExportCaption($this->remate_4);
					if ($this->vivolink->Exportable) $Doc->ExportCaption($this->vivolink);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->activa->Exportable) $Doc->ExportField($this->activa);
						if ($this->fecha_desde->Exportable) $Doc->ExportField($this->fecha_desde);
						if ($this->fecha_hasta->Exportable) $Doc->ExportField($this->fecha_hasta);
						if ($this->remate_1->Exportable) $Doc->ExportField($this->remate_1);
						if ($this->remate_2->Exportable) $Doc->ExportField($this->remate_2);
						if ($this->remate_3->Exportable) $Doc->ExportField($this->remate_3);
						if ($this->remate_4->Exportable) $Doc->ExportField($this->remate_4);
						if ($this->vivolink->Exportable) $Doc->ExportField($this->vivolink);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->activa->Exportable) $Doc->ExportField($this->activa);
						if ($this->fecha_desde->Exportable) $Doc->ExportField($this->fecha_desde);
						if ($this->fecha_hasta->Exportable) $Doc->ExportField($this->fecha_hasta);
						if ($this->remate_1->Exportable) $Doc->ExportField($this->remate_1);
						if ($this->remate_2->Exportable) $Doc->ExportField($this->remate_2);
						if ($this->remate_3->Exportable) $Doc->ExportField($this->remate_3);
						if ($this->remate_4->Exportable) $Doc->ExportField($this->remate_4);
						if ($this->vivolink->Exportable) $Doc->ExportField($this->vivolink);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
