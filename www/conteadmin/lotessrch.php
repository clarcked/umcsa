<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_search = NULL; // Initialize page object first

class clotes_search extends clotes {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("loteslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->observaciones->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "loteslist.php" . "?" . $sSrchStr;
						$this->Page_Terminate($sSrchStr); // Go to list page
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->id); // id
		$this->BuildSearchUrl($sSrchUrl, $this->venta_tipo); // venta_tipo
		$this->BuildSearchUrl($sSrchUrl, $this->activo); // activo
		$this->BuildSearchUrl($sSrchUrl, $this->vendido); // vendido
		$this->BuildSearchUrl($sSrchUrl, $this->codigo); // codigo
		$this->BuildSearchUrl($sSrchUrl, $this->remate_id); // remate_id
		$this->BuildSearchUrl($sSrchUrl, $this->conjunto_lote_id); // conjunto_lote_id
		$this->BuildSearchUrl($sSrchUrl, $this->orden); // orden
		$this->BuildSearchUrl($sSrchUrl, $this->video); // video
		$this->BuildSearchUrl($sSrchUrl, $this->foto_1); // foto_1
		$this->BuildSearchUrl($sSrchUrl, $this->foto_2); // foto_2
		$this->BuildSearchUrl($sSrchUrl, $this->foto_3); // foto_3
		$this->BuildSearchUrl($sSrchUrl, $this->foto_4); // foto_4
		$this->BuildSearchUrl($sSrchUrl, $this->precio); // precio
		$this->BuildSearchUrl($sSrchUrl, $this->fecha_film); // fecha_film
		$this->BuildSearchUrl($sSrchUrl, $this->establecimiento); // establecimiento
		$this->BuildSearchUrl($sSrchUrl, $this->remitente); // remitente
		$this->BuildSearchUrl($sSrchUrl, $this->tipo); // tipo
		$this->BuildSearchUrl($sSrchUrl, $this->categoria); // categoria
		$this->BuildSearchUrl($sSrchUrl, $this->sub_categoria); // sub_categoria
		$this->BuildSearchUrl($sSrchUrl, $this->cantidad); // cantidad
		$this->BuildSearchUrl($sSrchUrl, $this->peso); // peso
		$this->BuildSearchUrl($sSrchUrl, $this->caracteristicas); // caracteristicas
		$this->BuildSearchUrl($sSrchUrl, $this->provincia); // provincia
		$this->BuildSearchUrl($sSrchUrl, $this->localidad); // localidad
		$this->BuildSearchUrl($sSrchUrl, $this->lugar); // lugar
		$this->BuildSearchUrl($sSrchUrl, $this->titulo); // titulo
		$this->BuildSearchUrl($sSrchUrl, $this->edad_aprox); // edad_aprox
		$this->BuildSearchUrl($sSrchUrl, $this->trazabilidad); // trazabilidad
		$this->BuildSearchUrl($sSrchUrl, $this->mio_mio); // mio_mio
		$this->BuildSearchUrl($sSrchUrl, $this->garrapata); // garrapata
		$this->BuildSearchUrl($sSrchUrl, $this->observaciones); // observaciones
		$this->BuildSearchUrl($sSrchUrl, $this->plazo); // plazo
		$this->BuildSearchUrl($sSrchUrl, $this->visitas); // visitas
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// id

		$this->id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_id"));
		$this->id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_id");

		// venta_tipo
		$this->venta_tipo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_venta_tipo"));
		$this->venta_tipo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_venta_tipo");

		// activo
		$this->activo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_activo"));
		$this->activo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_activo");

		// vendido
		$this->vendido->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_vendido"));
		$this->vendido->AdvancedSearch->SearchOperator = $objForm->GetValue("z_vendido");

		// codigo
		$this->codigo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_codigo"));
		$this->codigo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_codigo");

		// remate_id
		$this->remate_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_remate_id"));
		$this->remate_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_remate_id");

		// conjunto_lote_id
		$this->conjunto_lote_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_conjunto_lote_id"));
		$this->conjunto_lote_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_conjunto_lote_id");

		// orden
		$this->orden->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_orden"));
		$this->orden->AdvancedSearch->SearchOperator = $objForm->GetValue("z_orden");

		// video
		$this->video->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_video"));
		$this->video->AdvancedSearch->SearchOperator = $objForm->GetValue("z_video");

		// foto_1
		$this->foto_1->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_foto_1"));
		$this->foto_1->AdvancedSearch->SearchOperator = $objForm->GetValue("z_foto_1");

		// foto_2
		$this->foto_2->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_foto_2"));
		$this->foto_2->AdvancedSearch->SearchOperator = $objForm->GetValue("z_foto_2");

		// foto_3
		$this->foto_3->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_foto_3"));
		$this->foto_3->AdvancedSearch->SearchOperator = $objForm->GetValue("z_foto_3");

		// foto_4
		$this->foto_4->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_foto_4"));
		$this->foto_4->AdvancedSearch->SearchOperator = $objForm->GetValue("z_foto_4");

		// precio
		$this->precio->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_precio"));
		$this->precio->AdvancedSearch->SearchOperator = $objForm->GetValue("z_precio");

		// fecha_film
		$this->fecha_film->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_fecha_film"));
		$this->fecha_film->AdvancedSearch->SearchOperator = $objForm->GetValue("z_fecha_film");

		// establecimiento
		$this->establecimiento->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_establecimiento"));
		$this->establecimiento->AdvancedSearch->SearchOperator = $objForm->GetValue("z_establecimiento");

		// remitente
		$this->remitente->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_remitente"));
		$this->remitente->AdvancedSearch->SearchOperator = $objForm->GetValue("z_remitente");

		// tipo
		$this->tipo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_tipo"));
		$this->tipo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_tipo");

		// categoria
		$this->categoria->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_categoria"));
		$this->categoria->AdvancedSearch->SearchOperator = $objForm->GetValue("z_categoria");

		// sub_categoria
		$this->sub_categoria->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_sub_categoria"));
		$this->sub_categoria->AdvancedSearch->SearchOperator = $objForm->GetValue("z_sub_categoria");

		// cantidad
		$this->cantidad->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_cantidad"));
		$this->cantidad->AdvancedSearch->SearchOperator = $objForm->GetValue("z_cantidad");

		// peso
		$this->peso->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_peso"));
		$this->peso->AdvancedSearch->SearchOperator = $objForm->GetValue("z_peso");

		// caracteristicas
		$this->caracteristicas->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_caracteristicas"));
		$this->caracteristicas->AdvancedSearch->SearchOperator = $objForm->GetValue("z_caracteristicas");

		// provincia
		$this->provincia->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_provincia"));
		$this->provincia->AdvancedSearch->SearchOperator = $objForm->GetValue("z_provincia");

		// localidad
		$this->localidad->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_localidad"));
		$this->localidad->AdvancedSearch->SearchOperator = $objForm->GetValue("z_localidad");

		// lugar
		$this->lugar->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_lugar"));
		$this->lugar->AdvancedSearch->SearchOperator = $objForm->GetValue("z_lugar");

		// titulo
		$this->titulo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_titulo"));
		$this->titulo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_titulo");

		// edad_aprox
		$this->edad_aprox->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_edad_aprox"));
		$this->edad_aprox->AdvancedSearch->SearchOperator = $objForm->GetValue("z_edad_aprox");

		// trazabilidad
		$this->trazabilidad->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_trazabilidad"));
		$this->trazabilidad->AdvancedSearch->SearchOperator = $objForm->GetValue("z_trazabilidad");

		// mio_mio
		$this->mio_mio->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_mio_mio"));
		$this->mio_mio->AdvancedSearch->SearchOperator = $objForm->GetValue("z_mio_mio");

		// garrapata
		$this->garrapata->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_garrapata"));
		$this->garrapata->AdvancedSearch->SearchOperator = $objForm->GetValue("z_garrapata");

		// observaciones
		$this->observaciones->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_observaciones"));
		$this->observaciones->AdvancedSearch->SearchOperator = $objForm->GetValue("z_observaciones");

		// plazo
		$this->plazo->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_plazo"));
		$this->plazo->AdvancedSearch->SearchOperator = $objForm->GetValue("z_plazo");

		// visitas
		$this->visitas->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_visitas"));
		$this->visitas->AdvancedSearch->SearchOperator = $objForm->GetValue("z_visitas");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// observaciones
		$this->observaciones->ViewValue = $this->observaciones->CurrentValue;
		$this->observaciones->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";
			$this->observaciones->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = ew_HtmlEncode($this->id->AdvancedSearch->SearchValue);
			$this->id->PlaceHolder = ew_RemoveHtml($this->id->FldCaption());

			// venta_tipo
			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->AdvancedSearch->SearchValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			$this->remate_id->EditValue = ew_HtmlEncode($this->remate_id->AdvancedSearch->SearchValue);
			$this->remate_id->PlaceHolder = ew_RemoveHtml($this->remate_id->FldCaption());

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			$this->conjunto_lote_id->EditValue = ew_HtmlEncode($this->conjunto_lote_id->AdvancedSearch->SearchValue);
			$this->conjunto_lote_id->PlaceHolder = ew_RemoveHtml($this->conjunto_lote_id->FldCaption());

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->AdvancedSearch->SearchValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->AdvancedSearch->SearchValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			$this->foto_1->EditValue = ew_HtmlEncode($this->foto_1->AdvancedSearch->SearchValue);
			$this->foto_1->PlaceHolder = ew_RemoveHtml($this->foto_1->FldCaption());

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			$this->foto_2->EditValue = ew_HtmlEncode($this->foto_2->AdvancedSearch->SearchValue);
			$this->foto_2->PlaceHolder = ew_RemoveHtml($this->foto_2->FldCaption());

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			$this->foto_3->EditValue = ew_HtmlEncode($this->foto_3->AdvancedSearch->SearchValue);
			$this->foto_3->PlaceHolder = ew_RemoveHtml($this->foto_3->FldCaption());

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			$this->foto_4->EditValue = ew_HtmlEncode($this->foto_4->AdvancedSearch->SearchValue);
			$this->foto_4->PlaceHolder = ew_RemoveHtml($this->foto_4->FldCaption());

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->AdvancedSearch->SearchValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->fecha_film->AdvancedSearch->SearchValue, 0), 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->AdvancedSearch->SearchValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->AdvancedSearch->SearchValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			$this->tipo->EditValue = ew_HtmlEncode($this->tipo->AdvancedSearch->SearchValue);
			$this->tipo->PlaceHolder = ew_RemoveHtml($this->tipo->FldCaption());

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			$this->categoria->EditValue = ew_HtmlEncode($this->categoria->AdvancedSearch->SearchValue);
			$this->categoria->PlaceHolder = ew_RemoveHtml($this->categoria->FldCaption());

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			$this->sub_categoria->EditValue = ew_HtmlEncode($this->sub_categoria->AdvancedSearch->SearchValue);
			$this->sub_categoria->PlaceHolder = ew_RemoveHtml($this->sub_categoria->FldCaption());

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->AdvancedSearch->SearchValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->AdvancedSearch->SearchValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->AdvancedSearch->SearchValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";
			if (trim(strval($this->provincia->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->provincia->EditValue = $arwrk;

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->AdvancedSearch->SearchValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->AdvancedSearch->SearchValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->AdvancedSearch->SearchValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->AdvancedSearch->SearchValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// observaciones
			$this->observaciones->EditAttrs["class"] = "form-control";
			$this->observaciones->EditCustomAttributes = "";
			$this->observaciones->EditValue = ew_HtmlEncode($this->observaciones->AdvancedSearch->SearchValue);
			$this->observaciones->PlaceHolder = ew_RemoveHtml($this->observaciones->FldCaption());

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->AdvancedSearch->SearchValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->AdvancedSearch->SearchValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->id->FldErrMsg());
		}
		if (!ew_CheckNumber($this->precio->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->precio->FldErrMsg());
		}
		if (!ew_CheckDateDef($this->fecha_film->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->fecha_film->FldErrMsg());
		}
		if (!ew_CheckInteger($this->cantidad->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->cantidad->FldErrMsg());
		}
		if (!ew_CheckInteger($this->visitas->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->visitas->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->id->AdvancedSearch->Load();
		$this->venta_tipo->AdvancedSearch->Load();
		$this->activo->AdvancedSearch->Load();
		$this->vendido->AdvancedSearch->Load();
		$this->codigo->AdvancedSearch->Load();
		$this->remate_id->AdvancedSearch->Load();
		$this->conjunto_lote_id->AdvancedSearch->Load();
		$this->orden->AdvancedSearch->Load();
		$this->video->AdvancedSearch->Load();
		$this->foto_1->AdvancedSearch->Load();
		$this->foto_2->AdvancedSearch->Load();
		$this->foto_3->AdvancedSearch->Load();
		$this->foto_4->AdvancedSearch->Load();
		$this->precio->AdvancedSearch->Load();
		$this->fecha_film->AdvancedSearch->Load();
		$this->establecimiento->AdvancedSearch->Load();
		$this->remitente->AdvancedSearch->Load();
		$this->tipo->AdvancedSearch->Load();
		$this->categoria->AdvancedSearch->Load();
		$this->sub_categoria->AdvancedSearch->Load();
		$this->cantidad->AdvancedSearch->Load();
		$this->peso->AdvancedSearch->Load();
		$this->caracteristicas->AdvancedSearch->Load();
		$this->provincia->AdvancedSearch->Load();
		$this->localidad->AdvancedSearch->Load();
		$this->lugar->AdvancedSearch->Load();
		$this->titulo->AdvancedSearch->Load();
		$this->edad_aprox->AdvancedSearch->Load();
		$this->trazabilidad->AdvancedSearch->Load();
		$this->mio_mio->AdvancedSearch->Load();
		$this->garrapata->AdvancedSearch->Load();
		$this->observaciones->AdvancedSearch->Load();
		$this->plazo->AdvancedSearch->Load();
		$this->visitas->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("loteslist.php"), "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_remate_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_conjunto_lote_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
			$sWhereWrk = "{filter}";
			$this->conjunto_lote_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`remate_id` IN ({filter_value})', "t1" => "19", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_tipo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_sub_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_provincia":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
			$sWhereWrk = "{filter}";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`provincia_id` IN ({filter_value})', "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld` FROM `localidades`";
			$sWhereWrk = "(`nombre` LIKE '{query_value}%') AND ({filter})";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f1" => "`provincia_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_search)) $lotes_search = new clotes_search();

// Page init
$lotes_search->Page_Init();

// Page main
$lotes_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($lotes_search->IsModal) { ?>
var CurrentAdvancedSearchForm = flotessearch = new ew_Form("flotessearch", "search");
<?php } else { ?>
var CurrentForm = flotessearch = new ew_Form("flotessearch", "search");
<?php } ?>

// Form_CustomValidate event
flotessearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
flotessearch.ValidateRequired = true;
<?php } else { ?>
flotessearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
flotessearch.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
flotessearch.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
flotessearch.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
flotessearch.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
flotessearch.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":["x_remate_id"],"ChildFields":[],"FilterFields":["x_remate_id"],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
flotessearch.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
flotessearch.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
flotessearch.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
flotessearch.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
flotessearch.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":["x_provincia"],"ChildFields":[],"FilterFields":["x_provincia_id"],"Options":[],"Template":"","LinkTable":"localidades"};
flotessearch.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
flotessearch.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
flotessearch.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotessearch.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
// Validate function for search

flotessearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_precio");
	if (elm && !ew_CheckNumber(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->precio->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_fecha_film");
	if (elm && !ew_CheckDateDef(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->fecha_film->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_cantidad");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->cantidad->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_visitas");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->visitas->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$lotes_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $lotes_search->ShowPageHeader(); ?>
<?php
$lotes_search->ShowMessage();
?>
<form name="flotessearch" id="flotessearch" class="<?php echo $lotes_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($lotes_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($lotes->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label for="x_id" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_id"><?php echo $lotes->id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_id" id="z_id" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->id->CellAttributes() ?>>
			<span id="el_lotes_id">
<input type="text" data-table="lotes" data-field="x_id" name="x_id" id="x_id" placeholder="<?php echo ew_HtmlEncode($lotes->id->getPlaceHolder()) ?>" value="<?php echo $lotes->id->EditValue ?>"<?php echo $lotes->id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
	<div id="r_venta_tipo" class="form-group">
		<label for="x_venta_tipo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_venta_tipo"><?php echo $lotes->venta_tipo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_venta_tipo" id="z_venta_tipo" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->venta_tipo->CellAttributes() ?>>
			<span id="el_lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x_venta_tipo" name="x_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x_venta_tipo") ?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
	<div id="r_activo" class="form-group">
		<label for="x_activo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_activo"><?php echo $lotes->activo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_activo" id="z_activo" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->activo->CellAttributes() ?>>
			<span id="el_lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x_activo" name="x_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x_activo") ?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<div id="r_vendido" class="form-group">
		<label for="x_vendido" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_vendido"><?php echo $lotes->vendido->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_vendido" id="z_vendido" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->vendido->CellAttributes() ?>>
			<span id="el_lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x_vendido" name="x_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x_vendido") ?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<div id="r_codigo" class="form-group">
		<label for="x_codigo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_codigo"><?php echo $lotes->codigo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_codigo" id="z_codigo" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->codigo->CellAttributes() ?>>
			<span id="el_lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x_codigo" id="x_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<div id="r_remate_id" class="form-group">
		<label for="x_remate_id" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_remate_id"><?php echo $lotes->remate_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_remate_id" id="z_remate_id" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->remate_id->CellAttributes() ?>>
			<span id="el_lotes_remate_id">
<input type="text" data-table="lotes" data-field="x_remate_id" name="x_remate_id" id="x_remate_id" size="10" placeholder="<?php echo ew_HtmlEncode($lotes->remate_id->getPlaceHolder()) ?>" value="<?php echo $lotes->remate_id->EditValue ?>"<?php echo $lotes->remate_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<div id="r_conjunto_lote_id" class="form-group">
		<label for="x_conjunto_lote_id" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_conjunto_lote_id"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_conjunto_lote_id" id="z_conjunto_lote_id" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
			<span id="el_lotes_conjunto_lote_id">
<input type="text" data-table="lotes" data-field="x_conjunto_lote_id" name="x_conjunto_lote_id" id="x_conjunto_lote_id" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->conjunto_lote_id->getPlaceHolder()) ?>" value="<?php echo $lotes->conjunto_lote_id->EditValue ?>"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
	<div id="r_orden" class="form-group">
		<label for="x_orden" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_orden"><?php echo $lotes->orden->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_orden" id="z_orden" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->orden->CellAttributes() ?>>
			<span id="el_lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x_orden" id="x_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
	<div id="r_video" class="form-group">
		<label for="x_video" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_video"><?php echo $lotes->video->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_video" id="z_video" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->video->CellAttributes() ?>>
			<span id="el_lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x_video" id="x_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
	<div id="r_foto_1" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_foto_1"><?php echo $lotes->foto_1->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_foto_1" id="z_foto_1" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->foto_1->CellAttributes() ?>>
			<span id="el_lotes_foto_1">
<input type="text" data-table="lotes" data-field="x_foto_1" name="x_foto_1" id="x_foto_1" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->foto_1->getPlaceHolder()) ?>" value="<?php echo $lotes->foto_1->EditValue ?>"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
	<div id="r_foto_2" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_foto_2"><?php echo $lotes->foto_2->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_foto_2" id="z_foto_2" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->foto_2->CellAttributes() ?>>
			<span id="el_lotes_foto_2">
<input type="text" data-table="lotes" data-field="x_foto_2" name="x_foto_2" id="x_foto_2" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->foto_2->getPlaceHolder()) ?>" value="<?php echo $lotes->foto_2->EditValue ?>"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
	<div id="r_foto_3" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_foto_3"><?php echo $lotes->foto_3->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_foto_3" id="z_foto_3" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->foto_3->CellAttributes() ?>>
			<span id="el_lotes_foto_3">
<input type="text" data-table="lotes" data-field="x_foto_3" name="x_foto_3" id="x_foto_3" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->foto_3->getPlaceHolder()) ?>" value="<?php echo $lotes->foto_3->EditValue ?>"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
	<div id="r_foto_4" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_foto_4"><?php echo $lotes->foto_4->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_foto_4" id="z_foto_4" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->foto_4->CellAttributes() ?>>
			<span id="el_lotes_foto_4">
<input type="text" data-table="lotes" data-field="x_foto_4" name="x_foto_4" id="x_foto_4" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->foto_4->getPlaceHolder()) ?>" value="<?php echo $lotes->foto_4->EditValue ?>"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
	<div id="r_precio" class="form-group">
		<label for="x_precio" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_precio"><?php echo $lotes->precio->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_precio" id="z_precio" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->precio->CellAttributes() ?>>
			<span id="el_lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x_precio" id="x_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
	<div id="r_fecha_film" class="form-group">
		<label for="x_fecha_film" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_fecha_film"><?php echo $lotes->fecha_film->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_fecha_film" id="z_fecha_film" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->fecha_film->CellAttributes() ?>>
			<span id="el_lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x_fecha_film" id="x_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("flotessearch", "x_fecha_film", 0);
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<div id="r_establecimiento" class="form-group">
		<label for="x_establecimiento" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_establecimiento"><?php echo $lotes->establecimiento->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_establecimiento" id="z_establecimiento" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->establecimiento->CellAttributes() ?>>
			<span id="el_lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x_establecimiento" id="x_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<div id="r_remitente" class="form-group">
		<label for="x_remitente" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_remitente"><?php echo $lotes->remitente->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_remitente" id="z_remitente" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->remitente->CellAttributes() ?>>
			<span id="el_lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x_remitente" id="x_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<div id="r_tipo" class="form-group">
		<label for="x_tipo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_tipo"><?php echo $lotes->tipo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_tipo" id="z_tipo" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->tipo->CellAttributes() ?>>
			<span id="el_lotes_tipo">
<input type="text" data-table="lotes" data-field="x_tipo" name="x_tipo" id="x_tipo" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->tipo->getPlaceHolder()) ?>" value="<?php echo $lotes->tipo->EditValue ?>"<?php echo $lotes->tipo->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<div id="r_categoria" class="form-group">
		<label for="x_categoria" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_categoria"><?php echo $lotes->categoria->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_categoria" id="z_categoria" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->categoria->CellAttributes() ?>>
			<span id="el_lotes_categoria">
<input type="text" data-table="lotes" data-field="x_categoria" name="x_categoria" id="x_categoria" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->categoria->getPlaceHolder()) ?>" value="<?php echo $lotes->categoria->EditValue ?>"<?php echo $lotes->categoria->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
	<div id="r_sub_categoria" class="form-group">
		<label for="x_sub_categoria" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_sub_categoria"><?php echo $lotes->sub_categoria->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_sub_categoria" id="z_sub_categoria" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->sub_categoria->CellAttributes() ?>>
			<span id="el_lotes_sub_categoria">
<input type="text" data-table="lotes" data-field="x_sub_categoria" name="x_sub_categoria" id="x_sub_categoria" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->sub_categoria->getPlaceHolder()) ?>" value="<?php echo $lotes->sub_categoria->EditValue ?>"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
	<div id="r_cantidad" class="form-group">
		<label for="x_cantidad" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_cantidad"><?php echo $lotes->cantidad->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_cantidad" id="z_cantidad" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->cantidad->CellAttributes() ?>>
			<span id="el_lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x_cantidad" id="x_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
	<div id="r_peso" class="form-group">
		<label for="x_peso" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_peso"><?php echo $lotes->peso->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_peso" id="z_peso" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->peso->CellAttributes() ?>>
			<span id="el_lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x_peso" id="x_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
	<div id="r_caracteristicas" class="form-group">
		<label for="x_caracteristicas" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_caracteristicas"><?php echo $lotes->caracteristicas->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_caracteristicas" id="z_caracteristicas" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->caracteristicas->CellAttributes() ?>>
			<span id="el_lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x_caracteristicas" id="x_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
	<div id="r_provincia" class="form-group">
		<label for="x_provincia" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_provincia"><?php echo $lotes->provincia->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_provincia" id="z_provincia" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->provincia->CellAttributes() ?>>
			<span id="el_lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x_provincia" name="x_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x_provincia") ?>
</select>
<input type="hidden" name="s_x_provincia" id="s_x_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
	<div id="r_localidad" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_localidad"><?php echo $lotes->localidad->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_localidad" id="z_localidad" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->localidad->CellAttributes() ?>>
			<span id="el_lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x_localidad" style="white-space: nowrap; z-index: 8750">
	<input type="text" name="sv_x_localidad" id="sv_x_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x_localidad" id="x_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->AdvancedSearch->SearchValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x_localidad" id="q_x_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
flotessearch.CreateAutoSuggest({"id":"x_localidad","forceSelect":false});
</script>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
	<div id="r_lugar" class="form-group">
		<label for="x_lugar" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_lugar"><?php echo $lotes->lugar->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_lugar" id="z_lugar" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->lugar->CellAttributes() ?>>
			<span id="el_lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x_lugar" id="x_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
	<div id="r_titulo" class="form-group">
		<label for="x_titulo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_titulo"><?php echo $lotes->titulo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_titulo" id="z_titulo" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->titulo->CellAttributes() ?>>
			<span id="el_lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x_titulo" id="x_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
	<div id="r_edad_aprox" class="form-group">
		<label for="x_edad_aprox" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_edad_aprox"><?php echo $lotes->edad_aprox->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_edad_aprox" id="z_edad_aprox" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->edad_aprox->CellAttributes() ?>>
			<span id="el_lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x_edad_aprox" id="x_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<div id="r_trazabilidad" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_trazabilidad"><?php echo $lotes->trazabilidad->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_trazabilidad" id="z_trazabilidad" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->trazabilidad->CellAttributes() ?>>
			<span id="el_lotes_trazabilidad">
<div id="tp_x_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x_trazabilidad" id="x_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x_trazabilidad") ?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<div id="r_mio_mio" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_mio_mio"><?php echo $lotes->mio_mio->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_mio_mio" id="z_mio_mio" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->mio_mio->CellAttributes() ?>>
			<span id="el_lotes_mio_mio">
<div id="tp_x_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x_mio_mio" id="x_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x_mio_mio") ?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<div id="r_garrapata" class="form-group">
		<label class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_garrapata"><?php echo $lotes->garrapata->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_garrapata" id="z_garrapata" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->garrapata->CellAttributes() ?>>
			<span id="el_lotes_garrapata">
<div id="tp_x_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x_garrapata" id="x_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x_garrapata") ?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->observaciones->Visible) { // observaciones ?>
	<div id="r_observaciones" class="form-group">
		<label for="x_observaciones" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_observaciones"><?php echo $lotes->observaciones->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_observaciones" id="z_observaciones" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->observaciones->CellAttributes() ?>>
			<span id="el_lotes_observaciones">
<input type="text" data-table="lotes" data-field="x_observaciones" name="x_observaciones" id="x_observaciones" size="35" placeholder="<?php echo ew_HtmlEncode($lotes->observaciones->getPlaceHolder()) ?>" value="<?php echo $lotes->observaciones->EditValue ?>"<?php echo $lotes->observaciones->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
	<div id="r_plazo" class="form-group">
		<label for="x_plazo" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_plazo"><?php echo $lotes->plazo->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_plazo" id="z_plazo" value="LIKE"></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->plazo->CellAttributes() ?>>
			<span id="el_lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x_plazo" id="x_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
	<div id="r_visitas" class="form-group">
		<label for="x_visitas" class="<?php echo $lotes_search->SearchLabelClass ?>"><span id="elh_lotes_visitas"><?php echo $lotes->visitas->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_visitas" id="z_visitas" value="="></p>
		</label>
		<div class="<?php echo $lotes_search->SearchRightColumnClass ?>"><div<?php echo $lotes->visitas->CellAttributes() ?>>
			<span id="el_lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x_visitas" id="x_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$lotes_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
flotessearch.Init();
</script>
<?php
$lotes_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$lotes_search->Page_Terminate();
?>
