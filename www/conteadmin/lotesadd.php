<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_add = NULL; // Initialize page object first

class clotes_add extends clotes {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("loteslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->observaciones->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $IsModal = FALSE;
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["id"] != "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->setKey("id", $this->id->CurrentValue); // Set up key
			} else {
				$this->setKey("id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		} else {
			if ($this->CurrentAction == "I") // Load default values for blank record
				$this->LoadDefaultValues();
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("loteslist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "loteslist.php")
						$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
					elseif (ew_GetPageName($sReturnUrl) == "lotesview.php")
						$sReturnUrl = $this->GetViewUrl(); // View page, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->foto_1->Upload->Index = $objForm->Index;
		$this->foto_1->Upload->UploadFile();
		$this->foto_1->CurrentValue = $this->foto_1->Upload->FileName;
		$this->foto_2->Upload->Index = $objForm->Index;
		$this->foto_2->Upload->UploadFile();
		$this->foto_2->CurrentValue = $this->foto_2->Upload->FileName;
		$this->foto_3->Upload->Index = $objForm->Index;
		$this->foto_3->Upload->UploadFile();
		$this->foto_3->CurrentValue = $this->foto_3->Upload->FileName;
		$this->foto_4->Upload->Index = $objForm->Index;
		$this->foto_4->Upload->UploadFile();
		$this->foto_4->CurrentValue = $this->foto_4->Upload->FileName;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->venta_tipo->CurrentValue = "REMATE";
		$this->activo->CurrentValue = "SI";
		$this->vendido->CurrentValue = "NO";
		$this->codigo->CurrentValue = NULL;
		$this->codigo->OldValue = $this->codigo->CurrentValue;
		$this->remate_id->CurrentValue = NULL;
		$this->remate_id->OldValue = $this->remate_id->CurrentValue;
		$this->conjunto_lote_id->CurrentValue = NULL;
		$this->conjunto_lote_id->OldValue = $this->conjunto_lote_id->CurrentValue;
		$this->orden->CurrentValue = NULL;
		$this->orden->OldValue = $this->orden->CurrentValue;
		$this->video->CurrentValue = NULL;
		$this->video->OldValue = $this->video->CurrentValue;
		$this->foto_1->Upload->DbValue = NULL;
		$this->foto_1->OldValue = $this->foto_1->Upload->DbValue;
		$this->foto_1->CurrentValue = NULL; // Clear file related field
		$this->foto_2->Upload->DbValue = NULL;
		$this->foto_2->OldValue = $this->foto_2->Upload->DbValue;
		$this->foto_2->CurrentValue = NULL; // Clear file related field
		$this->foto_3->Upload->DbValue = NULL;
		$this->foto_3->OldValue = $this->foto_3->Upload->DbValue;
		$this->foto_3->CurrentValue = NULL; // Clear file related field
		$this->foto_4->Upload->DbValue = NULL;
		$this->foto_4->OldValue = $this->foto_4->Upload->DbValue;
		$this->foto_4->CurrentValue = NULL; // Clear file related field
		$this->precio->CurrentValue = NULL;
		$this->precio->OldValue = $this->precio->CurrentValue;
		$this->fecha_film->CurrentValue = NULL;
		$this->fecha_film->OldValue = $this->fecha_film->CurrentValue;
		$this->establecimiento->CurrentValue = NULL;
		$this->establecimiento->OldValue = $this->establecimiento->CurrentValue;
		$this->remitente->CurrentValue = NULL;
		$this->remitente->OldValue = $this->remitente->CurrentValue;
		$this->tipo->CurrentValue = NULL;
		$this->tipo->OldValue = $this->tipo->CurrentValue;
		$this->categoria->CurrentValue = NULL;
		$this->categoria->OldValue = $this->categoria->CurrentValue;
		$this->sub_categoria->CurrentValue = NULL;
		$this->sub_categoria->OldValue = $this->sub_categoria->CurrentValue;
		$this->cantidad->CurrentValue = NULL;
		$this->cantidad->OldValue = $this->cantidad->CurrentValue;
		$this->peso->CurrentValue = NULL;
		$this->peso->OldValue = $this->peso->CurrentValue;
		$this->caracteristicas->CurrentValue = NULL;
		$this->caracteristicas->OldValue = $this->caracteristicas->CurrentValue;
		$this->provincia->CurrentValue = NULL;
		$this->provincia->OldValue = $this->provincia->CurrentValue;
		$this->localidad->CurrentValue = NULL;
		$this->localidad->OldValue = $this->localidad->CurrentValue;
		$this->lugar->CurrentValue = NULL;
		$this->lugar->OldValue = $this->lugar->CurrentValue;
		$this->titulo->CurrentValue = NULL;
		$this->titulo->OldValue = $this->titulo->CurrentValue;
		$this->edad_aprox->CurrentValue = NULL;
		$this->edad_aprox->OldValue = $this->edad_aprox->CurrentValue;
		$this->trazabilidad->CurrentValue = NULL;
		$this->trazabilidad->OldValue = $this->trazabilidad->CurrentValue;
		$this->mio_mio->CurrentValue = NULL;
		$this->mio_mio->OldValue = $this->mio_mio->CurrentValue;
		$this->garrapata->CurrentValue = NULL;
		$this->garrapata->OldValue = $this->garrapata->CurrentValue;
		$this->observaciones->CurrentValue = NULL;
		$this->observaciones->OldValue = $this->observaciones->CurrentValue;
		$this->plazo->CurrentValue = NULL;
		$this->plazo->OldValue = $this->plazo->CurrentValue;
		$this->visitas->CurrentValue = NULL;
		$this->visitas->OldValue = $this->visitas->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->venta_tipo->FldIsDetailKey) {
			$this->venta_tipo->setFormValue($objForm->GetValue("x_venta_tipo"));
		}
		if (!$this->activo->FldIsDetailKey) {
			$this->activo->setFormValue($objForm->GetValue("x_activo"));
		}
		if (!$this->vendido->FldIsDetailKey) {
			$this->vendido->setFormValue($objForm->GetValue("x_vendido"));
		}
		if (!$this->codigo->FldIsDetailKey) {
			$this->codigo->setFormValue($objForm->GetValue("x_codigo"));
		}
		if (!$this->remate_id->FldIsDetailKey) {
			$this->remate_id->setFormValue($objForm->GetValue("x_remate_id"));
		}
		if (!$this->conjunto_lote_id->FldIsDetailKey) {
			$this->conjunto_lote_id->setFormValue($objForm->GetValue("x_conjunto_lote_id"));
		}
		if (!$this->orden->FldIsDetailKey) {
			$this->orden->setFormValue($objForm->GetValue("x_orden"));
		}
		if (!$this->video->FldIsDetailKey) {
			$this->video->setFormValue($objForm->GetValue("x_video"));
		}
		if (!$this->precio->FldIsDetailKey) {
			$this->precio->setFormValue($objForm->GetValue("x_precio"));
		}
		if (!$this->fecha_film->FldIsDetailKey) {
			$this->fecha_film->setFormValue($objForm->GetValue("x_fecha_film"));
			$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		}
		if (!$this->establecimiento->FldIsDetailKey) {
			$this->establecimiento->setFormValue($objForm->GetValue("x_establecimiento"));
		}
		if (!$this->remitente->FldIsDetailKey) {
			$this->remitente->setFormValue($objForm->GetValue("x_remitente"));
		}
		if (!$this->tipo->FldIsDetailKey) {
			$this->tipo->setFormValue($objForm->GetValue("x_tipo"));
		}
		if (!$this->categoria->FldIsDetailKey) {
			$this->categoria->setFormValue($objForm->GetValue("x_categoria"));
		}
		if (!$this->sub_categoria->FldIsDetailKey) {
			$this->sub_categoria->setFormValue($objForm->GetValue("x_sub_categoria"));
		}
		if (!$this->cantidad->FldIsDetailKey) {
			$this->cantidad->setFormValue($objForm->GetValue("x_cantidad"));
		}
		if (!$this->peso->FldIsDetailKey) {
			$this->peso->setFormValue($objForm->GetValue("x_peso"));
		}
		if (!$this->caracteristicas->FldIsDetailKey) {
			$this->caracteristicas->setFormValue($objForm->GetValue("x_caracteristicas"));
		}
		if (!$this->provincia->FldIsDetailKey) {
			$this->provincia->setFormValue($objForm->GetValue("x_provincia"));
		}
		if (!$this->localidad->FldIsDetailKey) {
			$this->localidad->setFormValue($objForm->GetValue("x_localidad"));
		}
		if (!$this->lugar->FldIsDetailKey) {
			$this->lugar->setFormValue($objForm->GetValue("x_lugar"));
		}
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		if (!$this->edad_aprox->FldIsDetailKey) {
			$this->edad_aprox->setFormValue($objForm->GetValue("x_edad_aprox"));
		}
		if (!$this->trazabilidad->FldIsDetailKey) {
			$this->trazabilidad->setFormValue($objForm->GetValue("x_trazabilidad"));
		}
		if (!$this->mio_mio->FldIsDetailKey) {
			$this->mio_mio->setFormValue($objForm->GetValue("x_mio_mio"));
		}
		if (!$this->garrapata->FldIsDetailKey) {
			$this->garrapata->setFormValue($objForm->GetValue("x_garrapata"));
		}
		if (!$this->observaciones->FldIsDetailKey) {
			$this->observaciones->setFormValue($objForm->GetValue("x_observaciones"));
		}
		if (!$this->plazo->FldIsDetailKey) {
			$this->plazo->setFormValue($objForm->GetValue("x_plazo"));
		}
		if (!$this->visitas->FldIsDetailKey) {
			$this->visitas->setFormValue($objForm->GetValue("x_visitas"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->venta_tipo->CurrentValue = $this->venta_tipo->FormValue;
		$this->activo->CurrentValue = $this->activo->FormValue;
		$this->vendido->CurrentValue = $this->vendido->FormValue;
		$this->codigo->CurrentValue = $this->codigo->FormValue;
		$this->remate_id->CurrentValue = $this->remate_id->FormValue;
		$this->conjunto_lote_id->CurrentValue = $this->conjunto_lote_id->FormValue;
		$this->orden->CurrentValue = $this->orden->FormValue;
		$this->video->CurrentValue = $this->video->FormValue;
		$this->precio->CurrentValue = $this->precio->FormValue;
		$this->fecha_film->CurrentValue = $this->fecha_film->FormValue;
		$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		$this->establecimiento->CurrentValue = $this->establecimiento->FormValue;
		$this->remitente->CurrentValue = $this->remitente->FormValue;
		$this->tipo->CurrentValue = $this->tipo->FormValue;
		$this->categoria->CurrentValue = $this->categoria->FormValue;
		$this->sub_categoria->CurrentValue = $this->sub_categoria->FormValue;
		$this->cantidad->CurrentValue = $this->cantidad->FormValue;
		$this->peso->CurrentValue = $this->peso->FormValue;
		$this->caracteristicas->CurrentValue = $this->caracteristicas->FormValue;
		$this->provincia->CurrentValue = $this->provincia->FormValue;
		$this->localidad->CurrentValue = $this->localidad->FormValue;
		$this->lugar->CurrentValue = $this->lugar->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->edad_aprox->CurrentValue = $this->edad_aprox->FormValue;
		$this->trazabilidad->CurrentValue = $this->trazabilidad->FormValue;
		$this->mio_mio->CurrentValue = $this->mio_mio->FormValue;
		$this->garrapata->CurrentValue = $this->garrapata->FormValue;
		$this->observaciones->CurrentValue = $this->observaciones->FormValue;
		$this->plazo->CurrentValue = $this->plazo->FormValue;
		$this->visitas->CurrentValue = $this->visitas->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		if (array_key_exists('EV__remate_id', $rs->fields)) {
			$this->remate_id->VirtualValue = $rs->fields('EV__remate_id'); // Set up virtual field value
		} else {
			$this->remate_id->VirtualValue = ""; // Clear value
		}
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		if (array_key_exists('EV__conjunto_lote_id', $rs->fields)) {
			$this->conjunto_lote_id->VirtualValue = $rs->fields('EV__conjunto_lote_id'); // Set up virtual field value
		} else {
			$this->conjunto_lote_id->VirtualValue = ""; // Clear value
		}
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		if (array_key_exists('EV__tipo', $rs->fields)) {
			$this->tipo->VirtualValue = $rs->fields('EV__tipo'); // Set up virtual field value
		} else {
			$this->tipo->VirtualValue = ""; // Clear value
		}
		$this->categoria->setDbValue($rs->fields('categoria'));
		if (array_key_exists('EV__categoria', $rs->fields)) {
			$this->categoria->VirtualValue = $rs->fields('EV__categoria'); // Set up virtual field value
		} else {
			$this->categoria->VirtualValue = ""; // Clear value
		}
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		if (array_key_exists('EV__sub_categoria', $rs->fields)) {
			$this->sub_categoria->VirtualValue = $rs->fields('EV__sub_categoria'); // Set up virtual field value
		} else {
			$this->sub_categoria->VirtualValue = ""; // Clear value
		}
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		if (array_key_exists('EV__localidad', $rs->fields)) {
			$this->localidad->VirtualValue = $rs->fields('EV__localidad'); // Set up virtual field value
		} else {
			$this->localidad->VirtualValue = ""; // Clear value
		}
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->venta_tipo->DbValue = $row['venta_tipo'];
		$this->activo->DbValue = $row['activo'];
		$this->vendido->DbValue = $row['vendido'];
		$this->codigo->DbValue = $row['codigo'];
		$this->remate_id->DbValue = $row['remate_id'];
		$this->conjunto_lote_id->DbValue = $row['conjunto_lote_id'];
		$this->orden->DbValue = $row['orden'];
		$this->video->DbValue = $row['video'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->precio->DbValue = $row['precio'];
		$this->fecha_film->DbValue = $row['fecha_film'];
		$this->establecimiento->DbValue = $row['establecimiento'];
		$this->remitente->DbValue = $row['remitente'];
		$this->tipo->DbValue = $row['tipo'];
		$this->categoria->DbValue = $row['categoria'];
		$this->sub_categoria->DbValue = $row['sub_categoria'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->peso->DbValue = $row['peso'];
		$this->caracteristicas->DbValue = $row['caracteristicas'];
		$this->provincia->DbValue = $row['provincia'];
		$this->localidad->DbValue = $row['localidad'];
		$this->lugar->DbValue = $row['lugar'];
		$this->titulo->DbValue = $row['titulo'];
		$this->edad_aprox->DbValue = $row['edad_aprox'];
		$this->trazabilidad->DbValue = $row['trazabilidad'];
		$this->mio_mio->DbValue = $row['mio_mio'];
		$this->garrapata->DbValue = $row['garrapata'];
		$this->observaciones->DbValue = $row['observaciones'];
		$this->plazo->DbValue = $row['plazo'];
		$this->visitas->DbValue = $row['visitas'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// observaciones
		$this->observaciones->ViewValue = $this->observaciones->CurrentValue;
		$this->observaciones->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";
			$this->observaciones->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// venta_tipo
			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->CurrentValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			if (trim(strval($this->remate_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->remate_id->EditValue = $arwrk;

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			if (trim(strval($this->conjunto_lote_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `remate_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `conjuntos_lotes`";
			$sWhereWrk = "";
			$this->conjunto_lote_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->conjunto_lote_id->EditValue = $arwrk;

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->CurrentValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->CurrentValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_1->Upload->DbValue)) {
				$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
			} else {
				$this->foto_1->EditValue = "";
			}
			if (!ew_Empty($this->foto_1->CurrentValue))
				$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->foto_1);

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_2->Upload->DbValue)) {
				$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
			} else {
				$this->foto_2->EditValue = "";
			}
			if (!ew_Empty($this->foto_2->CurrentValue))
				$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->foto_2);

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_3->Upload->DbValue)) {
				$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
			} else {
				$this->foto_3->EditValue = "";
			}
			if (!ew_Empty($this->foto_3->CurrentValue))
				$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->foto_3);

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_4->Upload->DbValue)) {
				$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
			} else {
				$this->foto_4->EditValue = "";
			}
			if (!ew_Empty($this->foto_4->CurrentValue))
				$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->foto_4);

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->CurrentValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());
			if (strval($this->precio->EditValue) <> "" && is_numeric($this->precio->EditValue)) $this->precio->EditValue = ew_FormatNumber($this->precio->EditValue, -2, -1, -2, 0);

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_film->CurrentValue, 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->CurrentValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->CurrentValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			if (trim(strval($this->tipo->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->tipo->EditValue = $arwrk;

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			if (trim(strval($this->categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->categoria->EditValue = $arwrk;

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			if (trim(strval($this->sub_categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->sub_categoria->EditValue = $arwrk;

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->CurrentValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->CurrentValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";
			if (trim(strval($this->provincia->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->provincia->EditValue = $arwrk;

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->CurrentValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->CurrentValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// observaciones
			$this->observaciones->EditAttrs["class"] = "form-control";
			$this->observaciones->EditCustomAttributes = "";
			$this->observaciones->EditValue = ew_HtmlEncode($this->observaciones->CurrentValue);
			$this->observaciones->PlaceHolder = ew_RemoveHtml($this->observaciones->FldCaption());

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->CurrentValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->CurrentValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());

			// Add refer script
			// venta_tipo

			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckNumber($this->precio->FormValue)) {
			ew_AddMessage($gsFormError, $this->precio->FldErrMsg());
		}
		if (!ew_CheckDateDef($this->fecha_film->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha_film->FldErrMsg());
		}
		if (!$this->categoria->FldIsDetailKey && !is_null($this->categoria->FormValue) && $this->categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->categoria->FldCaption(), $this->categoria->ReqErrMsg));
		}
		if (!$this->sub_categoria->FldIsDetailKey && !is_null($this->sub_categoria->FormValue) && $this->sub_categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->sub_categoria->FldCaption(), $this->sub_categoria->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->cantidad->FormValue)) {
			ew_AddMessage($gsFormError, $this->cantidad->FldErrMsg());
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->visitas->FormValue)) {
			ew_AddMessage($gsFormError, $this->visitas->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// venta_tipo
		$this->venta_tipo->SetDbValueDef($rsnew, $this->venta_tipo->CurrentValue, NULL, strval($this->venta_tipo->CurrentValue) == "");

		// activo
		$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, NULL, strval($this->activo->CurrentValue) == "");

		// vendido
		$this->vendido->SetDbValueDef($rsnew, $this->vendido->CurrentValue, NULL, strval($this->vendido->CurrentValue) == "");

		// codigo
		$this->codigo->SetDbValueDef($rsnew, $this->codigo->CurrentValue, NULL, FALSE);

		// remate_id
		$this->remate_id->SetDbValueDef($rsnew, $this->remate_id->CurrentValue, NULL, FALSE);

		// conjunto_lote_id
		$this->conjunto_lote_id->SetDbValueDef($rsnew, $this->conjunto_lote_id->CurrentValue, NULL, FALSE);

		// orden
		$this->orden->SetDbValueDef($rsnew, $this->orden->CurrentValue, NULL, FALSE);

		// video
		$this->video->SetDbValueDef($rsnew, $this->video->CurrentValue, NULL, FALSE);

		// foto_1
		if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
			$this->foto_1->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_1->Upload->FileName == "") {
				$rsnew['foto_1'] = NULL;
			} else {
				$rsnew['foto_1'] = $this->foto_1->Upload->FileName;
			}
			$this->foto_1->ImageWidth = 750; // Resize width
			$this->foto_1->ImageHeight = 0; // Resize height
		}

		// foto_2
		if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
			$this->foto_2->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_2->Upload->FileName == "") {
				$rsnew['foto_2'] = NULL;
			} else {
				$rsnew['foto_2'] = $this->foto_2->Upload->FileName;
			}
			$this->foto_2->ImageWidth = 750; // Resize width
			$this->foto_2->ImageHeight = 0; // Resize height
		}

		// foto_3
		if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
			$this->foto_3->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_3->Upload->FileName == "") {
				$rsnew['foto_3'] = NULL;
			} else {
				$rsnew['foto_3'] = $this->foto_3->Upload->FileName;
			}
			$this->foto_3->ImageWidth = 750; // Resize width
			$this->foto_3->ImageHeight = 0; // Resize height
		}

		// foto_4
		if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
			$this->foto_4->Upload->DbValue = ""; // No need to delete old file
			if ($this->foto_4->Upload->FileName == "") {
				$rsnew['foto_4'] = NULL;
			} else {
				$rsnew['foto_4'] = $this->foto_4->Upload->FileName;
			}
			$this->foto_4->ImageWidth = 750; // Resize width
			$this->foto_4->ImageHeight = 0; // Resize height
		}

		// precio
		$this->precio->SetDbValueDef($rsnew, $this->precio->CurrentValue, NULL, FALSE);

		// fecha_film
		$this->fecha_film->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0), NULL, FALSE);

		// establecimiento
		$this->establecimiento->SetDbValueDef($rsnew, $this->establecimiento->CurrentValue, NULL, FALSE);

		// remitente
		$this->remitente->SetDbValueDef($rsnew, $this->remitente->CurrentValue, NULL, FALSE);

		// tipo
		$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, NULL, FALSE);

		// categoria
		$this->categoria->SetDbValueDef($rsnew, $this->categoria->CurrentValue, 0, FALSE);

		// sub_categoria
		$this->sub_categoria->SetDbValueDef($rsnew, $this->sub_categoria->CurrentValue, 0, FALSE);

		// cantidad
		$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, FALSE);

		// peso
		$this->peso->SetDbValueDef($rsnew, $this->peso->CurrentValue, NULL, FALSE);

		// caracteristicas
		$this->caracteristicas->SetDbValueDef($rsnew, $this->caracteristicas->CurrentValue, NULL, FALSE);

		// provincia
		$this->provincia->SetDbValueDef($rsnew, $this->provincia->CurrentValue, NULL, FALSE);

		// localidad
		$this->localidad->SetDbValueDef($rsnew, $this->localidad->CurrentValue, NULL, FALSE);

		// lugar
		$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, NULL, FALSE);

		// titulo
		$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", FALSE);

		// edad_aprox
		$this->edad_aprox->SetDbValueDef($rsnew, $this->edad_aprox->CurrentValue, NULL, FALSE);

		// trazabilidad
		$this->trazabilidad->SetDbValueDef($rsnew, $this->trazabilidad->CurrentValue, NULL, FALSE);

		// mio_mio
		$this->mio_mio->SetDbValueDef($rsnew, $this->mio_mio->CurrentValue, NULL, FALSE);

		// garrapata
		$this->garrapata->SetDbValueDef($rsnew, $this->garrapata->CurrentValue, NULL, FALSE);

		// observaciones
		$this->observaciones->SetDbValueDef($rsnew, $this->observaciones->CurrentValue, NULL, FALSE);

		// plazo
		$this->plazo->SetDbValueDef($rsnew, $this->plazo->CurrentValue, NULL, FALSE);

		// visitas
		$this->visitas->SetDbValueDef($rsnew, $this->visitas->CurrentValue, NULL, FALSE);
		if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
			if (!ew_Empty($this->foto_1->Upload->Value)) {
				$rsnew['foto_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_1->UploadPath), $rsnew['foto_1']); // Get new file name
			}
		}
		if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
			if (!ew_Empty($this->foto_2->Upload->Value)) {
				$rsnew['foto_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_2->UploadPath), $rsnew['foto_2']); // Get new file name
			}
		}
		if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
			if (!ew_Empty($this->foto_3->Upload->Value)) {
				$rsnew['foto_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_3->UploadPath), $rsnew['foto_3']); // Get new file name
			}
		}
		if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
			if (!ew_Empty($this->foto_4->Upload->Value)) {
				$rsnew['foto_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_4->UploadPath), $rsnew['foto_4']); // Get new file name
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->id->setDbValue($conn->Insert_ID());
				$rsnew['id'] = $this->id->DbValue;
				if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
					if (!ew_Empty($this->foto_1->Upload->Value)) {
						$this->foto_1->Upload->Resize($this->foto_1->ImageWidth, $this->foto_1->ImageHeight);
						if (!$this->foto_1->Upload->SaveToFile($this->foto_1->UploadPath, $rsnew['foto_1'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
					if (!ew_Empty($this->foto_2->Upload->Value)) {
						$this->foto_2->Upload->Resize($this->foto_2->ImageWidth, $this->foto_2->ImageHeight);
						if (!$this->foto_2->Upload->SaveToFile($this->foto_2->UploadPath, $rsnew['foto_2'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
					if (!ew_Empty($this->foto_3->Upload->Value)) {
						$this->foto_3->Upload->Resize($this->foto_3->ImageWidth, $this->foto_3->ImageHeight);
						if (!$this->foto_3->Upload->SaveToFile($this->foto_3->UploadPath, $rsnew['foto_3'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
				if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
					if (!ew_Empty($this->foto_4->Upload->Value)) {
						$this->foto_4->Upload->Resize($this->foto_4->ImageWidth, $this->foto_4->ImageHeight);
						if (!$this->foto_4->Upload->SaveToFile($this->foto_4->UploadPath, $rsnew['foto_4'], TRUE)) {
							$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
							return FALSE;
						}
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// foto_1
		ew_CleanUploadTempPath($this->foto_1, $this->foto_1->Upload->Index);

		// foto_2
		ew_CleanUploadTempPath($this->foto_2, $this->foto_2->Upload->Index);

		// foto_3
		ew_CleanUploadTempPath($this->foto_3, $this->foto_3->Upload->Index);

		// foto_4
		ew_CleanUploadTempPath($this->foto_4, $this->foto_4->Upload->Index);
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("loteslist.php"), "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_remate_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_conjunto_lote_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
			$sWhereWrk = "{filter}";
			$this->conjunto_lote_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`remate_id` IN ({filter_value})', "t1" => "19", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_tipo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_sub_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_provincia":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
			$sWhereWrk = "{filter}";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`provincia_id` IN ({filter_value})', "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld` FROM `localidades`";
			$sWhereWrk = "(`nombre` LIKE '{query_value}%') AND ({filter})";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f1" => "`provincia_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_add)) $lotes_add = new clotes_add();

// Page init
$lotes_add->Page_Init();

// Page main
$lotes_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = flotesadd = new ew_Form("flotesadd", "add");

// Validate form
flotesadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_precio");
			if (elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->precio->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha_film");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->fecha_film->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_categoria");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->categoria->FldCaption(), $lotes->categoria->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_sub_categoria");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->sub_categoria->FldCaption(), $lotes->sub_categoria->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_cantidad");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->cantidad->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->titulo->FldCaption(), $lotes->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_visitas");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->visitas->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
flotesadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
flotesadd.ValidateRequired = true;
<?php } else { ?>
flotesadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
flotesadd.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
flotesadd.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
flotesadd.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
flotesadd.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
flotesadd.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":["x_remate_id"],"ChildFields":[],"FilterFields":["x_remate_id"],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
flotesadd.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
flotesadd.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
flotesadd.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
flotesadd.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
flotesadd.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":["x_provincia"],"ChildFields":[],"FilterFields":["x_provincia_id"],"Options":[],"Template":"","LinkTable":"localidades"};
flotesadd.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
flotesadd.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
flotesadd.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesadd.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$lotes_add->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $lotes_add->ShowPageHeader(); ?>
<?php
$lotes_add->ShowMessage();
?>
<form name="flotesadd" id="flotesadd" class="<?php echo $lotes_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<input type="hidden" name="a_add" id="a_add" value="A">
<?php if ($lotes_add->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
	<div id="r_venta_tipo" class="form-group">
		<label id="elh_lotes_venta_tipo" for="x_venta_tipo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->venta_tipo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->venta_tipo->CellAttributes() ?>>
<span id="el_lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x_venta_tipo" name="x_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x_venta_tipo") ?>
</select>
</span>
<?php echo $lotes->venta_tipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
	<div id="r_activo" class="form-group">
		<label id="elh_lotes_activo" for="x_activo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->activo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->activo->CellAttributes() ?>>
<span id="el_lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x_activo" name="x_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x_activo") ?>
</select>
</span>
<?php echo $lotes->activo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<div id="r_vendido" class="form-group">
		<label id="elh_lotes_vendido" for="x_vendido" class="col-sm-2 control-label ewLabel"><?php echo $lotes->vendido->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->vendido->CellAttributes() ?>>
<span id="el_lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x_vendido" name="x_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x_vendido") ?>
</select>
</span>
<?php echo $lotes->vendido->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<div id="r_codigo" class="form-group">
		<label id="elh_lotes_codigo" for="x_codigo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->codigo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->codigo->CellAttributes() ?>>
<span id="el_lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x_codigo" id="x_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<?php echo $lotes->codigo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<div id="r_remate_id" class="form-group">
		<label id="elh_lotes_remate_id" for="x_remate_id" class="col-sm-2 control-label ewLabel"><?php echo $lotes->remate_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->remate_id->CellAttributes() ?>>
<span id="el_lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x_remate_id" name="x_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x_remate_id") ?>
</select>
<input type="hidden" name="s_x_remate_id" id="s_x_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->remate_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<div id="r_conjunto_lote_id" class="form-group">
		<label id="elh_lotes_conjunto_lote_id" for="x_conjunto_lote_id" class="col-sm-2 control-label ewLabel"><?php echo $lotes->conjunto_lote_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
<span id="el_lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x_conjunto_lote_id" name="x_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x_conjunto_lote_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "conjuntos_lotes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->conjunto_lote_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_conjunto_lote_id',url:'conjuntos_lotesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_conjunto_lote_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->conjunto_lote_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_conjunto_lote_id" id="s_x_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->conjunto_lote_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
	<div id="r_orden" class="form-group">
		<label id="elh_lotes_orden" for="x_orden" class="col-sm-2 control-label ewLabel"><?php echo $lotes->orden->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->orden->CellAttributes() ?>>
<span id="el_lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x_orden" id="x_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<?php echo $lotes->orden->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
	<div id="r_video" class="form-group">
		<label id="elh_lotes_video" for="x_video" class="col-sm-2 control-label ewLabel"><?php echo $lotes->video->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->video->CellAttributes() ?>>
<span id="el_lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x_video" id="x_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<?php echo $lotes->video->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
	<div id="r_foto_1" class="form-group">
		<label id="elh_lotes_foto_1" class="col-sm-2 control-label ewLabel"><?php echo $lotes->foto_1->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_1->CellAttributes() ?>>
<span id="el_lotes_foto_1">
<div id="fd_x_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x_foto_1" id="x_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_1" id= "fn_x_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<input type="hidden" name="fa_x_foto_1" id= "fa_x_foto_1" value="0">
<input type="hidden" name="fs_x_foto_1" id= "fs_x_foto_1" value="255">
<input type="hidden" name="fx_x_foto_1" id= "fx_x_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_1" id= "fm_x_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
	<div id="r_foto_2" class="form-group">
		<label id="elh_lotes_foto_2" class="col-sm-2 control-label ewLabel"><?php echo $lotes->foto_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_2->CellAttributes() ?>>
<span id="el_lotes_foto_2">
<div id="fd_x_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x_foto_2" id="x_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_2" id= "fn_x_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<input type="hidden" name="fa_x_foto_2" id= "fa_x_foto_2" value="0">
<input type="hidden" name="fs_x_foto_2" id= "fs_x_foto_2" value="255">
<input type="hidden" name="fx_x_foto_2" id= "fx_x_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_2" id= "fm_x_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
	<div id="r_foto_3" class="form-group">
		<label id="elh_lotes_foto_3" class="col-sm-2 control-label ewLabel"><?php echo $lotes->foto_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_3->CellAttributes() ?>>
<span id="el_lotes_foto_3">
<div id="fd_x_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x_foto_3" id="x_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_3" id= "fn_x_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<input type="hidden" name="fa_x_foto_3" id= "fa_x_foto_3" value="0">
<input type="hidden" name="fs_x_foto_3" id= "fs_x_foto_3" value="255">
<input type="hidden" name="fx_x_foto_3" id= "fx_x_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_3" id= "fm_x_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
	<div id="r_foto_4" class="form-group">
		<label id="elh_lotes_foto_4" class="col-sm-2 control-label ewLabel"><?php echo $lotes->foto_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_4->CellAttributes() ?>>
<span id="el_lotes_foto_4">
<div id="fd_x_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x_foto_4" id="x_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_4" id= "fn_x_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<input type="hidden" name="fa_x_foto_4" id= "fa_x_foto_4" value="0">
<input type="hidden" name="fs_x_foto_4" id= "fs_x_foto_4" value="255">
<input type="hidden" name="fx_x_foto_4" id= "fx_x_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_4" id= "fm_x_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
	<div id="r_precio" class="form-group">
		<label id="elh_lotes_precio" for="x_precio" class="col-sm-2 control-label ewLabel"><?php echo $lotes->precio->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->precio->CellAttributes() ?>>
<span id="el_lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x_precio" id="x_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<?php echo $lotes->precio->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
	<div id="r_fecha_film" class="form-group">
		<label id="elh_lotes_fecha_film" for="x_fecha_film" class="col-sm-2 control-label ewLabel"><?php echo $lotes->fecha_film->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->fecha_film->CellAttributes() ?>>
<span id="el_lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x_fecha_film" id="x_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("flotesadd", "x_fecha_film", 0);
</script>
<?php } ?>
</span>
<?php echo $lotes->fecha_film->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<div id="r_establecimiento" class="form-group">
		<label id="elh_lotes_establecimiento" for="x_establecimiento" class="col-sm-2 control-label ewLabel"><?php echo $lotes->establecimiento->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->establecimiento->CellAttributes() ?>>
<span id="el_lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x_establecimiento" id="x_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<?php echo $lotes->establecimiento->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<div id="r_remitente" class="form-group">
		<label id="elh_lotes_remitente" for="x_remitente" class="col-sm-2 control-label ewLabel"><?php echo $lotes->remitente->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->remitente->CellAttributes() ?>>
<span id="el_lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x_remitente" id="x_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<?php echo $lotes->remitente->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<div id="r_tipo" class="form-group">
		<label id="elh_lotes_tipo" for="x_tipo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->tipo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->tipo->CellAttributes() ?>>
<span id="el_lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x_tipo" name="x_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x_tipo") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "tipos")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->tipo->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_tipo',url:'tiposaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_tipo"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->tipo->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_tipo" id="s_x_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->tipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<div id="r_categoria" class="form-group">
		<label id="elh_lotes_categoria" for="x_categoria" class="col-sm-2 control-label ewLabel"><?php echo $lotes->categoria->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->categoria->CellAttributes() ?>>
<span id="el_lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x_categoria" name="x_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "categorias_ganado")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_categoria',url:'categorias_ganadoaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_categoria" id="s_x_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->categoria->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
	<div id="r_sub_categoria" class="form-group">
		<label id="elh_lotes_sub_categoria" for="x_sub_categoria" class="col-sm-2 control-label ewLabel"><?php echo $lotes->sub_categoria->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->sub_categoria->CellAttributes() ?>>
<span id="el_lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x_sub_categoria" name="x_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x_sub_categoria") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "sub_categorias")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->sub_categoria->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_sub_categoria',url:'sub_categoriasaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_sub_categoria"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->sub_categoria->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_sub_categoria" id="s_x_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->sub_categoria->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
	<div id="r_cantidad" class="form-group">
		<label id="elh_lotes_cantidad" for="x_cantidad" class="col-sm-2 control-label ewLabel"><?php echo $lotes->cantidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->cantidad->CellAttributes() ?>>
<span id="el_lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x_cantidad" id="x_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<?php echo $lotes->cantidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
	<div id="r_peso" class="form-group">
		<label id="elh_lotes_peso" for="x_peso" class="col-sm-2 control-label ewLabel"><?php echo $lotes->peso->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->peso->CellAttributes() ?>>
<span id="el_lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x_peso" id="x_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<?php echo $lotes->peso->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
	<div id="r_caracteristicas" class="form-group">
		<label id="elh_lotes_caracteristicas" for="x_caracteristicas" class="col-sm-2 control-label ewLabel"><?php echo $lotes->caracteristicas->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->caracteristicas->CellAttributes() ?>>
<span id="el_lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x_caracteristicas" id="x_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<?php echo $lotes->caracteristicas->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
	<div id="r_provincia" class="form-group">
		<label id="elh_lotes_provincia" for="x_provincia" class="col-sm-2 control-label ewLabel"><?php echo $lotes->provincia->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->provincia->CellAttributes() ?>>
<span id="el_lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x_provincia" name="x_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x_provincia") ?>
</select>
<input type="hidden" name="s_x_provincia" id="s_x_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->provincia->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
	<div id="r_localidad" class="form-group">
		<label id="elh_lotes_localidad" class="col-sm-2 control-label ewLabel"><?php echo $lotes->localidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->localidad->CellAttributes() ?>>
<span id="el_lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x_localidad" style="white-space: nowrap; z-index: 8750">
	<input type="text" name="sv_x_localidad" id="sv_x_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x_localidad" id="x_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x_localidad" id="q_x_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
flotesadd.CreateAutoSuggest({"id":"x_localidad","forceSelect":false});
</script>
<?php if (AllowAdd(CurrentProjectID() . "localidades")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $lotes->localidad->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_localidad',url:'localidadesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_localidad"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $lotes->localidad->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_localidad" id="s_x_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->localidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
	<div id="r_lugar" class="form-group">
		<label id="elh_lotes_lugar" for="x_lugar" class="col-sm-2 control-label ewLabel"><?php echo $lotes->lugar->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->lugar->CellAttributes() ?>>
<span id="el_lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x_lugar" id="x_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<?php echo $lotes->lugar->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
	<div id="r_titulo" class="form-group">
		<label id="elh_lotes_titulo" for="x_titulo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->titulo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->titulo->CellAttributes() ?>>
<span id="el_lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x_titulo" id="x_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<?php echo $lotes->titulo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
	<div id="r_edad_aprox" class="form-group">
		<label id="elh_lotes_edad_aprox" for="x_edad_aprox" class="col-sm-2 control-label ewLabel"><?php echo $lotes->edad_aprox->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->edad_aprox->CellAttributes() ?>>
<span id="el_lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x_edad_aprox" id="x_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<?php echo $lotes->edad_aprox->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<div id="r_trazabilidad" class="form-group">
		<label id="elh_lotes_trazabilidad" class="col-sm-2 control-label ewLabel"><?php echo $lotes->trazabilidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->trazabilidad->CellAttributes() ?>>
<span id="el_lotes_trazabilidad">
<div id="tp_x_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x_trazabilidad" id="x_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x_trazabilidad") ?>
</div></div>
</span>
<?php echo $lotes->trazabilidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<div id="r_mio_mio" class="form-group">
		<label id="elh_lotes_mio_mio" class="col-sm-2 control-label ewLabel"><?php echo $lotes->mio_mio->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->mio_mio->CellAttributes() ?>>
<span id="el_lotes_mio_mio">
<div id="tp_x_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x_mio_mio" id="x_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x_mio_mio") ?>
</div></div>
</span>
<?php echo $lotes->mio_mio->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<div id="r_garrapata" class="form-group">
		<label id="elh_lotes_garrapata" class="col-sm-2 control-label ewLabel"><?php echo $lotes->garrapata->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->garrapata->CellAttributes() ?>>
<span id="el_lotes_garrapata">
<div id="tp_x_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x_garrapata" id="x_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x_garrapata") ?>
</div></div>
</span>
<?php echo $lotes->garrapata->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->observaciones->Visible) { // observaciones ?>
	<div id="r_observaciones" class="form-group">
		<label id="elh_lotes_observaciones" for="x_observaciones" class="col-sm-2 control-label ewLabel"><?php echo $lotes->observaciones->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->observaciones->CellAttributes() ?>>
<span id="el_lotes_observaciones">
<textarea data-table="lotes" data-field="x_observaciones" name="x_observaciones" id="x_observaciones" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($lotes->observaciones->getPlaceHolder()) ?>"<?php echo $lotes->observaciones->EditAttributes() ?>><?php echo $lotes->observaciones->EditValue ?></textarea>
</span>
<?php echo $lotes->observaciones->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
	<div id="r_plazo" class="form-group">
		<label id="elh_lotes_plazo" for="x_plazo" class="col-sm-2 control-label ewLabel"><?php echo $lotes->plazo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->plazo->CellAttributes() ?>>
<span id="el_lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x_plazo" id="x_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<?php echo $lotes->plazo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
	<div id="r_visitas" class="form-group">
		<label id="elh_lotes_visitas" for="x_visitas" class="col-sm-2 control-label ewLabel"><?php echo $lotes->visitas->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->visitas->CellAttributes() ?>>
<span id="el_lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x_visitas" id="x_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<?php echo $lotes->visitas->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php if (!$lotes_add->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $lotes_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
flotesadd.Init();
</script>
<?php
$lotes_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$lotes_add->Page_Terminate();
?>
