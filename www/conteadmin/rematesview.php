<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "rematesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$remates_view = NULL; // Initialize page object first

class cremates_view extends cremates {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'remates';

	// Page object name
	var $PageObjName = 'remates_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (remates)
		if (!isset($GLOBALS["remates"]) || get_class($GLOBALS["remates"]) == "cremates") {
			$GLOBALS["remates"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["remates"];
		}
		$KeyUrl = "";
		if (@$_GET["id"] <> "") {
			$this->RecKey["id"] = $_GET["id"];
			$KeyUrl .= "&amp;id=" . urlencode($this->RecKey["id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'remates', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("remateslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->tipo->SetVisibility();
		$this->plataforma->SetVisibility();
		$this->ext_id->SetVisibility();
		$this->fecha->SetVisibility();
		$this->hora_inicio->SetVisibility();
		$this->hora_fin->SetVisibility();
		$this->zona->SetVisibility();
		$this->lugar->SetVisibility();
		$this->locacion->SetVisibility();
		$this->numero->SetVisibility();
		$this->titulo->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->descripcion->SetVisibility();
		$this->yacare_visible->SetVisibility();
		$this->banner_g->SetVisibility();
		$this->banner_g_visible->SetVisibility();
		$this->banner_g_z_index->SetVisibility();
		$this->banner_g_pos_x->SetVisibility();
		$this->banner_g_pos_y->SetVisibility();
		$this->banner_g_link->SetVisibility();
		$this->activo->SetVisibility();
		$this->archivo->SetVisibility();
		$this->precios->SetVisibility();
		$this->mapa_img->SetVisibility();
		$this->video_final_servidor->SetVisibility();
		$this->video_final_1->SetVisibility();
		$this->video_titu_1->SetVisibility();
		$this->video_final_2->SetVisibility();
		$this->video_titu_2->SetVisibility();
		$this->video_final_3->SetVisibility();
		$this->video_titu_3->SetVisibility();
		$this->video_final_4->SetVisibility();
		$this->video_titu_4->SetVisibility();
		$this->video_final_5->SetVisibility();
		$this->video_titu_5->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $remates;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($remates);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $IsModal = FALSE;
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["id"] <> "") {
				$this->id->setQueryStringValue($_GET["id"]);
				$this->RecKey["id"] = $this->id->QueryStringValue;
			} elseif (@$_POST["id"] <> "") {
				$this->id->setFormValue($_POST["id"]);
				$this->RecKey["id"] = $this->id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("remateslist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->id->CurrentValue) == strval($this->Recordset->fields('id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "remateslist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "remateslist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$addcaption = ew_HtmlTitle($Language->Phrase("ViewPageAddLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->AddUrl) . "',caption:'" . $addcaption . "'});\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . $addcaption . "\" data-caption=\"" . $addcaption . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$editcaption = ew_HtmlTitle($Language->Phrase("ViewPageEditLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->EditUrl) . "',caption:'" . $editcaption . "'});\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . $editcaption . "\" data-caption=\"" . $editcaption . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$copycaption = ew_HtmlTitle($Language->Phrase("ViewPageCopyLink"));
		if ($this->IsModal) // Modal
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"javascript:void(0);\" onclick=\"ew_ModalDialogShow({lnk:this,url:'" . ew_HtmlEncode($this->CopyUrl) . "',caption:'" . $copycaption . "'});\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		else
			$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . $copycaption . "\" data-caption=\"" . $copycaption . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a onclick=\"return ew_ConfirmDelete(this);\" class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->plataforma->setDbValue($rs->fields('plataforma'));
		$this->ext_id->setDbValue($rs->fields('ext_id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->hora_inicio->setDbValue($rs->fields('hora_inicio'));
		$this->hora_fin->setDbValue($rs->fields('hora_fin'));
		$this->zona->setDbValue($rs->fields('zona'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->locacion->setDbValue($rs->fields('locacion'));
		$this->numero->setDbValue($rs->fields('numero'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->descripcion->setDbValue($rs->fields('descripcion'));
		$this->yacare_visible->setDbValue($rs->fields('yacare_visible'));
		$this->banner_g->Upload->DbValue = $rs->fields('banner_g');
		$this->banner_g->CurrentValue = $this->banner_g->Upload->DbValue;
		$this->banner_g_visible->setDbValue($rs->fields('banner_g_visible'));
		$this->banner_g_z_index->setDbValue($rs->fields('banner_g_z_index'));
		$this->banner_g_pos_x->setDbValue($rs->fields('banner_g_pos_x'));
		$this->banner_g_pos_y->setDbValue($rs->fields('banner_g_pos_y'));
		$this->banner_g_link->setDbValue($rs->fields('banner_g_link'));
		$this->banner_ch->Upload->DbValue = $rs->fields('banner_ch');
		$this->banner_ch->CurrentValue = $this->banner_ch->Upload->DbValue;
		$this->activo->setDbValue($rs->fields('activo'));
		$this->archivo->Upload->DbValue = $rs->fields('archivo');
		$this->archivo->CurrentValue = $this->archivo->Upload->DbValue;
		$this->precios->setDbValue($rs->fields('precios'));
		$this->mapa_img->Upload->DbValue = $rs->fields('mapa_img');
		$this->mapa_img->CurrentValue = $this->mapa_img->Upload->DbValue;
		$this->video_final_servidor->setDbValue($rs->fields('video_final_servidor'));
		$this->video_final_1->setDbValue($rs->fields('video_final_1'));
		$this->video_titu_1->setDbValue($rs->fields('video_titu_1'));
		$this->video_final_2->setDbValue($rs->fields('video_final_2'));
		$this->video_titu_2->setDbValue($rs->fields('video_titu_2'));
		$this->video_final_3->setDbValue($rs->fields('video_final_3'));
		$this->video_titu_3->setDbValue($rs->fields('video_titu_3'));
		$this->video_final_4->setDbValue($rs->fields('video_final_4'));
		$this->video_titu_4->setDbValue($rs->fields('video_titu_4'));
		$this->video_final_5->setDbValue($rs->fields('video_final_5'));
		$this->video_titu_5->setDbValue($rs->fields('video_titu_5'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->tipo->DbValue = $row['tipo'];
		$this->plataforma->DbValue = $row['plataforma'];
		$this->ext_id->DbValue = $row['ext_id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->hora_inicio->DbValue = $row['hora_inicio'];
		$this->hora_fin->DbValue = $row['hora_fin'];
		$this->zona->DbValue = $row['zona'];
		$this->lugar->DbValue = $row['lugar'];
		$this->locacion->DbValue = $row['locacion'];
		$this->numero->DbValue = $row['numero'];
		$this->titulo->DbValue = $row['titulo'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->descripcion->DbValue = $row['descripcion'];
		$this->yacare_visible->DbValue = $row['yacare_visible'];
		$this->banner_g->Upload->DbValue = $row['banner_g'];
		$this->banner_g_visible->DbValue = $row['banner_g_visible'];
		$this->banner_g_z_index->DbValue = $row['banner_g_z_index'];
		$this->banner_g_pos_x->DbValue = $row['banner_g_pos_x'];
		$this->banner_g_pos_y->DbValue = $row['banner_g_pos_y'];
		$this->banner_g_link->DbValue = $row['banner_g_link'];
		$this->banner_ch->Upload->DbValue = $row['banner_ch'];
		$this->activo->DbValue = $row['activo'];
		$this->archivo->Upload->DbValue = $row['archivo'];
		$this->precios->DbValue = $row['precios'];
		$this->mapa_img->Upload->DbValue = $row['mapa_img'];
		$this->video_final_servidor->DbValue = $row['video_final_servidor'];
		$this->video_final_1->DbValue = $row['video_final_1'];
		$this->video_titu_1->DbValue = $row['video_titu_1'];
		$this->video_final_2->DbValue = $row['video_final_2'];
		$this->video_titu_2->DbValue = $row['video_titu_2'];
		$this->video_final_3->DbValue = $row['video_final_3'];
		$this->video_titu_3->DbValue = $row['video_titu_3'];
		$this->video_final_4->DbValue = $row['video_final_4'];
		$this->video_titu_4->DbValue = $row['video_titu_4'];
		$this->video_final_5->DbValue = $row['video_final_5'];
		$this->video_titu_5->DbValue = $row['video_titu_5'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// tipo
		// plataforma
		// ext_id
		// fecha
		// hora_inicio
		// hora_fin
		// zona
		// lugar
		// locacion
		// numero
		// titulo
		// cantidad
		// descripcion
		// yacare_visible
		// banner_g
		// banner_g_visible
		// banner_g_z_index
		// banner_g_pos_x
		// banner_g_pos_y
		// banner_g_link
		// banner_ch
		// activo
		// archivo
		// precios
		// mapa_img
		// video_final_servidor
		// video_final_1
		// video_titu_1
		// video_final_2
		// video_titu_2
		// video_final_3
		// video_titu_3
		// video_final_4
		// video_titu_4
		// video_final_5
		// video_titu_5

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// tipo
		if (strval($this->tipo->CurrentValue) <> "") {
			$this->tipo->ViewValue = $this->tipo->OptionCaption($this->tipo->CurrentValue);
		} else {
			$this->tipo->ViewValue = NULL;
		}
		$this->tipo->ViewCustomAttributes = "";

		// plataforma
		if (strval($this->plataforma->CurrentValue) <> "") {
			$this->plataforma->ViewValue = $this->plataforma->OptionCaption($this->plataforma->CurrentValue);
		} else {
			$this->plataforma->ViewValue = NULL;
		}
		$this->plataforma->ViewCustomAttributes = "";

		// ext_id
		$this->ext_id->ViewValue = $this->ext_id->CurrentValue;
		$this->ext_id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// hora_inicio
		$this->hora_inicio->ViewValue = $this->hora_inicio->CurrentValue;
		$this->hora_inicio->ViewCustomAttributes = "";

		// hora_fin
		$this->hora_fin->ViewValue = $this->hora_fin->CurrentValue;
		$this->hora_fin->ViewCustomAttributes = "";

		// zona
		if (strval($this->zona->CurrentValue) <> "") {
			$this->zona->ViewValue = $this->zona->OptionCaption($this->zona->CurrentValue);
		} else {
			$this->zona->ViewValue = NULL;
		}
		$this->zona->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewCustomAttributes = "";

		// locacion
		$this->locacion->ViewValue = $this->locacion->CurrentValue;
		$this->locacion->ViewCustomAttributes = "";

		// numero
		$this->numero->ViewValue = $this->numero->CurrentValue;
		$this->numero->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// descripcion
		$this->descripcion->ViewValue = $this->descripcion->CurrentValue;
		$this->descripcion->ViewCustomAttributes = "";

		// yacare_visible
		if (strval($this->yacare_visible->CurrentValue) <> "") {
			$this->yacare_visible->ViewValue = $this->yacare_visible->OptionCaption($this->yacare_visible->CurrentValue);
		} else {
			$this->yacare_visible->ViewValue = NULL;
		}
		$this->yacare_visible->ViewCustomAttributes = "";

		// banner_g
		if (!ew_Empty($this->banner_g->Upload->DbValue)) {
			$this->banner_g->ViewValue = $this->banner_g->Upload->DbValue;
		} else {
			$this->banner_g->ViewValue = "";
		}
		$this->banner_g->ViewCustomAttributes = "";

		// banner_g_visible
		if (strval($this->banner_g_visible->CurrentValue) <> "") {
			$this->banner_g_visible->ViewValue = $this->banner_g_visible->OptionCaption($this->banner_g_visible->CurrentValue);
		} else {
			$this->banner_g_visible->ViewValue = NULL;
		}
		$this->banner_g_visible->ViewCustomAttributes = "";

		// banner_g_z_index
		$this->banner_g_z_index->ViewValue = $this->banner_g_z_index->CurrentValue;
		$this->banner_g_z_index->ViewCustomAttributes = "";

		// banner_g_pos_x
		$this->banner_g_pos_x->ViewValue = $this->banner_g_pos_x->CurrentValue;
		$this->banner_g_pos_x->ViewCustomAttributes = "";

		// banner_g_pos_y
		$this->banner_g_pos_y->ViewValue = $this->banner_g_pos_y->CurrentValue;
		$this->banner_g_pos_y->ViewCustomAttributes = "";

		// banner_g_link
		$this->banner_g_link->ViewValue = $this->banner_g_link->CurrentValue;
		$this->banner_g_link->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// archivo
		if (!ew_Empty($this->archivo->Upload->DbValue)) {
			$this->archivo->ViewValue = $this->archivo->Upload->DbValue;
		} else {
			$this->archivo->ViewValue = "";
		}
		$this->archivo->ViewCustomAttributes = "";

		// precios
		if (strval($this->precios->CurrentValue) <> "") {
			$this->precios->ViewValue = $this->precios->OptionCaption($this->precios->CurrentValue);
		} else {
			$this->precios->ViewValue = NULL;
		}
		$this->precios->ViewCustomAttributes = "";

		// mapa_img
		if (!ew_Empty($this->mapa_img->Upload->DbValue)) {
			$this->mapa_img->ViewValue = $this->mapa_img->Upload->DbValue;
		} else {
			$this->mapa_img->ViewValue = "";
		}
		$this->mapa_img->ViewCustomAttributes = "";

		// video_final_servidor
		if (strval($this->video_final_servidor->CurrentValue) <> "") {
			$this->video_final_servidor->ViewValue = $this->video_final_servidor->OptionCaption($this->video_final_servidor->CurrentValue);
		} else {
			$this->video_final_servidor->ViewValue = NULL;
		}
		$this->video_final_servidor->ViewCustomAttributes = "";

		// video_final_1
		$this->video_final_1->ViewValue = $this->video_final_1->CurrentValue;
		$this->video_final_1->ViewCustomAttributes = "";

		// video_titu_1
		$this->video_titu_1->ViewValue = $this->video_titu_1->CurrentValue;
		$this->video_titu_1->ViewCustomAttributes = "";

		// video_final_2
		$this->video_final_2->ViewValue = $this->video_final_2->CurrentValue;
		$this->video_final_2->ViewCustomAttributes = "";

		// video_titu_2
		$this->video_titu_2->ViewValue = $this->video_titu_2->CurrentValue;
		$this->video_titu_2->ViewCustomAttributes = "";

		// video_final_3
		$this->video_final_3->ViewValue = $this->video_final_3->CurrentValue;
		$this->video_final_3->ViewCustomAttributes = "";

		// video_titu_3
		$this->video_titu_3->ViewValue = $this->video_titu_3->CurrentValue;
		$this->video_titu_3->ViewCustomAttributes = "";

		// video_final_4
		$this->video_final_4->ViewValue = $this->video_final_4->CurrentValue;
		$this->video_final_4->ViewCustomAttributes = "";

		// video_titu_4
		$this->video_titu_4->ViewValue = $this->video_titu_4->CurrentValue;
		$this->video_titu_4->ViewCustomAttributes = "";

		// video_final_5
		$this->video_final_5->ViewValue = $this->video_final_5->CurrentValue;
		$this->video_final_5->ViewCustomAttributes = "";

		// video_titu_5
		$this->video_titu_5->ViewValue = $this->video_titu_5->CurrentValue;
		$this->video_titu_5->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// plataforma
			$this->plataforma->LinkCustomAttributes = "";
			$this->plataforma->HrefValue = "";
			$this->plataforma->TooltipValue = "";

			// ext_id
			$this->ext_id->LinkCustomAttributes = "";
			$this->ext_id->HrefValue = "";
			$this->ext_id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// hora_inicio
			$this->hora_inicio->LinkCustomAttributes = "";
			$this->hora_inicio->HrefValue = "";
			$this->hora_inicio->TooltipValue = "";

			// hora_fin
			$this->hora_fin->LinkCustomAttributes = "";
			$this->hora_fin->HrefValue = "";
			$this->hora_fin->TooltipValue = "";

			// zona
			$this->zona->LinkCustomAttributes = "";
			$this->zona->HrefValue = "";
			$this->zona->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// locacion
			$this->locacion->LinkCustomAttributes = "";
			$this->locacion->HrefValue = "";
			$this->locacion->TooltipValue = "";

			// numero
			$this->numero->LinkCustomAttributes = "";
			$this->numero->HrefValue = "";
			$this->numero->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// descripcion
			$this->descripcion->LinkCustomAttributes = "";
			$this->descripcion->HrefValue = "";
			$this->descripcion->TooltipValue = "";

			// yacare_visible
			$this->yacare_visible->LinkCustomAttributes = "";
			$this->yacare_visible->HrefValue = "";
			$this->yacare_visible->TooltipValue = "";

			// banner_g
			$this->banner_g->LinkCustomAttributes = "";
			$this->banner_g->HrefValue = "";
			$this->banner_g->HrefValue2 = $this->banner_g->UploadPath . $this->banner_g->Upload->DbValue;
			$this->banner_g->TooltipValue = "";

			// banner_g_visible
			$this->banner_g_visible->LinkCustomAttributes = "";
			$this->banner_g_visible->HrefValue = "";
			$this->banner_g_visible->TooltipValue = "";

			// banner_g_z_index
			$this->banner_g_z_index->LinkCustomAttributes = "";
			$this->banner_g_z_index->HrefValue = "";
			$this->banner_g_z_index->TooltipValue = "";

			// banner_g_pos_x
			$this->banner_g_pos_x->LinkCustomAttributes = "";
			$this->banner_g_pos_x->HrefValue = "";
			$this->banner_g_pos_x->TooltipValue = "";

			// banner_g_pos_y
			$this->banner_g_pos_y->LinkCustomAttributes = "";
			$this->banner_g_pos_y->HrefValue = "";
			$this->banner_g_pos_y->TooltipValue = "";

			// banner_g_link
			$this->banner_g_link->LinkCustomAttributes = "";
			$this->banner_g_link->HrefValue = "";
			$this->banner_g_link->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// archivo
			$this->archivo->LinkCustomAttributes = "";
			$this->archivo->HrefValue = "";
			$this->archivo->HrefValue2 = $this->archivo->UploadPath . $this->archivo->Upload->DbValue;
			$this->archivo->TooltipValue = "";

			// precios
			$this->precios->LinkCustomAttributes = "";
			$this->precios->HrefValue = "";
			$this->precios->TooltipValue = "";

			// mapa_img
			$this->mapa_img->LinkCustomAttributes = "";
			$this->mapa_img->HrefValue = "";
			$this->mapa_img->HrefValue2 = $this->mapa_img->UploadPath . $this->mapa_img->Upload->DbValue;
			$this->mapa_img->TooltipValue = "";

			// video_final_servidor
			$this->video_final_servidor->LinkCustomAttributes = "";
			$this->video_final_servidor->HrefValue = "";
			$this->video_final_servidor->TooltipValue = "";

			// video_final_1
			$this->video_final_1->LinkCustomAttributes = "";
			$this->video_final_1->HrefValue = "";
			$this->video_final_1->TooltipValue = "";

			// video_titu_1
			$this->video_titu_1->LinkCustomAttributes = "";
			$this->video_titu_1->HrefValue = "";
			$this->video_titu_1->TooltipValue = "";

			// video_final_2
			$this->video_final_2->LinkCustomAttributes = "";
			$this->video_final_2->HrefValue = "";
			$this->video_final_2->TooltipValue = "";

			// video_titu_2
			$this->video_titu_2->LinkCustomAttributes = "";
			$this->video_titu_2->HrefValue = "";
			$this->video_titu_2->TooltipValue = "";

			// video_final_3
			$this->video_final_3->LinkCustomAttributes = "";
			$this->video_final_3->HrefValue = "";
			$this->video_final_3->TooltipValue = "";

			// video_titu_3
			$this->video_titu_3->LinkCustomAttributes = "";
			$this->video_titu_3->HrefValue = "";
			$this->video_titu_3->TooltipValue = "";

			// video_final_4
			$this->video_final_4->LinkCustomAttributes = "";
			$this->video_final_4->HrefValue = "";
			$this->video_final_4->TooltipValue = "";

			// video_titu_4
			$this->video_titu_4->LinkCustomAttributes = "";
			$this->video_titu_4->HrefValue = "";
			$this->video_titu_4->TooltipValue = "";

			// video_final_5
			$this->video_final_5->LinkCustomAttributes = "";
			$this->video_final_5->HrefValue = "";
			$this->video_final_5->TooltipValue = "";

			// video_titu_5
			$this->video_titu_5->LinkCustomAttributes = "";
			$this->video_titu_5->HrefValue = "";
			$this->video_titu_5->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = TRUE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = TRUE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = TRUE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = TRUE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_remates\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_remates',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.frematesview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED && $this->Export <> "pdf")
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("remateslist.php"), "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

		//$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($remates_view)) $remates_view = new cremates_view();

// Page init
$remates_view->Page_Init();

// Page main
$remates_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$remates_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = frematesview = new ew_Form("frematesview", "view");

// Form_CustomValidate event
frematesview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
frematesview.ValidateRequired = true;
<?php } else { ?>
frematesview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
frematesview.Lists["x_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_tipo"].Options = <?php echo json_encode($remates->tipo->Options()) ?>;
frematesview.Lists["x_plataforma"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_plataforma"].Options = <?php echo json_encode($remates->plataforma->Options()) ?>;
frematesview.Lists["x_zona"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_zona"].Options = <?php echo json_encode($remates->zona->Options()) ?>;
frematesview.Lists["x_yacare_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_yacare_visible"].Options = <?php echo json_encode($remates->yacare_visible->Options()) ?>;
frematesview.Lists["x_banner_g_visible"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_banner_g_visible"].Options = <?php echo json_encode($remates->banner_g_visible->Options()) ?>;
frematesview.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_activo"].Options = <?php echo json_encode($remates->activo->Options()) ?>;
frematesview.Lists["x_precios"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_precios"].Options = <?php echo json_encode($remates->precios->Options()) ?>;
frematesview.Lists["x_video_final_servidor"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
frematesview.Lists["x_video_final_servidor"].Options = <?php echo json_encode($remates->video_final_servidor->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($remates->Export == "") { ?>
<div class="ewToolbar">
<?php if (!$remates_view->IsModal) { ?>
<?php if ($remates->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php } ?>
<?php $remates_view->ExportOptions->Render("body") ?>
<?php
	foreach ($remates_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if (!$remates_view->IsModal) { ?>
<?php if ($remates->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $remates_view->ShowPageHeader(); ?>
<?php
$remates_view->ShowMessage();
?>
<?php if (!$remates_view->IsModal) { ?>
<?php if ($remates->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($remates_view->Pager)) $remates_view->Pager = new cPrevNextPager($remates_view->StartRec, $remates_view->DisplayRecs, $remates_view->TotalRecs) ?>
<?php if ($remates_view->Pager->RecordCount > 0 && $remates_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($remates_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($remates_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $remates_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($remates_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($remates_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $remates_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<?php } ?>
<form name="frematesview" id="frematesview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($remates_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $remates_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="remates">
<?php if ($remates_view->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($remates->id->Visible) { // id ?>
	<tr id="r_id">
		<td><span id="elh_remates_id"><?php echo $remates->id->FldCaption() ?></span></td>
		<td data-name="id"<?php echo $remates->id->CellAttributes() ?>>
<span id="el_remates_id">
<span<?php echo $remates->id->ViewAttributes() ?>>
<?php echo $remates->id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->tipo->Visible) { // tipo ?>
	<tr id="r_tipo">
		<td><span id="elh_remates_tipo"><?php echo $remates->tipo->FldCaption() ?></span></td>
		<td data-name="tipo"<?php echo $remates->tipo->CellAttributes() ?>>
<span id="el_remates_tipo">
<span<?php echo $remates->tipo->ViewAttributes() ?>>
<?php echo $remates->tipo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->plataforma->Visible) { // plataforma ?>
	<tr id="r_plataforma">
		<td><span id="elh_remates_plataforma"><?php echo $remates->plataforma->FldCaption() ?></span></td>
		<td data-name="plataforma"<?php echo $remates->plataforma->CellAttributes() ?>>
<span id="el_remates_plataforma">
<span<?php echo $remates->plataforma->ViewAttributes() ?>>
<?php echo $remates->plataforma->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->ext_id->Visible) { // ext_id ?>
	<tr id="r_ext_id">
		<td><span id="elh_remates_ext_id"><?php echo $remates->ext_id->FldCaption() ?></span></td>
		<td data-name="ext_id"<?php echo $remates->ext_id->CellAttributes() ?>>
<span id="el_remates_ext_id">
<span<?php echo $remates->ext_id->ViewAttributes() ?>>
<?php echo $remates->ext_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->fecha->Visible) { // fecha ?>
	<tr id="r_fecha">
		<td><span id="elh_remates_fecha"><?php echo $remates->fecha->FldCaption() ?></span></td>
		<td data-name="fecha"<?php echo $remates->fecha->CellAttributes() ?>>
<span id="el_remates_fecha">
<span<?php echo $remates->fecha->ViewAttributes() ?>>
<?php echo $remates->fecha->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->hora_inicio->Visible) { // hora_inicio ?>
	<tr id="r_hora_inicio">
		<td><span id="elh_remates_hora_inicio"><?php echo $remates->hora_inicio->FldCaption() ?></span></td>
		<td data-name="hora_inicio"<?php echo $remates->hora_inicio->CellAttributes() ?>>
<span id="el_remates_hora_inicio">
<span<?php echo $remates->hora_inicio->ViewAttributes() ?>>
<?php echo $remates->hora_inicio->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->hora_fin->Visible) { // hora_fin ?>
	<tr id="r_hora_fin">
		<td><span id="elh_remates_hora_fin"><?php echo $remates->hora_fin->FldCaption() ?></span></td>
		<td data-name="hora_fin"<?php echo $remates->hora_fin->CellAttributes() ?>>
<span id="el_remates_hora_fin">
<span<?php echo $remates->hora_fin->ViewAttributes() ?>>
<?php echo $remates->hora_fin->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->zona->Visible) { // zona ?>
	<tr id="r_zona">
		<td><span id="elh_remates_zona"><?php echo $remates->zona->FldCaption() ?></span></td>
		<td data-name="zona"<?php echo $remates->zona->CellAttributes() ?>>
<span id="el_remates_zona">
<span<?php echo $remates->zona->ViewAttributes() ?>>
<?php echo $remates->zona->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->lugar->Visible) { // lugar ?>
	<tr id="r_lugar">
		<td><span id="elh_remates_lugar"><?php echo $remates->lugar->FldCaption() ?></span></td>
		<td data-name="lugar"<?php echo $remates->lugar->CellAttributes() ?>>
<span id="el_remates_lugar">
<span<?php echo $remates->lugar->ViewAttributes() ?>>
<?php echo $remates->lugar->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->locacion->Visible) { // locacion ?>
	<tr id="r_locacion">
		<td><span id="elh_remates_locacion"><?php echo $remates->locacion->FldCaption() ?></span></td>
		<td data-name="locacion"<?php echo $remates->locacion->CellAttributes() ?>>
<span id="el_remates_locacion">
<span<?php echo $remates->locacion->ViewAttributes() ?>>
<?php echo $remates->locacion->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->numero->Visible) { // numero ?>
	<tr id="r_numero">
		<td><span id="elh_remates_numero"><?php echo $remates->numero->FldCaption() ?></span></td>
		<td data-name="numero"<?php echo $remates->numero->CellAttributes() ?>>
<span id="el_remates_numero">
<span<?php echo $remates->numero->ViewAttributes() ?>>
<?php echo $remates->numero->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->titulo->Visible) { // titulo ?>
	<tr id="r_titulo">
		<td><span id="elh_remates_titulo"><?php echo $remates->titulo->FldCaption() ?></span></td>
		<td data-name="titulo"<?php echo $remates->titulo->CellAttributes() ?>>
<span id="el_remates_titulo">
<span<?php echo $remates->titulo->ViewAttributes() ?>>
<?php echo $remates->titulo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->cantidad->Visible) { // cantidad ?>
	<tr id="r_cantidad">
		<td><span id="elh_remates_cantidad"><?php echo $remates->cantidad->FldCaption() ?></span></td>
		<td data-name="cantidad"<?php echo $remates->cantidad->CellAttributes() ?>>
<span id="el_remates_cantidad">
<span<?php echo $remates->cantidad->ViewAttributes() ?>>
<?php echo $remates->cantidad->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->descripcion->Visible) { // descripcion ?>
	<tr id="r_descripcion">
		<td><span id="elh_remates_descripcion"><?php echo $remates->descripcion->FldCaption() ?></span></td>
		<td data-name="descripcion"<?php echo $remates->descripcion->CellAttributes() ?>>
<span id="el_remates_descripcion">
<span<?php echo $remates->descripcion->ViewAttributes() ?>>
<?php echo $remates->descripcion->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->yacare_visible->Visible) { // yacare_visible ?>
	<tr id="r_yacare_visible">
		<td><span id="elh_remates_yacare_visible"><?php echo $remates->yacare_visible->FldCaption() ?></span></td>
		<td data-name="yacare_visible"<?php echo $remates->yacare_visible->CellAttributes() ?>>
<span id="el_remates_yacare_visible">
<span<?php echo $remates->yacare_visible->ViewAttributes() ?>>
<?php echo $remates->yacare_visible->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g->Visible) { // banner_g ?>
	<tr id="r_banner_g">
		<td><span id="elh_remates_banner_g"><?php echo $remates->banner_g->FldCaption() ?></span></td>
		<td data-name="banner_g"<?php echo $remates->banner_g->CellAttributes() ?>>
<span id="el_remates_banner_g">
<span<?php echo $remates->banner_g->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->banner_g, $remates->banner_g->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g_visible->Visible) { // banner_g_visible ?>
	<tr id="r_banner_g_visible">
		<td><span id="elh_remates_banner_g_visible"><?php echo $remates->banner_g_visible->FldCaption() ?></span></td>
		<td data-name="banner_g_visible"<?php echo $remates->banner_g_visible->CellAttributes() ?>>
<span id="el_remates_banner_g_visible">
<span<?php echo $remates->banner_g_visible->ViewAttributes() ?>>
<?php echo $remates->banner_g_visible->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g_z_index->Visible) { // banner_g_z_index ?>
	<tr id="r_banner_g_z_index">
		<td><span id="elh_remates_banner_g_z_index"><?php echo $remates->banner_g_z_index->FldCaption() ?></span></td>
		<td data-name="banner_g_z_index"<?php echo $remates->banner_g_z_index->CellAttributes() ?>>
<span id="el_remates_banner_g_z_index">
<span<?php echo $remates->banner_g_z_index->ViewAttributes() ?>>
<?php echo $remates->banner_g_z_index->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g_pos_x->Visible) { // banner_g_pos_x ?>
	<tr id="r_banner_g_pos_x">
		<td><span id="elh_remates_banner_g_pos_x"><?php echo $remates->banner_g_pos_x->FldCaption() ?></span></td>
		<td data-name="banner_g_pos_x"<?php echo $remates->banner_g_pos_x->CellAttributes() ?>>
<span id="el_remates_banner_g_pos_x">
<span<?php echo $remates->banner_g_pos_x->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_x->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g_pos_y->Visible) { // banner_g_pos_y ?>
	<tr id="r_banner_g_pos_y">
		<td><span id="elh_remates_banner_g_pos_y"><?php echo $remates->banner_g_pos_y->FldCaption() ?></span></td>
		<td data-name="banner_g_pos_y"<?php echo $remates->banner_g_pos_y->CellAttributes() ?>>
<span id="el_remates_banner_g_pos_y">
<span<?php echo $remates->banner_g_pos_y->ViewAttributes() ?>>
<?php echo $remates->banner_g_pos_y->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->banner_g_link->Visible) { // banner_g_link ?>
	<tr id="r_banner_g_link">
		<td><span id="elh_remates_banner_g_link"><?php echo $remates->banner_g_link->FldCaption() ?></span></td>
		<td data-name="banner_g_link"<?php echo $remates->banner_g_link->CellAttributes() ?>>
<span id="el_remates_banner_g_link">
<span<?php echo $remates->banner_g_link->ViewAttributes() ?>>
<?php echo $remates->banner_g_link->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->activo->Visible) { // activo ?>
	<tr id="r_activo">
		<td><span id="elh_remates_activo"><?php echo $remates->activo->FldCaption() ?></span></td>
		<td data-name="activo"<?php echo $remates->activo->CellAttributes() ?>>
<span id="el_remates_activo">
<span<?php echo $remates->activo->ViewAttributes() ?>>
<?php echo $remates->activo->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->archivo->Visible) { // archivo ?>
	<tr id="r_archivo">
		<td><span id="elh_remates_archivo"><?php echo $remates->archivo->FldCaption() ?></span></td>
		<td data-name="archivo"<?php echo $remates->archivo->CellAttributes() ?>>
<span id="el_remates_archivo">
<span<?php echo $remates->archivo->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->archivo, $remates->archivo->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->precios->Visible) { // precios ?>
	<tr id="r_precios">
		<td><span id="elh_remates_precios"><?php echo $remates->precios->FldCaption() ?></span></td>
		<td data-name="precios"<?php echo $remates->precios->CellAttributes() ?>>
<span id="el_remates_precios">
<span<?php echo $remates->precios->ViewAttributes() ?>>
<?php echo $remates->precios->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->mapa_img->Visible) { // mapa_img ?>
	<tr id="r_mapa_img">
		<td><span id="elh_remates_mapa_img"><?php echo $remates->mapa_img->FldCaption() ?></span></td>
		<td data-name="mapa_img"<?php echo $remates->mapa_img->CellAttributes() ?>>
<span id="el_remates_mapa_img">
<span<?php echo $remates->mapa_img->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($remates->mapa_img, $remates->mapa_img->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_servidor->Visible) { // video_final_servidor ?>
	<tr id="r_video_final_servidor">
		<td><span id="elh_remates_video_final_servidor"><?php echo $remates->video_final_servidor->FldCaption() ?></span></td>
		<td data-name="video_final_servidor"<?php echo $remates->video_final_servidor->CellAttributes() ?>>
<span id="el_remates_video_final_servidor">
<span<?php echo $remates->video_final_servidor->ViewAttributes() ?>>
<?php echo $remates->video_final_servidor->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_1->Visible) { // video_final_1 ?>
	<tr id="r_video_final_1">
		<td><span id="elh_remates_video_final_1"><?php echo $remates->video_final_1->FldCaption() ?></span></td>
		<td data-name="video_final_1"<?php echo $remates->video_final_1->CellAttributes() ?>>
<span id="el_remates_video_final_1">
<span<?php echo $remates->video_final_1->ViewAttributes() ?>>
<?php echo $remates->video_final_1->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_titu_1->Visible) { // video_titu_1 ?>
	<tr id="r_video_titu_1">
		<td><span id="elh_remates_video_titu_1"><?php echo $remates->video_titu_1->FldCaption() ?></span></td>
		<td data-name="video_titu_1"<?php echo $remates->video_titu_1->CellAttributes() ?>>
<span id="el_remates_video_titu_1">
<span<?php echo $remates->video_titu_1->ViewAttributes() ?>>
<?php echo $remates->video_titu_1->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_2->Visible) { // video_final_2 ?>
	<tr id="r_video_final_2">
		<td><span id="elh_remates_video_final_2"><?php echo $remates->video_final_2->FldCaption() ?></span></td>
		<td data-name="video_final_2"<?php echo $remates->video_final_2->CellAttributes() ?>>
<span id="el_remates_video_final_2">
<span<?php echo $remates->video_final_2->ViewAttributes() ?>>
<?php echo $remates->video_final_2->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_titu_2->Visible) { // video_titu_2 ?>
	<tr id="r_video_titu_2">
		<td><span id="elh_remates_video_titu_2"><?php echo $remates->video_titu_2->FldCaption() ?></span></td>
		<td data-name="video_titu_2"<?php echo $remates->video_titu_2->CellAttributes() ?>>
<span id="el_remates_video_titu_2">
<span<?php echo $remates->video_titu_2->ViewAttributes() ?>>
<?php echo $remates->video_titu_2->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_3->Visible) { // video_final_3 ?>
	<tr id="r_video_final_3">
		<td><span id="elh_remates_video_final_3"><?php echo $remates->video_final_3->FldCaption() ?></span></td>
		<td data-name="video_final_3"<?php echo $remates->video_final_3->CellAttributes() ?>>
<span id="el_remates_video_final_3">
<span<?php echo $remates->video_final_3->ViewAttributes() ?>>
<?php echo $remates->video_final_3->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_titu_3->Visible) { // video_titu_3 ?>
	<tr id="r_video_titu_3">
		<td><span id="elh_remates_video_titu_3"><?php echo $remates->video_titu_3->FldCaption() ?></span></td>
		<td data-name="video_titu_3"<?php echo $remates->video_titu_3->CellAttributes() ?>>
<span id="el_remates_video_titu_3">
<span<?php echo $remates->video_titu_3->ViewAttributes() ?>>
<?php echo $remates->video_titu_3->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_4->Visible) { // video_final_4 ?>
	<tr id="r_video_final_4">
		<td><span id="elh_remates_video_final_4"><?php echo $remates->video_final_4->FldCaption() ?></span></td>
		<td data-name="video_final_4"<?php echo $remates->video_final_4->CellAttributes() ?>>
<span id="el_remates_video_final_4">
<span<?php echo $remates->video_final_4->ViewAttributes() ?>>
<?php echo $remates->video_final_4->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_titu_4->Visible) { // video_titu_4 ?>
	<tr id="r_video_titu_4">
		<td><span id="elh_remates_video_titu_4"><?php echo $remates->video_titu_4->FldCaption() ?></span></td>
		<td data-name="video_titu_4"<?php echo $remates->video_titu_4->CellAttributes() ?>>
<span id="el_remates_video_titu_4">
<span<?php echo $remates->video_titu_4->ViewAttributes() ?>>
<?php echo $remates->video_titu_4->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_final_5->Visible) { // video_final_5 ?>
	<tr id="r_video_final_5">
		<td><span id="elh_remates_video_final_5"><?php echo $remates->video_final_5->FldCaption() ?></span></td>
		<td data-name="video_final_5"<?php echo $remates->video_final_5->CellAttributes() ?>>
<span id="el_remates_video_final_5">
<span<?php echo $remates->video_final_5->ViewAttributes() ?>>
<?php echo $remates->video_final_5->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($remates->video_titu_5->Visible) { // video_titu_5 ?>
	<tr id="r_video_titu_5">
		<td><span id="elh_remates_video_titu_5"><?php echo $remates->video_titu_5->FldCaption() ?></span></td>
		<td data-name="video_titu_5"<?php echo $remates->video_titu_5->CellAttributes() ?>>
<span id="el_remates_video_titu_5">
<span<?php echo $remates->video_titu_5->ViewAttributes() ?>>
<?php echo $remates->video_titu_5->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if (!$remates_view->IsModal) { ?>
<?php if ($remates->Export == "") { ?>
<?php if (!isset($remates_view->Pager)) $remates_view->Pager = new cPrevNextPager($remates_view->StartRec, $remates_view->DisplayRecs, $remates_view->TotalRecs) ?>
<?php if ($remates_view->Pager->RecordCount > 0 && $remates_view->Pager->Visible) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($remates_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($remates_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $remates_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($remates_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($remates_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $remates_view->PageUrl() ?>start=<?php echo $remates_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $remates_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
<?php } ?>
</form>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">
frematesview.Init();
</script>
<?php } ?>
<?php
$remates_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($remates->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$remates_view->Page_Terminate();
?>
