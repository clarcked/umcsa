<?php

// Global variable for table object
$lotes = NULL;

//
// Table class for lotes
//
class clotes extends cTable {
	var $id;
	var $venta_tipo;
	var $activo;
	var $vendido;
	var $codigo;
	var $remate_id;
	var $conjunto_lote_id;
	var $orden;
	var $video;
	var $foto_1;
	var $foto_2;
	var $foto_3;
	var $foto_4;
	var $precio;
	var $fecha_film;
	var $establecimiento;
	var $remitente;
	var $tipo;
	var $categoria;
	var $sub_categoria;
	var $cantidad;
	var $peso;
	var $caracteristicas;
	var $provincia;
	var $localidad;
	var $lugar;
	var $titulo;
	var $edad_aprox;
	var $trazabilidad;
	var $mio_mio;
	var $garrapata;
	var $observaciones;
	var $plazo;
	var $visitas;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'lotes';
		$this->TableName = 'lotes';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`lotes`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = ""; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = ""; // Page size (PHPExcel only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->DetailView = TRUE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 20;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('lotes', 'lotes', 'x_id', 'id', '`id`', '`id`', 19, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->id->Sortable = TRUE; // Allow sort
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// venta_tipo
		$this->venta_tipo = new cField('lotes', 'lotes', 'x_venta_tipo', 'venta_tipo', '`venta_tipo`', '`venta_tipo`', 202, -1, FALSE, '`venta_tipo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->venta_tipo->Sortable = TRUE; // Allow sort
		$this->venta_tipo->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->venta_tipo->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->venta_tipo->OptionCount = 2;
		$this->fields['venta_tipo'] = &$this->venta_tipo;

		// activo
		$this->activo = new cField('lotes', 'lotes', 'x_activo', 'activo', '`activo`', '`activo`', 202, -1, FALSE, '`activo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->activo->Sortable = TRUE; // Allow sort
		$this->activo->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->activo->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->activo->OptionCount = 2;
		$this->fields['activo'] = &$this->activo;

		// vendido
		$this->vendido = new cField('lotes', 'lotes', 'x_vendido', 'vendido', '`vendido`', '`vendido`', 202, -1, FALSE, '`vendido`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->vendido->Sortable = TRUE; // Allow sort
		$this->vendido->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->vendido->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->vendido->OptionCount = 2;
		$this->fields['vendido'] = &$this->vendido;

		// codigo
		$this->codigo = new cField('lotes', 'lotes', 'x_codigo', 'codigo', '`codigo`', '`codigo`', 200, -1, FALSE, '`codigo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->codigo->Sortable = TRUE; // Allow sort
		$this->fields['codigo'] = &$this->codigo;

		// remate_id
		$this->remate_id = new cField('lotes', 'lotes', 'x_remate_id', 'remate_id', '`remate_id`', '`remate_id`', 19, -1, FALSE, '`EV__remate_id`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->remate_id->Sortable = TRUE; // Allow sort
		$this->remate_id->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->remate_id->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->remate_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['remate_id'] = &$this->remate_id;

		// conjunto_lote_id
		$this->conjunto_lote_id = new cField('lotes', 'lotes', 'x_conjunto_lote_id', 'conjunto_lote_id', '`conjunto_lote_id`', '`conjunto_lote_id`', 19, -1, FALSE, '`EV__conjunto_lote_id`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->conjunto_lote_id->Sortable = TRUE; // Allow sort
		$this->conjunto_lote_id->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->conjunto_lote_id->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->conjunto_lote_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['conjunto_lote_id'] = &$this->conjunto_lote_id;

		// orden
		$this->orden = new cField('lotes', 'lotes', 'x_orden', 'orden', '`orden`', '`orden`', 200, -1, FALSE, '`orden`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->orden->Sortable = TRUE; // Allow sort
		$this->fields['orden'] = &$this->orden;

		// video
		$this->video = new cField('lotes', 'lotes', 'x_video', 'video', '`video`', '`video`', 201, -1, FALSE, '`video`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->video->Sortable = TRUE; // Allow sort
		$this->fields['video'] = &$this->video;

		// foto_1
		$this->foto_1 = new cField('lotes', 'lotes', 'x_foto_1', 'foto_1', '`foto_1`', '`foto_1`', 200, -1, TRUE, '`foto_1`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->foto_1->Sortable = TRUE; // Allow sort
		$this->fields['foto_1'] = &$this->foto_1;

		// foto_2
		$this->foto_2 = new cField('lotes', 'lotes', 'x_foto_2', 'foto_2', '`foto_2`', '`foto_2`', 200, -1, TRUE, '`foto_2`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->foto_2->Sortable = TRUE; // Allow sort
		$this->fields['foto_2'] = &$this->foto_2;

		// foto_3
		$this->foto_3 = new cField('lotes', 'lotes', 'x_foto_3', 'foto_3', '`foto_3`', '`foto_3`', 200, -1, TRUE, '`foto_3`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->foto_3->Sortable = TRUE; // Allow sort
		$this->fields['foto_3'] = &$this->foto_3;

		// foto_4
		$this->foto_4 = new cField('lotes', 'lotes', 'x_foto_4', 'foto_4', '`foto_4`', '`foto_4`', 200, -1, TRUE, '`foto_4`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->foto_4->Sortable = TRUE; // Allow sort
		$this->fields['foto_4'] = &$this->foto_4;

		// precio
		$this->precio = new cField('lotes', 'lotes', 'x_precio', 'precio', '`precio`', '`precio`', 131, -1, FALSE, '`precio`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->precio->Sortable = TRUE; // Allow sort
		$this->precio->FldDefaultErrMsg = $Language->Phrase("IncorrectFloat");
		$this->fields['precio'] = &$this->precio;

		// fecha_film
		$this->fecha_film = new cField('lotes', 'lotes', 'x_fecha_film', 'fecha_film', '`fecha_film`', ew_CastDateFieldForLike('`fecha_film`', 0, "DB"), 133, 0, FALSE, '`fecha_film`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fecha_film->Sortable = TRUE; // Allow sort
		$this->fecha_film->FldDefaultErrMsg = str_replace("%s", $GLOBALS["EW_DATE_FORMAT"], $Language->Phrase("IncorrectDate"));
		$this->fields['fecha_film'] = &$this->fecha_film;

		// establecimiento
		$this->establecimiento = new cField('lotes', 'lotes', 'x_establecimiento', 'establecimiento', '`establecimiento`', '`establecimiento`', 200, -1, FALSE, '`establecimiento`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->establecimiento->Sortable = TRUE; // Allow sort
		$this->fields['establecimiento'] = &$this->establecimiento;

		// remitente
		$this->remitente = new cField('lotes', 'lotes', 'x_remitente', 'remitente', '`remitente`', '`remitente`', 200, -1, FALSE, '`remitente`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->remitente->Sortable = TRUE; // Allow sort
		$this->fields['remitente'] = &$this->remitente;

		// tipo
		$this->tipo = new cField('lotes', 'lotes', 'x_tipo', 'tipo', '`tipo`', '`tipo`', 19, -1, FALSE, '`EV__tipo`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->tipo->Sortable = TRUE; // Allow sort
		$this->tipo->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->tipo->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->tipo->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['tipo'] = &$this->tipo;

		// categoria
		$this->categoria = new cField('lotes', 'lotes', 'x_categoria', 'categoria', '`categoria`', '`categoria`', 19, -1, FALSE, '`EV__categoria`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->categoria->Sortable = TRUE; // Allow sort
		$this->categoria->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->categoria->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->categoria->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['categoria'] = &$this->categoria;

		// sub_categoria
		$this->sub_categoria = new cField('lotes', 'lotes', 'x_sub_categoria', 'sub_categoria', '`sub_categoria`', '`sub_categoria`', 3, -1, FALSE, '`EV__sub_categoria`', TRUE, TRUE, TRUE, 'FORMATTED TEXT', 'SELECT');
		$this->sub_categoria->Sortable = TRUE; // Allow sort
		$this->sub_categoria->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->sub_categoria->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->sub_categoria->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['sub_categoria'] = &$this->sub_categoria;

		// cantidad
		$this->cantidad = new cField('lotes', 'lotes', 'x_cantidad', 'cantidad', '`cantidad`', '`cantidad`', 3, -1, FALSE, '`cantidad`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->cantidad->Sortable = TRUE; // Allow sort
		$this->cantidad->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['cantidad'] = &$this->cantidad;

		// peso
		$this->peso = new cField('lotes', 'lotes', 'x_peso', 'peso', '`peso`', '`peso`', 200, -1, FALSE, '`peso`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->peso->Sortable = TRUE; // Allow sort
		$this->fields['peso'] = &$this->peso;

		// caracteristicas
		$this->caracteristicas = new cField('lotes', 'lotes', 'x_caracteristicas', 'caracteristicas', '`caracteristicas`', '`caracteristicas`', 200, -1, FALSE, '`caracteristicas`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->caracteristicas->Sortable = TRUE; // Allow sort
		$this->fields['caracteristicas'] = &$this->caracteristicas;

		// provincia
		$this->provincia = new cField('lotes', 'lotes', 'x_provincia', 'provincia', '`provincia`', '`provincia`', 3, -1, FALSE, '`provincia`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->provincia->Sortable = TRUE; // Allow sort
		$this->provincia->UsePleaseSelect = TRUE; // Use PleaseSelect by default
		$this->provincia->PleaseSelectText = $Language->Phrase("PleaseSelect"); // PleaseSelect text
		$this->provincia->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['provincia'] = &$this->provincia;

		// localidad
		$this->localidad = new cField('lotes', 'lotes', 'x_localidad', 'localidad', '`localidad`', '`localidad`', 19, -1, FALSE, '`EV__localidad`', TRUE, FALSE, TRUE, 'FORMATTED TEXT', 'TEXT');
		$this->localidad->Sortable = TRUE; // Allow sort
		$this->localidad->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['localidad'] = &$this->localidad;

		// lugar
		$this->lugar = new cField('lotes', 'lotes', 'x_lugar', 'lugar', '`lugar`', '`lugar`', 200, 0, FALSE, '`lugar`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->lugar->Sortable = TRUE; // Allow sort
		$this->fields['lugar'] = &$this->lugar;

		// titulo
		$this->titulo = new cField('lotes', 'lotes', 'x_titulo', 'titulo', '`titulo`', '`titulo`', 200, -1, FALSE, '`titulo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->titulo->Sortable = TRUE; // Allow sort
		$this->fields['titulo'] = &$this->titulo;

		// edad_aprox
		$this->edad_aprox = new cField('lotes', 'lotes', 'x_edad_aprox', 'edad_aprox', '`edad_aprox`', '`edad_aprox`', 200, -1, FALSE, '`edad_aprox`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->edad_aprox->Sortable = TRUE; // Allow sort
		$this->fields['edad_aprox'] = &$this->edad_aprox;

		// trazabilidad
		$this->trazabilidad = new cField('lotes', 'lotes', 'x_trazabilidad', 'trazabilidad', '`trazabilidad`', '`trazabilidad`', 202, -1, FALSE, '`trazabilidad`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->trazabilidad->Sortable = TRUE; // Allow sort
		$this->trazabilidad->OptionCount = 3;
		$this->fields['trazabilidad'] = &$this->trazabilidad;

		// mio_mio
		$this->mio_mio = new cField('lotes', 'lotes', 'x_mio_mio', 'mio_mio', '`mio_mio`', '`mio_mio`', 202, -1, FALSE, '`mio_mio`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->mio_mio->Sortable = TRUE; // Allow sort
		$this->mio_mio->OptionCount = 2;
		$this->fields['mio_mio'] = &$this->mio_mio;

		// garrapata
		$this->garrapata = new cField('lotes', 'lotes', 'x_garrapata', 'garrapata', '`garrapata`', '`garrapata`', 202, -1, FALSE, '`garrapata`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->garrapata->Sortable = TRUE; // Allow sort
		$this->garrapata->OptionCount = 3;
		$this->fields['garrapata'] = &$this->garrapata;

		// observaciones
		$this->observaciones = new cField('lotes', 'lotes', 'x_observaciones', 'observaciones', '`observaciones`', '`observaciones`', 201, -1, FALSE, '`observaciones`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->observaciones->Sortable = TRUE; // Allow sort
		$this->fields['observaciones'] = &$this->observaciones;

		// plazo
		$this->plazo = new cField('lotes', 'lotes', 'x_plazo', 'plazo', '`plazo`', '`plazo`', 200, -1, FALSE, '`plazo`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->plazo->Sortable = TRUE; // Allow sort
		$this->fields['plazo'] = &$this->plazo;

		// visitas
		$this->visitas = new cField('lotes', 'lotes', 'x_visitas', 'visitas', '`visitas`', '`visitas`', 19, -1, FALSE, '`visitas`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->visitas->Sortable = TRUE; // Allow sort
		$this->visitas->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['visitas'] = &$this->visitas;
	}

	// Set Field Visibility
	function SetFieldVisibility($fldparm) {
		global $Security;
		return $this->$fldparm->Visible; // Returns original value
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
			$sSortFieldList = ($ofld->FldVirtualExpression <> "") ? $ofld->FldVirtualExpression : $sSortField;
			$this->setSessionOrderByList($sSortFieldList . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Session ORDER BY for List page
	function getSessionOrderByList() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY_LIST];
	}

	function setSessionOrderByList($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_ORDER_BY_LIST] = $v;
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`lotes`";
	}

	function SqlFrom() { // For backward compatibility
		return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
		$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
		return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
		$this->_SqlSelect = $v;
	}
	var $_SqlSelectList = "";

	function getSqlSelectList() { // Select for List page
		$select = "";
		$select = "SELECT * FROM (" .
			"SELECT *, (SELECT `numero` FROM `remates` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`remate_id` LIMIT 1) AS `EV__remate_id`, (SELECT `titulo` FROM `conjuntos_lotes` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`conjunto_lote_id` LIMIT 1) AS `EV__conjunto_lote_id`, (SELECT `nombre` FROM `tipos` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`tipo` LIMIT 1) AS `EV__tipo`, (SELECT `nombre` FROM `categorias_ganado` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`categoria` LIMIT 1) AS `EV__categoria`, (SELECT `abreviatura` FROM `sub_categorias` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`sub_categoria` LIMIT 1) AS `EV__sub_categoria`, (SELECT `nombre` FROM `localidades` `EW_TMP_LOOKUPTABLE` WHERE `EW_TMP_LOOKUPTABLE`.`id` = `lotes`.`localidad` LIMIT 1) AS `EV__localidad` FROM `lotes`" .
			") `EW_TMP_TABLE`";
		return ($this->_SqlSelectList <> "") ? $this->_SqlSelectList : $select;
	}

	function SqlSelectList() { // For backward compatibility
		return $this->getSqlSelectList();
	}

	function setSqlSelectList($v) {
		$this->_SqlSelectList = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
		return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
		$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
		return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
		$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
		return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
		$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "`remate_id` DESC";
	}

	function SqlOrderBy() { // For backward compatibility
		return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
		$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 8) == 8);
			case "search":
				return (($allow & 8) == 8);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		if ($this->UseVirtualFields()) {
			$sSort = $this->getSessionOrderByList();
			return ew_BuildSelectSql($this->getSqlSelectList(), $this->getSqlWhere(), $this->getSqlGroupBy(),
				$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
		} else {
			$sSort = $this->getSessionOrderBy();
			return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
				$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
		}
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = ($this->UseVirtualFields()) ? $this->getSessionOrderByList() : $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Check if virtual fields is used in SQL
	function UseVirtualFields() {
		$sWhere = $this->getSessionWhere();
		$sOrderBy = $this->getSessionOrderByList();
		if ($sWhere <> "")
			$sWhere = " " . str_replace(array("(",")"), array("",""), $sWhere) . " ";
		if ($sOrderBy <> "")
			$sOrderBy = " " . str_replace(array("(",")"), array("",""), $sOrderBy) . " ";
		if ($this->BasicSearch->getKeyword() <> "")
			return TRUE;
		if ($this->remate_id->AdvancedSearch->SearchValue <> "" ||
			$this->remate_id->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->remate_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->remate_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->conjunto_lote_id->AdvancedSearch->SearchValue <> "" ||
			$this->conjunto_lote_id->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->conjunto_lote_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->conjunto_lote_id->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->tipo->AdvancedSearch->SearchValue <> "" ||
			$this->tipo->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->tipo->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->tipo->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->categoria->AdvancedSearch->SearchValue <> "" ||
			$this->categoria->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->categoria->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->categoria->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->sub_categoria->AdvancedSearch->SearchValue <> "" ||
			$this->sub_categoria->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->sub_categoria->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->sub_categoria->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if ($this->localidad->AdvancedSearch->SearchValue <> "" ||
			$this->localidad->AdvancedSearch->SearchValue2 <> "" ||
			strpos($sWhere, " " . $this->localidad->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		if (strpos($sOrderBy, " " . $this->localidad->FldVirtualExpression . " ") !== FALSE)
			return TRUE;
		return FALSE;
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('id', $rs))
				ew_AddFilter($where, ew_QuotedName('id', $this->DBID) . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "loteslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "loteslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("lotesview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("lotesview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "lotesadd.php?" . $this->UrlParm($parm);
		else
			$url = "lotesadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("lotesedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("lotesadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("lotesdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "id:" . ew_VarToJson($this->id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return $this->AddMasterUrl(ew_CurrentPage() . "?" . $sUrlParm);
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			if ($isPost && isset($_POST["id"]))
				$arKeys[] = ew_StripSlashes($_POST["id"]);
			elseif (isset($_GET["id"]))
				$arKeys[] = ew_StripSlashes($_GET["id"]);
			else
				$arKeys = NULL; // Do not setup

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		if (is_array($arKeys)) {
			foreach ($arKeys as $key) {
				if (!is_numeric($key))
					continue;
				$ar[] = $key;
			}
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		$this->categoria->setDbValue($rs->fields('categoria'));
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// observaciones
		$this->observaciones->ViewValue = $this->observaciones->CurrentValue;
		$this->observaciones->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// venta_tipo
		$this->venta_tipo->LinkCustomAttributes = "";
		$this->venta_tipo->HrefValue = "";
		$this->venta_tipo->TooltipValue = "";

		// activo
		$this->activo->LinkCustomAttributes = "";
		$this->activo->HrefValue = "";
		$this->activo->TooltipValue = "";

		// vendido
		$this->vendido->LinkCustomAttributes = "";
		$this->vendido->HrefValue = "";
		$this->vendido->TooltipValue = "";

		// codigo
		$this->codigo->LinkCustomAttributes = "";
		$this->codigo->HrefValue = "";
		$this->codigo->TooltipValue = "";

		// remate_id
		$this->remate_id->LinkCustomAttributes = "";
		$this->remate_id->HrefValue = "";
		$this->remate_id->TooltipValue = "";

		// conjunto_lote_id
		$this->conjunto_lote_id->LinkCustomAttributes = "";
		$this->conjunto_lote_id->HrefValue = "";
		$this->conjunto_lote_id->TooltipValue = "";

		// orden
		$this->orden->LinkCustomAttributes = "";
		$this->orden->HrefValue = "";
		$this->orden->TooltipValue = "";

		// video
		$this->video->LinkCustomAttributes = "";
		$this->video->HrefValue = "";
		$this->video->TooltipValue = "";

		// foto_1
		$this->foto_1->LinkCustomAttributes = "";
		$this->foto_1->HrefValue = "";
		$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
		$this->foto_1->TooltipValue = "";

		// foto_2
		$this->foto_2->LinkCustomAttributes = "";
		$this->foto_2->HrefValue = "";
		$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
		$this->foto_2->TooltipValue = "";

		// foto_3
		$this->foto_3->LinkCustomAttributes = "";
		$this->foto_3->HrefValue = "";
		$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
		$this->foto_3->TooltipValue = "";

		// foto_4
		$this->foto_4->LinkCustomAttributes = "";
		$this->foto_4->HrefValue = "";
		$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
		$this->foto_4->TooltipValue = "";

		// precio
		$this->precio->LinkCustomAttributes = "";
		$this->precio->HrefValue = "";
		$this->precio->TooltipValue = "";

		// fecha_film
		$this->fecha_film->LinkCustomAttributes = "";
		$this->fecha_film->HrefValue = "";
		$this->fecha_film->TooltipValue = "";

		// establecimiento
		$this->establecimiento->LinkCustomAttributes = "";
		$this->establecimiento->HrefValue = "";
		$this->establecimiento->TooltipValue = "";

		// remitente
		$this->remitente->LinkCustomAttributes = "";
		$this->remitente->HrefValue = "";
		$this->remitente->TooltipValue = "";

		// tipo
		$this->tipo->LinkCustomAttributes = "";
		$this->tipo->HrefValue = "";
		$this->tipo->TooltipValue = "";

		// categoria
		$this->categoria->LinkCustomAttributes = "";
		$this->categoria->HrefValue = "";
		$this->categoria->TooltipValue = "";

		// sub_categoria
		$this->sub_categoria->LinkCustomAttributes = "";
		$this->sub_categoria->HrefValue = "";
		$this->sub_categoria->TooltipValue = "";

		// cantidad
		$this->cantidad->LinkCustomAttributes = "";
		$this->cantidad->HrefValue = "";
		$this->cantidad->TooltipValue = "";

		// peso
		$this->peso->LinkCustomAttributes = "";
		$this->peso->HrefValue = "";
		$this->peso->TooltipValue = "";

		// caracteristicas
		$this->caracteristicas->LinkCustomAttributes = "";
		$this->caracteristicas->HrefValue = "";
		$this->caracteristicas->TooltipValue = "";

		// provincia
		$this->provincia->LinkCustomAttributes = "";
		$this->provincia->HrefValue = "";
		$this->provincia->TooltipValue = "";

		// localidad
		$this->localidad->LinkCustomAttributes = "";
		$this->localidad->HrefValue = "";
		$this->localidad->TooltipValue = "";

		// lugar
		$this->lugar->LinkCustomAttributes = "";
		$this->lugar->HrefValue = "";
		$this->lugar->TooltipValue = "";

		// titulo
		$this->titulo->LinkCustomAttributes = "";
		$this->titulo->HrefValue = "";
		$this->titulo->TooltipValue = "";

		// edad_aprox
		$this->edad_aprox->LinkCustomAttributes = "";
		$this->edad_aprox->HrefValue = "";
		$this->edad_aprox->TooltipValue = "";

		// trazabilidad
		$this->trazabilidad->LinkCustomAttributes = "";
		$this->trazabilidad->HrefValue = "";
		$this->trazabilidad->TooltipValue = "";

		// mio_mio
		$this->mio_mio->LinkCustomAttributes = "";
		$this->mio_mio->HrefValue = "";
		$this->mio_mio->TooltipValue = "";

		// garrapata
		$this->garrapata->LinkCustomAttributes = "";
		$this->garrapata->HrefValue = "";
		$this->garrapata->TooltipValue = "";

		// observaciones
		$this->observaciones->LinkCustomAttributes = "";
		$this->observaciones->HrefValue = "";
		$this->observaciones->TooltipValue = "";

		// plazo
		$this->plazo->LinkCustomAttributes = "";
		$this->plazo->HrefValue = "";
		$this->plazo->TooltipValue = "";

		// visitas
		$this->visitas->LinkCustomAttributes = "";
		$this->visitas->HrefValue = "";
		$this->visitas->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// id
		$this->id->EditAttrs["class"] = "form-control";
		$this->id->EditCustomAttributes = "";
		$this->id->EditValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		$this->venta_tipo->EditAttrs["class"] = "form-control";
		$this->venta_tipo->EditCustomAttributes = "";
		$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

		// activo
		$this->activo->EditAttrs["class"] = "form-control";
		$this->activo->EditCustomAttributes = "";
		$this->activo->EditValue = $this->activo->Options(TRUE);

		// vendido
		$this->vendido->EditAttrs["class"] = "form-control";
		$this->vendido->EditCustomAttributes = "";
		$this->vendido->EditValue = $this->vendido->Options(TRUE);

		// codigo
		$this->codigo->EditAttrs["class"] = "form-control";
		$this->codigo->EditCustomAttributes = "";
		$this->codigo->EditValue = $this->codigo->CurrentValue;
		$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

		// remate_id
		$this->remate_id->EditAttrs["class"] = "form-control";
		$this->remate_id->EditCustomAttributes = "";

		// conjunto_lote_id
		$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
		$this->conjunto_lote_id->EditCustomAttributes = "";

		// orden
		$this->orden->EditAttrs["class"] = "form-control";
		$this->orden->EditCustomAttributes = "";
		$this->orden->EditValue = $this->orden->CurrentValue;
		$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

		// video
		$this->video->EditAttrs["class"] = "form-control";
		$this->video->EditCustomAttributes = "";
		$this->video->EditValue = $this->video->CurrentValue;
		$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

		// foto_1
		$this->foto_1->EditAttrs["class"] = "form-control";
		$this->foto_1->EditCustomAttributes = "";
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->EditValue = "";
		}
		if (!ew_Empty($this->foto_1->CurrentValue))
			$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;

		// foto_2
		$this->foto_2->EditAttrs["class"] = "form-control";
		$this->foto_2->EditCustomAttributes = "";
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->EditValue = "";
		}
		if (!ew_Empty($this->foto_2->CurrentValue))
			$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;

		// foto_3
		$this->foto_3->EditAttrs["class"] = "form-control";
		$this->foto_3->EditCustomAttributes = "";
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->EditValue = "";
		}
		if (!ew_Empty($this->foto_3->CurrentValue))
			$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;

		// foto_4
		$this->foto_4->EditAttrs["class"] = "form-control";
		$this->foto_4->EditCustomAttributes = "";
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->EditValue = "";
		}
		if (!ew_Empty($this->foto_4->CurrentValue))
			$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;

		// precio
		$this->precio->EditAttrs["class"] = "form-control";
		$this->precio->EditCustomAttributes = "";
		$this->precio->EditValue = $this->precio->CurrentValue;
		$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());
		if (strval($this->precio->EditValue) <> "" && is_numeric($this->precio->EditValue)) $this->precio->EditValue = ew_FormatNumber($this->precio->EditValue, -2, -1, -2, 0);

		// fecha_film
		$this->fecha_film->EditAttrs["class"] = "form-control";
		$this->fecha_film->EditCustomAttributes = "";
		$this->fecha_film->EditValue = ew_FormatDateTime($this->fecha_film->CurrentValue, 8);
		$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

		// establecimiento
		$this->establecimiento->EditAttrs["class"] = "form-control";
		$this->establecimiento->EditCustomAttributes = "";
		$this->establecimiento->EditValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

		// remitente
		$this->remitente->EditAttrs["class"] = "form-control";
		$this->remitente->EditCustomAttributes = "";
		$this->remitente->EditValue = $this->remitente->CurrentValue;
		$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

		// tipo
		$this->tipo->EditAttrs["class"] = "form-control";
		$this->tipo->EditCustomAttributes = "";

		// categoria
		$this->categoria->EditAttrs["class"] = "form-control";
		$this->categoria->EditCustomAttributes = "";

		// sub_categoria
		$this->sub_categoria->EditAttrs["class"] = "form-control";
		$this->sub_categoria->EditCustomAttributes = "";

		// cantidad
		$this->cantidad->EditAttrs["class"] = "form-control";
		$this->cantidad->EditCustomAttributes = "";
		$this->cantidad->EditValue = $this->cantidad->CurrentValue;
		$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

		// peso
		$this->peso->EditAttrs["class"] = "form-control";
		$this->peso->EditCustomAttributes = "";
		$this->peso->EditValue = $this->peso->CurrentValue;
		$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

		// caracteristicas
		$this->caracteristicas->EditAttrs["class"] = "form-control";
		$this->caracteristicas->EditCustomAttributes = "";
		$this->caracteristicas->EditValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

		// provincia
		$this->provincia->EditAttrs["class"] = "form-control";
		$this->provincia->EditCustomAttributes = "";

		// localidad
		$this->localidad->EditAttrs["class"] = "form-control";
		$this->localidad->EditCustomAttributes = "";
		$this->localidad->EditValue = $this->localidad->CurrentValue;
		$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

		// lugar
		$this->lugar->EditAttrs["class"] = "form-control";
		$this->lugar->EditCustomAttributes = "";
		$this->lugar->EditValue = $this->lugar->CurrentValue;
		$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

		// titulo
		$this->titulo->EditAttrs["class"] = "form-control";
		$this->titulo->EditCustomAttributes = "";
		$this->titulo->EditValue = $this->titulo->CurrentValue;
		$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

		// edad_aprox
		$this->edad_aprox->EditAttrs["class"] = "form-control";
		$this->edad_aprox->EditCustomAttributes = "";
		$this->edad_aprox->EditValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

		// trazabilidad
		$this->trazabilidad->EditCustomAttributes = "";
		$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

		// mio_mio
		$this->mio_mio->EditCustomAttributes = "";
		$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

		// garrapata
		$this->garrapata->EditCustomAttributes = "";
		$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

		// observaciones
		$this->observaciones->EditAttrs["class"] = "form-control";
		$this->observaciones->EditCustomAttributes = "";
		$this->observaciones->EditValue = $this->observaciones->CurrentValue;
		$this->observaciones->PlaceHolder = ew_RemoveHtml($this->observaciones->FldCaption());

		// plazo
		$this->plazo->EditAttrs["class"] = "form-control";
		$this->plazo->EditCustomAttributes = "";
		$this->plazo->EditValue = $this->plazo->CurrentValue;
		$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

		// visitas
		$this->visitas->EditAttrs["class"] = "form-control";
		$this->visitas->EditCustomAttributes = "";
		$this->visitas->EditValue = $this->visitas->CurrentValue;
		$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->venta_tipo->Exportable) $Doc->ExportCaption($this->venta_tipo);
					if ($this->activo->Exportable) $Doc->ExportCaption($this->activo);
					if ($this->vendido->Exportable) $Doc->ExportCaption($this->vendido);
					if ($this->codigo->Exportable) $Doc->ExportCaption($this->codigo);
					if ($this->remate_id->Exportable) $Doc->ExportCaption($this->remate_id);
					if ($this->conjunto_lote_id->Exportable) $Doc->ExportCaption($this->conjunto_lote_id);
					if ($this->orden->Exportable) $Doc->ExportCaption($this->orden);
					if ($this->video->Exportable) $Doc->ExportCaption($this->video);
					if ($this->foto_1->Exportable) $Doc->ExportCaption($this->foto_1);
					if ($this->foto_2->Exportable) $Doc->ExportCaption($this->foto_2);
					if ($this->foto_3->Exportable) $Doc->ExportCaption($this->foto_3);
					if ($this->foto_4->Exportable) $Doc->ExportCaption($this->foto_4);
					if ($this->precio->Exportable) $Doc->ExportCaption($this->precio);
					if ($this->fecha_film->Exportable) $Doc->ExportCaption($this->fecha_film);
					if ($this->establecimiento->Exportable) $Doc->ExportCaption($this->establecimiento);
					if ($this->remitente->Exportable) $Doc->ExportCaption($this->remitente);
					if ($this->tipo->Exportable) $Doc->ExportCaption($this->tipo);
					if ($this->categoria->Exportable) $Doc->ExportCaption($this->categoria);
					if ($this->sub_categoria->Exportable) $Doc->ExportCaption($this->sub_categoria);
					if ($this->cantidad->Exportable) $Doc->ExportCaption($this->cantidad);
					if ($this->peso->Exportable) $Doc->ExportCaption($this->peso);
					if ($this->caracteristicas->Exportable) $Doc->ExportCaption($this->caracteristicas);
					if ($this->provincia->Exportable) $Doc->ExportCaption($this->provincia);
					if ($this->localidad->Exportable) $Doc->ExportCaption($this->localidad);
					if ($this->lugar->Exportable) $Doc->ExportCaption($this->lugar);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->edad_aprox->Exportable) $Doc->ExportCaption($this->edad_aprox);
					if ($this->trazabilidad->Exportable) $Doc->ExportCaption($this->trazabilidad);
					if ($this->mio_mio->Exportable) $Doc->ExportCaption($this->mio_mio);
					if ($this->garrapata->Exportable) $Doc->ExportCaption($this->garrapata);
					if ($this->observaciones->Exportable) $Doc->ExportCaption($this->observaciones);
					if ($this->plazo->Exportable) $Doc->ExportCaption($this->plazo);
					if ($this->visitas->Exportable) $Doc->ExportCaption($this->visitas);
				} else {
					if ($this->id->Exportable) $Doc->ExportCaption($this->id);
					if ($this->venta_tipo->Exportable) $Doc->ExportCaption($this->venta_tipo);
					if ($this->activo->Exportable) $Doc->ExportCaption($this->activo);
					if ($this->vendido->Exportable) $Doc->ExportCaption($this->vendido);
					if ($this->codigo->Exportable) $Doc->ExportCaption($this->codigo);
					if ($this->remate_id->Exportable) $Doc->ExportCaption($this->remate_id);
					if ($this->conjunto_lote_id->Exportable) $Doc->ExportCaption($this->conjunto_lote_id);
					if ($this->orden->Exportable) $Doc->ExportCaption($this->orden);
					if ($this->video->Exportable) $Doc->ExportCaption($this->video);
					if ($this->foto_1->Exportable) $Doc->ExportCaption($this->foto_1);
					if ($this->foto_2->Exportable) $Doc->ExportCaption($this->foto_2);
					if ($this->foto_3->Exportable) $Doc->ExportCaption($this->foto_3);
					if ($this->foto_4->Exportable) $Doc->ExportCaption($this->foto_4);
					if ($this->precio->Exportable) $Doc->ExportCaption($this->precio);
					if ($this->fecha_film->Exportable) $Doc->ExportCaption($this->fecha_film);
					if ($this->establecimiento->Exportable) $Doc->ExportCaption($this->establecimiento);
					if ($this->remitente->Exportable) $Doc->ExportCaption($this->remitente);
					if ($this->tipo->Exportable) $Doc->ExportCaption($this->tipo);
					if ($this->categoria->Exportable) $Doc->ExportCaption($this->categoria);
					if ($this->sub_categoria->Exportable) $Doc->ExportCaption($this->sub_categoria);
					if ($this->cantidad->Exportable) $Doc->ExportCaption($this->cantidad);
					if ($this->peso->Exportable) $Doc->ExportCaption($this->peso);
					if ($this->caracteristicas->Exportable) $Doc->ExportCaption($this->caracteristicas);
					if ($this->provincia->Exportable) $Doc->ExportCaption($this->provincia);
					if ($this->localidad->Exportable) $Doc->ExportCaption($this->localidad);
					if ($this->lugar->Exportable) $Doc->ExportCaption($this->lugar);
					if ($this->titulo->Exportable) $Doc->ExportCaption($this->titulo);
					if ($this->edad_aprox->Exportable) $Doc->ExportCaption($this->edad_aprox);
					if ($this->trazabilidad->Exportable) $Doc->ExportCaption($this->trazabilidad);
					if ($this->mio_mio->Exportable) $Doc->ExportCaption($this->mio_mio);
					if ($this->garrapata->Exportable) $Doc->ExportCaption($this->garrapata);
					if ($this->plazo->Exportable) $Doc->ExportCaption($this->plazo);
					if ($this->visitas->Exportable) $Doc->ExportCaption($this->visitas);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->venta_tipo->Exportable) $Doc->ExportField($this->venta_tipo);
						if ($this->activo->Exportable) $Doc->ExportField($this->activo);
						if ($this->vendido->Exportable) $Doc->ExportField($this->vendido);
						if ($this->codigo->Exportable) $Doc->ExportField($this->codigo);
						if ($this->remate_id->Exportable) $Doc->ExportField($this->remate_id);
						if ($this->conjunto_lote_id->Exportable) $Doc->ExportField($this->conjunto_lote_id);
						if ($this->orden->Exportable) $Doc->ExportField($this->orden);
						if ($this->video->Exportable) $Doc->ExportField($this->video);
						if ($this->foto_1->Exportable) $Doc->ExportField($this->foto_1);
						if ($this->foto_2->Exportable) $Doc->ExportField($this->foto_2);
						if ($this->foto_3->Exportable) $Doc->ExportField($this->foto_3);
						if ($this->foto_4->Exportable) $Doc->ExportField($this->foto_4);
						if ($this->precio->Exportable) $Doc->ExportField($this->precio);
						if ($this->fecha_film->Exportable) $Doc->ExportField($this->fecha_film);
						if ($this->establecimiento->Exportable) $Doc->ExportField($this->establecimiento);
						if ($this->remitente->Exportable) $Doc->ExportField($this->remitente);
						if ($this->tipo->Exportable) $Doc->ExportField($this->tipo);
						if ($this->categoria->Exportable) $Doc->ExportField($this->categoria);
						if ($this->sub_categoria->Exportable) $Doc->ExportField($this->sub_categoria);
						if ($this->cantidad->Exportable) $Doc->ExportField($this->cantidad);
						if ($this->peso->Exportable) $Doc->ExportField($this->peso);
						if ($this->caracteristicas->Exportable) $Doc->ExportField($this->caracteristicas);
						if ($this->provincia->Exportable) $Doc->ExportField($this->provincia);
						if ($this->localidad->Exportable) $Doc->ExportField($this->localidad);
						if ($this->lugar->Exportable) $Doc->ExportField($this->lugar);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->edad_aprox->Exportable) $Doc->ExportField($this->edad_aprox);
						if ($this->trazabilidad->Exportable) $Doc->ExportField($this->trazabilidad);
						if ($this->mio_mio->Exportable) $Doc->ExportField($this->mio_mio);
						if ($this->garrapata->Exportable) $Doc->ExportField($this->garrapata);
						if ($this->observaciones->Exportable) $Doc->ExportField($this->observaciones);
						if ($this->plazo->Exportable) $Doc->ExportField($this->plazo);
						if ($this->visitas->Exportable) $Doc->ExportField($this->visitas);
					} else {
						if ($this->id->Exportable) $Doc->ExportField($this->id);
						if ($this->venta_tipo->Exportable) $Doc->ExportField($this->venta_tipo);
						if ($this->activo->Exportable) $Doc->ExportField($this->activo);
						if ($this->vendido->Exportable) $Doc->ExportField($this->vendido);
						if ($this->codigo->Exportable) $Doc->ExportField($this->codigo);
						if ($this->remate_id->Exportable) $Doc->ExportField($this->remate_id);
						if ($this->conjunto_lote_id->Exportable) $Doc->ExportField($this->conjunto_lote_id);
						if ($this->orden->Exportable) $Doc->ExportField($this->orden);
						if ($this->video->Exportable) $Doc->ExportField($this->video);
						if ($this->foto_1->Exportable) $Doc->ExportField($this->foto_1);
						if ($this->foto_2->Exportable) $Doc->ExportField($this->foto_2);
						if ($this->foto_3->Exportable) $Doc->ExportField($this->foto_3);
						if ($this->foto_4->Exportable) $Doc->ExportField($this->foto_4);
						if ($this->precio->Exportable) $Doc->ExportField($this->precio);
						if ($this->fecha_film->Exportable) $Doc->ExportField($this->fecha_film);
						if ($this->establecimiento->Exportable) $Doc->ExportField($this->establecimiento);
						if ($this->remitente->Exportable) $Doc->ExportField($this->remitente);
						if ($this->tipo->Exportable) $Doc->ExportField($this->tipo);
						if ($this->categoria->Exportable) $Doc->ExportField($this->categoria);
						if ($this->sub_categoria->Exportable) $Doc->ExportField($this->sub_categoria);
						if ($this->cantidad->Exportable) $Doc->ExportField($this->cantidad);
						if ($this->peso->Exportable) $Doc->ExportField($this->peso);
						if ($this->caracteristicas->Exportable) $Doc->ExportField($this->caracteristicas);
						if ($this->provincia->Exportable) $Doc->ExportField($this->provincia);
						if ($this->localidad->Exportable) $Doc->ExportField($this->localidad);
						if ($this->lugar->Exportable) $Doc->ExportField($this->lugar);
						if ($this->titulo->Exportable) $Doc->ExportField($this->titulo);
						if ($this->edad_aprox->Exportable) $Doc->ExportField($this->edad_aprox);
						if ($this->trazabilidad->Exportable) $Doc->ExportField($this->trazabilidad);
						if ($this->mio_mio->Exportable) $Doc->ExportField($this->mio_mio);
						if ($this->garrapata->Exportable) $Doc->ExportField($this->garrapata);
						if ($this->plazo->Exportable) $Doc->ExportField($this->plazo);
						if ($this->visitas->Exportable) $Doc->ExportField($this->visitas);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
