<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "lotesinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$lotes_update = NULL; // Initialize page object first

class clotes_update extends clotes {

	// Page ID
	var $PageID = 'update';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'lotes';

	// Page object name
	var $PageObjName = 'lotes_update';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (lotes)
		if (!isset($GLOBALS["lotes"]) || get_class($GLOBALS["lotes"]) == "clotes") {
			$GLOBALS["lotes"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["lotes"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'update', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'lotes', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("loteslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->venta_tipo->SetVisibility();
		$this->activo->SetVisibility();
		$this->vendido->SetVisibility();
		$this->codigo->SetVisibility();
		$this->remate_id->SetVisibility();
		$this->conjunto_lote_id->SetVisibility();
		$this->orden->SetVisibility();
		$this->video->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->precio->SetVisibility();
		$this->fecha_film->SetVisibility();
		$this->establecimiento->SetVisibility();
		$this->remitente->SetVisibility();
		$this->tipo->SetVisibility();
		$this->categoria->SetVisibility();
		$this->sub_categoria->SetVisibility();
		$this->cantidad->SetVisibility();
		$this->peso->SetVisibility();
		$this->caracteristicas->SetVisibility();
		$this->provincia->SetVisibility();
		$this->localidad->SetVisibility();
		$this->lugar->SetVisibility();
		$this->titulo->SetVisibility();
		$this->edad_aprox->SetVisibility();
		$this->trazabilidad->SetVisibility();
		$this->mio_mio->SetVisibility();
		$this->garrapata->SetVisibility();
		$this->observaciones->SetVisibility();
		$this->plazo->SetVisibility();
		$this->visitas->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $lotes;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($lotes);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewUpdateForm";
	var $IsModal = FALSE;
	var $RecKeys;
	var $Disabled;
	var $Recordset;
	var $UpdateCount = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Try to load keys from list form
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		if (@$_POST["a_update"] <> "") {

			// Get action
			$this->CurrentAction = $_POST["a_update"];
			$this->LoadFormValues(); // Get form values

			// Validate form
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->setFailureMessage($gsFormError);
			}
		} else {
			$this->LoadMultiUpdateValues(); // Load initial values to form
		}
		if (count($this->RecKeys) <= 0)
			$this->Page_Terminate("loteslist.php"); // No records selected, return to list
		switch ($this->CurrentAction) {
			case "U": // Update
				if ($this->UpdateRows()) { // Update Records based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up update success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				} else {
					$this->RestoreFormValues(); // Restore form values
				}
		}

		// Render row
		$this->RowType = EW_ROWTYPE_EDIT; // Render edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Load initial values to form if field values are identical in all selected records
	function LoadMultiUpdateValues() {
		$this->CurrentFilter = $this->GetKeyFilter();

		// Load recordset
		if ($this->Recordset = $this->LoadRecordset()) {
			$i = 1;
			while (!$this->Recordset->EOF) {
				if ($i == 1) {
					$this->venta_tipo->setDbValue($this->Recordset->fields('venta_tipo'));
					$this->activo->setDbValue($this->Recordset->fields('activo'));
					$this->vendido->setDbValue($this->Recordset->fields('vendido'));
					$this->codigo->setDbValue($this->Recordset->fields('codigo'));
					$this->remate_id->setDbValue($this->Recordset->fields('remate_id'));
					$this->conjunto_lote_id->setDbValue($this->Recordset->fields('conjunto_lote_id'));
					$this->orden->setDbValue($this->Recordset->fields('orden'));
					$this->video->setDbValue($this->Recordset->fields('video'));
					$this->precio->setDbValue($this->Recordset->fields('precio'));
					$this->fecha_film->setDbValue($this->Recordset->fields('fecha_film'));
					$this->establecimiento->setDbValue($this->Recordset->fields('establecimiento'));
					$this->remitente->setDbValue($this->Recordset->fields('remitente'));
					$this->tipo->setDbValue($this->Recordset->fields('tipo'));
					$this->categoria->setDbValue($this->Recordset->fields('categoria'));
					$this->sub_categoria->setDbValue($this->Recordset->fields('sub_categoria'));
					$this->cantidad->setDbValue($this->Recordset->fields('cantidad'));
					$this->peso->setDbValue($this->Recordset->fields('peso'));
					$this->caracteristicas->setDbValue($this->Recordset->fields('caracteristicas'));
					$this->provincia->setDbValue($this->Recordset->fields('provincia'));
					$this->localidad->setDbValue($this->Recordset->fields('localidad'));
					$this->lugar->setDbValue($this->Recordset->fields('lugar'));
					$this->titulo->setDbValue($this->Recordset->fields('titulo'));
					$this->edad_aprox->setDbValue($this->Recordset->fields('edad_aprox'));
					$this->trazabilidad->setDbValue($this->Recordset->fields('trazabilidad'));
					$this->mio_mio->setDbValue($this->Recordset->fields('mio_mio'));
					$this->garrapata->setDbValue($this->Recordset->fields('garrapata'));
					$this->observaciones->setDbValue($this->Recordset->fields('observaciones'));
					$this->plazo->setDbValue($this->Recordset->fields('plazo'));
					$this->visitas->setDbValue($this->Recordset->fields('visitas'));
				} else {
					if (!ew_CompareValue($this->venta_tipo->DbValue, $this->Recordset->fields('venta_tipo')))
						$this->venta_tipo->CurrentValue = NULL;
					if (!ew_CompareValue($this->activo->DbValue, $this->Recordset->fields('activo')))
						$this->activo->CurrentValue = NULL;
					if (!ew_CompareValue($this->vendido->DbValue, $this->Recordset->fields('vendido')))
						$this->vendido->CurrentValue = NULL;
					if (!ew_CompareValue($this->codigo->DbValue, $this->Recordset->fields('codigo')))
						$this->codigo->CurrentValue = NULL;
					if (!ew_CompareValue($this->remate_id->DbValue, $this->Recordset->fields('remate_id')))
						$this->remate_id->CurrentValue = NULL;
					if (!ew_CompareValue($this->conjunto_lote_id->DbValue, $this->Recordset->fields('conjunto_lote_id')))
						$this->conjunto_lote_id->CurrentValue = NULL;
					if (!ew_CompareValue($this->orden->DbValue, $this->Recordset->fields('orden')))
						$this->orden->CurrentValue = NULL;
					if (!ew_CompareValue($this->video->DbValue, $this->Recordset->fields('video')))
						$this->video->CurrentValue = NULL;
					if (!ew_CompareValue($this->precio->DbValue, $this->Recordset->fields('precio')))
						$this->precio->CurrentValue = NULL;
					if (!ew_CompareValue($this->fecha_film->DbValue, $this->Recordset->fields('fecha_film')))
						$this->fecha_film->CurrentValue = NULL;
					if (!ew_CompareValue($this->establecimiento->DbValue, $this->Recordset->fields('establecimiento')))
						$this->establecimiento->CurrentValue = NULL;
					if (!ew_CompareValue($this->remitente->DbValue, $this->Recordset->fields('remitente')))
						$this->remitente->CurrentValue = NULL;
					if (!ew_CompareValue($this->tipo->DbValue, $this->Recordset->fields('tipo')))
						$this->tipo->CurrentValue = NULL;
					if (!ew_CompareValue($this->categoria->DbValue, $this->Recordset->fields('categoria')))
						$this->categoria->CurrentValue = NULL;
					if (!ew_CompareValue($this->sub_categoria->DbValue, $this->Recordset->fields('sub_categoria')))
						$this->sub_categoria->CurrentValue = NULL;
					if (!ew_CompareValue($this->cantidad->DbValue, $this->Recordset->fields('cantidad')))
						$this->cantidad->CurrentValue = NULL;
					if (!ew_CompareValue($this->peso->DbValue, $this->Recordset->fields('peso')))
						$this->peso->CurrentValue = NULL;
					if (!ew_CompareValue($this->caracteristicas->DbValue, $this->Recordset->fields('caracteristicas')))
						$this->caracteristicas->CurrentValue = NULL;
					if (!ew_CompareValue($this->provincia->DbValue, $this->Recordset->fields('provincia')))
						$this->provincia->CurrentValue = NULL;
					if (!ew_CompareValue($this->localidad->DbValue, $this->Recordset->fields('localidad')))
						$this->localidad->CurrentValue = NULL;
					if (!ew_CompareValue($this->lugar->DbValue, $this->Recordset->fields('lugar')))
						$this->lugar->CurrentValue = NULL;
					if (!ew_CompareValue($this->titulo->DbValue, $this->Recordset->fields('titulo')))
						$this->titulo->CurrentValue = NULL;
					if (!ew_CompareValue($this->edad_aprox->DbValue, $this->Recordset->fields('edad_aprox')))
						$this->edad_aprox->CurrentValue = NULL;
					if (!ew_CompareValue($this->trazabilidad->DbValue, $this->Recordset->fields('trazabilidad')))
						$this->trazabilidad->CurrentValue = NULL;
					if (!ew_CompareValue($this->mio_mio->DbValue, $this->Recordset->fields('mio_mio')))
						$this->mio_mio->CurrentValue = NULL;
					if (!ew_CompareValue($this->garrapata->DbValue, $this->Recordset->fields('garrapata')))
						$this->garrapata->CurrentValue = NULL;
					if (!ew_CompareValue($this->observaciones->DbValue, $this->Recordset->fields('observaciones')))
						$this->observaciones->CurrentValue = NULL;
					if (!ew_CompareValue($this->plazo->DbValue, $this->Recordset->fields('plazo')))
						$this->plazo->CurrentValue = NULL;
					if (!ew_CompareValue($this->visitas->DbValue, $this->Recordset->fields('visitas')))
						$this->visitas->CurrentValue = NULL;
				}
				$i++;
				$this->Recordset->MoveNext();
			}
			$this->Recordset->Close();
		}
	}

	// Set up key value
	function SetupKeyValues($key) {
		$sKeyFld = $key;
		if (!is_numeric($sKeyFld))
			return FALSE;
		$this->id->CurrentValue = $sKeyFld;
		return TRUE;
	}

	// Update all selected rows
	function UpdateRows() {
		global $Language;
		$conn = &$this->Connection();
		$conn->BeginTrans();

		// Get old recordset
		$this->CurrentFilter = $this->GetKeyFilter();
		$sSql = $this->SQL();
		$rsold = $conn->Execute($sSql);

		// Update all rows
		$sKey = "";
		foreach ($this->RecKeys as $key) {
			if ($this->SetupKeyValues($key)) {
				$sThisKey = $key;
				$this->SendEmail = FALSE; // Do not send email on update success
				$this->UpdateCount += 1; // Update record count for records being updated
				$UpdateRows = $this->EditRow(); // Update this row
			} else {
				$UpdateRows = FALSE;
			}
			if (!$UpdateRows)
				break; // Update failed
			if ($sKey <> "") $sKey .= ", ";
			$sKey .= $sThisKey;
		}

		// Check if all rows updated
		if ($UpdateRows) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$rsnew = $conn->Execute($sSql);
		} else {
			$conn->RollbackTrans(); // Rollback transaction
		}
		return $UpdateRows;
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->foto_1->Upload->Index = $objForm->Index;
		$this->foto_1->Upload->UploadFile();
		$this->foto_1->CurrentValue = $this->foto_1->Upload->FileName;
		$this->foto_1->MultiUpdate = $objForm->GetValue("u_foto_1");
		$this->foto_2->Upload->Index = $objForm->Index;
		$this->foto_2->Upload->UploadFile();
		$this->foto_2->CurrentValue = $this->foto_2->Upload->FileName;
		$this->foto_2->MultiUpdate = $objForm->GetValue("u_foto_2");
		$this->foto_3->Upload->Index = $objForm->Index;
		$this->foto_3->Upload->UploadFile();
		$this->foto_3->CurrentValue = $this->foto_3->Upload->FileName;
		$this->foto_3->MultiUpdate = $objForm->GetValue("u_foto_3");
		$this->foto_4->Upload->Index = $objForm->Index;
		$this->foto_4->Upload->UploadFile();
		$this->foto_4->CurrentValue = $this->foto_4->Upload->FileName;
		$this->foto_4->MultiUpdate = $objForm->GetValue("u_foto_4");
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->venta_tipo->FldIsDetailKey) {
			$this->venta_tipo->setFormValue($objForm->GetValue("x_venta_tipo"));
		}
		$this->venta_tipo->MultiUpdate = $objForm->GetValue("u_venta_tipo");
		if (!$this->activo->FldIsDetailKey) {
			$this->activo->setFormValue($objForm->GetValue("x_activo"));
		}
		$this->activo->MultiUpdate = $objForm->GetValue("u_activo");
		if (!$this->vendido->FldIsDetailKey) {
			$this->vendido->setFormValue($objForm->GetValue("x_vendido"));
		}
		$this->vendido->MultiUpdate = $objForm->GetValue("u_vendido");
		if (!$this->codigo->FldIsDetailKey) {
			$this->codigo->setFormValue($objForm->GetValue("x_codigo"));
		}
		$this->codigo->MultiUpdate = $objForm->GetValue("u_codigo");
		if (!$this->remate_id->FldIsDetailKey) {
			$this->remate_id->setFormValue($objForm->GetValue("x_remate_id"));
		}
		$this->remate_id->MultiUpdate = $objForm->GetValue("u_remate_id");
		if (!$this->conjunto_lote_id->FldIsDetailKey) {
			$this->conjunto_lote_id->setFormValue($objForm->GetValue("x_conjunto_lote_id"));
		}
		$this->conjunto_lote_id->MultiUpdate = $objForm->GetValue("u_conjunto_lote_id");
		if (!$this->orden->FldIsDetailKey) {
			$this->orden->setFormValue($objForm->GetValue("x_orden"));
		}
		$this->orden->MultiUpdate = $objForm->GetValue("u_orden");
		if (!$this->video->FldIsDetailKey) {
			$this->video->setFormValue($objForm->GetValue("x_video"));
		}
		$this->video->MultiUpdate = $objForm->GetValue("u_video");
		if (!$this->precio->FldIsDetailKey) {
			$this->precio->setFormValue($objForm->GetValue("x_precio"));
		}
		$this->precio->MultiUpdate = $objForm->GetValue("u_precio");
		if (!$this->fecha_film->FldIsDetailKey) {
			$this->fecha_film->setFormValue($objForm->GetValue("x_fecha_film"));
			$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		}
		$this->fecha_film->MultiUpdate = $objForm->GetValue("u_fecha_film");
		if (!$this->establecimiento->FldIsDetailKey) {
			$this->establecimiento->setFormValue($objForm->GetValue("x_establecimiento"));
		}
		$this->establecimiento->MultiUpdate = $objForm->GetValue("u_establecimiento");
		if (!$this->remitente->FldIsDetailKey) {
			$this->remitente->setFormValue($objForm->GetValue("x_remitente"));
		}
		$this->remitente->MultiUpdate = $objForm->GetValue("u_remitente");
		if (!$this->tipo->FldIsDetailKey) {
			$this->tipo->setFormValue($objForm->GetValue("x_tipo"));
		}
		$this->tipo->MultiUpdate = $objForm->GetValue("u_tipo");
		if (!$this->categoria->FldIsDetailKey) {
			$this->categoria->setFormValue($objForm->GetValue("x_categoria"));
		}
		$this->categoria->MultiUpdate = $objForm->GetValue("u_categoria");
		if (!$this->sub_categoria->FldIsDetailKey) {
			$this->sub_categoria->setFormValue($objForm->GetValue("x_sub_categoria"));
		}
		$this->sub_categoria->MultiUpdate = $objForm->GetValue("u_sub_categoria");
		if (!$this->cantidad->FldIsDetailKey) {
			$this->cantidad->setFormValue($objForm->GetValue("x_cantidad"));
		}
		$this->cantidad->MultiUpdate = $objForm->GetValue("u_cantidad");
		if (!$this->peso->FldIsDetailKey) {
			$this->peso->setFormValue($objForm->GetValue("x_peso"));
		}
		$this->peso->MultiUpdate = $objForm->GetValue("u_peso");
		if (!$this->caracteristicas->FldIsDetailKey) {
			$this->caracteristicas->setFormValue($objForm->GetValue("x_caracteristicas"));
		}
		$this->caracteristicas->MultiUpdate = $objForm->GetValue("u_caracteristicas");
		if (!$this->provincia->FldIsDetailKey) {
			$this->provincia->setFormValue($objForm->GetValue("x_provincia"));
		}
		$this->provincia->MultiUpdate = $objForm->GetValue("u_provincia");
		if (!$this->localidad->FldIsDetailKey) {
			$this->localidad->setFormValue($objForm->GetValue("x_localidad"));
		}
		$this->localidad->MultiUpdate = $objForm->GetValue("u_localidad");
		if (!$this->lugar->FldIsDetailKey) {
			$this->lugar->setFormValue($objForm->GetValue("x_lugar"));
		}
		$this->lugar->MultiUpdate = $objForm->GetValue("u_lugar");
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		$this->titulo->MultiUpdate = $objForm->GetValue("u_titulo");
		if (!$this->edad_aprox->FldIsDetailKey) {
			$this->edad_aprox->setFormValue($objForm->GetValue("x_edad_aprox"));
		}
		$this->edad_aprox->MultiUpdate = $objForm->GetValue("u_edad_aprox");
		if (!$this->trazabilidad->FldIsDetailKey) {
			$this->trazabilidad->setFormValue($objForm->GetValue("x_trazabilidad"));
		}
		$this->trazabilidad->MultiUpdate = $objForm->GetValue("u_trazabilidad");
		if (!$this->mio_mio->FldIsDetailKey) {
			$this->mio_mio->setFormValue($objForm->GetValue("x_mio_mio"));
		}
		$this->mio_mio->MultiUpdate = $objForm->GetValue("u_mio_mio");
		if (!$this->garrapata->FldIsDetailKey) {
			$this->garrapata->setFormValue($objForm->GetValue("x_garrapata"));
		}
		$this->garrapata->MultiUpdate = $objForm->GetValue("u_garrapata");
		if (!$this->observaciones->FldIsDetailKey) {
			$this->observaciones->setFormValue($objForm->GetValue("x_observaciones"));
		}
		$this->observaciones->MultiUpdate = $objForm->GetValue("u_observaciones");
		if (!$this->plazo->FldIsDetailKey) {
			$this->plazo->setFormValue($objForm->GetValue("x_plazo"));
		}
		$this->plazo->MultiUpdate = $objForm->GetValue("u_plazo");
		if (!$this->visitas->FldIsDetailKey) {
			$this->visitas->setFormValue($objForm->GetValue("x_visitas"));
		}
		$this->visitas->MultiUpdate = $objForm->GetValue("u_visitas");
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->id->CurrentValue = $this->id->FormValue;
		$this->venta_tipo->CurrentValue = $this->venta_tipo->FormValue;
		$this->activo->CurrentValue = $this->activo->FormValue;
		$this->vendido->CurrentValue = $this->vendido->FormValue;
		$this->codigo->CurrentValue = $this->codigo->FormValue;
		$this->remate_id->CurrentValue = $this->remate_id->FormValue;
		$this->conjunto_lote_id->CurrentValue = $this->conjunto_lote_id->FormValue;
		$this->orden->CurrentValue = $this->orden->FormValue;
		$this->video->CurrentValue = $this->video->FormValue;
		$this->precio->CurrentValue = $this->precio->FormValue;
		$this->fecha_film->CurrentValue = $this->fecha_film->FormValue;
		$this->fecha_film->CurrentValue = ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0);
		$this->establecimiento->CurrentValue = $this->establecimiento->FormValue;
		$this->remitente->CurrentValue = $this->remitente->FormValue;
		$this->tipo->CurrentValue = $this->tipo->FormValue;
		$this->categoria->CurrentValue = $this->categoria->FormValue;
		$this->sub_categoria->CurrentValue = $this->sub_categoria->FormValue;
		$this->cantidad->CurrentValue = $this->cantidad->FormValue;
		$this->peso->CurrentValue = $this->peso->FormValue;
		$this->caracteristicas->CurrentValue = $this->caracteristicas->FormValue;
		$this->provincia->CurrentValue = $this->provincia->FormValue;
		$this->localidad->CurrentValue = $this->localidad->FormValue;
		$this->lugar->CurrentValue = $this->lugar->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->edad_aprox->CurrentValue = $this->edad_aprox->FormValue;
		$this->trazabilidad->CurrentValue = $this->trazabilidad->FormValue;
		$this->mio_mio->CurrentValue = $this->mio_mio->FormValue;
		$this->garrapata->CurrentValue = $this->garrapata->FormValue;
		$this->observaciones->CurrentValue = $this->observaciones->FormValue;
		$this->plazo->CurrentValue = $this->plazo->FormValue;
		$this->visitas->CurrentValue = $this->visitas->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderByList())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->venta_tipo->setDbValue($rs->fields('venta_tipo'));
		$this->activo->setDbValue($rs->fields('activo'));
		$this->vendido->setDbValue($rs->fields('vendido'));
		$this->codigo->setDbValue($rs->fields('codigo'));
		$this->remate_id->setDbValue($rs->fields('remate_id'));
		if (array_key_exists('EV__remate_id', $rs->fields)) {
			$this->remate_id->VirtualValue = $rs->fields('EV__remate_id'); // Set up virtual field value
		} else {
			$this->remate_id->VirtualValue = ""; // Clear value
		}
		$this->conjunto_lote_id->setDbValue($rs->fields('conjunto_lote_id'));
		if (array_key_exists('EV__conjunto_lote_id', $rs->fields)) {
			$this->conjunto_lote_id->VirtualValue = $rs->fields('EV__conjunto_lote_id'); // Set up virtual field value
		} else {
			$this->conjunto_lote_id->VirtualValue = ""; // Clear value
		}
		$this->orden->setDbValue($rs->fields('orden'));
		$this->video->setDbValue($rs->fields('video'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->precio->setDbValue($rs->fields('precio'));
		$this->fecha_film->setDbValue($rs->fields('fecha_film'));
		$this->establecimiento->setDbValue($rs->fields('establecimiento'));
		$this->remitente->setDbValue($rs->fields('remitente'));
		$this->tipo->setDbValue($rs->fields('tipo'));
		if (array_key_exists('EV__tipo', $rs->fields)) {
			$this->tipo->VirtualValue = $rs->fields('EV__tipo'); // Set up virtual field value
		} else {
			$this->tipo->VirtualValue = ""; // Clear value
		}
		$this->categoria->setDbValue($rs->fields('categoria'));
		if (array_key_exists('EV__categoria', $rs->fields)) {
			$this->categoria->VirtualValue = $rs->fields('EV__categoria'); // Set up virtual field value
		} else {
			$this->categoria->VirtualValue = ""; // Clear value
		}
		$this->sub_categoria->setDbValue($rs->fields('sub_categoria'));
		if (array_key_exists('EV__sub_categoria', $rs->fields)) {
			$this->sub_categoria->VirtualValue = $rs->fields('EV__sub_categoria'); // Set up virtual field value
		} else {
			$this->sub_categoria->VirtualValue = ""; // Clear value
		}
		$this->cantidad->setDbValue($rs->fields('cantidad'));
		$this->peso->setDbValue($rs->fields('peso'));
		$this->caracteristicas->setDbValue($rs->fields('caracteristicas'));
		$this->provincia->setDbValue($rs->fields('provincia'));
		$this->localidad->setDbValue($rs->fields('localidad'));
		if (array_key_exists('EV__localidad', $rs->fields)) {
			$this->localidad->VirtualValue = $rs->fields('EV__localidad'); // Set up virtual field value
		} else {
			$this->localidad->VirtualValue = ""; // Clear value
		}
		$this->lugar->setDbValue($rs->fields('lugar'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->edad_aprox->setDbValue($rs->fields('edad_aprox'));
		$this->trazabilidad->setDbValue($rs->fields('trazabilidad'));
		$this->mio_mio->setDbValue($rs->fields('mio_mio'));
		$this->garrapata->setDbValue($rs->fields('garrapata'));
		$this->observaciones->setDbValue($rs->fields('observaciones'));
		$this->plazo->setDbValue($rs->fields('plazo'));
		$this->visitas->setDbValue($rs->fields('visitas'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->venta_tipo->DbValue = $row['venta_tipo'];
		$this->activo->DbValue = $row['activo'];
		$this->vendido->DbValue = $row['vendido'];
		$this->codigo->DbValue = $row['codigo'];
		$this->remate_id->DbValue = $row['remate_id'];
		$this->conjunto_lote_id->DbValue = $row['conjunto_lote_id'];
		$this->orden->DbValue = $row['orden'];
		$this->video->DbValue = $row['video'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->precio->DbValue = $row['precio'];
		$this->fecha_film->DbValue = $row['fecha_film'];
		$this->establecimiento->DbValue = $row['establecimiento'];
		$this->remitente->DbValue = $row['remitente'];
		$this->tipo->DbValue = $row['tipo'];
		$this->categoria->DbValue = $row['categoria'];
		$this->sub_categoria->DbValue = $row['sub_categoria'];
		$this->cantidad->DbValue = $row['cantidad'];
		$this->peso->DbValue = $row['peso'];
		$this->caracteristicas->DbValue = $row['caracteristicas'];
		$this->provincia->DbValue = $row['provincia'];
		$this->localidad->DbValue = $row['localidad'];
		$this->lugar->DbValue = $row['lugar'];
		$this->titulo->DbValue = $row['titulo'];
		$this->edad_aprox->DbValue = $row['edad_aprox'];
		$this->trazabilidad->DbValue = $row['trazabilidad'];
		$this->mio_mio->DbValue = $row['mio_mio'];
		$this->garrapata->DbValue = $row['garrapata'];
		$this->observaciones->DbValue = $row['observaciones'];
		$this->plazo->DbValue = $row['plazo'];
		$this->visitas->DbValue = $row['visitas'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Convert decimal values if posted back

		if ($this->precio->FormValue == $this->precio->CurrentValue && is_numeric(ew_StrToFloat($this->precio->CurrentValue)))
			$this->precio->CurrentValue = ew_StrToFloat($this->precio->CurrentValue);

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// venta_tipo
		// activo
		// vendido
		// codigo
		// remate_id
		// conjunto_lote_id
		// orden
		// video
		// foto_1
		// foto_2
		// foto_3
		// foto_4
		// precio
		// fecha_film
		// establecimiento
		// remitente
		// tipo
		// categoria
		// sub_categoria
		// cantidad
		// peso
		// caracteristicas
		// provincia
		// localidad
		// lugar
		// titulo
		// edad_aprox
		// trazabilidad
		// mio_mio
		// garrapata
		// observaciones
		// plazo
		// visitas

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// venta_tipo
		if (strval($this->venta_tipo->CurrentValue) <> "") {
			$this->venta_tipo->ViewValue = $this->venta_tipo->OptionCaption($this->venta_tipo->CurrentValue);
		} else {
			$this->venta_tipo->ViewValue = NULL;
		}
		$this->venta_tipo->ViewCustomAttributes = "";

		// activo
		if (strval($this->activo->CurrentValue) <> "") {
			$this->activo->ViewValue = $this->activo->OptionCaption($this->activo->CurrentValue);
		} else {
			$this->activo->ViewValue = NULL;
		}
		$this->activo->ViewCustomAttributes = "";

		// vendido
		if (strval($this->vendido->CurrentValue) <> "") {
			$this->vendido->ViewValue = $this->vendido->OptionCaption($this->vendido->CurrentValue);
		} else {
			$this->vendido->ViewValue = NULL;
		}
		$this->vendido->ViewCustomAttributes = "";

		// codigo
		$this->codigo->ViewValue = $this->codigo->CurrentValue;
		$this->codigo->ViewCustomAttributes = "";

		// remate_id
		if ($this->remate_id->VirtualValue <> "") {
			$this->remate_id->ViewValue = $this->remate_id->VirtualValue;
		} else {
		if (strval($this->remate_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
		$sWhereWrk = "";
		$this->remate_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->remate_id->ViewValue = $this->remate_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->remate_id->ViewValue = $this->remate_id->CurrentValue;
			}
		} else {
			$this->remate_id->ViewValue = NULL;
		}
		}
		$this->remate_id->ViewCustomAttributes = "";

		// conjunto_lote_id
		if ($this->conjunto_lote_id->VirtualValue <> "") {
			$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->VirtualValue;
		} else {
		if (strval($this->conjunto_lote_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
		$sWhereWrk = "";
		$this->conjunto_lote_id->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->conjunto_lote_id->ViewValue = $this->conjunto_lote_id->CurrentValue;
			}
		} else {
			$this->conjunto_lote_id->ViewValue = NULL;
		}
		}
		$this->conjunto_lote_id->ViewCustomAttributes = "";

		// orden
		$this->orden->ViewValue = $this->orden->CurrentValue;
		$this->orden->ViewCustomAttributes = "";

		// video
		$this->video->ViewValue = $this->video->CurrentValue;
		$this->video->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// precio
		$this->precio->ViewValue = $this->precio->CurrentValue;
		$this->precio->ViewCustomAttributes = "";

		// fecha_film
		$this->fecha_film->ViewValue = $this->fecha_film->CurrentValue;
		$this->fecha_film->ViewValue = ew_FormatDateTime($this->fecha_film->ViewValue, 0);
		$this->fecha_film->ViewCustomAttributes = "";

		// establecimiento
		$this->establecimiento->ViewValue = $this->establecimiento->CurrentValue;
		$this->establecimiento->ViewCustomAttributes = "";

		// remitente
		$this->remitente->ViewValue = $this->remitente->CurrentValue;
		$this->remitente->ViewCustomAttributes = "";

		// tipo
		if ($this->tipo->VirtualValue <> "") {
			$this->tipo->ViewValue = $this->tipo->VirtualValue;
		} else {
		if (strval($this->tipo->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
		$sWhereWrk = "";
		$this->tipo->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->tipo->ViewValue = $this->tipo->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->tipo->ViewValue = $this->tipo->CurrentValue;
			}
		} else {
			$this->tipo->ViewValue = NULL;
		}
		}
		$this->tipo->ViewCustomAttributes = "";

		// categoria
		if ($this->categoria->VirtualValue <> "") {
			$this->categoria->ViewValue = $this->categoria->VirtualValue;
		} else {
		if (strval($this->categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
		$sWhereWrk = "";
		$this->categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->categoria->ViewValue = $this->categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->categoria->ViewValue = $this->categoria->CurrentValue;
			}
		} else {
			$this->categoria->ViewValue = NULL;
		}
		}
		$this->categoria->ViewCustomAttributes = "";

		// sub_categoria
		if ($this->sub_categoria->VirtualValue <> "") {
			$this->sub_categoria->ViewValue = $this->sub_categoria->VirtualValue;
		} else {
		if (strval($this->sub_categoria->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
		$sWhereWrk = "";
		$this->sub_categoria->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->sub_categoria->ViewValue = $this->sub_categoria->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->sub_categoria->ViewValue = $this->sub_categoria->CurrentValue;
			}
		} else {
			$this->sub_categoria->ViewValue = NULL;
		}
		}
		$this->sub_categoria->ViewCustomAttributes = "";

		// cantidad
		$this->cantidad->ViewValue = $this->cantidad->CurrentValue;
		$this->cantidad->ViewCustomAttributes = "";

		// peso
		$this->peso->ViewValue = $this->peso->CurrentValue;
		$this->peso->ViewCustomAttributes = "";

		// caracteristicas
		$this->caracteristicas->ViewValue = $this->caracteristicas->CurrentValue;
		$this->caracteristicas->ViewCustomAttributes = "";

		// provincia
		if (strval($this->provincia->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
		$sWhereWrk = "";
		$this->provincia->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->provincia->ViewValue = $this->provincia->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->provincia->ViewValue = $this->provincia->CurrentValue;
			}
		} else {
			$this->provincia->ViewValue = NULL;
		}
		$this->provincia->ViewCustomAttributes = "";

		// localidad
		if ($this->localidad->VirtualValue <> "") {
			$this->localidad->ViewValue = $this->localidad->VirtualValue;
		} else {
			$this->localidad->ViewValue = $this->localidad->CurrentValue;
		if (strval($this->localidad->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->localidad->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
		$sWhereWrk = "";
		$this->localidad->LookupFilters = array();
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->localidad->ViewValue = $this->localidad->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->localidad->ViewValue = $this->localidad->CurrentValue;
			}
		} else {
			$this->localidad->ViewValue = NULL;
		}
		}
		$this->localidad->ViewCustomAttributes = "";

		// lugar
		$this->lugar->ViewValue = $this->lugar->CurrentValue;
		$this->lugar->ViewValue = ew_FormatDateTime($this->lugar->ViewValue, 0);
		$this->lugar->ViewCustomAttributes = 'text-transform:capitalize;';

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// edad_aprox
		$this->edad_aprox->ViewValue = $this->edad_aprox->CurrentValue;
		$this->edad_aprox->ViewCustomAttributes = "";

		// trazabilidad
		if (strval($this->trazabilidad->CurrentValue) <> "") {
			$this->trazabilidad->ViewValue = $this->trazabilidad->OptionCaption($this->trazabilidad->CurrentValue);
		} else {
			$this->trazabilidad->ViewValue = NULL;
		}
		$this->trazabilidad->ViewCustomAttributes = "";

		// mio_mio
		if (strval($this->mio_mio->CurrentValue) <> "") {
			$this->mio_mio->ViewValue = $this->mio_mio->OptionCaption($this->mio_mio->CurrentValue);
		} else {
			$this->mio_mio->ViewValue = NULL;
		}
		$this->mio_mio->ViewCustomAttributes = "";

		// garrapata
		if (strval($this->garrapata->CurrentValue) <> "") {
			$this->garrapata->ViewValue = $this->garrapata->OptionCaption($this->garrapata->CurrentValue);
		} else {
			$this->garrapata->ViewValue = NULL;
		}
		$this->garrapata->ViewCustomAttributes = "";

		// observaciones
		$this->observaciones->ViewValue = $this->observaciones->CurrentValue;
		$this->observaciones->ViewCustomAttributes = "";

		// plazo
		$this->plazo->ViewValue = $this->plazo->CurrentValue;
		$this->plazo->ViewCustomAttributes = "";

		// visitas
		$this->visitas->ViewValue = $this->visitas->CurrentValue;
		$this->visitas->ViewCustomAttributes = "";

			// venta_tipo
			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";
			$this->venta_tipo->TooltipValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";
			$this->activo->TooltipValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";
			$this->vendido->TooltipValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";
			$this->codigo->TooltipValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";
			$this->remate_id->TooltipValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";
			$this->conjunto_lote_id->TooltipValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";
			$this->orden->TooltipValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";
			$this->video->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";
			$this->precio->TooltipValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";
			$this->fecha_film->TooltipValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";
			$this->establecimiento->TooltipValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";
			$this->remitente->TooltipValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";
			$this->tipo->TooltipValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";
			$this->categoria->TooltipValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";
			$this->sub_categoria->TooltipValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";
			$this->cantidad->TooltipValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";
			$this->peso->TooltipValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";
			$this->caracteristicas->TooltipValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";
			$this->provincia->TooltipValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";
			$this->localidad->TooltipValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";
			$this->lugar->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";
			$this->edad_aprox->TooltipValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";
			$this->trazabilidad->TooltipValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";
			$this->mio_mio->TooltipValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";
			$this->garrapata->TooltipValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";
			$this->observaciones->TooltipValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";
			$this->plazo->TooltipValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
			$this->visitas->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// venta_tipo
			$this->venta_tipo->EditAttrs["class"] = "form-control";
			$this->venta_tipo->EditCustomAttributes = "";
			$this->venta_tipo->EditValue = $this->venta_tipo->Options(TRUE);

			// activo
			$this->activo->EditAttrs["class"] = "form-control";
			$this->activo->EditCustomAttributes = "";
			$this->activo->EditValue = $this->activo->Options(TRUE);

			// vendido
			$this->vendido->EditAttrs["class"] = "form-control";
			$this->vendido->EditCustomAttributes = "";
			$this->vendido->EditValue = $this->vendido->Options(TRUE);

			// codigo
			$this->codigo->EditAttrs["class"] = "form-control";
			$this->codigo->EditCustomAttributes = "";
			$this->codigo->EditValue = ew_HtmlEncode($this->codigo->CurrentValue);
			$this->codigo->PlaceHolder = ew_RemoveHtml($this->codigo->FldCaption());

			// remate_id
			$this->remate_id->EditAttrs["class"] = "form-control";
			$this->remate_id->EditCustomAttributes = "";
			if (trim(strval($this->remate_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->remate_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->remate_id->EditValue = $arwrk;

			// conjunto_lote_id
			$this->conjunto_lote_id->EditAttrs["class"] = "form-control";
			$this->conjunto_lote_id->EditCustomAttributes = "";
			if (trim(strval($this->conjunto_lote_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->conjunto_lote_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `remate_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `conjuntos_lotes`";
			$sWhereWrk = "";
			$this->conjunto_lote_id->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->conjunto_lote_id->EditValue = $arwrk;

			// orden
			$this->orden->EditAttrs["class"] = "form-control";
			$this->orden->EditCustomAttributes = "";
			$this->orden->EditValue = ew_HtmlEncode($this->orden->CurrentValue);
			$this->orden->PlaceHolder = ew_RemoveHtml($this->orden->FldCaption());

			// video
			$this->video->EditAttrs["class"] = "form-control";
			$this->video->EditCustomAttributes = "";
			$this->video->EditValue = ew_HtmlEncode($this->video->CurrentValue);
			$this->video->PlaceHolder = ew_RemoveHtml($this->video->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_1->Upload->DbValue)) {
				$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
			} else {
				$this->foto_1->EditValue = "";
			}
			if (!ew_Empty($this->foto_1->CurrentValue))
				$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_2->Upload->DbValue)) {
				$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
			} else {
				$this->foto_2->EditValue = "";
			}
			if (!ew_Empty($this->foto_2->CurrentValue))
				$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_3->Upload->DbValue)) {
				$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
			} else {
				$this->foto_3->EditValue = "";
			}
			if (!ew_Empty($this->foto_3->CurrentValue))
				$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_4->Upload->DbValue)) {
				$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
			} else {
				$this->foto_4->EditValue = "";
			}
			if (!ew_Empty($this->foto_4->CurrentValue))
				$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;

			// precio
			$this->precio->EditAttrs["class"] = "form-control";
			$this->precio->EditCustomAttributes = "";
			$this->precio->EditValue = ew_HtmlEncode($this->precio->CurrentValue);
			$this->precio->PlaceHolder = ew_RemoveHtml($this->precio->FldCaption());
			if (strval($this->precio->EditValue) <> "" && is_numeric($this->precio->EditValue)) $this->precio->EditValue = ew_FormatNumber($this->precio->EditValue, -2, -1, -2, 0);

			// fecha_film
			$this->fecha_film->EditAttrs["class"] = "form-control";
			$this->fecha_film->EditCustomAttributes = "";
			$this->fecha_film->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha_film->CurrentValue, 8));
			$this->fecha_film->PlaceHolder = ew_RemoveHtml($this->fecha_film->FldCaption());

			// establecimiento
			$this->establecimiento->EditAttrs["class"] = "form-control";
			$this->establecimiento->EditCustomAttributes = "";
			$this->establecimiento->EditValue = ew_HtmlEncode($this->establecimiento->CurrentValue);
			$this->establecimiento->PlaceHolder = ew_RemoveHtml($this->establecimiento->FldCaption());

			// remitente
			$this->remitente->EditAttrs["class"] = "form-control";
			$this->remitente->EditCustomAttributes = "";
			$this->remitente->EditValue = ew_HtmlEncode($this->remitente->CurrentValue);
			$this->remitente->PlaceHolder = ew_RemoveHtml($this->remitente->FldCaption());

			// tipo
			$this->tipo->EditAttrs["class"] = "form-control";
			$this->tipo->EditCustomAttributes = "";
			if (trim(strval($this->tipo->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->tipo->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->tipo->EditValue = $arwrk;

			// categoria
			$this->categoria->EditAttrs["class"] = "form-control";
			$this->categoria->EditCustomAttributes = "";
			if (trim(strval($this->categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->categoria->EditValue = $arwrk;

			// sub_categoria
			$this->sub_categoria->EditAttrs["class"] = "form-control";
			$this->sub_categoria->EditCustomAttributes = "";
			if (trim(strval($this->sub_categoria->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->sub_categoria->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->sub_categoria->EditValue = $arwrk;

			// cantidad
			$this->cantidad->EditAttrs["class"] = "form-control";
			$this->cantidad->EditCustomAttributes = "";
			$this->cantidad->EditValue = ew_HtmlEncode($this->cantidad->CurrentValue);
			$this->cantidad->PlaceHolder = ew_RemoveHtml($this->cantidad->FldCaption());

			// peso
			$this->peso->EditAttrs["class"] = "form-control";
			$this->peso->EditCustomAttributes = "";
			$this->peso->EditValue = ew_HtmlEncode($this->peso->CurrentValue);
			$this->peso->PlaceHolder = ew_RemoveHtml($this->peso->FldCaption());

			// caracteristicas
			$this->caracteristicas->EditAttrs["class"] = "form-control";
			$this->caracteristicas->EditCustomAttributes = "";
			$this->caracteristicas->EditValue = ew_HtmlEncode($this->caracteristicas->CurrentValue);
			$this->caracteristicas->PlaceHolder = ew_RemoveHtml($this->caracteristicas->FldCaption());

			// provincia
			$this->provincia->EditAttrs["class"] = "form-control";
			$this->provincia->EditCustomAttributes = "";
			if (trim(strval($this->provincia->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->provincia->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->provincia->EditValue = $arwrk;

			// localidad
			$this->localidad->EditAttrs["class"] = "form-control";
			$this->localidad->EditCustomAttributes = "";
			$this->localidad->EditValue = ew_HtmlEncode($this->localidad->CurrentValue);
			$this->localidad->PlaceHolder = ew_RemoveHtml($this->localidad->FldCaption());

			// lugar
			$this->lugar->EditAttrs["class"] = "form-control";
			$this->lugar->EditCustomAttributes = "";
			$this->lugar->EditValue = ew_HtmlEncode($this->lugar->CurrentValue);
			$this->lugar->PlaceHolder = ew_RemoveHtml($this->lugar->FldCaption());

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// edad_aprox
			$this->edad_aprox->EditAttrs["class"] = "form-control";
			$this->edad_aprox->EditCustomAttributes = "";
			$this->edad_aprox->EditValue = ew_HtmlEncode($this->edad_aprox->CurrentValue);
			$this->edad_aprox->PlaceHolder = ew_RemoveHtml($this->edad_aprox->FldCaption());

			// trazabilidad
			$this->trazabilidad->EditCustomAttributes = "";
			$this->trazabilidad->EditValue = $this->trazabilidad->Options(FALSE);

			// mio_mio
			$this->mio_mio->EditCustomAttributes = "";
			$this->mio_mio->EditValue = $this->mio_mio->Options(FALSE);

			// garrapata
			$this->garrapata->EditCustomAttributes = "";
			$this->garrapata->EditValue = $this->garrapata->Options(FALSE);

			// observaciones
			$this->observaciones->EditAttrs["class"] = "form-control";
			$this->observaciones->EditCustomAttributes = "";
			$this->observaciones->EditValue = ew_HtmlEncode($this->observaciones->CurrentValue);
			$this->observaciones->PlaceHolder = ew_RemoveHtml($this->observaciones->FldCaption());

			// plazo
			$this->plazo->EditAttrs["class"] = "form-control";
			$this->plazo->EditCustomAttributes = "";
			$this->plazo->EditValue = ew_HtmlEncode($this->plazo->CurrentValue);
			$this->plazo->PlaceHolder = ew_RemoveHtml($this->plazo->FldCaption());

			// visitas
			$this->visitas->EditAttrs["class"] = "form-control";
			$this->visitas->EditCustomAttributes = "";
			$this->visitas->EditValue = ew_HtmlEncode($this->visitas->CurrentValue);
			$this->visitas->PlaceHolder = ew_RemoveHtml($this->visitas->FldCaption());

			// Edit refer script
			// venta_tipo

			$this->venta_tipo->LinkCustomAttributes = "";
			$this->venta_tipo->HrefValue = "";

			// activo
			$this->activo->LinkCustomAttributes = "";
			$this->activo->HrefValue = "";

			// vendido
			$this->vendido->LinkCustomAttributes = "";
			$this->vendido->HrefValue = "";

			// codigo
			$this->codigo->LinkCustomAttributes = "";
			$this->codigo->HrefValue = "";

			// remate_id
			$this->remate_id->LinkCustomAttributes = "";
			$this->remate_id->HrefValue = "";

			// conjunto_lote_id
			$this->conjunto_lote_id->LinkCustomAttributes = "";
			$this->conjunto_lote_id->HrefValue = "";

			// orden
			$this->orden->LinkCustomAttributes = "";
			$this->orden->HrefValue = "";

			// video
			$this->video->LinkCustomAttributes = "";
			$this->video->HrefValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;

			// precio
			$this->precio->LinkCustomAttributes = "";
			$this->precio->HrefValue = "";

			// fecha_film
			$this->fecha_film->LinkCustomAttributes = "";
			$this->fecha_film->HrefValue = "";

			// establecimiento
			$this->establecimiento->LinkCustomAttributes = "";
			$this->establecimiento->HrefValue = "";

			// remitente
			$this->remitente->LinkCustomAttributes = "";
			$this->remitente->HrefValue = "";

			// tipo
			$this->tipo->LinkCustomAttributes = "";
			$this->tipo->HrefValue = "";

			// categoria
			$this->categoria->LinkCustomAttributes = "";
			$this->categoria->HrefValue = "";

			// sub_categoria
			$this->sub_categoria->LinkCustomAttributes = "";
			$this->sub_categoria->HrefValue = "";

			// cantidad
			$this->cantidad->LinkCustomAttributes = "";
			$this->cantidad->HrefValue = "";

			// peso
			$this->peso->LinkCustomAttributes = "";
			$this->peso->HrefValue = "";

			// caracteristicas
			$this->caracteristicas->LinkCustomAttributes = "";
			$this->caracteristicas->HrefValue = "";

			// provincia
			$this->provincia->LinkCustomAttributes = "";
			$this->provincia->HrefValue = "";

			// localidad
			$this->localidad->LinkCustomAttributes = "";
			$this->localidad->HrefValue = "";

			// lugar
			$this->lugar->LinkCustomAttributes = "";
			$this->lugar->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// edad_aprox
			$this->edad_aprox->LinkCustomAttributes = "";
			$this->edad_aprox->HrefValue = "";

			// trazabilidad
			$this->trazabilidad->LinkCustomAttributes = "";
			$this->trazabilidad->HrefValue = "";

			// mio_mio
			$this->mio_mio->LinkCustomAttributes = "";
			$this->mio_mio->HrefValue = "";

			// garrapata
			$this->garrapata->LinkCustomAttributes = "";
			$this->garrapata->HrefValue = "";

			// observaciones
			$this->observaciones->LinkCustomAttributes = "";
			$this->observaciones->HrefValue = "";

			// plazo
			$this->plazo->LinkCustomAttributes = "";
			$this->plazo->HrefValue = "";

			// visitas
			$this->visitas->LinkCustomAttributes = "";
			$this->visitas->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";
		$lUpdateCnt = 0;
		if ($this->venta_tipo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->activo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->vendido->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->codigo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->remate_id->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->conjunto_lote_id->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->orden->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->video->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->foto_1->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->foto_2->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->foto_3->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->foto_4->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->precio->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->fecha_film->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->establecimiento->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->remitente->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->tipo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->categoria->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->sub_categoria->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->cantidad->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->peso->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->caracteristicas->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->provincia->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->localidad->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->lugar->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->titulo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->edad_aprox->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->trazabilidad->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->mio_mio->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->garrapata->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->observaciones->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->plazo->MultiUpdate == "1") $lUpdateCnt++;
		if ($this->visitas->MultiUpdate == "1") $lUpdateCnt++;
		if ($lUpdateCnt == 0) {
			$gsFormError = $Language->Phrase("NoFieldSelected");
			return FALSE;
		}

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if ($this->precio->MultiUpdate <> "") {
			if (!ew_CheckNumber($this->precio->FormValue)) {
				ew_AddMessage($gsFormError, $this->precio->FldErrMsg());
			}
		}
		if ($this->fecha_film->MultiUpdate <> "") {
			if (!ew_CheckDateDef($this->fecha_film->FormValue)) {
				ew_AddMessage($gsFormError, $this->fecha_film->FldErrMsg());
			}
		}
		if ($this->categoria->MultiUpdate <> "" && !$this->categoria->FldIsDetailKey && !is_null($this->categoria->FormValue) && $this->categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->categoria->FldCaption(), $this->categoria->ReqErrMsg));
		}
		if ($this->sub_categoria->MultiUpdate <> "" && !$this->sub_categoria->FldIsDetailKey && !is_null($this->sub_categoria->FormValue) && $this->sub_categoria->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->sub_categoria->FldCaption(), $this->sub_categoria->ReqErrMsg));
		}
		if ($this->cantidad->MultiUpdate <> "") {
			if (!ew_CheckInteger($this->cantidad->FormValue)) {
				ew_AddMessage($gsFormError, $this->cantidad->FldErrMsg());
			}
		}
		if ($this->titulo->MultiUpdate <> "" && !$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if ($this->visitas->MultiUpdate <> "") {
			if (!ew_CheckInteger($this->visitas->FormValue)) {
				ew_AddMessage($gsFormError, $this->visitas->FldErrMsg());
			}
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// venta_tipo
			$this->venta_tipo->SetDbValueDef($rsnew, $this->venta_tipo->CurrentValue, NULL, $this->venta_tipo->ReadOnly || $this->venta_tipo->MultiUpdate <> "1");

			// activo
			$this->activo->SetDbValueDef($rsnew, $this->activo->CurrentValue, NULL, $this->activo->ReadOnly || $this->activo->MultiUpdate <> "1");

			// vendido
			$this->vendido->SetDbValueDef($rsnew, $this->vendido->CurrentValue, NULL, $this->vendido->ReadOnly || $this->vendido->MultiUpdate <> "1");

			// codigo
			$this->codigo->SetDbValueDef($rsnew, $this->codigo->CurrentValue, NULL, $this->codigo->ReadOnly || $this->codigo->MultiUpdate <> "1");

			// remate_id
			$this->remate_id->SetDbValueDef($rsnew, $this->remate_id->CurrentValue, NULL, $this->remate_id->ReadOnly || $this->remate_id->MultiUpdate <> "1");

			// conjunto_lote_id
			$this->conjunto_lote_id->SetDbValueDef($rsnew, $this->conjunto_lote_id->CurrentValue, NULL, $this->conjunto_lote_id->ReadOnly || $this->conjunto_lote_id->MultiUpdate <> "1");

			// orden
			$this->orden->SetDbValueDef($rsnew, $this->orden->CurrentValue, NULL, $this->orden->ReadOnly || $this->orden->MultiUpdate <> "1");

			// video
			$this->video->SetDbValueDef($rsnew, $this->video->CurrentValue, NULL, $this->video->ReadOnly || $this->video->MultiUpdate <> "1");

			// foto_1
			if ($this->foto_1->Visible && !$this->foto_1->ReadOnly && strval($this->foto_1->MultiUpdate) == "1" && !$this->foto_1->Upload->KeepFile) {
				$this->foto_1->Upload->DbValue = $rsold['foto_1']; // Get original value
				if ($this->foto_1->Upload->FileName == "") {
					$rsnew['foto_1'] = NULL;
				} else {
					$rsnew['foto_1'] = $this->foto_1->Upload->FileName;
				}
				$this->foto_1->ImageWidth = 750; // Resize width
				$this->foto_1->ImageHeight = 0; // Resize height
			}

			// foto_2
			if ($this->foto_2->Visible && !$this->foto_2->ReadOnly && strval($this->foto_2->MultiUpdate) == "1" && !$this->foto_2->Upload->KeepFile) {
				$this->foto_2->Upload->DbValue = $rsold['foto_2']; // Get original value
				if ($this->foto_2->Upload->FileName == "") {
					$rsnew['foto_2'] = NULL;
				} else {
					$rsnew['foto_2'] = $this->foto_2->Upload->FileName;
				}
				$this->foto_2->ImageWidth = 750; // Resize width
				$this->foto_2->ImageHeight = 0; // Resize height
			}

			// foto_3
			if ($this->foto_3->Visible && !$this->foto_3->ReadOnly && strval($this->foto_3->MultiUpdate) == "1" && !$this->foto_3->Upload->KeepFile) {
				$this->foto_3->Upload->DbValue = $rsold['foto_3']; // Get original value
				if ($this->foto_3->Upload->FileName == "") {
					$rsnew['foto_3'] = NULL;
				} else {
					$rsnew['foto_3'] = $this->foto_3->Upload->FileName;
				}
				$this->foto_3->ImageWidth = 750; // Resize width
				$this->foto_3->ImageHeight = 0; // Resize height
			}

			// foto_4
			if ($this->foto_4->Visible && !$this->foto_4->ReadOnly && strval($this->foto_4->MultiUpdate) == "1" && !$this->foto_4->Upload->KeepFile) {
				$this->foto_4->Upload->DbValue = $rsold['foto_4']; // Get original value
				if ($this->foto_4->Upload->FileName == "") {
					$rsnew['foto_4'] = NULL;
				} else {
					$rsnew['foto_4'] = $this->foto_4->Upload->FileName;
				}
				$this->foto_4->ImageWidth = 750; // Resize width
				$this->foto_4->ImageHeight = 0; // Resize height
			}

			// precio
			$this->precio->SetDbValueDef($rsnew, $this->precio->CurrentValue, NULL, $this->precio->ReadOnly || $this->precio->MultiUpdate <> "1");

			// fecha_film
			$this->fecha_film->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha_film->CurrentValue, 0), NULL, $this->fecha_film->ReadOnly || $this->fecha_film->MultiUpdate <> "1");

			// establecimiento
			$this->establecimiento->SetDbValueDef($rsnew, $this->establecimiento->CurrentValue, NULL, $this->establecimiento->ReadOnly || $this->establecimiento->MultiUpdate <> "1");

			// remitente
			$this->remitente->SetDbValueDef($rsnew, $this->remitente->CurrentValue, NULL, $this->remitente->ReadOnly || $this->remitente->MultiUpdate <> "1");

			// tipo
			$this->tipo->SetDbValueDef($rsnew, $this->tipo->CurrentValue, NULL, $this->tipo->ReadOnly || $this->tipo->MultiUpdate <> "1");

			// categoria
			$this->categoria->SetDbValueDef($rsnew, $this->categoria->CurrentValue, 0, $this->categoria->ReadOnly || $this->categoria->MultiUpdate <> "1");

			// sub_categoria
			$this->sub_categoria->SetDbValueDef($rsnew, $this->sub_categoria->CurrentValue, 0, $this->sub_categoria->ReadOnly || $this->sub_categoria->MultiUpdate <> "1");

			// cantidad
			$this->cantidad->SetDbValueDef($rsnew, $this->cantidad->CurrentValue, NULL, $this->cantidad->ReadOnly || $this->cantidad->MultiUpdate <> "1");

			// peso
			$this->peso->SetDbValueDef($rsnew, $this->peso->CurrentValue, NULL, $this->peso->ReadOnly || $this->peso->MultiUpdate <> "1");

			// caracteristicas
			$this->caracteristicas->SetDbValueDef($rsnew, $this->caracteristicas->CurrentValue, NULL, $this->caracteristicas->ReadOnly || $this->caracteristicas->MultiUpdate <> "1");

			// provincia
			$this->provincia->SetDbValueDef($rsnew, $this->provincia->CurrentValue, NULL, $this->provincia->ReadOnly || $this->provincia->MultiUpdate <> "1");

			// localidad
			$this->localidad->SetDbValueDef($rsnew, $this->localidad->CurrentValue, NULL, $this->localidad->ReadOnly || $this->localidad->MultiUpdate <> "1");

			// lugar
			$this->lugar->SetDbValueDef($rsnew, $this->lugar->CurrentValue, NULL, $this->lugar->ReadOnly || $this->lugar->MultiUpdate <> "1");

			// titulo
			$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", $this->titulo->ReadOnly || $this->titulo->MultiUpdate <> "1");

			// edad_aprox
			$this->edad_aprox->SetDbValueDef($rsnew, $this->edad_aprox->CurrentValue, NULL, $this->edad_aprox->ReadOnly || $this->edad_aprox->MultiUpdate <> "1");

			// trazabilidad
			$this->trazabilidad->SetDbValueDef($rsnew, $this->trazabilidad->CurrentValue, NULL, $this->trazabilidad->ReadOnly || $this->trazabilidad->MultiUpdate <> "1");

			// mio_mio
			$this->mio_mio->SetDbValueDef($rsnew, $this->mio_mio->CurrentValue, NULL, $this->mio_mio->ReadOnly || $this->mio_mio->MultiUpdate <> "1");

			// garrapata
			$this->garrapata->SetDbValueDef($rsnew, $this->garrapata->CurrentValue, NULL, $this->garrapata->ReadOnly || $this->garrapata->MultiUpdate <> "1");

			// observaciones
			$this->observaciones->SetDbValueDef($rsnew, $this->observaciones->CurrentValue, NULL, $this->observaciones->ReadOnly || $this->observaciones->MultiUpdate <> "1");

			// plazo
			$this->plazo->SetDbValueDef($rsnew, $this->plazo->CurrentValue, NULL, $this->plazo->ReadOnly || $this->plazo->MultiUpdate <> "1");

			// visitas
			$this->visitas->SetDbValueDef($rsnew, $this->visitas->CurrentValue, NULL, $this->visitas->ReadOnly || $this->visitas->MultiUpdate <> "1");
			if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
				if (!ew_Empty($this->foto_1->Upload->Value) && $this->UpdateCount == 1) {
					$rsnew['foto_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_1->UploadPath), $rsnew['foto_1']); // Get new file name
				}
			}
			if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
				if (!ew_Empty($this->foto_2->Upload->Value) && $this->UpdateCount == 1) {
					$rsnew['foto_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_2->UploadPath), $rsnew['foto_2']); // Get new file name
				}
			}
			if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
				if (!ew_Empty($this->foto_3->Upload->Value) && $this->UpdateCount == 1) {
					$rsnew['foto_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_3->UploadPath), $rsnew['foto_3']); // Get new file name
				}
			}
			if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
				if (!ew_Empty($this->foto_4->Upload->Value) && $this->UpdateCount == 1) {
					$rsnew['foto_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_4->UploadPath), $rsnew['foto_4']); // Get new file name
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
						if (!ew_Empty($this->foto_1->Upload->Value) && $this->UpdateCount == 1) {
							$this->foto_1->Upload->Resize($this->foto_1->ImageWidth, $this->foto_1->ImageHeight);
							if (!$this->foto_1->Upload->SaveToFile($this->foto_1->UploadPath, $rsnew['foto_1'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
						if (!ew_Empty($this->foto_2->Upload->Value) && $this->UpdateCount == 1) {
							$this->foto_2->Upload->Resize($this->foto_2->ImageWidth, $this->foto_2->ImageHeight);
							if (!$this->foto_2->Upload->SaveToFile($this->foto_2->UploadPath, $rsnew['foto_2'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
						if (!ew_Empty($this->foto_3->Upload->Value) && $this->UpdateCount == 1) {
							$this->foto_3->Upload->Resize($this->foto_3->ImageWidth, $this->foto_3->ImageHeight);
							if (!$this->foto_3->Upload->SaveToFile($this->foto_3->UploadPath, $rsnew['foto_3'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
						if (!ew_Empty($this->foto_4->Upload->Value) && $this->UpdateCount == 1) {
							$this->foto_4->Upload->Resize($this->foto_4->ImageWidth, $this->foto_4->ImageHeight);
							if (!$this->foto_4->Upload->SaveToFile($this->foto_4->UploadPath, $rsnew['foto_4'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// foto_1
		ew_CleanUploadTempPath($this->foto_1, $this->foto_1->Upload->Index);

		// foto_2
		ew_CleanUploadTempPath($this->foto_2, $this->foto_2->Upload->Index);

		// foto_3
		ew_CleanUploadTempPath($this->foto_3, $this->foto_3->Upload->Index);

		// foto_4
		ew_CleanUploadTempPath($this->foto_4, $this->foto_4->Upload->Index);
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("loteslist.php"), "", $this->TableVar, TRUE);
		$PageId = "update";
		$Breadcrumb->Add("update", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_remate_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `numero` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `remates`";
			$sWhereWrk = "";
			$this->remate_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->remate_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_conjunto_lote_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `conjuntos_lotes`";
			$sWhereWrk = "{filter}";
			$this->conjunto_lote_id->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`remate_id` IN ({filter_value})', "t1" => "19", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->conjunto_lote_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo`";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_tipo":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `tipos`";
			$sWhereWrk = "";
			$this->tipo->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->tipo, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `categorias_ganado`";
			$sWhereWrk = "";
			$this->categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_sub_categoria":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `abreviatura` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `sub_categorias`";
			$sWhereWrk = "";
			$this->sub_categoria->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->sub_categoria, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `abreviatura` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_provincia":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `provincias`";
			$sWhereWrk = "";
			$this->provincia->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "3", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->provincia, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `nombre` Asc";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `nombre` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `localidades`";
			$sWhereWrk = "{filter}";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "", "f1" => '`provincia_id` IN ({filter_value})', "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_localidad":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id`, `nombre` AS `DispFld` FROM `localidades`";
			$sWhereWrk = "(`nombre` LIKE '{query_value}%') AND ({filter})";
			$this->localidad->LookupFilters = array();
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f1" => "`provincia_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->localidad, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($lotes_update)) $lotes_update = new clotes_update();

// Page init
$lotes_update->Page_Init();

// Page main
$lotes_update->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$lotes_update->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "update";
var CurrentForm = flotesupdate = new ew_Form("flotesupdate", "update");

// Validate form
flotesupdate.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	if (!ew_UpdateSelected(fobj)) {
		ew_Alert(ewLanguage.Phrase("NoFieldSelected"));
		return false;
	}
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_precio");
			uelm = this.GetElements("u" + infix + "_precio");
			if (uelm && uelm.checked && elm && !ew_CheckNumber(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->precio->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_fecha_film");
			uelm = this.GetElements("u" + infix + "_fecha_film");
			if (uelm && uelm.checked && elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->fecha_film->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_categoria");
			uelm = this.GetElements("u" + infix + "_categoria");
			if (uelm && uelm.checked) {
				if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
					return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->categoria->FldCaption(), $lotes->categoria->ReqErrMsg)) ?>");
			}
			elm = this.GetElements("x" + infix + "_sub_categoria");
			uelm = this.GetElements("u" + infix + "_sub_categoria");
			if (uelm && uelm.checked) {
				if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
					return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->sub_categoria->FldCaption(), $lotes->sub_categoria->ReqErrMsg)) ?>");
			}
			elm = this.GetElements("x" + infix + "_cantidad");
			uelm = this.GetElements("u" + infix + "_cantidad");
			if (uelm && uelm.checked && elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->cantidad->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			uelm = this.GetElements("u" + infix + "_titulo");
			if (uelm && uelm.checked) {
				if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
					return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $lotes->titulo->FldCaption(), $lotes->titulo->ReqErrMsg)) ?>");
			}
			elm = this.GetElements("x" + infix + "_visitas");
			uelm = this.GetElements("u" + infix + "_visitas");
			if (uelm && uelm.checked && elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($lotes->visitas->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}
	return true;
}

// Form_CustomValidate event
flotesupdate.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
flotesupdate.ValidateRequired = true;
<?php } else { ?>
flotesupdate.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
flotesupdate.Lists["x_venta_tipo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_venta_tipo"].Options = <?php echo json_encode($lotes->venta_tipo->Options()) ?>;
flotesupdate.Lists["x_activo"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_activo"].Options = <?php echo json_encode($lotes->activo->Options()) ?>;
flotesupdate.Lists["x_vendido"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_vendido"].Options = <?php echo json_encode($lotes->vendido->Options()) ?>;
flotesupdate.Lists["x_remate_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_numero","","",""],"ParentFields":[],"ChildFields":["x_conjunto_lote_id"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"remates"};
flotesupdate.Lists["x_conjunto_lote_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":["x_remate_id"],"ChildFields":[],"FilterFields":["x_remate_id"],"Options":[],"Template":"","LinkTable":"conjuntos_lotes"};
flotesupdate.Lists["x_tipo"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"tipos"};
flotesupdate.Lists["x_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"categorias_ganado"};
flotesupdate.Lists["x_sub_categoria"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_abreviatura","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"sub_categorias"};
flotesupdate.Lists["x_provincia"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":[],"ChildFields":["x_localidad"],"FilterFields":[],"Options":[],"Template":"","LinkTable":"provincias"};
flotesupdate.Lists["x_localidad"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_nombre","","",""],"ParentFields":["x_provincia"],"ChildFields":[],"FilterFields":["x_provincia_id"],"Options":[],"Template":"","LinkTable":"localidades"};
flotesupdate.Lists["x_trazabilidad"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_trazabilidad"].Options = <?php echo json_encode($lotes->trazabilidad->Options()) ?>;
flotesupdate.Lists["x_mio_mio"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_mio_mio"].Options = <?php echo json_encode($lotes->mio_mio->Options()) ?>;
flotesupdate.Lists["x_garrapata"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
flotesupdate.Lists["x_garrapata"].Options = <?php echo json_encode($lotes->garrapata->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$lotes_update->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $lotes_update->ShowPageHeader(); ?>
<?php
$lotes_update->ShowMessage();
?>
<form name="flotesupdate" id="flotesupdate" class="<?php echo $lotes_update->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($lotes_update->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $lotes_update->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="lotes">
<input type="hidden" name="a_update" id="a_update" value="U">
<?php if ($lotes_update->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<?php foreach ($lotes_update->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div id="tbl_lotesupdate">
	<div class="checkbox">
		<label><input type="checkbox" name="u" id="u" onclick="ew_SelectAll(this);"> <?php echo $Language->Phrase("UpdateSelectAll") ?></label>
	</div>
<?php if ($lotes->venta_tipo->Visible) { // venta_tipo ?>
	<div id="r_venta_tipo" class="form-group">
		<label for="x_venta_tipo" class="col-sm-2 control-label">
<input type="checkbox" name="u_venta_tipo" id="u_venta_tipo" value="1"<?php echo ($lotes->venta_tipo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->venta_tipo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->venta_tipo->CellAttributes() ?>>
<span id="el_lotes_venta_tipo">
<select data-table="lotes" data-field="x_venta_tipo" data-value-separator="<?php echo $lotes->venta_tipo->DisplayValueSeparatorAttribute() ?>" id="x_venta_tipo" name="x_venta_tipo"<?php echo $lotes->venta_tipo->EditAttributes() ?>>
<?php echo $lotes->venta_tipo->SelectOptionListHtml("x_venta_tipo") ?>
</select>
</span>
<?php echo $lotes->venta_tipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->activo->Visible) { // activo ?>
	<div id="r_activo" class="form-group">
		<label for="x_activo" class="col-sm-2 control-label">
<input type="checkbox" name="u_activo" id="u_activo" value="1"<?php echo ($lotes->activo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->activo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->activo->CellAttributes() ?>>
<span id="el_lotes_activo">
<select data-table="lotes" data-field="x_activo" data-value-separator="<?php echo $lotes->activo->DisplayValueSeparatorAttribute() ?>" id="x_activo" name="x_activo"<?php echo $lotes->activo->EditAttributes() ?>>
<?php echo $lotes->activo->SelectOptionListHtml("x_activo") ?>
</select>
</span>
<?php echo $lotes->activo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->vendido->Visible) { // vendido ?>
	<div id="r_vendido" class="form-group">
		<label for="x_vendido" class="col-sm-2 control-label">
<input type="checkbox" name="u_vendido" id="u_vendido" value="1"<?php echo ($lotes->vendido->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->vendido->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->vendido->CellAttributes() ?>>
<span id="el_lotes_vendido">
<select data-table="lotes" data-field="x_vendido" data-value-separator="<?php echo $lotes->vendido->DisplayValueSeparatorAttribute() ?>" id="x_vendido" name="x_vendido"<?php echo $lotes->vendido->EditAttributes() ?>>
<?php echo $lotes->vendido->SelectOptionListHtml("x_vendido") ?>
</select>
</span>
<?php echo $lotes->vendido->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->codigo->Visible) { // codigo ?>
	<div id="r_codigo" class="form-group">
		<label for="x_codigo" class="col-sm-2 control-label">
<input type="checkbox" name="u_codigo" id="u_codigo" value="1"<?php echo ($lotes->codigo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->codigo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->codigo->CellAttributes() ?>>
<span id="el_lotes_codigo">
<input type="text" data-table="lotes" data-field="x_codigo" name="x_codigo" id="x_codigo" size="8" maxlength="8" placeholder="<?php echo ew_HtmlEncode($lotes->codigo->getPlaceHolder()) ?>" value="<?php echo $lotes->codigo->EditValue ?>"<?php echo $lotes->codigo->EditAttributes() ?>>
</span>
<?php echo $lotes->codigo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->remate_id->Visible) { // remate_id ?>
	<div id="r_remate_id" class="form-group">
		<label for="x_remate_id" class="col-sm-2 control-label">
<input type="checkbox" name="u_remate_id" id="u_remate_id" value="1"<?php echo ($lotes->remate_id->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->remate_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->remate_id->CellAttributes() ?>>
<span id="el_lotes_remate_id">
<?php $lotes->remate_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->remate_id->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_remate_id" data-value-separator="<?php echo $lotes->remate_id->DisplayValueSeparatorAttribute() ?>" id="x_remate_id" name="x_remate_id"<?php echo $lotes->remate_id->EditAttributes() ?>>
<?php echo $lotes->remate_id->SelectOptionListHtml("x_remate_id") ?>
</select>
<input type="hidden" name="s_x_remate_id" id="s_x_remate_id" value="<?php echo $lotes->remate_id->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->remate_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->conjunto_lote_id->Visible) { // conjunto_lote_id ?>
	<div id="r_conjunto_lote_id" class="form-group">
		<label for="x_conjunto_lote_id" class="col-sm-2 control-label">
<input type="checkbox" name="u_conjunto_lote_id" id="u_conjunto_lote_id" value="1"<?php echo ($lotes->conjunto_lote_id->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->conjunto_lote_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->conjunto_lote_id->CellAttributes() ?>>
<span id="el_lotes_conjunto_lote_id">
<select data-table="lotes" data-field="x_conjunto_lote_id" data-value-separator="<?php echo $lotes->conjunto_lote_id->DisplayValueSeparatorAttribute() ?>" id="x_conjunto_lote_id" name="x_conjunto_lote_id"<?php echo $lotes->conjunto_lote_id->EditAttributes() ?>>
<?php echo $lotes->conjunto_lote_id->SelectOptionListHtml("x_conjunto_lote_id") ?>
</select>
<input type="hidden" name="s_x_conjunto_lote_id" id="s_x_conjunto_lote_id" value="<?php echo $lotes->conjunto_lote_id->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->conjunto_lote_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->orden->Visible) { // orden ?>
	<div id="r_orden" class="form-group">
		<label for="x_orden" class="col-sm-2 control-label">
<input type="checkbox" name="u_orden" id="u_orden" value="1"<?php echo ($lotes->orden->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->orden->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->orden->CellAttributes() ?>>
<span id="el_lotes_orden">
<input type="text" data-table="lotes" data-field="x_orden" name="x_orden" id="x_orden" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->orden->getPlaceHolder()) ?>" value="<?php echo $lotes->orden->EditValue ?>"<?php echo $lotes->orden->EditAttributes() ?>>
</span>
<?php echo $lotes->orden->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->video->Visible) { // video ?>
	<div id="r_video" class="form-group">
		<label for="x_video" class="col-sm-2 control-label">
<input type="checkbox" name="u_video" id="u_video" value="1"<?php echo ($lotes->video->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->video->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->video->CellAttributes() ?>>
<span id="el_lotes_video">
<input type="text" data-table="lotes" data-field="x_video" name="x_video" id="x_video" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->video->getPlaceHolder()) ?>" value="<?php echo $lotes->video->EditValue ?>"<?php echo $lotes->video->EditAttributes() ?>>
</span>
<?php echo $lotes->video->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_1->Visible) { // foto_1 ?>
	<div id="r_foto_1" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_foto_1" id="u_foto_1" value="1"<?php echo ($lotes->foto_1->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->foto_1->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_1->CellAttributes() ?>>
<span id="el_lotes_foto_1">
<div id="fd_x_foto_1">
<span title="<?php echo $lotes->foto_1->FldTitle() ? $lotes->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_1->ReadOnly || $lotes->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_1" name="x_foto_1" id="x_foto_1"<?php echo $lotes->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_1" id= "fn_x_foto_1" value="<?php echo $lotes->foto_1->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_1"] == "0") { ?>
<input type="hidden" name="fa_x_foto_1" id= "fa_x_foto_1" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_1" id= "fa_x_foto_1" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_1" id= "fs_x_foto_1" value="255">
<input type="hidden" name="fx_x_foto_1" id= "fx_x_foto_1" value="<?php echo $lotes->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_1" id= "fm_x_foto_1" value="<?php echo $lotes->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_2->Visible) { // foto_2 ?>
	<div id="r_foto_2" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_foto_2" id="u_foto_2" value="1"<?php echo ($lotes->foto_2->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->foto_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_2->CellAttributes() ?>>
<span id="el_lotes_foto_2">
<div id="fd_x_foto_2">
<span title="<?php echo $lotes->foto_2->FldTitle() ? $lotes->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_2->ReadOnly || $lotes->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_2" name="x_foto_2" id="x_foto_2"<?php echo $lotes->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_2" id= "fn_x_foto_2" value="<?php echo $lotes->foto_2->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_2"] == "0") { ?>
<input type="hidden" name="fa_x_foto_2" id= "fa_x_foto_2" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_2" id= "fa_x_foto_2" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_2" id= "fs_x_foto_2" value="255">
<input type="hidden" name="fx_x_foto_2" id= "fx_x_foto_2" value="<?php echo $lotes->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_2" id= "fm_x_foto_2" value="<?php echo $lotes->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_3->Visible) { // foto_3 ?>
	<div id="r_foto_3" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_foto_3" id="u_foto_3" value="1"<?php echo ($lotes->foto_3->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->foto_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_3->CellAttributes() ?>>
<span id="el_lotes_foto_3">
<div id="fd_x_foto_3">
<span title="<?php echo $lotes->foto_3->FldTitle() ? $lotes->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_3->ReadOnly || $lotes->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_3" name="x_foto_3" id="x_foto_3"<?php echo $lotes->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_3" id= "fn_x_foto_3" value="<?php echo $lotes->foto_3->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_3"] == "0") { ?>
<input type="hidden" name="fa_x_foto_3" id= "fa_x_foto_3" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_3" id= "fa_x_foto_3" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_3" id= "fs_x_foto_3" value="255">
<input type="hidden" name="fx_x_foto_3" id= "fx_x_foto_3" value="<?php echo $lotes->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_3" id= "fm_x_foto_3" value="<?php echo $lotes->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->foto_4->Visible) { // foto_4 ?>
	<div id="r_foto_4" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_foto_4" id="u_foto_4" value="1"<?php echo ($lotes->foto_4->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->foto_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->foto_4->CellAttributes() ?>>
<span id="el_lotes_foto_4">
<div id="fd_x_foto_4">
<span title="<?php echo $lotes->foto_4->FldTitle() ? $lotes->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($lotes->foto_4->ReadOnly || $lotes->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="lotes" data-field="x_foto_4" name="x_foto_4" id="x_foto_4"<?php echo $lotes->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_4" id= "fn_x_foto_4" value="<?php echo $lotes->foto_4->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_4"] == "0") { ?>
<input type="hidden" name="fa_x_foto_4" id= "fa_x_foto_4" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_4" id= "fa_x_foto_4" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_4" id= "fs_x_foto_4" value="255">
<input type="hidden" name="fx_x_foto_4" id= "fx_x_foto_4" value="<?php echo $lotes->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_4" id= "fm_x_foto_4" value="<?php echo $lotes->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $lotes->foto_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->precio->Visible) { // precio ?>
	<div id="r_precio" class="form-group">
		<label for="x_precio" class="col-sm-2 control-label">
<input type="checkbox" name="u_precio" id="u_precio" value="1"<?php echo ($lotes->precio->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->precio->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->precio->CellAttributes() ?>>
<span id="el_lotes_precio">
<input type="text" data-table="lotes" data-field="x_precio" name="x_precio" id="x_precio" size="10" maxlength="10" placeholder="<?php echo ew_HtmlEncode($lotes->precio->getPlaceHolder()) ?>" value="<?php echo $lotes->precio->EditValue ?>"<?php echo $lotes->precio->EditAttributes() ?>>
</span>
<?php echo $lotes->precio->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->fecha_film->Visible) { // fecha_film ?>
	<div id="r_fecha_film" class="form-group">
		<label for="x_fecha_film" class="col-sm-2 control-label">
<input type="checkbox" name="u_fecha_film" id="u_fecha_film" value="1"<?php echo ($lotes->fecha_film->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->fecha_film->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->fecha_film->CellAttributes() ?>>
<span id="el_lotes_fecha_film">
<input type="text" data-table="lotes" data-field="x_fecha_film" name="x_fecha_film" id="x_fecha_film" placeholder="<?php echo ew_HtmlEncode($lotes->fecha_film->getPlaceHolder()) ?>" value="<?php echo $lotes->fecha_film->EditValue ?>"<?php echo $lotes->fecha_film->EditAttributes() ?>>
<?php if (!$lotes->fecha_film->ReadOnly && !$lotes->fecha_film->Disabled && !isset($lotes->fecha_film->EditAttrs["readonly"]) && !isset($lotes->fecha_film->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("flotesupdate", "x_fecha_film", 0);
</script>
<?php } ?>
</span>
<?php echo $lotes->fecha_film->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->establecimiento->Visible) { // establecimiento ?>
	<div id="r_establecimiento" class="form-group">
		<label for="x_establecimiento" class="col-sm-2 control-label">
<input type="checkbox" name="u_establecimiento" id="u_establecimiento" value="1"<?php echo ($lotes->establecimiento->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->establecimiento->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->establecimiento->CellAttributes() ?>>
<span id="el_lotes_establecimiento">
<input type="text" data-table="lotes" data-field="x_establecimiento" name="x_establecimiento" id="x_establecimiento" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->establecimiento->getPlaceHolder()) ?>" value="<?php echo $lotes->establecimiento->EditValue ?>"<?php echo $lotes->establecimiento->EditAttributes() ?>>
</span>
<?php echo $lotes->establecimiento->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->remitente->Visible) { // remitente ?>
	<div id="r_remitente" class="form-group">
		<label for="x_remitente" class="col-sm-2 control-label">
<input type="checkbox" name="u_remitente" id="u_remitente" value="1"<?php echo ($lotes->remitente->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->remitente->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->remitente->CellAttributes() ?>>
<span id="el_lotes_remitente">
<input type="text" data-table="lotes" data-field="x_remitente" name="x_remitente" id="x_remitente" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->remitente->getPlaceHolder()) ?>" value="<?php echo $lotes->remitente->EditValue ?>"<?php echo $lotes->remitente->EditAttributes() ?>>
</span>
<?php echo $lotes->remitente->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->tipo->Visible) { // tipo ?>
	<div id="r_tipo" class="form-group">
		<label for="x_tipo" class="col-sm-2 control-label">
<input type="checkbox" name="u_tipo" id="u_tipo" value="1"<?php echo ($lotes->tipo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->tipo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->tipo->CellAttributes() ?>>
<span id="el_lotes_tipo">
<select data-table="lotes" data-field="x_tipo" data-value-separator="<?php echo $lotes->tipo->DisplayValueSeparatorAttribute() ?>" id="x_tipo" name="x_tipo"<?php echo $lotes->tipo->EditAttributes() ?>>
<?php echo $lotes->tipo->SelectOptionListHtml("x_tipo") ?>
</select>
<input type="hidden" name="s_x_tipo" id="s_x_tipo" value="<?php echo $lotes->tipo->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->tipo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->categoria->Visible) { // categoria ?>
	<div id="r_categoria" class="form-group">
		<label for="x_categoria" class="col-sm-2 control-label">
<input type="checkbox" name="u_categoria" id="u_categoria" value="1"<?php echo ($lotes->categoria->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->categoria->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->categoria->CellAttributes() ?>>
<span id="el_lotes_categoria">
<select data-table="lotes" data-field="x_categoria" data-value-separator="<?php echo $lotes->categoria->DisplayValueSeparatorAttribute() ?>" id="x_categoria" name="x_categoria"<?php echo $lotes->categoria->EditAttributes() ?>>
<?php echo $lotes->categoria->SelectOptionListHtml("x_categoria") ?>
</select>
<input type="hidden" name="s_x_categoria" id="s_x_categoria" value="<?php echo $lotes->categoria->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->categoria->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->sub_categoria->Visible) { // sub_categoria ?>
	<div id="r_sub_categoria" class="form-group">
		<label for="x_sub_categoria" class="col-sm-2 control-label">
<input type="checkbox" name="u_sub_categoria" id="u_sub_categoria" value="1"<?php echo ($lotes->sub_categoria->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->sub_categoria->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->sub_categoria->CellAttributes() ?>>
<span id="el_lotes_sub_categoria">
<select data-table="lotes" data-field="x_sub_categoria" data-value-separator="<?php echo $lotes->sub_categoria->DisplayValueSeparatorAttribute() ?>" id="x_sub_categoria" name="x_sub_categoria"<?php echo $lotes->sub_categoria->EditAttributes() ?>>
<?php echo $lotes->sub_categoria->SelectOptionListHtml("x_sub_categoria") ?>
</select>
<input type="hidden" name="s_x_sub_categoria" id="s_x_sub_categoria" value="<?php echo $lotes->sub_categoria->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->sub_categoria->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->cantidad->Visible) { // cantidad ?>
	<div id="r_cantidad" class="form-group">
		<label for="x_cantidad" class="col-sm-2 control-label">
<input type="checkbox" name="u_cantidad" id="u_cantidad" value="1"<?php echo ($lotes->cantidad->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->cantidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->cantidad->CellAttributes() ?>>
<span id="el_lotes_cantidad">
<input type="text" data-table="lotes" data-field="x_cantidad" name="x_cantidad" id="x_cantidad" size="6" maxlength="6" placeholder="<?php echo ew_HtmlEncode($lotes->cantidad->getPlaceHolder()) ?>" value="<?php echo $lotes->cantidad->EditValue ?>"<?php echo $lotes->cantidad->EditAttributes() ?>>
</span>
<?php echo $lotes->cantidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->peso->Visible) { // peso ?>
	<div id="r_peso" class="form-group">
		<label for="x_peso" class="col-sm-2 control-label">
<input type="checkbox" name="u_peso" id="u_peso" value="1"<?php echo ($lotes->peso->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->peso->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->peso->CellAttributes() ?>>
<span id="el_lotes_peso">
<input type="text" data-table="lotes" data-field="x_peso" name="x_peso" id="x_peso" size="60" maxlength="50" placeholder="<?php echo ew_HtmlEncode($lotes->peso->getPlaceHolder()) ?>" value="<?php echo $lotes->peso->EditValue ?>"<?php echo $lotes->peso->EditAttributes() ?>>
</span>
<?php echo $lotes->peso->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->caracteristicas->Visible) { // caracteristicas ?>
	<div id="r_caracteristicas" class="form-group">
		<label for="x_caracteristicas" class="col-sm-2 control-label">
<input type="checkbox" name="u_caracteristicas" id="u_caracteristicas" value="1"<?php echo ($lotes->caracteristicas->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->caracteristicas->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->caracteristicas->CellAttributes() ?>>
<span id="el_lotes_caracteristicas">
<input type="text" data-table="lotes" data-field="x_caracteristicas" name="x_caracteristicas" id="x_caracteristicas" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->caracteristicas->getPlaceHolder()) ?>" value="<?php echo $lotes->caracteristicas->EditValue ?>"<?php echo $lotes->caracteristicas->EditAttributes() ?>>
</span>
<?php echo $lotes->caracteristicas->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->provincia->Visible) { // provincia ?>
	<div id="r_provincia" class="form-group">
		<label for="x_provincia" class="col-sm-2 control-label">
<input type="checkbox" name="u_provincia" id="u_provincia" value="1"<?php echo ($lotes->provincia->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->provincia->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->provincia->CellAttributes() ?>>
<span id="el_lotes_provincia">
<?php $lotes->provincia->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$lotes->provincia->EditAttrs["onchange"]; ?>
<select data-table="lotes" data-field="x_provincia" data-value-separator="<?php echo $lotes->provincia->DisplayValueSeparatorAttribute() ?>" id="x_provincia" name="x_provincia"<?php echo $lotes->provincia->EditAttributes() ?>>
<?php echo $lotes->provincia->SelectOptionListHtml("x_provincia") ?>
</select>
<input type="hidden" name="s_x_provincia" id="s_x_provincia" value="<?php echo $lotes->provincia->LookupFilterQuery() ?>">
</span>
<?php echo $lotes->provincia->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->localidad->Visible) { // localidad ?>
	<div id="r_localidad" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_localidad" id="u_localidad" value="1"<?php echo ($lotes->localidad->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->localidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->localidad->CellAttributes() ?>>
<span id="el_lotes_localidad">
<?php
$wrkonchange = trim(" " . @$lotes->localidad->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$lotes->localidad->EditAttrs["onchange"] = "";
?>
<span id="as_x_localidad" style="white-space: nowrap; z-index: 8750">
	<input type="text" name="sv_x_localidad" id="sv_x_localidad" value="<?php echo $lotes->localidad->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($lotes->localidad->getPlaceHolder()) ?>"<?php echo $lotes->localidad->EditAttributes() ?>>
</span>
<input type="hidden" data-table="lotes" data-field="x_localidad" data-value-separator="<?php echo $lotes->localidad->DisplayValueSeparatorAttribute() ?>" name="x_localidad" id="x_localidad" value="<?php echo ew_HtmlEncode($lotes->localidad->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<input type="hidden" name="q_x_localidad" id="q_x_localidad" value="<?php echo $lotes->localidad->LookupFilterQuery(true) ?>">
<script type="text/javascript">
flotesupdate.CreateAutoSuggest({"id":"x_localidad","forceSelect":false});
</script>
</span>
<?php echo $lotes->localidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->lugar->Visible) { // lugar ?>
	<div id="r_lugar" class="form-group">
		<label for="x_lugar" class="col-sm-2 control-label">
<input type="checkbox" name="u_lugar" id="u_lugar" value="1"<?php echo ($lotes->lugar->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->lugar->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->lugar->CellAttributes() ?>>
<span id="el_lotes_lugar">
<input type="text" data-table="lotes" data-field="x_lugar" name="x_lugar" id="x_lugar" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->lugar->getPlaceHolder()) ?>" value="<?php echo $lotes->lugar->EditValue ?>"<?php echo $lotes->lugar->EditAttributes() ?>>
</span>
<?php echo $lotes->lugar->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->titulo->Visible) { // titulo ?>
	<div id="r_titulo" class="form-group">
		<label for="x_titulo" class="col-sm-2 control-label">
<input type="checkbox" name="u_titulo" id="u_titulo" value="1"<?php echo ($lotes->titulo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->titulo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->titulo->CellAttributes() ?>>
<span id="el_lotes_titulo">
<input type="text" data-table="lotes" data-field="x_titulo" name="x_titulo" id="x_titulo" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->titulo->getPlaceHolder()) ?>" value="<?php echo $lotes->titulo->EditValue ?>"<?php echo $lotes->titulo->EditAttributes() ?>>
</span>
<?php echo $lotes->titulo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->edad_aprox->Visible) { // edad_aprox ?>
	<div id="r_edad_aprox" class="form-group">
		<label for="x_edad_aprox" class="col-sm-2 control-label">
<input type="checkbox" name="u_edad_aprox" id="u_edad_aprox" value="1"<?php echo ($lotes->edad_aprox->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->edad_aprox->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->edad_aprox->CellAttributes() ?>>
<span id="el_lotes_edad_aprox">
<input type="text" data-table="lotes" data-field="x_edad_aprox" name="x_edad_aprox" id="x_edad_aprox" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->edad_aprox->getPlaceHolder()) ?>" value="<?php echo $lotes->edad_aprox->EditValue ?>"<?php echo $lotes->edad_aprox->EditAttributes() ?>>
</span>
<?php echo $lotes->edad_aprox->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->trazabilidad->Visible) { // trazabilidad ?>
	<div id="r_trazabilidad" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_trazabilidad" id="u_trazabilidad" value="1"<?php echo ($lotes->trazabilidad->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->trazabilidad->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->trazabilidad->CellAttributes() ?>>
<span id="el_lotes_trazabilidad">
<div id="tp_x_trazabilidad" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_trazabilidad" data-value-separator="<?php echo $lotes->trazabilidad->DisplayValueSeparatorAttribute() ?>" name="x_trazabilidad" id="x_trazabilidad" value="{value}"<?php echo $lotes->trazabilidad->EditAttributes() ?>></div>
<div id="dsl_x_trazabilidad" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->trazabilidad->RadioButtonListHtml(FALSE, "x_trazabilidad") ?>
</div></div>
</span>
<?php echo $lotes->trazabilidad->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->mio_mio->Visible) { // mio_mio ?>
	<div id="r_mio_mio" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_mio_mio" id="u_mio_mio" value="1"<?php echo ($lotes->mio_mio->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->mio_mio->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->mio_mio->CellAttributes() ?>>
<span id="el_lotes_mio_mio">
<div id="tp_x_mio_mio" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_mio_mio" data-value-separator="<?php echo $lotes->mio_mio->DisplayValueSeparatorAttribute() ?>" name="x_mio_mio" id="x_mio_mio" value="{value}"<?php echo $lotes->mio_mio->EditAttributes() ?>></div>
<div id="dsl_x_mio_mio" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->mio_mio->RadioButtonListHtml(FALSE, "x_mio_mio") ?>
</div></div>
</span>
<?php echo $lotes->mio_mio->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->garrapata->Visible) { // garrapata ?>
	<div id="r_garrapata" class="form-group">
		<label class="col-sm-2 control-label">
<input type="checkbox" name="u_garrapata" id="u_garrapata" value="1"<?php echo ($lotes->garrapata->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->garrapata->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->garrapata->CellAttributes() ?>>
<span id="el_lotes_garrapata">
<div id="tp_x_garrapata" class="ewTemplate"><input type="radio" data-table="lotes" data-field="x_garrapata" data-value-separator="<?php echo $lotes->garrapata->DisplayValueSeparatorAttribute() ?>" name="x_garrapata" id="x_garrapata" value="{value}"<?php echo $lotes->garrapata->EditAttributes() ?>></div>
<div id="dsl_x_garrapata" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php echo $lotes->garrapata->RadioButtonListHtml(FALSE, "x_garrapata") ?>
</div></div>
</span>
<?php echo $lotes->garrapata->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->observaciones->Visible) { // observaciones ?>
	<div id="r_observaciones" class="form-group">
		<label for="x_observaciones" class="col-sm-2 control-label">
<input type="checkbox" name="u_observaciones" id="u_observaciones" value="1"<?php echo ($lotes->observaciones->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->observaciones->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->observaciones->CellAttributes() ?>>
<span id="el_lotes_observaciones">
<textarea data-table="lotes" data-field="x_observaciones" name="x_observaciones" id="x_observaciones" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($lotes->observaciones->getPlaceHolder()) ?>"<?php echo $lotes->observaciones->EditAttributes() ?>><?php echo $lotes->observaciones->EditValue ?></textarea>
</span>
<?php echo $lotes->observaciones->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->plazo->Visible) { // plazo ?>
	<div id="r_plazo" class="form-group">
		<label for="x_plazo" class="col-sm-2 control-label">
<input type="checkbox" name="u_plazo" id="u_plazo" value="1"<?php echo ($lotes->plazo->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->plazo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->plazo->CellAttributes() ?>>
<span id="el_lotes_plazo">
<input type="text" data-table="lotes" data-field="x_plazo" name="x_plazo" id="x_plazo" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($lotes->plazo->getPlaceHolder()) ?>" value="<?php echo $lotes->plazo->EditValue ?>"<?php echo $lotes->plazo->EditAttributes() ?>>
</span>
<?php echo $lotes->plazo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($lotes->visitas->Visible) { // visitas ?>
	<div id="r_visitas" class="form-group">
		<label for="x_visitas" class="col-sm-2 control-label">
<input type="checkbox" name="u_visitas" id="u_visitas" value="1"<?php echo ($lotes->visitas->MultiUpdate == "1") ? " checked" : "" ?>>
 <?php echo $lotes->visitas->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $lotes->visitas->CellAttributes() ?>>
<span id="el_lotes_visitas">
<input type="text" data-table="lotes" data-field="x_visitas" name="x_visitas" id="x_visitas" size="30" placeholder="<?php echo ew_HtmlEncode($lotes->visitas->getPlaceHolder()) ?>" value="<?php echo $lotes->visitas->EditValue ?>"<?php echo $lotes->visitas->EditAttributes() ?>>
</span>
<?php echo $lotes->visitas->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if (!$lotes_update->IsModal) { ?>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("UpdateBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $lotes_update->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
		</div>
	</div>
<?php } ?>
</div>
</form>
<script type="text/javascript">
flotesupdate.Init();
</script>
<?php
$lotes_update->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$lotes_update->Page_Terminate();
?>
