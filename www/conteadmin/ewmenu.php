<!-- Begin Main Menu -->
<?php $RootMenu = new cMenu(EW_MENUBAR_ID) ?>
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(33, "mi_notiliniers", $Language->MenuPhrase("33", "MenuText"), "notilinierslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}notiliniers'), FALSE, FALSE);
$RootMenu->AddMenuItem(31, "mi_home_pautas", $Language->MenuPhrase("31", "MenuText"), "home_pautaslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}home_pautas'), FALSE, FALSE);
$RootMenu->AddMenuItem(27, "mi_videos_lotes", $Language->MenuPhrase("27", "MenuText"), "videos_loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}videos_lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(32, "mi_conjuntos_lotes", $Language->MenuPhrase("32", "MenuText"), "conjuntos_loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}conjuntos_lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(4, "mi_lotes", $Language->MenuPhrase("4", "MenuText"), "loteslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}lotes'), FALSE, FALSE);
$RootMenu->AddMenuItem(6, "mi_remates", $Language->MenuPhrase("6", "MenuText"), "remateslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}remates'), FALSE, FALSE);
$RootMenu->AddMenuItem(12, "mi_tipos", $Language->MenuPhrase("12", "MenuText"), "tiposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}tipos'), FALSE, FALSE);
$RootMenu->AddMenuItem(10, "mi_categorias_ganado", $Language->MenuPhrase("10", "MenuText"), "categorias_ganadolist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}categorias_ganado'), FALSE, FALSE);
$RootMenu->AddMenuItem(13, "mi_usuarios", $Language->MenuPhrase("13", "MenuText"), "usuarioslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}usuarios'), FALSE, FALSE);
$RootMenu->AddMenuItem(16, "mi_userlevelpermissions", $Language->MenuPhrase("16", "MenuText"), "userlevelpermissionslist.php", -1, "", (@$_SESSION[EW_SESSION_USER_LEVEL] & EW_ALLOW_ADMIN) == EW_ALLOW_ADMIN, FALSE, FALSE);
$RootMenu->AddMenuItem(17, "mi_userlevels", $Language->MenuPhrase("17", "MenuText"), "userlevelslist.php", -1, "", (@$_SESSION[EW_SESSION_USER_LEVEL] & EW_ALLOW_ADMIN) == EW_ALLOW_ADMIN, FALSE, FALSE);
$RootMenu->AddMenuItem(34, "mi_galerias_imagenes", $Language->MenuPhrase("34", "MenuText"), "galerias_imageneslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}galerias_imagenes'), FALSE, FALSE);
$RootMenu->AddMenuItem(9, "mi_fotos", $Language->MenuPhrase("9", "MenuText"), "fotoslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}fotos'), FALSE, FALSE);
$RootMenu->AddMenuItem(5, "mi_provincias", $Language->MenuPhrase("5", "MenuText"), "provinciaslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}provincias'), FALSE, FALSE);
$RootMenu->AddMenuItem(3, "mi_localidades", $Language->MenuPhrase("3", "MenuText"), "localidadeslist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}localidades'), FALSE, FALSE);
$RootMenu->AddMenuItem(26, "mci_Exportar_Planilla", $Language->MenuPhrase("26", "MenuText"), "remaplanilla-prev.php", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(28, "mi_staff", $Language->MenuPhrase("28", "MenuText"), "stafflist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff'), FALSE, FALSE);
$RootMenu->AddMenuItem(29, "mi_staff_grupos", $Language->MenuPhrase("29", "MenuText"), "staff_gruposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff_grupos'), FALSE, FALSE);
$RootMenu->AddMenuItem(30, "mi_staff_subgrupos", $Language->MenuPhrase("30", "MenuText"), "staff_subgruposlist.php", -1, "", AllowListMenu('{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}staff_subgrupos'), FALSE, FALSE);
$RootMenu->AddMenuItem(-1, "mi_logout", $Language->Phrase("Logout"), "logout.php", -1, "", IsLoggedIn());
$RootMenu->AddMenuItem(-1, "mi_login", $Language->Phrase("Login"), "login.php", -1, "", !IsLoggedIn() && substr(@$_SERVER["URL"], -1 * strlen("login.php")) <> "login.php");
$RootMenu->Render();
?>
<!-- End Main Menu -->
