<?php
if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg13.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql13.php") ?>
<?php include_once "phpfn13.php" ?>
<?php include_once "notiliniersinfo.php" ?>
<?php include_once "usuariosinfo.php" ?>
<?php include_once "userfn13.php" ?>
<?php

//
// Page class
//

$notiliniers_edit = NULL; // Initialize page object first

class cnotiliniers_edit extends cnotiliniers {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{13A35B93-CBD2-4088-A8F2-0C18D3EEFFDB}";

	// Table name
	var $TableName = 'notiliniers';

	// Page object name
	var $PageObjName = 'notiliniers_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		if (method_exists($this, "Message_Showing"))
			$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (notiliniers)
		if (!isset($GLOBALS["notiliniers"]) || get_class($GLOBALS["notiliniers"]) == "cnotiliniers") {
			$GLOBALS["notiliniers"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["notiliniers"];
		}

		// Table object (usuarios)
		if (!isset($GLOBALS['usuarios'])) $GLOBALS['usuarios'] = new cusuarios();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'notiliniers', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (usuarios)
		if (!isset($UserTable)) {
			$UserTable = new cusuarios();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	//
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage(ew_DeniedMsg()); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("notilinierslist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->id->SetVisibility();
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->fecha->SetVisibility();
		$this->activa->SetVisibility();
		$this->titulo->SetVisibility();
		$this->bajada->SetVisibility();
		$this->cuerpo->SetVisibility();
		$this->foto_1->SetVisibility();
		$this->epigrafe_1->SetVisibility();
		$this->foto_2->SetVisibility();
		$this->epigrafe_2->SetVisibility();
		$this->foto_3->SetVisibility();
		$this->epigrafe_3->SetVisibility();
		$this->foto_4->SetVisibility();
		$this->epigrafe_4->SetVisibility();
		$this->galeria_id->SetVisibility();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $notiliniers;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($notiliniers);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();

			// Handle modal response
			if ($this->IsModal) {
				$row = array();
				$row["url"] = $url;
				echo ew_ArrayToJson(array($row));
			} else {
				header("Location: " . $url);
			}
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $IsModal = FALSE;
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;
		global $gbSkipHeaderFooter;

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;

		// Load key from QueryString
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		}

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "") {
			$this->Page_Terminate("notilinierslist.php"); // Invalid key, return to list
		}

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("notilinierslist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				if (ew_GetPageName($sReturnUrl) == "notilinierslist.php")
					$sReturnUrl = $this->AddMasterUrl($sReturnUrl); // List page, return to list page with correct master key if necessary
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->foto_1->Upload->Index = $objForm->Index;
		$this->foto_1->Upload->UploadFile();
		$this->foto_1->CurrentValue = $this->foto_1->Upload->FileName;
		$this->foto_2->Upload->Index = $objForm->Index;
		$this->foto_2->Upload->UploadFile();
		$this->foto_2->CurrentValue = $this->foto_2->Upload->FileName;
		$this->foto_3->Upload->Index = $objForm->Index;
		$this->foto_3->Upload->UploadFile();
		$this->foto_3->CurrentValue = $this->foto_3->Upload->FileName;
		$this->foto_4->Upload->Index = $objForm->Index;
		$this->foto_4->Upload->UploadFile();
		$this->foto_4->CurrentValue = $this->foto_4->Upload->FileName;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->fecha->FldIsDetailKey) {
			$this->fecha->setFormValue($objForm->GetValue("x_fecha"));
			$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		}
		if (!$this->activa->FldIsDetailKey) {
			$this->activa->setFormValue($objForm->GetValue("x_activa"));
		}
		if (!$this->titulo->FldIsDetailKey) {
			$this->titulo->setFormValue($objForm->GetValue("x_titulo"));
		}
		if (!$this->bajada->FldIsDetailKey) {
			$this->bajada->setFormValue($objForm->GetValue("x_bajada"));
		}
		if (!$this->cuerpo->FldIsDetailKey) {
			$this->cuerpo->setFormValue($objForm->GetValue("x_cuerpo"));
		}
		if (!$this->epigrafe_1->FldIsDetailKey) {
			$this->epigrafe_1->setFormValue($objForm->GetValue("x_epigrafe_1"));
		}
		if (!$this->epigrafe_2->FldIsDetailKey) {
			$this->epigrafe_2->setFormValue($objForm->GetValue("x_epigrafe_2"));
		}
		if (!$this->epigrafe_3->FldIsDetailKey) {
			$this->epigrafe_3->setFormValue($objForm->GetValue("x_epigrafe_3"));
		}
		if (!$this->epigrafe_4->FldIsDetailKey) {
			$this->epigrafe_4->setFormValue($objForm->GetValue("x_epigrafe_4"));
		}
		if (!$this->galeria_id->FldIsDetailKey) {
			$this->galeria_id->setFormValue($objForm->GetValue("x_galeria_id"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->fecha->CurrentValue = $this->fecha->FormValue;
		$this->fecha->CurrentValue = ew_UnFormatDateTime($this->fecha->CurrentValue, 0);
		$this->activa->CurrentValue = $this->activa->FormValue;
		$this->titulo->CurrentValue = $this->titulo->FormValue;
		$this->bajada->CurrentValue = $this->bajada->FormValue;
		$this->cuerpo->CurrentValue = $this->cuerpo->FormValue;
		$this->epigrafe_1->CurrentValue = $this->epigrafe_1->FormValue;
		$this->epigrafe_2->CurrentValue = $this->epigrafe_2->FormValue;
		$this->epigrafe_3->CurrentValue = $this->epigrafe_3->FormValue;
		$this->epigrafe_4->CurrentValue = $this->epigrafe_4->FormValue;
		$this->galeria_id->CurrentValue = $this->galeria_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->fecha->setDbValue($rs->fields('fecha'));
		$this->activa->setDbValue($rs->fields('activa'));
		$this->titulo->setDbValue($rs->fields('titulo'));
		$this->bajada->setDbValue($rs->fields('bajada'));
		$this->cuerpo->setDbValue($rs->fields('cuerpo'));
		$this->foto_1->Upload->DbValue = $rs->fields('foto_1');
		$this->foto_1->CurrentValue = $this->foto_1->Upload->DbValue;
		$this->epigrafe_1->setDbValue($rs->fields('epigrafe_1'));
		$this->foto_2->Upload->DbValue = $rs->fields('foto_2');
		$this->foto_2->CurrentValue = $this->foto_2->Upload->DbValue;
		$this->epigrafe_2->setDbValue($rs->fields('epigrafe_2'));
		$this->foto_3->Upload->DbValue = $rs->fields('foto_3');
		$this->foto_3->CurrentValue = $this->foto_3->Upload->DbValue;
		$this->epigrafe_3->setDbValue($rs->fields('epigrafe_3'));
		$this->foto_4->Upload->DbValue = $rs->fields('foto_4');
		$this->foto_4->CurrentValue = $this->foto_4->Upload->DbValue;
		$this->epigrafe_4->setDbValue($rs->fields('epigrafe_4'));
		$this->galeria_id->setDbValue($rs->fields('galeria_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->id->DbValue = $row['id'];
		$this->fecha->DbValue = $row['fecha'];
		$this->activa->DbValue = $row['activa'];
		$this->titulo->DbValue = $row['titulo'];
		$this->bajada->DbValue = $row['bajada'];
		$this->cuerpo->DbValue = $row['cuerpo'];
		$this->foto_1->Upload->DbValue = $row['foto_1'];
		$this->epigrafe_1->DbValue = $row['epigrafe_1'];
		$this->foto_2->Upload->DbValue = $row['foto_2'];
		$this->epigrafe_2->DbValue = $row['epigrafe_2'];
		$this->foto_3->Upload->DbValue = $row['foto_3'];
		$this->epigrafe_3->DbValue = $row['epigrafe_3'];
		$this->foto_4->Upload->DbValue = $row['foto_4'];
		$this->epigrafe_4->DbValue = $row['epigrafe_4'];
		$this->galeria_id->DbValue = $row['galeria_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// fecha
		// activa
		// titulo
		// bajada
		// cuerpo
		// foto_1
		// epigrafe_1
		// foto_2
		// epigrafe_2
		// foto_3
		// epigrafe_3
		// foto_4
		// epigrafe_4
		// galeria_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// id
		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// fecha
		$this->fecha->ViewValue = $this->fecha->CurrentValue;
		$this->fecha->ViewValue = ew_FormatDateTime($this->fecha->ViewValue, 0);
		$this->fecha->ViewCustomAttributes = "";

		// activa
		if (strval($this->activa->CurrentValue) <> "") {
			$this->activa->ViewValue = $this->activa->OptionCaption($this->activa->CurrentValue);
		} else {
			$this->activa->ViewValue = NULL;
		}
		$this->activa->ViewCustomAttributes = "";

		// titulo
		$this->titulo->ViewValue = $this->titulo->CurrentValue;
		$this->titulo->ViewCustomAttributes = "";

		// bajada
		$this->bajada->ViewValue = $this->bajada->CurrentValue;
		$this->bajada->ViewCustomAttributes = "";

		// cuerpo
		$this->cuerpo->ViewValue = $this->cuerpo->CurrentValue;
		$this->cuerpo->ViewCustomAttributes = "";

		// foto_1
		if (!ew_Empty($this->foto_1->Upload->DbValue)) {
			$this->foto_1->ViewValue = $this->foto_1->Upload->DbValue;
		} else {
			$this->foto_1->ViewValue = "";
		}
		$this->foto_1->ViewCustomAttributes = "";

		// epigrafe_1
		$this->epigrafe_1->ViewValue = $this->epigrafe_1->CurrentValue;
		$this->epigrafe_1->ViewCustomAttributes = "";

		// foto_2
		if (!ew_Empty($this->foto_2->Upload->DbValue)) {
			$this->foto_2->ViewValue = $this->foto_2->Upload->DbValue;
		} else {
			$this->foto_2->ViewValue = "";
		}
		$this->foto_2->ViewCustomAttributes = "";

		// epigrafe_2
		$this->epigrafe_2->ViewValue = $this->epigrafe_2->CurrentValue;
		$this->epigrafe_2->ViewCustomAttributes = "";

		// foto_3
		if (!ew_Empty($this->foto_3->Upload->DbValue)) {
			$this->foto_3->ViewValue = $this->foto_3->Upload->DbValue;
		} else {
			$this->foto_3->ViewValue = "";
		}
		$this->foto_3->ViewCustomAttributes = "";

		// epigrafe_3
		$this->epigrafe_3->ViewValue = $this->epigrafe_3->CurrentValue;
		$this->epigrafe_3->ViewCustomAttributes = "";

		// foto_4
		if (!ew_Empty($this->foto_4->Upload->DbValue)) {
			$this->foto_4->ViewValue = $this->foto_4->Upload->DbValue;
		} else {
			$this->foto_4->ViewValue = "";
		}
		$this->foto_4->ViewCustomAttributes = "";

		// epigrafe_4
		$this->epigrafe_4->ViewValue = $this->epigrafe_4->CurrentValue;
		$this->epigrafe_4->ViewCustomAttributes = "";

		// galeria_id
		if (strval($this->galeria_id->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->galeria_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `galerias_imagenes`";
		$sWhereWrk = "";
		$this->galeria_id->LookupFilters = array();
		$lookuptblfilter = "`contenido_tipo`='LINIERS'";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->galeria_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `titulo` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->galeria_id->ViewValue = $this->galeria_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->galeria_id->ViewValue = $this->galeria_id->CurrentValue;
			}
		} else {
			$this->galeria_id->ViewValue = NULL;
		}
		$this->galeria_id->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";
			$this->fecha->TooltipValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";
			$this->activa->TooltipValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";
			$this->titulo->TooltipValue = "";

			// bajada
			$this->bajada->LinkCustomAttributes = "";
			$this->bajada->HrefValue = "";
			$this->bajada->TooltipValue = "";

			// cuerpo
			$this->cuerpo->LinkCustomAttributes = "";
			$this->cuerpo->HrefValue = "";
			$this->cuerpo->TooltipValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;
			$this->foto_1->TooltipValue = "";

			// epigrafe_1
			$this->epigrafe_1->LinkCustomAttributes = "";
			$this->epigrafe_1->HrefValue = "";
			$this->epigrafe_1->TooltipValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;
			$this->foto_2->TooltipValue = "";

			// epigrafe_2
			$this->epigrafe_2->LinkCustomAttributes = "";
			$this->epigrafe_2->HrefValue = "";
			$this->epigrafe_2->TooltipValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;
			$this->foto_3->TooltipValue = "";

			// epigrafe_3
			$this->epigrafe_3->LinkCustomAttributes = "";
			$this->epigrafe_3->HrefValue = "";
			$this->epigrafe_3->TooltipValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;
			$this->foto_4->TooltipValue = "";

			// epigrafe_4
			$this->epigrafe_4->LinkCustomAttributes = "";
			$this->epigrafe_4->HrefValue = "";
			$this->epigrafe_4->TooltipValue = "";

			// galeria_id
			$this->galeria_id->LinkCustomAttributes = "";
			$this->galeria_id->HrefValue = "";
			$this->galeria_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditAttrs["class"] = "form-control";
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// fecha
			$this->fecha->EditAttrs["class"] = "form-control";
			$this->fecha->EditCustomAttributes = "";
			$this->fecha->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->fecha->CurrentValue, 8));
			$this->fecha->PlaceHolder = ew_RemoveHtml($this->fecha->FldCaption());

			// activa
			$this->activa->EditAttrs["class"] = "form-control";
			$this->activa->EditCustomAttributes = "";
			$this->activa->EditValue = $this->activa->Options(TRUE);

			// titulo
			$this->titulo->EditAttrs["class"] = "form-control";
			$this->titulo->EditCustomAttributes = "";
			$this->titulo->EditValue = ew_HtmlEncode($this->titulo->CurrentValue);
			$this->titulo->PlaceHolder = ew_RemoveHtml($this->titulo->FldCaption());

			// bajada
			$this->bajada->EditAttrs["class"] = "form-control";
			$this->bajada->EditCustomAttributes = "";
			$this->bajada->EditValue = ew_HtmlEncode($this->bajada->CurrentValue);
			$this->bajada->PlaceHolder = ew_RemoveHtml($this->bajada->FldCaption());

			// cuerpo
			$this->cuerpo->EditAttrs["class"] = "form-control";
			$this->cuerpo->EditCustomAttributes = "";
			$this->cuerpo->EditValue = ew_HtmlEncode($this->cuerpo->CurrentValue);
			$this->cuerpo->PlaceHolder = ew_RemoveHtml($this->cuerpo->FldCaption());

			// foto_1
			$this->foto_1->EditAttrs["class"] = "form-control";
			$this->foto_1->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_1->Upload->DbValue)) {
				$this->foto_1->EditValue = $this->foto_1->Upload->DbValue;
			} else {
				$this->foto_1->EditValue = "";
			}
			if (!ew_Empty($this->foto_1->CurrentValue))
				$this->foto_1->Upload->FileName = $this->foto_1->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->foto_1);

			// epigrafe_1
			$this->epigrafe_1->EditAttrs["class"] = "form-control";
			$this->epigrafe_1->EditCustomAttributes = "";
			$this->epigrafe_1->EditValue = ew_HtmlEncode($this->epigrafe_1->CurrentValue);
			$this->epigrafe_1->PlaceHolder = ew_RemoveHtml($this->epigrafe_1->FldCaption());

			// foto_2
			$this->foto_2->EditAttrs["class"] = "form-control";
			$this->foto_2->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_2->Upload->DbValue)) {
				$this->foto_2->EditValue = $this->foto_2->Upload->DbValue;
			} else {
				$this->foto_2->EditValue = "";
			}
			if (!ew_Empty($this->foto_2->CurrentValue))
				$this->foto_2->Upload->FileName = $this->foto_2->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->foto_2);

			// epigrafe_2
			$this->epigrafe_2->EditAttrs["class"] = "form-control";
			$this->epigrafe_2->EditCustomAttributes = "";
			$this->epigrafe_2->EditValue = ew_HtmlEncode($this->epigrafe_2->CurrentValue);
			$this->epigrafe_2->PlaceHolder = ew_RemoveHtml($this->epigrafe_2->FldCaption());

			// foto_3
			$this->foto_3->EditAttrs["class"] = "form-control";
			$this->foto_3->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_3->Upload->DbValue)) {
				$this->foto_3->EditValue = $this->foto_3->Upload->DbValue;
			} else {
				$this->foto_3->EditValue = "";
			}
			if (!ew_Empty($this->foto_3->CurrentValue))
				$this->foto_3->Upload->FileName = $this->foto_3->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->foto_3);

			// epigrafe_3
			$this->epigrafe_3->EditAttrs["class"] = "form-control";
			$this->epigrafe_3->EditCustomAttributes = "";
			$this->epigrafe_3->EditValue = ew_HtmlEncode($this->epigrafe_3->CurrentValue);
			$this->epigrafe_3->PlaceHolder = ew_RemoveHtml($this->epigrafe_3->FldCaption());

			// foto_4
			$this->foto_4->EditAttrs["class"] = "form-control";
			$this->foto_4->EditCustomAttributes = "";
			if (!ew_Empty($this->foto_4->Upload->DbValue)) {
				$this->foto_4->EditValue = $this->foto_4->Upload->DbValue;
			} else {
				$this->foto_4->EditValue = "";
			}
			if (!ew_Empty($this->foto_4->CurrentValue))
				$this->foto_4->Upload->FileName = $this->foto_4->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->foto_4);

			// epigrafe_4
			$this->epigrafe_4->EditAttrs["class"] = "form-control";
			$this->epigrafe_4->EditCustomAttributes = "";
			$this->epigrafe_4->EditValue = ew_HtmlEncode($this->epigrafe_4->CurrentValue);
			$this->epigrafe_4->PlaceHolder = ew_RemoveHtml($this->epigrafe_4->FldCaption());

			// galeria_id
			$this->galeria_id->EditAttrs["class"] = "form-control";
			$this->galeria_id->EditCustomAttributes = "";
			if (trim(strval($this->galeria_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->galeria_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			$sSqlWrk = "SELECT `id`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `galerias_imagenes`";
			$sWhereWrk = "";
			$this->galeria_id->LookupFilters = array();
			$lookuptblfilter = "`contenido_tipo`='LINIERS'";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->galeria_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->galeria_id->EditValue = $arwrk;

			// Edit refer script
			// id

			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";

			// fecha
			$this->fecha->LinkCustomAttributes = "";
			$this->fecha->HrefValue = "";

			// activa
			$this->activa->LinkCustomAttributes = "";
			$this->activa->HrefValue = "";

			// titulo
			$this->titulo->LinkCustomAttributes = "";
			$this->titulo->HrefValue = "";

			// bajada
			$this->bajada->LinkCustomAttributes = "";
			$this->bajada->HrefValue = "";

			// cuerpo
			$this->cuerpo->LinkCustomAttributes = "";
			$this->cuerpo->HrefValue = "";

			// foto_1
			$this->foto_1->LinkCustomAttributes = "";
			$this->foto_1->HrefValue = "";
			$this->foto_1->HrefValue2 = $this->foto_1->UploadPath . $this->foto_1->Upload->DbValue;

			// epigrafe_1
			$this->epigrafe_1->LinkCustomAttributes = "";
			$this->epigrafe_1->HrefValue = "";

			// foto_2
			$this->foto_2->LinkCustomAttributes = "";
			$this->foto_2->HrefValue = "";
			$this->foto_2->HrefValue2 = $this->foto_2->UploadPath . $this->foto_2->Upload->DbValue;

			// epigrafe_2
			$this->epigrafe_2->LinkCustomAttributes = "";
			$this->epigrafe_2->HrefValue = "";

			// foto_3
			$this->foto_3->LinkCustomAttributes = "";
			$this->foto_3->HrefValue = "";
			$this->foto_3->HrefValue2 = $this->foto_3->UploadPath . $this->foto_3->Upload->DbValue;

			// epigrafe_3
			$this->epigrafe_3->LinkCustomAttributes = "";
			$this->epigrafe_3->HrefValue = "";

			// foto_4
			$this->foto_4->LinkCustomAttributes = "";
			$this->foto_4->HrefValue = "";
			$this->foto_4->HrefValue2 = $this->foto_4->UploadPath . $this->foto_4->Upload->DbValue;

			// epigrafe_4
			$this->epigrafe_4->LinkCustomAttributes = "";
			$this->epigrafe_4->HrefValue = "";

			// galeria_id
			$this->galeria_id->LinkCustomAttributes = "";
			$this->galeria_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->fecha->FldIsDetailKey && !is_null($this->fecha->FormValue) && $this->fecha->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->fecha->FldCaption(), $this->fecha->ReqErrMsg));
		}
		if (!ew_CheckDateDef($this->fecha->FormValue)) {
			ew_AddMessage($gsFormError, $this->fecha->FldErrMsg());
		}
		if (!$this->activa->FldIsDetailKey && !is_null($this->activa->FormValue) && $this->activa->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->activa->FldCaption(), $this->activa->ReqErrMsg));
		}
		if (!$this->titulo->FldIsDetailKey && !is_null($this->titulo->FormValue) && $this->titulo->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->titulo->FldCaption(), $this->titulo->ReqErrMsg));
		}
		if (!$this->bajada->FldIsDetailKey && !is_null($this->bajada->FormValue) && $this->bajada->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->bajada->FldCaption(), $this->bajada->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// fecha
			$this->fecha->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->fecha->CurrentValue, 0), ew_CurrentDate(), $this->fecha->ReadOnly);

			// activa
			$this->activa->SetDbValueDef($rsnew, $this->activa->CurrentValue, "", $this->activa->ReadOnly);

			// titulo
			$this->titulo->SetDbValueDef($rsnew, $this->titulo->CurrentValue, "", $this->titulo->ReadOnly);

			// bajada
			$this->bajada->SetDbValueDef($rsnew, $this->bajada->CurrentValue, "", $this->bajada->ReadOnly);

			// cuerpo
			$this->cuerpo->SetDbValueDef($rsnew, $this->cuerpo->CurrentValue, NULL, $this->cuerpo->ReadOnly);

			// foto_1
			if ($this->foto_1->Visible && !$this->foto_1->ReadOnly && !$this->foto_1->Upload->KeepFile) {
				$this->foto_1->Upload->DbValue = $rsold['foto_1']; // Get original value
				if ($this->foto_1->Upload->FileName == "") {
					$rsnew['foto_1'] = NULL;
				} else {
					$rsnew['foto_1'] = $this->foto_1->Upload->FileName;
				}
			}

			// epigrafe_1
			$this->epigrafe_1->SetDbValueDef($rsnew, $this->epigrafe_1->CurrentValue, NULL, $this->epigrafe_1->ReadOnly);

			// foto_2
			if ($this->foto_2->Visible && !$this->foto_2->ReadOnly && !$this->foto_2->Upload->KeepFile) {
				$this->foto_2->Upload->DbValue = $rsold['foto_2']; // Get original value
				if ($this->foto_2->Upload->FileName == "") {
					$rsnew['foto_2'] = NULL;
				} else {
					$rsnew['foto_2'] = $this->foto_2->Upload->FileName;
				}
			}

			// epigrafe_2
			$this->epigrafe_2->SetDbValueDef($rsnew, $this->epigrafe_2->CurrentValue, NULL, $this->epigrafe_2->ReadOnly);

			// foto_3
			if ($this->foto_3->Visible && !$this->foto_3->ReadOnly && !$this->foto_3->Upload->KeepFile) {
				$this->foto_3->Upload->DbValue = $rsold['foto_3']; // Get original value
				if ($this->foto_3->Upload->FileName == "") {
					$rsnew['foto_3'] = NULL;
				} else {
					$rsnew['foto_3'] = $this->foto_3->Upload->FileName;
				}
			}

			// epigrafe_3
			$this->epigrafe_3->SetDbValueDef($rsnew, $this->epigrafe_3->CurrentValue, NULL, $this->epigrafe_3->ReadOnly);

			// foto_4
			if ($this->foto_4->Visible && !$this->foto_4->ReadOnly && !$this->foto_4->Upload->KeepFile) {
				$this->foto_4->Upload->DbValue = $rsold['foto_4']; // Get original value
				if ($this->foto_4->Upload->FileName == "") {
					$rsnew['foto_4'] = NULL;
				} else {
					$rsnew['foto_4'] = $this->foto_4->Upload->FileName;
				}
			}

			// epigrafe_4
			$this->epigrafe_4->SetDbValueDef($rsnew, $this->epigrafe_4->CurrentValue, NULL, $this->epigrafe_4->ReadOnly);

			// galeria_id
			$this->galeria_id->SetDbValueDef($rsnew, $this->galeria_id->CurrentValue, NULL, $this->galeria_id->ReadOnly);
			if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
				if (!ew_Empty($this->foto_1->Upload->Value)) {
					$rsnew['foto_1'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_1->UploadPath), $rsnew['foto_1']); // Get new file name
				}
			}
			if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
				if (!ew_Empty($this->foto_2->Upload->Value)) {
					$rsnew['foto_2'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_2->UploadPath), $rsnew['foto_2']); // Get new file name
				}
			}
			if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
				if (!ew_Empty($this->foto_3->Upload->Value)) {
					$rsnew['foto_3'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_3->UploadPath), $rsnew['foto_3']); // Get new file name
				}
			}
			if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
				if (!ew_Empty($this->foto_4->Upload->Value)) {
					$rsnew['foto_4'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->foto_4->UploadPath), $rsnew['foto_4']); // Get new file name
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->foto_1->Visible && !$this->foto_1->Upload->KeepFile) {
						if (!ew_Empty($this->foto_1->Upload->Value)) {
							if (!$this->foto_1->Upload->SaveToFile($this->foto_1->UploadPath, $rsnew['foto_1'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_2->Visible && !$this->foto_2->Upload->KeepFile) {
						if (!ew_Empty($this->foto_2->Upload->Value)) {
							if (!$this->foto_2->Upload->SaveToFile($this->foto_2->UploadPath, $rsnew['foto_2'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_3->Visible && !$this->foto_3->Upload->KeepFile) {
						if (!ew_Empty($this->foto_3->Upload->Value)) {
							if (!$this->foto_3->Upload->SaveToFile($this->foto_3->UploadPath, $rsnew['foto_3'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
					if ($this->foto_4->Visible && !$this->foto_4->Upload->KeepFile) {
						if (!ew_Empty($this->foto_4->Upload->Value)) {
							if (!$this->foto_4->Upload->SaveToFile($this->foto_4->UploadPath, $rsnew['foto_4'], TRUE)) {
								$this->setFailureMessage($Language->Phrase("UploadErrMsg7"));
								return FALSE;
							}
						}
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// foto_1
		ew_CleanUploadTempPath($this->foto_1, $this->foto_1->Upload->Index);

		// foto_2
		ew_CleanUploadTempPath($this->foto_2, $this->foto_2->Upload->Index);

		// foto_3
		ew_CleanUploadTempPath($this->foto_3, $this->foto_3->Upload->Index);

		// foto_4
		ew_CleanUploadTempPath($this->foto_4, $this->foto_4->Upload->Index);
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, $this->AddMasterUrl("notilinierslist.php"), "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Setup lookup filters of a field
	function SetupLookupFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		case "x_galeria_id":
			$sSqlWrk = "";
			$sSqlWrk = "SELECT `id` AS `LinkFld`, `titulo` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `galerias_imagenes`";
			$sWhereWrk = "";
			$this->galeria_id->LookupFilters = array();
			$lookuptblfilter = "`contenido_tipo`='LINIERS'";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			$fld->LookupFilters += array("s" => $sSqlWrk, "d" => "", "f0" => '`id` = {filter_value}', "t0" => "19", "fn0" => "");
			$sSqlWrk = "";
			$this->Lookup_Selecting($this->galeria_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `titulo` ASC";
			if ($sSqlWrk <> "")
				$fld->LookupFilters["s"] .= $sSqlWrk;
			break;
		}
	}

	// Setup AutoSuggest filters of a field
	function SetupAutoSuggestFilters($fld, $pageId = null) {
		global $gsLanguage;
		$pageId = $pageId ?: $this->PageID;
		switch ($fld->FldVar) {
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($notiliniers_edit)) $notiliniers_edit = new cnotiliniers_edit();

// Page init
$notiliniers_edit->Page_Init();

// Page main
$notiliniers_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$notiliniers_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = fnotiliniersedit = new ew_Form("fnotiliniersedit", "edit");

// Validate form
fnotiliniersedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $notiliniers->fecha->FldCaption(), $notiliniers->fecha->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_fecha");
			if (elm && !ew_CheckDateDef(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($notiliniers->fecha->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_activa");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $notiliniers->activa->FldCaption(), $notiliniers->activa->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_titulo");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $notiliniers->titulo->FldCaption(), $notiliniers->titulo->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_bajada");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $notiliniers->bajada->FldCaption(), $notiliniers->bajada->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fnotiliniersedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fnotiliniersedit.ValidateRequired = true;
<?php } else { ?>
fnotiliniersedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fnotiliniersedit.Lists["x_activa"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
fnotiliniersedit.Lists["x_activa"].Options = <?php echo json_encode($notiliniers->activa->Options()) ?>;
fnotiliniersedit.Lists["x_galeria_id"] = {"LinkField":"x_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_titulo","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":"","LinkTable":"galerias_imagenes"};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$notiliniers_edit->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $notiliniers_edit->ShowPageHeader(); ?>
<?php
$notiliniers_edit->ShowMessage();
?>
<form name="fnotiliniersedit" id="fnotiliniersedit" class="<?php echo $notiliniers_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($notiliniers_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $notiliniers_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="notiliniers">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<?php if ($notiliniers_edit->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($notiliniers->id->Visible) { // id ?>
	<div id="r_id" class="form-group">
		<label id="elh_notiliniers_id" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->id->CellAttributes() ?>>
<span id="el_notiliniers_id">
<span<?php echo $notiliniers->id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $notiliniers->id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="notiliniers" data-field="x_id" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($notiliniers->id->CurrentValue) ?>">
<?php echo $notiliniers->id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->fecha->Visible) { // fecha ?>
	<div id="r_fecha" class="form-group">
		<label id="elh_notiliniers_fecha" for="x_fecha" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->fecha->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->fecha->CellAttributes() ?>>
<span id="el_notiliniers_fecha">
<input type="text" data-table="notiliniers" data-field="x_fecha" name="x_fecha" id="x_fecha" placeholder="<?php echo ew_HtmlEncode($notiliniers->fecha->getPlaceHolder()) ?>" value="<?php echo $notiliniers->fecha->EditValue ?>"<?php echo $notiliniers->fecha->EditAttributes() ?>>
<?php if (!$notiliniers->fecha->ReadOnly && !$notiliniers->fecha->Disabled && !isset($notiliniers->fecha->EditAttrs["readonly"]) && !isset($notiliniers->fecha->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("fnotiliniersedit", "x_fecha", 0);
</script>
<?php } ?>
</span>
<?php echo $notiliniers->fecha->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->activa->Visible) { // activa ?>
	<div id="r_activa" class="form-group">
		<label id="elh_notiliniers_activa" for="x_activa" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->activa->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->activa->CellAttributes() ?>>
<span id="el_notiliniers_activa">
<select data-table="notiliniers" data-field="x_activa" data-value-separator="<?php echo $notiliniers->activa->DisplayValueSeparatorAttribute() ?>" id="x_activa" name="x_activa"<?php echo $notiliniers->activa->EditAttributes() ?>>
<?php echo $notiliniers->activa->SelectOptionListHtml("x_activa") ?>
</select>
</span>
<?php echo $notiliniers->activa->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->titulo->Visible) { // titulo ?>
	<div id="r_titulo" class="form-group">
		<label id="elh_notiliniers_titulo" for="x_titulo" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->titulo->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->titulo->CellAttributes() ?>>
<span id="el_notiliniers_titulo">
<input type="text" data-table="notiliniers" data-field="x_titulo" name="x_titulo" id="x_titulo" size="60" maxlength="100" placeholder="<?php echo ew_HtmlEncode($notiliniers->titulo->getPlaceHolder()) ?>" value="<?php echo $notiliniers->titulo->EditValue ?>"<?php echo $notiliniers->titulo->EditAttributes() ?>>
</span>
<?php echo $notiliniers->titulo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->bajada->Visible) { // bajada ?>
	<div id="r_bajada" class="form-group">
		<label id="elh_notiliniers_bajada" for="x_bajada" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->bajada->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->bajada->CellAttributes() ?>>
<span id="el_notiliniers_bajada">
<input type="text" data-table="notiliniers" data-field="x_bajada" name="x_bajada" id="x_bajada" size="60" maxlength="255" placeholder="<?php echo ew_HtmlEncode($notiliniers->bajada->getPlaceHolder()) ?>" value="<?php echo $notiliniers->bajada->EditValue ?>"<?php echo $notiliniers->bajada->EditAttributes() ?>>
</span>
<?php echo $notiliniers->bajada->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->cuerpo->Visible) { // cuerpo ?>
	<div id="r_cuerpo" class="form-group">
		<label id="elh_notiliniers_cuerpo" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->cuerpo->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->cuerpo->CellAttributes() ?>>
<span id="el_notiliniers_cuerpo">
<?php ew_AppendClass($notiliniers->cuerpo->EditAttrs["class"], "editor"); ?>
<textarea data-table="notiliniers" data-field="x_cuerpo" name="x_cuerpo" id="x_cuerpo" cols="60" rows="6" placeholder="<?php echo ew_HtmlEncode($notiliniers->cuerpo->getPlaceHolder()) ?>"<?php echo $notiliniers->cuerpo->EditAttributes() ?>><?php echo $notiliniers->cuerpo->EditValue ?></textarea>
<script type="text/javascript">
ew_CreateEditor("fnotiliniersedit", "x_cuerpo", 60, 6, <?php echo ($notiliniers->cuerpo->ReadOnly || FALSE) ? "true" : "false" ?>);
</script>
</span>
<?php echo $notiliniers->cuerpo->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->foto_1->Visible) { // foto_1 ?>
	<div id="r_foto_1" class="form-group">
		<label id="elh_notiliniers_foto_1" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->foto_1->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->foto_1->CellAttributes() ?>>
<span id="el_notiliniers_foto_1">
<div id="fd_x_foto_1">
<span title="<?php echo $notiliniers->foto_1->FldTitle() ? $notiliniers->foto_1->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($notiliniers->foto_1->ReadOnly || $notiliniers->foto_1->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="notiliniers" data-field="x_foto_1" name="x_foto_1" id="x_foto_1"<?php echo $notiliniers->foto_1->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_1" id= "fn_x_foto_1" value="<?php echo $notiliniers->foto_1->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_1"] == "0") { ?>
<input type="hidden" name="fa_x_foto_1" id= "fa_x_foto_1" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_1" id= "fa_x_foto_1" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_1" id= "fs_x_foto_1" value="255">
<input type="hidden" name="fx_x_foto_1" id= "fx_x_foto_1" value="<?php echo $notiliniers->foto_1->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_1" id= "fm_x_foto_1" value="<?php echo $notiliniers->foto_1->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_1" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $notiliniers->foto_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->epigrafe_1->Visible) { // epigrafe_1 ?>
	<div id="r_epigrafe_1" class="form-group">
		<label id="elh_notiliniers_epigrafe_1" for="x_epigrafe_1" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->epigrafe_1->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->epigrafe_1->CellAttributes() ?>>
<span id="el_notiliniers_epigrafe_1">
<input type="text" data-table="notiliniers" data-field="x_epigrafe_1" name="x_epigrafe_1" id="x_epigrafe_1" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($notiliniers->epigrafe_1->getPlaceHolder()) ?>" value="<?php echo $notiliniers->epigrafe_1->EditValue ?>"<?php echo $notiliniers->epigrafe_1->EditAttributes() ?>>
</span>
<?php echo $notiliniers->epigrafe_1->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->foto_2->Visible) { // foto_2 ?>
	<div id="r_foto_2" class="form-group">
		<label id="elh_notiliniers_foto_2" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->foto_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->foto_2->CellAttributes() ?>>
<span id="el_notiliniers_foto_2">
<div id="fd_x_foto_2">
<span title="<?php echo $notiliniers->foto_2->FldTitle() ? $notiliniers->foto_2->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($notiliniers->foto_2->ReadOnly || $notiliniers->foto_2->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="notiliniers" data-field="x_foto_2" name="x_foto_2" id="x_foto_2"<?php echo $notiliniers->foto_2->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_2" id= "fn_x_foto_2" value="<?php echo $notiliniers->foto_2->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_2"] == "0") { ?>
<input type="hidden" name="fa_x_foto_2" id= "fa_x_foto_2" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_2" id= "fa_x_foto_2" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_2" id= "fs_x_foto_2" value="255">
<input type="hidden" name="fx_x_foto_2" id= "fx_x_foto_2" value="<?php echo $notiliniers->foto_2->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_2" id= "fm_x_foto_2" value="<?php echo $notiliniers->foto_2->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_2" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $notiliniers->foto_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->epigrafe_2->Visible) { // epigrafe_2 ?>
	<div id="r_epigrafe_2" class="form-group">
		<label id="elh_notiliniers_epigrafe_2" for="x_epigrafe_2" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->epigrafe_2->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->epigrafe_2->CellAttributes() ?>>
<span id="el_notiliniers_epigrafe_2">
<input type="text" data-table="notiliniers" data-field="x_epigrafe_2" name="x_epigrafe_2" id="x_epigrafe_2" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($notiliniers->epigrafe_2->getPlaceHolder()) ?>" value="<?php echo $notiliniers->epigrafe_2->EditValue ?>"<?php echo $notiliniers->epigrafe_2->EditAttributes() ?>>
</span>
<?php echo $notiliniers->epigrafe_2->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->foto_3->Visible) { // foto_3 ?>
	<div id="r_foto_3" class="form-group">
		<label id="elh_notiliniers_foto_3" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->foto_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->foto_3->CellAttributes() ?>>
<span id="el_notiliniers_foto_3">
<div id="fd_x_foto_3">
<span title="<?php echo $notiliniers->foto_3->FldTitle() ? $notiliniers->foto_3->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($notiliniers->foto_3->ReadOnly || $notiliniers->foto_3->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="notiliniers" data-field="x_foto_3" name="x_foto_3" id="x_foto_3"<?php echo $notiliniers->foto_3->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_3" id= "fn_x_foto_3" value="<?php echo $notiliniers->foto_3->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_3"] == "0") { ?>
<input type="hidden" name="fa_x_foto_3" id= "fa_x_foto_3" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_3" id= "fa_x_foto_3" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_3" id= "fs_x_foto_3" value="255">
<input type="hidden" name="fx_x_foto_3" id= "fx_x_foto_3" value="<?php echo $notiliniers->foto_3->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_3" id= "fm_x_foto_3" value="<?php echo $notiliniers->foto_3->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_3" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $notiliniers->foto_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->epigrafe_3->Visible) { // epigrafe_3 ?>
	<div id="r_epigrafe_3" class="form-group">
		<label id="elh_notiliniers_epigrafe_3" for="x_epigrafe_3" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->epigrafe_3->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->epigrafe_3->CellAttributes() ?>>
<span id="el_notiliniers_epigrafe_3">
<input type="text" data-table="notiliniers" data-field="x_epigrafe_3" name="x_epigrafe_3" id="x_epigrafe_3" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($notiliniers->epigrafe_3->getPlaceHolder()) ?>" value="<?php echo $notiliniers->epigrafe_3->EditValue ?>"<?php echo $notiliniers->epigrafe_3->EditAttributes() ?>>
</span>
<?php echo $notiliniers->epigrafe_3->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->foto_4->Visible) { // foto_4 ?>
	<div id="r_foto_4" class="form-group">
		<label id="elh_notiliniers_foto_4" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->foto_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->foto_4->CellAttributes() ?>>
<span id="el_notiliniers_foto_4">
<div id="fd_x_foto_4">
<span title="<?php echo $notiliniers->foto_4->FldTitle() ? $notiliniers->foto_4->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($notiliniers->foto_4->ReadOnly || $notiliniers->foto_4->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="notiliniers" data-field="x_foto_4" name="x_foto_4" id="x_foto_4"<?php echo $notiliniers->foto_4->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_foto_4" id= "fn_x_foto_4" value="<?php echo $notiliniers->foto_4->Upload->FileName ?>">
<?php if (@$_POST["fa_x_foto_4"] == "0") { ?>
<input type="hidden" name="fa_x_foto_4" id= "fa_x_foto_4" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_foto_4" id= "fa_x_foto_4" value="1">
<?php } ?>
<input type="hidden" name="fs_x_foto_4" id= "fs_x_foto_4" value="255">
<input type="hidden" name="fx_x_foto_4" id= "fx_x_foto_4" value="<?php echo $notiliniers->foto_4->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_foto_4" id= "fm_x_foto_4" value="<?php echo $notiliniers->foto_4->UploadMaxFileSize ?>">
</div>
<table id="ft_x_foto_4" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $notiliniers->foto_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->epigrafe_4->Visible) { // epigrafe_4 ?>
	<div id="r_epigrafe_4" class="form-group">
		<label id="elh_notiliniers_epigrafe_4" for="x_epigrafe_4" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->epigrafe_4->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->epigrafe_4->CellAttributes() ?>>
<span id="el_notiliniers_epigrafe_4">
<input type="text" data-table="notiliniers" data-field="x_epigrafe_4" name="x_epigrafe_4" id="x_epigrafe_4" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($notiliniers->epigrafe_4->getPlaceHolder()) ?>" value="<?php echo $notiliniers->epigrafe_4->EditValue ?>"<?php echo $notiliniers->epigrafe_4->EditAttributes() ?>>
</span>
<?php echo $notiliniers->epigrafe_4->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($notiliniers->galeria_id->Visible) { // galeria_id ?>
	<div id="r_galeria_id" class="form-group">
		<label id="elh_notiliniers_galeria_id" for="x_galeria_id" class="col-sm-2 control-label ewLabel"><?php echo $notiliniers->galeria_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $notiliniers->galeria_id->CellAttributes() ?>>
<span id="el_notiliniers_galeria_id">
<select data-table="notiliniers" data-field="x_galeria_id" data-value-separator="<?php echo $notiliniers->galeria_id->DisplayValueSeparatorAttribute() ?>" id="x_galeria_id" name="x_galeria_id"<?php echo $notiliniers->galeria_id->EditAttributes() ?>>
<?php echo $notiliniers->galeria_id->SelectOptionListHtml("x_galeria_id") ?>
</select>
<?php if (AllowAdd(CurrentProjectID() . "galerias_imagenes")) { ?>
<button type="button" title="<?php echo ew_HtmlTitle($Language->Phrase("AddLink")) . "&nbsp;" . $notiliniers->galeria_id->FldCaption() ?>" onclick="ew_AddOptDialogShow({lnk:this,el:'x_galeria_id',url:'galerias_imagenesaddopt.php'});" class="ewAddOptBtn btn btn-default btn-sm" id="aol_x_galeria_id"><span class="glyphicon glyphicon-plus ewIcon"></span><span class="hide"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $notiliniers->galeria_id->FldCaption() ?></span></button>
<?php } ?>
<input type="hidden" name="s_x_galeria_id" id="s_x_galeria_id" value="<?php echo $notiliniers->galeria_id->LookupFilterQuery() ?>">
</span>
<?php echo $notiliniers->galeria_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php if (!$notiliniers_edit->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $notiliniers_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
fnotiliniersedit.Init();
</script>
<?php
$notiliniers_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$notiliniers_edit->Page_Terminate();
?>
