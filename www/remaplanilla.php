<?php

header("Pragma: public"); 
header("Expires: 0"); 
if (isset($_GET["xlsname"])){
$xlsname = $_GET["xlsname"];
}else{
$xlsname = '';
}
$filename = $xlsname.".xls"; 
header("Content-type: application/x-msexcel"); 
header("Content-Disposition: attachment; filename=$filename"); 
header("Pragma: no-cache"); 
header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 

$miURL = $_SERVER['SCRIPT_NAME'];

include 'includes/MySQL.php';

Conectarse();


if (isset($_GET["remaid"])){
$remaid = $_GET["remaid"];
}else{
$remaid = '';
}

$sql="SELECT
lotes.id AS loteid,
lotes.activo,
lotes.venta_tipo,
lotes.vendido,
lotes.remate_id,
lotes.fecha_film,
lotes.establecimiento,
lotes.remitente,
lotes.tipo,
lotes.categoria,
lotes.sub_categoria,
lotes.codigo,
lotes.orden,
lotes.titulo,
lotes.precio,
lotes.cantidad,
lotes.peso,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
lotes.provincia,
lotes.localidad,
lotes.plazo,
localidades.id AS locaid,
localidades.nombre AS locanombre,
provincias.id,
provincias.nombre AS provinombre,
provincias.pais_id,
categorias_ganado.id,
categorias_ganado.nombre AS catenombre,
sub_categorias.id,
sub_categorias.nombre,
sub_categorias.abreviatura AS subcateabrev,
remates.id AS remaid,
remates.fecha,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
remates.banner_g,
remates.banner_ch,
remates.archivo,
lotes.edad_aprox,
lotes.trazabilidad,
lotes.mio_mio,
lotes.garrapata,
lotes.caracteristicas,
tipos.id AS tipoid,
tipos.nombre AS tiponombre

FROM
lotes
Left Join tipos ON tipos.id = lotes.tipo
Left Join provincias ON provincias.id = lotes.provincia
Left Join localidades ON localidades.id = lotes.localidad
Left Join categorias_ganado ON categorias_ganado.id = lotes.categoria
Left Join remates ON remates.id = lotes.remate_id
Inner Join sub_categorias ON sub_categorias.id = lotes.sub_categoria
WHERE
lotes.remate_id =  '$remaid'
ORDER BY ifnull(lotes.orden, catenombre)";

$rs = mysql_query($sql);

function replaceMayus($campo){
	$strfinal = $campo;
	$strfinal = str_replace("á","Á",$strfinal);
	$strfinal = str_replace("é","É",$strfinal);
	$strfinal = str_replace("í","Í",$strfinal);
	$strfinal = str_replace("ó","Ó",$strfinal);
	$strfinal = str_replace("ú","Ú",$strfinal);
	$strfinal = str_replace("ü","Ü",$strfinal);
	$strfinal = str_replace("ñ","Ñ",$strfinal);
	return $strfinal;
	}

?>
<html>
<head>
<style type="text/css">
.txtRef
{
font-family: Trebuchet MS;
font-size:10pt;
font-weight:bold;
color:#333333;
text-align:center;
vertical-align:middle;

}
.txtCont
{
font-family: Trebuchet MS;
font-size:10pt;
color:#333333;
text-align:center;
vertical-align:middle;
}
.txtCate
{
font-family: Trebuchet MS;
font-size:10pt;
font-weight:bold;
color:#ffffff;
text-align:center;
vertical-align:middle;
}
.txtDatos
{
font-family: Trebuchet MS;
font-size:10pt;
font-weight:bold;
color:#333333;
text-align:left;
vertical-align:middle;
}
</style>	
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">
<META HTTP-EQUIV="Expires" CONTENT="0"> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<title>:: Urioste, M&eacute;ndez Casariego &amp; Canessa S.A. ::</title>
<table id="tablaTotal" cellspacing="0" cellpadding="2" border="2" bordercolor="#000000" align="center" height="100%" width="620" align="center">
<tr>
<td>
<table id="tablaInt" cellspacing="0" cellpadding="2" border="1" bordercolor="#ffffff" align="center" height="100%" width="620" align="center">
  <tr><td height="20"></td></tr>
  <?php
   $cuentatotal=0;
   $cuentacolor = 0;
   $catenombre = '';
   
   while ($row = mysql_fetch_assoc($rs))
	 {
	 
	 if($row['precio']){
		 $arrPrecio =  explode(".",$row['precio']);
	   if(strlen($arrPrecio[0]) > 2){
		  $precio = '$'.$arrPrecio[0];
		 }else{
		 	$preciodec = $arrPrecio[1];
		  $precio = '$'.$arrPrecio[0].'.'.$preciodec;
		 }
	 }else{
		$precio = '-';
	 }
	 if($cuentatotal==0){
   	?>
   	 <tr>
	     <td id="conteBannerCH" colspan="9" height="240" align="center"><img src="http://uv0007.us45.toservers.com/repositorio/<?php echo $row['banner_ch'] ?>" height="240"></td>
	    </tr>
   	<?php
   }
	 if($catenombre==$row['catenombre']){
	 
	}else{	
	 $catenombre = $row['catenombre'];
	?>
	
	<tr>
	  <td height="25" colspan="9" bgcolor="808080" align="left" valign="middle" class="txtCate" style="border:1px solid #FFFFFF;"><?php echo strtoupper(utf8_decode($row['catenombre'])) ?></td>
	</tr>
	<tr>
  	<td height="20" width="30" bgcolor="#ffcc00" class="txtRef">Lote N&deg;</td>
    <td height="20" width="40" bgcolor="#ffcc00" class="txtRef">Categ.</td>
    <td height="20" width="30" bgcolor="#ffcc00" class="txtRef">Cant.</td>
    <td height="20" width="70" bgcolor="#ffcc00" class="txtRef">Kgs.</td>
    <td height="20" width="40" bgcolor="#ffcc00" class="txtRef">MIO MIO.</td>
    <td height="20" width="40" bgcolor="#ffcc00" class="txtRef">Grpta.</td>
    <td height="20" width="150" bgcolor="#ffcc00" class="txtRef">Localidad</td>
    <td height="20" width="100" bgcolor="#ffcc00" class="txtRef">Pcia.</td>
    <td height="20" width="120" bgcolor="#ffcc00" class="txtRef">&nbsp;&nbsp;&nbsp;Precio&nbsp;&nbsp;&nbsp;</td>
  </tr>
	<?php
  }
  
  if($row['orden']==''){
  	$orden=$row['codigo'];
  }else{
  	$orden=$row['orden'];
  	if($orden != ''){
	  	if(substr($orden, 0, 2)=="00"){
	  	  $orden = str_replace("00","",$orden);	
	    	if(substr($orden, 0, 1)=="0"){
	    	$orden = str_replace("0","",$orden);
	      }	
	  	}else{
	  	if(substr($orden, 0, 1)=="0"){
	  		$orden = substr_replace($orden, "", 0, 1); 
	     }	
	  	}
	 }
  }
  if($cuentacolor==0){
  	$rowcolor="FFFFFF";
  	$preciocolor = "e5e5e5";
  }else{
  	$rowcolor="f2f2f2";
  	$preciocolor = "d8d4d4";
  }
	?>
  <tr>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><b><?php echo $orden ?></b></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><?php echo strtoupper(replaceMayus($row['subcateabrev'])) ?></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><b><?php echo $row['cantidad'] ?></b></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><?php echo $row['peso'] ?></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><?php echo $row['mio_mio'] ?></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont"><?php echo $row['garrapata'] ?></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont" style="text-align:left;"><?php echo strtoupper(replaceMayus($row['locanombre'])) ?></td>
    <td bgcolor="#<?php echo $rowcolor ?>" class="txtCont" style="text-align:left;"><?php echo strtoupper(replaceMayus($row['provinombre'])) ?></td>
    <td bgcolor="#<?php echo $preciocolor ?>" class="txtRef" style="borde:1px solid #666666;"><?php echo $precio ?></td>
  </tr>
 <?php
 if($cuentacolor==0){
 	$cuentacolor=1;
 }else{
	$cuentacolor=0;
	}
	$cuentatotal+=1;	
 }
 ?>
 <tr bgcolor="#000000">
   <td colspan="9" height="2" bgcolor="#000000"></td>
 </tr>
 <tr>
   <td colspan="9" height="40" bgcolor="#ffffff"></td>
 </tr>
 <tr bgcolor="#000000">
   <td colspan="9" height="2" bgcolor="#000000"></td>
 </tr>
 <tr bgcolor="#ffffff">
   <td colspan="2" class="txtDatos">Informes:</td>
   <td colspan="4" height="25" class="txtDatos">
    Guillermo Roca (0379) 15-4660342
   </td>
   <td colspan="2" height="25" class="txtDatos">
    Fredy Casal (03777) 15-633574
   </td>
   <td></td>
 </tr>
 <tr bgcolor="#ffffff">
   <td colspan="2"></td>
   <td colspan="4" height="25" class="txtDatos">
    Ezequiel Roca (0379) 15-4223127
   </td>
   <td colspan="2" height="25" class="txtDatos">
    Francisco Méndez Casariego (011)15-54021907
   </td>
   <td></td>
 </tr>
 <tr bgcolor="#ffffff">
   <td colspan="2"></td>
   <td colspan="4" height="25" class="txtDatos">
    Luis Biaus (011) 15-41413792
   </td>
   <td colspan="2" height="25" class="txtDatos">
    Luis Lawler (0379) 15-4687032
   </td>
   <td></td>
 </tr>
 <tr bgcolor="#ffffff">
   <td colspan="2"></td>
   <td colspan="4" height="25" class="txtDatos">
    Diego Gomez Coll (0379) 15-4687231
   </td>
   <td colspan="2" height="25" class="txtDatos">
    Federico Casal (h)  (03777) 15-603882
   </td>
   <td></td>
 </tr>
 <tr bgcolor="#ffffff">
   <td colspan="2"></td>
   <td colspan="4" height="25" class="txtDatos">
    Nicolás Canessa (011) 15-50080951
   </td>
   <td colspan="2" height="25" class="txtDatos">
    Casa Central (011) 4780-1278
   </td>
   <td></td>
 </tr>
</table>
   </td>
 </tr>
</table>
</td>
</tr>
</table>
 