<?php
$sqlCAT = "SELECT DISTINCT
SQL_CALC_FOUND_ROWS categorias_ganado.id,
categorias_ganado.id AS cateidselect,
categorias_ganado.nombre AS catenomselect,
categorias_ganado.descripcion,
lotes.categoria,
lotes.remate_id
FROM
categorias_ganado";


$sqlVisitLotesCate="SELECT
lotes.id AS loteid,
lotes.activo,
lotes.remate_id AS remaid,
lotes.tipo AS tipoid,
lotes.categoria AS cateid,
lotes.sub_categoria AS subcateid,
lotes.visitas,
categorias_ganado.nombre,
categorias_ganado.id,
tipos.id,
tipos.nombre AS tiponom,
remates.id,
remates.tipo,
remates.fecha AS remafecha,
remates.lugar AS remalugar,
remates.titulo AS rematitu,
localidades.id,
localidades.nombre AS locanom,
provincias.id,
provincias.nombre AS provinom,
SUM(lotes.visitas) as sumavisitas
FROM
lotes
INNER JOIN categorias_ganado ON categorias_ganado.id = lotes.categoria
INNER JOIN tipos ON lotes.tipo = tipos.id
INNER JOIN remates ON lotes.remate_id = remates.id
INNER JOIN provincias ON lotes.provincia = provincias.id
INNER JOIN localidades ON lotes.localidad = localidades.id
WHERE
".$whereRema ."
lotes.visitas <> '0'
GROUP BY categorias_ganado.nombre
ORDER BY sumavisitas DESC
LIMIT 0, 20";
?>
