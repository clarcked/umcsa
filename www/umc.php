<?php
require_once("app/config/bootstrap.php");
use Umc\App\Services\Auth\Auth;
$auth = new Auth();
?>
<!DOCTYPE html>

<?php
$pageID = 'nos';
?>

<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>:: UMC S.A. Negocios Agropecuarios ::</title>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="includes/funcFondopopup.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script type="text/javascript">

  $(document).ready(function(){

	$("#abreStaff").click(function(e){
		$("#staffTD").fadeIn(1500);
		$("#conteTD").fadeOut(50);

	});
	$("#cierraStaff").click(function(e){
		$("#conteTD").fadeIn(1500);
		$("#staffTD").fadeOut(50);
	});

  })


  function cambiarEstado()
  {
  window.status="UMC S.A. Negocios Agropecuarios - Integrantes"
  }
  cambiarEstado();
  function cambiaFondo(pId, pColor){
	document.getElementById(pId).style.backgroundColor=pColor;
  }
  function abreDiv(pDiv, pImagenbtn){
	  varDisplay=document.getElementById(pDiv).style.display;
	  //alert(varDisplay);
	  if(varDisplay=='none'){
		document.getElementById(pDiv).style.display='';
		pImagenbtn.src='images/btn-div-cierra.png';
		}else{
		document.getElementById(pDiv).style.display='none';
		pImagenbtn.src='images/btn-div-abre.png';
		}
  }
  function cambiaClase(pId, pClase){
	document.getElementById(pId).className = pClase;
  }
  </script>

<?php
  include 'includes/funciones.php';
?>
</head>

<body>





<?php
  include 'header.php';
?>


<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close cierramodal" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Envienos su consulta</h3>
	  </div>
	  <div class="modal-body">

		<form class="form-horizontal col-sm-12" method="POST" name="contacto" onSubmit="return validar(this)" action="contacto-send.php">
		  <div class="form-group"><label>Asunto</label><input id="subjectH" name="subjectH" type="text" value="<?php echo $subject ?>" class="form-control required" placeholder="Asunto" data-placement="top" data-trigger="manual"></div>
		  <div class="form-group"><label>Nombre y Apellido</label><input id="name" name="name" type="text" class="form-control required" placeholder="Tu Nombre y Apellido" data-placement="top" data-trigger="manual"></div>
		  <div class="form-group"><label>Empresa</label><input id="empresa" name="empresa" type="text" class="form-control required" placeholder="Tu Empresa" data-placement="top" data-trigger="manual"></div>
		  <div class="form-group"><label>E-Mail</label><input id="email" name="email" type="text" class="form-control email" placeholder="email@you.com" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)"></div>
		  <div class="form-group"><label>Consulta</label><textarea id="message" name="message" class="form-control" placeholder="Mensaje.." data-placement="top" data-trigger="manual"></textarea></div>
		  <div class="form-group"><button type="submit" class="btn btn-success pull-right">Send It!</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  </div>
	</div>
  </div>
</div>

<div class="headtop headcel">
  <div class="container">
	<div class="row ">
	  <div class="col-sm-6 col-lg-7">
		<h1>UMC &#8226; Haciendas Villaguay</h1>
		<p>Nuestras empresas tienen distintos orígenes, similares historias y muchas cosas en común, Santa Fé, Entre Ríos, Corrientes, Buenos Aires, La Pampa. No importa, da igual</p>
		<p>Nuestro ADN es el campo, somos productores y nuestra pasión los negocios ganaderos. Nuestro principal valor: la palabra</p>
	  </div>
	  <div class="col-sm-6 col-lg-5 fs11">
		<p>A estos conceptos -transmitidos de generaciones anteriores- le sumamos el esfuerzo y el profesionalismo que exige la mecánica del mercado agro-ganadero actual. El resultado es lo que somos.</p>
		<p>Confiabilidad, seriedad, flexibilidad, capacidad resolutiva, cumplimiento. Nada distinto a lo que nuestros clientes esperan de nosotros. Pero nosotros se lo brindamos.</p>
		<p><b>UMC & Haciendas Villaguay <br> La diferencia es el equipo.</b></p>
	  </div>
	</div>
  </div>
</div>

<div class="container pb50">
  <div class="row" id="conteTD">

	<div class="col-sm-6 nos_info">
	  <h1 >REMATES TELEVISADOS</h1>
	  <p>Esta revolucionaria forma de comercializar la hacienda, en especial de invernada y cría, ha roto todas las barreras, <b>acortando distancias y disminuyendo costos</b>. Hacienda de todo el país es ofrecida para la venta a través de la pantalla de TV por Canal Rural a compradores, también de todo el país.</p>
	  <p>Somos una de las empresas líderes en remates televisadosm especialmente en la Mesopotamia Argentina y en general en toda la zona del NEA y NOA de nuestro país</p>
	  <p>Tenemos un intenso calendario de remates, y lo planificamos rotando entre <b>nuestras principales sedes en Entre Ríos, Corrientes, Misiones, Formosa, Chaco y Buenos Aires.</b></p>
	  <h2>COMPRADORES</h2> 
	  <p><b>Pueden comprar durante la transmisión en vivo llamando al 0810-333-7872 o lo que es más habitual, a los teléfonos de nuestros representantes</b> que se encuentren en la sede del remate.</p>
	  <p>En el caso de no ser cliente de UMC ni de Haciendas Villaguay, deberá comunicarse con nuestro equipo con anterioridad para solicitar su crédito. Esta comunicación previa es muy importante, porque además del otorgamiento del crédito, le informamos sobre las <b>distintas posibilidades de financiación</b>, tarjetas agro vigentes y líneas de préstamos especiales por acuerdos con entidades bancarias.</p>
	  <h2>QUIERO CONSIGNAR MI HACIENDA</h2>
	  <p>Si usted desea consignar su hacienda para comercializarla a través de nuestros remates televisados, le recomendamos contactarnos vía telefónica.</p>
	  <p><b>Nuestra gente se encargará de visitarlo y filmar su hacienda en su campo</b>. Esto debe realizarse con una telación mínima de 10 días de la fecha del remate. Es importante respetar el plazo para dar tiempo a una prolija edición de su lote, junto al resto de las consignaciones recibidas</p>
	  <p>48 hs antes del remate, <b>todos los lotes filmados que saldrán a la venta se suben a este sitio web </b>, con el fin de que los potenciales compradores puedan verlos y apreciarlos con tranquilidad, para así ir decidiendo su compra -o al menos su interés- con anticipación.</p>
	  <p>Aquí dejamos a su disposición el <a href="">Reglamento de compra / venta.</a></p>
	</div>
	<div class="col-sm-6 nos_info2">
	  <h1>REMATES FERIA Y ESPECIALES DE CABAÑAS</h1>
	  <p>Además de los remates televisados, <b>continuamos realizando los tradicionales remates ferias y especiales de reproductores y vientres</b> de las más reconocidas Cabañas del país</p>
	  <p>La Sociedad Rural de Villuaguay, Sociedad Rural de Esquina, así como la Soc. Rural de Clorinda, Pilcomayo, o las instalaciones San Jorge en Rosario del Tala, entre otras, son ferias que mantenemos vigentes para los productores que prefieran esta alternativa de venta más tradicional y que forma parte de la hsitoria del campo argentino.</p>
	  <p>En el cronograma de remates se publican las próximas fechas.</p>
	  <h1>MERCADO DE LINIERS Y OPERACIONES DIRECTOS</h1>
	  <p>En hacienda para faena, desde hace más de 100 años, el tradicional Mercado de Liniers es la referencia indiscutida. Por ello, ofrecemos a nuestros clientes la posibilidad de <b>vender su gordo en nuestra propia casilla dentro de este Mercado</b>, la cual opera a diario.</p>
	  <p>Otra alternativa muy utilizada -y que entendemos es una fortaleza de nuestra empresa-, son las <b>operaciones directas</b>. Contamos con una cartera de clientes amplia y trabajamos con los frigoríficos y supermercados mas importantes del país, a quienes abastecemos para su faena diaria con todas las categorías de hacienda, sea para consumo interno o exportación.</p>
	  <p>Consúltenos. También somos productores. Sabemos de que se trata</p>

	  <div class="ldesc">
		<b>Descargas:</b>
		<!--<div class="lreglamento"><a href="pdf/reglamento.pdf" target="_blank">REGLAMENTO DE COMPRA/VENTA</a></div>-->
		<div class="lcertificado"><a href="xls/certificado.xls" target="_blank">CERTIFICADO DE FILMACIÓN</a></div>
	  </div>

	</div>
	
			<div class="col-md-6 col-md-offset-1 infosigna hidden">
				  <div class = "page-header umc_item" >
					<h2>Sobre UMC</h2>
				  </div>
				  <p class="lead">
					<b>UMC</b> es una empresa de servicios orientada a ofrecer la mejor calidad en los negocios rurales o vinculados al campo argentino. Es un equilibrio entre la vasta experiencia de familias dedicadas desde siempre al campo y el esfuerzo de un equipo de profesionales jóvenes altamente capacitados para la dinámica del mercado agro-ganadero.
				<br><br>
					<b>Experiencia y profesionalismo, juntos para los buenos negocios rurales.</b>
				<br><br>
					Y es, así mismo, confianza. No solo aquella que hemos ganado de nuestros clientes, sino también la que tenemos en nosotros mismos, confianza que nos permite saber que obtenemos resultados superiores a sus exigencias.
				<br><br>
					Hoy lo invitamos a conocernos y lo esperamos para invertir en nuestro país, en el que tanto la ganadería como la agricultura son mundialmente reconocidas por la calidad de sus producciones.
				  </p>

				  <div class = "umc_item">
					<h2>Remates Televisados</h2>
					<hr>
				  </div>
				  <p class="lead">
					Desde el 2010 realizamos nuestras ventas en vivo por Canal Rural desde las Provincias de Corrientes y Misiones para todo el pais.
				<br><br>
					Los remates televisados consisten en mostrar la hacienda a través de la pantalla y la compra se realiza vía telefónica desde cualquier punto del país. El beneficio de este práctico formato de ventas es acortar distancias y en consecuencia, reducir gastos para el productor.
				  </p>

				  <div class = "umc_item">
					<h2>Compradores</h2>
					<hr>
				  </div>
				  <p class="lead">
					Pueden comprar durante la transmisión en vivo llamando al 0810-333-7872 o a los teléfonos de nuestros representantes que se encontrarán en el piso.
				<br><br>
					En el caso de no ser cliente de la firma UMC, le solicitamos que se comunique telefónicamente o vía email con anterioridad. Además será informado sobre las distintas posibilidades de financiación y fletes.
				<br><br>
					Para ser un comprador habilitado, solicite o actualice su crédito con UMC S.A., complete el:
				<br><br>
					<a href="javascript:abreFormC('formCredito')"><u><b>FORMULARIO DE SOLICITUD DE CRÉDITO</b></u></a>
				  </p>

				  <div class = "umc_item">
					<h2>Quiero consignar mi hacienda</h2>
					<hr>
				  </div>
				  <p class="lead">
					Si usted quiere consignar su hacienda para que UMC la venda a través de los remates televisados, debe contactarnos vía telefónica y luego nos encargaremos de filmar su hacienda en su campo.
				<br><br>
					Esto debe realizarse mínimo 10 días antes del día del remate, para luego poder ser editado junto a todas las consignaciones y asi armar el órden de venta. Además, las imágenes de los lotes se podrán ver previamente aquí en nuestro sitio web, en la sección PRÓXIMOS REMATES con los detalles correspondientes.
				<br><br>
					Aquí dejamos a su disposición el Reglamento de compra/venta.   <a href="pdf/reglamento.pdf" target="_blank"><u><b>ABRIR</b></u> <img src="images/ico-pdf-mini-off.png" border="0"></a>
				  </p>


			</div>

			<div class="col-md-4 colder hidden">
				<img src="images/boton-quienes-somos-off.jpg" id="abreStaff" width="312" onmouseover="this.src='images/boton-quienes-somos-on.jpg'" onmouseout="this.src='images/boton-quienes-somos-off.jpg'" style="cursor:pointer;">
				<table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="60" style="border: 0px dotted #b1b0b0; background-color: #ffedab;margin-top: 15px;">
				  <tr>
					<td height="20" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td height="10" align="left"><img src="images/titu-descargas.jpg"></td>
				  </tr>
				  <tr>
					<td height="5" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td class="txtSobreumc" style="font-size:8pt;">DOCUMENTACIÓN PARA REMATES TELEVISADOS</td>
				  </tr>
				  <tr>
					<td height="10" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td class="txtSobreumc" style="font-size:8pt;"><a href="xls/certificado.xls" target="_blank"><u><b>CERTIFICADO DE FILMACIÓN</b></u>&nbsp;&nbsp;<img src="images/ico-xls-mini-off.jpg" border="0"></a></td>
				  </tr>
				  <tr>
					<td height="7" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td class="txtSobreumc" style="font-size:8pt;"><a href="pdf/conformidad-carga.pdf" target="_blank"><u><b>DOCUMENTO DE CONFORMIDAD DE CARGA</b></u> <img src="images/ico-pdf-mini-off.png" border="0"></a></td>
				  </tr>
				  <tr>
					<td height="7" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td class="txtSobreumc" valign="bottom" style="font-size:8pt;"><a href="pdf/reglamento.pdf" target="_blank"><u><b>REGLAMENTO COMPRA/VENTA</b></u> <img src="images/ico-pdf-mini-off.png" border="0"></a></td>
				  </tr>
				  <tr>
					<td colspan="2" height="20"></td>
				  </tr>
				</table>


				<table cellspacing="0" cellpadding="0" border="0" id="" width="100%" height="60" style="border: 0px dotted #b1b0b0;background-color: #d0d0d0;margin-top: 15px;margin-bottom: 150px;">
				  <tr>
					<td height="20" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td height="10" align="left"><img src="images/titu-umc-infoguias.jpg"></td>
				  </tr>
				  <tr>
					<td height="10" colspan="2"></td>
				  </tr>
				  <tr>
					<td width="20"></td>
					<td class="txtSobreumc" style="font-size:11pt; letter-spacing:1px;">

					Consignada: U.M.C. S.A.<br>
														CUIT: 30-70810065-6<br>ONCCA: 92934-4<br>
														Venta en Mercado de Liniers S.A.:<br>
														CUIT: 30-64902393-6<br>ONCCA: 00010-8

					</td>
				  </tr>
				  <tr>
					<td colspan="2" height="20"></td>
				  </tr>
				</table>


			</div>

  </div>
</div>


<div class=" bggrisre">
  <div class="container relative">
	<div class="row bggris">

	  <div class="juancho"><img src="images/juancho.png" alt=""></div>

          <div class="col-md-4" style="width: 310px; margin-right: 20px;">
		<iframe style="margin:auto;" src="http://www.delsector.com/ifr-umc.php" frameborder="0"  width="312" height="254"></iframe>
	  </div>
	  <div class="col-md-4 box2">
		<h2 class="a">UMC S.A.</h2>
		<p>Vuelta de Obligado 1947 CABA - Argentina</p>
		<p>CASA CENTRAL: +54 11 4780-1278</p>
		<p>E-mail: info@umcsa.com.ar</p>

		<h2 class="b">HACIENDAS VILLAGUAY S.R.L.</h2>
		<p>Urquiza 454 - Villaguay (Entre Ríos) - Argentina</p>
		<p>Tel: (03455) 421050</p>
		<p>E-mail: haciendasvillaguay@viguay.com.ar</p>

	  </div>
	  <div class="col-md-4 box3 text-left">
		<p class="a"><a href="">QUIERO CONSIGNAR MI HACIENDA</a></p>
		<hr>
		<p class="b"><a href="">SOLICITAR CRÉDITO</a></p>
		<hr>
		<p class="c"><a href="">REPRESENTANTES</a></p>
	  </div>
	</div>
  </div>
</div>

<div class="row">
  <div id="staffTD" style="display:none; position:relative;" class="col-md-10 col-md-offset-1">
	  <div class="row">
		  <?php include 'staff.php' ?>
	  </div>
  </div>
</div>

















</div>
<br><br>
<div id="divfooter" style="position: fixed; bottom: 0px; z-index: 1000; text-align: center; margin-top: 20px; margin-bottom: 0px; padding: 0px; left: 0px; right: 0px; width: auto; height: 30px; background-color: #444444;">
        <?php include 'footer.php'?>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
	
</body>
</html>
