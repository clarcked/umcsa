rematedeta_n.php line 496
rematedeta_a.php line 420

- Mostrar las tres ultimas preofertas
  - solicitud a un api 
    recupero las 3 ultimas preofertas mas altas por remate_id y lote_id
    order by valor desc
    where remate_id = x and lote_id = y
    join left remate_id para obtener informaciones del remate
  - icon: ico-price-ribbon, ico-minus, ico-plus, ico-smile
  - bg.color: #2d3436 dark, #00a988 green
  - en la vista se muestra fecha y hora de la preoferta y el valor
  - fecha en formato: dd/mm/aaaa HH:mm
  - apertura y cierre de la pauta de este remate si tiene ( las pautas son por remate )
  - corregir la leyenda de la tabla pautas (leyenda_espacial)

  - utilizar fetch o axios? 
  - si el usuario no esta loggeado no muestra nada ?

  - Indicar la fecha de apertura y la fecha de cierre
  - Formato de fecha:  DD/MM/AAAA HH:mm 


- Modulo de preoferta
  - Mostrar un boton para hacer preoferta simpre cuando el remate tiene pautas de preoferta.
  - El usuario incrementa un valor de preoferta y luego apreta el boton de preoferta.
  - al guardar la preoferta recibe un email de confirmacion el staff tambien recibe un email informando que un usuario hizo una preoferta.
  - si no incrementa nada no puede seguir con la operacion
  - no puede descrementar menos que el valor de la ultima oferta o 0
  - no se puede guardar un oferta menor o igual a la ultima oferta o 0
  - Pedir una confirmacion antes de guardar la preoferta
  - si el usuario de la ultima oferta es el mismo que el usuario loggeado entonces este tiene la oferta mas alta

- Creation de Preoferta
  - Recuperar los datos desde el body
  - validar los datos a partir de los campos que quiero guardar
  - Armar la preoferta
  - Antes de guardar:
    - Pasamos todo el mundo como perdedor porque lo que sigue es la mas alta
    - Al guadar la oferta asigna todas las preofertas de este lote anteriores como perdedores y a esta nueva como ganador
    - Si el usuario anteriormente tenia preoferta para este lote cambiamos el estado a "-1" ( cancelado ) esto te salva la vida cuando solo quieras listar las ofertas mas alta de este usuario sin tener lote duplicado.
  - Guardar la preoferta
  - Despues de guardar
    - Notificamos por email:
      - al staff para avizar que hubo una preoferta
      - al usuario para avizar que su oferta es la mas alta
      - al ultimo usuario que tenia la mas alta para avisar que lo superaron

- Historial de preoferta:
  - mostrar las preoferta mas alta y superada (estado 1 y 0) del usuario
  - excluir las canceladas estado -1
  - campos visibles:
    - REMATE	FECHA	FECHA CIERRE	LOTE	TITULO	LUGAR	VALOR LINK
  - Un link en la superada que lleva a la pagina de remate 
    - variable get : remaid, loteid

- Api
  - entity
    - GET: 
      - /  
        - las 100 primeras entities
      - /?filter=[one,all]&fields=[key1,key2]&val=[val1,val2]&join=[table]&limit=[10]&offset=[10]&mode=[escalar,normal]&order=[key1,asc]
        - las entities segun el filtro
        - RequestFilter class para gestionar eso
    - POST:
      - /
        - body as json

      
